#!/bin/bash

function update_expansion()
{
    APK_VERSION=`cat ./android/AndroidManifest.xml  | grep versionCode |  sed 's/[^0-9]*//g'`
    OLD_VERSION=`cat ./core/src/com/blotunga/bote/constants/GameConstants.java  | grep apkExpansionFileVersion |  sed 's/[^0-9]*//g'`
    OLD_SIZE=`cat ./core/src/com/blotunga/bote/constants/GameConstants.java  | grep apkExpansionFileSize |  sed 's/[^0-9]*//g'`
    sed -i "s/apkExpansionFileVersion    = ${OLD_VERSION}/apkExpansionFileVersion    = ${APK_VERSION}/g" core/src/com/blotunga/bote/constants/GameConstants.java
    pushd assets
    zip main.$APK_VERSION.com.blotunga.bote.android.zip -0 -r ./graphics ./sounds
    mv main.$APK_VERSION.com.blotunga.bote.android.zip ../main.$APK_VERSION.com.blotunga.bote.android.obb
    NEW_SIZE=`ls -nl ../main.$APK_VERSION.com.blotunga.bote.android.obb | awk '{print $5}'`
    popd
    sed -i "s/apkExpansionFileSize      = ${OLD_SIZE}/apkExpansionFileSize      = ${NEW_SIZE}/g" core/src/com/blotunga/bote/constants/GameConstants.java
}

function patch_build_gradle()
{
    export APK_ANDROID_VERSION=`cat ./android/AndroidManifest.xml  | grep versionName |  sed 's/[^0-9.]*//g'`
    export BUILD_GRADLE_VERSION=`cat ./build.gradle  | grep " version = " |  sed 's/[^0-9.]*//g'`
    sed -i "s/version = '${BUILD_GRADLE_VERSION}'/version = '${APK_ANDROID_VERSION}'/g" build.gradle
}

function restore_build_gradle()
{
    sed -i "s/version = '${APK_ANDROID_VERSION}'/version = '${BUILD_GRADLE_VERSION}'/g" build.gradle
}

function check_args()
{
    for var in $@; do
        if [[ $var = "--exp" || $var = "-e" ]]; then
            need_update_expansion=true
        fi
        if [[ $var = "--google" || $var = "-g" ]]; then
            gen_google=true
        fi
        if [[ $var = "--amazon" || $var = "-a" ]]; then
            gen_amazon=true
        fi
        if [[ $var = "--standalone" || $var = "-s" ]]; then
            gen_standalone=true
        fi
        if [[ $var = "--desktop" || $var = "-d" ]]; then
            gen_desktop=true
        fi
        if [[ $var = "--all" ]]; then
            gen_google=true
            gen_amazon=true
            gen_standalone=true
            gen_desktop=true
        fi

    done
}

function release_google()
{
    mv -f ./assets/graphics ../assets
    mv -f ./assets/sounds ../assets
    touch ./assets/data/googlemarker
    if [[ $need_update_expansion = true ]]; then
        update_expansion
    fi
    ./gradlew android:assembleRelease
    mv -f ../assets/* ./assets/
    rm -f ./assets/data/googlemarker
    cp ./android/build/outputs/apk/release/android-release-unsigned.apk ../../packaging
    pushd ../../packaging
    ./sign.sh
    popd
}

function release_amazon()
{
    touch ./android/assets/data/amazonmarker
    ./gradlew android:assembleRelease
    rm -f ./android/assets/data/amazonmarker
    cp ./android/build/outputs/apk/release/android-release-unsigned.apk ../../packaging
    pushd ../../packaging
    ./sign.sh
#    mv ./birth-of-the-empires.apk $APK_NAME-com.blotunga.bote.android-bote-nodrm.apk
    mv ./birth-of-the-empires.apk birth-of-the-empires-amazon.apk
    popd
}

function release_standalone()
{
    ./gradlew android:assembleRelease
    cp ./android/build/outputs/apk/release/android-release-unsigned.apk ../../packaging
    pushd ../../packaging
    ./sign.sh
    mv ./birth-of-the-empires.apk birth-of-the-empires-standalone.apk
    popd
}

function release_desktop()
{
    BUILD_VERSION=`cat ./android/AndroidManifest.xml  | grep versionName |  sed 's/[^0-9.]*//g'`
    ./gradlew desktop:dist
    cp ./desktop/build/libs/desktop-$BUILD_VERSION.jar ../../packaging
    pushd ../../packaging
    rm -fR ./bote-linux-x64
    rm -fR ./bote-windows
    rm -fR ./bote-windows-xp
    ./pack.sh $BUILD_VERSION
    popd
}

if [[ ($# < 1) ]]; then
    echo "Release script usage:"
    echo "	-g --google	- release for google play"
    echo "	-e --exp	- generate expansion file, use with -g"
    echo "	-a --amazon	- release for amazon"
    echo "	-s --standalone	- release as standalone apk"
    echo "	-d --desktop	- release for desktop"
    echo "	--all		- release for all supported. Gradle might crash sometimes so not really recommended"
fi
check_args $@
#because build.gradle is always a version ahead of the current version, this allows it to stay in sync with the android version for the release's purpose
patch_build_gradle
if [[ $gen_amazon = true ]]; then
    release_amazon
fi
if [[ $gen_standalone = true ]]; then
    release_standalone
fi
if [[ $gen_google = true ]]; then
    release_google
fi
if [[ $gen_desktop = true ]]; then
    release_desktop
fi
restore_build_gradle
