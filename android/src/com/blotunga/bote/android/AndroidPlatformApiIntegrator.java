/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import java.util.Collections;
import java.util.Iterator;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.PlatformApiIntegration;
import com.blotunga.bote.PlatformCallback;
import com.blotunga.bote.achievements.AchievementsList;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.android.gms.tasks.Task;

public class AndroidPlatformApiIntegrator implements PlatformApiIntegration {
    //TODO: amazon implementation if I ever have the time
    GoogleApiClient mGoogleApiClient;
    AndroidApplication launcher;
    private boolean mInSignInFlow = false;
    private boolean mExplicitSignOut = false;
    private final static int requestCode = 9099;
    public static int RC_SIGN_IN = 9001;
    private PlatformCallback callback;
    private boolean triedSignIn = false;
    private DriveIntegration driveIntegration;
    private StorageIntegration storageIntegration;
    private AchievementsClient mAchievementsClient;
    private GoogleSignInClient mGoogleSignInClient;
    
    public AndroidPlatformApiIntegrator(GoogleApiClient client, AndroidApplication launcher) {
        this.mGoogleApiClient = client;
        this.launcher = launcher;
        this.storageIntegration = new AndroidStorageIntegration(launcher);
    }

    @Override
    public void signIn() {
        triedSignIn = true;
        if (!mInSignInFlow && mGoogleSignInClient == null) {
            mInSignInFlow = true;
            mGoogleSignInClient = GoogleSignIn.getClient(launcher,
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestScopes(new Scope(DriveScopes.DRIVE_FILE), new Scope(DriveScopes.DRIVE_APPDATA))
                    .requestEmail().build());            
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            launcher.startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    public void initAfterSignIn(Intent intent) {
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(launcher.getContext());
        this.driveIntegration = new GoogleCloudIntegration(this);
        mAchievementsClient = Games.getAchievementsClient(launcher, signInAccount);        
        mAchievementsClient.load(true).addOnSuccessListener(new OnSuccessListener<AnnotatedData<AchievementBuffer>>() {
            @Override
            public void onSuccess(AnnotatedData<AchievementBuffer> achievementBufferAnnotatedData) {
                AchievementBuffer aBuffer = null;
                AndroidLauncher gameLauncher = (AndroidLauncher) launcher;
                try {
                    Achievement ach;
                    aBuffer = achievementBufferAnnotatedData.get();
                    Iterator<Achievement> aIterator = aBuffer.iterator();

                    while (aIterator.hasNext()) {
                        ach = aIterator.next();
                        AchievementsList alItem = AchievementsList.fromId(ach.getAchievementId());
                        if (ach.getState() != Achievement.STATE_UNLOCKED) {
                            if (ach.getType() == Achievement.TYPE_INCREMENTAL) {
                                int diff = ach.getCurrentSteps() - gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()];
                                if (diff < 0)
                                    incrementAchievement(alItem, Math.abs(diff));
                                else
                                    gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()] = ach.getCurrentSteps();
                            } else if (alItem.isUnlocked(gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()])) {
                                unlockAchievement(alItem);
                            }
                        } else if (ach.getState() == Achievement.STATE_UNLOCKED) {
                            gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()] = alItem.getValue();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (aBuffer != null)
                    aBuffer.close();
                if (callback != null)
                    callback.call();
                if (mInSignInFlow)
                    setInSignInFlow(false);
            }
        });
    }

    @Override
    public void signOut() {
        signOut(false);
    }

    @Override
    public void signOut(boolean explicite) {
        mExplicitSignOut = explicite;
        mGoogleSignInClient.signOut();
        mGoogleSignInClient = null;
        mAchievementsClient = null;
        triedSignIn = false;
    }

    public boolean isSignInFlow() {
        return mInSignInFlow;
    }

    public boolean isExplicitSignOut() {
        return mExplicitSignOut;
    }

    public void setInSignInFlow(boolean val) {
        mInSignInFlow = val;
    }

    @Override
    public boolean isConnected() {
        return triedSignIn && mGoogleSignInClient != null && mAchievementsClient != null && mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    @Override
    public void rateGame() {
        if (isConnected()) {
            Uri uri = Uri.parse("market://details?id=" + launcher.getContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button, 
            // to taken back to our application, we need to add following flags to intent. 
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                launcher.startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                launcher.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="
                        + launcher.getContext().getPackageName())));
            }
        }
    }

    public void unlockAchievement(AchievementsList achievement) {
        if (isConnected() == true) {
            mAchievementsClient.reveal(achievement.getId());
            mAchievementsClient.unlock(achievement.getId());
        }
    }

    @Override
    public void incrementAchievement(AchievementsList achievement, int value) {
        if (isConnected() == true) {
            mAchievementsClient.increment(achievement.getId(), value);
        }
    }

    @Override
    public void showAchievements() {
        if (isConnected() == true) {
            mAchievementsClient.getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                @Override
                public void onSuccess(Intent intent) {
                    launcher.startActivityForResult(intent, requestCode);
                }
            });
        }
    }

    @Override
    public PlatformType getPlatformType() {
        if (launcher instanceof AndroidLauncher)
            return ((AndroidLauncher) launcher).getPlatformType();
        else
            return ((EditorLauncher) launcher).getPlatformType();
    }

    @Override
    public boolean networkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager)launcher.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                              activeNetwork.isConnected();
        return isConnected;
    }

    @Override
    public void setCallback(PlatformCallback callback) {
        this.callback = callback;
    }

    @Override
    public DriveIntegration getDriveIntegration() {
        return driveIntegration;
    }

    @Override
    public StorageIntegration getStorageIntegration() {
        return storageIntegration;
    }
}
