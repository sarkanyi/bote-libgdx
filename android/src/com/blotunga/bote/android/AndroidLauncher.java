/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import android.view.WindowManager;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.blotunga.bote.AndroidIntegration;
import com.blotunga.bote.BotE;
import com.blotunga.bote.PlatformApiIntegration;
import com.blotunga.bote.PlatformApiIntegration.PlatformType;
import com.blotunga.bote.android.util.AndroidApkExpansionFileSetter;
import com.blotunga.bote.android.util.ZipFileHandleResolver;
import com.blotunga.bote.utils.ApkExpansionSetter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BotEBaseGameUtils;

public class AndroidLauncher extends AndroidApplication implements AndroidIntegration, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private AndroidApplicationConfiguration config;
    private GoogleApiClient mGoogleApiClient;
    private static int RC_SIGN_IN = 9001;
    public static final int REQUEST_DIRECTORY = 9111;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_SD = 11001;

    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInFlow = true;

    private AndroidPlatformApiIntegrator integrator;
    private ActivityManager.MemoryInfo mInfo;
    private FileHandleResolver resolver;
    private ApkExpansionSetter setter;
    private BotE bote;

    @SuppressLint("InlinedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= Build.VERSION_CODES.GINGERBREAD /* 9 */
                && sdkInt < Build.VERSION_CODES.JELLY_BEAN_MR2 /* 18 */) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else if (sdkInt < Build.VERSION_CODES.GINGERBREAD /* 9 */) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (sdkInt >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        }

        ActivityManager actvityManager = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
        mInfo = new ActivityManager.MemoryInfo();
        actvityManager.getMemoryInfo(mInfo);

        config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        config.numSamples = 2;
        resolver = null;
        setter = null;
        if (getPlatformType() == PlatformType.PlatformGoogle) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                    .addApi(Games.API).addScope(Games.SCOPE_GAMES) // Games API
                    .addApi(Drive.API).addScope(Drive.SCOPE_APPFOLDER).addScope(Drive.SCOPE_FILE) // Drive API
                    // add other APIs and scopes here as needed
                    .build();
            integrator = new AndroidPlatformApiIntegrator(mGoogleApiClient, this);
            resolver = new ZipFileHandleResolver();
            setter = new AndroidApkExpansionFileSetter();
        } else {
            integrator = new AndroidPlatformApiIntegrator(null, this);
        }
        bote = new BotE(this, (PlatformApiIntegration) integrator);
        initialize(bote, config);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //integrator.signIn();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //integrator.signOut();
    }

    public PlatformType getPlatformType() {
        String[] fileNames = { "data/googlemarker", "data/amazonmarker" };
        for (String fileName : fileNames) {
            try {
                this.getAssets().open(fileName).close(); // Check if file exists.
                if (fileName.contains("google"))
                    return PlatformType.PlatformGoogle;
                else if (fileName.contains("amazon"))
                    return PlatformType.PlatformAmazon;
            } catch (Exception ex) {
            }
        }
        return PlatformType.PlatformStandalone;
    }

    @Override
    public void onConnected(Bundle bundle) {
        // The player is signed in. Hide the sign-in button and allow the
        // player to proceed.
        ((AndroidPlatformApiIntegrator) integrator).initAfterSignIn(getIntent());
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Attempt to reconnect
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            // Already resolving
            return;
        }

        // If the sign in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        if (integrator.isSignInFlow() || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            integrator.setInSignInFlow(false);
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign in, please try again later."
            if (mGoogleApiClient != null && !BotEBaseGameUtils.resolveConnectionFailure(this, mGoogleApiClient, connectionResult,
                    RC_SIGN_IN, getString(R.string.signin_other_error))) {
                mResolvingConnectionFailure = false;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            integrator.setInSignInFlow(false);
            mResolvingConnectionFailure = false;
            if (resultCode == RESULT_OK) {
                mGoogleApiClient.connect();
            } else {
                // Bring up an error dialog to alert the user that sign-in
                // failed. The R.string.signin_failure should reference an error
                // string in your strings.xml file that tells the user they
                // could not be signed in, such as "Unable to sign in."
                BotEBaseGameUtils.showActivityResultError(this, requestCode, resultCode, R.string.sign_in_failed);
            }
        }

        if (requestCode == REQUEST_DIRECTORY) {
            if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                integrator.getStorageIntegration().setDirectory(intent
                    .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR));
            } else {
                // Nothing selected
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_SD: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    integrator.getStorageIntegration().setDirectory(integrator.getStorageIntegration().getDirectory());
                } else {
                    showAlert(getString(R.string.error), getString(R.string.permission_error));
                }
                return;
            }
        }
    }

    public void showAlert(final String title, final String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AndroidLauncher.this);
        builder.setMessage(message).setTitle(title).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ActivityCompat.requestPermissions(AndroidLauncher.this,
                        new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, MY_PERMISSIONS_REQUEST_WRITE_SD);
            }
        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public long getAvailableMemInfo() {
        return mInfo.availMem;
    }

    @Override
    public ApkExpansionSetter getApkExpansionSetter() {
        return setter;
    }

    @Override
    public FileHandleResolver getFileHandleResolver() {
        return resolver;
    }

    public BotE getGame() {
        return bote;
    }
}
