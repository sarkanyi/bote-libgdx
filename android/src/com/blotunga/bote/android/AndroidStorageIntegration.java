/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.PlatformApiIntegration.StorageIntegration;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.ui.optionsview.SaveInfo;
import com.google.api.client.util.IOUtils;

public class AndroidStorageIntegration implements StorageIntegration {
    private enum StorageState {
        NONE,
        EXPORT,
        IMPORT
    }

    private AndroidApplication launcher;
    private StorageState state;
    private Array<String> fileNames;
    private String directory;

    public AndroidStorageIntegration(AndroidApplication launcher) {
        this.launcher = launcher;
        state = StorageState.NONE;
        fileNames = new Array<String>();
        directory = launcher.getContext().getExternalFilesDir(Context.DOWNLOAD_SERVICE) + "/BirthOfTheEmpires";
    }

    private void chooseDirectory() {
        launcher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Intent chooserIntent = new Intent(launcher, DirectoryChooserActivity.class);

                final DirectoryChooserConfig config = DirectoryChooserConfig.builder().newDirectoryName("BirthOfTheEmpires")
                        .allowReadOnlyDirectory(false).allowNewDirectoryNameModification(false)
                        .initialDirectory(launcher.getContext().getExternalFilesDir(Context.DOWNLOAD_SERVICE) + "/BirthOfTheEmpires").build();

                chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_CONFIG, config);

                // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
                launcher.startActivityForResult(chooserIntent, AndroidLauncher.REQUEST_DIRECTORY);
            }
        });
    }

    @Override
    public void exportSaves(Array<SaveInfo> saveInfos) {
        state = StorageState.EXPORT;
        fileNames.clear();
        for (SaveInfo si : saveInfos) {
            String imgPath = GameConstants.getSaveLocation() + si.fileName + ".png";
            fileNames.add(si.fileName + ".sav");
            FileHandle fh = new FileHandle(imgPath);
            if (fh.exists())
                fileNames.add(si.fileName + ".png");
        }
        fileNames.add("settings.ini");
        handleState();
    }

    private void handleState() {
        if (havePermission()) {
            if (directory.isEmpty() || !(new File(directory).isDirectory()))
                chooseDirectory();
            else
                if (state == StorageState.IMPORT)
                    importInternal();
                else
                    exportInternal();
        } else {
            launcher.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(launcher, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (launcher instanceof AndroidLauncher)
                            ((AndroidLauncher) launcher).showAlert(launcher.getString(R.string.error), launcher.getString(R.string.permission_error));
                    } else {
                        ActivityCompat.requestPermissions(launcher, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                                AndroidLauncher.MY_PERMISSIONS_REQUEST_WRITE_SD);
                    }
                }
            });
        }
    }

    @Override
    public void importSaves() {
        state = StorageState.IMPORT;
        handleState();
    }

    private void exportInternal() {
        for (String fn : fileNames) {
            try {
                String path = fn.endsWith(".ini") ? Gdx.files.getLocalStoragePath() : GameConstants.getSaveLocation();
                InputStream in = new FileInputStream(path + fn);
                OutputStream out = new FileOutputStream(directory + "/" + fn);
                IOUtils.copy(in, out);
                out.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        state = StorageState.NONE;
    }

    private void importInternal() {
        FileHandle directoryHandle = new FileHandle(directory);
        FileHandle[] files = directoryHandle.list();
        for (FileHandle fh : files) {
            if (!fh.isDirectory() && fh.extension().equals("png") || fh.extension().equals("sav") || fh.extension().equals("ini")) {
                String path = fh.extension().equals("ini") ? Gdx.files.getLocalStoragePath() : GameConstants.getSaveLocation();
                try {
                    InputStream in = new FileInputStream(fh.file());
                    OutputStream out = new FileOutputStream(path + fh.name());
                    IOUtils.copy(in, out);
                    out.close();
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        state = StorageState.NONE;
    }

    private boolean havePermission() {
        if (ContextCompat.checkSelfPermission(launcher, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            return false;
        else
            return true;
    }

    @Override
    public void setDirectory(String directory) {
        this.directory = directory;
        handleState();
    }

    @Override
    public String getDirectory() {
        return directory;
    }
}
