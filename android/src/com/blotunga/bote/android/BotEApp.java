/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import android.app.Application;
import android.content.Context;
import androidx.multidex.*;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

//This is for ACRA
@ReportsCrashes(mailTo = "boteandroid@gmail.com",
           mode = ReportingInteractionMode.TOAST,
           resToastText = R.string.crash_toast_text,
           reportSenderFactoryClasses = {com.blotunga.bote.android.BotEReportSender.class, org.acra.sender.DefaultReportSenderFactory.class})
public class BotEApp extends Application {
    @Override
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
