/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Semaphore;

import android.os.Handler;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.ui.optionsview.SaveInfo;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.http.FileContent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import android.util.Log;

public class GoogleCloudIntegration implements DriveIntegration {
    private static final String TAG = "bote.GoogleCloudIntegration";
    AndroidPlatformApiIntegrator apiIntegrator;
    private Array<String> filenames;
    private String fileName;
    private DriveIntegrationCallback callback;
    final String folderName = "BirthOfTheEmpires";
    private boolean toastShown;
    private Drive mDriveService;
    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    private static final String FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
 
    public GoogleCloudIntegration(AndroidPlatformApiIntegrator apiIntegrator) {
        this.apiIntegrator = apiIntegrator;
        toastShown = false;
        initDriveService();
    }

    private void initDriveService() {
        if (mDriveService != null) {
            return;
        }
        
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(apiIntegrator.launcher.getContext());
        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(
                apiIntegrator.launcher, Arrays.asList(DriveScopes.DRIVE_FILE, DriveScopes.DRIVE_APPDATA));
        String accountName = "";
        if (signInAccount == null) {
            return;
        }

        accountName = signInAccount.getEmail();        
        credential.setSelectedAccountName(accountName);        
        
        if (accountName == null) {
            return;
        }

        mDriveService = new Drive.Builder(
                AndroidHttp.newCompatibleTransport(),
                new GsonFactory(),
                credential)
                .setApplicationName("Birth of The Empires")
                .build();
    }

    protected void showToastOnMainThread(final String text, final int length) {
        Handler mainHandler = new Handler(apiIntegrator.launcher.getMainLooper());

        Runnable toastRunnable = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(apiIntegrator.launcher.getContext(), text, length).show();
            } // This is your code
        };
        mainHandler.post(toastRunnable);
    }

    private String createFolder(String parentPath, String newDirName) throws Exception {
        File body = new File();
        body.setName(newDirName);
        body.setMimeType(FOLDER_MIME_TYPE);
        
        try
        {
            File file = mDriveService.files().create(body).execute();
            return file.getId();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private ArrayList<File> getAllFiles(String parentId) {
        ArrayList<File> files = new ArrayList();
        try
        {
            String parent = parentId.isEmpty() ? "" : " and '" + parentId + "' in parents";
            Files.List request = mDriveService.files().list()
                    .setQ("trashed=false" + parent);

            try {
                do{
                    FileList fl = request.execute();
                    files.addAll(fl.getFiles());                    
                } while (request.getPageToken() != null && request.getPageToken().length() > 0);
            } catch (Exception e) {
                Log.e(TAG, "Can't read list: " + e);                
            }
        } catch (Exception e) {
            Log.e(TAG, "Can't list: " + e);
        }

        return files;
    }
    
    private String getDriveId(String fileName, String parentId) {
        ArrayList<File> files = new ArrayList();
        try
        {
            String parent = parentId.isEmpty() ? "" : " and '" + parentId + "' in parents";
            Files.List request = mDriveService.files().list()
                    .setQ("trashed=false and name='" + fileName + "'" + parent);

            try {
                do{
                    FileList fl = request.execute();
                    files.addAll(fl.getFiles());
                } while (request.getPageToken() != null && request.getPageToken().length() > 0);
            } catch (Exception e) {
                Log.e(TAG, "Can't read list: " + e);                
            }
        } catch (Exception e) {
            Log.e(TAG, "Can't list: " + e);
        }
        File file = null;
        for(File f : files) {
            if (f.getName().equals(fileName)) {
                file = f;
                break;
            }
        }
        return file == null ? "" : file.getId();
    }
    
    private String createFilePath(String parentPath, String newFileName) throws Exception {
        File body = new File();
        body.setName(newFileName);
        
        ArrayList<String> parents = new ArrayList();
        parents.add(parentPath);
        body.setParents(parents);

        try
        {
            File file = mDriveService.files().create(body).execute();
            return file.getId();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private Task<Void> saveFile(String folderId, String path, String name) {
        return Tasks.call(mExecutor, () -> {
                // Create a File containing any metadata changes.                
                File metadata = new File().setName(name);
                FileHandle fh = new FileHandle(path + name);
                java.io.File file = fh.file();
                FileContent content = new FileContent(null, file);
                String fileId = getDriveId(name, folderId);
                if (fileId.isEmpty()) {
                    try {
                        fileId = createFilePath(folderId, name);
                    } catch (Exception e) {
                        Log.e(TAG, "Can't create" + e);
                    }                    
                }

                // Update the metadata and contents.
                mDriveService.files().update(fileId, metadata, content).execute();
                return null;
            });
    }

    @Override
    public void setCallback(DriveIntegrationCallback callback) {
        this.callback = callback;
    }

    @Override
    public void writeSavesToDrive(Array<SaveInfo> saveInfos) {
        if (callback != null)
            callback.signalBusy(true);

        toastShown = false;
        String folderId = getDriveId(folderName, "");
        if (folderId.isEmpty()) {
            try {
                folderId = createFolder("", folderName);
            } catch (Exception e) {
                Log.e(TAG, "Can't create" + e);
            }
        }
        filenames = new Array<String>();
        for (SaveInfo si : saveInfos) {
            String imgPath = GameConstants.getSaveLocation() + si.fileName + ".png";
            filenames.add(si.fileName + ".sav");
            FileHandle fh = new FileHandle(imgPath);
            if (fh.exists())
                filenames.add(si.fileName + ".png");
        }
        filenames.add("settings.ini");
        final String folderIdf = folderId;
        new Thread() {
            @Override
            public void run() {
	            for (String f : filenames) {
	                String path = f.endsWith(".ini") ? Gdx.files.getLocalStoragePath() : GameConstants
	                        .getSaveLocation();
	                saveFile(folderIdf, path, f).addOnFailureListener(exception -> {
	                    Log.e(TAG, "Error = " + exception);
	                    if (!toastShown) {
	                        showToastOnMainThread(apiIntegrator.launcher.getString(R.string.cloud_sync_error) + ": " + exception, Toast.LENGTH_LONG);
	                        toastShown = true;
	                    }
	                });
	            }
	    
	            if (callback != null)
	                callback.signalBusy(false);
            }
        }.start();
    }

    @Override
    public void readSavesFromDrive() {
        String folderId = getDriveId(folderName, "");
        if (folderId.isEmpty())
            return;

        if (callback != null)
            callback.signalBusy(true);

        final ArrayList<File> files = getAllFiles(folderId);
        new Thread() {
            @Override
            public void run() {
                for (File driveFile: files) {
                    String path = driveFile.getName().endsWith(".ini") ? Gdx.files.getLocalStoragePath() : GameConstants
                            .getSaveLocation();
                    FileOutputStream fo;
                    try {
                        fo = new FileOutputStream(path + driveFile.getName());
                        mDriveService.files().get(driveFile.getId()).executeMediaAndDownloadTo(fo);
                        fo.close();
                    } catch (IOException e) {
                        Log.e(TAG, "Error writing: " + e);
                    }                        

                }

                if (callback != null)
                    callback.signalBusy(false);
            }
        }.start();
    }
}
