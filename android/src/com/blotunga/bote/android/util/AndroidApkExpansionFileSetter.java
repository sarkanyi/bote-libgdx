/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidFiles;
import com.blotunga.bote.utils.ApkExpansionSetter;

public class AndroidApkExpansionFileSetter implements ApkExpansionSetter {

    @Override
    public boolean setApkExpansionFile(int mainVersion, int patchVersion) {
        try {
            if(Gdx.files != null)
                return ((AndroidFiles) Gdx.files).setAPKExpansion(mainVersion, patchVersion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
