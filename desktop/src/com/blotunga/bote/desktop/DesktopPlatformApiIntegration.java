/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.desktop;

import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.PlatformApiIntegration;
import com.blotunga.bote.PlatformCallback;
import com.blotunga.bote.achievements.AchievementsList;

public class DesktopPlatformApiIntegration implements PlatformApiIntegration {
    private boolean connected = false;

    @Override
    public void signIn() {
        connected = true;
    }

    @Override
    public void signOut() {
        signOut(false);
    }

    @Override
    public void signOut(boolean explicite) {
        connected = false;
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public void rateGame() {
        System.out.println("Rate game");
    }

    @Override
    public void unlockAchievement(AchievementsList achievement) {
        System.out.println("Unlock achievement " + achievement);
    }

    @Override
    public void incrementAchievement(AchievementsList achievement, int value) {
    }

    @Override
    public void showAchievements() {
    }

    @Override
    public PlatformType getPlatformType() {
       return PlatformType.PlatformStandalone;
    }

    @Override
    public boolean networkAvailable() {
        return true;
    }

    @Override
    public void setCallback(PlatformCallback callback) {
    }

    @Override
    public DriveIntegration getDriveIntegration() {
        return null;
    }

    @Override
    public StorageIntegration getStorageIntegration() {
        return null;
    }
}
