/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;

public class EventRaceKilled extends EventScreen {
    private String killedRace;
    private String killedRaceName;
    private String path;

    public EventRaceKilled(ResourceManager game, String killedRace, String killedRaceName, String killedRaceImage) {
        super(game, "RaceKilled", EventScreenType.EVENT_SCREEN_TYPE_RACE_KILLED);
        this.killedRace = killedRace;
        this.killedRaceName = killedRaceName;
        path = "graphics/races/" + killedRaceImage + ".jpg";
    }

    @Override
    public void show() {
        super.show();
        headLine = StringDB.getString("IMPORTANT_NEWS");
        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 70);
        headLineTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        headLineTable.clear();
        headLineTable.add(headLine, "hugeFont", normalColor);

        rect = GameConstants.coordsToRelative(35, 490, 233, 233);
        Image image = new Image(game.loadTextureImmediate(path));
        image.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(image);

        rect = GameConstants.coordsToRelative(210, 315, 58, 58);
        TextureRegion region = game.getSymbolTextures(killedRace);
        image = new Image(new TextureRegionDrawable(region));
        image.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(image);

        text = StringDB.getString("RACE_ELIMINATED", false, killedRaceName);
        rect = GameConstants.coordsToRelative(300, 495, 840, 240);
        Label desc = new Label(text, skin, "xlFont", normalColor);
        desc.setWrap(true);
        desc.setAlignment(Align.center);
        desc.setTouchable(Touchable.disabled);
        desc.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(desc);
    }

    @Override
    public void dispose() {
        if (game.getAssetManager().isLoaded(path))
            game.getAssetManager().unload(path);
        super.dispose();
    }
}
