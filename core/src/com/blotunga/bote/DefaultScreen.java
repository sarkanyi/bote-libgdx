/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ui.CompositeImage;

public abstract class DefaultScreen implements Screen {
    protected ResourceManager game;
    int subMenu;

    public DefaultScreen(ResourceManager game) {
        this.game = game;
    }

    public void setInputProcessor(InputProcessor inputProcessor) {
        Gdx.input.setInputProcessor(inputProcessor);
    }

    public ResourceManager getResourceManager() {
        return game;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    public Image drawLine(float x1, float x2, float y1, float y2, float lineWidth, Color color) {
        Texture lineTexture = game.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class);
        Image rangeImage = new Image();
        rangeImage.setTouchable(Touchable.disabled);
        Sprite sp = new Sprite(lineTexture);
        sp.setColor(color);
        rangeImage.setOrigin(rangeImage.getImageWidth() / 2, rangeImage.getImageHeight());
        rangeImage.setDrawable(new SpriteDrawable(sp));

        float dist = GameConstants.distBetween(x1, x2, y1, y2);
        rangeImage.setBounds(x1, y1 - lineWidth / 2, lineWidth, dist);
        float angle = GameConstants.angleBetween(x1, x2, y1, y2);
        rangeImage.rotateBy((-1) * angle);
        return rangeImage;
    }

    public Image drawLine(IntPoint begin, IntPoint end, float lineWidth, Color color) {
        float x1 = begin.x * GamePreferences.spriteSize;
        float x2 = end.x * GamePreferences.spriteSize;
        float y1 = begin.y * GamePreferences.spriteSize;
        float y2 = end.y * GamePreferences.spriteSize;

        x1 += GamePreferences.spriteSize / 2;
        x2 += GamePreferences.spriteSize / 2;
        y1 += GamePreferences.spriteSize / 2;
        y2 += GamePreferences.spriteSize / 2;

        return drawLine(x1, x2, y1, y2, lineWidth, color);
    }

    public CompositeImage drawDashedLine(float x1, float x2, float y1, float y2, float lineWidth, Color color, float dashLength,
            float spaceLength) {
        CompositeImage composite = new CompositeImage();
        Texture lineTexture = game.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class);
        float dist = GameConstants.distBetween(x1, x2, y1, y2);
        float angle = GameConstants.angleBetween(x1, x2, y1, y2);
        double rad = Math.atan2(x2 - x1, y2 - y1);

        int numSegments = (int) (dist / (dashLength + spaceLength));
        for (int i = 0; i < numSegments; i++) {
            Image rangeImage = new Image();
            rangeImage.setTouchable(Touchable.disabled);
            Sprite sp = new Sprite(lineTexture);
            sp.setColor(color);
            rangeImage.setOrigin(rangeImage.getImageWidth() / 2, rangeImage.getImageHeight());
            rangeImage.setDrawable(new SpriteDrawable(sp));
            rangeImage.setBounds((float) (x1 + i * Math.sin(rad) * (dashLength + spaceLength)), (float) (y1 - lineWidth / 2 + i
                    * Math.cos(rad) * (dashLength + spaceLength)), lineWidth, dashLength);
            rangeImage.rotateBy((-1) * angle);
            composite.add(rangeImage);
        }
        return composite;
    }

    public CompositeImage drawDashedLine(IntPoint begin, IntPoint end, float lineWidth, Color color, float dashLength,
            float spaceLength) {
        float x1 = begin.x * GamePreferences.spriteSize;
        float x2 = end.x * GamePreferences.spriteSize;
        float y1 = begin.y * GamePreferences.spriteSize;
        float y2 = end.y * GamePreferences.spriteSize;

        x1 += GamePreferences.spriteSize / 2;
        x2 += GamePreferences.spriteSize / 2;
        y1 += GamePreferences.spriteSize / 2;
        y2 += GamePreferences.spriteSize / 2;

        return drawDashedLine(x1, x2, y1, y2, lineWidth, color, dashLength, spaceLength);
    }

    public CompositeImage drawBezierCurve(IntPoint begin, IntPoint end, float angle, float lineWidth, Color color) {
        CompositeImage composite = new CompositeImage();
        Vector2 start = new Vector2(begin.x * GamePreferences.spriteSize + GamePreferences.spriteSize / 2,
                begin.y * GamePreferences.spriteSize + GamePreferences.spriteSize / 2);
        Vector2 stop = new Vector2(end.x * GamePreferences.spriteSize + GamePreferences.spriteSize / 2,
                end.y * GamePreferences.spriteSize + GamePreferences.spriteSize / 2);
        Texture lineTexture = game.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class);
        float dist = GameConstants.distBetween(start.x, stop.x, start.y, stop.y);

        Vector2 points[] = new Vector2[3];
        points[0] = start.cpy();
        points[points.length - 1] = stop.cpy();
        float directAngle = GameConstants.angleBetween(start.x, stop.x, start.y, stop.y);
        for (int i = 1; i < points.length - 1; i++) {
            Vector2 mid = new Vector2( (stop.x - start.x) * i / (points.length - 1), (stop.y - start.y)  * i / (points.length - 1) );
            points[i] = new Vector2(start);
            points[i].add(mid);
            float len = GameConstants.distBetween(points[i - 1].x, points[i].x, points[i - 1].y, points[i].y);
            float xy = len / (MathUtils.cosDeg(angle));
            float newAngle = 180 - angle - directAngle;
            points[i].x = xy * MathUtils.sinDeg(newAngle);
            points[i].y = (-1) * xy * MathUtils.cosDeg(newAngle);
            points[i].x += start.x;
            points[i].y += start.y;
        }

        float div = Gdx.app.getType() == ApplicationType.Desktop ? 8.0f : 12.0f;
        int loopend = Math.round(dist / div);
        for (int i = 0; i < loopend; i++) {
            Vector2 next = GameConstants.calcBezierPoint(points, ((float) i) / ((float) loopend - 1));
            directAngle = GameConstants.angleBetween(start.x, next.x, start.y, next.y);
            dist = GameConstants.distBetween(start.x, next.x, start.y, next.y);
            Image rangeImage = new Image();
            rangeImage.setTouchable(Touchable.disabled);
            Sprite sp = new Sprite(lineTexture);
            sp.setColor(color);
            rangeImage.setOrigin(rangeImage.getImageWidth() / 2, rangeImage.getImageHeight());
            rangeImage.setDrawable(new SpriteDrawable(sp));
            rangeImage.setBounds(start.x, start.y - lineWidth / 2, lineWidth, dist);
            rangeImage.rotateBy((-1) * directAngle);
            composite.add(rangeImage);
            start = next;
        }

        return composite;
    }

    public SpriteDrawable getTintedDrawable(String path, Color color) {
        Texture texture = game.getAssetManager().get(path);
        texture.setFilter(game.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        TextureRegion dr = new TextureRegion(texture);
        Sprite sp = new Sprite(dr);
        sp.setColor(color);
        SpriteDrawable sd = new SpriteDrawable(sp);
        return sd;
    }

    public void setSubMenu(int subMenu) {
        this.subMenu = subMenu;
    }

    public int getSubMenu() {
        return subMenu;
    }
}
