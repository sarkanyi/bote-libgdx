/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;

/**
 * @author dragon
 * @version 1
 */
public class BeamWeapons {
    private String beamName;	// name of the beam
    private int beamPower;		// power of the beam
    private int beamType;		// type of the beam
    private int beamNumber;		// number of beam systems
    private int shootNumber;	// how many rays are shot per shot from the beam system
    private int bonus;			// bonus of the beam through the firing system (canon, array etc)
    private int beamLength;		// length of beam
    private int rechargeTime;	// time to recharge after firing
    private boolean piercing;	// pierces shields, except regenerating ones
    private boolean modulating; // modulating beam -> always 50% of the damage hits hull
    private FireArc fireArc;	// firing angle and mount point of the weapon

    public BeamWeapons() {
        beamName = "";
        beamPower = 0;
        beamType = -1;
        beamNumber = 0;
        shootNumber = 0;
        bonus = 0;
        beamLength = 0;
        rechargeTime = 0;
        piercing = false;
        modulating = false;
        fireArc = new FireArc();
    }

    public BeamWeapons(BeamWeapons oldBeam) {
        beamName = oldBeam.beamName;
        beamPower = oldBeam.beamPower;
        beamType = oldBeam.beamType;
        beamNumber = oldBeam.beamNumber;
        shootNumber = oldBeam.shootNumber;
        bonus = oldBeam.bonus;
        beamLength = oldBeam.beamLength;
        rechargeTime = oldBeam.rechargeTime;
        piercing = oldBeam.piercing;
        modulating = oldBeam.modulating;
        fireArc = new FireArc(oldBeam.getFireArc().getPosition(), oldBeam.getFireArc().getAngle());
    }

    public String getBeamName() {
        return beamName;
    }

    public int getBeamPower() {
        return beamPower;
    }

    public int getBeamType() {
        return beamType;
    }

    public int getBeamNumber() {
        return beamNumber;
    }

    public int getShootNumber() {
        return shootNumber;
    }

    public int getBonus() {
        return bonus;
    }

    public int getBeamLength() {
        return beamLength;
    }

    public int getRechargeTime() {
        return rechargeTime;
    }

    public boolean isPiercing() {
        return piercing;
    }

    public boolean isModulating() {
        return modulating;
    }

    public FireArc getFireArc() {
        return fireArc;
    }

    public void setBeamPower(int power) {
        beamPower = power;
    }

    public void setRechargeTime(int time) {
        rechargeTime = time;
    }

    public void modifyBeamName(String beamName) {
        this.beamName = beamName;
    }

    public void modifyBeamWeapon(int beamType, int beamPower, int beamNumber, String beamName, boolean modulating,
            boolean pierdcing, int bonus, int beamLength, int rechargeTime, int shootNumber) {
        this.beamName = beamName;
        this.beamPower = beamPower;
        this.beamType = beamType;
        this.beamNumber = beamNumber;
        this.shootNumber = shootNumber;
        this.bonus = bonus;
        this.beamLength = beamLength;
        this.rechargeTime = rechargeTime;
        this.piercing = pierdcing;
        this.modulating = modulating;
    }

    public int getBeamDamage() {
        int beamDmg = 0;
        int tempBeamDmg = getBeamPower() * getBeamNumber() * getShootNumber();
        beamDmg += tempBeamDmg * (getBeamLength() * (100 / (getBeamLength() + getRechargeTime())));
        beamDmg += tempBeamDmg * ((getBeamLength() > (100 % (getBeamLength() + getRechargeTime()))) ? (100 % (getBeamLength() + getRechargeTime())) : getBeamLength());
        return beamDmg;
    }

    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        table.clearChildren();
        String text = String.format("%s %d %s", StringDB.getString("TYPE"), beamType, beamName);
        Label l = new Label(text, skin, headerFont, headerColor);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(300));
        table.row();

        text = StringDB.getString("BEAM_DAMAGE") + ": " + getBeamDamage();
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("ACCURACY") + ": " + (60 + beamType + bonus) + "%";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("BEAM_ORIENTATION") + ": " + getFireArc().getPosition() + "\u00b0";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("BEAM_ANGLE") + ": " + getFireArc().getAngle() + "\u00b0";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        if(piercing) {
            text = StringDB.getString("PIERCING");
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }

        if(modulating) {
            text = StringDB.getString("MODULATING");
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }
    }
}
