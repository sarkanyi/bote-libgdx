/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectIntMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.*;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.GameResources;
import com.blotunga.bote.general.InGameEntity;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchComplex;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Ship extends InGameEntity {
    protected Hull hull;								// hull strength of the ship
    protected Shield shield;							// shield strength of the ship
    protected Array<TorpedoWeapons> torpedoWeapons;		// torpedo weapons of the ship
    protected Array<BeamWeapons> beamWeapons;			// beam weapons of the ship;
    protected int ID;					// ID of the ship
    protected int maintenanceCosts;		// maintenance costs of the ship
    protected ShipType shipType;		// type of the ship
    protected ShipSize shipSize;		// size of the ship
    protected int maneuverabilty;		// maneuverability of the ship
    protected int speed;				// the speed of the ship
    protected ShipRange range;			// range of the ship
    protected int scanPower;			// scan power of the ship
    protected int scanRange;			// scan range of the ship
    protected int storageRoom;			// storage size of the ship
    protected int colonizePoints;		// colony/terraform points -> time to colonize/terraform
    protected int stationBuildPoints;	// outpost build points -> time to build
    protected ShipSpecial[] special;	// the two possible special abilities of the ship
    protected String shipClass;			// name of the ship class

    private int stealthGrade;			// cloak power of the ship - 0 = no cloaking
    private ShipOrder currentOrder;		// the current orders of the ship
    private IntPoint targetCoord;		// the target of the ship
    private Array<IntPoint> path;		// the path of the ship towards the goal
    private boolean cloakOn;			// is the cloak turned on?
    private int terraformingPlanet;		// number of the planet which is currently terraformed
    //correct values: (-1: none, 0 <= m_nTerraformingPlanet <= (planet count -1) otherwise
    private boolean flagShip;			// is this ship a flagship? (only one per Empire)
    private int crewExperience;			// experience of the crew of the ship
    private Array<Troop> troops;		// troops which are transported by the ship
    private int[] loadedResources;		// the resources which are loaded on the ship

    protected CombatTactics combatTactics;	// tactics of the ship in combat

    protected class FleetInfoForGetTooltip {
        public int fleetShipType;
        public ShipRange fleetRange;
        public int fleetSpeed;

        public FleetInfoForGetTooltip(int fleetShipType, ShipRange fleetRange, int fleetSpeed) {
            this.fleetShipType = fleetShipType;
            this.fleetRange = fleetRange;
            this.fleetSpeed = fleetSpeed;
        }

        public FleetInfoForGetTooltip() {
            this(-1, ShipRange.LONG, 0);
        }
    }

    public Ship() {
        this((ResourceManager) null);
    }

    public Ship(ResourceManager res) {
        super(res);
        type = EntityType.SHIP;
        hull = new Hull();
        shield = new Shield();
        torpedoWeapons = new Array<TorpedoWeapons>(true, 3, TorpedoWeapons.class);
        beamWeapons = new Array<BeamWeapons>(true, 3);
        ID = 0;
        maintenanceCosts = 0;
        shipType = ShipType.PROBE;
        shipSize = ShipSize.SMALL;
        maneuverabilty = 0;
        speed = 0;
        range = ShipRange.LONG;
        scanPower = 0;
        scanRange = 0;
        storageRoom = 0;
        colonizePoints = 0;
        stationBuildPoints = 0;
        special = new ShipSpecial[2];
        special[0] = special[1] = ShipSpecial.NONE;
        shipClass = "";
        stealthGrade = 0;
        currentOrder = ShipOrder.NONE;
        targetCoord = new IntPoint(-1, -1);
        path = new Array<IntPoint>(false, 3);
        cloakOn = false;
        terraformingPlanet = -1;
        flagShip = false;
        crewExperience = 0;
        troops = new Array<Troop>(true, 3);
        loadedResources = new int[ResourceTypes.DERITIUM.getType() + 1];
        combatTactics = CombatTactics.CT_ATTACK;
    }

    public Ship(Ship ship) {
        super(ship);
        type = EntityType.SHIP;
        hull = new Hull(ship.hull);
        shield = new Shield(ship.shield);
        torpedoWeapons = new Array<TorpedoWeapons>(true, 3, TorpedoWeapons.class);
        for (int i = 0; i < ship.torpedoWeapons.size; i++)
            torpedoWeapons.add(new TorpedoWeapons(ship.torpedoWeapons.get(i)));
        beamWeapons = new Array<BeamWeapons>(true, 3);
        for (int i = 0; i < ship.beamWeapons.size; i++)
            beamWeapons.add(new BeamWeapons(ship.getBeamWeapons().get(i)));
        ID = ship.ID;
        maintenanceCosts = ship.maintenanceCosts;
        shipType = ship.shipType;
        shipSize = ship.shipSize;
        maneuverabilty = ship.maneuverabilty;
        speed = ship.speed;
        range = ship.range;
        scanPower = ship.scanPower;
        scanRange = ship.scanRange;
        storageRoom = ship.storageRoom;
        colonizePoints = ship.colonizePoints;
        stationBuildPoints = ship.stationBuildPoints;
        special = ship.special.clone();
        shipClass = ship.shipClass;
        stealthGrade = ship.stealthGrade;
        currentOrder = ship.currentOrder;
        targetCoord = new IntPoint(ship.targetCoord);
        path = new Array<IntPoint>(false, 3);
        for (int i = 0; i < ship.path.size; i++)
            path.add(new IntPoint(ship.path.get(i)));
        cloakOn = ship.cloakOn;
        terraformingPlanet = ship.terraformingPlanet;
        flagShip = ship.flagShip;
        crewExperience = ship.crewExperience;
        troops = new Array<Troop>(true, 3);
        for (int i = 0; i < ship.troops.size; i++)
            troops.add(new Troop(ship.getTroops().get(i)));
        loadedResources = ship.loadedResources.clone();
        combatTactics = ship.combatTactics;
    }

    public Hull getHull() {
        return hull;
    }

    public Shield getShield() {
        return shield;
    }

    public Array<TorpedoWeapons> getTorpedoWeapons() {
        return torpedoWeapons;
    }

    public Array<BeamWeapons> getBeamWeapons() {
        return beamWeapons;
    }

    public Array<Troop> getTroops() {
        return troops;
    }

    public int getID() {
        return ID;
    }

    public IntPoint getTargetCoord() {
        return targetCoord;
    }

    public Array<IntPoint> getPath() {
        return path;
    }

    public int getMaintenanceCosts() {
        return maintenanceCosts;
    }

    public int getStealthGrade() {
        return stealthGrade;
    }

    public boolean isCloakOn() {
        return cloakOn;
    }

    public ShipType getShipType() {
        return shipType;
    }

    public ShipSize getSize() {
        return shipSize;
    }

    public int getManeuverabilty() {
        return maneuverabilty;
    }

    public ShipRange getRange() {
        return range;
    }

    public int getSpeed() {
        return speed;
    }

    public int getScanPower() {
        return scanPower;
    }

    public int getScanRange() {
        return scanRange;
    }

    public int getColonizePoints() {
        return colonizePoints;
    }

    public int getStationBuildPoints() {
        return stationBuildPoints;
    }

    public ShipOrder getCurrentOrder() {
        return currentOrder;
    }

    public int getTerraformingPlanet() {
        return terraformingPlanet;
    }

    public String getShipClass() {
        return shipClass;
    }

    public String getShipImageName() {
        return resourceManager.getShipImgName(ID - 10000);
    }

    public String getShipTypeAsString() {
        return getShipTypeAsString(false);
    }

    public String getShipTypeAsString(boolean plural) {
        if (plural)
            return StringDB.getString(shipType.getPluralName());
        else
            return StringDB.getString(shipType.getName());
    }

    public String getCurrentOrderAsString() {
        return StringDB.getString(currentOrder.getName());
    }

    public String getCurrentTacticsAsString() {
        return StringDB.getString(combatTactics.getName());
    }

    public String getCurrentTargetAsString(Race forRace) {
        String target;
        if (forRace.getRaceId().equals(ownerID)
                || forRace.getAgreement(ownerID).getType() >= DiplomaticAgreement.ALLIANCE.getType()) {
            if (hasTarget()) {
                StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(targetCoord.x, targetCoord.y);
                target = system.getName();
            } else
                target = StringDB.getString(ShipOrder.NONE.getName());
        } else
            target = StringDB.getString("UNKNOWN");
        return target;
    }

    public boolean isFlagShip() {
        return flagShip;
    }

    public int getCrewExperience() {
        return crewExperience;
    }

    public int getStorageRoom() {
        return storageRoom;
    }

    public int getLoadedResources(int res) {
        return loadedResources[res];
    }

    public CombatTactics getCombatTactics() {
        return combatTactics;
    }

    public void setID(int iD) {
        ID = iD + 10000;
    }

    public void setHull(Hull hull) {
        this.hull = hull;
    }

    @Override
    public void setCoordinates(IntPoint p) {
        coordinates = p;
    }

    public void setTargetCoord(IntPoint targetCoord) {
        setTargetCoord(targetCoord, false);
    }

    private void updateHistory() {
        if (getOwner() != null && getOwner().isMajor())
            ((Major) getOwner()).getShipHistory().modifyShip(shipHistoryInfo(),
                    resourceManager.getUniverseMap().getStarSystemAt(coordinates).coordsName(true));
    }

    //when setting a ship target, some running orders need to be unset if they're active
    //this function calculates whether that's needed
    private static boolean shouldUnsetOrder(ShipOrder order) {
        switch (order) {
            case ATTACK_SYSTEM:
            case BLOCKADE_SYSTEM:
            case DESTROY_SHIP:
            case COLONIZE:
            case TERRAFORM:
            case BUILD_OUTPOST:
            case BUILD_STARBASE:
            case UPGRADE_OUTPOST:
            case UPGRADE_STARBASE:
            case WAIT_SHIP_ORDER:
            case SENTRY_SHIP_ORDER:
            case REPAIR:
            case IMPROVE_SHIELDS:
            case EXTRACT_DEUTERIUM:
            case EXPLORE_WORMHOLE:
            case AUTO_EXPLORE:
                return true;
            default:
                return false;
        }
    }

    public void setTargetCoord(IntPoint targetCoord, boolean simpleSetter) {
        this.targetCoord = targetCoord;
        if (!hasTarget())
            path.clear();

        updateHistory();
        if (simpleSetter)
            return;
        if (!targetCoord.equals(new IntPoint(-1, -1)) && shouldUnsetOrder(currentOrder)) {
            setTerraform(-1);
            unsetCurrentOrder();
        }
    }

    public void setMaintenanceCosts(int maintenanceCosts) {
        this.maintenanceCosts = maintenanceCosts;
    }

    public void setPath(Array<IntPoint> path) {
        this.path = path;
    }

    public void setType(ShipType type) {
        this.shipType = type;
    }

    public void setSize(ShipSize size) {
        this.shipSize = size;
    }

    public void setManeuverabilty(int maneuverabilty) {
        this.maneuverabilty = maneuverabilty;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setRange(ShipRange range) {
        this.range = range;
    }

    public void setScanPower(int scanPower) {
        this.scanPower = scanPower;
    }

    public void setScanRange(int scanRange) {
        this.scanRange = scanRange;
    }

    public void setStealthGrade(int stealthGrade) {
        this.stealthGrade = stealthGrade;
    }

    public void setCloak(boolean cloakOn) {
        if (!canCloak()) {
            return;
        }
        this.cloakOn = cloakOn;
    }

    public void setStorageRoom(int storageRoom) {
        this.storageRoom = storageRoom;
    }

    public void setLoadedResource(int add, int res) {
        loadedResources[res] += add;
    }

    public void setColonizePoints(int colonizePoints) {
        this.colonizePoints = colonizePoints;
    }

    public void setStationBuildPoints(int stationBuildPoints) {
        this.stationBuildPoints = stationBuildPoints;
    }

    public void setCurrentOrder(ShipOrder order) {
        if (currentOrder == ShipOrder.TERRAFORM)
            setTerraform(-1);
        currentOrder = order;
        updateHistory();
    }

    public void setSpecial(int n, ShipSpecial ability) {
        special[n] = ability;
    }

    public void setTerraform() {
        setTerraform(-1);
    }

    public void setTerraform(int planetNumber) {
        if (planetNumber == -1) {
            if (currentOrder == ShipOrder.TERRAFORM)
                currentOrder = ShipOrder.NONE;
        } else
            currentOrder = ShipOrder.TERRAFORM;
        terraformingPlanet = planetNumber;
        resourceManager.getUniverseMap().getStarSystemAt(coordinates.x, coordinates.y).recalcPlanetsTerraformingStatus();
        updateHistory();
    }

    public void setShipClass(String shipClass) {
        this.shipClass = shipClass;
    }

    public void setFlagShip(boolean is) {
        flagShip = is;
    }

    public void setCombatTactics(CombatTactics tactics, boolean alsoIfRetreat) {
        if (!alsoIfRetreat && combatTactics == CombatTactics.CT_RETREAT)
            return;
        combatTactics = tactics;
    }

    public void setCurrentOrderAccordingToType() {
        unsetCurrentOrder();
        setCombatTacticsAccordingToType();
    }

    /**
     * sets tactics to AVOID if it's a civilian ship, else to ATTACK
     */
    public void setCombatTacticsAccordingToType() {
        combatTactics = isNonCombat() ? CombatTactics.CT_AVOID : CombatTactics.CT_ATTACK;
    }

    public void unsetCurrentOrder(boolean updateHistory) {
        terraformingPlanet = -1;
        boolean wasTerraform = currentOrder == ShipOrder.TERRAFORM;
        currentOrder = ShipOrder.NONE;
        if (wasTerraform)
            resourceManager.getUniverseMap().getStarSystemAt(coordinates.x, coordinates.y).recalcPlanetsTerraformingStatus();
        if (updateHistory)
            updateHistory();
    }

    public void unsetCurrentOrder() {
        unsetCurrentOrder(true);
    }

    public boolean removeDestroyed(Race owner, int round, String event, String status, Array<String> destroyedShips) {
        return removeDestroyed(owner, round, event, status, destroyedShips, "");
    }

    public boolean removeDestroyed(Race owner, int round, String event, String status, Array<String> destroyedShips,
            String anomaly) {
        if (isAlive())
            return true;
        owner.addLostShipHistory(shipHistoryInfo(), event, status, round, coordinates);
        if (destroyedShips != null)
            destroyedShips.add(name + " ( " + getShipTypeAsString() + " , " + shipClass + ")");
        if (flagShip)
            owner.lostFlagShip(this);
        if (isStation())
            owner.lostStation(shipType);
        if (!anomaly.isEmpty())
            owner.lostShipToAnomaly((Ships) this, anomaly);
        return false;
    }

    /**
     * This function calculates the experience of the ship after a new turn.
     * It doesn't includes experience received in combat as those are added after the battle
     */
    public void calcExp() {
        /*
         Starts with 0 (except ships created in systems with an Academy built, which start with 250?).
         0 - inexperienced, experience grows with 10/turn until 250
         250 - rookie, experience grows with 5/turn until 500
         500 - normal, experience doesn't grows except in combat
         1000 - elite
         2500 - Professional etc.

         For gaining experience there are several possibilities:
         1. random events, which give out exp (ie: survived ion storm, diplomatic escort)
         2. exploration missions (scanned pulsar, neutron star, black hole, wormhole, nebula)
         3. academy: if a normal or higher ship is in the fleet, the exp gain of inexperienced or rookie crews will be doubled for each turn
         4. invasion: ships get (population loss in billions) * 100 + active ship defense exp
         5. system blockades, raids: doubles the exp gain for inexperienced and rookie crews
         6. system attack: (population loss in billions) * 100 + active ship defense exp
         7. ship battle: ((medium exp of all enemy ships)/((medium exp of all enemy ships) + (medium exp of own ships)))*(total hull damage of enemy) / 100. This results in a higher exp for damage inflicted to experienced ships
         */
        int expAdd = 0;
        switch (getExpLevel()) {
            case 0:
                expAdd = 15;
                break;
            case 1:
                expAdd = 10;
                break;
            case 2:
                expAdd = 5;
                break;
        }
        setCrewExperience(expAdd);
    }

    public void adoptOrdersFrom(Ship ship) {
        ShipOrder order = ship.getCurrentOrder();
        setTerraform(ship.terraformingPlanet);
        currentOrder = order;
        combatTactics = ship.getCombatTactics();
        targetCoord = ship.getTargetCoord();
    }

    public void setCrewExperience(int add) {
        if (shipType != ShipType.PROBE && !isAlien())
            crewExperience = Math.min(64000, crewExperience + add);
    }

    public void applyTraining(int xp, boolean veteran) {
        if (!veteran || getExpLevel() >= 4)
            setCrewExperience(xp);
        else
            setCrewExperience(xp * 2);
    }

    public boolean applyIonStormEffects() {
        // all crew experience is lost
        crewExperience = 0;
        // maximum shield grows by 3% up to the double of the shield type
        int maxMaxShield = getMaxMaxShield();
        int oldMaxShield = shield.getMaxShield();
        if (oldMaxShield >= maxMaxShield)
            return true;
        int newMaxShield = (int) (oldMaxShield * 1.03);
        boolean improvementFinished = false;
        if (newMaxShield >= maxMaxShield) {
            newMaxShield = maxMaxShield;
            improvementFinished = true;
        }
        shield.modifyShield(newMaxShield, shield.getShieldType(), shield.isRegenerative());
        return improvementFinished;
    }

    public boolean unassignFlagship() {
        boolean is = isFlagShip();
        setFlagShip(false);
        return is;
    }

    public int getCompleteOffensivePower() {
        return getCompleteOffensivePower(true, true);
    }

    /**
     * Function returns the entire offensive power of the ship (for comparison)
     * @param beams
     * @param torpedos
     * @return
     */
    public int getCompleteOffensivePower(boolean beams, boolean torpedos) {
        int beamDmg = 0;
        if (beams) {
            for (BeamWeapons bw : beamWeapons) {
                double typeBonus = bw.getBeamType() / 10.0;
                typeBonus *= (100 + bw.getBonus()) / 100.0;
                int tempBeamDmg = bw.getBeamPower() * bw.getBeamNumber() * bw.getShootNumber();
                // if no damage is inflicted to the hull because the damage is too low, reduce the
                // beam damage, except if piercing
                boolean halfdmg = bw.getBeamPower() * GameConstants.DAMAGE_TO_HULL < 1;

                if (bw.isPiercing())
                    tempBeamDmg = (int) (tempBeamDmg * 1.5);
                if (bw.isModulating())
                    tempBeamDmg *= 3;
                else if (halfdmg)
                    tempBeamDmg /= 2;

                beamDmg += tempBeamDmg * (bw.getBeamLength() * (100 / (bw.getBeamLength() + bw.getRechargeTime())));
                beamDmg += tempBeamDmg * ((bw.getBeamLength() > (100 % (bw.getBeamLength() + bw.getRechargeTime()))) ? (100 % (bw
                            .getBeamLength() + bw.getRechargeTime())) : bw.getBeamLength());
                beamDmg = (int) (beamDmg * typeBonus);
            }
            beamDmg /= 2;
        }
        int torpedoDmg = 0;
        if (torpedos) {
            for (TorpedoWeapons tw : torpedoWeapons) {
                int tempTorpedoDmg = tw.getTorpedoPower() * tw.getNumber() * 100 * tw.getNumberOfTubes() / tw.getTubeFirerate();
                boolean halfdmg = tw.getTorpedoPower() * GameConstants.DAMAGE_TO_HULL < 1;

                int type = tw.getTorpedoType();
                // not the real damage, but special abilities do a higher damage
                if (TorpedoInfo.isPenetrating(type))
                    tempTorpedoDmg = (int) (tempTorpedoDmg * 1.5);
                if (TorpedoInfo.isIgnoreAllShields(type))
                    tempTorpedoDmg *= 3;
                if (TorpedoInfo.isCollapseShields(type))
                    tempTorpedoDmg = (int) (tempTorpedoDmg * 1.25);
                if (TorpedoInfo.isDoubleShieldDmg(type))
                    tempTorpedoDmg = (int) (tempTorpedoDmg * 1.5);
                if (TorpedoInfo.isDoubleHullDmg(type))
                    tempTorpedoDmg = tempTorpedoDmg * 2;
                if (TorpedoInfo.isReduceManeuver(type))
                    tempTorpedoDmg = (int) (tempTorpedoDmg * 1.1);

                if (halfdmg && !TorpedoInfo.isIgnoreAllShields(type))
                    tempTorpedoDmg /= 2;

                torpedoDmg += tempTorpedoDmg;
            }
        }

        // stations get a bonus because they have no firing angles to consider
        if (isStation()) {
            beamDmg = (int) (beamDmg * 1.5);
            torpedoDmg = (int) (torpedoDmg * 1.5);
        }

        // consider maneuverability
        double man = 1.0;
        if (!isStation())
            man = (maneuverabilty - 4.0) / 10.0 * 1.75 + 1.0;

        double cloak = 1.0;
        if (cloakOn)
            cloak = 1.25;

        return (int) ((beamDmg + torpedoDmg) * man * cloak);
    }

    public int getCompleteDefensivePower() {
        return getCompleteDefensivePower(true, true);
    }

    /**
     * Function returns the entire defensive strength of the ship.
     * The maximum hull and shield values are used, and it doesn't represent a combat info.
     * @param useShields
     * @param useHull
     * @return
     */
    public int getCompleteDefensivePower(boolean useShields, boolean useHull) {
        int def = 0;
        if (useHull) {
            def += hull.getMaxHull();
            if (hull.isAblative())
                def = (int) (def * 1.3);
            if (hull.isPolarisation())
                def = (int) (def * 1.3);
            // hull is twice as important as hull
            def *= 2;
        }

        if (useShields)
            def += shield.getMaxShield() + (shield.getMaxShield() / 300 + 2 * shield.getShieldType()) * 100;

        double man = 1.0;
        if (!isStation())
            man = (maneuverabilty - 4.0) / 10.0 * 1.75 + 1.0;

        double cloak = 1.0;
        if (cloakOn)
            cloak = 1.25;

        return (int) (def * man * cloak);
    }

    /**
     * Function returns the experience level of the ship
     * @return
     */
    public int getExpLevel() {
        int exp = crewExperience;
        // Inexperienced
        if (exp < 250)
            return 0;
        // Rookie
        else if (exp < 500)
            return 1;
        // Normal
        else if (exp < 1000)
            return 2;
        // Professional
        else if (exp < 2500)
            return 3;
        // Veteran
        else if (exp < 5000)
            return 4;
        // Elite
        else if (exp < 10000)
            return 5;
        // Legendary
        else
            return 6;
    }

    public int getUsedStorageRoom(Array<TroopInfo> troopInfo) {
        int usedStorage = 0;
        for (Troop tr : getTroops()) {
            int id = tr.getID();
            usedStorage += troopInfo.get(id).getSize();
        }
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.DERITIUM.getType(); i++) {
            // deritium is stored in a 1:250 ratio
            if (i == ResourceTypes.DERITIUM.getType())
                usedStorage += getLoadedResources(i) * 250;
            else
                usedStorage += getLoadedResources(i);
        }
        return usedStorage;
    }

    public int getFreeStorageRoom() {
        int result = storageRoom - getUsedStorageRoom(resourceManager.getTroopInfos());
        return result;
    }

    public int getStealthPower() {
        int level = stealthGrade;
        if (!isCloakOn())
            level = Math.min(level, 3);
        return level * 30;
    }

    public int getMaxMaxShield() {
        return resourceManager.getShipInfos().get(ID - 10000).getShield().getMaxShield() * 2;
    }

    /**
     * Judge whether this ship is "worse" than the passed one.
     * Currently only used for special techs which can improve stations,
     * extend with missing things if needed.
     * @param other
     * @return
     */
    public boolean isWorseThan(Ship other) {
        if (shield.getMaxShield() < other.shield.getMaxShield() || hull.getMaxHull() < other.hull.getMaxHull()
                || scanPower < other.scanPower)
            return true;
        if (beamWeapons.size < other.beamWeapons.size)
            return true;
        if (beamWeapons.size != 0) {
            BeamWeapons ours = beamWeapons.get(0);
            BeamWeapons theirs = other.beamWeapons.get(0);
            if (ours.getBeamPower() < theirs.getBeamPower() || ours.getRechargeTime() > theirs.getRechargeTime())
                return true;
        }
        if (torpedoWeapons.size < other.torpedoWeapons.size)
            return true;
        if (torpedoWeapons.size != 0) {
            TorpedoWeapons ours = torpedoWeapons.get(0);
            TorpedoWeapons theirs = other.torpedoWeapons.get(0);
            if (ours.getAccuracy() < theirs.getAccuracy() || ours.getTubeFirerate() > theirs.getTubeFirerate())
                return true;
        }
        return false;
    }

    public boolean ionStormCanImproveShields() {
        return getMaxMaxShield() > shield.getMaxShield();
    }

    public boolean isNonCombat() {
        return shipType.getType() <= ShipType.PROBE.getType();
    }

    public boolean isStation() {
        return shipType == ShipType.OUTPOST || shipType == ShipType.STARBASE;
    }

    public boolean isAlien() {
        return shipType == ShipType.ALIEN;
    }

    public boolean hasTroops() {
        return getTroops().size != 0;
    }

    public boolean hasTarget() {
        return targetCoord.x != -1 && targetCoord.y != -1;
    }

    /**
     * Is this ship in need for a player command input in this round ?
     * Does not cover "self-renewing" orders without a turn limit
     * such as ATTACK_SYSTEM; player is expected to look after such fleets manually	 *
     * @return true if yes
     */
    public boolean hasNothingToDo() {
        return currentOrder == ShipOrder.NONE && !hasTarget() && !isStation();
    }

    /**
     * Are the hull of this ship and all the hulls of the ships in its fleet at their maximums ?
     * @return true if no
     */
    public boolean needsRepair() {
        return hull.getCurrentHull() < hull.getMaxHull() || shield.getCurrentShield() < shield.getMaxShield();
    }

    public boolean canExtractDeuterium() {
        return shipType == ShipType.TRANSPORTER && getFreeStorageRoom() > 0;
    }

    /**
     * Tests whether a ship has the type of special ability specified by parameter
     * @param ability
     * @return
     */
    public boolean hasSpecial(ShipSpecial ability) {
        if (special[0] == ability || special[1] == ability)
            return true;
        else
            return false;
    }

    /**
     * Needed for the editor
     * @param idx
     * @return
     */
    public ShipSpecial getSpecial(int idx) {
        if (idx < special.length)
            return special[idx];
        return ShipSpecial.NONE;
    }

    public boolean isVeteran() {
        return getExpLevel() >= 4;
    }

    private boolean checkNew(ShipOrder order, ShipOrder co, CombatTactics ct) {
        if (order == ShipOrder.AVOID)
            return ct != CombatTactics.CT_AVOID;
        else if (order == ShipOrder.ATTACK)
            return ct != CombatTactics.CT_ATTACK;
        return co != order;
    }

    /**
     * This function returns true if the ship can execute the order
     * @param order
     * @param requireNew
     * @return if the ship can't execute the order,false
     */
    public boolean canHaveOrder(ShipOrder order, boolean requireNew) {
        if (requireNew && !checkNew(order, currentOrder, combatTactics))
            return false;
        switch (order) {
            case NONE:
            case AVOID:
            case ATTACK:
            case DESTROY_SHIP:
            case CANCEL:
                return true;
            case IMPROVE_SHIELDS:
                return ionStormCanImproveShields();
            case EXTRACT_DEUTERIUM:
                return canExtractDeuterium();
            case WAIT_SHIP_ORDER:
            case SENTRY_SHIP_ORDER:
            case CREATE_FLEET:
            case EXPLORE_WORMHOLE:
                return !isStation();
            case REPAIR:
                return needsRepair();
            case ASSIGN_FLAGSHIP:
                return !flagShip && !isStation();
            case ENCLOAK:
                return canCloak() && !cloakOn;
            case DECLOAK:
                return cloakOn;
            case COLONIZE:
            case TERRAFORM:
                return getColonizePoints() >= 1;
            case BUILD_OUTPOST:
                return stationBuildPoints >= 1 && !isDoingStationWork(ShipOrder.BUILD_OUTPOST);
            case BUILD_STARBASE:
                return stationBuildPoints >= 1 && !isDoingStationWork(ShipOrder.BUILD_STARBASE);
            case UPGRADE_OUTPOST:
                return stationBuildPoints >= 1 && !isDoingStationWork(ShipOrder.UPGRADE_OUTPOST);
            case UPGRADE_STARBASE:
                return stationBuildPoints >= 1 && !isDoingStationWork(ShipOrder.UPGRADE_STARBASE);
            case BLOCKADE_SYSTEM:
                return hasSpecial(ShipSpecial.BLOCKADESHIP);
            case ATTACK_SYSTEM:
                return !isCloakOn() && !isStation();
            case TRANSPORT:
                return getStorageRoom() >= 1;
            case RAID_SYSTEM:
                return hasSpecial(ShipSpecial.RAIDER);
            case AUTO_EXPLORE:
                return (getShipType() == ShipType.SCOUT || getShipType() == ShipType.PROBE);
            default: // new commands unhandled
                System.out.println("Command not found!");
        }
        return false;
    }

    public boolean canCloak() {
        return stealthGrade >= 4 && !isStation();
    }

    public boolean isAlive() {
        return hull.getCurrentHull() >= 1;
    }

    public boolean isDoingStationWork() {
        return isDoingStationWork(ShipOrder.NONE);
    }

    public boolean isDoingStationWork(ShipOrder ignore) {
        ShipOrder[] values = { ShipOrder.BUILD_OUTPOST, ShipOrder.BUILD_STARBASE, ShipOrder.UPGRADE_OUTPOST,
                ShipOrder.UPGRADE_STARBASE };
        Array<ShipOrder> stationOrders = new Array<ShipOrder>(false, 4);
        stationOrders.addAll(values);
        if (ignore != ShipOrder.NONE)
            stationOrders.removeValue(ignore, false);
        for (ShipOrder order : stationOrders)
            if (currentOrder == order)
                return true;
        return false;
    }

    /**
     * Execute a 1-turn shield (always) and hull (if bAtShipPort == TRUE) repairing step
     * @param atShipPort
     * @param fasterShieldRecharge
     */
    public void repair(boolean atShipPort, boolean fasterShieldRecharge) {
        shield.rechargeShields(200 * (fasterShieldRecharge ? 2 : 1));
        if (atShipPort)
            hull.repairHull();
    }

    public void extractDeuterium() {
        int deut = 50;
        Research research = getOwner().getEmpire().getResearch();
        deut += deut * Math.min(research.getEnergyTech(), research.getPropulsionTech());
        if (resourceManager.getMonopolOwner(ResourceTypes.DEUTERIUM.getType()).equals(getOwnerId()))
            deut *= 2;
        int bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 2);
        if (bonus != 0)
            deut += bonus * deut / 100;
        deut = Math.min(deut, getFreeStorageRoom());
        loadedResources[ResourceTypes.DEUTERIUM.getType()] += deut;
    }

    public void retreatShip(IntPoint retreatSector) {
        retreatShip(retreatSector, null);
    }

    public void retreatShip(IntPoint retreatSector, CombatTactics newTactics) {
        unsetCurrentOrder();// cancel terraform or station build
        //erase retreat tactic
        if (newTactics != null)
            combatTactics = newTactics;
        else
            setCombatTacticsAccordingToType();
        // can the ship still fly?
        if (speed > 0) {
            coordinates = retreatSector;
            //delete curent target
            targetCoord = new IntPoint(-1, -1);
        }
    }

    public void calcEffectsForSingleShip(Sector sector, Race race, boolean deactivatedShipScanner, boolean betterScanner,
            boolean fleetship) {
        String raceId = race.getRaceId();
        boolean isMajor = race.isMajor();
        if (!fleetship && isMajor)
            sector.setFullKnown(raceId);
        if (!deactivatedShipScanner)
            sector.putScannedSquare(getScanRange(), getScanPower(), race, betterScanner, hasSpecial(ShipSpecial.PATROLSHIP));
        // Ships if we have not enough scanpower. From stealthGrade > 4 it has to be cloacked, otherwise only grade 3 is take into account
        if (!isStation()) {
            int stealthPower = stealthGrade;
            if (!isCloakOn() && stealthPower > 3)
                stealthPower = 3;
            int neededScanPower = stealthPower * 20;
            if (neededScanPower < sector.getNeededScanPower(raceId))
                sector.setNeededScanPower(neededScanPower, raceId);
        }
        if (!fleetship) {
            ShipOrder currentOrder = getCurrentOrder();
            if (isDoingStationWork())
                sector.setIsStationBuilding(currentOrder, raceId);
            else if (currentOrder == ShipOrder.TERRAFORM)
                sector.terraforming(this);
        }
        if (isMajor) {
            Major major = (Major) race;
            major.getEmpire().addShipCosts(getMaintenanceCosts());
            major.getShipHistory().modifyShip(shipHistoryInfo(), sector.coordsName(true));
        }
        calcExp();
    }

    public void setNewExploreCoord(StarMap starMap) {
        targetCoord = starMap.popClosestExploreSector(this);
        if (!targetCoord.equals(new IntPoint())) {
            Pair<IntPoint, Array<IntPoint>> p = starMap.calcPath(coordinates, targetCoord, range.getRange(), speed);
            path = p.getSecond();
        }
    }

    /**
     * Takes into account the special research of the race and upgrades the stats of the ship
     * @param owner
     */
    public void addSpecialResearchBoni(Race owner) {
        addSpecialResearchBoni(owner, ResearchComplexType.NONE);
    }

    public void addSpecialResearchBoni(Race owner, ResearchComplexType type) {
        ResearchInfo info = null;
        if (owner == null)
            info = getOwner().getEmpire().getResearch().getResearchInfo();
        else
            info = owner.getEmpire().getResearch().getResearchInfo();
        if (info == null)
            return;
        ResearchComplex rc;
        //Special Research #0: Weapons
        if (type == ResearchComplexType.WEAPONS_TECHNOLOGY || type == ResearchComplexType.NONE) {
            rc = info.getResearchComplex(ResearchComplexType.WEAPONS_TECHNOLOGY);
            if (rc.getComplexStatus() == ResearchStatus.RESEARCHED) {
                // 30% higher phasor damage
                if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                    for (int i = 0; i < beamWeapons.size; i++) {
                        int oldPower = beamWeapons.get(i).getBeamPower();
                        beamWeapons.get(i).setBeamPower(oldPower + (oldPower * rc.getBonus(1) / 100));
                    }
                } // 20% better torpedo accuracy
                else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                    for (int i = 0; i < torpedoWeapons.size; i++) {
                        int oldAcc = torpedoWeapons.get(i).getAccuracy();
                        torpedoWeapons.get(i).setAccuracy(oldAcc + (oldAcc * rc.getBonus(2) / 100));
                    }
                } // 20% higher firing rate
                else if (rc.getFieldStatus(3) == ResearchStatus.RESEARCHED) {
                    for (int i = 0; i < beamWeapons.size; i++) {
                        int oldRate = beamWeapons.get(i).getRechargeTime();
                        beamWeapons.get(i).setRechargeTime(oldRate - (oldRate * rc.getBonus(3) / 100));
                    }
                    for (int i = 0; i < torpedoWeapons.size; i++) {
                        int oldRate = torpedoWeapons.get(i).getTubeFirerate();
                        torpedoWeapons.get(i).setTubeFirerate(oldRate - (oldRate * rc.getBonus(3) / 100));
                    }
                }
            }
        }
        //Special Research #1: Construction
        if (type == ResearchComplexType.CONSTRUCTION_TECHNOLOGY || type == ResearchComplexType.NONE) {
            rc = info.getResearchComplex(ResearchComplexType.CONSTRUCTION_TECHNOLOGY);
            if (rc.getComplexStatus() == ResearchStatus.RESEARCHED) {
                //20% better shields
                if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                    int maxShield = shield.getMaxShield();
                    int shieldType = shield.getShieldType();
                    boolean regenerative = shield.isRegenerative();
                    shield.modifyShield((maxShield + (maxShield * rc.getBonus(1) / 100)), shieldType, regenerative);
                } //20% better hull
                else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                    boolean doubleHull = hull.isDoubleHull();
                    boolean ablative = hull.isAblative();
                    boolean polarisation = hull.isPolarisation();
                    int baseHull = hull.getBaseHull();
                    int hullMaterial = hull.getHullMaterial();
                    hull.modifyHull(doubleHull, (baseHull + (baseHull * rc.getBonus(2) / 100)), hullMaterial, ablative, polarisation);
                } //50$ better scanner
                else if (rc.getFieldStatus(3) == ResearchStatus.RESEARCHED) {
                    int sp = getScanPower();
                    setScanPower(sp + (sp * rc.getBonus(3) / 100));
                }
            }
        }
        //Special Research #2: General Ship Technology
        if (type == ResearchComplexType.GENERAL_SHIP_TECHNOLOGY || type == ResearchComplexType.NONE) {
            rc = info.getResearchComplex(ResearchComplexType.GENERAL_SHIP_TECHNOLOGY);
            if (rc.getComplexStatus() == ResearchStatus.RESEARCHED) {
                // higher range for ships which had short range
                if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                    if (range == ShipRange.SHORT)
                        setRange(ShipRange.fromInt(rc.getBonus(1)));
                }// higher speed for ships with speed == 1
                else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                    if (speed == 1)
                        setSpeed(rc.getBonus(2));
                }
            }
        }
        //Special Research #3: Peaceful Ship Technology
        if (type == ResearchComplexType.PEACEFUL_SHIP_TECHNOLOGY || type == ResearchComplexType.NONE) {
            rc = info.getResearchComplex(ResearchComplexType.PEACEFUL_SHIP_TECHNOLOGY);
            if (rc.getComplexStatus() == ResearchStatus.RESEARCHED && shipType.getType() <= ShipType.COLONYSHIP.getType()) {
                //25% extra transport capacity
                if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                    int storage = getStorageRoom();
                    setStorageRoom(storage + (storage * rc.getBonus(1) / 100));
                }
                // no maintenance costs
                if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                    setMaintenanceCosts(0);
                }
            }
        }
        //Special Research #5: Troops
        if (type == ResearchComplexType.TROOPS && hasTroops()) {
            rc = info.getResearchComplex(ResearchComplexType.TROOPS);
            if (rc.getComplexStatus() == ResearchStatus.RESEARCHED) {
                for (int i = 0; i < troops.size; i++) {
                    //20% offensive
                    if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                        int power = troops.get(i).getOffense();
                        troops.get(i).setOffense(power + (power * rc.getBonus(1)) / 100);
                    } else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                        troops.get(i).addExperiencePoints(rc.getBonus(2));
                    }
                }
            }
        }
    }

    public boolean buildStationbyShip(ShipOrder order, Sector sector, Major race, int id) {
        if (!sector.setNeededStationPoints(stationBuildPoints, ownerID))
            return false;
        ShipType type = (order == ShipOrder.BUILD_OUTPOST || order == ShipOrder.UPGRADE_OUTPOST) ? ShipType.OUTPOST
                : ShipType.STARBASE;
        sector.buildStation(type, ownerID);
        EmpireNews message = new EmpireNews();
        String s1 = (order == ShipOrder.BUILD_OUTPOST) ? "OUTPOST_FINISHED"
                : ((order == ShipOrder.BUILD_STARBASE) ? "STARBASE_FINISHED"
                        : ((order == ShipOrder.UPGRADE_OUTPOST) ? "OUTPOST_UPGRADE_FINISHED" : "STARBASE_UPGRADE_FINISHED"));
        message.CreateNews(StringDB.getString(s1), EmpireNewsType.MILITARY, "", sector.getCoordinates());
        race.getEmpire().addMsg(message);
        String s2 = (order == ShipOrder.BUILD_OUTPOST) ? "OUTPOST_CONSTRUCTION"
                : ((order == ShipOrder.BUILD_STARBASE) ? "STARBASE_CONSTRUCTION"
                        : ((order == ShipOrder.UPGRADE_OUTPOST) ? "OUTPOST_UPGRADE" : "STARBASE_UPGRADE"));
        if (race.isHumanPlayer()) {
            SndMgrValue val = SndMgrValue.SNDMGR_MSG_OUTPOST_READY;
            if (type == ShipType.STARBASE)
                val = SndMgrValue.SNDMGR_MSG_STARBASE_READY;
            resourceManager.getClientWorker().addSoundMessage(val, race, 0);
            resourceManager.getClientWorker().setEmpireViewFor(race);
        }
        resourceManager.getUniverseMap().buildShip(id, sector.getCoordinates(), ownerID);
        unsetCurrentOrder(false);
        race.addLostShipHistory(shipHistoryInfo(), StringDB.getString(s2), StringDB.getString("DESTROYED"),
                resourceManager.getCurrentRound(), coordinates);
        return true;
    }

    public void scrap(Major major, StarSystem system, boolean disassembly) {
        major.getShipHistory().modifyShip(shipHistoryInfo(), system.coordsName(true), resourceManager.getCurrentRound(),
                StringDB.getString(disassembly ? "DISASSEMBLY" : "UPGRADE"), StringDB.getString("DESTROYED"));
        if (!system.getOwnerId().equals(ownerID))
            return;
        //if we scrap in an owned sector, then we get back some of the resources and maybe some money
        double percent = RandUtil.random() * 26 + 50;
        percent += system.getProduction().getShipRecycling();
        int id = ID - 10000;
        ShipInfo si = resourceManager.getShipInfos().get(id);
        GameResources res = si.getNeededResource();
        double factor = percent / 100;
        res.deritium = 0;
        res.multiply(factor);
        system.setStores(res);
        major.getEmpire().setCredits((int) (si.getNeededIndustry() * factor));
    }

    @Override
    public void setOwner(String id) {
        super.setOwner(id);
        troops.clear();
    }

    public ShipHistoryStruct shipHistoryInfo() {
        return new ShipHistoryStruct(name, getShipTypeAsString(), shipClass, "", "", getCurrentOrderAsString(), "",
                getCurrentTargetAsString(getOwner()), 0, 0, crewExperience);
    }

    public void setResourceManager(ResourceManager res) {
        setDescription(res.getShipInfos().get(ID - 10000).getDescription());
        super.setResourceManager(res);
    }

    /**
     * Returns the beam info of the ship
     * @param bwps - will hold the count of different beam weapons
     * @return
     */
    public int getBeamInfo(ObjectIntMap<String> bwps) {
        bwps.clear();
        int overallDmg = 0;

        for (int i = 0; i < beamWeapons.size; i++) {
            BeamWeapons beam = beamWeapons.get(i);
            String s = StringDB.getString("TYPE") + " " + beam.getBeamType() + " " + beam.getBeamName();
            int cnt = bwps.get(s, 0);
            bwps.put(s, cnt + beam.getBeamNumber());

            overallDmg += beam.getBeamDamage();
        }
        return overallDmg;
    }

    public int getTorpedoInfo(ObjectIntMap<String> twps) {
        twps.clear();
        int overallDmg = 0;
        for (int i = 0; i < torpedoWeapons.size; i++) {
            TorpedoWeapons torpedo = torpedoWeapons.get(i);
            String s = torpedo.getTubeName() + " (" + torpedo.getTorpedoName() + ")";
            int cnt = twps.get(s, 0);
            twps.put(s, cnt + torpedo.getNumberOfTubes());
            overallDmg += (torpedo.getTorpedoPower() * torpedo.getNumber() * 100 * torpedo.getNumberOfTubes())
                    / torpedo.getTubeFirerate();
        }
        return overallDmg;
    }

    public void fillWeaponStats(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor,
            boolean showOnlyDamage) {
        String text;
        Label l;
        ObjectIntMap<String> bwps = new ObjectIntMap<String>();

        text = StringDB.getString("BEAMWEAPONS") + " ";
        if (!showOnlyDamage)
            text += "(";
        text += StringDB.getString("DAMAGE") + ": " + getBeamInfo(bwps);
        if (!showOnlyDamage)
            text += ")";

        l = new Label(text, skin, textFont, showOnlyDamage ? textColor : headerColor);
        table.add(l);
        table.row();

        if (!showOnlyDamage) {
            text = "";
            for (Iterator<Entry<String>> iter = bwps.entries().iterator(); iter.hasNext();) {
                Entry<String> e = iter.next();
                text = e.value + " x " + e.key;
                l = new Label(text, skin, textFont, textColor);
                table.add(l);
                table.row();
            }

            if (text.isEmpty()) {
                text = StringDB.getString("NONE");
                l = new Label(text, skin, textFont, textColor);
                table.add(l);
                table.row();
            }
        }

        ObjectIntMap<String> twps = new ObjectIntMap<String>();

        text = StringDB.getString("TORPEDOWEAPONS") + " ";
        if (!showOnlyDamage)
            text += "(";
        text += StringDB.getString("DAMAGE") + ": " + getTorpedoInfo(twps);
        if (!showOnlyDamage)
            text += ")";

        l = new Label(text, skin, textFont, showOnlyDamage ? textColor : headerColor);
        table.add(l);
        table.row();

        if (!showOnlyDamage) {
            text = "";
            for (Iterator<Entry<String>> iter = twps.entries().iterator(); iter.hasNext();) {
                Entry<String> e = iter.next();
                text = e.value + " x " + e.key;
                l = new Label(text, skin, textFont, textColor);
                table.add(l);
                table.row();
            }

            if (text.isEmpty()) {
                text = StringDB.getString("NONE");
                l = new Label(text, skin, textFont, textColor);
                table.add(l);
                table.row();
            }
        }
    }

    /**
     * Generates a tooltip from the ship's characteristics
     * @param info
     * @param table
     * @param skin
     * @param headerFont
     * @param headerColor
     * @param textFont
     * @param textColor
     */
    public void getTooltip(FleetInfoForGetTooltip info, Table table, Skin skin, String headerFont, Color headerColor,
            String textFont, Color textColor) {
        table.clearChildren();
        String text;
        if (!name.isEmpty()) {
            if (info != null)
                text = ((Ships) this).getFleetName();
            else
                text = getName();
            Label l = new Label(text, skin, headerFont, headerColor);
            table.add(l);
            table.row();
        }

        if (info != null && info.fleetShipType == -1)
            text = "(" + StringDB.getString("MIXED_FLEET") + ")";
        else if (info != null)
            text = "(" + getShipTypeAsString(true) + ")";
        else
            text = (name.isEmpty() ? "" : "(") + getShipTypeAsString() + (name.isEmpty() ? "" : ")");
        Label l = new Label(text, skin, name.isEmpty() ? headerFont : textFont, headerColor);
        table.add(l);
        table.row();

        text = String.format("%s: %d     %s: %d     %s: %d", StringDB.getString("SHIPSIZE"), getSize().getSize(),
                StringDB.getString("SHIPCOSTS"), getMaintenanceCosts(), StringDB.getString("EXPERIANCE"), getCrewExperience());
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        //movement
        text = StringDB.getString("MOVEMENT");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        Table tmpTable = new Table();
        ShipRange rng = getRange();
        if (info != null)
            rng = info.fleetRange;
        text = StringDB.getString("RANGE") + ": " + StringDB.getString(rng.getName());
        l = new Label(text, skin, textFont, textColor);
        tmpTable.add(l).spaceRight(GameConstants.wToRelative(50));

        int speed = getSpeed();
        if (info != null)
            speed = info.fleetSpeed;
        text = StringDB.getString("SPEED") + ": " + speed;
        l = new Label(text, skin, textFont, textColor);
        tmpTable.add(l);
        if (info != null) {//if it's a fleet show no more infos
            table.add(tmpTable);
            table.row();
            return;
        }
        if (storageRoom > 0) {
            text = StringDB.getString("PLACE") + ": " + storageRoom;
            l = new Label(text, skin, textFont, textColor);
            tmpTable.add(l).spaceLeft(GameConstants.wToRelative(50));
        }
        table.add(tmpTable);
        table.row();

        text = StringDB.getString("MANEUVERABILITY") + ": " + ShipInfo.getManeuverabilityAsString(getManeuverabilty());
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        fillWeaponStats(table, skin, headerFont, headerColor, textFont, textColor, false);

        //Shields and hull
        text = StringDB.getString("SHIELDS") + " " + StringDB.getString("AND") + " " + StringDB.getString("HULL");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        if (shield != null) {
            text = String.format("%s %d %s: %s %d/%d", StringDB.getString("TYPE"), shield.getShieldType(),
                    StringDB.getString("SHIELDS"), StringDB.getString("CAPACITY"), shield.getCurrentShield(),
                    shield.getMaxShield());
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }

        String hullMaterial = StringDB.getString(ResourceTypes.fromResourceTypes(hull.getHullMaterial()).getName());
        text = String.format("%s %s: %s %d/%d", hullMaterial,
                StringDB.getString(hull.isDoubleHull() ? "DOUBLE_HULL_ARMOUR" : "HULL_ARMOR"), StringDB.getString("INTEGRITY"),
                hull.getCurrentHull(), hull.getMaxHull());
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        //sensors
        text = StringDB.getString("SENSORS");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();
        tmpTable = new Table();
        text = StringDB.getString("SCANRANGE") + ": " + getScanRange();
        l = new Label(text, skin, textFont, textColor);
        tmpTable.add(l).spaceRight(GameConstants.wToRelative(50));
        text = StringDB.getString("SCANPOWER") + ": " + getScanPower();
        l = new Label(text, skin, textFont, textColor);
        tmpTable.add(l);
        table.add(tmpTable);
        table.row();

        //special abilities
        text = StringDB.getString("SPECIAL_ABILITIES");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();
        text = "";
        if (hasSpecial(ShipSpecial.ASSAULTSHIP))
            text += StringDB.getString("ASSAULTSHIP") + "\n";
        if (hasSpecial(ShipSpecial.BLOCKADESHIP))
            text += StringDB.getString("BLOCKADESHIP") + "\n";
        if (hasSpecial(ShipSpecial.COMMANDSHIP))
            text += StringDB.getString("COMMANDSHIP") + "\n";
        if (hasSpecial(ShipSpecial.DOGFIGHTER))
            text += StringDB.getString("DOGFIGHTER") + "\n";
        if (hasSpecial(ShipSpecial.DOGKILLER))
            text += StringDB.getString("DOGKILLER") + "\n";
        if (hasSpecial(ShipSpecial.PATROLSHIP))
            text += StringDB.getString("PATROLSHIP") + "\n";
        if (hasSpecial(ShipSpecial.RAIDER))
            text += StringDB.getString("RAIDER") + "\n";
        if (hasSpecial(ShipSpecial.SCIENCEVESSEL))
            text += StringDB.getString("SCIENCESHIP") + "\n";
        if (shield.isRegenerative())
            text += StringDB.getString("REGENERATIVE_SHIELDS") + "\n";
        if (hull.isAblative())
            text += StringDB.getString("ABLATIVE_ARMOR") + "\n";
        if (hull.isPolarisation())
            text += StringDB.getString("HULLPOLARISATION") + "\n";
        if (canCloak())
            text += StringDB.getString("CAN_CLOAK") + "\n";
        text = text.trim();
        if (text.isEmpty())
            text = StringDB.getString("NONE");
        l = new Label(text, skin, textFont, textColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(500));
        table.row();

        text = getDescription();
        l = new Label(text, skin, textFont, textColor);
        l.setWrap(true);
        l.setAlignment(Align.center, Align.top);
        table.add(l).width(GameConstants.wToRelative(500));
        table.row();
    }

    /**
     * Serialization output
     * @param kryo
     * @param output
     */
    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        kryo.writeObject(output, hull);
        kryo.writeObject(output, shield);
        kryo.writeObject(output, torpedoWeapons);
        kryo.writeObject(output, beamWeapons);
        output.writeInt(ID);
        output.writeInt(maintenanceCosts);
        kryo.writeObject(output, shipType);
        kryo.writeObject(output, shipSize);
        output.writeInt(maneuverabilty);
        output.writeInt(speed);
        kryo.writeObject(output, range);
        output.writeInt(scanPower);
        output.writeInt(scanRange);
        output.writeInt(storageRoom);
        output.writeInt(colonizePoints);
        output.writeInt(stationBuildPoints);
        kryo.writeObject(output, special);
        output.writeString(shipClass);
        output.writeInt(stealthGrade);
        kryo.writeObject(output, currentOrder);
        kryo.writeObject(output, targetCoord);
        kryo.writeObject(output, path);
        output.writeBoolean(cloakOn);
        output.writeInt(terraformingPlanet);
        output.writeBoolean(flagShip);
        output.writeInt(crewExperience);
        kryo.writeObject(output, troops);
        kryo.writeObject(output, loadedResources);
        kryo.writeObject(output, combatTactics);
    }

    /**
     * Serialization input
     * @param kryo
     * @param input
     */
    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        hull = kryo.readObject(input, Hull.class);
        shield = kryo.readObject(input, Shield.class);
        torpedoWeapons = kryo.readObject(input, Array.class);
        beamWeapons = kryo.readObject(input, Array.class);
        ID = input.readInt();
        maintenanceCosts = input.readInt();
        shipType = kryo.readObject(input, ShipType.class);
        shipSize = kryo.readObject(input, ShipSize.class);
        maneuverabilty = input.readInt();
        speed = input.readInt();
        range = kryo.readObject(input, ShipRange.class);
        scanPower = input.readInt();
        scanRange = input.readInt();
        storageRoom = input.readInt();
        colonizePoints = input.readInt();
        stationBuildPoints = input.readInt();
        special = kryo.readObject(input, ShipSpecial[].class);
        shipClass = input.readString();
        stealthGrade = input.readInt();
        currentOrder = kryo.readObject(input, ShipOrder.class);
        targetCoord = kryo.readObject(input, IntPoint.class);
        path = kryo.readObject(input, Array.class);
        cloakOn = input.readBoolean();
        terraformingPlanet = input.readInt();
        flagShip = input.readBoolean();
        crewExperience = input.readInt();
        troops = kryo.readObject(input, Array.class);
        loadedResources = kryo.readObject(input, int[].class);
        combatTactics = kryo.readObject(input, CombatTactics.class);
    }
    //TODO: sanity checks
}
