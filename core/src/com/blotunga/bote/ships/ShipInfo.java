/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.EntityType;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.general.GameResources;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.utils.KryoV;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * @author dragon
 * @version 1.1
 */
public class ShipInfo extends Ship {
    private int race;	// which race can build the ship
    // necessary research
    private int bioTech;
    private int energyTech;
    private int compTech;
    private int propulsionTech;
    private int constructionTech;
    private int weaponTech;
    // resources needed to build ship
    private int neededIndustry;
    private int neededTitan;
    private int neededDeuterium;
    private int neededDuranium;
    private int neededCrystal;
    private int neededIridium;
    private int neededDeritium;
    // base costs, as the needed costs vary over time
    private int baseIndustry;
    private int baseTitan;
    private int baseDeuterium;
    private int baseDuranium;
    private int baseCrystal;
    private int baseIridium;
    private int baseDeritium;
    // restricted to build in system == name?
    private String onlyInSystem;
    // ship class which is replaced by this one.
    private String obsoletesClass = ""; //deprecated field - kept only for save compatibility
    private int obsoletesClassId;

    public ShipInfo() {
        this((ResourceManager) null);
    }

    public ShipInfo(ShipInfo other) {
        this(other, other);
    }

    public ShipInfo(Ship ship, ShipInfo other) {
        super(ship);
        race = other.race;
        bioTech = other.bioTech;
        energyTech = other.energyTech;
        compTech = other.compTech;
        propulsionTech = other.propulsionTech;
        constructionTech = other.constructionTech;
        weaponTech = other.weaponTech;
        neededIndustry = other.neededIndustry;
        neededTitan = other.neededTitan;
        neededDeuterium = other.neededDeuterium;
        neededDuranium = other.neededDuranium;
        neededCrystal = other.neededCrystal;
        neededIridium = other.neededIridium;
        neededDeritium = other.neededDeritium;
        baseIndustry = other.baseIndustry;
        baseTitan = other.baseTitan;
        baseDeuterium = other.baseDeuterium;
        baseDuranium = other.baseDuranium;
        baseCrystal = other.baseCrystal;
        baseIridium = other.baseIridium;
        baseDeritium = other.baseDeritium;
        onlyInSystem = other.onlyInSystem;
        obsoletesClassId = other.obsoletesClassId;
    }

    public ShipInfo(ResourceManager res) {
        super(res);
        type = EntityType.SHIPINFO;
        race = 0;
        bioTech = 0;
        energyTech = 0;
        compTech = 0;
        propulsionTech = 0;
        constructionTech = 0;
        weaponTech = 0;
        neededIndustry = 0;
        neededTitan = 0;
        neededDeuterium = 0;
        neededDuranium = 0;
        neededCrystal = 0;
        neededIridium = 0;
        neededDeritium = 0;
        baseIndustry = 0;
        baseTitan = 0;
        baseDeuterium = 0;
        baseDuranium = 0;
        baseCrystal = 0;
        baseIridium = 0;
        baseDeritium = 0;
        onlyInSystem = "";
        obsoletesClassId = -1;
    }

    @Override
    public void setResourceManager(ResourceManager res) {
        super.setResourceManager(res);
        if (!obsoletesClass.isEmpty())
            obsoletesClassId = getObsoletesClassIdx(obsoletesClass);
    }

    public int getRace() {
        return race;
    }

    public int getBioTech() {
        return bioTech;
    }

    public int getEnergyTech() {
        return energyTech;
    }

    public int getCompTech() {
        return compTech;
    }

    public int getPropulsionTech() {
        return propulsionTech;
    }

    public int getConstructionTech() {
        return constructionTech;
    }

    public int getWeaponTech() {
        return weaponTech;
    }

    public int[] getNeededResearchLevels() {
        int researchLevels[] = { bioTech, energyTech, compTech, propulsionTech, constructionTech, weaponTech };
        return researchLevels;
    }

    public int getNeededIndustry() {
        return neededIndustry;
    }

    public int getBaseIndustry() {
        return baseIndustry;
    }

    public int getNeededTitan() {
        return neededTitan;
    }

    public int getNeededDeuterium() {
        return neededDeuterium;
    }

    public int getNeededDuranium() {
        return neededDuranium;
    }

    public int getNeededCrystal() {
        return neededCrystal;
    }

    public int getNeededIridium() {
        return neededIridium;
    }

    public int getNeededDeritium() {
        return neededDeritium;
    }

    public GameResources getNeededResource() {
        return new GameResources(0, neededTitan, neededDeuterium, neededDuranium, neededCrystal, neededIridium, neededDeritium);
    }

    public int getNeededResource(int res) {
        ResourceTypes rt = ResourceTypes.fromResourceTypes(res);
        switch (rt) {
            case TITAN:
                return neededTitan;
            case DEUTERIUM:
                return neededDeuterium;
            case DURANIUM:
                return neededDuranium;
            case CRYSTAL:
                return neededCrystal;
            case IRIDIUM:
                return neededIridium;
            case DERITIUM:
                return neededDeritium;
            default:
                return 0;
        }
    }

    public int getNeededIndustryBase() {
        return baseIndustry;
    }

    public int getNeededResourceBase(int res) {
        ResourceTypes rt = ResourceTypes.fromResourceTypes(res);
        switch (rt) {
            case TITAN:
                return baseTitan;
            case DEUTERIUM:
                return baseDeuterium;
            case DURANIUM:
                return baseDuranium;
            case CRYSTAL:
                return baseCrystal;
            case IRIDIUM:
                return baseIridium;
            case DERITIUM:
                return baseDeritium;
            default:
                return 0;
        }
    }

    public String getOnlyInSystem() {
        return onlyInSystem;
    }

    public int getObsoletesClassId() {
        return obsoletesClassId + 10000;
    }

    public int getObsoletesClassIdx(String name) {
        for (ShipInfo si : resourceManager.getShipInfos())
            if (si.getShipClass().equals(name))
                return si.getID() - 10000;
        return -1;
    }

    public int getObsoletesClassIdx() {
        return obsoletesClassId;
    }

    public void setRace(int race) {
        this.race = race;
    }

    public void setResearch(ResearchType rt, int value) {
        switch (rt) {
            case BIO:
                this.bioTech = value;
                break;
            case ENERGY:
                this.energyTech = value;
                break;
            case COMPUTER:
                this.compTech = value;
                break;
            case PROPULSION:
                this.propulsionTech = value;
                break;
            case CONSTRUCTION:
                this.constructionTech = value;
                break;
            case WEAPON:
                this.weaponTech = value;
                break;
            default:
                break;
        }
    }

    public void setBioTech(int bioTech) {
        this.bioTech = bioTech;
    }

    public void setEnergyTech(int energyTech) {
        this.energyTech = energyTech;
    }

    public void setCompTech(int compTech) {
        this.compTech = compTech;
    }

    public void setPropulsionTech(int propulsionTech) {
        this.propulsionTech = propulsionTech;
    }

    public void setConstructionTech(int constructionTech) {
        this.constructionTech = constructionTech;
    }

    public void setWeaponTech(int weaponTech) {
        this.weaponTech = weaponTech;
    }

    public void setNeededIndustry(int neededIndustry) {
        this.neededIndustry = neededIndustry;
        baseIndustry = neededIndustry;
    }

    public void setNeededIndustryBase(int neededIndustry) {
        this.baseIndustry = neededIndustry;
    }

    public void setNeededResourceBase(ResourceTypes rt, int value) {
        switch (rt) {
            case TITAN:
                baseTitan = value;
                break;
            case DEUTERIUM:
                baseDeuterium = value;
                break;
            case DURANIUM:
                baseDuranium = value;
                break;
            case CRYSTAL:
                baseCrystal = value;
                break;
            case IRIDIUM:
                baseIridium = value;
                break;
            case DERITIUM:
                baseDeritium = value;
                break;
            default:
                break;
        }
    }

    public void setNeededResource(ResourceTypes rt, int value) {
        switch (rt) {
            case TITAN:
                setNeededTitan(value);
                break;
            case DEUTERIUM:
                setNeededDeuterium(value);
                break;
            case DURANIUM:
                setNeededDuranium(value);
                break;
            case CRYSTAL:
                setNeededCrystal(value);
                break;
            case IRIDIUM:
                setNeededIridium(value);
                break;
            case DERITIUM:
                setNeededDeritium(value);
                break;
            default:
                break;
        }
    }

    public void setNeededTitan(int neededTitan) {
        this.neededTitan = neededTitan;
        baseTitan = neededTitan;
    }

    public void setNeededDeuterium(int neededDeuterium) {
        this.neededDeuterium = neededDeuterium;
        baseDeuterium = neededDeuterium;
    }

    public void setNeededDuranium(int neededDuranium) {
        this.neededDuranium = neededDuranium;
        baseDuranium = neededDuranium;
    }

    public void setNeededCrystal(int neededCrystal) {
        this.neededCrystal = neededCrystal;
        baseCrystal = neededCrystal;
    }

    public void setNeededIridium(int neededIridium) {
        this.neededIridium = neededIridium;
        baseIridium = neededIridium;
    }

    public void setNeededDeritium(int neededDeritium) {
        this.neededDeritium = neededDeritium;
        baseDeritium = neededDeritium;
    }

    public void setOnlyInSystem(String onlyInSystem) {
        this.onlyInSystem = onlyInSystem;
    }

    public void setObsoletesClass(int obsoletesClass) {
        this.obsoletesClassId = obsoletesClass;
    }

    public void deleteWeapons() {
        torpedoWeapons.clear();
        beamWeapons.clear();
    }

    /**
     * Calculates the final resource costs of the ship
     */
    public void calculateFinalCosts() {
        neededIndustry = 0;
        neededTitan = 0;
        neededDeuterium = 0;
        neededDuranium = 0;
        neededCrystal = 0;
        neededIridium = 0;
        neededDeritium = 0;
        int beamTypeAdd = 0;
        for (BeamWeapons bw : beamWeapons)
            beamTypeAdd += (bw.getBeamPower() * bw.getBeamNumber() * bw.getBeamType() * 3);
        if (beamWeapons.size > 1)
            beamTypeAdd /= beamWeapons.size;
        neededIndustry += beamTypeAdd;
        neededIndustry += getCompleteOffensivePower();
        // a double hull grows industry costs by 25%
        if (hull.isDoubleHull())
            neededIndustry += (int) (getCompleteDefensivePower() / 1.5f);
        else
            neededIndustry += getCompleteDefensivePower() / 2;
        neededIndustry += getShield().getMaxShield() / 200 * (int) (Math.pow(getShield().getShieldType(), 2.5f));
        neededIndustry /= 2;

        neededIndustry += baseIndustry;
        neededTitan += baseTitan;
        neededDeuterium += baseDeuterium;
        neededDuranium += baseDuranium;
        neededCrystal += baseCrystal;
        neededIridium += baseIridium;
        neededDeritium += baseDeritium;

        int hullMaterial = hull.getHullMaterial();
        if (hullMaterial == ResourceTypes.TITAN.getType()) {
            neededTitan += hull.getBaseHull() * (hull.isDoubleHull() ? 2 : 1);
        } else if (hullMaterial == ResourceTypes.DURANIUM.getType()) {
            neededDuranium += hull.getBaseHull() * (hull.isDoubleHull() ? 2 : 1);
        } else if (hullMaterial == ResourceTypes.IRIDIUM.getType()) {
            neededIridium += hull.getBaseHull() * (hull.isDoubleHull() ? 2 : 1);
        }
        // for torpedoes with power over 500 we need extra deuterium
        for (TorpedoWeapons tw : torpedoWeapons)
            if (tw.getTorpedoPower() >= 500)
                neededDeuterium += tw.getTorpedoPower() * tw.getNumber() * tw.getNumberOfTubes();
    }

    /**
     * Function sets the first order of the ship after being built based in its type
     */
    public void setStartOrder() {
        setCurrentOrderAccordingToType();
    }

    public void setStartTactics() {
        setCombatTacticsAccordingToType();
    }

    public boolean isThisShipBuildableNow(int[] researchLevels) {
        if (researchLevels[ResearchType.WEAPON.getType()] < getWeaponTech())
            return false;
        if (researchLevels[ResearchType.CONSTRUCTION.getType()] < getConstructionTech())
            return false;
        if (researchLevels[ResearchType.PROPULSION.getType()] < getPropulsionTech())
            return false;
        if (researchLevels[ResearchType.COMPUTER.getType()] < getCompTech())
            return false;
        if (researchLevels[ResearchType.ENERGY.getType()] < getEnergyTech())
            return false;
        if (researchLevels[ResearchType.BIO.getType()] < getBioTech())
            return false;

        return true;
    }

    /**
     * @param infoTable
     * @param skin
     * @param markColor
     * @param normalColor
     * @param width
     * @param vpadding
     * @param version - 0 - buildmenu, 1 - shipdesign, 2 - database
     */
    public void drawShipInfo(Table infoTable, Skin skin, Color markColor, Color normalColor, float width, float vpadding,
            int version) {
        if (version == 0 || version == 2) { //buildmenu or database
            Label nameLabel = new Label(getShipTypeAsString(), skin, "normalFont", markColor);
            nameLabel.setWrap(true);
            nameLabel.setAlignment(Align.center);
            infoTable.add(nameLabel).width(width).spaceBottom(vpadding);
            infoTable.row();
        }
        String text = "";
        if (version == 0 || version == 2) { //buildmenu or database
            text = StringDB.getString("SPEED") + ": " + speed + " - " + StringDB.getString("RANGE") + ": "
                    + StringDB.getString(range.getName());
        } else { //designmenu
            text = StringDB.getString("SHIPSIZE") + ": " + StringDB.getString(shipSize.getName()) + " - "
                    + StringDB.getString("SHIPCOSTS") + ": " + maintenanceCosts + " - " + StringDB.getString("SPEED") + ": "
                    + speed + " - " + StringDB.getString("RANGE") + ": " + StringDB.getString(range.getName());
        }
        if (storageRoom > 0)
            text += " - " + StringDB.getString("PLACE") + ": " + storageRoom;
        Label speedLabel = new Label(text, skin, "normalFont", normalColor);
        speedLabel.setWrap(true);
        speedLabel.setAlignment(Align.center);
        infoTable.add(speedLabel).width(width);
        infoTable.row();
        if (version == 0) {
            Label armamentLabel = new Label(StringDB.getString("ARMAMENT"), skin, "normalFont", markColor);
            armamentLabel.setWrap(true);
            armamentLabel.setAlignment(Align.center);
            infoTable.add(armamentLabel).width(width).spaceTop(vpadding);
            infoTable.row();
        }

        fillWeaponStats(infoTable, skin, "normalFont", markColor, "normalFont", normalColor, version == 0);

        text = StringDB.getString("SHIELDS") + " " + StringDB.getString("AND") + " " + StringDB.getString("HULL");
        Label shLabel = new Label(text, skin, "normalFont", markColor);
        shLabel.setWrap(true);
        shLabel.setAlignment(Align.center);
        infoTable.add(shLabel).width(width);
        infoTable.row();
        text = "";
        text = StringDB.getString("TYPE") + " " + shield.getShieldType() + " " + StringDB.getString("SHIELDS") + ": "
                + StringDB.getString("CAPACITY") + " " + shield.getMaxShield();
        String material = StringDB.getString(ResourceTypes.fromResourceTypes(hull.getHullMaterial()).getName());
        text += "\n" + material + " "
                + (hull.isDoubleHull() ? StringDB.getString("DOUBLE_HULL_ARMOUR") : StringDB.getString("HULL_ARMOR")) + ": "
                + StringDB.getString("INTEGRITY") + " " + hull.getMaxHull();
        Label shInfoLabel = new Label(text, skin, "normalFont", normalColor);
        shInfoLabel.setWrap(true);
        shInfoLabel.setAlignment(Align.center);
        infoTable.add(shInfoLabel).width(width);
        infoTable.row();
        if (version > 0) {
            if (version == 1) {
                Label prodLabel = new Label(StringDB.getString("BUILDCOSTS"), skin, "normalFont", markColor);
                prodLabel.setWrap(true);
                prodLabel.setAlignment(Align.center);
                infoTable.add(prodLabel).width(width);
                infoTable.row();
                text = StringDB.getString("INDUSTRY") + ": " + getNeededIndustry() + " " + StringDB.getString("TITAN") + ": "
                        + getNeededTitan() + " " + StringDB.getString("DEUTERIUM") + ": " + getNeededDeuterium();
                text += "\n" + StringDB.getString("DURANIUM") + ": " + getNeededDuranium() + " " + StringDB.getString("CRYSTAL")
                        + ": " + getNeededCrystal() + " " + StringDB.getString("IRIDIUM") + ": " + getNeededIridium() + " "
                        + StringDB.getString("DERITIUM") + ": " + getNeededDeritium();
                Label prodInfoLabel = new Label(text, skin, "normalFont", normalColor);
                prodInfoLabel.setWrap(true);
                prodInfoLabel.setAlignment(Align.center);
                infoTable.add(prodInfoLabel).width(width);
                infoTable.row();
            }
            Label manLabel = new Label(StringDB.getString("MANEUVERABILITY"), skin, "normalFont", markColor);
            manLabel.setWrap(true);
            manLabel.setAlignment(Align.center);
            infoTable.add(manLabel).width(width);
            infoTable.row();
            text = getManeuverabilityAsString(maneuverabilty);
            Label manInfoLabel = new Label(text, skin, "normalFont", normalColor);
            manInfoLabel.setWrap(true);
            manInfoLabel.setAlignment(Align.center);
            infoTable.add(manInfoLabel).width(width);
            infoTable.row();
            Label sensorLabel = new Label(StringDB.getString("SENSORS"), skin, "normalFont", markColor);
            sensorLabel.setWrap(true);
            sensorLabel.setAlignment(Align.center);
            infoTable.add(sensorLabel).width(width);
            infoTable.row();
            text = StringDB.getString("SCANRANGE") + ": " + scanRange + " - " + StringDB.getString("SCANPOWER") + ": "
                    + scanPower;
            Label sensorInfoLabel = new Label(text, skin, "normalFont", normalColor);
            sensorInfoLabel.setWrap(true);
            sensorInfoLabel.setAlignment(Align.center);
            infoTable.add(sensorInfoLabel).width(width);
            infoTable.row();
            Label specLabel = new Label(StringDB.getString("SPECIAL_ABILITIES"), skin, "normalFont", markColor);
            specLabel.setWrap(true);
            specLabel.setAlignment(Align.center);
            infoTable.add(specLabel).width(width);
            infoTable.row();
            text = "";
            if (hasSpecial(ShipSpecial.ASSAULTSHIP))
                text += StringDB.getString("ASSAULTSHIP") + "\n";
            if (hasSpecial(ShipSpecial.BLOCKADESHIP))
                text += StringDB.getString("BLOCKADESHIP") + "\n";
            if (hasSpecial(ShipSpecial.COMMANDSHIP))
                text += StringDB.getString("COMMANDSHIP") + "\n";
            if (hasSpecial(ShipSpecial.DOGFIGHTER))
                text += StringDB.getString("DOGFIGHTER") + "\n";
            if (hasSpecial(ShipSpecial.DOGKILLER))
                text += StringDB.getString("DOGKILLER") + "\n";
            if (hasSpecial(ShipSpecial.PATROLSHIP))
                text += StringDB.getString("PATROLSHIP") + "\n";
            if (hasSpecial(ShipSpecial.RAIDER))
                text += StringDB.getString("RAIDER") + "\n";
            if (hasSpecial(ShipSpecial.SCIENCEVESSEL))
                text += StringDB.getString("SCIENCESHIP") + "\n";
            if (shield.isRegenerative())
                text += StringDB.getString("REGENERATIVE_SHIELDS") + "\n";
            if (hull.isAblative())
                text += StringDB.getString("ABLATIVE_ARMOR") + "\n";
            if (hull.isPolarisation())
                text += StringDB.getString("HULLPOLARISATION") + "\n";
            if (canCloak())
                text += StringDB.getString("CAN_CLOAK") + "\n";
            text = text.trim();
            if (text.isEmpty())
                text = StringDB.getString("NONE");
            Label specInfoLabel = new Label(text, skin, "normalFont", normalColor);
            specInfoLabel.setWrap(true);
            specInfoLabel.setAlignment(Align.center);
            infoTable.add(specInfoLabel).width(width);
        }
    }

    public static String getManeuverabilityAsString(int value) {
        String text;
        switch (value) {
            case 9:
                text = StringDB.getString("PHENOMENAL");
                break;
            case 8:
                text = StringDB.getString("EXCELLENT");
                break;
            case 7:
                text = StringDB.getString("VERYGOOD");
                break;
            case 6:
                text = StringDB.getString("GOOD");
                break;
            case 5:
                text = StringDB.getString("NORMAL");
                break;
            case 4:
                text = StringDB.getString("ADEQUATE");
                break;
            case 3:
                text = StringDB.getString("BAD");
                break;
            case 2:
                text = StringDB.getString("VERYBAD");
                break;
            case 1:
                text = StringDB.getString("MISERABLE");
                break;
            default:
                text = StringDB.getString("NONE");
                break;
        }
        return text;
    }

    public int getNeededTechLevel(ResearchType type) {
        switch (type) {
            case BIO:
                return bioTech;
            case ENERGY:
                return energyTech;
            case COMPUTER:
                return compTech;
            case PROPULSION:
                return propulsionTech;
            case CONSTRUCTION:
                return constructionTech;
            case WEAPON:
                return weaponTech;
            default:
                return -1;
        }
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeInt(race);
        output.writeInt(bioTech);
        output.writeInt(energyTech);
        output.writeInt(compTech);
        output.writeInt(propulsionTech);
        output.writeInt(constructionTech);
        output.writeInt(weaponTech);
        output.writeInt(neededIndustry);
        output.writeInt(neededTitan);
        output.writeInt(neededDeuterium);
        output.writeInt(neededDuranium);
        output.writeInt(neededCrystal);
        output.writeInt(neededIridium);
        output.writeInt(neededDeritium);
        output.writeInt(baseIndustry);
        output.writeInt(baseTitan);
        output.writeInt(baseDeuterium);
        output.writeInt(baseDuranium);
        output.writeInt(baseCrystal);
        output.writeInt(baseIridium);
        output.writeInt(baseDeritium);
        output.writeString(onlyInSystem);
        output.writeInt(obsoletesClassId);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        race = input.readInt();
        bioTech = input.readInt();
        energyTech = input.readInt();
        compTech = input.readInt();
        propulsionTech = input.readInt();
        constructionTech = input.readInt();
        weaponTech = input.readInt();
        neededIndustry = input.readInt();
        neededTitan = input.readInt();
        neededDeuterium = input.readInt();
        neededDuranium = input.readInt();
        neededCrystal = input.readInt();
        neededIridium = input.readInt();
        neededDeritium = input.readInt();
        baseIndustry = input.readInt();
        baseTitan = input.readInt();
        baseDeuterium = input.readInt();
        baseDuranium = input.readInt();
        baseCrystal = input.readInt();
        baseIridium = input.readInt();
        baseDeritium = input.readInt();
        onlyInSystem = input.readString();
        if (((KryoV) kryo).getSaveVersion() < 7)
            obsoletesClass = input.readString();
        else
            obsoletesClassId = input.readInt();
    }
}
