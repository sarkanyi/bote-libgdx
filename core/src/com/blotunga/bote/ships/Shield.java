/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

/**
 * @author dragon
 * @version 1
 */
public class Shield {
    private int shieldType;
    private int maxShield;
    private int currentShield;
    private boolean regenerative;

    public Shield() {
        shieldType = -1;
        maxShield = 0;
        currentShield = 0;
        regenerative = false;
    }

    public Shield(Shield oldShield) {
        shieldType = oldShield.shieldType;
        maxShield = oldShield.maxShield;
        currentShield = oldShield.currentShield;
        regenerative = oldShield.regenerative;
    }

    public int getShieldType() {
        return shieldType;
    }

    public int getMaxShield() {
        return maxShield;
    }

    public int getCurrentShield() {
        return currentShield;
    }

    public boolean isRegenerative() {
        return regenerative;
    }

    public void setCurrentShield(int newCurrentShield) {
        if (newCurrentShield < 0)
            currentShield = 0;
        else if (newCurrentShield > currentShield)
            currentShield = 0;
        else
            currentShield = newCurrentShield;
    }

    public void modifyShield(int maxShield, int shieldType, boolean regenerative) {
        this.shieldType = shieldType;
        this.maxShield = maxShield;
        this.currentShield = maxShield;
        this.regenerative = regenerative;
    }

    public void rechargeShields() {
        rechargeShields(1);
    }

    /**
     * This function recharges the shields in accordance with their tpye.
     * @param multi
     */
    public void rechargeShields(int multi) {
        int maxRecharge = maxShield - currentShield;
        int recharge = multi * (int) (Math.max(maxShield * 0.001f, 1) * shieldType);
        if (recharge > maxRecharge)
            recharge = maxRecharge;
        currentShield += recharge;
    }
}
