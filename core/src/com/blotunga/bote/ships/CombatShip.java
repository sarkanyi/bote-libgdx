/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.CombatShipEffectSound.CombatShipEffectHandler;
import com.blotunga.bote.ships.CombatShipEffectSound.EffectType;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;
import com.blotunga.bote.utils.Vec3i;

public class CombatShip {
    class ShootTime {
        IntArray phaser; ///< says when we can fire a phaser again
        IntArray torpedo; ///< says when we can fire a torpedo again
        boolean phaserIsShooting;

        public ShootTime() {
            phaser = new IntArray();
            torpedo = new IntArray();
            phaserIsShooting = false;
        }
    }

    Ships ship; ///< the ship which is currently in combat
    Vec3i coord; ///< coordinate of the ship in combat
    Array<Vec3i> route; ///< route of the ship
    ShootTime fire; ///< time until the ship can fire again
    private boolean pulseFire; ///< were pulse beams fired (for combat sim)
    int maneuverability;
    int modifier; ///< defense bonus/malus
    CombatShip target;
    private boolean regShieldStatus; ///< are the regenerative shields synched or not
    int cloak; ///< is the ship cloaked? After firing the ship has 50-70 ticks until it can be attacked. if it's higher than 0 it is cloaked
    int reCloak; ///< if the counter reached 255, the ship can be cloaked again
    boolean shootCloaked; ///< had the ship fired while cloaked?
    ///effects of anomalies
    boolean canUseShields;
    boolean canUseTorpedoes;
    boolean fasterShieldRecharge;
    int retreatCounter;///< in case of retreat this has to be counted down
    Ships killedByShip; ///< the ship which destroyed this (null if it wasn't destroyed)
    Table sprite;
    CombatShipEffectHandler effectCB;

    public CombatShip(Ships ship) {
        this.ship = ship;
        init();
    }

    public CombatShip(CombatShip other) {
        ship = new Ships(other.ship);
        route = new Array<Vec3i>();
        for (Vec3i r : other.route)
            route.add(new Vec3i(r));
        target = other.target;
        modifier = other.modifier;
        fire = new ShootTime();
        for (int i = 0; i < ship.getBeamWeapons().size; i++)
            fire.phaser.add(0);
        for (int i = 0; i < ship.getTorpedoWeapons().size; i++)
            fire.torpedo.add(0);
        regShieldStatus = other.regShieldStatus;
        cloak = other.cloak;
        reCloak = other.reCloak;
        shootCloaked = other.shootCloaked;
        retreatCounter = other.retreatCounter;
        canUseShields = other.canUseShields;
        canUseTorpedoes = other.canUseTorpedoes;
        fasterShieldRecharge = other.fasterShieldRecharge;
        sprite = other.sprite;
        effectCB = other.effectCB;
        coord = new Vec3i(other.coord);
        maneuverability = other.maneuverability;
    }

    private void init() {
        coord = new Vec3i();
        route = new Array<Vec3i>();
        target = null;
        modifier = 100;
        fire = new ShootTime();
        for (int i = 0; i < ship.getBeamWeapons().size; i++)
            fire.phaser.add(0);
        for (int i = 0; i < ship.getTorpedoWeapons().size; i++)
            fire.torpedo.add(0);
        regShieldStatus = false;
        cloak = 0;
        reCloak = 0;
        shootCloaked = false;
        retreatCounter = (int) (RandUtil.random() * 50) + 100;

        canUseShields = true;
        canUseTorpedoes = true;
        fasterShieldRecharge = false;
    }

    /**
     * This sets the maneuverability of the ship and has to be called right after the constructor
     * @param man
     */
    public void setManeuverability(int man) {
        maneuverability = man;
    }

    /**
     * This function returns a possible bonus caused by the difference in maneuverability
     * @param att - maneuverability of the attacker
     * @param def - maneuverability of the defender
     * @return
     */
    public static int getToHitBonus(int att, int def) {
        int bonus[][] = {
                { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 5,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 10, 5,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 15, 10, 5,  0,  0,  0,  0,  0,  0,  0 },
                { 20, 15, 10, 5,  0,  0,  0,  0,  0,  0 },
                { 25, 20, 15, 10, 5,  0,  0,  0,  0,  0 },
                { 30, 25, 20, 15, 10, 5,  0,  0,  0,  0 },
                { 35, 30, 25, 20, 15, 10, 5,  0,  0,  0 },
                { 40, 35, 30, 25, 20, 15, 10, 5,  0,  0 },
                { 45, 40, 35, 30, 25, 20, 15, 10, 5,  0 }
        };

        return bonus[att][def];
    }

    /**
     * This function returns a possible malus caused by the difference in maneuverability
     * @param att - maneuverability of the attacker
     * @param def - maneuverability of the defender
     * @return
     */
    public static int getToHitMalus(int att, int def) {
        int malus[][] = {
                { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 4,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 10, 4,  0,  0,  0,  0,  0,  0,  0,  0 },
                { 18, 10, 4,  0,  0,  0,  0,  0,  0,  0 },
                { 28, 18, 10, 4,  0,  0,  0,  0,  0,  0 },
                { 40, 28, 18, 10, 4,  0,  0,  0,  0,  0 },
                { 54, 40, 28, 18, 10, 4,  0,  0,  0,  0 },
                { 70, 54, 40, 28, 18, 10, 4,  0,  0,  0 },
                { 88, 70, 54, 40, 28, 18, 10, 4,  0,  0 },
                { 96, 88, 70, 54, 40, 28, 18, 10, 4,  0 }
        };

        return malus[att][def];
    }

    /**
     * This function sets the existing regenerative shields to the enemy's weapon's frequency
     */
    public void actRegShield() {
        if (ship.getShield().isRegenerative())
            regShieldStatus = true;
    }

    /**
     * This function calculates the next point on the way to the target coordinate of the ship
     */
    public void calculateNextPosition() {
        for (int i = 0; i < fire.phaser.size; i++)
            if (fire.phaser.get(i) > 0)
                fire.phaser.incr(i, -1);
        for (int i = 0; i < fire.torpedo.size; i++)
            if (fire.torpedo.get(i) > 0)
                fire.torpedo.incr(i, -1);

        if (cloak == 0 && reCloak < 255 && ship.isCloakOn())
            reCloak++;

        if (shootCloaked && cloak > 0)
            cloak--;

        if (maneuverability == 0)
            return;

        //if the route wasn't yet calculated
        if (route.size == 0) {

            if (ship.getCombatTactics() == CombatTactics.CT_ATTACK) {
                if (target != null) {
                    calcRoute(target.coord.cpy(), 10);
                }
                return;
            }
            if (ship.getCombatTactics() == CombatTactics.CT_AVOID) {
                calcRoute(coord.cpy(), 100);//random position
                return;
            }
            if (ship.getCombatTactics() == CombatTactics.CT_RETREAT) {
                if (retreatCounter > 0) {
                    retreatCounter--;
                    if (retreatCounter == 0) {
                        Vec3i b = new Vec3i();
                        b.x = (int) (RandUtil.random() * 30);
                        b.y = (int) (RandUtil.random() * 30);
                        b.z = (int) (RandUtil.random() * 30);
                        calcRoute(b, 1);
                    }
                }
            }
        }
    }

    private void calcRoute(Vec3i targetCoord, int minDistance) {
        Vec3i a = coord.cpy();
        Vec3i b = targetCoord; //safe because we have a copy of the target already

        int distance = (int) a.dst(b);

        //in close range we still have a new random position so that the ships don't just stand around
        if (distance < minDistance) {
            int multi = (int) (RandUtil.random() * 2);
            if (multi == 0)
                multi = -1;
            b.x = (int) (RandUtil.random() * 200) * multi;
            b.y = (int) (RandUtil.random() * 200) * multi;
            b.z = (int) (RandUtil.random() * 200) * multi;
            distance = (int) a.dst(b);
        }

        int speedUp = 0;//the acceleration of the ship (in case of a maneuverability from 5 we start with 1 and then we accelerate to 5)
        int speed = this.maneuverability;
        while (distance >= 1) {
            int speedUpWay = 0;//needed to stop - length of braking
            for (int i = speedUp; i > 0; i--)
                speedUpWay += i;
            if (distance <= speedUpWay && speedUp > 0) //we have to brake
                speedUp--;
            else if (speedUp < speed) //we can accelerate
                speedUp++;
            //if our ship could go further than the distance, then we have to reduce speed
            if (speedUp > distance)
                speedUp = Math.round(distance);

            float multi = (float) speedUp / (float) distance;
            Vec3i temp = new Vec3i(b);
            temp.sub(a);
            temp.x = (int) Math.floor((temp.x * multi + 0.5f));
            temp.y = (int) Math.floor((temp.y * multi + 0.5f));
            temp.z = (int) Math.floor((temp.z * multi + 0.5f));

            a.add(temp);
            route.add(a.cpy());
            distance = (int) a.dst(b);
        }
    }

    /**
     *This function sets the ship on the next point in the route
     */
    public void gotoNextPosition() {
        if (route.size != 0) {
            coord = route.first();
            route.removeIndex(0);
        }
    }

    /**
     * This function executes a beam attack on the target
     * @param beamStart - holds the beam from which we start calculating
     * @return - x - the number of the beam in the array, y - the number of beams in bank if a ship was destroyed, -1, -1 else
     */
    public IntPoint attackEnemyWithBeam(IntPoint beamStart) {
        if (target != null) {
            fire.phaserIsShooting = false;
            //only if the distance is higher than 1 can we shoot, else we will ram
            float distance = coord.dst(target.coord);
            if (distance >= 1) {
                int bonus = getAccBonusFromSpecials();
                int beamWeapon = beamStart.x;
                int beamRay = beamStart.y;
                for (int i = beamWeapon; i < ship.getBeamWeapons().size; i++) {
                    if (beamRay >= ship.getBeamWeapons().get(i).getBeamNumber()) {
                        beamRay = 0;
                        continue;
                    }

                    if (fire.phaser.get(i) > ship.getBeamWeapons().get(i).getRechargeTime() || fire.phaser.get(i) == 0) {
                        BeamWeapons bw = ship.getBeamWeapons().get(i);
                        //range = type(mk)*5 + bonus + 50 (to have it between 60-160)
                        if (distance <= (bw.getBeamType() * 5 + bw.getBonus() + 50)) {
                            if (allowFire(bw.getFireArc())) {
                                //the counter is set to recharge time + length - if hte counter is higher than rechargetime it will shoot.
                                //afterwards we wait until the recharge time is 0
                                if (fire.phaser.get(i) == 0)
                                    fire.phaser.set(i, bw.getRechargeTime() + bw.getBeamLength());
                                //here we calculate the damage for each beam
                                while (beamRay++ < bw.getBeamNumber()) {
                                    fire.phaserIsShooting = false;

                                    pulseFire = false;
                                    if (bw.getShootNumber() > 1)
                                        pulseFire = true;

                                    fireBeam(i, distance, bonus);

                                    if (!target.ship.isAlive())
                                        return new IntPoint(i, beamRay);
                                }
                                //the next phasor can shoot again with the first beam
                                beamRay = 0;
                            }
                        }
                    }
                }
            }
        }
        return new IntPoint();
    }

    public void attackEnemyWithTorpedo(Array<Torpedo> ct) {
        if (target != null) {
            int distance = (int) coord.dst(target.coord);
            //if the ship is cloaked then it will fire if it is at around 2/3 of the maximum distance
            if (!shootCloaked && distance > GameConstants.MAX_TORPEDO_RANGE / 1.5)
                return;

            if (distance >= 3) {
                int bonus = getAccBonusFromSpecials();

                //iterate trough tubes
                boolean firedTorpedo = false;
                for (int i = 0; i < ship.getTorpedoWeapons().size; i++) {
                    if (fire.torpedo.get(i) == 0) {
                        //calculate the target of the torpedo. for this look at the route of the target ship and calculate how long it would take to impact
                        Vec3i targetCoord = target.coord.cpy();
                        if (distance > GameConstants.TORPEDO_SPEED) {
                            int oldDist = distance;
                            //on which route entry of the ship do we target?
                            int targetRoute = 0;
                            do {
                                if (target.route.size > targetRoute) {
                                    distance = (int) coord.dst(target.route.get(targetRoute));
                                    targetCoord = target.route.get(targetRoute).cpy();
                                    targetRoute++;
                                } else
                                    break;
                            } while ((distance / GameConstants.TORPEDO_SPEED) >= targetRoute);
                            distance = oldDist;

                            //if the distance is smaller than MAX_TORPEDO_RANGE, the torpedoes will be fired
                            if (distance <= GameConstants.MAX_TORPEDO_RANGE) {
                                if (allowFire(ship.getTorpedoWeapons().get(i).getFireArc())) {
                                    int n = 0;
                                    while (n++ < ship.getTorpedoWeapons().get(i).getNumberOfTubes()) {
                                        fireTorpedo(ct, i, targetCoord, bonus);
                                        firedTorpedo = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (firedTorpedo && effectCB != null)
                    effectCB.playEffect(EffectType.EFFECT_PHOTON);
            }
        }
    }

    /**
     * @return - calculates the bonus on attack which ships get due to their special abilities
     */
    private int getAccBonusFromSpecials() {
        int bonus = 0;

        //cloak gives a 20% accuracy and damages bonus
        if (cloak > 0)
            bonus += 20;
        //dogfighter gets 30% bonus for small targets
        if (ship.hasSpecial(ShipSpecial.DOGFIGHTER) && target.ship.getSize() == ShipSize.SMALL)
            bonus += 30;
        //anti-dogfighters get 30% against dogfighters
        if (ship.hasSpecial(ShipSpecial.DOGKILLER) && target.ship.hasSpecial(ShipSpecial.DOGFIGHTER))
            bonus += 30;

        return bonus;
    }

    /**
     * The function calculates if a weapon system can fire based on its position on the ship and the position of the ship itself
     * @param arc
     * @return
     */
    private boolean allowFire(FireArc arc) {
        if (ship.isStation())
            return true;
        int dirarc_v = arc.getPosition();
        int firearc_v = arc.getAngle();
        boolean allow = false;
        Vector3 v = new Vector3();

        if (route.size != 0) {
            Vec3i p = route.get(0).cpy().sub(coord);
            v.set(p.x, p.y, p.z);
        } else { //point to 0, 0, 0
            v.set(-coord.x, -coord.y, -coord.z);
        }
        v = v.scl(1 / v.len());

        //vector between ship and target
        Vector3 diff = new Vector3();
        if (target != null) {
            diff = target.coord.cpy().sub(coord).toVector3();
            diff = diff.scl(1 / diff.len());
        }

        int diffarc = 0;
        Vector3 newZ = v.cpy().crs(diff);//cross product
        if (newZ.x < 1 && newZ.x > -1
        		&& newZ.y < 1 && newZ.y > -1
        		&& newZ.x < 1 && newZ.z > -1) {
            if (v.cpy().dot(diff) >= 0)
                diffarc = 0;
            else
                diffarc = 180;
        } else {
            //Rules to avoid positive axis and that we fire from the same side every time
            Vector3 g_x = new Vector3();
            Vector3 g_z = new Vector3();
            Vector3 g_y = new Vector3(diff.cpy().scl(1 / diff.len())); //g_y stays constant
            Vector3 gYTemp = new Vector3(g_y.cpy().scl(v.cpy().dot(g_y)));

            boolean aOrB = false; //temp variable to decide which g_x and g_z to use
            if (diff.y <= -1) {
                if (diff.cpy().dot(v) <= -1)
                    aOrB = true;
            } else if (diff.y >= 1) {
                if (diff.cpy().dot(v) > -1)
                    aOrB = true;
            } else if (diff.x <= -1) {
                if (diff.cpy().dot(v) <= -1)
                    aOrB = true;
            } else if (diff.x >= 1) {
                if (diff.cpy().dot(v) > -1)
                    aOrB = true;
            } else if (diff.z <= -1) {
                if (diff.cpy().dot(v) <= -1)
                    aOrB = true;
            } else if (diff.z >= 1) {
                if (diff.cpy().dot(v) > -1)
                    aOrB = true;
            }//else is already false

            if (aOrB) {
                g_x = gYTemp.cpy().sub(v).scl(1 / gYTemp.cpy().sub(v).len());
                g_z = newZ.cpy().scl(-1).scl(1 / newZ.len());
            } else {
                g_x = v.cpy().sub(gYTemp).scl(1 / v.cpy().sub(gYTemp).len());
                g_z = newZ.cpy().scl(1 / newZ.len());
            }

            Vector3 V = new Vector3(g_x.x * v.x + g_x.y * v.y + g_x.z * v.z, g_y.x * v.x + g_y.y * v.y + g_y.z * v.z,
                    g_z.x * v.x + g_z.y * v.y + g_z.z * v.z);
            Vector3 DIFF = new Vector3(g_x.x * diff.x + g_x.y * diff.y + g_x.z * diff.z, g_y.x * diff.x + g_y.y
                    * diff.y + g_y.z * diff.z, g_z.x * diff.x + g_z.y * diff.y + g_z.z * diff.z);
            Vector3 diffv_arcdiff = new Vector3(DIFF.cpy().dot(V) / V.len(), DIFF.y * V.x / V.len(), 0);
            float diffarc_length;
            if (diffv_arcdiff.y < 0)
                diffarc_length = (float) (Math.PI * 2 - Math.acos(diffv_arcdiff.x / diffv_arcdiff.len()));
            else
                diffarc_length = (float) (Math.acos(diffv_arcdiff.x / diffv_arcdiff.len()));
            diffarc = (int) (diffarc_length * 180 / Math.PI);
        }

        if ((dirarc_v - firearc_v / 2 < diffarc - 360)
                || ((dirarc_v - firearc_v / 2 < diffarc) && (diffarc < dirarc_v + firearc_v / 2))
                || (diffarc + 360 < dirarc_v + firearc_v / 2))
            allow = true;

        return allow;
    }

    private void fireBeam(int beamWeapon, float distance, int bonus) {
        int beamDamage = 0;
        int toHull = 0;
        BeamWeapons bw = ship.getBeamWeapons().get(beamWeapon);

        //the number of beams/shot are taken into account - usually one, but canon and pulse weapons can have more
        for (int t = 0; t < bw.getShootNumber(); t++) {
            int acc = (int) (60 - distance * 0.1 + (getCrewExperienceModifier() - target.getCrewExperienceModifier())
                    * 0.1 + (getToHitBonus(ship.getManeuverabilty(), target.ship.getManeuverabilty()) - getToHitMalus(
                    	ship.getManeuverabilty(), target.ship.getManeuverabilty())) * 0.2 + bw.getBonus() + bw.getBeamType());

            if (target.ship.getSize() == ShipSize.SMALL)
                acc = (int) (acc * 0.66);
            else if (target.ship.getSize() == ShipSize.BIG)
                acc = (int) (acc * 1.33);
            else if (target.ship.getSize() == ShipSize.HUGE)
                acc = (int) (acc * 1.66);

            //did we damage?
            if ((acc + bonus) > RandUtil.random() * 100)
                beamDamage += ((bw.getBeamPower() * (modifier + bonus)) / target.modifier);
        }

        if (beamDamage > 0)
            shootCloaked = true;

        //if the hull is not ablative, 10% will go to the hull
        if (!target.ship.getHull().isAblative())
            toHull = (int) (beamDamage * GameConstants.DAMAGE_TO_HULL);

        //if the beam is piercing and their shields are not set to it, we get a full hit on the hull
        if (bw.isPiercing() && !target.getActRegShields())
            toHull = beamDamage;
        else if (bw.isModulating())//if it's a modulating weapon then 50% still hits
            toHull = (int) (beamDamage * 0.5);
        target.ship.getHull().setCurrentHull(-toHull);

        beamDamage -= toHull;
        //the rest goes first on the shields
        if (target.ship.getShield().getCurrentShield() - beamDamage >= 0 && canUseShields)
            target.ship.getShield().setCurrentShield(target.ship.getShield().getCurrentShield() - beamDamage);
        else {
            target.ship.getHull().setCurrentHull(target.ship.getShield().getCurrentShield() - beamDamage);
            target.ship.getShield().setCurrentShield(0);
        }

        if (bw.isPiercing() && target.ship.getShield().isRegenerative())
            target.actRegShield();
        fire.phaserIsShooting = true;

        if (!target.ship.isAlive())
            target.killedByShip = ship;
    }

    int getCrewExperienceModifier() {
        return ship.getExpLevel() * 20;
    }

    public boolean getActRegShields() {
        return regShieldStatus;
    }

    public void startShootingSound() {
        if (effectCB != null)
            effectCB.playEffect(isPulsing() ? EffectType.EFFECT_PHASER_PULSING : EffectType.EFFECT_PHASER);
    }

    private void fireTorpedo(Array<Torpedo> ct, int torpedoWeapon, Vec3i targetCoord, int bonus) {
        shootCloaked = true;
        TorpedoWeapons tw = ship.getTorpedoWeapons().get(torpedoWeapon);
        fire.torpedo.set(torpedoWeapon, tw.getTubeFirerate());
        Color color = Color.WHITE;
        Race owner = ship.getOwner();
        if (owner.isMajor())
            color = ((Major) owner).getRaceDesign().clrGalaxySectorText;
        Torpedo torpedo = new Torpedo(coord, targetCoord, color);
        torpedo.number = tw.getNumber();
        torpedo.power = tw.getTorpedoPower() * (modifier + bonus) / 100;
        torpedo.type = tw.getTorpedoType();
        torpedo.shipFiredTorpedo = ship;
        torpedo.maneuverability = maneuverability;
        torpedo.modifier = getCrewExperienceModifier() + tw.getAccuracy() + bonus;
        ct.add(torpedo);
    }

    public void loadSprite(ResourceManager manager) {
        Image img = new Image(manager.loadTextureImmediate("graphics/ships/" + manager.getShipImgName(ship.ID - 10000) + ".png"));
        img.setName("GFX");
        sprite = new Table();
        Color color = Color.WHITE;
        Race owner = ship.getOwner();
        if (owner.isMajor())
            color = ((Major) owner).getRaceDesign().clrGalaxySectorText;
        Label l = new Label(ship.getShipClass(), manager.getSkin(), "mediumFont", color);
        l.setAlignment(Align.center);
        sprite.add(l);
        sprite.row();
        l = new Label("HP", manager.getSkin(), "mediumFont", color);
        l.setAlignment(Align.center);
        l.setName("HITPOINTS");
        sprite.add(l);
        sprite.row();
        sprite.add(img).width(Gdx.graphics.getWidth() / 20).height(Gdx.graphics.getWidth() / 20);
        sprite.row();
    }

    public Table getSprite() {
        return sprite;
    }

    public Vector3 getCoord() {
        Vector3 cf = new Vector3(coord.x, coord.y, coord.z);
        cf.scl(Gdx.graphics.getHeight()/450f);
        return cf;
    }

    public CombatShip getTarget() {
        return target;
    }

    public boolean isFiring() {
        return fire.phaserIsShooting;
    }

    public boolean isPulsing() {
        return pulseFire;
    }

    public Ships getShip() {
        return ship;
    }

    public boolean isCloaked() {
        return cloak > 0;
    }

    public void setEffectsCB(CombatShipEffectHandler cb) {
        effectCB = cb;
    }
}
