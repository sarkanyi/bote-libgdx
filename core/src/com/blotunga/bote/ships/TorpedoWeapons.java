/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;

/**
 * @author dragon
 * @version 1.1
 */
public class TorpedoWeapons {
    private int torpedoType;			// type of torpedo
    private int number;					// number of torpedoes fired per second or turn
    private int tubeFirerate;			// at which rate per second can we fire "number" of torpedoes
    private int numberOfTubes;			// number of tubes
    private int accuracy;				// accuracy of the torpedo
    private String tubeName;			// name of the weapons tube
    private boolean onlyMicroPhoton;	// can the weapon fire only micro photon torpedoes?
    private FireArc fireArc;			// firing angle and mount point of the weapon

    public TorpedoWeapons() {
        torpedoType = 0;
        number = 0;
        tubeFirerate = 0;
        numberOfTubes = 0;
        accuracy = 0;
        tubeName = "";
        onlyMicroPhoton = false;
        fireArc = new FireArc();
    }

    public TorpedoWeapons(TorpedoWeapons oldTorpedo) {
        torpedoType = oldTorpedo.torpedoType;
        number = oldTorpedo.number;
        tubeFirerate = oldTorpedo.tubeFirerate;
        numberOfTubes = oldTorpedo.numberOfTubes;
        accuracy = oldTorpedo.accuracy;
        tubeName = oldTorpedo.tubeName;
        onlyMicroPhoton = oldTorpedo.onlyMicroPhoton;
        fireArc = new FireArc(oldTorpedo.getFireArc().getPosition(), oldTorpedo.getFireArc().getAngle());
    }

    public String getTubeName() {
        return tubeName;
    }

    public int getNumber() {
        return number;
    }

    public int getTubeFirerate() {
        return tubeFirerate;
    }

    public int getNumberOfTubes() {
        return numberOfTubes;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public boolean isOnlyMicroPhoton() {
        return onlyMicroPhoton;
    }

    public FireArc getFireArc() {
        return fireArc;
    }

    public int getTorpedoType() {
        return torpedoType;
    }

    public String getTorpedoName() {
        return TorpedoInfo.getName(torpedoType);
    }

    public int getTorpedoPower() {
        return TorpedoInfo.getPower(torpedoType);
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public void setTubeFirerate(int rate) {
        tubeFirerate = rate;
    }

    public void modifyTubeName(String tubeName) {
        this.tubeName = tubeName;
    }

    public void modifyTorpedoWeapon(int torpedoType, int number, int tubeFirerate, int numberOfTubes, String tubeName,
            boolean onlyMicroPhoton, int accuracy) {
        this.torpedoType = torpedoType;
        this.number = number;
        this.tubeFirerate = tubeFirerate;
        this.numberOfTubes = numberOfTubes;
        this.tubeName = tubeName;
        this.onlyMicroPhoton = onlyMicroPhoton;
        this.accuracy = accuracy;
    }

    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        table.clearChildren();
        String text = getTubeName();
        Label l = new Label(text, skin, headerFont, headerColor);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(300));
        table.row();

        text = String.format("%s (%d)", TorpedoInfo.getName(torpedoType), TorpedoInfo.getPower(torpedoType));
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        text = StringDB.getString("TOTAL_TORPEDO_DAMAGE")+ ": " + (getTorpedoPower() * getNumber() * 100 * getNumberOfTubes()) / getTubeFirerate();
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("ACCURACY") + ": " + accuracy + "%";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("TORPEDO_ORIENTATION") + ": " + getFireArc().getPosition() + "\u00b0";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("TORPEDO_ANGLE") + ": " + getFireArc().getAngle() + "\u00b0";
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        Array<String> specials = TorpedoInfo.getSpecialString(torpedoType);
        for (String s : specials) {
            l = new Label(s, skin, textFont, textColor);
            table.add(l);
            table.row();
        }
    }
}
