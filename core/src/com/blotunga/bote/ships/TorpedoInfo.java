/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.general.StringDB;

/**
 * @author dragon
 * @version 1.1
 */
/**
 * @author dragon
 *
 */
public class TorpedoInfo {
    public enum TorpedoSpecials {
        NO_SPECIAL(0, "NONE"),
        MICROTORPEDO(1, "MICROTORPEDO"),		// microtorpedoes (only for micro tubes)
        PENETRATING(2, "PENETRATING"),			// penetrates shields
        DOUBLESHIELDDMG(4, "DOUBLESHIELDDMG"),		// does double damage to shields
        DOUBLEHULLDMG(8, "DOUBLEHULLDMG"),		// does double damage to hull
        IGNOREALLSHIELDS(16, "IGNOREALLSHIELDS"),	// ignores all types of shields
        COLLAPSESHIELDS(32, "COLLAPSESHIELDS"),	// hit has a chance of collapsing shields
        REDUCEMANEUVER(64, "REDUCEMANEUVER");		// hit has a chance to reduce maneuverability

        int special;
        String dbName;

        TorpedoSpecials(int special, String dbName) {
            this.special = special;
            this.dbName = dbName;
        }

        public int getSpecial() {
            return special;
        }

        public String getDBName() {
            return dbName;
        }
    }

    public class TorpedoStats {
        String name;
        int dmg;
        int specials;

        public TorpedoStats(String name, int dmg) {
            this(name, dmg, TorpedoSpecials.NO_SPECIAL.getSpecial());
        }

        public TorpedoStats(String name, int dmg, int specials) {
            this.name = name;
            this.dmg = dmg;
            this.specials = specials;
        }

        @Override
        public String toString() {
            return name + " - " + dmg;
        }
    }

    private Array<TorpedoStats> stats;
    final private static TorpedoInfo instance = new TorpedoInfo();

    private static TorpedoInfo getInstance() {
        return instance;
    }

    private TorpedoInfo() {
        stats = new Array<TorpedoStats>(false, 29);
        //a) nuclear torpedos
        stats.add(new TorpedoStats("Fusionstorpedo", 44));
        stats.add(new TorpedoStats("Nucleartorpedo", 75));
        //b) photon torpedos
        stats.add(new TorpedoStats("Prototorpedo", 150));
        stats.add(new TorpedoStats("Photontorpedo", 250));
        stats.add(new TorpedoStats("Microphotontorpedo", 5, TorpedoSpecials.MICROTORPEDO.getSpecial() | TorpedoSpecials.PENETRATING.getSpecial()));
        //c) plasma torpedos
        stats.add(new TorpedoStats("Plasmatorpedo", 300));
        stats.add(new TorpedoStats("Plasmatorpedo I", 400));
        stats.add(new TorpedoStats("Plasmatorpedo II", 600));
        stats.add(new TorpedoStats("Plasmatorpedo III", 800));
        stats.add(new TorpedoStats("Microplasmatorpedo", 6, TorpedoSpecials.MICROTORPEDO.getSpecial() | TorpedoSpecials.PENETRATING.getSpecial()));
        //d) ion torpedos
        stats.add(new TorpedoStats("Iontorpedo", 225));
        stats.add(new TorpedoStats("Iontorpedo II", 450));
        //e) quantum torpedos
        stats.add(new TorpedoStats("Quantumtorpedo", 575));
        stats.add(new TorpedoStats("Myonictorpedo", 646));
        //f) mytronic torpedos
        stats.add(new TorpedoStats("Mytronictorpedo I", 200));
        stats.add(new TorpedoStats("Mytronictorpedo II", 500));
        stats.add(new TorpedoStats("Micromytrontic", 4, TorpedoSpecials.MICROTORPEDO.getSpecial() | TorpedoSpecials.PENETRATING.getSpecial()));
        //g) polaron torpedos
        stats.add(new TorpedoStats("Polarontorpedo", 288, TorpedoSpecials.PENETRATING.getSpecial()));
        //h) gravimetric torpedos
        stats.add(new TorpedoStats("Gravimetrictorpedo", 600, TorpedoSpecials.PENETRATING.getSpecial()));
        //i) trilithium torpedoes
        stats.add(new TorpedoStats("Trilithiumtorpedo", 1000, TorpedoSpecials.DOUBLESHIELDDMG.getSpecial()));
        //j) tricobalt torpedos
        stats.add(new TorpedoStats("Tricobalttorpedo", 900, TorpedoSpecials.PENETRATING.getSpecial()));
        //k) ultritium torpedos
        stats.add(new TorpedoStats("Ultritiumtorpedo", 800, TorpedoSpecials.COLLAPSESHIELDS.getSpecial()));
        //l) positron torpedos
        stats.add(new TorpedoStats("Positrontorpedo", 400, TorpedoSpecials.REDUCEMANEUVER.getSpecial()));
        //m) chroniton torpedos
        stats.add(new TorpedoStats("Chronitontorpedo", 300, TorpedoSpecials.PENETRATING.getSpecial() | TorpedoSpecials.DOUBLEHULLDMG.getSpecial()));
        //n) transphasic torpedos
        stats.add(new TorpedoStats("Transquantumtorpedo", 500, TorpedoSpecials.IGNOREALLSHIELDS.getSpecial()));
        //d)++ ion torpedo
        stats.add(new TorpedoStats("Microiontorpedo", 8, TorpedoSpecials.MICROTORPEDO.getSpecial() | TorpedoSpecials.PENETRATING.getSpecial()));
        //o) energy dampening weapons
        stats.add(new TorpedoStats("Energy Dissipator 1", 50, TorpedoSpecials.REDUCEMANEUVER.getSpecial()));
        stats.add(new TorpedoStats("Energy Dissipator 2", 100, TorpedoSpecials.REDUCEMANEUVER.getSpecial() | TorpedoSpecials.PENETRATING.getSpecial()));
        stats.add(new TorpedoStats("Energy Dissipator 3", 150, TorpedoSpecials.REDUCEMANEUVER.getSpecial() | TorpedoSpecials.COLLAPSESHIELDS.getSpecial() | TorpedoSpecials.IGNOREALLSHIELDS.getSpecial()));
    }

    public static String getName(int type) {
        return getInstance().stats.get(type).name;
    }

    public static int getPower(int type) {
        return getInstance().stats.get(type).dmg;
    }

    public static boolean isMicro(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.MICROTORPEDO.getSpecial()) == TorpedoSpecials.MICROTORPEDO.getSpecial();
    }

    public static boolean isPenetrating(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.PENETRATING.getSpecial()) == TorpedoSpecials.PENETRATING.getSpecial();
    }

    public static boolean isDoubleShieldDmg(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.DOUBLESHIELDDMG.getSpecial()) == TorpedoSpecials.DOUBLESHIELDDMG.getSpecial();
    }

    public static boolean isDoubleHullDmg(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.DOUBLEHULLDMG.getSpecial()) == TorpedoSpecials.DOUBLEHULLDMG.getSpecial();
    }

    public static boolean isIgnoreAllShields(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.IGNOREALLSHIELDS.getSpecial()) == TorpedoSpecials.IGNOREALLSHIELDS.getSpecial();
    }

    public static boolean isCollapseShields(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.COLLAPSESHIELDS.getSpecial()) == TorpedoSpecials.COLLAPSESHIELDS.getSpecial();
    }

    public static boolean isReduceManeuver(int type) {
        return (getInstance().stats.get(type).specials & TorpedoSpecials.REDUCEMANEUVER.getSpecial()) == TorpedoSpecials.REDUCEMANEUVER.getSpecial();
    }

    public static Array<String> getSpecialString(int type) {
        Array<String> values = new Array<String>();
        for (TorpedoSpecials specials : TorpedoSpecials.values())
            if (specials != TorpedoSpecials.NO_SPECIAL)
                if ((getInstance().stats.get(type).specials & specials.getSpecial()) == specials.getSpecial())
                    values.add(StringDB.getString(specials.getDBName()));
        return values;
    }

    public static Array<TorpedoStats> getStats() {
        return getStats(false);
    }

    public static Array<TorpedoStats> getStats(boolean onlyMicro) {
        if (!onlyMicro)
            return getInstance().stats;
        Array<TorpedoStats> filtered = new Array<TorpedoStats>();
        for (TorpedoStats ti : getInstance().stats)
            if ((ti.specials & TorpedoSpecials.MICROTORPEDO.getSpecial()) == TorpedoSpecials.MICROTORPEDO.getSpecial())
                filtered.add(ti);
        return filtered;
    }

    public static int findTorpedoIdx(TorpedoStats stat) {
        int torpedoType = 0;
        Array<TorpedoStats> stats = TorpedoInfo.getStats();
        for (int i = 0; i < stats.size; i++)
            if(stat.toString().equals(stats.get(i).toString())) {
                torpedoType = i;
                break;
            }
        return torpedoType;
    }
}
