/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

public class FireArc {
    private int mountPos; // where is the weapon located on the ship? FRONT, RIGHT, BACK, LEF
    private int angle;  // angle of firing (between 0 and 360 degrees)

    public FireArc() {
        this(0, 90);
    }

    public FireArc(int mountPos, int angle) {
        this.mountPos = mountPos;
        this.angle = angle;
    }

    public int getPosition() {
        return mountPos;
    }

    public int getAngle() {
        return angle;
    }

    public void setValues(int mountPos, int angle) {
        this.mountPos = mountPos;
        this.angle = angle;
    }
}
