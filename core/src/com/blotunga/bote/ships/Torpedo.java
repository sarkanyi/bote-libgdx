/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.utils.RandUtil;
import com.blotunga.bote.utils.Vec3i;

public class Torpedo {
    private Vec3i coord;		///< current position in space
    private Vec3i targetCoord;	///< target position
    int number;					///< if we fire more than one on the same target, then this holds the number of Torpedoes on that coordinate
    int distance;				///< distance traveled by the torpedo (if it reaches MAX_TORPEDO_RANGE then it self-destroys)
    int power;					///< damage done at target
    int type;					///< type
    Ships shipFiredTorpedo;		///< which ship fired the torpedo
    int maneuverability;		///< maneuverability of the ship which fired the torpedo
    int modifier;				///< chance to hit of the torpedo launcher + bonus trough experience
    Color color;

    public Torpedo(Vec3i coord, Vec3i targetCoord, Color color) {
        this.coord = coord;
        this.targetCoord = targetCoord;
        this.color = color;
        number = 0;
        distance = 0;
        power = 0;
        type = 0;
        shipFiredTorpedo = null;
        maneuverability = 0;
        modifier = 0;
    }

    /**
     * The function calculates the path of the torpedo and makes a collision check with all other ships. if the torpedo hits, the damage will be calculated on the ship and we return true
     * If the torpedo didn't hit anything and the MAX_TORPEDO_RANGE was reached, we return true also
     * @param cs
     * @return
     */
    public boolean fly(Array<CombatShip> cs) {
        Vec3i c = targetCoord.cpy().sub(coord);
        int minDistance = -1;
        int shipNumber = -1;
        for (int i = 0; i < cs.size; i++) {
            if (!shipFiredTorpedo.getOwnerId().equals(cs.get(i).ship.getOwnerId()) && cs.get(i).ship.isAlive()) {
                Vector3 t = new Vector3();
                if (c.x != 0)
                    t.x = (cs.get(i).coord.x - coord.x) / c.x;
                else
                    t.x = 0;
                if (c.y != 0)
                    t.y = (cs.get(i).coord.y - coord.y) / c.y;
                else
                    t.y = 0;
                if (c.z != 0)
                    t.z = (cs.get(i).coord.z - coord.z) / c.z;
                else
                    t.z = 0;

                //if one is not 0 but negative, then it's on our course but in the opposite direction
                if (t.x >= 0 && t.y >= 0 && t.z >= 0)
                    if ((t.x == 0 && t.y == t.z) || (t.y == 0 && t.x == t.z) || (t.z == 0 && t.x == t.y)
                            || (t.x == t.y && t.y == t.z))
                        if (minDistance == -1 || minDistance > (int) coord.dst(cs.get(i).coord)) {
                            minDistance = (int) coord.dst(cs.get(i).coord);
                            shipNumber = i;
                        }
            }
        }

        //if the minimum distance equals TORPEDOSPEED, then we hit
        if (minDistance <= GameConstants.TORPEDO_SPEED && shipNumber != -1) {
            int count = 0;
            for (int i = 0; i < number; i++)
                //if it hits count is incremented
                count += perhapsImpact(cs.get(shipNumber), minDistance);
            number -= count;
            if (number == 0)
                return true;
            else
                return flyToNextPosition();
        } else
            return flyToNextPosition();
    }

    /**
     * This function calculates the damage done by the torpedo;
     * @param cs
     */
    private void makeDamage(CombatShip cs) {
        int torpedoDamage = (power * 100) / cs.modifier;

        int toHull = 0;
        //if the hull is not ablative, 10% will go to the hull
        if (!cs.ship.getHull().isAblative())
            toHull = (int) (torpedoDamage * GameConstants.DAMAGE_TO_HULL);
        //if it's a penetrating torpedo and the enemy ship has no regenerative shields set up for blocking then all damage goes to the hull
        //also if we have torpedoes that ignore shields
        if ((TorpedoInfo.isPenetrating(type) && !cs.getActRegShields()) || TorpedoInfo.isIgnoreAllShields(type)
                || !cs.canUseShields)
            toHull = torpedoDamage;
        int hullDmg = TorpedoInfo.isDoubleHullDmg(type) ? toHull * 2 : toHull;
        cs.ship.getHull().setCurrentHull(-hullDmg);
        //calculate the rest which didn't go to the hull
        torpedoDamage -= toHull;
        //torpedoes loose efficiency if the shields are strong (except microtorpedoes)
        if (!TorpedoInfo.isMicro(type)) {
            int maxShield = cs.ship.getShield().getMaxShield();
            int curShield = cs.ship.getShield().getCurrentShield();
            float perc = (float) curShield / (float) maxShield;
            if (perc > 0.75f)
                perc = 0.25f;
            else if (perc > 0.5f)
                perc = 0.5f;
            else if (perc > 0.25f)
                perc = 0.75f;
            else
                perc = 1.0f;
            torpedoDamage = (int) (torpedoDamage * perc);
        }
        if (TorpedoInfo.isCollapseShields(type))
            if ((int) (RandUtil.random() * 100) < 5) //5% chance to collapse shields
                cs.ship.getShield().setCurrentShield(0);
        //the rest goes all to the shields
        int shieldDmg = TorpedoInfo.isDoubleShieldDmg(type) ? torpedoDamage * 2 : torpedoDamage;
        if (cs.ship.getShield().getCurrentShield() - shieldDmg >= 0)
            cs.ship.getShield().setCurrentShield(cs.ship.getShield().getCurrentShield() - shieldDmg);
        else {
            torpedoDamage = shieldDmg - cs.ship.getShield().getCurrentShield();
            torpedoDamage = TorpedoInfo.isDoubleShieldDmg(type) ? torpedoDamage / 2 : torpedoDamage; //reverse doubling
            if (TorpedoInfo.isDoubleHullDmg(type))
                torpedoDamage *= 2;
            cs.ship.getHull().setCurrentHull(-torpedoDamage);
            cs.ship.getShield().setCurrentShield(0);
        }

        //if we have penetrating torpedoes and they have regenerative shields, they can adapt
        if (TorpedoInfo.isPenetrating(type) && cs.ship.getShield().isRegenerative())
            cs.actRegShield();
        //if it reduces maneuverability
        if (TorpedoInfo.isReduceManeuver(type))
            if ((int) (RandUtil.random() * 100) < 5) //55% chance to reduce
                cs.setManeuverability(0);

        if (!cs.ship.isAlive())
            cs.killedByShip = shipFiredTorpedo;
    }

    /**
     * This function puts the torpedo on the next point on its course
     * @return
     */
    private boolean flyToNextPosition() {
        Vec3i a = coord.cpy();
        Vec3i b = targetCoord.cpy();
        int distance = (int) a.dst(b);
        if (distance == 0)
            return true;
        int speed = GameConstants.TORPEDO_SPEED;
        if (speed > distance)
            speed = distance;

        float multi = (float) speed / (float) distance;
        Vec3i temp = b.cpy().sub(a);
        temp.x = (int) Math.floor(temp.x * multi + 0.5);
        temp.y = (int) Math.floor(temp.y * multi + 0.5);
        temp.z = (int) Math.floor(temp.z * multi + 0.5);

        coord = a.cpy().add(temp);

        distance += GameConstants.TORPEDO_SPEED;
        if (distance >= GameConstants.MAX_TORPEDO_RANGE)
            return true;
        return false;
    }

    /**
     * @param cs
     * @param minDistance
     * @return - true if the torpedo hits
     */
    private int perhapsImpact(CombatShip cs, int minDistance) {
        modifier += CombatShip.getToHitBonus(maneuverability, cs.maneuverability);

        //if the distance is higher the chance to hit is reduced
        //the faster the target, the lower the chance
        //the more experienced the crew, the lower the chance
        int probability = modifier - (int) ((distance + minDistance) * 0.1)
                - CombatShip.getToHitMalus(maneuverability, cs.maneuverability) - cs.getCrewExperienceModifier();

        if (cs.ship.getSize() == ShipSize.SMALL)
            probability = (int) (probability * 0.66);
        else if (cs.ship.getSize() == ShipSize.BIG)
            probability = (int) (probability * 1.33);
        else if (cs.ship.getSize() == ShipSize.HUGE)
            probability = (int) (probability * 1.66);

        //there always is a 10% chance to hit
        probability = Math.max(probability, 10);
        int random = (int) (RandUtil.random() * 100);

        if (probability > random) {
            makeDamage(cs);
            return 1;
        } else
            return 0;
    }

    public Vector3 getCoord() {
        Vector3 cf = new Vector3(coord.x, coord.y, coord.z);
        cf.scl(Gdx.graphics.getHeight()/450f);
        return cf;
    }

    public Color getColor() {
        return color;
    }
}
