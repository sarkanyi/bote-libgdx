/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.lang.reflect.Constructor;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectIntMap.Entry;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.LeftSideBar;

public abstract class ScreenManager extends ResourceManager {
    private BitmapFont scaledFont;
    private ObjectIntMap<DefaultScreen> screenStorage;
    private LeftSideBar leftSideBar;
    private DefaultScreen previousScreen;
    private ViewTypes currentView;

    public ScreenManager(PlatformApiIntegration apiIntegration) {
        this((AndroidIntegration) null, apiIntegration);
    }

    public ScreenManager(AndroidIntegration integrator, PlatformApiIntegration apiIntegration) {
        super(integrator, apiIntegration);
    }

    public ScreenManager(String[] arg, PlatformApiIntegration apiIntegration) {
        super(arg, apiIntegration);
    }

    @Override
    public void create() {
        screenStorage = new ObjectIntMap<DefaultScreen>(10);
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/YoungSerif-Regular.otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();

        param.size = (int) (GamePreferences.sceneHeight / 42);
        param.genMipMaps = useMipMaps();
        param.minFilter = useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
        param.magFilter = TextureFilter.Linear;
        scaledFont = generator.generateFont(param);
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
    }

    public void initSideBar(Camera camera) {
        initSkin();
        leftSideBar = new LeftSideBar(this, camera);
    }

    public DefaultScreen showScreen(Class<?> screenClass) {
        return showScreen(screenClass, false);
    }

    private DefaultScreen showScreen(Class<?> screenClass, boolean disposable) {
        previousScreen = (DefaultScreen) getScreen();
        if (screen == null || getScreen().getClass() != screenClass || isDisposableScreen(previousScreen)) { //dispose old screen if it's disposable
            if (previousScreen != null && isDisposableScreen(previousScreen)) {
                removeScreen(previousScreen);
                previousScreen.dispose();
            }

            DefaultScreen screen = getScreenType(screenClass);
            if (screen == null) {
                try {
                    Constructor<?> ctor = screenClass.getConstructor(ScreenManager.class);
                    screen = (DefaultScreen) ctor.newInstance(new Object[] { this });
                    addScreen(screen, disposable);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            setScreen(screen);
            return screen;
        } else {
            previousScreen.show();
            return previousScreen;
        }
    }

    public DefaultScreen getPreviousScreen() {
        return previousScreen;
    }

    private boolean isDisposableScreen(DefaultScreen screen) {
        return screenStorage.get(screen, 0) != 0;
    }

    private void addScreen(DefaultScreen screen, boolean disposable) {
        screenStorage.put(screen, disposable ? 1 : 0);
    }

    private void removeScreen(DefaultScreen screen) {
        screenStorage.remove(screen, 0);
    }

    private DefaultScreen getScreenType(Class<?> screentype) {
        for (DefaultScreen sc : screenStorage.keys())
            if (sc != null && ((Object) sc).getClass() == screentype)
                return sc;
        return null;
    }

    public BitmapFont getScaledFont() {
        return scaledFont;
    }

    public boolean loadDefaultAssets() {
        if(getResolver() != null && integrator.getApkExpansionSetter() != null) {
            if (!integrator.getApkExpansionSetter().setApkExpansionFile(GameConstants.apkExpansionFileVersion, 0)) {
                return false;
            }
        }

        if(!getAssetManager().isLoaded(GameConstants.UISKIN_PATH))
            getAssetManager().load(GameConstants.UISKIN_PATH, Skin.class);
        if(!getAssetManager().isLoaded("graphics/symbols/symbols.pack"))
            getAssetManager().load("graphics/symbols/symbols.pack", TextureAtlas.class);
        if(!getAssetManager().isLoaded("graphics/ui/ui.pack"))
            getAssetManager().load("graphics/ui/ui.pack", TextureAtlas.class);
        TextureParameter param = new TextureParameter();
        param.genMipMaps = useMipMaps();
        param.minFilter = TextureFilter.Linear;
        param.magFilter = TextureFilter.Linear;
        if(!getAssetManager().isLoaded(GameConstants.UI_BG_ROUNDED))
            getAssetManager().load(GameConstants.UI_BG_ROUNDED, Texture.class, param);
        if(!getAssetManager().isLoaded(GameConstants.UI_BG_SIMPLE))
		    getAssetManager().load(GameConstants.UI_BG_SIMPLE, Texture.class, param);
        getAssetManager().finishLoading();
        return true;
    }

    public DefaultScreen setView(ViewTypes viewType) {
        return setView(viewType, false);
    }

    public DefaultScreen setView(ViewTypes viewType, boolean disposable) {
        if(viewType == ViewTypes.MAIN_MENU || viewType == ViewTypes.RESOURCE_EDITOR) {
            TextureParameter param = new TextureParameter();
            param.genMipMaps = useMipMaps();
            param.minFilter = useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
            param.magFilter = TextureFilter.Linear;           
            unLoadBackgounds();
            if(!getAssetManager().isLoaded("graphics/events/Startmenu.jpg"))
                getAssetManager().load("graphics/events/Startmenu.jpg", Texture.class, param);
            clear();
            uninit();
            getAssetManager().finishLoading();
        }
        currentView = viewType;
        return showScreen(viewType.getClassType(), disposable);
    }

    public ViewTypes getCurrentView() {
        return currentView;
    }

    public void setSubMenu(DefaultScreen screen, int value) {
        try {
            ((Object) screen).getClass().getMethod("setSubMenu", int.class).invoke(screen, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clear() {
        for (Iterator<Entry<DefaultScreen>> iter = screenStorage.entries().iterator(); iter.hasNext();) {
            Entry<DefaultScreen> screen = iter.next();
            screen.key.hide();
            screen.key.dispose();
        }
        screenStorage.clear();
        if (getMusic() != null) {
            getMusic().stop();
            getMusic().dispose();
        }
        if (getSoundManager() != null) {
            getSoundManager().dispose();
        }
        clearSkin();
        setContinueAfterVictory(false);
    }

    public LeftSideBar getSidebarLeft() {
        return leftSideBar;
    }

    @Override
    public void dispose() {
        super.dispose();
        if (leftSideBar != null)
            leftSideBar.dispose();
        clear();
    }

    public DefaultScreen getScreen() {
        return (DefaultScreen) super.getScreen();
    }

    @Override
    public void setScreen(Screen screen) {
        previousScreen = (DefaultScreen) getScreen();
        super.setScreen(screen);
    }

    public void endTurn() {
        if(!processingTurn()) {
            Major playerRace = getRaceController().getPlayerRace();
            if (playerRace != null && playerRace.aHumanPlays())
                getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ENDOFROUND);
            setProcessingTurn(true);

            if (!getGameSettings().stickyScreen) {
                getUniverseMap().getRenderer().getRightSideBar().getShipRenderer().clearSelectedShip();
                getUniverseMap().getRenderer().updateScreen();
                getUniverseMap().getRenderer().showPlanets(true);
                setView(ViewTypes.GALAXY_VIEW, false);
            }

            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    getUniverseMap().processTurn();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            setProcessingTurn(false);
                            getUniverseMap().afterProcessTurn();
                        }
                     });
                }
            });

            th.start();
        }
    }
}
