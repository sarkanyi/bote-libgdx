/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.util.Arrays;

import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.TutorialType;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class GameSettings implements KryoSerializable {
    public enum GalaxyShowState {
        POLITICAL,
        SCANRANGE,
        DIPLOMACY,
        EXPANSION;
    }

    //stuff that can be modified at any time
    public int version = 6;
    public boolean drawGrid;
    public boolean drawRoutes;
    public boolean showStarSystemNames;
    public float musicVolume;
    public float effectsVolume;
    public boolean autoSave;
    public String backgroundMod; // - empty if texture fits, mediumres otherwise
    private int memprofile; //0 - disable preload, 1 - enable preload, 2 - lowmemprofile
    public boolean useCompressedSave; //initialized with false, but saved with true after saves are converted
    public boolean[] tutorialSeen;
    public boolean autoSignIn;
    public int[] achievements;
    private GalaxyShowState showState;
    public boolean showOptionsMenu;
    public boolean stickyScreen;

    public GameSettings() {
        drawGrid = false;
        drawRoutes = true;
        showStarSystemNames = true;
        musicVolume = 0.6f;
        effectsVolume = 1.0f;
        autoSave = true;
        memprofile = 0;
        setDisablePreload();
        backgroundMod = "mediumres/";
        useCompressedSave = true;
        tutorialSeen = new boolean[TutorialType.values().length];
        Arrays.fill(tutorialSeen, false);
        showState = GalaxyShowState.POLITICAL;
        autoSignIn = false;
        achievements = new int[AchievementsList.values().length];
        Arrays.fill(achievements, 0);
        showOptionsMenu = true;
        stickyScreen = false;
    }

    public GameSettings(GameSettings other) {
        drawGrid = other.drawGrid;
        drawRoutes = other.drawRoutes;
        showStarSystemNames = other.showStarSystemNames;
        musicVolume = other.musicVolume;
        effectsVolume = other.effectsVolume;
        autoSave = other.autoSave;
        memprofile = other.memprofile;
        backgroundMod = other.backgroundMod;
        useCompressedSave = other.useCompressedSave;
        //clone can't be used because new tutorials might be added in the future
        tutorialSeen = new boolean[TutorialType.values().length];
        Arrays.fill(tutorialSeen, false);
        for(int i = 0; i < other.tutorialSeen.length; i++)
            tutorialSeen[i] = other.tutorialSeen[i];
        showState = other.showState;
        autoSignIn = other.autoSignIn;
        achievements = new int[AchievementsList.values().length];
        Arrays.fill(achievements, 0);
        for(int i = 0; i < other.achievements.length; i++)
            achievements[i] = other.achievements[i];
        showOptionsMenu = other.showOptionsMenu;
        stickyScreen = other.stickyScreen;
    }

    public void setEnablePreload() {
        memprofile &= ~1;
    }

    public void setDisablePreload() {
        memprofile |= 1;
    }

    public boolean isPreload() {
        return (memprofile & 1) == 0;
    }

    public void setEnableLowMemProfile() {
        memprofile |= 2;
    }

    public void setDisableLowMemProfile() {
        memprofile &= ~2;
    }

    public boolean isLowMemProfile() {
        return ((memprofile & 2) != 0) && isPreload();
    }

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeInt(version, true);
        output.writeBoolean(autoSave);
        kryo.writeObject(output, backgroundMod);
        output.writeBoolean(drawGrid);
        output.writeBoolean(drawRoutes);
        output.writeFloat(effectsVolume);
        output.writeInt(memprofile, false);
        output.writeFloat(musicVolume);
        output.writeBoolean(showStarSystemNames);
        kryo.writeObject(output, tutorialSeen);
        output.writeBoolean(useCompressedSave);
        output.writeBoolean(autoSignIn);
        kryo.writeObject(output, showState);
        output.writeBoolean(showOptionsMenu);
        output.writeBoolean(stickyScreen);
        kryo.writeObject(output, achievements);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        version = input.readInt(true);
        autoSave = input.readBoolean();
        backgroundMod = kryo.readObject(input,  String.class);
        drawGrid = input.readBoolean();
        drawRoutes = input.readBoolean();
        effectsVolume = input.readFloat();
        memprofile = input.readInt(false);
        musicVolume = input.readFloat();
        if (version < 4)
            input.readBoolean(); // formerly showScanRange
        showStarSystemNames = input.readBoolean();
        tutorialSeen = kryo.readObject(input,  boolean[].class);
        useCompressedSave = input.readBoolean();
        autoSignIn = input.readBoolean();
        if (version >= 4)
            showState = kryo.readObject(input,  GalaxyShowState.class);
        if (version >= 5)
            showOptionsMenu = input.readBoolean();
        if (version >= 6)
            stickyScreen = input.readBoolean();
        achievements = kryo.readObject(input,  int[].class);
    }

    public void setShowState(GalaxyShowState newState) {
        showState = newState;
    }

    public GalaxyShowState getShowState() {
        return showState;
    }
}
