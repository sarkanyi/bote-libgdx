/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.races.Major;

public class SkinManager implements Disposable {
    private Skin skin;

    public SkinManager(ResourceManager screenManager) {
        Major playerRace = screenManager.getRaceController().getPlayerRace();
        skin = new Skin(Gdx.files.internal(GameConstants.UISKIN_PATH)); //can't use the one in the assetManager because we might go under with it

        BitmapFont smallFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 55));
        smallFont.setFixedWidthGlyphs("1234567890");
        smallFont.setUseIntegerPositions(true);
        skin.add("smallFont", smallFont, BitmapFont.class);

        BitmapFont mediumFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int)  (GamePreferences.sceneHeight / 50));
        mediumFont.setFixedWidthGlyphs("1234567890");
        mediumFont.setUseIntegerPositions(true);
        skin.add("mediumFont", mediumFont, BitmapFont.class);

        BitmapFont normalFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 45));
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("normalFont", normalFont, BitmapFont.class);

        BitmapFont largeFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 40));
        largeFont.setFixedWidthGlyphs("1234567890");
        largeFont.setUseIntegerPositions(true);
        skin.add("largeFont", largeFont, BitmapFont.class);

        BitmapFont xlFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 30));
        xlFont.setFixedWidthGlyphs("1234567890");
        xlFont.setUseIntegerPositions(true);
        skin.add("xlFont", xlFont, BitmapFont.class);

        BitmapFont xxlFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 25));
        xxlFont.setFixedWidthGlyphs("1234567890");
        xxlFont.setUseIntegerPositions(true);
        skin.add("xxlFont", xxlFont, BitmapFont.class);

        BitmapFont hugeFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 20));
        hugeFont.setFixedWidthGlyphs("1234567890");
        hugeFont.setUseIntegerPositions(true);
        skin.add("hugeFont", hugeFont, BitmapFont.class);

        BitmapFont vsFont = screenManager.getRacialFont(playerRace.getRaceDesign().fontName,
                (int) (GamePreferences.sceneHeight / 10));
        vsFont.setFixedWidthGlyphs("1234567890");
        vsFont.setUseIntegerPositions(true);
        skin.add("vsFont", vsFont, BitmapFont.class);

        TextureAtlas buttonAtlas = screenManager.getAssetManager().get(
                "graphics/ui/" + playerRace.getPrefix() + "ui.pack");
        if (!skin.has("button_small", Sprite.class)) {
            TextureRegion buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "button_small"));
            buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
            skin.add("button_small", new Sprite(buttonTex));
        }
        if (!skin.has("buttonminus", Sprite.class)) {
            TextureRegion buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttonminus"));
            buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
            skin.add("buttonminus", new Sprite(buttonTex));
            buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttonminusa"));
            buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
            skin.add("buttonminusa", new Sprite(buttonTex));
        }
        if (!skin.has("buttonplus", Sprite.class)) {
            TextureRegion buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttonplus"));
            buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
            skin.add("buttonplus", new Sprite(buttonTex));
            buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttonplusa"));
            buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
            skin.add("buttonplusa", new Sprite(buttonTex));
        }

        if (!skin.has("small-buttons", TextButtonStyle.class)) {
            TextButtonStyle styleSmall = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            styleSmall.font = skin.getFont("normalFont");
            styleSmall.fontColor = playerRace.getRaceDesign().clrSmallBtn;
            styleSmall.disabledFontColor = Color.DARK_GRAY;
            styleSmall.up = skin.getDrawable("button_small");
            styleSmall.down = skin.newDrawable("button_small", Color.LIGHT_GRAY);
            styleSmall.over = skin.newDrawable("button_small");
            styleSmall.disabled = skin.newDrawable("button_small", Color.DARK_GRAY);
            skin.add("small-buttons", styleSmall);
        }

        TextureRegion buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "button"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("background", new Sprite(buttonTex));
        buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttoni"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("backgroundi", new Sprite(buttonTex));
        buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "buttona"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("backgrounda", new Sprite(buttonTex));
        buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "button_roundend"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("ebackground", new Sprite(buttonTex));
        buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "button_roundendi"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("ebackgroundi", new Sprite(buttonTex));
        buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix() + "button_roundenda"));
        buttonTex.getTexture().setFilter(screenManager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        skin.add("ebackgrounda", new Sprite(buttonTex));
        TextButtonStyle buttonStyle = skin.get("default", TextButtonStyle.class);
        buttonStyle.font = screenManager.getRacialFont(playerRace.getRaceDesign().fontName, (int) (GamePreferences.sceneHeight / 32));
        buttonStyle.fontColor = playerRace.getRaceDesign().clrLargeBtn;
        buttonStyle.up = skin.getDrawable("background");
        buttonStyle.down = skin.getDrawable("backgroundi");
        buttonStyle.over = skin.getDrawable("backgrounda");
        TextButtonStyle disabledStyle = new TextButtonStyle(buttonStyle);
        disabledStyle.up = skin.getDrawable("backgroundi");
        disabledStyle.fontColor = Color.BLACK;
        disabledStyle.downFontColor = Color.BLACK;
        disabledStyle.down = skin.getDrawable("backgroundi");
        disabledStyle.over = skin.getDrawable("backgroundi");
        skin.add("default-disabled", disabledStyle);

        ScrollPaneStyle st = new ScrollPaneStyle();
        Sprite sp = new Sprite(screenManager.getUiTexture("sidescroll"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScroll = new SpriteDrawable(sp);
        sp = new Sprite(screenManager.getUiTexture("listselect"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.hScroll = new SpriteDrawable(sp);
        sp = new Sprite(screenManager.getUiTexture("shippath"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScrollKnob = new SpriteDrawable(sp);
        st.hScrollKnob = new SpriteDrawable(sp);
        skin.add("default", st);
    }

    public Skin getSkin() {
        return skin;
    }

    @Override
    public void dispose() {
       //these should not be in the skin as they are freed from the assetManager
        skin.remove("smallFont", BitmapFont.class);
        skin.remove("mediumFont", BitmapFont.class);
        skin.remove("normalFont", BitmapFont.class);
        skin.remove("largeFont", BitmapFont.class);
        skin.remove("xlFont", BitmapFont.class);
        skin.remove("xxlFont", BitmapFont.class);
        skin.remove("hugeFont", BitmapFont.class);
        skin.remove("vsFont", BitmapFont.class);
        skin.dispose();
    }
}
