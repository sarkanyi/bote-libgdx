/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.badlogic.gdx.utils.Array;

public class IntelReports {
    private Array<IntelObject> intelObjects;		///< Field with all intel reports
    private IntelObject attemptObject;				///< the object which should be used for an intel hit
    private int activeReport;						///< the clicked/active report
    private boolean addedReport;					///< true if in the current round a report was added

    public IntelReports() {
        activeReport = -1;
        addedReport = false;
        attemptObject = null;
        intelObjects = new Array<IntelObject>();
    }

    /**
     * Gets an intel report
     * @param n
     * @return
     */
    public IntelObject getReport(int n) {
        return intelObjects.get(n);
    }

    /**
     * Gets the array with the reports
     * @return
     */
    public Array<IntelObject> getAllReports() {
        return intelObjects;
    }

    /**
     * Size of the array with the reports
     * @return
     */
    public int getNumberOfReports() {
        return intelObjects.size;
    }

    /**
     * The number of the currently active report
     * @return
     */
    public int getActiveReport() {
        return activeReport;
    }

    /**
     * Returns the possible attemptObject
     * @return - null if there is none
     */
    public IntelObject getAttemptObject() {
        return attemptObject;
    }

    /**
     * The function creates a new attempt object
     * @param spyReport
     */
    public void createAttemptObject(IntelObject spyReport) {
        int type = spyReport.getType();
        switch (type) {
            case 0:
                attemptObject = new EconIntelObject((EconIntelObject) spyReport);
                break;
            case 1:
                attemptObject = new ScienceIntelObject((ScienceIntelObject) spyReport);
                break;
            case 2:
                attemptObject = new MilitaryIntelObject((MilitaryIntelObject) spyReport);
                break;
            case 3:
                attemptObject = new DiplomacyIntelObject((DiplomacyIntelObject) spyReport);
                break;
            default:
                attemptObject = null;
        }
    }

    /**
     * Returns whether a report was added in this turn
     * @return
     */
    public boolean isReportAdded() {
        return addedReport;
    }

    /**
     * The function sets a report as active report
     * @param n
     */
    public void setActiveReport(int n) {
        if (n < intelObjects.size)
            activeReport = n;
        else
            activeReport = intelObjects.size - 1;
    }

    /**
     * Sets whether a report was added or not
     * @param isAdded
     */
    public void setIsReportAdded(boolean isAdded) {
        addedReport = isAdded;
    }

    /**
     * Adds a report into the array
     * @param report
     */
    public void addReport(IntelObject report) {
        intelObjects.add(report);
        setIsReportAdded(true);
    }

    /**
     * The function removes a report from the array
     * @param n
     */
    public void removeReport(int n) {
        intelObjects.removeIndex(n);
    }

    /**
     * Clears the array
     */
    public void removeAllReports() {
        intelObjects.clear();
    }

    public void sortAllReports() {
        sortAllReports(true);
    }

    /**
     * Sorts the array
     * @param desc
     */
    public void sortAllReports(boolean desc) {
        intelObjects.sort();
        if (desc)
            intelObjects.reverse();
    }

    public void removeAttemptObject() {
        attemptObject = null;
    }

    /**
     * Deletes reports older than {@code turn}. Assumes the array is already reverse sorted.
     * @param turn
     */
    public void deleteOlder(int turn) {
        int start = 0;
        int end = intelObjects.size - 1;
        int middle = -1;
        int lastValue = 0;
        //binary search for the closest value
        while (start <= end) {
            middle = start + (end - start) / 2;
            lastValue = intelObjects.get(middle).round;

            if (turn < lastValue) {
                start = middle + 1;
            }
            else if (turn > lastValue) {
                end = middle - 1;
            }
            else break;
        }
        if (middle > 0 && intelObjects.get(middle).round <= turn)
            intelObjects.removeRange(middle, intelObjects.size -  1);
    }
}
