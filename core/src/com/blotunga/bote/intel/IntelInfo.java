/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import java.util.Iterator;

import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class IntelInfo {
    private ObjectIntMap<String> controlledSectors;	///< number of controlled sectors
    private ObjectIntMap<String> ownedSystems;		///< number of owned systems (also uninhabited)
    private ObjectIntMap<String> inhabitedSystems;	///< number of inhabited systems
    private ObjectIntMap<String> knownMinors;		///< number of knowns minors
    private ObjectIntMap<String> minorMembers;		///< number of minors who are members
    private boolean calculated;						///< was the information calculated in this turn?

    public IntelInfo() {
        controlledSectors = new ObjectIntMap<String>();
        ownedSystems = new ObjectIntMap<String>();
        inhabitedSystems = new ObjectIntMap<String>();
        knownMinors = new ObjectIntMap<String>();
        minorMembers = new ObjectIntMap<String>();
        calculated = false;
    }

    public void reset() {
        controlledSectors.clear();
        ownedSystems.clear();
        inhabitedSystems.clear();
        knownMinors.clear();
        minorMembers.clear();
        calculated = false;
    }

    public int getControlledSectors(String race) {
        return controlledSectors.get(race, 0);
    }

    public int getOwnedSystems(String race) {
        return ownedSystems.get(race, 0);
    }

    public int getInhabitedSystems(String race) {
        return inhabitedSystems.get(race, 0);
    }

    public int getKnownMinors(String race) {
        return knownMinors.get(race, 0);
    }

    public int getMinorMembers(String race) {
        return minorMembers.get(race, 0);
    }

    @SuppressWarnings("unchecked")
    public void calcIntelInfo(ResourceManager manager, Major ourRace) {
        if (calculated)
            return;
        reset();
        //the value if this is : 1 - controlled sector, 2 - owned sector, 3 - inhabited sector
        ObjectIntMap<String>[][] sectors = new ObjectIntMap[manager.getGridSizeX()][manager.getGridSizeY()];
        // 1 - known minor or 2 - member minor
        ObjectIntMap<String>[][] races = new ObjectIntMap[manager.getGridSizeX()][manager.getGridSizeY()];
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++) {
                sectors[x][y] = new ObjectIntMap<String>();
                races[x][y] = new ObjectIntMap<String>();
            }

        for (int i = 0; i < ourRace.getEmpire().getIntelligence().getIntelReports().getNumberOfReports(); i++) {
            IntelObject intelObj = ourRace.getEmpire().getIntelligence().getIntelReports().getReport(i);
            if (!intelObj.getEnemy().equals(ourRace.getRaceId())) {
                IntPoint coord = new IntPoint();
                switch (intelObj.getType()) {
                    case 0:
                        coord = ((EconIntelObject) intelObj).getCoord();
                        break;
                    case 1:
                        coord = ((ScienceIntelObject) intelObj).getCoord();
                        break;
                    case 2:
                        MilitaryIntelObject miobj = (MilitaryIntelObject) intelObj;
                        if (miobj.isBuilding())
                            coord = miobj.getCoord();
                        break;
                }
                if (!coord.equals(new IntPoint())) {
                    //check wheter these are true, old reports can have false informations
                    StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                    if (ss.getOwnerId().equals(intelObj.getEnemy())) {
                        sectors[coord.x][coord.y].put(intelObj.getEnemy(), 1);
                        if (ss.isSunSystem()) {
                            sectors[coord.x][coord.y].put(intelObj.getEnemy(), 2);
                            if (ss.getCurrentInhabitants() > 0.0f)
                                sectors[coord.x][coord.y].put(intelObj.getEnemy(), 3);
                        }
                    }
                }
                if (intelObj.getType() == 3) { //-diplomacy object
                    coord = ((DiplomacyIntelObject) intelObj).getMinorRaceCoord();
                    if (!coord.equals(new IntPoint()))
                        if (races[coord.x][coord.y].get(intelObj.getEnemy(), 0) < 2) {
                            Race minor = manager.getUniverseMap().getStarSystemAt(coord).isHomeOf();
                            if (minor != null) {
                                if (((DiplomacyIntelObject) intelObj).getAgreement() == DiplomaticAgreement.MEMBERSHIP)
                                    races[coord.x][coord.y].put(intelObj.getEnemy(), 2);
                                else
                                    races[coord.x][coord.y].put(intelObj.getEnemy(), 1);
                            }
                        }
                }
            }
        }

        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();
        for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            Minor minor = e.value;
            if (minor.isAlien())
                continue;
            //if we know a minor and we have at least friendship with them, then we can know that another race also knows them
            //if we know of the minors home system and this was made member by someone then we also know about membership
            IntPoint coord = minor.getCoordinates();
            if (!coord.equals(new IntPoint())) {
                if (minor.getAgreement(ourRace.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType()) {
                    for (int i = 0; i < majors.size; i++) {
                        String otherID = majors.getKeyAt(i);
                        if (!otherID.equals(ourRace.getRaceId()))
                            if (minor.isRaceContacted(otherID))
                                if (races[coord.x][coord.y].get(otherID, 0) < 1)
                                    races[coord.x][coord.y].put(otherID, 1);
                    }
                } else if (manager.getUniverseMap().getStarSystemAt(coord).getKnown(ourRace.getRaceId())) { //can only happen if we have less than friendship
                    for (int i = 0; i < majors.size; i++) {
                        String otherID = majors.getKeyAt(i);
                        Major otherRace = majors.getValueAt(i);
                        if (!otherID.equals(ourRace.getRaceId()))
                            if (otherRace.isRaceContacted(ourRace.getRaceId()))
                                if (minor.isMemberTo(otherID))
                                    races[coord.x][coord.y].put(otherID, 2);
                    }
                }
            }
            //calculate own relationship with mnors
            if (minor.isRaceContacted(ourRace.getRaceId()))
                races[coord.x][coord.y].put(ourRace.getRaceId(), 1);
            if (minor.isMemberTo(ourRace.getRaceId()))
                races[coord.x][coord.y].put(ourRace.getRaceId(), 2);
        }

        //now calculate controlled, owned and inhabited systems
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                for (int i = 0; i < majors.size; i++) {
                    Major major = majors.getValueAt(i);
                    String majorID = majors.getKeyAt(i);
                    StarSystem ss = manager.getUniverseMap().getStarSystemAt(x, y);
                    if (major.isRaceContacted(ourRace.getRaceId()) || majorID.equals(ourRace.getRaceId()))
                        if (ss.getScanned(ourRace.getRaceId()))
                            if (ss.getOwnerId().equals(majorID)) {
                                if (sectors[x][y].get(majorID, 0) < 1)
                                    sectors[x][y].put(majorID, 1);
                                if (ss.isSunSystem()) {
                                    if (sectors[x][y].get(majorID, 0) < 2)
                                        sectors[x][y].put(majorID, 2);
                                    if (ss.getKnown(ourRace.getRaceId()) && ss.getInhabitants() > 0.0f)
                                        if (sectors[x][y].get(majorID, 0) < 3)
                                            sectors[x][y].put(majorID, 3);
                                }
                            }

                    if (sectors[x][y].get(majorID, 0) > 0) {
                        controlledSectors.put(majorID, controlledSectors.get(majorID, 0) + 1);
                        if (sectors[x][y].get(majorID, 0) == 3) {
                            inhabitedSystems.put(majorID, inhabitedSystems.get(majorID, 0) + 1);
                            ownedSystems.put(majorID, ownedSystems.get(majorID, 0) + 1);
                        } else if (sectors[x][y].get(majorID, 0) == 2)
                            ownedSystems.put(majorID, ownedSystems.get(majorID, 0) + 1);
                    }
                    if (races[x][y].get(majorID, 0) > 0) {
                        knownMinors.put(majorID, knownMinors.get(majorID, 0) + 1);
                        if (races[x][y].get(majorID, 0) == 2)
                            minorMembers.put(majorID, minorMembers.get(majorID, 0) + 1);
                    }
                }
        calculated = true;
    }
}
