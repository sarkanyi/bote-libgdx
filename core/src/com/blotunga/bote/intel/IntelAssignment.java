/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.RandUtil;

public class IntelAssignment {
    private ObjectIntMap<String> percentage_[];  ///< percentage between espionage and sabotage
    private ObjectIntMap<String> spyPercentage_[]; ///< percentage of the sperate espionage fields
    private ObjectIntMap<String> sabPercentage_[]; ///< percentage of the sabotage fields

    @SuppressWarnings("unchecked")
    public IntelAssignment() {
        percentage_ = new ObjectIntMap[2];
        for (int i = 0; i < percentage_.length; i++)
            percentage_[i] = new ObjectIntMap<String>();
        spyPercentage_ = new ObjectIntMap[4];
        for (int i = 0; i < spyPercentage_.length; i++)
            spyPercentage_[i] = new ObjectIntMap<String>();
        sabPercentage_ = new ObjectIntMap[4];
        for (int i = 0; i < sabPercentage_.length; i++)
            sabPercentage_[i] = new ObjectIntMap<String>();
    }

    /**
     * Function gets the inner security percentage
     * @return
     */
    public int getInnerSecurityPercentage() {
        int perc = 100;
        for (int i = 0; i < percentage_.length; i++)
            for (ObjectIntMap.Entries<String> iter = percentage_[i].entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                perc -= e.value;
            }
        return perc;
    }

    /**
     * Function returns the espionage percentage
     * @param race
     * @return
     */
    public int getGlobalSpyPercentage(String race) {
        return percentage_[0].get(race, 0);
    }

    /**
     * Function returns the sabotage percentage
     * @param race
     * @return
     */
    public int getGlobalSabotagePercentage(String race) {
        return percentage_[1].get(race, 0);
    }

    /**
     * Gets the espionage assignments of a race
     * @param race
     * @param type economy == 0, science == 1, military == 2, diplomacy == 3, storage == 4
     * @return
     */
    public int getSpyPercentages(String race, int type) {
        if (type != 4) {
            return spyPercentage_[type].get(race, 0);
        } else {
            int innerSec = 0;
            for (int i = 0; i < 4; i++) {
                innerSec += spyPercentage_[i].get(race, 0);
            }
            return 100 - innerSec;
        }
    }

    /**
     * Gets the sabotage assignments of a race
     * @param race
     * @param type economy == 0, science == 1, military == 2, diplomacy == 3, storage == 4
     * @return
     */
    public int getSabotagePercentages(String race, int type) {
        if (type != 4) {
            return sabPercentage_[type].get(race, 0);
        } else {
            int innerSec = 0;
            for (int i = 0; i < 4; i++) {
                innerSec += sabPercentage_[i].get(race, 0);
            }
            return 100 - innerSec;
        }
    }

    /**
     * Function returns the field of the global balance
     * @param race
     * @param type - 0 espionage, 1 sabotage
     * @return
     */
    public int getGlobalPercentage(String race, int type) {
        return percentage_[type].get(race, 0);
    }

    /**
     * The function changes the global percentages of the different espionage fields. For this the others are modified acoordingly
     * @param type - 0 - espionage, 1 - sabotage, 2 - inner security
     * @param perc
     * @param major
     * @param race
     * @param majors
     */
    public void setGlobalPercentage(int type, int perc, Major major, String race, ArrayMap<String, Major> majors) {
        int known = 0;
        for (int i = 0; i < majors.size; i++) {
            String otherRaceID = majors.getKeyAt(i);
            if (!otherRaceID.equals(major.getRaceId()) && major.isRaceContacted(otherRaceID))
                known++;
            else {
                if (percentage_[0].get(otherRaceID, 0) > 0)
                    percentage_[0].put(otherRaceID, 0);
                if (percentage_[1].get(otherRaceID, 0) > 0)
                    percentage_[1].put(otherRaceID, 0);
            }
        }

        //if the inner security settings is modified
        if (type == 2 && known > 0) {
            @SuppressWarnings("unchecked")
            ObjectIntMap<String> percentages[] = new ObjectIntMap[2];
            for (int i = 0; i < percentages.length; i++)
                percentages[i] = new ObjectIntMap<String>();
            for (int i = 0; i < majors.size; i++) {
                String otherRaceID = majors.getKeyAt(i);
                for (int j = 0; j < 2; j++)
                    percentages[j].put(otherRaceID, percentage_[j].get(otherRaceID, 0));
            }
            do {
                int diff = getInnerSecurityPercentage() - perc;
                int t = diff / (known * 2);
                int rest = diff - t * (known * 2);
                Array<String> randoms = new Array<String>();
                for (int i = 0; i < majors.size; i++) {
                    String otherRaceID = majors.getKeyAt(i);
                    if (!otherRaceID.equals(major.getRaceId()) && major.isRaceContacted(otherRaceID)) {
                        randoms.add(otherRaceID);
                        for (int j = 0; j < 2; j++)
                            percentages[j].put(otherRaceID, percentages[j].get(otherRaceID, 0) + t);
                    }
                }
                if (rest != 0 && randoms.size != 0) {
                    String randomID = randoms.get((int) (RandUtil.random() * randoms.size));
                    int idx = (int) (RandUtil.random() * 2);
                    percentages[idx].put(randomID, percentages[idx].get(randomID, 0) + rest);
                }
                //check that there is nowhere less than 0 or more than 100
                for (int i = 0; i < majors.size; i++) {
                    String otherRaceID = majors.getKeyAt(i);
                    for (int j = 0; j < 2; j++) {
                        if (percentages[j].get(otherRaceID, 0) < 0)
                            percentages[j].put(otherRaceID, 0);
                        else if (percentages[j].get(otherRaceID, 0) > 100)
                            percentages[j].put(otherRaceID, 100);
                    }
                }
                for (int i = 0; i < majors.size; i++) {
                    String otherRaceID = majors.getKeyAt(i);
                    for (int j = 0; j < 2; j++) {
                        percentage_[j].put(otherRaceID, percentages[j].get(otherRaceID, 0));
                    }
                }
            } while (getInnerSecurityPercentage() != perc);
        } else if (type != 2 && major.isRaceContacted(race) && !race.equals(major.getRaceId())) {
            int innerSecurity = getInnerSecurityPercentage();
            if (perc - percentage_[type].get(race, 0) > innerSecurity)
                perc = percentage_[type].get(race, 0) + innerSecurity;
            percentage_[type].put(race, perc);
        }
    }

    /**
     * Function changes the different percentages for espionage. Unassigned points will be in the espionage storage of the race
     * @param type
     * @param perc
     * @param race
     */
    public void setSpyPercentage(int type, int perc, String race) {
        if (type == 4) {
            int percentages[] = new int[4];
            for (int i = 0; i < 4; i++)
                percentages[i] = spyPercentage_[i].get(race, 0);

            do {
                int diff = getSpyPercentages(race, 4);
                diff -= perc;
                int t = diff / 4;
                int rest = diff - t * 4;
                //distribute the points equally
                for (int i = 0; i < 4; i++)
                    percentages[i] += t;
                if (rest != 0) {
                    int random = (int) (RandUtil.random() * 4);
                    percentages[random] += rest;
                }
                for (int i = 0; i < 4; i++) {
                    if (percentages[i] < 0)
                        percentages[i] = 0;
                    else if (percentages[i] > 100)
                        percentages[i] = 100;
                }
                for (int i = 0; i < 4; i++)
                    spyPercentage_[i].put(race, percentages[i]);
            } while (getSpyPercentages(race, 4) != perc);
        } else {
            int spyDepot = getSpyPercentages(race, 4);
            if (perc - spyPercentage_[type].get(race, 0) > spyDepot)
                perc = spyPercentage_[type].get(race, 0) + spyDepot;
            spyPercentage_[type].put(race, perc);
        }
    }

    /**
     * Function changes the different percentages for sabotage. Unassigned points will be in the sabotage storage of the race
     * @param type
     * @param perc
     * @param race
     */
    public void setSabotagePercentage(int type, int perc, String race) {
        if (type == 4) {
            int percentages[] = new int[4];
            for (int i = 0; i < 4; i++)
                percentages[i] = sabPercentage_[i].get(race, 0);

            do {
                int diff = getSabotagePercentages(race, 4);
                diff -= perc;
                int t = diff / 4;
                int rest = diff - t * 4;
                //distribute the points equally
                for (int i = 0; i < 4; i++)
                    percentages[i] += t;
                if (rest != 0) {
                    int random = (int) (RandUtil.random() * 4);
                    percentages[random] += rest;
                }
                for (int i = 0; i < 4; i++) {
                    if (percentages[i] < 0)
                        percentages[i] = 0;
                    else if (percentages[i] > 100)
                        percentages[i] = 100;
                }
                for (int i = 0; i < 4; i++)
                    sabPercentage_[i].put(race, percentages[i]);
            } while (getSabotagePercentages(race, 4) != perc);
        } else {
            int sabDepot = getSabotagePercentages(race, 4);
            if (perc - sabPercentage_[type].get(race, 0) > sabDepot)
                perc = sabPercentage_[type].get(race, 0) + sabDepot;
            sabPercentage_[type].put(race, perc);
        }
    }

    public void removeRaceFromAssignments(String race) {
        for (int i = 0; i < 2; i++)
            percentage_[i].remove(race, 0);
        for (int i = 0; i < 4; i++) {
            spyPercentage_[i].remove(race, 0);
            sabPercentage_[i].remove(race, 0);
        }
    }
}
