/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum RaceProperty {
    NOTHING_SPECIAL(0),
    FINANCIAL(1),
    WARLIKE(2),
    AGRARIAN(3),
    INDUSTRIAL(4),
    SECRET(5),
    SCIENTIFIC(6),
    PRODUCER(7),
    PACIFIST(8),
    SNEAKY(9),
    SOLOING(10),
    HOSTILE(11);

    private static final IntMap<RaceProperty> intToTypeMap = new IntMap<RaceProperty>();

    private int type;

    RaceProperty(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    static {
        for (RaceProperty rp : RaceProperty.values()) {
            intToTypeMap.put(rp.type, rp);
        }
    }

    public static RaceProperty fromInt(int i) {
        RaceProperty at = intToTypeMap.get(i);
        return at;
    }
}
