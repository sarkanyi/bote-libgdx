/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import java.util.Locale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.blotunga.bote.GamePreferences;

public class GameConstants {
    private static transient Locale locale             = null;
    public static int screenshotMulti                  = 10;
    public final static int apkExpansionFileVersion    = 149;
    public final static long apkExpansionFileSize      = 177235296L;
    public final static int SmallestCombatibleSave     = 2;
    public final static int MAX_FOOD_STORE             = 25000;
    public final static int MAX_RES_STORE              = 125000;
    public final static int MAX_DERITIUM_STORE         = 100;
    public final static int MAX_MORALE                 = 200;
    public final static double DAMAGE_TO_HULL          = 0.1; // percent of damage which always hits the hull
    public final static double SPECIAL_RESEARCH_DIV    = 1.67;// value by which the researchpoints for the special research are divided (higher means less RP needed)
    public final static double TECH_PROD_BONUS         = 2.0; //techbonus on production (for example 2%/level for energy etc)
    public final static int ALE                        = 8;   // assembly list entries
    public final static int NOBIOL                     = 12;  // Number of Buildings in Overview List
    public final static int NOBIEL                     = 9;
    public final static int TURNS_TO_KEEP_INTEL_MSG    = 30; //number of turns after to which delete old intel reports

    public final static int TRADEROUTEHAB              = 20;  // number of inhabitants needed for a traderoute
    public final static double POPSUPPORT_MULTI        = 3.0; //multiplier for ship support costs

    public final static int MINBASEPOINTS              = 120; //minimum points needed for outpost building for the AI

    public final static int MIN_TROOPS_FOR_INVASION    = 3;   //minimum number of troops needed for the AI to begin launching an invasion

    public final static int DIPLOMACY_PRESENT_VALUE    = 200; //the minimum value which has to be reached to improve relationship with a minor
    public final static int MAX_RESEARCH_LEVEL         = 14;

    public final static int DIFFERENT_TORPEDOS         = 29;
    public final static int TORPEDO_SPEED              = 15;
    public final static int MAX_TORPEDO_RANGE          = 200;

    public final static int MAX_ALIEN_FREQUENCY        = 60;  //the maximum frequency of alien races

    public final static String UISKIN_PATH             = "ui/uiskin.json";
    public final static String UI_BG_SIMPLE            = "graphics/ui/line.png";
    public final static String UI_BG_ROUNDED           = "graphics/ui/workerselect.png";
    public final static long MIN_MEM_FOR_BACKGROUNDS   = 40 * 1024 * 1024 * 4;

    public final static double ERROR_TOLERANCE         = 10e-12; //minimum tolerance for double comparison

    public static boolean Equals(double is, double should) {
        return should - ERROR_TOLERANCE < is && is < should + ERROR_TOLERANCE;
    }

    public static int setAttributes(boolean is, int attribute, int variable) {
        if (is)
            variable |= attribute;
        else
            variable &= ~attribute;
        return variable;
    }

    public static Rectangle coordsToRelative(float x, float y, float width, float height) {
        width = width / 1440.0f * GamePreferences.sceneWidth;
        height = height / 810.0f * GamePreferences.sceneHeight;
        x = x / 1440.0f * GamePreferences.sceneWidth;
        y = y / 810.0f * GamePreferences.sceneHeight - height;
        return new Rectangle(x, y, width, height);
    }

    public static float hToRelative(float h) {
        return h / 810.0f * GamePreferences.sceneHeight;
    }

    public static float wToRelative(float w) {
        return w / 1440.0f * GamePreferences.sceneWidth;
    }

    public static Rectangle coordsToRelative(Rectangle rect) {
        return coordsToRelative(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }

    public static float distBetween(float x1, float x2, float y1, float y2) {
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static float angleBetween(float x1, float x2, float y1, float y2) {
        return (float) Math.toDegrees(Math.atan2(x2 - x1, y2 - y1));
    }

    public static Vector2 calcBezierPoint(Vector2[] points, float t) {
        Vector2[] tmp = new Vector2[points.length];
        for (int i = 0; i < points.length; i++)
            tmp[i] = points[i].cpy();
        int i = tmp.length - 1;
        while (i > 0) {
            for (int k = 0; k < i; k++) {
                tmp[k].x = tmp[k].x + t * ( tmp[k+1].x - tmp[k].x );
                tmp[k].y = tmp[k].y + t * ( tmp[k+1].y - tmp[k].y );
            }
            i--;
        }
        return tmp[0];
    }

    public static void setLocale(String languageID) { //overrides locale settings
        locale = new Locale(languageID);
    }

    public static String getLocalePrefix() {
        String prefix = "";
        if (!GameConstants.getLocale().getLanguage().equals(new Locale("en").getLanguage()))
            prefix = "/" + GameConstants.getLocale().getLanguage();
        return prefix;
    }

    public static Locale getLocale() {
        if (locale == null)
            locale = Locale.getDefault();
        if (    !( locale.getLanguage().equals(new Locale("de").getLanguage())
                || locale.getLanguage().equals(new Locale("en").getLanguage())
                || locale.getLanguage().equals(new Locale("fr").getLanguage()) /*|| locale.getLanguage().equals(new Locale("ru").getLanguage())*/ ))
            locale = new Locale("en");
        return locale;
    }

    public static String[] getSupportedLanguages() {
        String[] langs = new String[3];
        langs[0] = "en";
        langs[1] = "de";
        langs[2] = "fr";
        return langs;
    }

    public static String getCharset(String locale) {
        if (locale.equals("ru"))
            return "windows-1251";
        return "ISO-8859-1";
    }

    public static String getCharset() {
        return getCharset(getLocalePrefix());
    }

    public static int compare(int x, int y) { //we can't use Integer.compare() because java 6 doesn't has it
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    public static int compare(boolean x, boolean y) {
        return (x == false && y == true) ? -1 : ((x == y) ? 0 : 1);
    }

    public static String getSaveLocation() {
        return Gdx.files.getLocalStoragePath() + "/save/";
    }
}
