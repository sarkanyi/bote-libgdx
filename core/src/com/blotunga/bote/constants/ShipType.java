/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ShipType {
    TRANSPORTER(0, "TRANSPORTER", "TRANSPORTERS"),
    COLONYSHIP(1, "COLONIZESHIP", "COLONIZESHIPS"),
    PROBE(2, "PROBE", "PROBES"),
    SCOUT(3, "SCOUT", "SCOUTS"),
    FIGHTER(4, "FIGHTER", "FIGHTERS"),
    FRIGATE(5, "FRIGATE", "FRIGATES"),
    DESTROYER(6, "DESTROYER", "DESTROYERS"),
    CRUISER(7, "CRUISER", "CRUISERS"),
    HEAVY_DESTROYER(8, "HEAVY_DESTROYER", "HEAVY_DESTROYERS"),
    HEAVY_CRUISER(9, "HEAVY_CRUISER", "HEAVY_CRUISERS"),
    BATTLESHIP(10, "BATTLESHIP", "BATTLESHIPS"),
    DREADNOUGHT(11, "DREADNOUGHT", "DREADNOUGHTS"),
    OUTPOST(12, "OUTPOST", "OUTPOSTS"),
    STARBASE(13, "STARBASE", "STARBASES"),
    ALIEN(14, "ALIEN", "ALIENS");

    private int type;
    private String name;		// name in the stringDB
    private String pluralName;	// plural name in the stringDB
    private static final IntMap<ShipType> intToTypeMap = new IntMap<ShipType>();

    ShipType(int type, String name, String pluralName) {
        this.type = type;
        this.name = name;
        this.pluralName = pluralName;
    }

    static {
        for (ShipType st : ShipType.values()) {
            intToTypeMap.put(st.type, st);
        }
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public static ShipType fromInt(int i) {
        ShipType st = intToTypeMap.get(i);
        return st;
    }
}
