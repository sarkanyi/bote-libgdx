/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;
import com.blotunga.bote.utils.RandUtil;

public enum StarType {
    BLUE_STAR("blue", 0),
    GREEN_STAR("green", 1),
    ORANGE_STAR("orange", 2),
    RED_STAR("red", 3),
    VIOLET_STAR("violet", 4),
    WHITE_STAR("white", 5),
    YELLOW_STAR("yellow", 6);

    private String typeName; //name of the star in the texture pack
    private int type;
    private static final IntMap<StarType> intToTypeMap = new IntMap<StarType>();

    StarType(String typeName, int type) {
        this.typeName = typeName;
        this.type = type;
    }

    static {
        for (StarType st : StarType.values()) {
            intToTypeMap.put(st.type, st);
        }
    }

    public String getName() {
        return "star_" + typeName;
    }

    public String getSunName() {
        return "sun_" + typeName;
    }

    public String getDBName() {
        return typeName.toUpperCase() + "_STAR";
    }

    public int getType() {
        return type;
    }

    public static StarType getNewStarOnChance() {
        return fromInt((int) Math.floor(RandUtil.random() * StarType.values().length));
    }

    public static StarType fromInt(int i) {
        StarType st = intToTypeMap.get(i);
        return st;
    }
}
