/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum Aliens {
    IONISIERENDES_GASWESEN("Ionisierendes Gaswesen"),
    GABALLIANER_SEUCHENSCHIFF("Gaballianer"),
    BLIZZARD_PLASMAWESEN("Blizzard-Plasmawesen"),
    MORLOCK_RAIDER("Morlock-Raider"),
    EELEN_GUARD("Ehlen"),
    BOSEANER("Boseaner"),
    KAMPFSTATION("Kampfstation"),
    KRYONITWESEN("Kryonitwesen"),
    MIDWAY_ZEITREISENDE("Midway-Zeitreisende"),
    ANAEROBE_MAKROBE("Anaerobe Makrobe"),
    ISOTOPOSPHAERISCHES_WESEN("Isotoposphaerisches Wesen");

    private String id;

    Aliens(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
