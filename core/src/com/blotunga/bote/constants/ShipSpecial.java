/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ShipSpecial {
    NONE(0),
    ASSAULTSHIP(1),
    BLOCKADESHIP(2),
    COMMANDSHIP(3),
    COMBATTRACTORBEAM(4),
    DOGFIGHTER(5),
    DOGKILLER(6),
    PATROLSHIP(7),
    RAIDER(8),
    SCIENCEVESSEL(9);

    int type;
    private static final IntMap<ShipSpecial> intToTypeMap = new IntMap<ShipSpecial>();

    ShipSpecial(int type) {
        this.type = type;
    }

    static {
        for (ShipSpecial ss : ShipSpecial.values()) {
            intToTypeMap.put(ss.type, ss);
        }
    }

    public int getType() {
        return type;
    }

    public static ShipSpecial fromInt(int i) {
        ShipSpecial ss = intToTypeMap.get(i);
        return ss;
    }
}
