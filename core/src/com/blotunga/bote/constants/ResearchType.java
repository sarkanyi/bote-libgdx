/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ResearchType {
    NONE(-1, -1, "", "", "", ""),
    BIO(0, 0, "BIO_FINISHED", "biotech", "BIOTECH_SHORT", "BIOTECH"),
    ENERGY(1, 1, "ENERGY_FINISHED", "energytech", "ENERGYTECH_SHORT", "ENERGYTECH"),
    COMPUTER(2, 2, "COMPUTER_FINISHED", "computertech", "COMPUTERTECH_SHORT", "COMPUTERTECH"),
    CONSTRUCTION(3, 4, "CONSTRUCTION_FINISHED", "constructiontech", "CONSTRUCTIONTECH_SHORT", "CONSTRUCTIONTECH"),
    PROPULSION(4, 3, "PROPULSION_FINISHED", "propulsiontech", "PROPULSIONTECH_SHORT", "PROPULSIONTECH"),
    WEAPON(5, 5, "WEAPON_FINISHED", "weapontech", "WEAPONTECH_SHORT", "WEAPONTECH"),
    UNIQUE(6, 6, "SPECIAL_FINISHED", "specialtech", "", "");

    private int idx; //used for display
    private int type; //type used in most places
    private String name; //stringDB entry
    private String imgName;
    private String shortName;
    private String key;
    private static final IntMap<ResearchType> intToTypeMap = new IntMap<ResearchType>();
    private static final IntMap<ResearchType> idxMap = new IntMap<ResearchType>();

    ResearchType(int idx, int type, String name, String imgName, String shortName, String key) {
        this.idx = idx;
        this.type = type;
        this.name = name;
        this.imgName = imgName;
        this.shortName = shortName;
        this.key = key;
    }

    static {
        for (ResearchType rt : ResearchType.values()) {
            intToTypeMap.put(rt.type, rt);
        }
    }
    static {
        for (ResearchType rt : ResearchType.values()) {
            idxMap.put(rt.idx, rt);
        }
    }

    public int getType() {
        return type;
    }

    public int getIndex() {
        return idx;
    }

    public String getName() {
        return name;
    }

    public String getImgName() {
        return imgName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getKey() {
        return key;
    }

    public static ResearchType fromIdx(int i) {
        ResearchType rt = idxMap.get(i);
        return rt;
    }

    public static ResearchType fromType(int i) {
        ResearchType rt = intToTypeMap.get(i);
        return rt;
    }
}
