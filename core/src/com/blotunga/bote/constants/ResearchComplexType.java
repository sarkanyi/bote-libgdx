/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ResearchComplexType {
    NONE(-1),
    WEAPONS_TECHNOLOGY(0),
    CONSTRUCTION_TECHNOLOGY(1),
    GENERAL_SHIP_TECHNOLOGY(2),
    PEACEFUL_SHIP_TECHNOLOGY(3),
    TROOPS(4),
    ECONOMY(5),
    PRODUCTION(6),
    DEVELOPMENT_AND_SECURITY(7),
    RESEARCH(8),
    SECURITY(9),
    STORAGE_AND_TRANSPORT(10),
    TRADE(11),
    FINANCES(12),
    COMPLEX_COUNT(13); //for looping

    private int type;
    private static final IntMap<ResearchComplexType> intToTypeMap = new IntMap<ResearchComplexType>();

    ResearchComplexType(int type) {
        this.type = type;
    }

    static {
        for (ResearchComplexType ct : ResearchComplexType.values()) {
            intToTypeMap.put(ct.type, ct);
        }
    }

    public int getType() {
        return type;
    }

    public static ResearchComplexType fromInt(int i) {
        ResearchComplexType ct = intToTypeMap.get(i);
        if (ct == null)
            return ResearchComplexType.NONE;
        return ct;
    }
}
