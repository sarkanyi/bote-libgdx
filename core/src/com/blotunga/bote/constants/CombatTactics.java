/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum CombatTactics {
    CT_ATTACK(0, "ATTACK_ORDER", "tactic_attack"),
    CT_AVOID(1, "AVOID_ORDER", "tactic_avoid"),
    CT_RETREAT(2, "", "tactic_retreat");

    private int type;
    private String name; // StringDB entry
    private String imgName;

    CombatTactics(int type, String name, String imgName) {
        this.type = type;
        this.name = name;
        this.imgName = imgName;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getImgName() {
        return imgName;
    }
}
