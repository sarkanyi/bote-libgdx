/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ResourceTypes {
    TITAN(0, "TITAN", "titanSmall"),
    DEUTERIUM(1, "DEUTERIUM", "deuteriumSmall"),
    DURANIUM(2, "DURANIUM", "duraniumSmall"),
    CRYSTAL(3, "CRYSTAL", "crystalSmall"),
    IRIDIUM(4, "IRIDIUM", "iridiumSmall"),
    DERITIUM(5, "DERITIUM", "Deritium"),
    FOOD(6, "FOOD", "foodSmall");

    private int type;
    private String name;
    private String imgName;
    private static final IntMap<ResourceTypes> intToTypeMap = new IntMap<ResourceTypes>();

    ResourceTypes(int type, String name, String imgName) {
        this.type = type;
        this.name = name;
        this.imgName = imgName;
    }

    static {
        for (ResourceTypes rt : ResourceTypes.values()) {
            intToTypeMap.put(rt.type, rt);
        }
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getImgName() {
        return imgName;
    }

    public static ResourceTypes fromResourceTypes(int i) {
        ResourceTypes rt = intToTypeMap.get(i);
        return rt;
    }
}
