/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum AnomalyType {
    // nebulas
    RADIONEBULA(0, "RadioNebula", "RADIONEBULA"),
    METNEBULA(1, "MetNebula", "METNEBULA"),
    DEUTNEBULA(2, "DeutNebula", "DEUTNEBULA"),
    IONSTORM(3, "Ionstorm", "IONSTORM"),
    BINEBULA(4, "BiNebula", "BINEBULA"),
    TORIONGASNEBULA(5, "ToriongasNebula", "TORIONGASNEBULA"),

    // neutron stars
    NEUTRONSTAR(6, "NeutronStar", "NEUTRONSTAR"),
    RADIOPULSAR(7, "RadioPulsar", "RADIOPULSAR"),
    XRAYPULSAR(8, "XRayPulsar", "XRAYPULSAR"),
    MAGNETAR(9, "Magnetar", "MAGNETAR"),

    // distortions
    GRAVDISTORTION(10, "GravDistortion", "GRAVDISTORTION"),
    CONTINUUMRIP(11, "ContinuumRip", "CONTINUUMRIP"),
    BLACKHOLE(12, "BlackHole", "BLACKHOLE"),

    //other
    QUASAR(13, "Quasar", "QUASAR"),
    WORMHOLE(14, "Wormhole", "WORMHOLE");

    private String typeName;
    private String mapName;
    private int type;
    private static final IntMap<AnomalyType> intToTypeMap = new IntMap<AnomalyType>();

    AnomalyType(int type, String typeName, String mapName) {
        this.type = type;
        this.typeName = typeName;
        this.mapName = mapName;
    }

    static {
        for (AnomalyType at : AnomalyType.values()) {
            intToTypeMap.put(at.type, at);
        }
    }

    public String getName() {
        return typeName;
    }

    public String getMapName() {
        return mapName;
    }

    public static AnomalyType fromInt(int i) {
        AnomalyType at = intToTypeMap.get(i);
        return at;
    }
}
