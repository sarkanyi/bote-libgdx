/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum WorkerType {
    NONE(-1, null, "", ""),
    FOOD_WORKER(0, ResourceTypes.FOOD, "foodSmall", "FOOD"),
    INDUSTRY_WORKER(1, null, "industrySmall", "INDUSTRY"),
    ENERGY_WORKER(2, null, "energySmall", "ENERGY"),
    SECURITY_WORKER(3, null, "securitySmall", "SECURITY"),
    RESEARCH_WORKER(4, null, "researchSmall", "RESEARCH"),
    TITAN_WORKER(5, ResourceTypes.TITAN, "titanSmall", "TITAN"),
    DEUTERIUM_WORKER(6, ResourceTypes.DEUTERIUM, "deuteriumSmall", "DEUTERIUM"),
    DURANIUM_WORKER(7, ResourceTypes.DURANIUM, "duraniumSmall", "DURANIUM"),
    CRYSTAL_WORKER(8, ResourceTypes.CRYSTAL, "crystalSmall", "CRYSTAL"),
    IRIDIUM_WORKER(9, ResourceTypes.IRIDIUM, "iridiumSmall", "IRIDIUM"),
    ALL_WORKER(10, null, "", ""),
    FREE_WORKER(11, null, "", "");

    private int type;
    private ResourceTypes res;
    private String graphic;
    private String dbString;
    private static final IntMap<WorkerType> intToTypeMap = new IntMap<WorkerType>();

    WorkerType(int type, ResourceTypes res, String graphic, String dbString) {
        this.type = type;
        this.res = res;
        this.graphic = graphic;
        this.dbString = dbString;
    }

    static {
        for (WorkerType wt : WorkerType.values()) {
            intToTypeMap.put(wt.type, wt);
        }
    }

    public int getType() {
        return type;
    }

    public ResourceTypes getResource() {
        return res;
    }

    public String getGraphic() {
        return graphic;
    }

    public static WorkerType fromWorkerType(int i) {
        WorkerType wt = intToTypeMap.get(i);
        return wt;
    }

    public String getDBString() {
        return dbString;
    }
}
