/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.graphics.Color;

/**
 * @author dragon
 * The number represents the upper limit for the range so for example 0-24 we have WEAK
 */
public enum ScanPower {
    NONE(0),
    VERY_WEAK(25),
    WEAK(50),
    MEDIUM(75),
    HIGH(100),
    VERY_HIGH(125),
    MAX(Integer.MAX_VALUE);

    private int limit;
    ScanPower(int limit) {
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }

    public static Color getColor(int power, float alpha) {
        Color color = new Color(245 / 255.0f, 0, 0, alpha);
        if (power >= VERY_HIGH.limit)
            color = new Color(0, 250 / 255.0f, 0, alpha);
        else if (power >= HIGH.limit)
            color = new Color(50 / 255.0f, 200 / 255.0f, 0 / 255.0f, alpha);
        else if (power >= MEDIUM.limit)
            color = new Color(100 / 255.0f, 150 / 255.0f, 0 / 255.0f, alpha);
        else if (power >= WEAK.limit)
            color = new Color(230 / 255.0f, 230 / 255.0f, 20 / 255.0f, alpha);
        else if (power >= VERY_WEAK.limit)
            color = new Color(255 / 255.0f, 165 / 255.0f, 0 / 255.0f, alpha);
        else if (power > NONE.limit)
            color = new Color(230 / 255.0f, 100 / 255.0f, 0, alpha);
        return color;
    }

    public static String getName(int power) {
        String name = "SCAN_NONE";
        if (power >= VERY_HIGH.limit)
            name = "SCAN_MAX";
        else if (power >= HIGH.limit)
            name = "SCAN_VERY_HIGH";
        else if (power >= MEDIUM.limit)
            name = "SCAN_HIGH";
        else if (power >= WEAK.limit)
            name = "SCAN_MEDIUM";
        else if (power >= VERY_WEAK.limit)
            name = "SCAN_WEAK";
        else if (power > NONE.limit)
            name = "SCAN_VERY_WEAK";
        return name;
    }
}
