/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.trade;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.blotunga.bote.constants.ResourceTypes;

/**
 * @author dragon
 * @version 1
 */
public class TradeHistory {
    private Array<IntArray> prices;

    public TradeHistory() {
        prices = new Array<IntArray>(5);
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++)
            prices.add(new IntArray());
    }

    /**
     * Function returns all prices of all goods
     * @return
     */
    public Array<IntArray> getHistoryPrices() {
        return prices;
    }

    /**
     * Function returns the price history of the resource specified
     * @param res
     * @return
     */
    public IntArray getHistoryPricesFromRes(int res) {
        return prices.get(res);
    }

    /**
     * Function save the current prices in the arraylist
     * @param newprices
     * @param currentTax
     */
    public void saveCurrentPrices(int[] newprices, float currentTax) {
        IntArray items;
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            items = prices.get(i);
            items.add((int) Math.ceil(newprices[i] * currentTax));
        }
    }

    public int getMinPrice(int res) {
        int min = Integer.MAX_VALUE;
        for (int j = 0; j < prices.get(res).size; j++) {
            int i = prices.get(res).get(j);
            if (i < min)
                min = i;
        }
        return min;
    }

    public int getMaxPrice(int res) {
        return getMaxPrice(res, 0, 0);
    }

    public int getMaxPrice(int res, int start, int end) {
        int max = 0;
        if (start == 0 && end == 0) {
            for (int j = 0; j < prices.get(res).size; j++) {
                int i = prices.get(res).get(j);
                if (i > max)
                    max = i;
            }
        } else {
            if (end < prices.get(res).size)
                for (int i = start; i <= end; i++)
                    if (prices.get(res).get(i) > max)
                        max = prices.get(res).get(i);
        }
        return max;
    }

    public int getAveragePrice(int res) {
        double price = 0.0;
        for (int j = 0; j < prices.get(res).size; j++)
            price += prices.get(res).get(j);
        if (prices.get(res).size > 0)
            price /= prices.get(res).size;
        return (int) price;
    }

    public void reset() {
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++)
            prices.get(i).clear();
    }
}
