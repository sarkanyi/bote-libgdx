/*
 * Copyright (C) 2014-2017 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.races.Race.RaceSpecialAbilities;
import com.blotunga.bote.races.Race.RaceType;

public class MinorRaceEditor {
    class RaceInfo {
        String name;
        int id;

        public RaceInfo(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RaceInfo)
                return ((RaceInfo) obj).id == id;
            return false;
        }
    }

    private Skin skin;
    private ScreenManager game;
    private Stage stage;
    private ScrollPane minorScroller;
    private Table minorTable;
    private TextButtonStyle styleSmall;
    private TextureRegion selectTexture;
    private Color normalColor = Color.WHITE;
    private Color markColor = Color.CYAN;
    private Color oldColor;
    private Button minorSelection = null;
    private int selectedItem = -1;
    private Table minorInfoTable;
    private Array<Button> minorItems;
    private Minor selectedMinor;
    private String loadedTexture;
    private TextField minorFilter;
    private Table mainButtonTable;
    private boolean visible;
    private String selectedLanguage;
    private ArrayMap<String, ArrayMap<String, Minor>> localeMinorInfos;
    private Minor localeMinorInfo;

    public MinorRaceEditor(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        this.stage = stage;

        selectedLanguage = GameConstants.getLocale().getLanguage();
        localeMinorInfos = new ArrayMap<String, ArrayMap<String, Minor>>();
        for (String lang : GameConstants.getSupportedLanguages()) {
            RaceController ctrl = new RaceController(game);
            localeMinorInfos.put(lang, ctrl.initMinorsAndAliensFromFile("/" + lang));
        }

        minorTable = new Table();
        minorTable.align(Align.top);
        minorScroller = new ScrollPane(minorTable, skin);
        minorScroller.setVariableSizeKnobs(false);
        minorScroller.setScrollingDisabled(true, false);
        stage.addActor(minorScroller);
        Rectangle rect = GameConstants.coordsToRelative(5, 770, 260, 725);
        minorScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        minorTable.align(Align.topLeft);
        minorScroller.setVisible(false);

        minorFilter = new TextField("", skin);
        rect = GameConstants.coordsToRelative(5, 805, 260, 30);
        minorFilter.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        minorFilter.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (textField.isVisible()) {
                    for (int i = 0; i < game.getRaceController().getMinors().size; i++) {
                        Minor minor = game.getRaceController().getMinors().getValueAt(i);
                        if (minor.getName().toLowerCase().contains(textField.getText().toLowerCase())) {
                            selectedItem = i;
                            show();
                            break;
                        }
                    }
                }
            }
        });
        stage.addActor(minorFilter);

        mainButtonTable = new Table();
        mainButtonTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(5, 40, 1430, 35);
        mainButtonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(mainButtonTable);

        styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        loadedTexture = "";
        selectTexture = manager.getUiTexture("listselect");

        minorInfoTable = new Table();
        rect = GameConstants.coordsToRelative(270, 805, 1130, 710);
        stage.addActor(minorInfoTable);
        minorInfoTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        minorInfoTable.align(Align.topLeft);
        minorInfoTable.setVisible(false);

        minorItems = new Array<Button>();
        stage.setKeyboardFocus(minorTable);

        visible = false;
    }

    protected void saveMinorInfosToFile(boolean aliens) {
        String pfx = aliens ? "alienentities" : "minor";
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle statHandle = Gdx.files.absolute(pathPrefix + "/data/races/" + pfx + "statlist.txt");
        FileHandle defaultInfoHandle = Gdx.files.absolute(pathPrefix + "/data/races/" + pfx + "infolist.txt");
        int numLangs = GameConstants.getSupportedLanguages().length;
        FileHandle[] langSpecificHandle = new FileHandle[numLangs];
        for (int i = 0; i < numLangs; i++) {
            String lng = GameConstants.getSupportedLanguages()[i];
            if (lng.equals("en"))
                langSpecificHandle[i] = defaultInfoHandle;
            else
                langSpecificHandle[i] = Gdx.files.absolute(pathPrefix + "/data/" + lng + "/races/" + pfx + "infolist.txt");
        }
        String statOut = "";
        String[] infoOut = new String[numLangs];
        Arrays.fill(infoOut, "");
        Minor[] minors = new Minor[game.getRaceController().getMinors().size];
        for(int i = 0; i < game.getRaceController().getMinors().size; i++) {
            minors[i] = game.getRaceController().getMinors().getValueAt(i);
        }
        Arrays.sort(minors);

        for (int i = 0; i < minors.length; i++) {
            Minor minor = (Minor)minors[i];
            if ((!aliens && !minor.isAlien()) || (aliens && minor.isAlien())) {
                statOut += minor.getRaceId() + ":" + "\n";
                for (int j = 0; j < numLangs; j++) {
                    infoOut[j] += minor.getRaceId() + ":" + "\n";
                    infoOut[j] += localeMinorInfos.get(GameConstants.getSupportedLanguages()[j]).get(minor.getRaceId()).getName() + "\n";
                    infoOut[j] += localeMinorInfos.get(GameConstants.getSupportedLanguages()[j]).get(minor.getRaceId()).getDescription() + "\n";
                }
                statOut += minor.getGraphicFileName() + ".jpg" + "\n";
                statOut += minor.getTechnologicalProgress() + "\n";
                boolean hasProperty = false;
                String text = "";
                for (int j = 1; j <= RaceProperty.HOSTILE.getType(); j++) {
                    RaceProperty prop = RaceProperty.fromInt(j);
                    if (minor.isRaceProperty(prop)) {
                        hasProperty = true;
                        if (!text.isEmpty())
                            text += ",";
                        text += prop.getType();
                    }
                }
                if (!hasProperty)
                    text = "0";
                statOut += text + "\n";
                if (minor.isAlien())
                    statOut += minor.getSpecialAbility() + "\n";
                statOut += (minor.getSpaceFlightNation() ? 1 : 0) + "\n";
                statOut += minor.getCorruptibility() + "\n";
            }
        }
        statHandle.writeString(statOut, false, "ISO-8859-1");
        for (int j = 0; j < numLangs; j++)
            langSpecificHandle[j].writeString(infoOut[j], false,
                    GameConstants.getCharset(GameConstants.getSupportedLanguages()[j]));
    }

    public void show() {
        visible = true;
        show(true, true);
    }

    public void show(boolean resetSelectedminor, boolean resetminorTable) {
        if (resetminorTable) {
            minorItems.clear();
            minorTable.clear();
            minorTable.addListener(new InputListener() {
                @Override
                public boolean keyDown(InputEvent event, final int keycode) {
                    Button b = minorItems.get(selectedItem);
                    switch (keycode) {
                        case Keys.DOWN:
                            selectedItem++;
                            break;
                        case Keys.UP:
                            selectedItem--;
                            break;
                        case Keys.HOME:
                            selectedItem = 0;
                            break;
                        case Keys.END:
                            selectedItem = minorItems.size - 1;
                            break;
                        case Keys.PAGE_DOWN:
                            selectedItem += minorScroller.getScrollHeight() / b.getHeight();
                            break;
                        case Keys.PAGE_UP:
                            selectedItem -= minorScroller.getScrollHeight() / b.getHeight();
                            break;
                    }

                    if (MinorRaceEditor.this.isVisible()) {
                        if (selectedItem >= minorItems.size)
                            selectedItem = minorItems.size - 1;
                        if (selectedItem < 0)
                            selectedItem = 0;

                        b = minorItems.get(selectedItem);
                        markminorListSelected(b);

                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                show();
                            }
                        });
                        Thread th = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(150);
                                } catch (InterruptedException e) {
                                }
                                if (Gdx.input.isKeyPressed(keycode)) {
                                    Gdx.app.postRunnable(new Runnable() {
                                        @Override
                                        public void run() {
                                            InputEvent event = new InputEvent();
                                            event.setType(Type.keyDown);
                                            event.setKeyCode(keycode);
                                            minorTable.fire(event);
                                        }
                                    });
                                }
                            }
                        };
                        th.start();
                    }

                    return false;
                }
            });

            for (int i = 0; i < localeMinorInfos.get(selectedLanguage).size; i++) {
                Minor lminor = localeMinorInfos.get(selectedLanguage).getValueAt(i);
                Button.ButtonStyle bs = new Button.ButtonStyle();
                Button button = new Button(bs);
                Color color = normalColor;
                Label l = new Label(lminor.getName(), skin, "mediumFont", Color.WHITE);
                l.setColor(color);
                l.setUserObject(new Integer(i));
                l.setEllipsis(true);
                button.add(l).width(minorScroller.getWidth());
                button.setUserObject(l);
                ActorGestureListener gestureListener = new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        Button b = (Button) event.getListenerActor();
                        markminorListSelected(b);
                        selectedItem = getIndex(b);
                        show(true, false);
                        stage.setKeyboardFocus(minorTable);
                    }
                };
                button.addListener(gestureListener);

                if (lminor.getName().toLowerCase().contains(minorFilter.getText().toLowerCase())) {
                    minorTable.add(button).align(Align.left);
                    minorItems.add(button);
                    minorTable.row();
                }
            }
        }
        minorScroller.setVisible(true);
        minorFilter.setVisible(true);
        drawMainButtonTable();

        Button btn = null;
        if (selectedItem != -1) {
            for (Button b : minorItems) {
                if (selectedItem == getIndex(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (minorItems.size > 0) {
                btn = minorItems.get(0);
                selectedItem = getIndex(btn);
            }
        }

        showminorInfo(resetSelectedminor);
        stage.draw();

        if (btn != null)
            markminorListSelected(btn);
    }

    private void showminorInfo(boolean resetSelectedminor) {
        if (selectedItem != -1) {
            if (resetSelectedminor) {
                selectedMinor = new Minor(game.getRaceController().getMinors().getValueAt(selectedItem));
                localeMinorInfo = new Minor(localeMinorInfos.get(selectedLanguage).getValueAt(selectedItem));
            }
            Minor minor = selectedMinor;
            drawMinorInfo(minor);
            minorInfoTable.setVisible(true);
        }
    }

    private void drawMinorInfo(Minor minor) {
        minorInfoTable.clear();
        Table table = new Table();
        Table container = new Table();
        TextField tf = new TextField(localeMinorInfo.getName(), skin);
        tf.setName("RACE_NAME");
        container.add(tf).width(GameConstants.wToRelative(200));
        container.row();
        loadedTexture = "graphics/races/" + minor.getGraphicFileName() + ".jpg";
        Image image = new Image(game.loadTextureImmediate(loadedTexture));
        image.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    final TextField tf = new TextField(selectedMinor.getGraphicFileName(), skin);
                    Dialog dialog = new Dialog("Change minor image:", skin) {
                        protected void result(Object object) {
                            if ((Boolean) object) {
                                selectedMinor.setGraphicFileName(tf.getText());
                                MinorRaceEditor.this.show(false, false);
                            }
                        }
                    }.text("New image name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                            .key(Keys.ESCAPE, false).show(stage);
                    dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                    dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                            .width(GameConstants.wToRelative(220));
                    dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                    dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                    dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                    dialog.invalidateHierarchy();
                    dialog.invalidate();
                    dialog.layout();
                    dialog.show(stage);
                }
            }
        });
        container.add(image).width(GameConstants.wToRelative(140)).height(GameConstants.hToRelative(80))
                .spaceTop(GameConstants.hToRelative(5));
        table.add(container).width(GameConstants.wToRelative(200)).height(GameConstants.hToRelative(110));

        tf = new TextArea(localeMinorInfo.getDescription(), skin);
        tf.setName("DESCRIPTION");
        table.add(tf).width(GameConstants.wToRelative(920)).height(GameConstants.hToRelative(110))
                .spaceLeft(GameConstants.wToRelative(10));
        minorInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        minorInfoTable.row();

        //second row
        table = new Table();
        Label l = new Label(minor.isAlien() ? "Race id: " : "Homesystem: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(minor.isAlien() ? 80 : 130));
        tf = new TextField(minor.isAlien()  ? minor.getRaceId() : minor.getHomeSystemName() + "", skin);
        tf.setName("RACE_HOME");
        table.add(tf).width(GameConstants.wToRelative(minor.isAlien() ? 190 : 140))
                .spaceRight(GameConstants.wToRelative(30));

        l = new Label("Corruptibility: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(130));
        Array<RaceInfo> corruptibilities = new Array<MinorRaceEditor.RaceInfo>();
        for (int i = 0; i < 5; i++) {
            corruptibilities.add(new RaceInfo(i, Minor.getCorruptibilityAsString(i)));
        }
        SelectBox<RaceInfo> sb = new SelectBox<RaceInfo>(skin);
        sb.setItems(corruptibilities);
        sb.setSelected(corruptibilities.get(corruptibilities.indexOf(
                new RaceInfo(minor.getCorruptibility(), minor.getCorruptibilityAsString()), false)));
        sb.setName("RACE_CORRUPTIBILITY");
        table.add(sb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(100))
                .spaceRight(GameConstants.wToRelative(30));

        l = new Label("Progress: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(90));
        Array<RaceInfo> progresses = new Array<MinorRaceEditor.RaceInfo>();
        for (int i = 0; i < 5; i++) {
            progresses.add(new RaceInfo(i, Minor.getTechnologicalProgressAsString(i)));
        }
        sb = new SelectBox<RaceInfo>(skin);
        sb.setItems(progresses);
        sb.setSelected(progresses.get(progresses.indexOf(
                new RaceInfo(minor.getTechnologicalProgress(), minor.getTechnologicalProgressAsString()), false)));
        sb.setName("RACE_PROGRESS");
        table.add(sb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                .spaceRight(GameConstants.wToRelative(30));

        l = new Label("Spaceflight: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(100));
        CheckBox cb = new CheckBox("", skin);
        cb.setChecked(minor.getSpaceFlightNation());
        cb.setName("RACE_SPACEFLIGHT");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
            .spaceRight(GameConstants.wToRelative(30));

        minorInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5)).align(Align.left);
        minorInfoTable.row();

        table = new Table();
        for (int i = 1; i <= RaceProperty.HOSTILE.getType(); i++) {
            RaceProperty prop = RaceProperty.fromInt(i);
            l = new Label(StringDB.getString(prop.name()) + ": ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(160));
            cb = new CheckBox("", skin);
            cb.setChecked(minor.isRaceProperty(prop));
            cb.setName("RACE_PROPERTY_" + prop.name());
            table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));
            if (i % 4 == 0) {
                minorInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5)).align(Align.left);;
                minorInfoTable.row();
                table = new Table();
            }
        }
        minorInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5)).align(Align.left);
        minorInfoTable.row();

        table = new Table();
        l = new Label("Alien: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        cb = new CheckBox("", skin);
        cb.setChecked(minor.isAlien());
        cb.setName("RACE_ALIEN");
        cb.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                saveMinorInfo();
                show(false, false);
            }
        });
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15)).spaceRight(GameConstants.wToRelative(30));

        container = new Table();
        for (int i = 0; i <= RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.ordinal(); i++) {
            RaceSpecialAbilities ability = RaceSpecialAbilities.values()[i];
            String text = ability.name().substring(8, ability.name().length()).toLowerCase() + ": ";
            l = new Label(text, skin, "mediumFont", normalColor);
            container.add(l).width(GameConstants.wToRelative(140));
            cb = new CheckBox("", skin);
            cb.setChecked(minor.hasSpecialAbility(ability.getAbility()));
            cb.setName("RACE_SPECIAL_" + ability.name());
            container.add(cb).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(30));
        }
        if (minor.isAlien())
            container.setVisible(true);
        else
            container.setVisible(false);

        table.add(container).height(GameConstants.hToRelative(30));
        minorInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5)).align(Align.left);
        minorInfoTable.row();
    }

    private void saveMinorInfo() {
        Minor minor = selectedMinor;
        localeMinorInfo.setName(getTextFieldValue("RACE_NAME"));
        localeMinorInfo.setDescription(getTextFieldValue("DESCRIPTION"));
        minor.setRaceType(getCheckBoxValue("RACE_ALIEN") ? RaceType.RACE_TYPE_ALIEN : RaceType.RACE_TYPE_MINOR);
        String text = getTextFieldValue("RACE_HOME");
        if (!text.isEmpty())
            minor.setRaceID(minor.isAlien() ? text : text.toUpperCase());
        if (!minor.isAlien())
            minor.setHomeSystemName(text);
        minor.setCorruptibility(((RaceInfo) getSelectBoxValue("RACE_CORRUPTIBILITY")).id);
        minor.setTechnologicalProgress(((RaceInfo) getSelectBoxValue("RACE_PROGRESS")).id);
        minor.setSpaceFlight(getCheckBoxValue("RACE_SPACEFLIGHT"));
        for (int i = 1; i <= RaceProperty.HOSTILE.getType(); i++) {
            RaceProperty prop = RaceProperty.fromInt(i);
            minor.setRaceProperty(prop, getCheckBoxValue("RACE_PROPERTY_" + prop.name()));
        }
    }

    private String getTextFieldValue(String name) {
        return ((TextField) minorInfoTable.findActor(name)).getText();
    }

    @SuppressWarnings("rawtypes")
    private Object getSelectBoxValue(String name) {
        return ((SelectBox) minorInfoTable.findActor(name)).getSelected();
    }

    private boolean getCheckBoxValue(String name) {
        return ((CheckBox) minorInfoTable.findActor(name)).isChecked();
    }

    public void hide() {
        visible = false;
        minorFilter.setVisible(false);
        minorScroller.setVisible(false);
        mainButtonTable.setVisible(false);
        minorInfoTable.setVisible(false);
    }

    private void markminorListSelected(Button b) {
        if (minorSelection != null) {
            minorSelection.getStyle().up = null;
            minorSelection.getStyle().down = null;
            ((Label) minorSelection.getUserObject()).setColor(oldColor);
            if (!loadedTexture.isEmpty() && game.getAssetManager().isLoaded(loadedTexture) && selectedItem != getIndex(b)) {
                game.getAssetManager().unload(loadedTexture);
                loadedTexture = "";
            }
        }
        if (b == null)
            b = minorSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        minorSelection = b;
        selectedItem = getIndex(b);
        float buttonPos = b.getHeight() * selectedItem;
        float scrollerHeight = minorScroller.getScrollHeight();
        float scrollerPos = minorScroller.getScrollY();
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0) {
            minorScroller.setScrollY(b.getHeight() * selectedItem - minorScroller.getScrollHeight() * 2);
        } else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight) {
            minorScroller.setScrollY(b.getHeight() * (selectedItem - minorScroller.getScrollHeight() / b.getHeight() + 1));
        }
    }

    private int getIndex(Button b) {
        return (Integer) (((Label) b.getUserObject()).getUserObject());
    }

    private void drawMainButtonTable() {
        mainButtonTable.clear();
        TextButton addBtn = new TextButton("Add", styleSmall);
        addBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Minor minor = new Minor(game);
                int rnd = (int) (Math.random() * 10000);
                minor.setRaceID(String.format("SPECIES%04d", rnd));
                String homeSystem = minor.getRaceId().toLowerCase();
                String firstLetter = homeSystem.substring(0, 1);
                homeSystem = homeSystem.replaceFirst(firstLetter, firstLetter.toUpperCase());
                minor.setHomeSystemName(homeSystem);
                minor.setName("Species " + rnd);
                game.getRaceController().getMinors().put(minor.getRaceId(), minor);
                for (String lang : GameConstants.getSupportedLanguages())
                    localeMinorInfos.get(lang).put(minor.getRaceId(), minor);
                selectedItem = game.getRaceController().getMinors().size - 1;
                show();
            }
        });
        mainButtonTable.add(addBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton copyBtn = new TextButton("Copy", styleSmall);
        copyBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Minor minor = new Minor(game.getRaceController().getMinors().getValueAt(selectedItem));
                int rnd = (int) (Math.random() * 10000);
                minor.setRaceID(String.format("SPECIES%04d", rnd));
                String homeSystem = minor.getRaceId().toLowerCase();
                String firstLetter = homeSystem.substring(0, 1);
                homeSystem = homeSystem.replaceFirst(firstLetter, firstLetter.toUpperCase());
                minor.setHomeSystemName(homeSystem);
                game.getRaceController().getMinors().put(minor.getRaceId(), minor);
                for (String lang : GameConstants.getSupportedLanguages())
                    localeMinorInfos.get(lang).put(minor.getRaceId(), localeMinorInfos.get(lang).get(game.getRaceController().getMinors().getValueAt(selectedItem).getRaceId()));
                selectedItem = game.getRaceController().getMinors().size - 1;
                show();
            }
        });
        mainButtonTable.add(copyBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton removeBtn = new TextButton("Remove", styleSmall);
        removeBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                game.getRaceController().getMinors().removeIndex(selectedItem);
                for (String lang : GameConstants.getSupportedLanguages()) {
                    localeMinorInfos.get(lang).removeIndex(selectedItem);
                }
                show();
            }
        });
        mainButtonTable.add(removeBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonUpdate = new TextButton("Update", styleSmall);
        buttonUpdate.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                saveMinorInfo();
                String newID = selectedMinor.getRaceId();
                String oldID = game.getRaceController().getMinors().getKeyAt(selectedItem); 
                game.getRaceController().getMinors().removeIndex(selectedItem);
                Minor minor = new Minor(selectedMinor);
                game.getRaceController().getMinors().insert(selectedItem, newID, minor);
                localeMinorInfos.get(selectedLanguage).removeIndex(selectedItem);
                minor = new Minor(localeMinorInfo);
                localeMinorInfos.get(selectedLanguage).insert(selectedItem, newID, minor);
                if (!oldID.equals(newID)) {
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        if(!lang.equals(selectedLanguage)) {
                            minor = localeMinorInfos.get(lang).getValueAt(selectedItem);
                            localeMinorInfos.get(lang).removeIndex(selectedItem);
                            localeMinorInfos.get(lang).insert(selectedItem, newID, minor);
                        }
                    }
                }
                show();
            }
        });
        mainButtonTable.add(buttonUpdate).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonSave = new TextButton("Save", styleSmall);
        buttonSave.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                saveMinorInfosToFile(false);
                saveMinorInfosToFile(true);
            }
        });
        mainButtonTable.add(buttonSave).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        SelectBox<String> languageSelect = new SelectBox<String>(skin);
        languageSelect.setItems(GameConstants.getSupportedLanguages());
        languageSelect.setSelected(selectedLanguage);
        languageSelect.addListener(new ChangeListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                selectedLanguage = ((SelectBox<String>) actor).getSelected();
                show();
            }
        });
        mainButtonTable.add(languageSelect).width(GameConstants.wToRelative(100)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((ResourceEditor) game.getScreen()).showMainMenu();
            }
        });
        mainButtonTable.add(buttonBack).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceLeft(GameConstants.wToRelative(5)).align(Align.right).expand();
        mainButtonTable.setVisible(true);
    }

    public boolean isVisible() {
        return visible;
    }
}