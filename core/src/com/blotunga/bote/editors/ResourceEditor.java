/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import java.nio.IntBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip.TextTooltipStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.editors.ShipEditor.ShipEditorState;
import com.blotunga.bote.utils.ui.ZoomWithLock;

public class ResourceEditor extends DefaultScreen {
    public enum EditorState {
        MainMenu(0x10),
        ShipEditor(0x20),
        BuildingEditor(0x40),
        TroopEditor(0x80),
        MinorRaceEditor(0x100);

        private int mask;

        private EditorState(int mask) {
            this.mask = mask;
        }

        public int getMask() {
            return mask;
        }
    }

    private final OrthographicCamera camera;
    private final ZoomWithLock inputs;
    private final Texture texture;
    private final Stage stage;
    private final Skin skin;
    private final Image backImg;
    private String extraTexture;
    private EditorMain editorMain;
    private ShipEditor shipEditor;
    private BuildingEditor buildingEditor;
    private TroopEditor troopEditor;
    private MinorRaceEditor minorRaceEditor;
    private EditorState state;

    public ResourceEditor(ScreenManager screenManager) {
        super(screenManager);

        camera = new OrthographicCamera();
        stage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));

        Rectangle rect = GameConstants.coordsToRelative(1390, 810, 50, 50);
        inputs = new ZoomWithLock(game, camera, stage, rect);
        Gdx.input.setInputProcessor(inputs);

        texture = game.getAssetManager().get("graphics/events/Startmenu.jpg");

        skin = screenManager.getAssetManager().get(GameConstants.UISKIN_PATH);
        fixFonts();
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);

        style.font = screenManager.getScaledFont();
        TextButtonStyle disabledStyle = new TextButtonStyle(style);
        disabledStyle.fontColor = Color.BLACK;
        disabledStyle.downFontColor = Color.BLACK;
        disabledStyle.down = style.up;
        skin.add("default-disabled", disabledStyle);
        ScrollPaneStyle st = new ScrollPaneStyle();
        Sprite sp = new Sprite(screenManager.getUiTexture("sidescroll"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScroll = new SpriteDrawable(sp);
        sp = new Sprite(screenManager.getUiTexture("shippath"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScrollKnob = new SpriteDrawable(sp);
        skin.add("default", st);

        backImg = new Image(new TextureRegionDrawable(new TextureRegion(texture)));
        backImg.setBounds(0, 0, GamePreferences.sceneWidth, GamePreferences.sceneHeight);
        stage.addActor(backImg);

        stage.addActor(inputs.getImage()); //this has to be added after all the background images
        extraTexture = "";

        BitmapFont normalFont = ((ScreenManager) game).getScaledFont();
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("menuFont", normalFont, BitmapFont.class);
        TextButtonStyle styleSmall = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        styleSmall.font = normalFont;
        skin.add("small-buttons", styleSmall);

        editorMain = new EditorMain(screenManager, stage, skin);
        shipEditor = new ShipEditor(screenManager, stage, skin);
        buildingEditor = new BuildingEditor(screenManager, stage, skin);
        troopEditor = new TroopEditor(screenManager, stage, skin);
        minorRaceEditor = new MinorRaceEditor(screenManager, stage, skin);
        state = EditorState.MainMenu;
    }

    public void showShipEditor() {
        hideAll();
        backImg.setColor(new Color(0.2f, 0.2f, 0.2f, 0.8f));
        state = EditorState.ShipEditor;
        shipEditor.show();
    }

    public void showBuildingEditor() {
        hideAll();
        backImg.setColor(new Color(0.2f, 0.2f, 0.2f, 0.8f));
        state = EditorState.BuildingEditor;
        buildingEditor.show();
    }

    public void showTroopEditor() {
        hideAll();
        backImg.setColor(new Color(0.2f, 0.2f, 0.2f, 0.8f));
        state = EditorState.TroopEditor;
        troopEditor.show();
    }

    public void showMinorRaceEditor() {
        hideAll();
        backImg.setColor(new Color(0.2f, 0.2f, 0.2f, 0.8f));
        state = EditorState.MinorRaceEditor;
        minorRaceEditor.show();
    }

    public void hideAll() {
        backImg.setColor(Color.WHITE);
        editorMain.hide();
        shipEditor.hide();
        buildingEditor.hide();
        troopEditor.hide();
        minorRaceEditor.hide();
        inputs.resetPositionAndZoom();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void dispose() {
        Gdx.app.postRunnable(new Runnable() {
            private final Runnable _self = this;
            private int counter = 10;

            @Override
            public void run() {
                if (counter-- > 0) {
                    Gdx.app.postRunnable(_self);
                    return;
                }
                System.out.println("Dispose ResourceEditor");
                if(game.getAssetManager().isLoaded("graphics/events/Startmenu.jpg"))
                    game.getAssetManager().unload("graphics/events/Startmenu.jpg");
                if(!extraTexture.isEmpty() && game.getAssetManager().isLoaded(extraTexture))
                    game.getAssetManager().unload(extraTexture);
                stage.dispose();
                inputs.dispose();
            }
        });
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {
        hide();
        switch (state) {
            case MainMenu:
                editorMain.show();
                break;
            case ShipEditor:
                showShipEditor();
                break;
            case BuildingEditor:
                showBuildingEditor();
                break;
            case TroopEditor:
                showTroopEditor();
                break;
            case MinorRaceEditor:
                showMinorRaceEditor();
                break;
        }
        IntBuffer buf = BufferUtils.newIntBuffer(16);
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buf);
        final int maxSize = buf.get(0);
        game.Init(maxSize);
        game.setGameLoaded("");
    }

    public void showMainMenu() {
        state = EditorState.MainMenu;
        show();
    }

    @Override
    public void hide() {
        hideAll();
    }

    @Override
    public void setSubMenu(int subMenu) {
        if ((subMenu & EditorState.ShipEditor.getMask()) > 0) {
            shipEditor.setState(ShipEditorState.fromMask(subMenu));
            showShipEditor();
        }
    }

    private void fixFonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/DroidSans.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();

        param.size = 16;
        param.genMipMaps = true;
        BitmapFont uiFont = generator.generateFont(param);
        uiFont.setUseIntegerPositions(true);
        param.size = 18;
        BitmapFont mediumFont = generator.generateFont(param);
        mediumFont.setFixedWidthGlyphs("1234567890");
        mediumFont.setUseIntegerPositions(true);
        skin.add("mediumFont", mediumFont, BitmapFont.class);
        param.size = 21;
        BitmapFont normalFont = generator.generateFont(param);
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("normalFont", normalFont, BitmapFont.class);
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
        skin.remove("default-font", BitmapFont.class);
        skin.add("default-font", uiFont, BitmapFont.class);
        skin.get("default", TextButtonStyle.class).font = uiFont;
        skin.get("toggle", TextButtonStyle.class).font = uiFont;
        skin.get("default", SelectBoxStyle.class).font = uiFont;
        skin.get("default", SelectBoxStyle.class).listStyle.font = uiFont;
        skin.get("default", WindowStyle.class).titleFont = uiFont;
        skin.get("dialog", WindowStyle.class).titleFont = uiFont;
        skin.get("default", LabelStyle.class).font = uiFont;
        skin.get("default", TextFieldStyle.class).font = uiFont;
        skin.get("default", CheckBoxStyle.class).font = uiFont;
        skin.get("default", ListStyle.class).font = uiFont;
        skin.get("default", TextTooltipStyle.class).label.font = uiFont;
    }
}