/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.BeamWeapons;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.TorpedoInfo;
import com.blotunga.bote.ships.TorpedoInfo.TorpedoStats;
import com.blotunga.bote.ships.TorpedoWeapons;
import com.blotunga.bote.ui.screens.CombatSimulator;

public class ShipEditor {
    /**
     * Helper class for ManeuverAbility
     */
    class ManeuverAbility {
        int maneuverability;

        public ManeuverAbility(int val) {
            this.maneuverability = val;
        }

        public int toInt() {
            return maneuverability;
        }

        @Override
        public String toString() {
            return ShipInfo.getManeuverabilityAsString(maneuverability);
        }
    }

    /**
     * Helper class for RaceInfo
     */
    class RaceInfo {
        String name;
        int id;

        public RaceInfo(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RaceInfo)
                return ((RaceInfo) obj).id == id;
            return false;
        }
    }

    public enum ShipEditorState {
        StateEditor("Editor", 0x1),
        StateCombatSimulator("Simulator", 0x2);

        private String btnName;
        private int mask;

        private ShipEditorState(String btnName, int mask) {
            this.btnName = btnName;
            this.mask = mask;
        }

        public String getNextBtnName() {
            return this == StateEditor ? StateCombatSimulator.btnName : StateEditor.btnName;
        }

        public int getMask() {
            return mask;
        }

        public static ShipEditorState fromMask(int mask) {
            ShipEditorState state = StateEditor;
            if ((mask & 0x1) == 1)
                state = StateEditor;
            else if ((mask & 0x2) == 2)
                state = StateCombatSimulator;
            return state;
        }
    }

    private Skin skin;
    private ScreenManager game;
    private Stage stage;
    private ScrollPane shipScroller;
    private Table shipTable;
    private TextButtonStyle styleSmall;
    private TextureRegion selectTexture;
    private Color normalColor = Color.WHITE;
    private Color markColor = Color.CYAN;
    private Color oldColor;
    private Color oldWpColor = Color.WHITE;
    private Button shipSelection = null;
    private Button bwListSelected = null;
    private Button twListSelected = null;
    private int selectedItem = -1;
    private int selectedBwItem = -1;
    private int selectedTwItem;
    private Table shipInfoTable;
    private Array<Button> shipItems;
    private Array<Button> bwItems;
    private Array<Button> twItems;
    private ShipInfo selectedShipInfo;
    private String loadedTexture;
    private boolean changeLock = false;
    private String focusItem = "";
    private TextField shipFilter;
    private Table mainButtonTable;
    private ShipEditorState state;
    private IntMap<IntArray> participants;
    private int selectedRace = 0;
    private boolean visible;
    private String selectedLanguage;
    private ArrayMap<String, Array<ShipInfo>> localeShipInfos;
    private ShipInfo localeShipInfo;
    private int simulatorSpeed = 50;
    private int numberOfSimulatorRuns = 1;
    private Anomaly simulatorAnomaly = null;

    public ShipEditor(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        this.stage = stage;
        state = ShipEditorState.StateEditor;
        participants = new IntMap<IntArray>();
        selectedLanguage = GameConstants.getLocale().getLanguage();
        localeShipInfos = new ArrayMap<String, Array<ShipInfo>>();
        for (String lang : GameConstants.getSupportedLanguages())
            localeShipInfos.put(lang, manager.readShipInfosFromFile("/" + lang));

        shipTable = new Table();
        shipTable.align(Align.top);
        shipScroller = new ScrollPane(shipTable, skin);
        shipScroller.setVariableSizeKnobs(false);
        shipScroller.setScrollingDisabled(true, false);
        stage.addActor(shipScroller);
        Rectangle rect = GameConstants.coordsToRelative(5, 770, 260, 725);
        shipScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        shipTable.align(Align.topLeft);
        shipScroller.setVisible(false);

        shipFilter = new TextField("", skin);
        rect = GameConstants.coordsToRelative(5, 805, 260, 30);
        shipFilter.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        shipFilter.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (textField.isVisible()) {
                    for (int i = 0; i < localeShipInfos.get(selectedLanguage).size; i++) {
                        ShipInfo si = localeShipInfos.get(selectedLanguage).get(i);
                        if (si.getShipClass().toLowerCase().contains(textField.getText().toLowerCase())) {
                            selectedItem = i;
                            show();
                            break;
                        }
                    }
                }
            }
        });
        stage.addActor(shipFilter);

        mainButtonTable = new Table();
        mainButtonTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(5, 40, 1430, 35);
        mainButtonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(mainButtonTable);

        styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        loadedTexture = "";
        selectTexture = manager.getUiTexture("listselect");

        shipInfoTable = new Table();
        rect = GameConstants.coordsToRelative(270, 805, 1130, 765);
        stage.addActor(shipInfoTable);
        shipInfoTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        shipInfoTable.align(Align.topLeft);
        shipInfoTable.setVisible(false);
        shipItems = new Array<Button>();
        bwItems = new Array<Button>();
        twItems = new Array<Button>();
        stage.setKeyboardFocus(shipTable);
        visible = false;
    }

    protected void saveShipInfosToFile() {
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle statHandle = Gdx.files.absolute(pathPrefix + "/data/ships/shipstatlist.txt");
        FileHandle defaultInfoHandle = Gdx.files.absolute(pathPrefix + "/data/ships/shipinfolist.txt");
        int numLangs = GameConstants.getSupportedLanguages().length;
        FileHandle[] langSpecificHandle = new FileHandle[numLangs];
        for (int i = 0; i < numLangs; i++) {
            String lng = GameConstants.getSupportedLanguages()[i];
            if (lng.equals("en"))
                langSpecificHandle[i] = defaultInfoHandle;
            else
                langSpecificHandle[i] = Gdx.files.absolute(pathPrefix + "/data/" + lng + "/ships/shipinfolist.txt");
        }
        String statOut = "";
        String[] infoOut = new String[numLangs];
        Arrays.fill(infoOut, "");
        for (int i = 0; i < game.getShipInfos().size; i++) {
            ShipInfo si = game.getShipInfos().get(i);
            statOut += i + "\n";
            for (int j = 0; j < numLangs; j++)
                infoOut[j] += i + "\n";
            statOut += si.getShipImageName() + "\n";
            statOut += si.getRace() + "\n";
            for (int j = 0; j < numLangs; j++) {
                infoOut[j] += localeShipInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getShipClass() + "\n";
                infoOut[j] += localeShipInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getDescription() + "\n";
            }
            statOut += si.getShipType().getType() + "\n";
            statOut += si.getSize().getSize() + "\n";
            statOut += si.getManeuverabilty() + "\n";
            statOut += si.getBioTech() + "\n";
            statOut += si.getEnergyTech() + "\n";
            statOut += si.getCompTech() + "\n";
            statOut += si.getPropulsionTech() + "\n";
            statOut += si.getConstructionTech() + "\n";
            statOut += si.getWeaponTech() + "\n";
            statOut += si.getBaseIndustry() + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.TITAN.getType()) + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.DEUTERIUM.getType()) + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.DURANIUM.getType()) + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.CRYSTAL.getType()) + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.IRIDIUM.getType()) + "\n";
            statOut += si.getNeededResourceBase(ResourceTypes.DERITIUM.getType()) + "\n";
            statOut += si.getOnlyInSystem() + "\n";
            statOut += si.getHull().getBaseHull() + "\n";
            statOut += si.getHull().getHullMaterial() + "\n";
            statOut += (si.getHull().isDoubleHull() ? 1 : 0) + "\n";
            statOut += (si.getHull().isAblative() ? 1 : 0) + "\n";
            statOut += (si.getHull().isPolarisation() ? 1 : 0) + "\n";
            statOut += si.getShield().getMaxShield() + "\n";
            statOut += si.getShield().getShieldType() + "\n";
            statOut += (si.getShield().isRegenerative() ? 1 : 0) + "\n";
            statOut += si.getSpeed() + "\n";
            statOut += si.getRange().getRange() + "\n";
            statOut += si.getScanPower() + "\n";
            statOut += si.getScanRange() + "\n";
            statOut += si.getStealthGrade() + "\n";
            statOut += si.getStorageRoom() + "\n";
            statOut += si.getColonizePoints() + "\n";
            statOut += si.getStationBuildPoints() + "\n";
            statOut += si.getMaintenanceCosts() + "\n";
            statOut += si.getSpecial(0).getType() + "\n";
            statOut += si.getSpecial(1).getType() + "\n";
            statOut += si.getObsoletesClassIdx() + "\n";
            for (TorpedoWeapons tw : si.getTorpedoWeapons()) {
                statOut += "$Torpedo$\n";
                statOut += tw.getTorpedoType() + "\n";
                statOut += tw.getNumber() + "\n";
                statOut += tw.getTubeFirerate() + "\n";
                statOut += tw.getNumberOfTubes() + "\n";
                statOut += tw.getTubeName() + "\n";
                statOut += (tw.isOnlyMicroPhoton() ? 1 : 0) + "\n";
                statOut += tw.getAccuracy() + "\n";
                statOut += tw.getFireArc().getPosition() + "\n";
                statOut += tw.getFireArc().getAngle() + "\n";
            }
            for (BeamWeapons bw : si.getBeamWeapons()) {
                statOut += "$Beam$\n";
                statOut += bw.getBeamType() + "\n";
                statOut += bw.getBeamPower() + "\n";
                statOut += bw.getBeamNumber() + "\n";
                statOut += bw.getBeamName() + "\n";
                statOut += (bw.isModulating() ? 1 : 0) + "\n";
                statOut += (bw.isPiercing() ? 1 : 0) + "\n";
                statOut += bw.getBonus() + "\n";
                statOut += bw.getBeamLength() + "\n";
                statOut += bw.getRechargeTime() + "\n";
                statOut += bw.getShootNumber() + "\n";
                statOut += bw.getFireArc().getPosition() + "\n";
                statOut += bw.getFireArc().getAngle() + "\n";
            }
            statOut += "$END_OF_SHIPDATA$\n";
            for (int j = 0; j < numLangs; j++)
                infoOut[j] += "$END_OF_SHIPDATA$\n";
        }
        statHandle.writeString(statOut, false, "ISO-8859-1");
        for (int j = 0; j < numLangs; j++)
            langSpecificHandle[j].writeString(infoOut[j], false,
                    GameConstants.getCharset(GameConstants.getSupportedLanguages()[j]));
    }

    public void show() {
        visible = true;
        loadParticipants();
        show(true, true);
    }

    public void show(boolean resetSelectedShip, boolean resetShipTable) {
        if (resetShipTable) {
            shipItems.clear();
            shipTable.clear();
            shipTable.addListener(new InputListener() {
                @Override
                public boolean keyDown(InputEvent event, final int keycode) {
                    if (shipItems.size < selectedItem)
                        selectedItem = shipItems.size - 1;
                    Button b = shipItems.get(selectedItem);
                    switch (keycode) {
                        case Keys.DOWN:
                            selectedItem++;
                            break;
                        case Keys.UP:
                            selectedItem--;
                            break;
                        case Keys.HOME:
                            selectedItem = 0;
                            break;
                        case Keys.END:
                            selectedItem = shipItems.size - 1;
                            break;
                        case Keys.PAGE_DOWN:
                            selectedItem += shipScroller.getScrollHeight() / b.getHeight();
                            break;
                        case Keys.PAGE_UP:
                            selectedItem -= shipScroller.getScrollHeight() / b.getHeight();
                            break;
                    }

                    if (ShipEditor.this.isVisible()) {
                        if (selectedItem >= shipItems.size)
                            selectedItem = shipItems.size - 1;
                        if (selectedItem < 0)
                            selectedItem = 0;

                        b = shipItems.get(selectedItem);
                        markShipListSelected(b);
                        selectedBwItem = -1;

                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                show();
                            }
                        });
                        Thread th = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(150);
                                } catch (InterruptedException e) {
                                }
                                if (Gdx.input.isKeyPressed(keycode)) {
                                    Gdx.app.postRunnable(new Runnable() {
                                        @Override
                                        public void run() {
                                            InputEvent event = new InputEvent();
                                            event.setType(Type.keyDown);
                                            event.setKeyCode(keycode);
                                            shipTable.fire(event);
                                        }
                                    });
                                }
                            }
                        };
                        th.start();
                    }

                    return false;
                }
            });

            for (int i = 0; i < localeShipInfos.get(selectedLanguage).size; i++) {
                ShipInfo lsi = localeShipInfos.get(selectedLanguage).get(i);
                ShipInfo si = game.getShipInfos().get(i);
                Button.ButtonStyle bs = new Button.ButtonStyle();
                Button button = new Button(bs);
                Color color = normalColor;
                if (si.getRace() != 255) {
                    //use the racecolor from the normal ship infos
                    Major major = game.getRaceController().getMajors().getValueAt(si.getRace() - 1);
                    color = major.getRaceDesign().clrSector;
                }
                Label l = new Label(String.format("%03d", lsi.getID() - 10000) + ": " + lsi.getShipClass(), skin, "mediumFont",
                        Color.WHITE);
                l.setColor(color);
                l.setUserObject(new Integer(i));
                l.setEllipsis(true);
                button.add(l).width(shipScroller.getWidth());
                button.setUserObject(l);
                ActorGestureListener gestureListener = new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        Button b = (Button) event.getListenerActor();
                        markShipListSelected(b);
                        selectedItem = getIndex(b);
                        selectedBwItem = -1;
                        if (state == ShipEditorState.StateCombatSimulator && count > 1)
                            addToParticipants(selectedItem);
                        show(state == ShipEditorState.StateEditor, false);
                        stage.setKeyboardFocus(shipTable);
                    }
                };
                button.addListener(gestureListener);

                if (lsi.getShipClass().toLowerCase().contains(shipFilter.getText().toLowerCase())) {
                    shipTable.add(button).align(Align.left);
                    shipItems.add(button);
                    shipTable.row();
                }
            }
        }
        shipScroller.setVisible(true);
        shipFilter.setVisible(true);
        drawMainButtonTable();

        Button btn = null;
        if (selectedItem != -1) {
            for (Button b : shipItems) {
                if (selectedItem == getIndex(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (shipItems.size > 0) {
                btn = shipItems.get(0);
                selectedItem = getIndex(btn);
            }
        }

        if (state == ShipEditorState.StateEditor)
            showShipInfo(resetSelectedShip);
        else if (state == ShipEditorState.StateCombatSimulator)
            showCombatSimSetup(resetSelectedShip);

        stage.draw();

        if (btn != null)
            markShipListSelected(btn);
        if (bwListSelected != null)
            markBwListSelected(bwListSelected);
        if (twListSelected != null)
            markTwListSelected(twListSelected);
    }

    private void showShipInfo(boolean resetSelectedShip) {
        shipInfoTable.clear();
        TextFieldListener resourceChangeListener = new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                saveShipInfo();
            }
        };

        if (selectedItem != -1) {
            //first row
            if (resetSelectedShip) {
                selectedShipInfo = new ShipInfo(game.getShipInfos().get(selectedItem));
                localeShipInfo = new ShipInfo(localeShipInfos.get(selectedLanguage).get(selectedItem));
            }
            ShipInfo si = selectedShipInfo;
            Table table = new Table();
            Table container = new Table();
            TextField tf = new TextField(localeShipInfo.getShipClass(), skin);
            tf.setName("SHIPCLASS");
            container.add(tf).width(GameConstants.wToRelative(200));
            container.row();
            loadedTexture = "graphics/ships/" + si.getShipImageName() + ".png";
            Image image = new Image(game.loadTextureImmediate(loadedTexture));
            image.addListener(new ActorGestureListener() {
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (count > 1) {
                        final TextField tf = new TextField(selectedShipInfo.getShipImageName(), skin);
                        Dialog dialog = new Dialog("Change ship image:", skin) {
                            protected void result(Object object) {
                                if ((Boolean) object) {
                                    game.getShipImageNames().put(selectedShipInfo.getID() - 10000, tf.getText());
                                    ShipEditor.this.show(false, false);
                                }
                            }
                        }.text("New image name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                                .key(Keys.ESCAPE, false).show(stage);
                        dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                        dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                                .width(GameConstants.wToRelative(220));
                        dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                        dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                        dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                        dialog.invalidateHierarchy();
                        dialog.invalidate();
                        dialog.layout();
                        dialog.show(stage);
                    }
                }
            });
            container.add(image).width(GameConstants.wToRelative(150));
            table.add(container).width(GameConstants.wToRelative(200)).height(GameConstants.hToRelative(110));

            tf = new TextArea(localeShipInfo.getDescription(), skin);
            tf.setName("DESCRIPTION");
            table.add(tf).width(GameConstants.wToRelative(920)).height(GameConstants.hToRelative(110))
                    .spaceLeft(GameConstants.wToRelative(10));
            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();

            //second row
            table = new Table();
            Label l = new Label("ID: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(35));

            tf = new TextField((si.getID() - 10000) + "", skin);
            tf.setName("SHIP_ID");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(3);
            tf.setTextFieldListener(new TextFieldListener() {
                @Override
                public void keyTyped(TextField textField, char c) {
                    if (!textField.getText().isEmpty()) {
                        int id = Integer.parseInt(textField.getText());
                        if (id >= game.getShipInfos().size)
                            textField.setText((game.getShipInfos().size - 1) + "");
                    }
                }
            });
            table.add(tf).width(GameConstants.wToRelative(80)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Race: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(60));
            Array<RaceInfo> races = new Array<ShipEditor.RaceInfo>();
            for (int i = 1; i <= 6; i++)
                races.add(new RaceInfo(i, getRaceName(i)));
            races.add(new RaceInfo(255, getRaceName(255)));
            SelectBox<RaceInfo> shipRaceSelect = new SelectBox<RaceInfo>(skin);
            shipRaceSelect.setItems(races);
            shipRaceSelect.setSelected(races.get(races.indexOf(new RaceInfo(si.getRace(), getRaceName(si.getRace())), false)));
            shipRaceSelect.setName("RACE");

            table.add(shipRaceSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Type: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(60));
            SelectBox<ShipType> shipTypeSelect = new SelectBox<ShipType>(skin);
            shipTypeSelect.setItems(ShipType.values());
            shipTypeSelect.setSelected(si.getShipType());
            shipTypeSelect.setName("TYPE");
            table.add(shipTypeSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Size: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(60));
            SelectBox<ShipSize> shipSizeSelect = new SelectBox<ShipSize>(skin);
            shipSizeSelect.setItems(ShipSize.values());
            shipSizeSelect.setSelected(si.getSize());
            shipSizeSelect.setName("SIZE");
            table.add(shipSizeSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(130))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Maneuverability: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(180));
            Array<ManeuverAbility> manuvs = new Array<ShipEditor.ManeuverAbility>();
            for (int i = 0; i < 10; i++)
                manuvs.add(new ManeuverAbility(i));
            SelectBox<ManeuverAbility> shipManeuverabilitySelect = new SelectBox<ManeuverAbility>(skin);
            shipManeuverabilitySelect.setItems(manuvs);
            shipManeuverabilitySelect.setSelected(manuvs.get(si.getManeuverabilty()));
            shipManeuverabilitySelect.setName("MANEUVER");
            table.add(shipManeuverabilitySelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(120))
                    .spaceRight(GameConstants.wToRelative(10));

            shipInfoTable.add(table).spaceBottom(GameConstants.wToRelative(5));
            shipInfoTable.row();

            //third row
            table = new Table();
            for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
                ResearchType rt = ResearchType.fromIdx(i);
                l = new Label(StringDB.getString(rt.getShortName()) + ": ", skin, "mediumFont", normalColor);
                table.add(l).width(GameConstants.wToRelative(140));
                tf = new TextField(si.getNeededTechLevel(rt) + "", skin);
                tf.setName(rt.getKey());
                tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
                tf.setMaxLength(3);
                table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));
            }
            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();

            //fourth row
            table = new Table();
            for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
                ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
                l = new Label("Base " + StringDB.getString(rt.getName()) + ": ", skin, "mediumFont", normalColor);
                l.setEllipsis(true);
                table.add(l).width(GameConstants.wToRelative(120));
                tf = new TextField(si.getNeededResourceBase(rt.getType()) + "", skin);
                tf.setName(rt.getName() + "_BASE");
                tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
                tf.setMaxLength(5);
                tf.setTextFieldListener(resourceChangeListener);
                if (i == ResourceTypes.DERITIUM.getType())
                    tf.setMaxLength(2);
                table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));
            }
            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();

            //fifth row
            table = new Table();
            for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
                ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
                l = new Label(StringDB.getString(rt.getName()) + ": ", skin, "mediumFont", normalColor);
                table.add(l).width(GameConstants.wToRelative(120));
                l = new Label(si.getNeededResource(rt.getType()) + "", skin, "mediumFont", normalColor);
                l.setName(rt.getName());
                table.add(l).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));
            }
            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();

            //sixth row
            table = new Table();

            l = new Label("Base Industry: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(150));
            tf = new TextField(si.getNeededIndustryBase() + "", skin);
            tf.setName("INDUSTRY_BASE");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(6);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Industry: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            l = new Label(si.getNeededIndustry() + "", skin, "mediumFont", normalColor);
            l.setName("INDUSTRY");
            table.add(l).width(GameConstants.wToRelative(65)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Only in: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(90));
            tf = new TextField(si.getOnlyInSystem(), skin);
            tf.setName("ONLY_IN");
            table.add(tf).width(GameConstants.wToRelative(150)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Max shield: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(si.getShield().getMaxShield() + "", skin);
            tf.setName("MAX_SHIELD");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(6);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Shield type: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(si.getShield().getShieldType() + "", skin);
            tf.setName("SHIELD_TYPE");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(2);
            table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Sh. regen.: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(110));
            CheckBox shipRegenerativeShield = new CheckBox("", skin);
            shipRegenerativeShield.setChecked(si.getShield().isRegenerative());
            shipRegenerativeShield.setName("REGENERATIVE_SHIELD");
            shipRegenerativeShield.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    saveShipInfo();
                }
            });
            table.add(shipRegenerativeShield).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                    .spaceRight(GameConstants.wToRelative(10));

            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();
            //seventh row
            table = new Table();

            l = new Label("Base hull: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            tf = new TextField(si.getHull().getBaseHull() + "", skin);
            tf.setName("BASE_HULL");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(6);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Material: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            Array<ResourceTypes> materials = new Array<ResourceTypes>();
            materials.add(ResourceTypes.TITAN);
            materials.add(ResourceTypes.DURANIUM);
            materials.add(ResourceTypes.IRIDIUM);
            SelectBox<ResourceTypes> shipHullMaterialSelect = new SelectBox<ResourceTypes>(skin);
            shipHullMaterialSelect.setItems(materials);
            shipHullMaterialSelect.setSelected(ResourceTypes.fromResourceTypes(si.getHull().getHullMaterial()));
            shipHullMaterialSelect.setName("MATERIAL");
            shipHullMaterialSelect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    saveShipInfo();
                }
            });
            table.add(shipHullMaterialSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(110))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Doublehull: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            CheckBox shipDoubleHullSelect = new CheckBox("", skin);
            shipDoubleHullSelect.setChecked(si.getHull().isDoubleHull());
            shipDoubleHullSelect.setName("DOUBLE_HULL");
            shipDoubleHullSelect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    saveShipInfo();
                }
            });
            table.add(shipDoubleHullSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Ablative: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            CheckBox shipAblativeSelect = new CheckBox("", skin);
            shipAblativeSelect.setChecked(si.getHull().isAblative());
            shipAblativeSelect.setName("ABLATIVE_HULL");
            shipAblativeSelect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    saveShipInfo();
                }
            });
            table.add(shipAblativeSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Polarised: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(110));
            CheckBox shipPolarisedSelect = new CheckBox("", skin);
            shipPolarisedSelect.setChecked(si.getHull().isPolarisation());
            shipPolarisedSelect.setName("POLARISED_HULL");
            shipPolarisedSelect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    saveShipInfo();
                }
            });
            table.add(shipPolarisedSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                    .spaceRight(GameConstants.wToRelative(10));

            l = new Label("Speed: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(80));
            tf = new TextField(si.getSpeed() + "", skin);
            tf.setName("SPEED");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(1);
            table.add(tf).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Range: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(70));
            SelectBox<ShipRange> shipRangeSelect = new SelectBox<ShipRange>(skin);
            shipRangeSelect.setItems(ShipRange.values());
            shipRangeSelect.setSelected(si.getRange());
            shipRangeSelect.setName("RANGE");
            table.add(shipRangeSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(100))
                    .spaceRight(GameConstants.wToRelative(10));

            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();
            //eight row
            table = new Table();

            l = new Label("Scanpower: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(si.getScanPower() + "", skin);
            tf.setName("SCANPOWER");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(3);
            table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Scanrange: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(si.getScanRange() + "", skin);
            tf.setName("SCANRANGE");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(1);
            table.add(tf).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Stealth: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(90));
            tf = new TextField(si.getStealthGrade() + "", skin);
            tf.setName("STEALTH");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(1);
            table.add(tf).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Storage: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(90));
            tf = new TextField(si.getStorageRoom() + "", skin);
            tf.setName("STORAGE");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(5);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Stationp.: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            tf = new TextField(si.getStationBuildPoints() + "", skin);
            tf.setName("STATIONPOINTS");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(1);
            table.add(tf).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Colonyp.: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            tf = new TextField(si.getColonizePoints() + "", skin);
            tf.setName("COLONYPOINTS");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(1);
            table.add(tf).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));

            l = new Label("Upkeep: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(90));
            tf = new TextField(si.getMaintenanceCosts() + "", skin);
            tf.setName("UPKEEP");
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setTextFieldListener(resourceChangeListener);
            tf.setMaxLength(3);
            table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));

            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();
            //ninth row
            table = new Table();
            Array<String> sameClass = getShipsFromSameClassAndRace(si);
            l = new Label("Obsoletes: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            SelectBox<String> shipObsoletsSelect = new SelectBox<String>(skin);
            shipObsoletsSelect.setItems(sameClass);
            shipObsoletsSelect.setSelected(si.getObsoletesClassIdx() == -1 ? "" : game.getShipInfos()
                    .get(si.getObsoletesClassIdx()).getShipClass());
            shipObsoletsSelect.setName("OBSOLETES");
            table.add(shipObsoletsSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(200))
                    .spaceRight(GameConstants.wToRelative(10));

            Array<ShipSpecial> specialsLeft = getShipSpecialsLeft(si, 0);
            l = new Label("Special 1: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            SelectBox<ShipSpecial> shipSpecial1Select = new SelectBox<ShipSpecial>(skin);
            shipSpecial1Select.setItems(specialsLeft);
            shipSpecial1Select.setSelected(si.getSpecial(0));
            shipSpecial1Select.setName("SPECIAL1");
            shipSpecial1Select.addListener(new ChangeListener() {
                @SuppressWarnings("unchecked")
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (!changeLock) {
                        changeLock = true;
                        ShipSpecial special1 = (ShipSpecial) getSelectBoxValue("SPECIAL1");
                        ShipSpecial special2 = (ShipSpecial) getSelectBoxValue("SPECIAL2");
                        Array<ShipSpecial> specials = new Array<ShipSpecial>(ShipSpecial.values());
                        if (special1 != ShipSpecial.NONE)
                            specials.removeValue(special1, false);
                        SelectBox<ShipSpecial> select = ((SelectBox<ShipSpecial>) shipInfoTable.findActor("SPECIAL2"));
                        select.clearItems();
                        select.setItems(specials);
                        select.setSelected(special2);
                        changeLock = false;
                    }
                }
            });

            table.add(shipSpecial1Select).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(200))
                    .spaceRight(GameConstants.wToRelative(10));

            specialsLeft = getShipSpecialsLeft(si, 1);
            l = new Label("Special 2: ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(100));
            SelectBox<ShipSpecial> shipSpecial2Select = new SelectBox<ShipSpecial>(skin);
            shipSpecial2Select.setItems(specialsLeft);
            shipSpecial2Select.setSelected(si.getSpecial(1));
            shipSpecial2Select.setName("SPECIAL2");
            shipSpecial2Select.addListener(new ChangeListener() {
                @SuppressWarnings("unchecked")
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (!changeLock) {
                        changeLock = true;
                        ShipSpecial special1 = (ShipSpecial) getSelectBoxValue("SPECIAL1");
                        ShipSpecial special2 = (ShipSpecial) getSelectBoxValue("SPECIAL2");
                        Array<ShipSpecial> specials = new Array<ShipSpecial>(ShipSpecial.values());
                        if (special2 != ShipSpecial.NONE)
                            specials.removeValue(special2, false);
                        SelectBox<ShipSpecial> select = ((SelectBox<ShipSpecial>) shipInfoTable.findActor("SPECIAL1"));
                        select.clearItems();
                        select.setItems(specials);
                        select.setSelected(special1);
                        changeLock = false;
                    }
                }
            });
            table.add(shipSpecial2Select).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(200))
                    .spaceRight(GameConstants.wToRelative(10));

            shipInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
            shipInfoTable.row();
            //weapons
            table = new Table();
            shipInfoTable.add(table).width(GameConstants.wToRelative(1130)).height(GameConstants.hToRelative(360));

            Table bwtable = new Table(); //beams
            bwtable.align(Align.topLeft);
            table.add(bwtable).width(GameConstants.wToRelative(560)).spaceRight(GameConstants.wToRelative(10))
                    .height(GameConstants.hToRelative(360));

            Table bwContainer = new Table();
            bwContainer.align(Align.topLeft);
            Table bwList = new Table();
            bwList.align(Align.topLeft);
            bwList.setName("BW_LIST");
            ScrollPane beamScroller = new ScrollPane(bwList, skin);
            beamScroller.setVariableSizeKnobs(false);
            beamScroller.setScrollingDisabled(true, false);
            bwContainer.add(beamScroller).width(GameConstants.wToRelative(345)).height(GameConstants.hToRelative(295));
            bwContainer.row();

            Table bwButtons = new Table();
            TextButton buttonBwAdd = new TextButton("Add", styleSmall);
            buttonBwAdd.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    BeamWeapons bw = new BeamWeapons();
                    bw.modifyBeamWeapon(1, 10, 1, "unnamed", false, false, 0, 10, 50, 1);
                    selectedShipInfo.getBeamWeapons().add(bw);
                    selectedBwItem = selectedShipInfo.getBeamWeapons().size - 1;
                    show(false, false);
                }
            });
            bwButtons.add(buttonBwAdd).spaceRight(GameConstants.wToRelative(25)).height(GameConstants.hToRelative(35))
                    .width(GameConstants.wToRelative(130));

            TextButton buttonBwDelete = new TextButton("Remove", styleSmall);
            buttonBwDelete.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (selectedShipInfo.getBeamWeapons().size > 0 && selectedBwItem != -1) {
                        selectedShipInfo.getBeamWeapons().removeIndex(selectedBwItem);
                        updateResources();
                        selectedBwItem--;
                        if (selectedShipInfo.getBeamWeapons().size == 0)
                            selectedBwItem = -1;
                        show(false, false);
                    }
                }
            });
            bwButtons.add(buttonBwDelete).height(GameConstants.hToRelative(35)).width(GameConstants.wToRelative(130));
            bwContainer.add(bwButtons);
            bwContainer.row();

            int beamDamage = si.getBeamInfo(new ObjectIntMap<String>());
            String text = "Total beam damage: " + beamDamage;
            l = new Label(text, skin, "mediumFont", normalColor);
            bwContainer.add(l).height(GameConstants.hToRelative(20)).spaceTop(GameConstants.hToRelative(5));

            bwtable.add(bwContainer).width(GameConstants.wToRelative(345)).height(GameConstants.hToRelative(360))
                    .spaceRight(GameConstants.wToRelative(10));

            Table bwInfo = new Table();
            bwInfo.align(Align.topLeft);
            bwInfo.setName("BW_INFO");
            bwtable.add(bwInfo).width(GameConstants.wToRelative(205)).height(GameConstants.hToRelative(360));

            populateBeamWeapons();

            Table twtable = new Table();
            twtable.align(Align.topLeft);
            table.add(twtable).width(GameConstants.wToRelative(560)).spaceRight(GameConstants.wToRelative(10))
                    .height(GameConstants.hToRelative(360));

            Table twContainer = new Table();
            twContainer.align(Align.topLeft);
            Table twList = new Table();
            twList.align(Align.topLeft);
            twList.setName("TW_LIST");
            ScrollPane torpedoScroller = new ScrollPane(twList, skin);
            torpedoScroller.setVariableSizeKnobs(false);
            torpedoScroller.setScrollingDisabled(true, false);
            twContainer.add(torpedoScroller).width(GameConstants.wToRelative(345)).height(GameConstants.hToRelative(295));
            twContainer.row();

            Table twButtons = new Table();
            TextButton buttonTwAdd = new TextButton("Add", styleSmall);
            buttonTwAdd.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    TorpedoWeapons tw = new TorpedoWeapons();
                    tw.modifyTorpedoWeapon(0, 1, 50, 1, "unnamed", false, 50);
                    selectedShipInfo.getTorpedoWeapons().add(tw);
                    selectedTwItem = selectedShipInfo.getTorpedoWeapons().size - 1;
                    show(false, false);
                }
            });
            twButtons.add(buttonTwAdd).spaceRight(GameConstants.wToRelative(25)).height(GameConstants.hToRelative(35))
                    .width(GameConstants.wToRelative(130));

            TextButton buttonTwDelete = new TextButton("Remove", styleSmall);
            buttonTwDelete.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (selectedShipInfo.getTorpedoWeapons().size > 0 && selectedTwItem != -1) {
                        selectedShipInfo.getTorpedoWeapons().removeIndex(selectedBwItem);
                        updateResources();
                        selectedTwItem--;
                        if (selectedShipInfo.getTorpedoWeapons().size == 0)
                            selectedTwItem = -1;
                        show(false, false);
                    }
                }
            });
            twButtons.add(buttonTwDelete).height(GameConstants.hToRelative(35)).width(GameConstants.wToRelative(130));
            twContainer.add(twButtons);
            twContainer.row();

            int torpedoDamage = si.getTorpedoInfo(new ObjectIntMap<String>());
            text = "Total torpedo damage: " + torpedoDamage;
            l = new Label(text, skin, "mediumFont", normalColor);
            twContainer.add(l).height(GameConstants.hToRelative(20)).spaceTop(GameConstants.hToRelative(5));

            twtable.add(twContainer).width(GameConstants.wToRelative(345)).height(GameConstants.hToRelative(360))
                    .spaceRight(GameConstants.wToRelative(10));

            Table twInfo = new Table();
            twInfo.align(Align.topLeft);
            twInfo.setName("TW_INFO");
            twtable.add(twInfo).width(GameConstants.wToRelative(205)).height(GameConstants.hToRelative(360));
            populateTorpedoWeapons();

            shipInfoTable.row();
            table = new Table();
            text = "Total potential damage: " + (torpedoDamage + beamDamage);
            l = new Label(text, skin, "mediumFont", normalColor);
            table.add(l).height(GameConstants.hToRelative(20));
            text = "";
            //hack to move the text a bit to the left
            l = new Label(text, skin, "mediumFont", normalColor);
            table.add(l).height(GameConstants.hToRelative(20)).width(GameConstants.wToRelative(250));
            shipInfoTable.add(table).spaceTop(GameConstants.hToRelative(5));
            shipInfoTable.row();
        }
        shipInfoTable.setVisible(true);
    }

    private void populateTorpedoWeapons() {
        Table twList = shipInfoTable.findActor("TW_LIST");
        twList.clear();
        twItems.clear();
        ShipInfo si = selectedShipInfo;
        for (int i = 0; i < si.getTorpedoWeapons().size; i++) {
            TorpedoWeapons tw = si.getTorpedoWeapons().get(i);
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            Label l = new Label(tw.getNumberOfTubes() + "x " + tw.getTubeName() + " [" + tw.getFireArc().getPosition() + "-"
                    + tw.getFireArc().getAngle() + "]", skin, "default-font", normalColor);
            l.setUserObject(i);
            l.setEllipsis(true);
            button.add(l).width(GameConstants.wToRelative(345));
            button.setUserObject(l);
            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markTwListSelected(b);
                    showTwInfo(getIndex(b));
                }
            };
            button.addListener(gestureListener);
            twList.add(button).align(Align.left);
            twItems.add(button);
            twList.row();
        }

        Button btn = null;
        if (selectedTwItem != -1) {
            for (Button b : twItems) {
                if (selectedTwItem == getIndex(b)) {
                    btn = b;
                    twListSelected = btn;
                }
            }
        }
        if (btn == null) {
            if (twItems.size > 0) {
                btn = twItems.get(0);
                selectedTwItem = getIndex(btn);
                twListSelected = btn;
            }
        }

        if (selectedTwItem != -1 && twItems.size > 0)
            showTwInfo(selectedTwItem);
    }

    private void showTwInfo(int idx) {
        final TorpedoWeapons tw = selectedShipInfo.getTorpedoWeapons().get(idx);
        final String oldName = tw.getTubeName();
        Table twInfo = shipInfoTable.findActor("TW_INFO");
        twInfo.clear();

        //this listener applies changes for all weapons of the current type (except: name, torpedotype, nr tubes, mountpoint)
        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                focusItem = actor.getName();
                for (ShipInfo si : game.getShipInfos()) {
                    updateTubes(oldName, si);
                    si.calculateFinalCosts();
                }

                updateTubes(oldName, selectedShipInfo);
                updateResources();
                show(false, false);
                refocusAfterChange();
            }
        };

        //this listener applies changes that are only for the current weapon (name, torpedotype, nr tubes, mountpoint)
        ChangeListener uniqueChangeListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                focusItem = actor.getName();
                updateTube(tw, oldName, false);
                updateResources();
                show(false, false);
                refocusAfterChange();
            }
        };

        Array<String> weaponNames = new Array<String>();
        for (ShipInfo si : game.getShipInfos())
            for (TorpedoWeapons twp : si.getTorpedoWeapons())
                if (!weaponNames.contains(twp.getTubeName(), false))
                    weaponNames.add(twp.getTubeName());
        for (TorpedoWeapons twp : selectedShipInfo.getTorpedoWeapons())
            if (!weaponNames.contains(twp.getTubeName(), false))
                weaponNames.add(twp.getTubeName());
        weaponNames.sort();
        SelectBox<String> wselect = new SelectBox<String>(skin);
        wselect.setItems(weaponNames);
        wselect.setName("TW_NAME");
        wselect.setSelected(tw.getTubeName());
        twInfo.add(wselect).width(GameConstants.wToRelative(205));

        wselect.addListener(uniqueChangeListener);
        wselect.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    final TextField tf = new TextField(oldName, skin);
                    Dialog dialog = new Dialog("Change weapon name", skin) {
                        protected void result(Object object) {
                            if ((Boolean) object) {
                                updateTube(tw, oldName, true, tf.getText());
                                ShipEditor.this.show(false, false);
                            }
                        }
                    }.text("New Tube Name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                            .key(Keys.ESCAPE, false).show(stage);
                    dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                    dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                            .width(GameConstants.wToRelative(220));
                    dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                    dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                    dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                    dialog.invalidateHierarchy();
                    dialog.invalidate();
                    dialog.layout();
                    dialog.show(stage);
                }
            }
        });
        twInfo.row();

        Table table = new Table();
        Label l = new Label("#Tubes: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(65));
        TextField tf = new TextField(tw.getNumberOfTubes() + "", skin);
        tf.setName("TW_TUBES_NO");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(uniqueChangeListener);
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(5));

        l = new Label("Rate: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(50));
        tf = new TextField(tw.getTubeFirerate() + "", skin);
        tf.setName("TW_RATE");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        l = new Label("T/shot: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(65));
        tf = new TextField(tw.getNumber() + "", skin);
        tf.setName("TW_NUMBER");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(5));

        l = new Label("Accuracy: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(75));
        tf = new TextField(tw.getAccuracy() + "", skin);
        tf.setName("TW_ACCURACY");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        l = new Label("Only micro: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(90));
        CheckBox checkbox = new CheckBox("", skin);
        checkbox.setChecked(tw.isOnlyMicroPhoton());
        checkbox.setName("TW_MICRO");
        checkbox.addListener(changeListener);
        table.add(checkbox).width(GameConstants.wToRelative(20));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        l = new Label("Mountposition: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(tw.getFireArc().getPosition() + "", skin);
        tf.setName("TW_MOUNT");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(uniqueChangeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        l = new Label("Angle: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(tw.getFireArc().getAngle() + "", skin);
        tf.setName("TW_ANGLE");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        SelectBox<TorpedoStats> tselect = new SelectBox<TorpedoStats>(skin);
        tselect.setItems(TorpedoInfo.getStats(tw.isOnlyMicroPhoton()));
        tselect.setName("TW_TORPEDOES");
        tselect.setSelected(TorpedoInfo.getStats().get(tw.getTorpedoType()));
        tselect.addListener(uniqueChangeListener);
        twInfo.add(tselect).width(GameConstants.wToRelative(205));
        twInfo.row();

        float infoHeight = GameConstants.hToRelative(25);
        l = new Label("Damge: " + TorpedoInfo.getPower(tw.getTorpedoType()), skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(90)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table);
        twInfo.row();

        table = new Table();
        l = new Label("Penetrating: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(95));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isPenetrating(tw.getTorpedoType()));
        checkbox.setName("TW_PENETRATING");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();

        table = new Table();
        l = new Label("Double shield damge: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(165));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isDoubleShieldDmg(tw.getTorpedoType()));
        checkbox.setName("TW_DOUBLE_SHIELD");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();

        table = new Table();
        l = new Label("Double hull damge: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(150));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isDoubleHullDmg(tw.getTorpedoType()));
        checkbox.setName("TW_DOUBLE_HULL");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();

        table = new Table();
        l = new Label("Ignore all shields: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(135));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isIgnoreAllShields(tw.getTorpedoType()));
        checkbox.setName("TW_IGNORE_SHIELD");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();

        table = new Table();
        l = new Label("Collapse shields: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(125));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isCollapseShields(tw.getTorpedoType()));
        checkbox.setName("TW_COLLAPSE_SHIELD");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();

        table = new Table();
        l = new Label("Reduce maneuver: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(145));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(TorpedoInfo.isReduceManeuver(tw.getTorpedoType()));
        checkbox.setName("TW_REDUCE_MANEUVER");
        checkbox.setDisabled(true);
        table.add(checkbox).width(GameConstants.wToRelative(20)).height(GameConstants.hToRelative(infoHeight));
        twInfo.add(table).align(Align.right);
        twInfo.row();
    }

    private void updateTubes(String oldName, ShipInfo si) {
        for (TorpedoWeapons modTW : si.getTorpedoWeapons()) {
            if (oldName.equals(modTW.getTubeName())) {
                updateTube(modTW, oldName, true);
            }
        }
    }

    private void updateTube(TorpedoWeapons modTW, String oldName, boolean modifyOnly) {
        updateTube(modTW, oldName, modifyOnly, "");
    }

    private void updateTube(TorpedoWeapons modTW, String oldName, boolean modifyOnly, String newName) {
        String tubeName = oldName;

        int fireNumber = parseIntDefault(getTextFieldValue("TW_NUMBER"));
        int tubeFirerate = parseIntDefault(getTextFieldValue("TW_RATE"));
        if (tubeFirerate == 0)
            tubeFirerate = 100;
        int accuracy = parseIntDefault(getTextFieldValue("TW_ACCURACY"));
        int numberOfTubes = modTW.getNumberOfTubes();
        int position = modTW.getFireArc().getPosition();
        boolean onlyMicroPhoton = getCheckBoxValue("TW_MICRO");
        int angle = parseIntDefault(getTextFieldValue("TW_ANGLE"));
        int torpedoType = modTW.getTorpedoType();

        if (!modifyOnly) {
            tubeName = getSelectBoxValue("TW_NAME").toString();
            numberOfTubes = parseIntDefault(getTextFieldValue("TW_TUBES_NO"));
            position = parseIntDefault(getTextFieldValue("TW_MOUNT"));
            boolean wasMicroProton = onlyMicroPhoton;
            if (!tubeName.equals(oldName))
                for (ShipInfo si : game.getShipInfos())
                    for (TorpedoWeapons twp : si.getTorpedoWeapons())
                        if (twp.getTubeName().equals(tubeName)) {
                            fireNumber = twp.getNumber();
                            tubeFirerate = twp.getTubeFirerate();
                            accuracy = twp.getAccuracy();
                            onlyMicroPhoton = twp.isOnlyMicroPhoton();
                            angle = twp.getFireArc().getAngle();
                            break;
                        }
            TorpedoStats selectedStat = (TorpedoStats) getSelectBoxValue("TW_TORPEDOES");
            if (onlyMicroPhoton != wasMicroProton)
                selectedStat = TorpedoInfo.getStats(onlyMicroPhoton).get(
                        torpedoType < TorpedoInfo.getStats(onlyMicroPhoton).size ? torpedoType : 0);
            torpedoType = TorpedoInfo.findTorpedoIdx(selectedStat);
        }

        if (angle > 360)
            angle = 360;
        if (position > 360)
            position = 360;
        modTW.modifyTorpedoWeapon(torpedoType, fireNumber, tubeFirerate, numberOfTubes, newName.isEmpty() ? tubeName : newName,
                onlyMicroPhoton, accuracy);
        modTW.getFireArc().setValues(position, angle);
    }

    private void populateBeamWeapons() {
        Table bwList = shipInfoTable.findActor("BW_LIST");
        bwList.clear();
        bwItems.clear();
        ShipInfo si = selectedShipInfo;
        for (int i = 0; i < si.getBeamWeapons().size; i++) {
            BeamWeapons bw = si.getBeamWeapons().get(i);
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            Label l = new Label(bw.getBeamNumber() + "x " + bw.getBeamName() + " " + bw.getBeamType() + " ["
                    + bw.getFireArc().getPosition() + "-" + bw.getFireArc().getAngle() + "]", skin, "default-font", normalColor);
            l.setUserObject(i);
            l.setEllipsis(true);
            button.add(l).width(GameConstants.wToRelative(345));
            button.setUserObject(l);
            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markBwListSelected(b);
                    showBwInfo(getIndex(b));
                }
            };
            button.addListener(gestureListener);
            bwList.add(button).align(Align.left);
            bwItems.add(button);
            bwList.row();
        }

        Button btn = null;
        if (selectedBwItem != -1) {
            for (Button b : bwItems) {
                if (selectedBwItem == getIndex(b)) {
                    btn = b;
                    bwListSelected = btn;
                }
            }
        }
        if (btn == null) {
            if (bwItems.size > 0) {
                btn = bwItems.get(0);
                selectedBwItem = getIndex(btn);
                bwListSelected = btn;
            }
        }

        if (selectedBwItem != -1 && bwItems.size > 0)
            showBwInfo(selectedBwItem);
    }

    private void showBwInfo(int idx) {
        final BeamWeapons bw = selectedShipInfo.getBeamWeapons().get(idx);
        final String oldName = bw.getBeamName();
        Table bwInfo = shipInfoTable.findActor("BW_INFO");
        bwInfo.clear();

        //this listener applies changes for all weapons of the current type (except: name, type, count, mountpoint)
        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                focusItem = actor.getName();
                for (ShipInfo si : game.getShipInfos()) {
                    updateBeams(oldName, si);
                    si.calculateFinalCosts();
                }

                updateBeams(oldName, selectedShipInfo);
                updateResources();
                show(false, false);
                refocusAfterChange();
            }
        };

        //this listener applies changes that are only for the current weapon (name, type, count, mountpoint)
        ChangeListener uniqueChangeListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                focusItem = actor.getName();
                updateBeam(bw, oldName, false);
                updateResources();
                show(false, false);
                refocusAfterChange();
            }
        };

        Array<String> weaponNames = new Array<String>();
        for (ShipInfo si : game.getShipInfos())
            for (BeamWeapons bwp : si.getBeamWeapons())
                if (!weaponNames.contains(bwp.getBeamName(), false))
                    weaponNames.add(bwp.getBeamName());
        for (BeamWeapons bwp : selectedShipInfo.getBeamWeapons())
            if (!weaponNames.contains(bwp.getBeamName(), false))
                weaponNames.add(bwp.getBeamName());
        weaponNames.sort();
        SelectBox<String> wselect = new SelectBox<String>(skin);
        wselect.setItems(weaponNames);
        wselect.setName("BW_NAME");
        wselect.setSelected(bw.getBeamName());
        bwInfo.add(wselect).width(GameConstants.wToRelative(205));
        wselect.addListener(uniqueChangeListener);
        wselect.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    final TextField tf = new TextField(oldName, skin);
                    Dialog dialog = new Dialog("Change weapon name", skin) {
                        protected void result(Object object) {
                            if ((Boolean) object) {
                                updateBeam(bw, oldName, true, tf.getText());
                                ShipEditor.this.show(false, false);
                            }
                        }
                    }.text("New Weapon Name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                            .key(Keys.ESCAPE, false).show(stage);
                    dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                    dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                            .width(GameConstants.wToRelative(220));
                    dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                    dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                    dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                    dialog.invalidateHierarchy();
                    dialog.invalidate();
                    dialog.layout();
                    dialog.show(stage);
                }
            }
        });
        bwInfo.row();

        Table table = new Table();
        Label l = new Label("Type: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        TextField tf = new TextField(bw.getBeamType() + "", skin);
        tf.setName("BW_TYPE");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(uniqueChangeListener);
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(5));

        l = new Label("Cnt: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(50));
        tf = new TextField(bw.getBeamNumber() + "", skin);
        tf.setName("BW_CNT");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(uniqueChangeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(40));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Power: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(55));
        tf = new TextField(bw.getBeamPower() + "", skin);
        tf.setName("BW_POWER");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60));

        l = new Label("Shots: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(50));
        tf = new TextField(bw.getShootNumber() + "", skin);
        tf.setName("BW_SHOOT");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Time: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(55));
        tf = new TextField(bw.getBeamLength() + "", skin);
        tf.setName("BW_TIME");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));

        l = new Label("Recharge: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(75));
        tf = new TextField(bw.getRechargeTime() + "", skin);
        tf.setName("BW_RECHARGE");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Bonus: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(55));
        tf = new TextField(bw.getBonus() + "", skin);
        tf.setName("BW_BONUS");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Modulating: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(90));
        CheckBox checkbox = new CheckBox("", skin);
        checkbox.setChecked(bw.isModulating());
        checkbox.setName("BW_MODULATING");
        checkbox.addListener(changeListener);
        table.add(checkbox).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(5));

        l = new Label("Piercing: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(65));
        checkbox = new CheckBox("", skin);
        checkbox.setChecked(bw.isPiercing());
        checkbox.setName("BW_PIERCING");
        checkbox.addListener(changeListener);
        table.add(checkbox).width(GameConstants.wToRelative(20));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Mountposition: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bw.getFireArc().getPosition() + "", skin);
        tf.setName("BW_MOUNT");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(uniqueChangeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        bwInfo.add(table);
        bwInfo.row();

        table = new Table();
        l = new Label("Angle: ", skin, "default-font", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bw.getFireArc().getAngle() + "", skin);
        tf.setName("BW_ANGLE");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.addListener(changeListener);
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(35));
        bwInfo.add(table);
    }

    private void refocusAfterChange() {
        Actor newActor = shipInfoTable.findActor(focusItem);
        stage.setKeyboardFocus(newActor);
        if (newActor instanceof TextField) {
            TextField tf = (TextField) newActor;
            tf.setCursorPosition(tf.getText().length());
        }
    }

    private void updateBeams(String oldName, ShipInfo si) {
        for (BeamWeapons modBW : si.getBeamWeapons()) {
            if (oldName.equals(modBW.getBeamName())) {
                updateBeam(modBW, oldName, true);
            }
        }
    }

    private void updateBeam(BeamWeapons modBW, String oldName, boolean modifyOnly) {
        updateBeam(modBW, oldName, modifyOnly, "");
    }

    private void updateBeam(BeamWeapons modBW, String oldName, boolean modifyOnly, String newName) {
        String beamName = oldName;
        int beamType = modBW.getBeamType();
        int beamNumber = modBW.getBeamNumber();
        int beamPower = parseIntDefault(getTextFieldValue("BW_POWER"));
        boolean modulating = getCheckBoxValue("BW_MODULATING");
        boolean piercing = getCheckBoxValue("BW_PIERCING");
        int bonus = parseIntDefault(getTextFieldValue("BW_BONUS"));
        int beamLength = parseIntDefault(getTextFieldValue("BW_TIME"));
        int rechargeTime = parseIntDefault(getTextFieldValue("BW_RECHARGE"));
        int shootNumber = parseIntDefault(getTextFieldValue("BW_SHOOT"));
        int position = modBW.getFireArc().getPosition();
        int angle = parseIntDefault(getTextFieldValue("BW_ANGLE"));

        if (!modifyOnly) {
            beamType = parseIntDefault(getTextFieldValue("BW_TYPE"));
            beamName = getSelectBoxValue("BW_NAME").toString();
            beamNumber = parseIntDefault(getTextFieldValue("BW_CNT"));
            position = parseIntDefault(getTextFieldValue("BW_MOUNT"));
            if (!beamName.equals(oldName))
                for (ShipInfo si : game.getShipInfos())
                    for (BeamWeapons bwp : si.getBeamWeapons())
                        if (bwp.getBeamName().equals(beamName)) {
                            beamPower = bwp.getBeamPower();
                            modulating = bwp.isModulating();
                            piercing = bwp.isPiercing();
                            bonus = bwp.getBonus();
                            beamLength = bwp.getBeamLength();
                            rechargeTime = bwp.getRechargeTime();
                            shootNumber = bwp.getShootNumber();
                            angle = bwp.getFireArc().getAngle();
                            break;
                        }
        }

        if (beamLength <= 0)
            beamLength = 1;
        if (angle > 360)
            angle = 360;
        if (position > 360)
            position = 360;
        modBW.modifyBeamWeapon(beamType, beamPower, beamNumber, newName.isEmpty() ? beamName : newName, modulating, piercing,
                bonus, beamLength, rechargeTime, shootNumber);
        modBW.getFireArc().setValues(position, angle);
    }

    private Array<String> getShipsFromSameClassAndRace(ShipInfo si) {
        Array<String> candidates = new Array<String>();
        candidates.add("");
        for (int i = 0; i < game.getShipInfos().size; i++) {
            ShipInfo other = game.getShipInfos().get(i);
            if (!si.getShipClass().equals(other.getShipClass()) && (si.getRace() == other.getRace())
                    && other.getBioTech() <= si.getBioTech() && other.getEnergyTech() <= si.getEnergyTech()
                    && other.getCompTech() <= si.getCompTech() && other.getConstructionTech() <= si.getConstructionTech()
                    && other.getPropulsionTech() <= si.getPropulsionTech() && other.getWeaponTech() <= si.getWeaponTech()) {
                candidates.add(other.getShipClass());
            }
        }
        return candidates;
    }

    private Array<ShipSpecial> getShipSpecialsLeft(ShipInfo si, int idx) {
        Array<ShipSpecial> specials = new Array<ShipSpecial>();
        for (ShipSpecial sp : ShipSpecial.values())
            if (!si.hasSpecial(sp) || sp == ShipSpecial.NONE || si.getSpecial(idx == 0 ? 1 : 0) != sp)
                specials.add(sp);
        return specials;
    }

    private void updateResources() {
        ShipInfo si = selectedShipInfo;
        si.calculateFinalCosts();
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            ((Label) shipInfoTable.findActor(rt.getName())).setText(si.getNeededResource(rt.getType()) + "");
        }
        ((Label) shipInfoTable.findActor("INDUSTRY")).setText(si.getNeededIndustry() + "");
    }

    private String getRaceName(int shipNumber) {
        ArrayMap<String, Major> majors = game.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (shipNumber == major.getRaceShipNumber())
                return major.getName();
        }
        return "Minor Race";
    }

    private String getTextFieldValue(String name) {
        return ((TextField) shipInfoTable.findActor(name)).getText();
    }

    @SuppressWarnings("rawtypes")
    private Object getSelectBoxValue(String name) {
        return ((SelectBox) shipInfoTable.findActor(name)).getSelected();
    }

    private boolean getCheckBoxValue(String name) {
        return ((CheckBox) shipInfoTable.findActor(name)).isChecked();
    }

    private int parseIntDefault(String text) {
        return parseIntDefault(text, 0);
    }

    private int parseIntDefault(String text, int def) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    private void saveShipInfo() {
        ShipInfo si = selectedShipInfo;
        si.setID(parseIntDefault(getTextFieldValue("SHIP_ID")));
        localeShipInfo.setShipClass(getTextFieldValue("SHIPCLASS"));
        localeShipInfo.setDescription(getTextFieldValue("DESCRIPTION"));
        si.setRace(((RaceInfo) getSelectBoxValue("RACE")).getId());
        si.setType((ShipType) getSelectBoxValue("TYPE"));
        si.setSize((ShipSize) getSelectBoxValue("SIZE"));
        si.setManeuverabilty(((ManeuverAbility) getSelectBoxValue("MANEUVER")).toInt());
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            si.setResearch(rt, parseIntDefault(getTextFieldValue(rt.getKey())));
        }
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            si.setNeededResource(rt, parseIntDefault(getTextFieldValue(rt.getName() + "_BASE")));
        }
        si.setNeededIndustryBase(parseIntDefault(getTextFieldValue("INDUSTRY_BASE")));
        si.setOnlyInSystem(getTextFieldValue("ONLY_IN"));
        int baseHull = parseIntDefault(getTextFieldValue("BASE_HULL"));
        int hullMaterial = ((ResourceTypes) getSelectBoxValue("MATERIAL")).getType();
        boolean doubleHull = getCheckBoxValue("DOUBLE_HULL");
        boolean ablativeHull = getCheckBoxValue("ABLATIVE_HULL");
        boolean polarisedHull = getCheckBoxValue("POLARISED_HULL");
        si.getHull().modifyHull(doubleHull, baseHull, hullMaterial, ablativeHull, polarisedHull);
        int maxShield = parseIntDefault(getTextFieldValue("MAX_SHIELD"));
        int shieldType = parseIntDefault(getTextFieldValue("SHIELD_TYPE"));
        boolean regenerative = getCheckBoxValue("REGENERATIVE_SHIELD");
        si.getShield().modifyShield(maxShield, shieldType, regenerative);
        si.setSpeed(parseIntDefault(getTextFieldValue("SPEED")));
        si.setRange(((ShipRange) getSelectBoxValue("RANGE")));
        si.setScanPower(parseIntDefault(getTextFieldValue("SCANPOWER")));
        si.setScanRange(parseIntDefault(getTextFieldValue("SCANRANGE")));
        si.setStealthGrade(parseIntDefault(getTextFieldValue("STEALTH")));
        si.setStorageRoom(parseIntDefault(getTextFieldValue("STORAGE")));
        si.setStationBuildPoints(parseIntDefault(getTextFieldValue("STATIONPOINTS")));
        si.setColonizePoints(parseIntDefault(getTextFieldValue("COLONYPOINTS")));
        si.setMaintenanceCosts(parseIntDefault(getTextFieldValue("UPKEEP")));
        si.setObsoletesClass(si.getObsoletesClassIdx((String) getSelectBoxValue("OBSOLETES")));
        si.setSpecial(0, (ShipSpecial) getSelectBoxValue("SPECIAL1"));
        si.setSpecial(1, (ShipSpecial) getSelectBoxValue("SPECIAL2"));
        updateResources();
    }

    public void hide() {
        visible = false;
        shipFilter.setVisible(false);
        shipScroller.setVisible(false);
        mainButtonTable.setVisible(false);
        shipInfoTable.setVisible(false);
        saveParticipants();
    }

    private void markBwListSelected(Button b) {
        if (bwListSelected != null) {
            bwListSelected.getStyle().up = null;
            bwListSelected.getStyle().down = null;
            ((Label) bwListSelected.getUserObject()).setColor(oldWpColor);
        }
        if (b == null)
            b = bwListSelected;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldWpColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        bwListSelected = b;
        selectedBwItem = getIndex(b);
    }

    private void markTwListSelected(Button b) {
        if (twListSelected != null) {
            twListSelected.getStyle().up = null;
            twListSelected.getStyle().down = null;
            ((Label) twListSelected.getUserObject()).setColor(oldWpColor);
        }
        if (b == null)
            b = twListSelected;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldWpColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        twListSelected = b;
        selectedTwItem = getIndex(b);
    }

    private void markShipListSelected(Button b) {
        if (shipSelection != null) {
            shipSelection.getStyle().up = null;
            shipSelection.getStyle().down = null;
            ((Label) shipSelection.getUserObject()).setColor(oldColor);
            if (!loadedTexture.isEmpty() && game.getAssetManager().isLoaded(loadedTexture) && selectedItem != getIndex(b)) {
                game.getAssetManager().unload(loadedTexture);
                loadedTexture = "";
            }
        }
        if (b == null)
            b = shipSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        shipSelection = b;
        selectedItem = getIndex(b);
        float buttonPos = b.getHeight() * selectedItem;
        float scrollerHeight = shipScroller.getScrollHeight();
        float scrollerPos = shipScroller.getScrollY();
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0) {
            shipScroller.setScrollY(b.getHeight() * selectedItem - shipScroller.getScrollHeight() * 2);
        } else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight) {
            shipScroller.setScrollY(b.getHeight() * (selectedItem - shipScroller.getScrollHeight() / b.getHeight() + 1));
        }
    }

    private int getIndex(Button b) {
        return (Integer) (((Label) b.getUserObject()).getUserObject());
    }

    private void updateShipInfos(Array<ShipInfo> infos) {
        for (int i = 0; i < infos.size; i++) {
            ShipInfo si = infos.get(i);
            if (si.getObsoletesClassIdx() != -1) {
                boolean found = false;
                for (int j = 0; j < infos.size; j++) {
                    if (si.getObsoletesClassId() == infos.get(j).getID()) {
                        si.setObsoletesClass(j);
                        found = true;
                        break;
                    }
                }
                if (!found)
                    si.setObsoletesClass(-1);
            }
        }
        for (int i = 0; i < infos.size; i++) {
            ShipInfo si = infos.get(i);
            si.setID(i);
        }
    }

    private void drawMainButtonTable() {
        mainButtonTable.clear();
        if (state == ShipEditorState.StateEditor) {
            TextButton addBtn = new TextButton("Add", styleSmall);
            addBtn.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    ShipInfo si = new ShipInfo(game);
                    si.setRace(1);
                    si.setID(game.getShipInfos().size);
                    game.getShipInfos().add(si);
                    for (String lang : GameConstants.getSupportedLanguages())
                        localeShipInfos.get(lang).add(si);
                    selectedItem = game.getShipInfos().size - 1;
                    show();
                }
            });
            mainButtonTable.add(addBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            TextButton copyBtn = new TextButton("Copy", styleSmall);
            copyBtn.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    ShipInfo si = new ShipInfo(game.getShipInfos().get(selectedItem));
                    si.setID(game.getShipInfos().size);
                    game.getShipInfos().add(si);
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        ShipInfo localSi = new ShipInfo(localeShipInfos.get(lang).get(selectedItem));
                        localSi.setID(localeShipInfos.get(lang).size);
                        localeShipInfos.get(lang).add(localSi);
                    }
                    selectedItem = game.getShipInfos().size - 1;
                    show();
                }
            });
            mainButtonTable.add(copyBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            TextButton removeBtn = new TextButton("Remove", styleSmall);
            removeBtn.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    game.getShipInfos().removeIndex(selectedItem);
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        localeShipInfos.get(lang).removeIndex(selectedItem);
                    }

                    game.getShipImageNames().remove(selectedItem);
                    for (int i = selectedItem; i < game.getShipInfos().size; i++) {
                        ShipInfo si = game.getShipInfos().get(i);
                        int newIdx = si.getID() - 1 - 10000;
                        game.getShipImageNames().put(newIdx, game.getShipImageNames().get(newIdx + 1));
                    }
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        updateShipInfos(localeShipInfos.get(lang));
                    }
                    updateShipInfos(game.getShipInfos());
                    show();
                }
            });
            mainButtonTable.add(removeBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            TextButton buttonUpdate = new TextButton("Update", styleSmall);
            buttonUpdate.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    saveShipInfo();
                    int newIdx = selectedShipInfo.getID() - 10000;
                    String shipImg = game.getShipImageNames().get(selectedItem);
                    game.getShipInfos().removeIndex(selectedItem);
                    ShipInfo si = new ShipInfo(selectedShipInfo);
                    si.setID(selectedItem);
                    game.getShipInfos().insert(newIdx != selectedItem ? newIdx : selectedItem, si);
                    localeShipInfos.get(selectedLanguage).removeIndex(selectedItem);
                    si = new ShipInfo(localeShipInfo);
                    si.setID(selectedItem);
                    localeShipInfos.get(selectedLanguage).insert(newIdx != selectedItem ? newIdx : selectedItem, si);
                    IntMap<String> images = new IntMap<String>();
                    if (newIdx != selectedItem) {
                        for (String lang : GameConstants.getSupportedLanguages()) {
                            if (!lang.equals(selectedLanguage)) {
                                si = localeShipInfos.get(lang).removeIndex(selectedItem);
                                si.setID(selectedItem);
                                localeShipInfos.get(lang).insert(newIdx != selectedItem ? newIdx : selectedItem, si);
                            }
                        }
                        for (int i = 0; i < game.getShipInfos().size; i++) {
                            si = game.getShipInfos().get(i);
                            String imgName = i == newIdx ? shipImg : si.getShipImageName();
                            images.put(i, imgName);
                        }
                        for (String lang : GameConstants.getSupportedLanguages()) {
                            updateShipInfos(localeShipInfos.get(lang));
                        }
                        updateShipInfos(game.getShipInfos());
                        game.getShipImageNames().clear();
                        game.getShipImageNames().putAll(images);
                    }
                    show();
                }
            });
            mainButtonTable.add(buttonUpdate).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            TextButton buttonSave = new TextButton("Save", styleSmall);
            buttonSave.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    saveShipInfosToFile();
                }
            });
            mainButtonTable.add(buttonSave).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            SelectBox<String> languageSelect = new SelectBox<String>(skin);
            languageSelect.setItems(GameConstants.getSupportedLanguages());
            languageSelect.setSelected(selectedLanguage);
            languageSelect.addListener(new ChangeListener() {
                @SuppressWarnings("unchecked")
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    selectedLanguage = ((SelectBox<String>) actor).getSelected();
                    show();
                }
            });
            mainButtonTable.add(languageSelect).width(GameConstants.wToRelative(100)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));
        } else if (state == ShipEditorState.StateCombatSimulator) {
            TextButton addBtn = new TextButton("Add", styleSmall);
            addBtn.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    addToParticipants(selectedItem);
                    show(false, false);
                }
            });
            mainButtonTable.add(addBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            TextButton clearBtn = new TextButton("Clear", styleSmall);
            clearBtn.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    participants.clear();
                    show(true, false);
                }
            });
            mainButtonTable.add(clearBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                    .spaceRight(GameConstants.wToRelative(5));

            if (participants.size > 1) {
                TextButton fightBtn = new TextButton("Fight!", styleSmall);
                fightBtn.addListener(new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        DefaultScreen screen = game.setView(ViewTypes.COMBAT_SIMULATOR, true);
                        simulatorAnomaly = (Anomaly) (getCheckBoxValue("SIMULATOR_HAS_ANOMALY") ? getSelectBoxValue("SIMULATOR_ANOMALY") : null);
                        ((CombatSimulator) screen).init(participants, simulatorAnomaly);
                        ((CombatSimulator) screen).setSpeed(simulatorSpeed);
                        ((CombatSimulator) screen).setNumberOfRuns(numberOfSimulatorRuns);
                    }
                });
                mainButtonTable.add(fightBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                        .spaceRight(GameConstants.wToRelative(5));
            }
        }

        TextButton buttonSim = new TextButton(state.getNextBtnName(), styleSmall);
        buttonSim.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                state = (state == ShipEditorState.StateCombatSimulator) ? ShipEditorState.StateEditor
                        : ShipEditorState.StateCombatSimulator;
                ((TextButton) event.getListenerActor()).setText(state.getNextBtnName());
                hide();
                show();
            }
        });
        mainButtonTable.add(buttonSim).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5)).align(Align.right).expand();

        TextButton buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((ResourceEditor) game.getScreen()).showMainMenu();
            }
        });
        mainButtonTable.add(buttonBack).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceLeft(GameConstants.wToRelative(5)).align(Align.right);
        mainButtonTable.setVisible(true);
    }

    private void addToParticipants(int shipId) {
        SelectBox<RaceInfo> raceSelect = shipInfoTable.findActor("RACE");
        int race = raceSelect.getSelected().id;
        IntArray ships = participants.get(race, new IntArray());
        boolean canAdd = true;
        if (race == 255) {
            ShipInfo newShip = game.getShipInfos().get(shipId);
            for (int i = 0; i < ships.size; i++) {
                int old = ships.get(i);
                if (!game.getShipInfos().get(old).getOnlyInSystem().equals(newShip.getOnlyInSystem())) {
                    canAdd = false;
                    break;
                }
            }
        }
        if (canAdd) {
            ships.add(shipId);
            participants.put(race, ships);
        }
    }

    private void showCombatSimSetup(boolean resetSelectedShip) {
        shipInfoTable.clear();
        shipInfoTable.setVisible(true);
        if (resetSelectedShip) {
            selectedShipInfo = new ShipInfo(game.getShipInfos().get(selectedItem));
            selectedRace = selectedShipInfo.getRace();
        }
        Table container = new Table();
        Table table = new Table();
        Label l = new Label("Race: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        Array<RaceInfo> races = new Array<ShipEditor.RaceInfo>();
        for (int i = 1; i <= 6; i++)
            races.add(new RaceInfo(i, getRaceName(i)));
        races.add(new RaceInfo(255, getRaceName(255)));
        SelectBox<RaceInfo> shipRaceSelect = new SelectBox<RaceInfo>(skin);
        shipRaceSelect.setItems(races);
        shipRaceSelect.setSelected(races.get(races.indexOf(new RaceInfo(selectedRace, getRaceName(selectedRace)), false)));
        shipRaceSelect.setName("RACE");
        shipRaceSelect.addListener(new ChangeListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                selectedRace = ((SelectBox<RaceInfo>)actor).getSelected().id;
                show(false, false);
            }
        });

        table.add(shipRaceSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                .spaceRight(GameConstants.wToRelative(10));
        container.add(table);
        container.row();
        ActorGestureListener labelListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int toRemove = (Integer) event.getListenerActor().getUserObject();
                int race = toRemove / 10000;
                int shipidx = toRemove % 10000;
                IntArray ships = participants.get(race, new IntArray());
                ships.removeIndex(shipidx);
                if (ships.size == 0)
                    participants.remove(race);
                else
                    participants.put(race, ships);
                show(false, false);
            }
        };
        IntArray ships = participants.get(selectedRace, new IntArray());
        Table shipList = new Table();
        for (int i = 0; i < ships.size; i++) {
            int id = ships.get(i);
            l = new Label(game.getShipInfos().get(id).getShipClass(), skin, "mediumFont", normalColor);
            l.setAlignment(Align.center);
            l.setUserObject(selectedRace * 10000 + i);
            l.addListener(labelListener);
            shipList.add(l).width(GameConstants.wToRelative(240));
            shipList.row();
        }
        container.add(shipList);
        shipInfoTable.add(container).spaceBottom(GameConstants.hToRelative(5)).align(Align.top);

        container = new Table();
        l = new Label("Summary: ", skin, "mediumFont", normalColor);
        table = new Table();
        table.add(l).width(GameConstants.wToRelative(500));
        table.row();
        for (Iterator<Entry<IntArray>> it = participants.entries().iterator(); it.hasNext();) {
            Entry<IntArray> e = it.next();
            l = new Label(getRaceName(e.key) + ":", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(500));
            table.row();
            for (int i = 0; i < e.value.size; i++) {
                int id = e.value.get(i);
                l = new Label("         " + game.getShipInfos().get(id).getShipClass(), skin, "mediumFont", normalColor);
                l.setUserObject(e.key * 10000 + i);
                l.addListener(labelListener);
                table.add(l).width(GameConstants.wToRelative(500));
                table.row();
            }
        }
        container.add(table);
        shipInfoTable.add(container).spaceBottom(GameConstants.hToRelative(5)).spaceLeft(GameConstants.wToRelative(5)).align(Align.topLeft);

        container = new Table();
        l = new Label("Speed: ", skin, "mediumFont", normalColor);
        table = new Table();
        table.add(l).width(GameConstants.wToRelative(60));
        table.row();
        TextField tf = new TextField(simulatorSpeed + "", skin);
        tf.setName("SIMULATOR_SPEED");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (!textField.getText().isEmpty()) {
                    simulatorSpeed = Integer.parseInt(textField.getText());
                    if (simulatorSpeed > 100)
                        simulatorSpeed = 100;
                    if (simulatorSpeed < 0)
                        simulatorSpeed = 0;
                    textField.setText("" + simulatorSpeed);
                }
            }
        });
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(50)).spaceRight(GameConstants.wToRelative(10)).align(Align.left);
        container.add(table).align(Align.left);
        container.row();

        l = new Label("Nr. of runs: ", skin, "mediumFont", normalColor);
        table = new Table();
        table.add(l).width(GameConstants.wToRelative(100));
        table.row();
        tf = new TextField(numberOfSimulatorRuns + "", skin);
        tf.setName("SIMULATOR_RUNS");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (!textField.getText().isEmpty()) {
                    numberOfSimulatorRuns = Integer.parseInt(textField.getText());
                    if (numberOfSimulatorRuns > 100000)
                        numberOfSimulatorRuns = 100000;
                    if (numberOfSimulatorRuns < 1)
                        numberOfSimulatorRuns = 1;
                    textField.setText("" + numberOfSimulatorRuns);
                }
            }
        });
        tf.setMaxLength(7);
        table.add(tf).width(GameConstants.wToRelative(100)).spaceRight(GameConstants.wToRelative(10)).align(Align.left);
        container.add(table).align(Align.left);
        container.row();

        final CheckBox cb = new CheckBox("   Anomaly", skin);
        cb.setName("SIMULATOR_HAS_ANOMALY");
        cb.setChecked(simulatorAnomaly != null);
        cb.addListener(new ChangeListener() {
            @SuppressWarnings("rawtypes")
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Actor anomalySel = shipInfoTable.findActor("SIMULATOR_ANOMALY");
                boolean set = ((CheckBox) event.getListenerActor()).isChecked();
                ((SelectBox) anomalySel).setDisabled(!set);
                simulatorAnomaly = (Anomaly) (set ? getSelectBoxValue("SIMULATOR_ANOMALY") : null);
            }
        });
        table = new Table();
        table.add(cb).width(GameConstants.wToRelative(100)).align(Align.left);
        table.row();
        final SelectBox<Anomaly> anomalySelect = new SelectBox<Anomaly>(skin);
        Array<Anomaly> anomalies = new Array<Anomaly>();
        for (int i = 0; i < AnomalyType.values().length; i++) {
            Anomaly anomaly = new Anomaly(game, i);
            anomalies.add(anomaly);
            if (simulatorAnomaly != null && simulatorAnomaly.getType() == anomaly.getType())
                simulatorAnomaly = anomaly;
        }
        anomalySelect.setItems(anomalies);
        anomalySelect.setDisabled(simulatorAnomaly == null);
        anomalySelect.setName("SIMULATOR_ANOMALY");
        anomalySelect.setSelected(simulatorAnomaly);
        anomalySelect.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                simulatorAnomaly = cb.isChecked() ? anomalySelect.getSelected() : null;
            }
        });
        table.add(anomalySelect).width(GameConstants.wToRelative(150)).spaceRight(GameConstants.wToRelative(10)).align(Align.left);
        container.add(table).align(Align.left);
        container.row();

        shipInfoTable.add(container).spaceBottom(GameConstants.hToRelative(5)).spaceLeft(GameConstants.wToRelative(5)).align(Align.top);
}

    public void setState(ShipEditorState state) {
        this.state = state;
    }

    public boolean isVisible() {
        return visible;
    }

    public void loadParticipants() {
        participants.clear();
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle simulator = Gdx.files.absolute(pathPrefix + "/simulator.ini");
        if (simulator.exists()) {
            String text = simulator.readString("ISO-8859-1");
            String[] lines = text.split("\n");
            if (lines.length > 0 && !lines[0].isEmpty()) {
                int version = 0;
                int i = 0;
                if (lines[i].startsWith("V")) {
                    version = Integer.parseInt(lines[i].substring(1, lines[i].length()).trim());
                    i++;
                }
                if (version > 0) {
                    if (lines[i].startsWith("SPEED")) {
                        simulatorSpeed = Integer.parseInt(lines[i].substring(7, lines[i].length()).trim());
                        i++;
                    }
                }
                if (version > 1) {
                    if (lines[i].startsWith("LOOP")) {
                        numberOfSimulatorRuns = Integer.parseInt(lines[i].substring(6, lines[i].length()).trim());
                        i++;
                    }
                    if (lines[i].startsWith("ANOMALY")) {
                        int type = Integer.parseInt(lines[i].substring(9, lines[i].length()).trim());
                        if (type != -1)
                            simulatorAnomaly = new Anomaly(game, type);
                        i++;
                    }
                }
                int race = 0;
                for (; i < lines.length; i++) {
                    race = Integer.parseInt(lines[i].trim());
                    while (!lines[i + 1].trim().equals("$END_RACE$")) {
                        IntArray ar = participants.get(race, new IntArray());
                        ar.add(Integer.parseInt(lines[++i]));
                        participants.put(race, ar);
                    }
                    i++;
                }
            }
        }
    }

    public void saveParticipants() {
        if (participants.size != 0) {
            String pathPrefix = Gdx.files.getLocalStoragePath();
            FileHandle simulator = Gdx.files.absolute(pathPrefix + "/simulator.ini");
            String text = "V2" + "\n";
            text += "SPEED = " + simulatorSpeed + "\n";
            text += "LOOP = " + numberOfSimulatorRuns + "\n";
            int anomalyType = simulatorAnomaly == null ? -1 : simulatorAnomaly.getType().ordinal();
            text += "ANOMALY = " + anomalyType + "\n";

            for (Iterator<Entry<IntArray>> it = participants.entries().iterator(); it.hasNext();) {
                Entry<IntArray> e = it.next();
                text += e.key + "\n";
                for (int i = 0; i < e.value.size; i++) {
                    int id = e.value.get(i);
                    text += id + "\n";
                }
                text += "$END_RACE$" + "\n";
            }
            simulator.writeString(text, false, "ISO-8859-1");
        }
    }
}
