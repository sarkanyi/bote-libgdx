/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.nio.IntBuffer;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.BufferUtils;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;

public class BotE extends ScreenManager {
    public BotE(PlatformApiIntegration apiIntegrator) {
        this((AndroidIntegration) null, apiIntegrator);
    }

    public BotE(AndroidIntegration integrator, PlatformApiIntegration apiIntegrator) {
        super(integrator, apiIntegrator);
    }

    public BotE(String[] arg, PlatformApiIntegration apiIntegration) {
        super(arg, apiIntegration);
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_INFO);
        Gdx.graphics.setContinuousRendering(false);
        super.create();
        if (!loadDefaultAssets()) {
            return;
        }
        //initialize preferences
        switch (Gdx.app.getType()) {
            case Android:
                Gdx.input.setCatchBackKey(true);
                break;
            default:
                break;
        }
        IntBuffer buf = BufferUtils.newIntBuffer(16);
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buf);
        final int maxSize = buf.get(0);
        StringDB.Init();
        Init(maxSize); // initializes the ResourceManager
        setView(ViewTypes.MAIN_MENU, true);
    }

    @Override
    public void dispose() {
        saveGameSettings();
        super.dispose();
    }
}