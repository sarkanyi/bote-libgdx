/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;

public class ResearchOverview {
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table nameTable;
    private Table infoTable;
    private Array<ResearchWidget> widgets;
    private Array<Table> widgetTitles;

    public ResearchOverview(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("RESEARCH_MENUE"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(950, 556, 210, 230);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.top);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);

        widgets = new Array<ResearchWidget>();
        widgetTitles = new Array<Table>();
        int spaceX = 0;
        int spaceY = 0;
        int i = 0;
        for (ResearchType rt : ResearchType.values()) {
            if (rt != ResearchType.NONE && rt != ResearchType.UNIQUE) {
                if (i % 3 == 0 && i != 0) {
                    spaceY++;
                    spaceX = 0;
                }
                rect = GameConstants.coordsToRelative(30 + spaceX * 289, 532 - spaceY * 285, 245, 215);
                rect.x += xOffset;
                rect.y += yOffset;
                ResearchWidget widget = new ResearchWidget(rect, manager, stage, skin, rt);
                widgets.add(widget);

                Table title = new Table();
                title.setSkin(skin);
                rect = GameConstants.coordsToRelative(30 + spaceX * 289, 558 - spaceY * 285, 245, 25);
                title.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
                String text = String.format("%s - %s %d", StringDB.getString(rt.getKey()), StringDB.getString("LEVEL"),
                        playerRace.getEmpire().getResearch().getResearchLevel(rt));
                title.add(text, "mediumFont", normalColor);
                title.setVisible(false);
                stage.addActor(title);
                widgetTitles.add(title);
                i++;
                spaceX++;
            }
        }
    }

    public void show() {
        nameTable.setVisible(true);
        drawResearchInfo();
        for (ResearchWidget widget : widgets)
            widget.show();
        for (int i = 0; i < widgetTitles.size; i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            Table wt = widgetTitles.get(i);
            wt.clear();
            String text = String.format("%s - %s %d", StringDB.getString(rt.getKey()), StringDB.getString("LEVEL"),
                    playerRace.getEmpire().getResearch().getResearchLevel(rt));
            wt.add(text, "mediumFont", normalColor);
            wt.setVisible(true);
        }
    }

    public void hide() {
        nameTable.setVisible(false);
        infoTable.setVisible(false);
        for (ResearchWidget widget : widgets)
            widget.hide();
        for (Table wt : widgetTitles)
            wt.setVisible(false);
    }

    public void drawResearchInfo() {
        infoTable.clear();
        infoTable.add(StringDB.getString("RESEARCHPOINTS"), "mediumFont", normalColor);
        infoTable.row();
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.center);
        button.setSkin(skin);
        BitmapFont font = skin.getFont("mediumFont");
        String text = StringDB.getString("TOTAL").toUpperCase() + ":";
        button.add(text, "mediumFont", markColor);
        GlyphLayout layout = new GlyphLayout(font, text);
        float width = layout.width;
        text = String.format("%d %s", playerRace.getEmpire().getResearchPoints(), StringDB.getString("FP"));
        layout.setText(font, text);
        button.add(text, "mediumFont", markColor).spaceLeft(infoTable.getWidth() - width - layout.width);
        infoTable.add(button);
        infoTable.row();
        infoTable.add(StringDB.getString("RESEARCH_BONI"), "mediumFont", normalColor);
        for (ResearchType rt : ResearchType.values()) {
            if (rt != ResearchType.NONE && rt != ResearchType.UNIQUE) {
                infoTable.row();
                button = new Button(bs);
                button.align(Align.center);
                button.setSkin(skin);
                text = StringDB.getString(rt.getShortName()).toUpperCase() + ":";
                button.add(text, "mediumFont", markColor);
                layout.setText(font, text);
                width = layout.width;
                text = String.format("%d%%", playerRace.getEmpire().getResearch().getResearchBoni(rt));
                layout.setText(font, text);
                float sp = infoTable.getWidth() - width - layout.width;
                sp = Math.max(sp, 0);
                button.add(text, "mediumFont", markColor).spaceLeft(sp);
                infoTable.add(button);
            }
        }
        infoTable.setVisible(true);
    }
}
