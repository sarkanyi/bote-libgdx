/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.ui.screens.ResearchScreen;
import com.blotunga.bote.utils.ui.BaseTooltip;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class ResearchWidget implements ValueChangedEvent {
    private ResourceManager manager;
    private Skin skin;
    private Major playerRace;
    private Table widgetTable;
    private ResearchType type;
    private Color normalColor;
    private Array<String> loadedTextures;

    public ResearchWidget(Rectangle rect, ResourceManager manager, Stage stage, Skin skin, ResearchType rt) {
        this.manager = manager;
        this.skin = skin;
        this.type = rt;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;

        loadedTextures = new Array<String>();

        widgetTable = new Table();
        widgetTable.setSkin(skin);
        widgetTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        widgetTable.setVisible(false);
        stage.addActor(widgetTable);
    }

    public void show() {
        widgetTable.clear();
        widgetTable.setVisible(true);
        Research research = playerRace.getEmpire().getResearch();
        String text = "";
        Label label;
        ActorGestureListener listener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Research research = playerRace.getEmpire().getResearch();
                ((ResearchScreen) manager.getScreen()).showBottomView(type);
                if (count >= 2) {
                    research.setLock(type, !research.getLockStatus(type));
                    show();
                }
            }
        };
        if (type != ResearchType.UNIQUE)
            text = research.getResearchInfo().getTechName(type.getType());
        else
            text = research.getResearchInfo().getCurrentResearchComplex().getComplexName();
        label = new Label(text, skin, "mediumFont", normalColor);
        label.addListener(listener);
        widgetTable.add(label);        
        widgetTable.row();
        String path = "graphics/research/" + type.getImgName() + ".png";
        Image image = new Image(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(path))));
        image.addListener(listener);
        float imgWidth = GameConstants.wToRelative(120);
        float imgHeight = imgWidth / 1.2f;
        widgetTable.add(image).width((int) imgWidth).height((int) imgHeight);
        widgetTable.row();
        text = research.getLockStatus(type) ? StringDB.getString("LOCKED") : StringDB.getString("UNLOCKED");
        label = new Label(text, skin, "mediumFont", Color.WHITE);
        label.addListener(listener);
        widgetTable.add(label);
        widgetTable.row();
        text = String.format("%s: %d%%", StringDB.getString("PROGRESS"),
                research.getProgress(type, manager.getGamePreferences().researchSpeedFactor));
        label = new Label(text, skin, "mediumFont", normalColor);
        label.addListener(listener);
        widgetTable.add(label);
        widgetTable.row();
        text = String.format("%s: %d%%", StringDB.getString("ASSIGNMENT"), research.getPercentage(type));
        label = new Label(text, skin, "mediumFont", normalColor);
        label.addListener(listener);
        widgetTable.add(label);
        widgetTable.row();
        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
        TextureRegion dr = new TextureRegion(texture);

        float bheight = GameConstants.hToRelative(20);
        float pad = GameConstants.wToRelative(2);
        float bwidth = Math.round(GameConstants.wToRelative(10)) - (int) pad;
        PercentageWidget pcwidget = new PercentageWidget(0, skin, bwidth, bheight, pad, 20, 0, 100,
                research.getPercentage(type), dr, this);
        widgetTable.add(pcwidget.getWidget());
        loadedTextures.add(path);

        if(type != ResearchType.UNIQUE) {
            Texture tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
            BaseTooltip<Table> tooltip = BaseTooltip.createTableTooltip(image, tooltipTexture);
            image.setUserObject(tooltip);
            image.addListener(tooltip.getListener());
            generateTooltip(tooltip);
        }
    }

    public void generateTooltip(BaseTooltip<Table> tooltip){
        Research research = playerRace.getEmpire().getResearch();
        Table table = tooltip.getActor();
        Color headColor = playerRace.getRaceDesign().clrListMarkTextColor;
        Color textColor = playerRace.getRaceDesign().clrNormalText;
        String headerFont = "xlFont";
        String textFont = "largeFont";

        table.clearChildren();
        int[] researchLevels = research.getResearchLevels();
        researchLevels[type.getType()] += 1;
        Array<BuildingInfo> newBuildings = new Array<BuildingInfo>();
        Array<ShipInfo> newShips = new Array<ShipInfo>();
        Array<TroopInfo> newTroops = new Array<TroopInfo>();
        for(int i = 0; i < manager.getBuildingInfos().size; i++) {
            BuildingInfo buildingInfo = manager.getBuildingInfos().get(i);
            if(buildingInfo.getOwnerOfBuilding() == playerRace.getRaceBuildingNumber()) {
                if(buildingInfo.isBuildingBuildableNow(researchLevels)) {
                    int techLevel = buildingInfo.getNeededTechLevel(type);
                    if(techLevel != -1 && techLevel == researchLevels[type.getType()])
                        newBuildings.add(buildingInfo);
                }
            }
        }

        for(int i = 0; i < manager.getShipInfos().size; i++) {
            ShipInfo shipInfo = manager.getShipInfos().get(i);
            if(shipInfo.getRace() == playerRace.getRaceShipNumber())
                if(shipInfo.isThisShipBuildableNow(researchLevels)) {
                    int techLevel = shipInfo.getNeededTechLevel(type);
                    if(techLevel != -1 && techLevel == researchLevels[type.getType()])
                        newShips.add(shipInfo);
                }
        }

        for(int i = 0; i < manager.getTroopInfos().size; i++) {
            TroopInfo troopInfo = manager.getTroopInfos().get(i);
            if(troopInfo.getOwner().equals(playerRace.getRaceId()))
                if(troopInfo.isThisTroopBuildableNow(researchLevels)) {
                    int techLevel = troopInfo.getNeededTechlevel(type.getType());
                    if(techLevel != -1 && techLevel == researchLevels[type.getType()])
                        newTroops.add(troopInfo);
                }
        }

        String text = StringDB.getString("NEW_TECHNOLOGY_ALLOWS");
        Label l = new Label(text, skin, headerFont, headColor);
        table.add(l);
        table.row();

        text = StringDB.getString("RESEARCHEVENT_NEWBUILDINGS");
        l = new Label(text, skin, textFont, headColor);
        table.add(l);
        table.row();
        for(int i = 0; i < newBuildings.size; i++) {
            text = newBuildings.get(i).getBuildingName();
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }
        if(newBuildings.size == 0) {
            text = StringDB.getString("NONE");
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }

        text = StringDB.getString("RESEARCHEVENT_NEWSHIPS_AND_TROOPS");
        l = new Label(text, skin, textFont, headColor);
        table.add(l);
        table.row();
        for(int i = 0; i < newShips.size; i++) {
            ShipInfo si = newShips.get(i);
            text = si.getShipClass() + "-" + StringDB.getString("CLASS") + " (" + si.getShipTypeAsString() + ")";
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }
        for(int i = 0; i < newTroops.size; i++) {
            text = newTroops.get(i).getName();
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }

        if(newShips.size == 0 && newTroops.size == 0) {
            text = StringDB.getString("NONE");
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
        }
    }

    public void hide() {
        widgetTable.setVisible(false);
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
    }

    @Override
    public void valueChanged(int typeID, int value) {
        //typeID is always 0 in this case
        Research research = playerRace.getEmpire().getResearch();
        research.setPercentage(type, value);
        manager.getScreen().show();
    }
}
