/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ui.screens.ResearchScreen;

public class ResearchSpecialView {
    private ScreenManager manager;
    private Skin skin;
    private Color normalColor;
    private Color markColor;
    private Major playerRace;
    private Table nameTable;
    private ResearchWidget widget;
    private Table infoTable;
    private ScrollPane fieldInfoPane[];
    private Table fieldInfo[];
    private Table fieldInfosHeader[];
    private TextButton fieldButtons[];

    public ResearchSpecialView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 810, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        rect = GameConstants.coordsToRelative(30, 540, 245, 215);
        rect.x += xOffset;
        rect.y += yOffset;
        widget = new ResearchWidget(rect, manager, stage, skin, ResearchType.UNIQUE);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(430, 560, 420, 255);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.top);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);

        fieldInfosHeader = new Table[3];
        for (int i = 0; i < 3; i++) {
            rect = GameConstants.coordsToRelative(30 + i * 291, 258, 245, 25);//190
            fieldInfosHeader[i] = new Table();
            fieldInfosHeader[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            fieldInfosHeader[i].align(Align.top);
            fieldInfosHeader[i].setSkin(skin);
            stage.addActor(fieldInfosHeader[i]);
            fieldInfosHeader[i].setVisible(false);
        }

        fieldInfoPane = new ScrollPane[3];
        fieldInfo = new Table[3];
        for (int i = 0; i < 3; i++) {
            rect = GameConstants.coordsToRelative(30 + i * 291, 233, 245, 170);
            fieldInfo[i] = new Table();
            fieldInfo[i].align(Align.top);
            fieldInfoPane[i] = new ScrollPane(fieldInfo[i], skin);
            fieldInfoPane[i].setVariableSizeKnobs(false);
            fieldInfoPane[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            fieldInfoPane[i].setScrollingDisabled(true, false);
            stage.addActor(fieldInfoPane[i]);
            fieldInfoPane[i].setVisible(false);
        }

        fieldButtons = new TextButton[3];
        for (int i = 0; i < 3; i++) {
            rect = GameConstants.coordsToRelative(78 + i * 291, 43, 150, 30);
            TextButtonStyle style = skin.get("default", TextButtonStyle.class);
            fieldButtons[i] = new TextButton(StringDB.getString("BTN_SELECT"), style);
            fieldButtons[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            fieldButtons[i].setUserObject(i);
            fieldButtons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    int idx = (Integer) event.getListenerActor().getUserObject();
                    ResearchInfo ri = playerRace.getEmpire().getResearch().getResearchInfo();
                    ri.setUniqueResearchChoosePossibility(idx + 1);
                    hide();
                    show();
                }
            });
            fieldButtons[i].setVisible(false);
            stage.addActor(fieldButtons[i]);
        }
    }

    public void show() {
        ResearchInfo ri = playerRace.getEmpire().getResearch().getResearchInfo();
        nameTable.clear();
        String text;
        text = String.format("%s - %s", StringDB.getString("SPECIAL_RESEARCH"), ri.getCurrentResearchComplex()
                .getComplexName());
        nameTable.add(text, "hugeFont", normalColor);
        nameTable.setVisible(true);

        widget.show();

        infoTable.clear();
        text = ri.getCurrentResearchComplex().getComplexName();
        Label infoLabel = new Label(text, skin, "mediumFont", markColor);
        infoLabel.setAlignment(Align.top);
        infoTable.add(infoLabel).height((int) GameConstants.hToRelative(35));
        infoTable.row();
        text = ri.getCurrentResearchComplex().getComplexDescription();
        infoLabel = new Label(text, skin, "mediumFont", normalColor);
        infoLabel.setWrap(true);
        infoLabel.setAlignment(Align.top, Align.center);
        infoTable.add(infoLabel).width((int) infoTable.getWidth());
        infoTable.setVisible(true);

        for (int i = 0; i < 3; i++) {
            fieldInfosHeader[i].clear();
            fieldInfo[i].clear();
            if (!ri.getChoiceTaken()
                    || ri.getCurrentResearchComplex().getFieldStatus(i + 1) == ResearchStatus.RESEARCHING) {
                text = ri.getCurrentResearchComplex().getFieldName(i + 1);
                infoLabel = new Label(text, skin, "mediumFont", markColor);
                infoLabel.setAlignment(Align.top);
                fieldInfosHeader[i].add(infoLabel).height((int) GameConstants.hToRelative(25));
                text = ri.getCurrentResearchComplex().getFieldDescription(i + 1);
                infoLabel = new Label(text, skin, "mediumFont", normalColor);
                infoLabel.setWrap(true);
                infoLabel.setAlignment(Align.top, Align.center);
                fieldInfo[i].add(infoLabel).width(fieldInfosHeader[i].getWidth());
                if (!ri.getChoiceTaken()) {
                    fieldButtons[i].setVisible(true);
                }
                fieldInfosHeader[i].setVisible(true);
                fieldInfoPane[i].setVisible(true);
            }
        }

        ((ResearchScreen) manager.getScreen()).showBottomView(ResearchType.UNIQUE);
    }

    public void hide() {
        nameTable.setVisible(false);
        widget.hide();
        infoTable.setVisible(false);
        for (int i = 0; i < 3; i++) {
            fieldInfosHeader[i].setVisible(false);
            fieldButtons[i].setVisible(false);
            fieldInfoPane[i].setVisible(false);
        }
    }
}
