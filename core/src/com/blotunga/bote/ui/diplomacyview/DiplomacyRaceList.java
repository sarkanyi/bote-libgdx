/*
 * Copyright (C) 2014-2022 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import java.util.Comparator;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Sort;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.ui.BaseTooltip;
import com.blotunga.bote.utils.ui.ScrollEvent;
import com.blotunga.bote.utils.ui.VerticalScroller;

public class DiplomacyRaceList implements ScrollEvent {
    private class RaceCompareByName implements Comparator<Race> {
        @Override
        public int compare(Race o1, Race o2) {
            if (o1.isMajor() && !o2.isMajor())
                return 1;
            if (!o1.isMajor() && o2.isMajor())
                return -1;
            if (!o1.isAlien() && o2.isAlien())
                return -1;
            if (!o2.isAlien() && o1.isAlien())
                return 1;

            return o1.getName().compareTo(o2.getName());
        }
    }

    private class RaceCompareByAgreement implements Comparator<Race> {
        @Override
        public int compare(Race o1, Race o2) {
            if (o1.isMinor() && ((Minor) o1).isSubjugated() && o2.isMinor() && ((Minor) o2).isSubjugated())
                return o1.getName().compareTo(o2.getName());
            if (o1.isMinor() && ((Minor) o1).isSubjugated())
                return 1;
            if (o2.isMinor() && ((Minor) o2).isSubjugated())
                return -1;
            if (playerRace.getAgreement(o1.getRaceId()) == playerRace.getAgreement(o2.getRaceId()))
                return o1.getName().compareTo(o2.getName());
            return playerRace.getAgreement(o1.getRaceId()).getType() < playerRace.getAgreement(o2.getRaceId())
                    .getType() ? -1 : 1;
        }
    }

    private enum SortType {
        BY_NAME,
        BY_AGREEMENT
    }

    private Skin skin;
    private ScreenManager manager;
    private Stage stage;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Color markPenColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private SortType sortType;

    private TextButton sortButton;
    private VerticalScroller racesListPane;
    private Table racesListTable;
    private Image raceImage;
    private Array<Race> raceList;
    private Race clickedRace;
    private Button raceSelection = null;
    private DiplomacyInfo clickedInfo;
    private Array<Button> raceItems;
    private Array<String> imgPaths;
    private Label raceSentiment;
    private Table raceSentimentTable;
    private boolean showRaces;
    private Array<DiplomacyInfo> incomingInfos;

    //tooltip
    private Texture tooltipTexture;
    private Color headerColor;
    private Color textColor;
    private String headerFont = "xlFont";
    private String textFont = "largeFont";
    private Table ttable;
    private boolean isVisible;

    public DiplomacyRaceList(ScreenManager manager, Stage stage, float xOffset, float yOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = manager.getSkin();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        markPenColor = playerRace.getRaceDesign().clrListMarkPenColor;
        headerColor = playerRace.getRaceDesign().clrListMarkTextColor;
        textColor = normalColor;

        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        selectTexture = manager.getUiTexture("listselect");

        racesListTable = new Table();
        racesListTable.align(Align.topLeft);

        racesListPane = new VerticalScroller(racesListTable);
        racesListPane.setStyle(skin.get("default", ScrollPaneStyle.class));
        racesListPane.setVariableSizeKnobs(false);
        racesListPane.setFadeScrollBars(false);
        racesListPane.setEventHandler(this);
        Rectangle rect = GameConstants.coordsToRelative(15, 535, 164, 450);
        racesListPane.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        racesListPane.setScrollingDisabled(true, false);
        stage.addActor(racesListPane);
        racesListPane.setVisible(false);

        rect = GameConstants.coordsToRelative(44, 563, 90, 25);
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        sortButton = new TextButton("A-Z", smallButtonStyle);
        sortButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        sortButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (sortType == SortType.BY_NAME) {
                    button.setText(StringDB.getString("SORT2"));
                    sortType = SortType.BY_AGREEMENT;
                } else {
                    button.setText("A-Z");
                    sortType = SortType.BY_NAME;
                }
                if (showRaces) {
                    refreshInfo();
                    showRaceList(null);
                } else
                    showRaceList(incomingInfos);
            }
        });
        sortButton.setVisible(false);
        stage.addActor(sortButton);

        rect = GameConstants.coordsToRelative(815, 535, 330, 245);
        raceImage = new Image();
        raceImage.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(raceImage);
        raceImage.setVisible(false);
        ttable = BaseTooltip.createTableTooltip(raceImage, tooltipTexture).getActor();
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1)
                    jumpToRaceHome();
            }
        };
        raceImage.addListener(gestureListener);

        rect = GameConstants.coordsToRelative(815, 288, 330, 20);
        raceSentiment = new Label("", skin, "mediumFont", Color.WHITE);
        raceSentiment.setAlignment(Align.center, Align.center);
        raceSentiment.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(raceSentiment);
        raceSentiment.setVisible(false);

        rect = GameConstants.coordsToRelative(721, 532, 35, 240);
        raceSentimentTable = new Table();
        raceSentimentTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        raceSentimentTable.setVisible(false);
        raceSentimentTable.setOrigin(rect.width / 2, rect.height / 2);
        stage.addActor(raceSentimentTable);

        imgPaths = new Array<String>();
        raceList = new Array<Race>();
        sortType = SortType.BY_NAME;
        clickedRace = null;
        clickedInfo = null;
        raceItems = new Array<Button>();
        showRaces = true;
        isVisible = false;
    }

    private void refreshInfo() {
        raceList.clear();
        Array<Major> majorl = new Array<Major>();
        Array<Minor> minorl = new Array<Minor>();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();

        for (int i = 0; i < majors.size; i++)
            if (playerRace.isRaceContacted(majors.getKeyAt(i)) && majors.getValueAt(i).getEmpire().countSystems() > 0)
                majorl.add(majors.getValueAt(i));

        for (Iterator<ObjectMap.Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            if (playerRace.isRaceContacted(e.key))
                minorl.add(e.value);
        }
        if (sortType == SortType.BY_NAME) {
            Sort sortByName = new Sort();
            sortByName.sort(majorl, new RaceCompareByName());
            sortByName.sort(minorl, new RaceCompareByName());
        }
        raceList.addAll(majorl);
        raceList.addAll(minorl);
        if (sortType == SortType.BY_AGREEMENT) {
            Sort sortByAgr = new Sort();
            sortByAgr.sort(raceList, new RaceCompareByAgreement());
        }
    }

    public void showRaceList(final Array<DiplomacyInfo> incomingInfos) {
        isVisible = true;
        stage.setKeyboardFocus(racesListTable);
        stage.setScrollFocus(racesListPane);
        sortButton.setVisible(true);
        int pad = (int) GameConstants.wToRelative(7);
        if (incomingInfos == null) {
            showRaces = true;
            refreshInfo();
        } else {
            showRaces = false;
            this.incomingInfos = incomingInfos;
        }

        raceItems.clear();
        racesListTable.clear();

        int size = showRaces ? raceList.size : incomingInfos.size;
        for (int i = 0; i < size; i++) {
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.align(Align.left);
            Label rlabel;
            Race race;
            if (showRaces) {
                race = raceList.get(i);
                rlabel = new Label(race.getName(), skin, "normalFont", Color.WHITE);
                Pair<String, Color> status = ((DiplomacyScreen) manager.getScreen()).printDiplomacyStatus(playerRace, race);
                rlabel.setColor(status.getSecond());
                rlabel.setUserObject(race);
            } else {
                race = manager.getRaceController().getRace(incomingInfos.get(i).fromRace);
                rlabel = new Label(race.getName(), skin, "normalFont", Color.WHITE);
                rlabel.setColor(normalColor);
                rlabel.setUserObject(incomingInfos.get(i));
            }
            button.setUserObject(rlabel);
            button.add().width(pad);
            button.add(rlabel).width((int) (racesListPane.getWidth() - pad * 2));
            button.add().width(pad);
            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markRaceListSelected(b);
                    if (count > 1)
                        jumpToRaceHome();
                }
            };
            button.addListener(gestureListener);
            race.getTooltip(BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin, headerFont, headerColor, textFont, textColor, false);
            if (showRaces && sortType == SortType.BY_NAME) {
                boolean drawLine = false;
                if (race.isAlien()) {
                    if (i != 0 && !raceList.get(i - 1).isAlien()) {
                        drawLine = true;
                    }
                } else if (race.isMinor()) {
                    if (i != 0 && (!raceList.get(i - 1).isMinor() || (raceList.get(i - 1).isAlien()))) {
                        drawLine = true;
                    }
                }
                if (drawLine) {
                    Table table = new Table();
                    Texture tex = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class);
                    Sprite sp = new Sprite(tex);
                    sp.setColor(markPenColor);
                    Drawable newDrawable = new SpriteDrawable(sp);
                    Image img = new Image(newDrawable);
                    table.add(img).height((int) GameConstants.hToRelative(3)).width((int) racesListPane.getWidth());
                    racesListTable.add(table);
                    racesListTable.row();
                }
            }
            racesListTable.add(button);
            racesListTable.row();
            raceItems.add(button);
        }

        racesListTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int selectedItem = raceItems.indexOf(raceSelection, true);
                if (raceItems.size < selectedItem)
                    selectedItem = raceItems.size - 1;
                Button b = raceItems.get(selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = raceItems.size - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += racesListPane.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= racesListPane.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.ENTER:
                        jumpToRaceHome();
                        return true;
                }

                if (DiplomacyRaceList.this.isVisible()) {
                    if (selectedItem >= raceItems.size)
                        selectedItem = raceItems.size - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = raceItems.get(selectedItem);
                    markRaceListSelected(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        racesListTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        racesListPane.setVisible(true);
        if (size == 0) //if there are no incoming news, reset clicked info
            clickedInfo = null;

        stage.draw();
        Button btn = null;
        if (showRaces) {
            if (clickedRace != null) {
                for (Button b : raceItems) {
                    if (clickedRace.equals(getRaces(b))) {
                        btn = b;
                        break;
                    }
                }
            }
        } else {
            if (clickedInfo != null) {
                for (Button b : raceItems) {
                    DiplomacyInfo info = getDiploInfo(b);
                    if (info.type == clickedInfo.type && info.flag == clickedInfo.flag
                            && info.fromRace.equals(clickedInfo.fromRace) && info.toRace.equals(clickedInfo.toRace)) {
                        btn = b;
                        break;
                    }
                }
            }
        }
        if (btn == null) {
            if (raceItems.size > 0) {
                btn = raceItems.get(0);
                clickedRace = getRaces(btn);
                if (!showRaces)
                    clickedInfo = getDiploInfo(btn);
            }
        }
        if (btn != null)
            markRaceListSelected(btn);
    }

    protected boolean isVisible() {
        return isVisible;
    }

    public void hide() {
        isVisible = false;
        sortButton.setVisible(false);
        racesListPane.setVisible(false);
        raceImage.setVisible(false);
        raceSentiment.setVisible(false);
        raceSentimentTable.setVisible(false);
        for (String path : imgPaths)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        imgPaths.clear();
    }

    private void drawRaceImage(Race race) {
        String path = "graphics/races/" + race.getGraphicFileName() + ".jpg";
        imgPaths.add(path);
        raceImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(path))));
        raceImage.setVisible(true);
        race.getTooltip(ttable, skin, headerFont, headerColor, textFont, textColor, true);
    }

    private void drawRaceSentiment(Race race) {
        raceSentimentTable.clear();
        if (race.isMinor() || race.isMajor() && !((Major) race).isHumanPlayer()) {
            int relation = race.getRelation(playerRace.getRaceId());

            Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
            TextureRegion dr = new TextureRegion(texture);
            Color fontColor = null;
            float pad = GameConstants.hToRelative(2);
            float imgHeight = Math.round(raceSentimentTable.getHeight() / 20) - (int) pad;
            float imgWidth = raceSentimentTable.getWidth();
            for (int i = 19; i >= 0; i--) {
                Sprite sprite = new Sprite(dr);
                Color color;
                if (race.getRelation(playerRace.getRaceId()) * 2 / 10 > i) {
                    color = new Color((250 - i * 12) / 255.0f, (i * 12) / 255.0f, 0.0f, 200 / 255.0f);
                    if (fontColor == null)
                        fontColor = new Color((250 - i * 12) / 255.0f, (i * 12) / 255.0f, 0.0f, 1.0f);
                } else
                    color = new Color(100 / 255.0f, 100 / 255.0f, 100 / 255.0f, 100 / 255.0f);
                sprite.setColor(color);
                Drawable newDrawable = new SpriteDrawable(sprite);
                Image relImage = new Image(newDrawable);
                relImage.setTouchable(Touchable.disabled);
                if (i != 19)
                    raceSentimentTable.row();
                raceSentimentTable.add(relImage).width((int) imgWidth).height((int) imgHeight);
                if (i != 0) {
                    raceSentimentTable.row();
                    raceSentimentTable.add().height((int) pad);
                }
            }
            if (race.getRelation(playerRace.getRaceId()) * 2 / 10 == 0)
                fontColor = new Color(Color.RED);

            String text = "";
            if (relation < 5)
                text = StringDB.getString("HATEFUL");
            else if (relation < 15)
                text = StringDB.getString("FURIOUS");
            else if (relation < 25)
                text = StringDB.getString("HOSTILE");
            else if (relation < 35)
                text = StringDB.getString("ANGRY");
            else if (relation < 45)
                text = StringDB.getString("NOT_COOPERATIVE");
            else if (relation < 55)
                text = StringDB.getString("NEUTRAL");
            else if (relation < 65)
                text = StringDB.getString("COOPERATIVE");
            else if (relation < 75)
                text = StringDB.getString("FRIENDLY");
            else if (relation < 85)
                text = StringDB.getString("OPTIMISTIC");
            else if (relation < 95)
                text = StringDB.getString("ENTHUSED");
            else
                text = StringDB.getString("DEVOTED");
            raceSentiment.setText(text);
            raceSentiment.setColor(fontColor);
            raceSentiment.setVisible(true);
            raceSentimentTable.setVisible(true);
        }
    }

    private void markRaceListSelected(Button b) {
        if (!(manager.getScreen() instanceof DiplomacyScreen))
            return;

        if (raceSelection != null) {
            raceSelection.getStyle().up = null;
            raceSelection.getStyle().down = null;
            ((Label) raceSelection.getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = raceSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        raceSelection = b;

        //show image
        Race race = getRaces(b);
        drawRaceImage(race);
        drawRaceSentiment(race);

        clickedRace = race;
        if (!showRaces)
            clickedInfo = getDiploInfo(b);
        ((DiplomacyScreen) manager.getScreen()).updateRaceListChange();

        float scrollerHeight = racesListPane.getScrollHeight();
        float scrollerPos = racesListPane.getScrollY();
        int selectedItem = raceItems.indexOf(b, true);
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            racesListPane.setScrollY(b.getHeight() * selectedItem - racesListPane.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            racesListPane.setScrollY(b.getHeight() * (selectedItem - racesListPane.getScrollHeight() / b.getHeight() + 1));
    }

    private Race getRaces(Button b) {
        if (showRaces)
            return (Race) ((Label) b.getUserObject()).getUserObject();
        else
            return manager.getRaceController().getRace(
                    ((DiplomacyInfo) ((Label) b.getUserObject()).getUserObject()).fromRace);
    }

    private DiplomacyInfo getDiploInfo(Button b) {
        return ((DiplomacyInfo) ((Label) b.getUserObject()).getUserObject());
    }

    private void jumpToRaceHome() {
        if (clickedRace != null) {
            String home = clickedRace.getHomeSystemName();
            if (!home.isEmpty())
                if (!clickedRace.getCoordinates().equals(new IntPoint())) {
                    StarSystem ss = manager.getUniverseMap().getStarSystemAt(clickedRace.getCoordinates());
                    if (ss.getName().equals(home) && ss.getKnown(playerRace.getRaceId())) {
                        manager.getUniverseMap().setSelectedCoordValue(ss.getCoordinates());
                        manager.setView(ViewTypes.GALAXY_VIEW);
                    }
                }
        }
    }

    public Race getSelectedRace() {
        return clickedRace;
    }

    public DiplomacyInfo getSelectedDiploInfo() {
        return clickedInfo;
    }

    public void setDiploInfo(DiplomacyInfo info) {
        this.clickedInfo = info;
    }

    @Override
    public void scrollEventY(float pixelsY) {
        if (!showRaces) {
            ((DiplomacyScreen) manager.getScreen()).getDiplomacyInView().setScrollY(pixelsY);
        }
    }

    public void setScrollY(float pixelsY) {
        racesListPane.setScrollY(pixelsY);
    }
}
