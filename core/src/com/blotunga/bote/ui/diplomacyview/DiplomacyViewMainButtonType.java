/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import com.badlogic.gdx.utils.IntMap;

public enum DiplomacyViewMainButtonType {
    INFO_BUTTON("BTN_INFORMATION", "diploinfomenu", 0),
    OFFER_BUTTON("OFFER", "diplooutmenu", 1),
    RECEIVED_BUTTON("RECEIPT", "diploinmenu", 2);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<DiplomacyViewMainButtonType> intToTypeMap = new IntMap<DiplomacyViewMainButtonType>();

    DiplomacyViewMainButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (DiplomacyViewMainButtonType et : DiplomacyViewMainButtonType.values()) {
            intToTypeMap.put(et.ord, et);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static DiplomacyViewMainButtonType fromInt(int i) {
        DiplomacyViewMainButtonType et = intToTypeMap.get(i);
        return et;
    }
}
