/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.GenDiploMessage;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.StarSystem.SystemSortType;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.utils.IntPoint;

public class DiplomacyOutView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Table nameTable;
    private Table chooseText;
    private Table offerTypeButtons;
    private TextButton sendButton;
    private DiplomacyInfo outGoingInfo;
    private boolean showSendButton;
    private Table creditsLabel;
    private Button creditsButton;
    private Table resourceLabel;
    private Button resourceButton;
    private ResourceTypes whichResourceIsChosen;
    private IntPoint resourceFromSystem;
    private Table resourceInfoTable;
    private Table resourceSelect;
    private Table systemSelect;
    private Table miscSelector;
    private Array<StarSystem> systems;

    public DiplomacyOutView(final ScreenManager manager, Stage stage, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = manager.getSkin();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;

        outGoingInfo = new DiplomacyInfo();

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 800, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("DIPLOMACY_MENUE_OFFERS"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        chooseText = new Table();
        rect = GameConstants.coordsToRelative(260, 540, 320, 25);
        chooseText.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        chooseText.align(Align.center);
        chooseText.setSkin(skin);
        stage.addActor(chooseText);
        chooseText.add(StringDB.getString("CHOOSE_OFFER") + ":", "normalFont", normalColor);
        chooseText.setVisible(false);

        offerTypeButtons = new Table();
        rect = GameConstants.coordsToRelative(235, 505, 372, 200);
        offerTypeButtons.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        offerTypeButtons.align(Align.center);
        offerTypeButtons.setSkin(skin);
        stage.addActor(offerTypeButtons);
        offerTypeButtons.setVisible(false);

        TextButton.TextButtonStyle style = skin.get("default", TextButton.TextButtonStyle.class);
        sendButton = new TextButton(StringDB.getString("BTN_SEND"), style);
        rect = GameConstants.coordsToRelative(975, 50, 180, 35);
        sendButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(sendButton);
        sendButton.setVisible(false);
        sendButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                String label = StringDB.getString("BTN_SEND");
                if (button.getText().toString().equals(label)) {
                    outGoingInfo.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
                    outGoingInfo.sendRound = manager.getCurrentRound();
                    playerRace.getOutgoingDiplomacyNews().add(new DiplomacyInfo(outGoingInfo));
                    ((DiplomacyScreen) manager.getScreen()).takeorGetBackResLat(outGoingInfo, true);
                } else {
                    for (int i = 0; i < playerRace.getOutgoingDiplomacyNews().size; i++) {
                        DiplomacyInfo di = playerRace.getOutgoingDiplomacyNews().get(i);
                        Race race = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
                        if (di.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()
                                && di.toRace.equals(race.getRaceId()) && di.fromRace.equals(playerRace.getRaceId())) {
                            outGoingInfo = di;
                            playerRace.getOutgoingDiplomacyNews().removeIndex(i);
                            ((DiplomacyScreen) manager.getScreen()).takeorGetBackResLat(outGoingInfo, false);
                            break;
                        }
                    }
                }
                show();
            }
        });
        showSendButton = true;

        creditsLabel = new Table();
        rect = GameConstants.coordsToRelative(230, 279, 274, 20);
        creditsLabel.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        creditsLabel.setSkin(skin);
        creditsLabel.align(Align.topLeft);
        stage.addActor(creditsLabel);
        creditsLabel.setVisible(false);

        Button.ButtonStyle bs = new Button.ButtonStyle();
        creditsButton = new Button(bs);
        rect = GameConstants.coordsToRelative(225, 256, 268, 20);
        creditsButton
                .setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(creditsButton);
        creditsButton.setVisible(false);

        whichResourceIsChosen = ResourceTypes.TITAN;
        resourceFromSystem = new IntPoint();
        resourceLabel = new Table();
        rect = GameConstants.coordsToRelative(230, 212, 274, 20);
        resourceLabel
                .setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        resourceLabel.setSkin(skin);
        resourceLabel.align(Align.topLeft);
        stage.addActor(resourceLabel);
        resourceLabel.setVisible(false);

        resourceButton = new Button(bs);
        rect = GameConstants.coordsToRelative(225, 190, 268, 20);
        resourceButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        stage.addActor(resourceButton);
        resourceButton.setVisible(false);

        resourceInfoTable = new Table();
        rect = GameConstants.coordsToRelative(230, 172, 274, 20);
        resourceInfoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        resourceInfoTable.setSkin(skin);
        resourceInfoTable.align(Align.topLeft);
        stage.addActor(resourceInfoTable);
        resourceInfoTable.setVisible(false);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        resourceSelect = new Table();
        rect = GameConstants.coordsToRelative(567, 215, 134, 50);
        resourceSelect.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        resourceSelect.setSkin(skin);
        resourceSelect.align(Align.center);
        stage.addActor(resourceSelect);
        float height = GameConstants.hToRelative(25);
        resourceSelect.add(StringDB.getString("RESOURCE"), "mediumFont", normalColor).height((int) height);
        resourceSelect.row();
        TextButton resButton = new TextButton(StringDB.getString(whichResourceIsChosen.getName()), smallButtonStyle);
        resButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (button == 1)
                    updateResource(event.getListenerActor(), -1);
                else
                    updateResource(event.getListenerActor(), 1);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                updateResource(actor, -1);
                return true;
            }

        });
        resButton.setName("RESOURCE_NAME");
        resourceSelect.add(resButton).height((int) height);
        resourceSelect.setVisible(false);

        systemSelect = new Table();
        rect = GameConstants.coordsToRelative(567, 145, 134, 50);
        systemSelect.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        systemSelect.setSkin(skin);
        systemSelect.align(Align.center);
        stage.addActor(systemSelect);
        systemSelect.add(StringDB.getString("FROM_SYSTEM"), "mediumFont", normalColor).height((int) height);
        systemSelect.row();
        TextButton systemButton = new TextButton("", smallButtonStyle);
        systemSelect.setUserObject(systemButton);
        systemButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (showSendButton) {
                    TextButton btn = (TextButton) event.getListenerActor();
                    int increment = 1;
                    if (button == 1)
                        increment = -1;
                    btn.setText(manager.getUniverseMap().getStarSystemAt(findNexSystem(increment)).getName());
                    show();
                }
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                if (showSendButton) {
                    TextButton btn = (TextButton) actor;
                    int increment = -1;
                    btn.setText(manager.getUniverseMap().getStarSystemAt(findNexSystem(increment)).getName());
                    show();
                }
                return true;
            }
        });
        systemButton.setName("RESOURCE_FROM_SYSTEM");
        systemSelect.add(systemButton).height((int) height);
        systemSelect.setVisible(false);

        miscSelector = new Table();
        rect = GameConstants.coordsToRelative(567, 280, 134, 50);
        miscSelector.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        miscSelector.setSkin(skin);
        miscSelector.align(Align.center);
        stage.addActor(miscSelector);
        miscSelector.setVisible(false);

        systems = new Array<StarSystem>();
    }

    private void updateResource(Actor actor, int increment) {
        if (showSendButton) {
            TextButton btn = (TextButton) actor;
            int ord = whichResourceIsChosen.getType();
            outGoingInfo.resources[ord] = 0;
            ord += increment;
            if (ord < 0)
                ord = ResourceTypes.DERITIUM.getType();
            if (ord > ResourceTypes.DERITIUM.getType())
                ord = 0;
            whichResourceIsChosen = ResourceTypes.fromResourceTypes(ord);
            btn.setText(StringDB.getString(whichResourceIsChosen.getName()));
            resourceFromSystem = new IntPoint(); //reset, it will be recalculated
            show();
        }
    }

    private IntPoint findNexSystem(int increment) {
        int current = -1;
        for (int i = 0; i < systems.size; i++)
            if (systems.get(i).getCoordinates().equals(resourceFromSystem)) {
                current = i;
                break;
            }
        if (current != -1)
            current += increment;
        if (current == systems.size)
            current = 0;
        if (current == -1)
            current = systems.size - 1;
        resourceFromSystem = systems.get(current).getCoordinates();
        Arrays.fill(outGoingInfo.resources, 0); // needed to change the label if quantity was set
        return resourceFromSystem;
    }

    private void initSystems() {
        systems.clear();
        Array<IntPoint> systemList = playerRace.getEmpire().getSystemList();
        for (IntPoint p : systemList)
            systems.add(manager.getUniverseMap().getStarSystemAt(p));
        switch(whichResourceIsChosen) {
            case CRYSTAL:
                StarSystem.setSortType(SystemSortType.BY_CRYSTALSTORE);
                break;
            case DERITIUM:
                StarSystem.setSortType(SystemSortType.BY_DERITIUMSTORE);
                break;
            case DEUTERIUM:
                StarSystem.setSortType(SystemSortType.BY_DEUTERIUMSTORE);
                break;
            case DURANIUM:
                StarSystem.setSortType(SystemSortType.BY_DURANIUMSTORE);
                break;
            case IRIDIUM:
                StarSystem.setSortType(SystemSortType.BY_IRIDIUMSTORE);
                break;
            case TITAN:
                StarSystem.setSortType(SystemSortType.BY_TITANSTORE);
                break;
            default:
                break;
        }

        systems.sort();
        systems.reverse();
    }
    public void show() {
        hide();
        initSystems();

        if (resourceFromSystem.equals(new IntPoint())
                || !manager.getUniverseMap().getStarSystemAt(resourceFromSystem).getOwnerId()
                        .equals(playerRace.getRaceId())) {
            if (systems.size > 0)
                resourceFromSystem = systems.get(0).getCoordinates();
        }
        ((TextButton) systemSelect.getUserObject()).setText(manager.getUniverseMap()
                .getStarSystemAt(resourceFromSystem).getName());

        nameTable.setVisible(true);
        chooseText.setVisible(true);
        Race race = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
        if (race != null) {
            showSendButton = (playerRace.getPendingOffer(race.getRaceId()) == null);
            if (!showSendButton)
                outGoingInfo = playerRace.getPendingOffer(race.getRaceId());
            drawOfferMainButtons(race);
            drawOffer(race);
        }
    }

    public void hide() {
        nameTable.setVisible(false);
        chooseText.setVisible(false);
        offerTypeButtons.setVisible(false);
        sendButton.setVisible(false);
        creditsLabel.setVisible(false);
        creditsButton.setVisible(false);
        resourceLabel.setVisible(false);
        resourceButton.setVisible(false);
        resourceInfoTable.setVisible(false);
        resourceSelect.setVisible(false);
        systemSelect.setVisible(false);
        miscSelector.setVisible(false);
    }

    private void drawOfferMainButtons(Race race) {
        offerTypeButtons.clear();
        TextButton.TextButtonStyle style = skin.get("default", TextButton.TextButtonStyle.class);
        float hpad = GameConstants.wToRelative(10);
        float buttonWidth = offerTypeButtons.getWidth() / 2 - hpad;
        float vpad = GameConstants.hToRelative(5);
        float buttonHeight = offerTypeButtons.getHeight() / 5 - vpad;
        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (!button.isDisabled()) {
                    Race race = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
                    outGoingInfo.reset();
                    outGoingInfo.coord = resourceFromSystem;
                    outGoingInfo.type = (DiplomaticAgreement) button.getUserObject();
                    outGoingInfo.fromRace = playerRace.getRaceId();
                    outGoingInfo.toRace = race.getRaceId();
                    show();
                }
            }
        };
        TextButton button = new TextButton(StringDB.getString("BTN_TRADECONTRACT"), style);
        button.setUserObject(DiplomaticAgreement.TRADE);
        button.addListener(listener);
        disableIfNeeded(button, DiplomaticAgreement.TRADE);
        offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                .spaceRight((int) hpad);
        button = new TextButton(StringDB.getString("BTN_FRIENDSHIP"), style);
        button.setUserObject(DiplomaticAgreement.FRIENDSHIP);
        button.addListener(listener);
        disableIfNeeded(button, DiplomaticAgreement.FRIENDSHIP);
        offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
        offerTypeButtons.row();
        button = new TextButton(StringDB.getString("BTN_COOPERATION"), style);
        button.setUserObject(DiplomaticAgreement.COOPERATION);
        button.addListener(listener);
        disableIfNeeded(button, DiplomaticAgreement.COOPERATION);
        offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                .spaceRight((int) hpad);
        button = new TextButton(StringDB.getString("BTN_ALLIANCE"), style);
        button.setUserObject(DiplomaticAgreement.ALLIANCE);
        button.addListener(listener);
        disableIfNeeded(button, DiplomaticAgreement.ALLIANCE);
        offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
        offerTypeButtons.row();
        if (race.isMajor()) {
            button = new TextButton(StringDB.getString("BTN_NAP"), style);
            button.setUserObject(DiplomaticAgreement.NAP);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.NAP);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                    .spaceRight((int) hpad);
            button = new TextButton(StringDB.getString("BTN_DEFENCE"), style);
            button.setUserObject(DiplomaticAgreement.DEFENCEPACT);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.DEFENCEPACT);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
            offerTypeButtons.row();
            button = new TextButton(StringDB.getString("BTN_PRESENT"), style);
            button.setUserObject(DiplomaticAgreement.PRESENT);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.PRESENT);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                    .spaceRight((int) hpad);
            button = new TextButton(StringDB.getString("BTN_REQUEST"), style);
            button.setUserObject(DiplomaticAgreement.REQUEST);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.REQUEST);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
            offerTypeButtons.row();
            button = new TextButton(StringDB.getString("BTN_WAR"), style);
            button.setUserObject(DiplomaticAgreement.WAR);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.WAR);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                    .spaceRight((int) hpad);
            button = new TextButton(StringDB.getString("BTN_WARPACT"), style);
            button.setUserObject(DiplomaticAgreement.WARPACT);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.WARPACT);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
        } else {
            button = new TextButton(StringDB.getString("BTN_MEMBERSHIP"), style);
            button.setUserObject(DiplomaticAgreement.MEMBERSHIP);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.MEMBERSHIP);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                    .spaceRight((int) hpad);
            button = new TextButton(StringDB.getString("BTN_WAR"), style);
            button.setUserObject(DiplomaticAgreement.WAR);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.WAR);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
            offerTypeButtons.row();
            button = new TextButton(StringDB.getString("BTN_PRESENT"), style);
            button.setUserObject(DiplomaticAgreement.PRESENT);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.PRESENT);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad)
                    .spaceRight((int) hpad);
            button = new TextButton(StringDB.getString("BTN_CORRUPTION"), style);
            button.setUserObject(DiplomaticAgreement.CORRUPTION);
            button.addListener(listener);
            disableIfNeeded(button, DiplomaticAgreement.CORRUPTION);
            offerTypeButtons.add(button).width((int) buttonWidth).height((int) buttonHeight).spaceBottom((int) vpad);
        }
        offerTypeButtons.setVisible(true);
    }

    private void drawOffer(Race race) {
        String sendStr = "";

        if (outGoingInfo.type != DiplomaticAgreement.NONE) {
            if (outGoingInfo.type != DiplomaticAgreement.WAR) {
                drawResourceInfo(race);
                TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
                float miscSelectHeight = GameConstants.hToRelative(25);
                if (outGoingInfo.type == DiplomaticAgreement.CORRUPTION) {
                    if (outGoingInfo.corruptedRace.isEmpty()) {
                        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                        if (race.getAgreement(playerRace.getRaceId()).getType() < DiplomaticAgreement.FRIENDSHIP
                                .getType()) {
                            for (int i = 0; i < majors.size; i++) {
                                String otherID = majors.getKeyAt(i);
                                if (!otherID.equals(playerRace.getRaceId())
                                        && race.getAgreement(otherID).getType() >= DiplomaticAgreement.ALLIANCE
                                                .getType()) {
                                    outGoingInfo.corruptedRace = otherID;
                                    break;
                                }
                            }
                        } else {
                            for (int i = 0; i < majors.size; i++) {
                                String otherID = majors.getKeyAt(i);
                                if (!otherID.equals(playerRace.getRaceId())
                                        && race.getAgreement(otherID).getType() >= DiplomaticAgreement.TRADE.getType()) {
                                    outGoingInfo.corruptedRace = otherID;
                                    break;
                                }
                            }
                        }
                    }
                    if (race.getAgreement(playerRace.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType()) {
                        Major corruptedMajor = Major.toMajor(manager.getRaceController().getRace(
                                outGoingInfo.corruptedRace));
                        if (corruptedMajor != null) {
                            miscSelector.clear();
                            miscSelector.add(StringDB.getString("ENEMY"), "mediumFont", normalColor).height(
                                    (int) miscSelectHeight);
                            miscSelector.row();
                            TextButton miscButton = new TextButton(corruptedMajor.getName(), smallButtonStyle);
                            miscButton.setUserObject(race);
                            miscButton.addListener(new ClickListener() {
                                @Override
                                public void clicked(InputEvent event, float x, float y) {
                                    if (showSendButton) {
                                        Race r = (Race) event.getListenerActor().getUserObject();
                                        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                                        if (playerRace.getAgreement(r.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP
                                                .getType()) {
                                            int i = majors.indexOfKey(outGoingInfo.corruptedRace);
                                            while (true) {
                                                i++;
                                                if (i == majors.size)
                                                    i = 0;
                                                String otherID = majors.getKeyAt(i);
                                                if (!otherID.equals(playerRace.getRaceId())
                                                        && r.getAgreement(otherID).getType() >= DiplomaticAgreement.TRADE
                                                                .getType()) {
                                                    outGoingInfo.corruptedRace = otherID;
                                                    show();
                                                    break;
                                                } else if (otherID.equals(outGoingInfo.corruptedRace))
                                                    break;
                                            }
                                        }
                                    }
                                }
                            });
                            miscSelector.add(miscButton).height((int) miscSelectHeight);
                            miscSelector.setVisible(true);
                        }
                    }
                } else if (race.isMajor() && outGoingInfo.type != DiplomaticAgreement.PRESENT
                        && outGoingInfo.type != DiplomaticAgreement.REQUEST
                        && outGoingInfo.type != DiplomaticAgreement.WARPACT) {
                    String text = "";
                    if (outGoingInfo.duration == 0)
                        text = StringDB.getString("UNLIMITED");
                    else
                        text = String.format("%d %s", outGoingInfo.duration, StringDB.getString("ROUNDS"));

                    miscSelector.clear();
                    miscSelector.add(StringDB.getString("CONTRACT_DURATION"), "mediumFont", normalColor).height(
                            (int) miscSelectHeight);
                    miscSelector.row();
                    TextButton miscButton = new TextButton(text, smallButtonStyle);
                    miscButton.addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (showSendButton) {
                                outGoingInfo.duration += 10;
                                if (outGoingInfo.duration > 100)
                                    outGoingInfo.duration = 0;
                                show();
                            }
                        }
                    });
                    miscSelector.add(miscButton).height((int) miscSelectHeight);
                    miscSelector.setVisible(true);
                } else if (outGoingInfo.type == DiplomaticAgreement.WARPACT) {
                    if (outGoingInfo.warpactEnemy.isEmpty()) {
                        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                        for (int i = 0; i < majors.size; i++) {
                            String otherID = majors.getKeyAt(i);
                            if (!otherID.equals(playerRace.getRaceId()) && !otherID.equals(race.getRaceId())
                                    && playerRace.getAgreement(race.getRaceId()) != DiplomaticAgreement.WAR
                                    && playerRace.isRaceContacted(otherID) && race.isRaceContacted(otherID)
                                    && race.getAgreement(otherID) != DiplomaticAgreement.WAR) {
                                outGoingInfo.warpactEnemy = otherID;
                                break;
                            }
                        }
                    }
                    if (!outGoingInfo.warpactEnemy.isEmpty()) {
                        Major warPactEnemy = Major.toMajor(manager.getRaceController().getRace(
                                outGoingInfo.warpactEnemy));
                        if (warPactEnemy != null) {
                            miscSelector.clear();
                            miscSelector.add(StringDB.getString("WARPACT_ENEMY"), "mediumFont", normalColor).height(
                                    (int) miscSelectHeight);
                            miscSelector.row();
                            TextButton miscButton = new TextButton(warPactEnemy.getName(), smallButtonStyle);
                            miscButton.setUserObject(race);
                            miscButton.addListener(new ClickListener() {
                                @Override
                                public void clicked(InputEvent event, float x, float y) {
                                    if (showSendButton) {
                                        Race r = (Race) event.getListenerActor().getUserObject();
                                        Array<String> enemies = new Array<String>();
                                        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                                        for (int i = 0; i < majors.size; i++) {
                                            String otherID = majors.getKeyAt(i);
                                            if (!otherID.equals(playerRace.getRaceId())
                                                    && !otherID.equals(r.getRaceId())
                                                    && playerRace.getAgreement(r.getRaceId()) != DiplomaticAgreement.WAR
                                                    && playerRace.isRaceContacted(otherID)
                                                    && r.isRaceContacted(otherID)
                                                    && r.getAgreement(otherID) != DiplomaticAgreement.WAR) {
                                                enemies.add(otherID);
                                            }
                                        }
                                        for (int i = 0; i < enemies.size; i++) {
                                            if (enemies.get(i).equals(outGoingInfo.warpactEnemy)) {
                                                if (i < enemies.size - 1)
                                                    outGoingInfo.warpactEnemy = enemies.get(i + 1);
                                                else
                                                    outGoingInfo.warpactEnemy = enemies.get(0);
                                                break;
                                            }
                                        }
                                        show();
                                    }
                                }
                            });
                            miscSelector.add(miscButton).height((int) miscSelectHeight);
                            miscSelector.setVisible(true);
                        }
                    }
                }
            }
        }

        if (!showSendButton) {
            sendStr = StringDB.getString("BTN_CANCEL");
        } else {
            if (outGoingInfo.type != DiplomaticAgreement.NONE && outGoingInfo.type != DiplomaticAgreement.PRESENT
                    && outGoingInfo.type != DiplomaticAgreement.CORRUPTION
                    && outGoingInfo.type != DiplomaticAgreement.REQUEST
                    && outGoingInfo.type != DiplomaticAgreement.WARPACT)
                sendStr = StringDB.getString("BTN_SEND");
            else if (outGoingInfo.type == DiplomaticAgreement.PRESENT
                    || outGoingInfo.type == DiplomaticAgreement.CORRUPTION
                    || outGoingInfo.type == DiplomaticAgreement.REQUEST) {
                if (outGoingInfo.credits > 0 || outGoingInfo.resources[ResourceTypes.TITAN.getType()] > 0
                        || outGoingInfo.resources[ResourceTypes.DEUTERIUM.getType()] > 0
                        || outGoingInfo.resources[ResourceTypes.DURANIUM.getType()] > 0
                        || outGoingInfo.resources[ResourceTypes.CRYSTAL.getType()] > 0
                        || outGoingInfo.resources[ResourceTypes.IRIDIUM.getType()] > 0
                        || outGoingInfo.resources[ResourceTypes.DERITIUM.getType()] > 0)
                    sendStr = StringDB.getString("BTN_SEND");
            } else if (outGoingInfo.type == DiplomaticAgreement.WARPACT)
                if (!outGoingInfo.warpactEnemy.isEmpty())
                    sendStr = StringDB.getString("BTN_SEND");
        }
        if (!sendStr.isEmpty()) {
            sendButton.setText(sendStr);
            sendButton.setVisible(true);
        } else
            sendButton.setVisible(false);

        GenDiploMessage.generateMajorOffer(manager, outGoingInfo);
        if (outGoingInfo.type != DiplomaticAgreement.NONE && sendButton.isVisible())
            ((DiplomacyScreen) manager.getScreen()).getBottomView().show(outGoingInfo.headLine, outGoingInfo.text);
        else
            ((DiplomacyScreen) manager.getScreen()).getBottomView().show();
    }

    private void drawResourceInfo(Race race) {
        if (outGoingInfo.type != DiplomaticAgreement.REQUEST && showSendButton
                && outGoingInfo.credits > playerRace.getEmpire().getCredits()) {
            outGoingInfo.credits = Math.max(0, playerRace.getEmpire().getCredits() / 250);
            outGoingInfo.credits *= 250;
        }

        creditsLabel.clear();
        String text = String.format("%s: %d %s", StringDB.getString("PAYMENT"), outGoingInfo.credits,
                StringDB.getString("CREDITS"));
        creditsLabel.add(text, "mediumFont", normalColor);
        creditsLabel.setVisible(true);

        TextButtonStyle minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        TextButtonStyle plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");
        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
        TextureRegion dr = new TextureRegion(texture);

        float bheight = creditsButton.getHeight();
        float pad = GameConstants.wToRelative(2);
        float bwidth = creditsButton.getWidth() / 20 - (int) pad;

        creditsButton.clear();
        TextButton minusButton = new TextButton("", minusStyle);
        minusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (showSendButton) {
                    outGoingInfo.credits -= 250;
                    outGoingInfo.credits = Math.max(0, outGoingInfo.credits);
                    show();
                }
            }
        });
        creditsButton.add(minusButton).width((int) (bwidth * 2)).height((int) (bheight));
        for (int t = 0; t < 20; t++) {
            Sprite sprite = new Sprite(dr);
            Color color;
            if (outGoingInfo.credits / 250 > t)
                color = new Color((200 - t * 10f) / 255.0f, (200) / 255.0f, 0, 1.0f);
            else
                color = new Color(100 / 255.0f, 100 / 255.0f, 100 / 255.0f, 100 / 255.0f);
            sprite.setColor(color);
            Drawable newDrawable = new SpriteDrawable(sprite);
            Image img = new Image(newDrawable);
            img.setUserObject(t);
            img.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (showSendButton) {
                        outGoingInfo.credits = ((Integer) (event.getListenerActor().getUserObject()) + 1) * 250;
                        show();
                    }
                }
            });
            creditsButton.add(img).width((int) bwidth).height((int) bheight).spaceLeft((int) pad);
        }
        TextButton plusButton = new TextButton("", plusStyle);
        plusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (showSendButton) {
                    outGoingInfo.credits += 250;
                    outGoingInfo.credits = Math.min(5000, outGoingInfo.credits);
                    show();
                }
            }
        });
        creditsButton.add(plusButton).width((int) (bwidth * 2)).height((int) (bheight)).spaceLeft((int) pad);
        creditsButton.setVisible(true);

        if (playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.TRADE.getType()
                || outGoingInfo.type == DiplomaticAgreement.REQUEST) {
            for (int i = ResourceTypes.TITAN.getType(); i < ResourceTypes.FOOD.getType(); i++)
                if (outGoingInfo.resources[i] > 0) {
                    whichResourceIsChosen = ResourceTypes.fromResourceTypes(i);
                    if (!outGoingInfo.coord.equals(new IntPoint())) {
                        resourceFromSystem = outGoingInfo.coord;
                    }
                    break;
                }
            ((TextButton) resourceSelect.findActor("RESOURCE_NAME")).setText(StringDB.getString(whichResourceIsChosen.getName()));
            ((TextButton) systemSelect.findActor("RESOURCE_FROM_SYSTEM")).setText(manager.getUniverseMap().getStarSystemAt(resourceFromSystem).getName());

            outGoingInfo.coord = resourceFromSystem;
            String res = StringDB.getString(whichResourceIsChosen.getName());
            int unit = whichResourceIsChosen == ResourceTypes.DERITIUM ? 5 : 1000;

            if (outGoingInfo.type != DiplomaticAgreement.REQUEST && showSendButton
                    && outGoingInfo.resources[whichResourceIsChosen.getType()] > 0
                    && !outGoingInfo.coord.equals(new IntPoint())) {
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(outGoingInfo.coord);
                if (outGoingInfo.resources[whichResourceIsChosen.getType()] > ss.getResourceStore(whichResourceIsChosen
                        .getType())) {
                    outGoingInfo.resources[whichResourceIsChosen.getType()] = ss.getResourceStore(whichResourceIsChosen
                            .getType()) / unit;
                    outGoingInfo.resources[whichResourceIsChosen.getType()] *= unit;
                }
            }

            resourceLabel.clear();
            text = String.format("%s: %d %s %s", StringDB.getString("TRANSFER"),
                    outGoingInfo.resources[whichResourceIsChosen.getType()], StringDB.getString("UNITS"), res);
            resourceLabel.add(text, "mediumFont", normalColor);
            resourceLabel.setVisible(true);

            resourceButton.clear();
            minusButton = new TextButton("", minusStyle);
            minusButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (showSendButton) {
                        int unit = whichResourceIsChosen == ResourceTypes.DERITIUM ? 5 : 1000;
                        outGoingInfo.resources[whichResourceIsChosen.getType()] -= unit;
                        outGoingInfo.resources[whichResourceIsChosen.getType()] = Math.max(0,
                                outGoingInfo.resources[whichResourceIsChosen.getType()]);
                        show();
                    }
                }
            });
            resourceButton.add(minusButton).width((int) (bwidth * 2)).height((int) (bheight));
            for (int t = 0; t < 20; t++) {
                Sprite sprite = new Sprite(dr);
                Color color;
                if (outGoingInfo.resources[whichResourceIsChosen.getType()] / unit > t)
                    color = new Color((200 - t * 10f) / 255.0f, (200) / 255.0f, 0, 1.0f);
                else
                    color = new Color(100 / 255.0f, 100 / 255.0f, 100 / 255.0f, 100 / 255.0f);
                sprite.setColor(color);
                Drawable newDrawable = new SpriteDrawable(sprite);
                Image img = new Image(newDrawable);
                img.setUserObject(t);
                img.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if (showSendButton) {
                            int unit = whichResourceIsChosen == ResourceTypes.DERITIUM ? 5 : 1000;
                            outGoingInfo.resources[whichResourceIsChosen.getType()] = ((Integer) (event
                                    .getListenerActor().getUserObject()) + 1) * unit;
                            show();
                        }
                    }
                });
                resourceButton.add(img).width((int) bwidth).height((int) bheight).spaceLeft((int) pad);
            }
            plusButton = new TextButton("", plusStyle);
            plusButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (showSendButton) {
                        int unit = whichResourceIsChosen == ResourceTypes.DERITIUM ? 5 : 1000;
                        outGoingInfo.resources[whichResourceIsChosen.getType()] += unit;
                        outGoingInfo.resources[whichResourceIsChosen.getType()] = Math.min(whichResourceIsChosen == ResourceTypes.DERITIUM ? 100 : 20000,
                                outGoingInfo.resources[whichResourceIsChosen.getType()]);
                        show();
                    }
                }
            });
            resourceButton.add(plusButton).width((int) (bwidth * 2)).height((int) (bheight)).spaceLeft((int) pad);
            resourceButton.setVisible(true);
            resourceSelect.setVisible(true);
            if (outGoingInfo.type != DiplomaticAgreement.REQUEST) {
                systemSelect.setVisible(true);
                resourceInfoTable.clear();
                int storage = manager.getUniverseMap().getStarSystemAt(outGoingInfo.coord)
                        .getResourceStore(whichResourceIsChosen.getType());
                if (storage <= unit)
                    text = StringDB.getString("SCARCELY_EXISTING");
                else if (storage <= 2 * unit)
                    text = StringDB.getString("VERY_LESS_EXISTING");
                else if (storage <= 4 * unit)
                    text = StringDB.getString("LESS_EXISTING");
                else if (storage <= 8 * unit)
                    text = StringDB.getString("MODERATE_EXISTING");
                else if (storage <= 16 * unit)
                    text = StringDB.getString("MUCH_EXISTING");
                else if (storage <= 32 * unit)
                    text = StringDB.getString("VERY_MUCH_EXISTING");
                else
                    text = StringDB.getString("ABOUNDING_EXISTING");

                resourceInfoTable.add(text, "mediumFont", normalColor);
                resourceInfoTable.setVisible(true);
            }
        }
    }

    private void disableIfNeeded(TextButton button, DiplomaticAgreement agreement) {
        if (agreement == outGoingInfo.type) {
            disableButton(button);
            return;
        }
        Race race = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
        if (!showSendButton)
            disableButton(button);

        boolean alienDiplomacy = false;
        if (race.hasSpecialAbility(Race.RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility())
                || playerRace.hasSpecialAbility(Race.RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility()))
            alienDiplomacy = true;

        if (race.isMajor()) {
            if (agreement == DiplomaticAgreement.TRADE)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.TRADE.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.FRIENDSHIP)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP
                                .getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.COOPERATION)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.COOPERATION
                                .getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.ALLIANCE)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.ALLIANCE
                                .getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.NAP)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP
                                .getType() || playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.NAP)
                    disableButton(button);
            if (agreement == DiplomaticAgreement.DEFENCEPACT)
                if (alienDiplomacy || playerRace.getDefencePact(race.getRaceId())
                        || playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.ALLIANCE
                        || playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.WAR)
                    disableButton(button);
            if (agreement == DiplomaticAgreement.REQUEST)
                if (alienDiplomacy)
                    disableButton(button);
            if (agreement == DiplomaticAgreement.WARPACT)
                if (alienDiplomacy || playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.WAR)
                    disableButton(button);
                else {
                    //we need to know at least 2 majors to form a warpact with one of them
                    boolean foundEnemy = false;
                    ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                    for (int i = 0; i < majors.size; i++) {
                        String otherID = majors.getKeyAt(i);
                        if (!otherID.equals(playerRace.getRaceId()) && !otherID.equals(race.getRaceId())
                                && race.getAgreement(otherID) != DiplomaticAgreement.WAR
                                && playerRace.isRaceContacted(otherID) && race.isRaceContacted(otherID)) {
                            foundEnemy = true;
                        }
                    }
                    if (!foundEnemy)
                        disableButton(button);
                }
            if (agreement == DiplomaticAgreement.WAR)
                if (playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.WAR)
                    disableButton(button);
            if (agreement == DiplomaticAgreement.PRESENT)
                if (alienDiplomacy)
                    disableButton(button);

        } else if (race.isMinor()) {
            //check if we can make an offer (for example if it's a member or subjugated..)
            DiplomaticAgreement status = DiplomaticAgreement.NONE;
            ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                String otherID = majors.getKeyAt(i);
                if (!otherID.equals(playerRace.getRaceId())) {
                    DiplomaticAgreement temp = race.getAgreement(otherID);
                    if (temp.getType() > status.getType())
                        status = temp;
                }
            }

            Minor minor = (Minor) race;
            if (minor.isSubjugated())
                disableButton(button);

            if (agreement == DiplomaticAgreement.TRADE)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.TRADE.getType()
                        || status.getType() > DiplomaticAgreement.ALLIANCE.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.FRIENDSHIP)
                if (playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType()
                        || status.getType() > DiplomaticAgreement.COOPERATION.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.COOPERATION)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.COOPERATION
                                .getType() || status.getType() > DiplomaticAgreement.FRIENDSHIP.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.ALLIANCE)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.ALLIANCE
                                .getType() || status.getType() > DiplomaticAgreement.FRIENDSHIP.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.MEMBERSHIP)
                if (alienDiplomacy
                        || playerRace.getAgreement(race.getRaceId()).getType() >= DiplomaticAgreement.MEMBERSHIP
                                .getType() || status.getType() > DiplomaticAgreement.FRIENDSHIP.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.WAR)
                if (playerRace.getAgreement(race.getRaceId()) == DiplomaticAgreement.WAR
                        || status.getType() > DiplomaticAgreement.ALLIANCE.getType())
                    disableButton(button);
            if (agreement == DiplomaticAgreement.CORRUPTION) {
                if (!alienDiplomacy
                        && (status.getType() >= DiplomaticAgreement.ALLIANCE.getType() || (playerRace.getAgreement(
                        race.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType() && status
                        .getType() >= DiplomaticAgreement.TRADE.getType()))) {
                } else
                    disableButton(button);
            }
            if (agreement == DiplomaticAgreement.PRESENT)
                if (alienDiplomacy)
                    disableButton(button);
        }
    }

    private void disableButton(TextButton button) {
        button.setDisabled(true);
        skin.setEnabled(button, false);
    }

    public void resetOutGoingInfo() {
        outGoingInfo = new DiplomacyInfo();
        resourceFromSystem = new IntPoint();
    }
}
