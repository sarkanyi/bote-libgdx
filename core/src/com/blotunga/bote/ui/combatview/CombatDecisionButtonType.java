/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.combatview;

import com.blotunga.bote.constants.CombatOrder;

public enum CombatDecisionButtonType {
    DETAILS_BUTTON("BTN_DETAILS", CombatOrder.USER),
    HAILING_BUTTON("BTN_HAILING", CombatOrder.HAILING),
    RETREAT_BUTTON("BTN_RETREAT", CombatOrder.RETREAT),
    AUTOCOMBAT_BUTTON("BTN_AUTOCOMBAT", CombatOrder.AUTOCOMBAT);

    private String label;
    private CombatOrder ord;

    CombatDecisionButtonType(String label, CombatOrder ord) {
        this.label = label;
        this.ord = ord;
    }

    public String getLabel() {
        return label;
    }

    public CombatOrder getCombatOrder() {
        return ord;
    }
}
