/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui;

import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.blotunga.bote.constants.ViewTypes;

public enum UIButtonType {
    OPTIONS("BTN_OPTIONS", new UIButtonListeners(ViewTypes.OPTIONS_VIEW)),
    GALAXY_MAP_BUTTON("BTN_GALAXY", new UIButtonListeners(ViewTypes.GALAXY_VIEW)),
    SYSTEM_VIEW_BUTTON("BTN_SYSTEM", new UIButtonListeners(ViewTypes.SYSTEM_VIEW)),
    RESEARCH_BUTTON("BTN_RESEARCH", new UIButtonListeners(ViewTypes.RESEARCH_VIEW)),
    INTELLIGENCE_BUTTON("BTN_SECURITY", new UIButtonListeners(ViewTypes.INTEL_VIEW)),
    DIPLOMACY_BUTTON("BTN_DIPLOMACY", new UIButtonListeners(ViewTypes.DIPLOMACY_VIEW)),
    TRADE_BUTTON("BTN_TRADE", new UIButtonListeners(ViewTypes.TRADE_VIEW)),
    EMPIRE_OVERVIEW_BUTTON("BTN_EMPIRE", new UIButtonListeners(ViewTypes.EMPIRE_VIEW)),
    END_TURN("BTN_ROUNDEND", new UIButtonListeners.EndTurnButtonListener());

    private String label;
    private EventListener listener;

    UIButtonType(String label, EventListener listener) {
        this.label = label;
        this.listener = listener;
    }

    public String getLabel() {
        return label;
    }

    public EventListener getListener() {
        return listener;
    }
}
