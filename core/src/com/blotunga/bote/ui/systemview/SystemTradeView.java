/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.starsystem.GlobalStorage;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.trade.TradeRoute;
import com.blotunga.bote.ui.screens.UniverseRenderer;
import com.blotunga.bote.utils.IntPoint;

public class SystemTradeView {
    private ScreenManager manager;
    private Skin skin;
    private StarSystem starSystem;
    private Major playerRace;
    private Color normalColor;
    private Color markTextColor;
    private Table nameTable;
    private Table tradeAndResourceHeader;
    private Table globalStorageHeader;
    private Table stellarStorageHeader;
    private Table systemStorageHeader;
    private Table numTotalRoutes;
    private Table numActiveRoutes;
    private Table routesHeader;
    private ScrollPane routesPane;
    private Table routesTable;
    private TextButton tradeRouteBtn;
    private TextButton resourceRouteBtn;
    private TextButton resourceTypeBtn;
    private ResourceTypes selectedResource;
    private Table takenFromStorageInfo;
    private Table storageTable;
    private TextButtonStyle plusStyle;
    private TextButtonStyle minusStyle;
    private Table lostInfoTable;
    private Table multiplierTable;
    private int globalStorageQuantity;

    public SystemTradeView(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markTextColor = playerRace.getRaceDesign().clrListMarkTextColor;
        float xOffset = position.getWidth();
        globalStorageQuantity = 1;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(285, 800, 602, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.setVisible(false);
        stage.addActor(nameTable);

        tradeAndResourceHeader = new Table();
        rect = GameConstants.coordsToRelative(65, 720, 470, 25);
        tradeAndResourceHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        tradeAndResourceHeader.align(Align.center);
        tradeAndResourceHeader.setSkin(skin);
        tradeAndResourceHeader.setVisible(false);
        tradeAndResourceHeader.add(StringDB.getString("TRADE_AND_RESOURCEROUTES"), "normalFont", markTextColor);
        stage.addActor(tradeAndResourceHeader);

        globalStorageHeader = new Table();
        rect = GameConstants.coordsToRelative(660, 720, 470, 25);
        globalStorageHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        globalStorageHeader.align(Align.center);
        globalStorageHeader.setSkin(skin);
        globalStorageHeader.setVisible(false);
        globalStorageHeader.add(StringDB.getString("STELLAR_STORAGE"), "normalFont", markTextColor);
        stage.addActor(globalStorageHeader);

        systemStorageHeader = new Table();
        rect = GameConstants.coordsToRelative(650, 650, 250, 25);
        systemStorageHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        systemStorageHeader.align(Align.center);
        systemStorageHeader.setSkin(skin);
        systemStorageHeader.setVisible(false);
        systemStorageHeader.add(StringDB.getString("SYSTEM_STORAGE"), "normalFont", markTextColor);
        stage.addActor(systemStorageHeader);

        stellarStorageHeader = new Table();
        rect = GameConstants.coordsToRelative(890, 650, 250, 25);
        stellarStorageHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stellarStorageHeader.align(Align.center);
        stellarStorageHeader.setSkin(skin);
        stellarStorageHeader.setVisible(false);
        stellarStorageHeader.add(StringDB.getString("STELLAR_STORAGE"), "normalFont", markTextColor);
        stage.addActor(stellarStorageHeader);

        numTotalRoutes = new Table();
        rect = GameConstants.coordsToRelative(75, 650, 250, 25);
        numTotalRoutes.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        numTotalRoutes.align(Align.left);
        numTotalRoutes.setSkin(skin);
        numTotalRoutes.setVisible(false);
        stage.addActor(numTotalRoutes);

        numActiveRoutes = new Table();
        rect = GameConstants.coordsToRelative(75, 620, 250, 25);
        numActiveRoutes.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        numActiveRoutes.align(Align.left);
        numActiveRoutes.setSkin(skin);
        numActiveRoutes.setVisible(false);
        stage.addActor(numActiveRoutes);

        routesHeader = new Table();
        rect = GameConstants.coordsToRelative(75, 570, 250, 25);
        routesHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        routesHeader.align(Align.left);
        routesHeader.setSkin(skin);
        routesHeader.setVisible(false);
        routesHeader.add(StringDB.getString("ROUTES_TO"), "normalFont", normalColor);
        stage.addActor(routesHeader);

        routesTable = new Table();
        rect = GameConstants.coordsToRelative(90, 520, 430, 340);
        routesTable.align(Align.topLeft);
        routesTable.setSkin(skin);
        routesPane = new ScrollPane(routesTable, skin);
        routesPane.setVariableSizeKnobs(false);
        routesPane.setFadeScrollBars(false);
        routesPane.setScrollbarsOnTop(true);
        routesPane.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        routesPane.setScrollingDisabled(true, false);
        routesPane.setVisible(false);
        stage.addActor(routesPane);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        tradeRouteBtn = new TextButton(StringDB.getString("BTN_TRADEROUTE"), smallButtonStyle);
        rect = GameConstants.coordsToRelative(70, 159, 140, 30);
        tradeRouteBtn.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        tradeRouteBtn.setVisible(false);
        tradeRouteBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                manager.getUniverseMap().setSelectedCoordValue(starSystem.getCoordinates());
                manager.setView(ViewTypes.GALAXY_VIEW);
                manager.getUniverseMap().getRenderer().setRouteType(UniverseRenderer.RouteType.TRADE);
            }
        });
        stage.addActor(tradeRouteBtn);

        resourceRouteBtn = new TextButton(StringDB.getString("BTN_RESOURCEROUTE"), smallButtonStyle);
        rect = GameConstants.coordsToRelative(395, 159, 140, 30);
        resourceRouteBtn.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        resourceRouteBtn.setVisible(false);
        resourceRouteBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                manager.getUniverseMap().setSelectedCoordValue(starSystem.getCoordinates());
                manager.setView(ViewTypes.GALAXY_VIEW);
                manager.getUniverseMap().getRenderer()
                        .setRouteType(UniverseRenderer.RouteType.RESOURCE, selectedResource);
            }
        });
        stage.addActor(resourceRouteBtn);

        selectedResource = ResourceTypes.TITAN;
        resourceTypeBtn = new TextButton(StringDB.getString(selectedResource.getName()), smallButtonStyle);
        rect = GameConstants.coordsToRelative(395, 119, 140, 30);
        resourceTypeBtn.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        resourceTypeBtn.setVisible(false);
        resourceTypeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (selectedResource.getType() < ResourceTypes.IRIDIUM.getType())
                    selectedResource = ResourceTypes.fromResourceTypes(selectedResource.getType() + 1);
                else
                    selectedResource = ResourceTypes.TITAN;
                button.setText(StringDB.getString(selectedResource.getName()));
            }
        });
        stage.addActor(resourceTypeBtn);

        takenFromStorageInfo = new Table();
        rect = GameConstants.coordsToRelative(650, 610, 500, 25);
        takenFromStorageInfo.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        takenFromStorageInfo.align(Align.center);
        takenFromStorageInfo.setSkin(skin);
        takenFromStorageInfo.setVisible(false);
        stage.addActor(takenFromStorageInfo);

        storageTable = new Table();
        rect = GameConstants.coordsToRelative(670, 560, 454, 314);
        storageTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        storageTable.align(Align.top);
        storageTable.setSkin(skin);
        storageTable.setVisible(false);
        stage.addActor(storageTable);

        plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");

        minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");

        lostInfoTable = new Table();
        rect = GameConstants.coordsToRelative(650, 215, 500, 25);
        lostInfoTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        lostInfoTable.align(Align.center);
        lostInfoTable.setSkin(skin);
        lostInfoTable.setVisible(false);
        stage.addActor(lostInfoTable);

        multiplierTable = new Table();
        rect = GameConstants.coordsToRelative(600, 165, 500, 30);
        multiplierTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        multiplierTable.align(Align.center);
        multiplierTable.setSkin(skin);
        multiplierTable.setVisible(false);
        stage.addActor(multiplierTable);
        float pad = GameConstants.wToRelative(50);
        multiplierTable.add(StringDB.getString("MULTIPLIER"), "normalFont", normalColor);
        TextButton multiButton = new TextButton("" + globalStorageQuantity, smallButtonStyle);
        multiButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton b = (TextButton) event.getListenerActor();
                globalStorageQuantity *= 10;
                if (globalStorageQuantity > 10000)
                    globalStorageQuantity = 1;
                b.setText("" + globalStorageQuantity);
            }
        });
        multiplierTable.add(multiButton).height(multiplierTable.getHeight())
                .width((int) (GameConstants.wToRelative(130))).spaceLeft(pad);
    }

    public void show() {
        nameTable.setVisible(true);
        nameTable.clear();
        nameTable.add(String.format("%s %s", StringDB.getString("TRADEOVERVIEW_IN"), starSystem.getName()), "hugeFont",
                normalColor);
        tradeAndResourceHeader.setVisible(true);
        globalStorageHeader.setVisible(true);
        systemStorageHeader.setVisible(true);
        stellarStorageHeader.setVisible(true);
        numTotalRoutes.clear();
        numTotalRoutes.setVisible(true);

        int maxTradeRoutes = (int) (starSystem.getInhabitants() / GameConstants.TRADEROUTEHAB)
                + starSystem.getProduction().getAddedTradeRoutes();
        int addResRoute = 1;
        ///SPECIAL RESEARCH///
        ResearchInfo ri = playerRace.getEmpire().getResearch().getResearchInfo();
        if (ri.getResearchComplex(ResearchComplexType.TRADE).getFieldStatus(3) == ResearchStatus.RESEARCHED)
            if (maxTradeRoutes == 0) {
                maxTradeRoutes += ri.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
                addResRoute += ri.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
            }
        addResRoute += ri.isResearchedThenGetBonus(ResearchComplexType.STORAGE_AND_TRANSPORT, 3);
        int maxResRoutes = (int) (starSystem.getInhabitants() / GameConstants.TRADEROUTEHAB)
                + starSystem.getProduction().getAddedTradeRoutes() + addResRoute;
        numTotalRoutes.add(StringDB.getString("SYSTEM_SUPPORTS_ROUTES", false, "" + maxTradeRoutes, "" + maxResRoutes),
                "normalFont", normalColor);

        numActiveRoutes.clear();
        numActiveRoutes.setVisible(true);
        String s = "" + starSystem.getTradeRoutes().size;
        String s2 = "" + starSystem.getResourceRoutes().size;
        numActiveRoutes.add(StringDB.getString("SYSTEM_HAS_ROUTES", false, s, s2), "normalFont", normalColor);

        routesHeader.setVisible(true);
        routesPane.setVisible(true);
        drawRoutes();

        tradeRouteBtn.setVisible(true);
        resourceRouteBtn.setVisible(true);
        resourceTypeBtn.setVisible(true);
        resourceTypeBtn.setText(StringDB.getString(selectedResource.getName()));

        takenFromStorageInfo.clear();
        takenFromStorageInfo.setVisible(true);
        s = "" + playerRace.getEmpire().getGlobalStorage().getTakenResources();
        s2 = "" + playerRace.getEmpire().getGlobalStorage().getMaxTakenResources();
        takenFromStorageInfo.add(StringDB.getString("TAKE_FROM_STORAGE", false, s, s2), "normalFont", normalColor);

        drawGlobalStorage();

        lostInfoTable.clear();
        lostInfoTable.setVisible(true);
        s = "" + playerRace.getEmpire().getGlobalStorage().getLosing();
        lostInfoTable.add(StringDB.getString("LOST_PER_ROUND", false, s), "normalFont", normalColor);

        multiplierTable.setVisible(true);
    }

    private void drawRoutes() {
        routesTable.clear();
        float columnWidth = routesPane.getWidth() / 3;
        int cnt = 0;
        for (int i = 0; i < starSystem.getTradeRoutes().size; i++, cnt++) {
            if (cnt != 0)
                routesTable.row();
            TradeRoute tr = starSystem.getTradeRoutes().get(i);
            IntPoint dest = tr.getDestCoord();
            StarSystem destSS = manager.getUniverseMap().getStarSystemAt(dest);
            String text = destSS.coordsName(destSS.getKnown(starSystem.getOwnerId()));
            routesTable.add(text, "normalFont", normalColor).width((int) columnWidth);
            int lat = tr.getCredits(starSystem.getProduction().getIncomeOnTradeRoutes());
            text = String.format("%s: %d %s", StringDB.getString("PROFIT"), lat, StringDB.getString("CREDITS"));
            routesTable.add(text, "normalFont", normalColor).width((int) columnWidth);
            int duration = tr.getDuration();
            if (duration < 0)
                duration = 6 - Math.abs(duration);
            if (duration > 1)
                text = String.format("%s %d %s", StringDB.getString("STILL"), duration, StringDB.getString("ROUNDS"));
            else
                text = String.format("%s %d %s", StringDB.getString("STILL"), duration, StringDB.getString("ROUND"));
            Label routesLabel = new Label(text, skin, "normalFont", normalColor);
            routesLabel.setAlignment(Align.center);
            routesTable.add(routesLabel).width((int) columnWidth);
        }

        for (int i = 0; i < starSystem.getResourceRoutes().size; i++, cnt++) {
            if (cnt != 0)
                routesTable.row();
            ResourceRoute rr = starSystem.getResourceRoutes().get(i);
            IntPoint dest = rr.getCoord();
            String text = manager.getUniverseMap().getStarSystemAt(dest).getName();
            routesTable.add(text, "normalFont", normalColor).width((int) columnWidth);
            text = StringDB.getString(rr.getResource().getName());
            routesTable.add(text, "normalFont", normalColor).width((int) columnWidth);
            TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
            TextButton cancelBtn = new TextButton(StringDB.getString("BTN_ANNUL"), smallButtonStyle);
            cancelBtn.setUserObject(i);
            cancelBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    int idx = (Integer) button.getUserObject();
                    starSystem.getResourceRoutes().removeIndex(idx);
                    show();
                }
            });
            routesTable.add(cancelBtn).width((int) columnWidth).height(GameConstants.hToRelative(30));
        }
    }

    private void drawGlobalStorage() {
        storageTable.clear();
        storageTable.setVisible(true);
        float buttonHeight = GameConstants.hToRelative(40);
        float wPadding = GameConstants.hToRelative(11);
        float hPadding = GameConstants.hToRelative(50);
        float textWidth = GameConstants.wToRelative(80);
        GlobalStorage gs = playerRace.getEmpire().getGlobalStorage();
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            if (i != 0)
                storageTable.row().spaceTop(wPadding);
            TextButton minusButton = new TextButton("", minusStyle);
            minusButton.setUserObject(rt);
            minusButton.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (starSystem.getBlockade() > 0)
                        return;
                    ResourceTypes rt = (ResourceTypes) event.getListenerActor().getUserObject();
                    int res = rt.getType();
                    GlobalStorage gs = playerRace.getEmpire().getGlobalStorage();
                    starSystem.addResourceStore(res,
                            gs.subResource(globalStorageQuantity, res, starSystem.getCoordinates()));
                    drawGlobalStorage();
                }
            });
            storageTable.add(minusButton).width(buttonHeight).height(buttonHeight).spaceRight(hPadding);
            Table storageInfo = new Table();
            storageInfo.setSkin(skin);
            String text;
            text = "" + starSystem.getResourceStore(i);
            Label l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            storageInfo.add(l).width(textWidth);
            text = StringDB.getString(rt.getName());
            l = new Label(text, skin, "normalFont", markTextColor);
            l.setAlignment(Align.center);
            storageInfo.add(l).width(storageTable.getWidth() - 2 * textWidth - 2 * buttonHeight - 2 * hPadding);
            text = "" + gs.getResourceStorage(i);
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.right);
            storageInfo.add(l).width(textWidth);
            storageInfo.row();
            text = String.format(
                    "(%d)",
                    gs.getSubResource(i, starSystem.getCoordinates())
                            - gs.getAddedResource(i, starSystem.getCoordinates()));
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            storageInfo.add(l).align(Align.left);
            storageInfo.add("", "normalFont", normalColor);
            text = String.format("(%d)", gs.getAllAddedResource(i) - gs.getAllSubResource(i));
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.right);
            storageInfo.add(l).align(Align.right);
            storageTable.add(storageInfo).height((storageTable.getHeight() - 4 * wPadding) / 5);
            TextButton plusButton = new TextButton("", plusStyle);
            plusButton.setUserObject(rt);
            plusButton.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (starSystem.getBlockade() > 0)
                        return;
                    ResourceTypes rt = (ResourceTypes) event.getListenerActor().getUserObject();
                    int res = rt.getType();
                    GlobalStorage gs = playerRace.getEmpire().getGlobalStorage();
                    if (starSystem.getResourceStore(res) > 0 || gs.getSubResource(res, starSystem.getCoordinates()) > 0) {
                        int tempQuantity = globalStorageQuantity;
                        if (starSystem.getResourceStore(res) < globalStorageQuantity
                                && gs.getSubResource(res, starSystem.getCoordinates()) == 0)
                            globalStorageQuantity = starSystem.getResourceStore(res);
                        int getBack = gs.addResource(globalStorageQuantity, res, starSystem.getCoordinates());
                        starSystem.addResourceStore(res, getBack - globalStorageQuantity);
                        globalStorageQuantity = tempQuantity;
                        drawGlobalStorage();
                    }
                }
            });
            storageTable.add(plusButton).width(buttonHeight).height(buttonHeight).spaceLeft(hPadding);
        }
    }

    public void hide() {
        nameTable.setVisible(false);
        tradeAndResourceHeader.setVisible(false);
        globalStorageHeader.setVisible(false);
        systemStorageHeader.setVisible(false);
        stellarStorageHeader.setVisible(false);
        numTotalRoutes.setVisible(false);
        numActiveRoutes.setVisible(false);
        routesHeader.setVisible(false);
        routesPane.setVisible(false);
        tradeRouteBtn.setVisible(false);
        resourceRouteBtn.setVisible(false);
        resourceTypeBtn.setVisible(false);
        takenFromStorageInfo.setVisible(false);
        storageTable.setVisible(false);
        lostInfoTable.setVisible(false);
        multiplierTable.setVisible(false);
    }

    public void setStarSystem(StarSystem ss) {
        starSystem = ss;
    }
}
