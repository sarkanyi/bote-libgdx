/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntSet;
import com.badlogic.gdx.utils.IntSet.IntSetIterator;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemManager;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class SystemManagerView implements ValueChangedEvent {
    private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private StarSystem starSystem;
    private TextureRegion lineDrawable;
    private TextureRegion selectTexture;
    private Table nameTable;
    private Table activateManagerTable;
    private Table bombardTable;
    private Table takeOnOfflineTable;
    private Table workersDistributionTable;
    private Table bottomSettingsTable;
    private Table energyBuildingsTable;
    private Table consumersHeader;
    private Table ignoredHeader;
    private ScrollPane energyBuildingsScroller;
    private Table energyBuildingsList;
    private IntSet availableEnergyBuildings;
    private Button availableEnergySelection = null;
    private int selectedEnergyBuilding;
    private Color energyBuildingsOldColor;
    private ScrollPane ignoredBuildingsScroller;
    private Table ignoredBuildingsList;
    private Button ignoredEnergySelection = null;
    private int selectedIgnoredBuilding;
    private Color ignoredBuildingsOldColor;
    private Table energyButtonsTable;
    private boolean visible;

    public SystemManagerView(ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.stage = stage;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        float xOffset = position.getWidth();
        lineDrawable = new TextureRegion(manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class));
        selectTexture = manager.getUiTexture("listselect");

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(285, 800, 602, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.setVisible(false);
        stage.addActor(nameTable);

        activateManagerTable = new Table();
        rect = GameConstants.coordsToRelative(60, 690, 250, 30);
        activateManagerTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        activateManagerTable.setVisible(false);
        stage.addActor(activateManagerTable);

        bombardTable = new Table();
        rect = GameConstants.coordsToRelative(320, 690, 280, 30);
        bombardTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        bombardTable.setVisible(false);
        stage.addActor(bombardTable);

        takeOnOfflineTable = new Table();
        rect = GameConstants.coordsToRelative(610, 690, 250, 30);
        takeOnOfflineTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        takeOnOfflineTable.setVisible(false);
        stage.addActor(takeOnOfflineTable);

        workersDistributionTable = new Table();
        workersDistributionTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(60, 655, 540, 490);
        workersDistributionTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        workersDistributionTable.setVisible(false);
        stage.addActor(workersDistributionTable);

        bottomSettingsTable = new Table();
        bottomSettingsTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(60, 200, 540, 65);
        bottomSettingsTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        bottomSettingsTable.setVisible(false);
        stage.addActor(bottomSettingsTable);

        energyBuildingsTable = new Table();
        energyBuildingsTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(610, 655, 540, 130);
        energyBuildingsTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        energyBuildingsTable.setVisible(false);
        stage.addActor(energyBuildingsTable);

        consumersHeader = new Table();
        consumersHeader.align(Align.center);
        rect = GameConstants.coordsToRelative(610, 515, 260, 30);
        consumersHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        consumersHeader.setSkin(skin);
        consumersHeader.add(StringDB.getString("ENERGY_CONSUMERS"), "normalFont", markColor);
        consumersHeader.setVisible(false);
        stage.addActor(consumersHeader);

        ignoredHeader = new Table();
        ignoredHeader.align(Align.center);
        rect = GameConstants.coordsToRelative(870, 515, 260, 30);
        ignoredHeader.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        ignoredHeader.setSkin(skin);
        ignoredHeader.add(StringDB.getString("EXCEPTIONS"), "normalFont", markColor);
        ignoredHeader.setVisible(false);
        stage.addActor(ignoredHeader);

        energyBuildingsList = new Table();
        energyBuildingsList.align(Align.top);
        energyBuildingsScroller = new ScrollPane(energyBuildingsList, skin);
        energyBuildingsScroller.setVariableSizeKnobs(false);
        energyBuildingsScroller.setFadeScrollBars(false);
        energyBuildingsScroller.setScrollbarsOnTop(true);
        energyBuildingsScroller.setScrollingDisabled(true, false);
        stage.addActor(energyBuildingsScroller);
        rect = GameConstants.coordsToRelative(610, 475, 260, 300);
        energyBuildingsScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        energyBuildingsList.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        energyBuildingsScroller.setVisible(false);

        availableEnergyBuildings = new IntSet();
        selectedEnergyBuilding = -1;

        ignoredBuildingsList = new Table();
        ignoredBuildingsList.align(Align.top);
        ignoredBuildingsScroller = new ScrollPane(ignoredBuildingsList, skin);
        ignoredBuildingsScroller.setVariableSizeKnobs(false);
        ignoredBuildingsScroller.setFadeScrollBars(false);
        ignoredBuildingsScroller.setScrollbarsOnTop(true);
        ignoredBuildingsScroller.setScrollingDisabled(true, false);
        stage.addActor(ignoredBuildingsScroller);
        rect = GameConstants.coordsToRelative(870, 475, 260, 300);
        ignoredBuildingsScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        ignoredBuildingsList.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        ignoredBuildingsScroller.setVisible(false);

        selectedIgnoredBuilding = -1;

        energyButtonsTable = new Table();
        energyButtonsTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(610, 165, 540, 30);
        energyButtonsTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        energyButtonsTable.setVisible(false);
        stage.addActor(energyButtonsTable);
        visible = false;
    }

    public void show() {
        hide();
        float buttonHeight = GameConstants.hToRelative(30);
        SystemManager sysMngr = starSystem.getManager();
        nameTable.clear();
        nameTable.add(String.format("%s %s", StringDB.getString("SYSTEM_MANAGER_MENUE"), starSystem.getName()),
                "hugeFont", normalColor);
        nameTable.setVisible(true);

        activateManagerTable.clear();
        String text = StringDB.getString("SYSTEM_MANAGER");
        Label label = new Label(text, skin, "normalFont", normalColor);
        activateManagerTable.add(label).align(Align.left).width(GameConstants.wToRelative(120));

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        TextButton button = new TextButton("", smallButtonStyle);
        String onOff = StringDB.getString(!sysMngr.isActive() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                starSystem.getManager().setActive(!starSystem.getManager().isActive());
                show();
            }
        });
        activateManagerTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight);
        activateManagerTable.setVisible(true);

        bombardTable.clear();
        text = StringDB.getString("BOMBARD_WARNING_CHECK");
        label = new Label(text, skin, "normalFont", normalColor);
        bombardTable.add(label).align(Align.left).width(GameConstants.wToRelative(150));

        button = new TextButton("", smallButtonStyle);
        onOff = StringDB.getString(!sysMngr.isBombWarning() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.setDisabled(!sysMngr.isActive());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    starSystem.getManager().setBombWarning(!starSystem.getManager().isBombWarning());
                    show();
                }
            }
        });
        bombardTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight);
        bombardTable.setVisible(true);

        takeOnOfflineTable.clear();
        text = StringDB.getString("TAKE_ON_OFF");
        label = new Label(text, skin, "normalFont", normalColor);
        takeOnOfflineTable.add(label).align(Align.left).width(GameConstants.wToRelative(120));

        button = new TextButton("", smallButtonStyle);
        onOff = StringDB.getString(!sysMngr.isOnOffline() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.setDisabled(!sysMngr.isActive());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    starSystem.getManager().setOnOffline(!starSystem.getManager().isOnOffline());
                    show();
                }
            }
        });
        takeOnOfflineTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight);
        takeOnOfflineTable.setVisible(true);

        workersDistributionTable.clear();
        text = StringDB.getString("WORKER_DISTRIBUTION");
        label = new Label(text, skin, "normalFont", markColor);
        float vPad = GameConstants.hToRelative(5);
        float widgetWidth = GameConstants.wToRelative(12);
        float widgetHeight = GameConstants.hToRelative(30);
        float pad = GameConstants.wToRelative(4);
        workersDistributionTable.add(label).align(Align.left).height(GameConstants.hToRelative(30)).spaceBottom(vPad);
        workersDistributionTable.row();
        ButtonStyle bs = new ButtonStyle();
        for (int i = WorkerType.INDUSTRY_WORKER.getType(); i < WorkerType.ALL_WORKER.getType(); i++) {
            WorkerType wt = WorkerType.fromWorkerType(i);
            if (wt == WorkerType.ENERGY_WORKER)
                continue;
            Button btn = new Button(bs);
            text = StringDB.getString(wt.getDBString());
            label = new Label(text, skin, "normalFont", normalColor);
            btn.add(label).width(GameConstants.wToRelative(70)).align(Align.left);
            int prio = sysMngr.getPriority(wt);
            text = "" + prio;
            if (prio == SystemManager.min_priority)
                text = StringDB.getString("NONE");
            else if (prio == SystemManager.max_priority)
                text = StringDB.getString("FILL_ALL");
            label = new Label(text, skin, "normalFont", normalColor);
            btn.add(label).width(GameConstants.wToRelative(70)).align(Align.left);
            boolean enabled = !(sysMngr.isActive() && (starSystem.getNumberOfWorkBuildings(wt, 0) != 0));
            PercentageWidget widget = new PercentageWidget(i, skin, widgetWidth, widgetHeight, pad,
                    SystemManager.max_priority / 2, SystemManager.min_priority, SystemManager.max_priority, prio,
                    lineDrawable, this, enabled);
            btn.add(widget.getWidget());
            workersDistributionTable.add(btn).spaceBottom(vPad);
            workersDistributionTable.row();
        }
        workersDistributionTable.setVisible(true);

        bottomSettingsTable.clear();
        text = StringDB.getString("SAFE_MORALE_LOSS");
        label = new Label(text, skin, "normalFont", normalColor);
        bottomSettingsTable.add(label).align(Align.left).width(GameConstants.wToRelative(120))
                .height(GameConstants.hToRelative(30)).spaceBottom(GameConstants.hToRelative(5));

        button = new TextButton("", smallButtonStyle);
        onOff = StringDB.getString(!sysMngr.isSafeMorale() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.setDisabled(!sysMngr.isActive());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    starSystem.getManager().setSafeMorale(!starSystem.getManager().isSafeMorale());
                    show();
                }
            }
        });
        bottomSettingsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight)
                .spaceBottom(GameConstants.hToRelative(5));

        text = StringDB.getString("USE_STORED_FOOD");
        label = new Label(text, skin, "normalFont", normalColor);
        bottomSettingsTable.add(label).align(Align.left).width(GameConstants.wToRelative(120))
                .spaceLeft(GameConstants.wToRelative(40)).spaceBottom(GameConstants.hToRelative(5));

        button = new TextButton("", smallButtonStyle);
        onOff = StringDB.getString(!sysMngr.isNeglectFood() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.setDisabled(!sysMngr.isActive());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    starSystem.getManager().setNeglectFood(!starSystem.getManager().isNeglectFood());
                    show();
                }
            }
        });
        bottomSettingsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight)
                .spaceBottom(GameConstants.hToRelative(5));

        bottomSettingsTable.row();
        text = StringDB.getString("MAX_INDUSTRY");
        label = new Label(text, skin, "normalFont", normalColor);
        bottomSettingsTable.add(label).align(Align.left).width(GameConstants.wToRelative(120))
                .spaceLeft(GameConstants.wToRelative(40));

        button = new TextButton("", smallButtonStyle);
        onOff = StringDB.getString(!sysMngr.isMaxIndustry() ? "ENABLE" : "DISABLE");
        button.setText(onOff);
        button.setDisabled(!sysMngr.isActive());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    starSystem.getManager().setMaxIndustry(!starSystem.getManager().isMaxIndustry());
                    show();
                }
            }
        });
        bottomSettingsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight);

        bottomSettingsTable.setVisible(true);

        energyBuildingsTable.clear();
        text = StringDB.getString("ENERGY_BUILDINGS");
        label = new Label(text, skin, "normalFont", markColor);
        energyBuildingsTable.add(label).align(Align.left).height(GameConstants.hToRelative(30)).spaceBottom(vPad);
        energyBuildingsTable.row();
        Button btn = new Button(bs);
        text = StringDB.getString("MIN_MORALE");
        label = new Label(text, skin, "normalFont", normalColor);
        btn.add(label).width(GameConstants.wToRelative(70)).align(Align.left);
        int val = sysMngr.getMinMorale();
        text = "" + val;
        label = new Label(text, skin, "normalFont", normalColor);
        btn.add(label).width(GameConstants.wToRelative(70)).align(Align.left);
        PercentageWidget widget = new PercentageWidget(20, skin, widgetWidth, widgetHeight, pad, 20,
                SystemManager.min_min_morale, SystemManager.max_min_morale, val, lineDrawable, this,
                !sysMngr.isActive() || !sysMngr.isOnOffline());
        btn.add(widget.getWidget());
        energyBuildingsTable.add(btn).spaceBottom(vPad);

        energyBuildingsTable.row();
        btn = new Button(bs);
        text = StringDB.getString("MIN_MORALE_PROD");
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.left);
        btn.add(label).width(GameConstants.wToRelative(110)).align(Align.left);
        val = sysMngr.getMinMoraleProd();
        text = "" + val;
        label = new Label(text, skin, "normalFont", normalColor);
        btn.add(label).width(GameConstants.wToRelative(70)).align(Align.left);
        widget = new PercentageWidget(21, skin, widgetWidth, widgetHeight, pad, 15, SystemManager.min_min_morale_prod,
                SystemManager.max_min_morale_prod, val, lineDrawable, this, !sysMngr.isActive()
                        || !sysMngr.isOnOffline());
        btn.add(widget.getWidget());
        energyBuildingsTable.add(btn).spaceBottom(vPad).align(Align.left);
        energyBuildingsTable.row();
        energyBuildingsTable.setVisible(true);

        if (sysMngr.isActive() && sysMngr.isOnOffline()) {
            consumersHeader.setVisible(true);
            ignoredHeader.setVisible(true);

            populateAvailableEnergyBuildings();
            populateIngoredEnergyBuildings();

            energyButtonsTable.clear();
            energyButtonsTable.add().width(GameConstants.wToRelative(65));
            button = new TextButton(StringDB.getString("ADD"), smallButtonStyle);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (selectedEnergyBuilding != -1 && availableEnergySelection != null)
                        starSystem.getManager().getIgnoredBuildings().add(selectedEnergyBuilding);
                    show();
                }
            });
            energyButtonsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight);
            button = new TextButton(StringDB.getString("REMOVE"), smallButtonStyle);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (selectedIgnoredBuilding != -1 && ignoredEnergySelection != null)
                        starSystem.getManager().getIgnoredBuildings().remove(selectedIgnoredBuilding);
                    show();
                }
            });
            energyButtonsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight).spaceLeft(65);
            button = new TextButton(StringDB.getString("REMOVE_ALL"), smallButtonStyle);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    starSystem.getManager().getIgnoredBuildings().clear();
                    show();
                }
            });
            energyButtonsTable.add(button).align(Align.right).width(GameConstants.wToRelative(130)).height(buttonHeight).spaceLeft(10);
            energyButtonsTable.setVisible(true);

            if (!availableEnergyBuildings.contains(selectedEnergyBuilding))
                selectedEnergyBuilding = -1;
            if (!sysMngr.getIgnoredBuildings().contains(selectedIgnoredBuilding))
                selectedIgnoredBuilding = -1;

            Button b = null;
            stage.draw();
            if (selectedEnergyBuilding != -1) {
                for (int i = 0; i < energyBuildingsList.getCells().size; i++) {
                    Button temp = (Button) (energyBuildingsList.getCells().get(i).getActor());
                    if (selectedEnergyBuilding == getEnergyBuilding(temp)) {
                        b = temp;
                        break;
                    }
                }

                if (b != null)
                    markAvailableEnergyListSelected(b);
            }
            if (selectedIgnoredBuilding != -1) {
                for (int i = 0; i < ignoredBuildingsList.getCells().size; i++) {
                    Button temp = (Button) (ignoredBuildingsList.getCells().get(i).getActor());
                    if (selectedIgnoredBuilding == getEnergyBuilding(temp)) {
                        b = temp;
                        break;
                    }
                }

                if (b != null)
                    markIgnoredEnergyListSelected(b);
            }
        }
        visible = true;
    }

    private void populateAvailableEnergyBuildings() {
        SystemManager sysMngr = starSystem.getManager();
        energyBuildingsList.clear();
        availableEnergyBuildings.clear();
        Array<Building> buildings = starSystem.getAllBuildings();
        for (Building building : buildings) {
            int rn = building.getRunningNumber();
            BuildingInfo bi = manager.getBuildingInfo(rn);
            if (bi.getNeededEnergy() > 0 && !sysMngr.getIgnoredBuildings().contains(rn))
                availableEnergyBuildings.add(rn);
        }
        for (IntSetIterator iter = availableEnergyBuildings.iterator(); iter.hasNext;) {
            int rn = iter.next();
            BuildingInfo bi = manager.getBuildingInfo(rn);
            ButtonStyle bs = new ButtonStyle();
            Button btn = new Button(bs);
            String text = bi.getBuildingName();
            Label label = new Label(text, skin, "normalFont", Color.WHITE);
            label.setUserObject(bi);
            label.setColor(normalColor);
            label.setAlignment(Align.center);
            btn.add(label).width(energyBuildingsScroller.getWidth());
            btn.setUserObject(label);
            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markAvailableEnergyListSelected(b);
                    if (count > 1) {
                        starSystem.getManager().getIgnoredBuildings().add(getEnergyBuilding(b));
                        show();
                    }
                }
            };
            btn.addListener(gestureListener);
            energyBuildingsList.add(btn);
            energyBuildingsList.row();
        }
        energyBuildingsScroller.setVisible(true);
    }

    private void populateIngoredEnergyBuildings() {
        SystemManager sysMngr = starSystem.getManager();
        ignoredBuildingsList.clear();
        for (IntSetIterator iter = sysMngr.getIgnoredBuildings().iterator(); iter.hasNext;) {
            int rn = iter.next();
            BuildingInfo bi = manager.getBuildingInfo(rn);
            ButtonStyle bs = new ButtonStyle();
            Button btn = new Button(bs);
            String text = bi.getBuildingName();
            Label label = new Label(text, skin, "normalFont", Color.WHITE);
            label.setUserObject(bi);
            label.setColor(normalColor);
            label.setAlignment(Align.center);
            btn.add(label).width(ignoredBuildingsScroller.getWidth());
            btn.setUserObject(label);
            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markIgnoredEnergyListSelected(b);
                    if (count > 1) {
                        starSystem.getManager().getIgnoredBuildings().remove(getEnergyBuilding(b));
                        show();
                    }
                }
            };
            btn.addListener(gestureListener);
            ignoredBuildingsList.add(btn);
            ignoredBuildingsList.row();
        }
        ignoredBuildingsScroller.setVisible(true);
    }

    public void hide() {
        if (visible)
            playerRace.reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, true);
        nameTable.setVisible(false);
        activateManagerTable.setVisible(false);
        bombardTable.setVisible(false);
        takeOnOfflineTable.setVisible(false);
        workersDistributionTable.setVisible(false);
        bottomSettingsTable.setVisible(false);
        energyBuildingsTable.setVisible(false);
        consumersHeader.setVisible(false);
        ignoredHeader.setVisible(false);
        energyBuildingsScroller.setVisible(false);
        ignoredBuildingsScroller.setVisible(false);
        energyButtonsTable.setVisible(false);
        visible = false;
    }

    public void setStarSystem(StarSystem ss) {
        starSystem = ss;
    }

    @Override
    public void valueChanged(int typeID, int value) {
        if (starSystem.getManager().isActive()) {
            if (typeID < WorkerType.ALL_WORKER.getType()) {
                WorkerType wt = WorkerType.fromWorkerType(typeID);
                starSystem.getManager().setPriority(wt, value);
            } else if (typeID == 20) {//Morale
                starSystem.getManager().setMinMorale(value);
            } else if (typeID == 21) {//Morale Prod
                starSystem.getManager().setMinMoraleProd(value);
            }
            show();
        }
    }

    private void markAvailableEnergyListSelected(Button b) {
        if (availableEnergySelection != null) {
            availableEnergySelection.getStyle().up = null;
            availableEnergySelection.getStyle().down = null;
            ((Label) availableEnergySelection.getUserObject()).setColor(energyBuildingsOldColor);
        }
        if (b == null)
            b = availableEnergySelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        energyBuildingsOldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        availableEnergySelection = b;
        selectedEnergyBuilding = getEnergyBuilding(b);
    }

    private int getEnergyBuilding(Button b) {
        return ((BuildingInfo) ((Label) b.getUserObject()).getUserObject()).getRunningNumber();
    }

    private void markIgnoredEnergyListSelected(Button b) {
        if (ignoredEnergySelection != null) {
            ignoredEnergySelection.getStyle().up = null;
            ignoredEnergySelection.getStyle().down = null;
            ((Label) ignoredEnergySelection.getUserObject()).setColor(ignoredBuildingsOldColor);
        }
        if (b == null)
            b = ignoredEnergySelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        ignoredBuildingsOldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        ignoredEnergySelection = b;
        selectedIgnoredBuilding = getEnergyBuilding(b);
    }
}
