/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.OverlayBanner;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class SystemBuildingsView {
    private Skin skin;
    final private ScreenManager manager;
    private Stage stage;
    private StarSystem starSystem;
    private Table nameTable;
    private Major race;
    private float xOffset;
    private int pageNr;
    private Array<Button> buildingsTable;
    private ObjectSet<String> loadedTextures;
    private Color normalColor;
    private Color markTextColor;
    private TextButton plusButton;
    private TextButton minusButton;
    private IntArray buildingOverview;
    private Array<OverlayBanner> banners;
    //tooltip
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture tooltipTexture;

    public SystemBuildingsView(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;

        race = manager.getRaceController().getPlayerRace();
        normalColor = race.getRaceDesign().clrNormalText;
        markTextColor = race.getRaceDesign().clrListMarkTextColor;
        tooltipHeaderColor = race.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = race.getRaceDesign().clrNormalText;
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        pageNr = 0;

        nameTable = new Table();
        xOffset = position.getWidth();
        Rectangle rect = GameConstants.coordsToRelative(285, 800, 602, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);

        buildingsTable = new Array<Button>();
        buildingOverview = new IntArray();
        banners = new Array<OverlayBanner>();

        rect = GameConstants.coordsToRelative(1130, 300, 60, 60);
        TextButtonStyle minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        minusButton = new TextButton("", minusStyle);
        minusButton.setVisible(false);
        minusButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        minusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pageNr--;
                show();
            }
        });

        rect = GameConstants.coordsToRelative(1130, 600, 60, 60);
        TextButtonStyle plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");
        plusButton = new TextButton("", plusStyle);
        plusButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        plusButton.setVisible(false);
        plusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pageNr++;
                show();
            }
        });

        stage.addActor(nameTable);
        stage.addActor(plusButton);
        stage.addActor(minusButton);

        loadedTextures = new ObjectSet<String>();
        hide();
    }

    public void show() {
        nameTable.clear();
        nameTable.add(StringDB.getString("BUILDING_OVERVIEW_MENUE") + " " + starSystem.getName(), "hugeFont",
                race.getRaceDesign().clrNormalText);
        nameTable.setVisible(true);

        drawSystemBuildings();
    }

    public void hide() {
        plusButton.setVisible(false);
        minusButton.setVisible(false);

        nameTable.setVisible(false);
        for (Button building : buildingsTable)
            building.setVisible(false);
        for (OverlayBanner banner : banners)
            banner.remove();
        banners.clear();
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    private void drawSystemBuildings() {
        for (int i = 0; i < buildingsTable.size; i++) {
            buildingsTable.get(i).remove();
        }
        buildingsTable.clear();
        buildingOverview.clear();
        for (OverlayBanner banner : banners)
            banner.remove();
        banners.clear();

        int numberOfBuildings = starSystem.getAllBuildings().size;
        int minRunningNumber = 0;
        int k = 0;
        while (k < numberOfBuildings) {
            int currentRunningNumber = starSystem.getAllBuildings().get(k).getRunningNumber();
            if (currentRunningNumber > minRunningNumber) {
                buildingOverview.add(currentRunningNumber);
                minRunningNumber = currentRunningNumber;
            }
            k++;
        }

        plusButton.setVisible(false);
        minusButton.setVisible(false);
        if (pageNr * GameConstants.NOBIOL > buildingOverview.size)
            pageNr = 0;
        if (buildingOverview.size > pageNr * GameConstants.NOBIOL + GameConstants.NOBIOL) {
            plusButton.setVisible(true);
        }
        if (pageNr > 0) {
            minusButton.setVisible(true);
        }

        int spaceX = 0;
        int spaceY = 0;
        for (int i = pageNr * GameConstants.NOBIOL; i < buildingOverview.size; i++) {
            //check that we are on the correct page
            if (i < pageNr * GameConstants.NOBIOL + GameConstants.NOBIOL) {
                if (i % 4 == 0 && i != pageNr * GameConstants.NOBIOL) {
                    spaceX++;
                    spaceY = 0;
                }
                Rectangle rect = GameConstants.coordsToRelative(55 + (spaceX) * 355, 730 - (spaceY) * 163, 345, 140);
                drawBuilding(rect, i);
                spaceY++;
            }
        }

        Rectangle rect = GameConstants.coordsToRelative(300, 550, 570, 170);
        rect.x += xOffset;
        if (starSystem.getBlockade() > 0) {
            OverlayBanner blockadeBanner = new OverlayBanner(rect, StringDB.getString("ONLY_PARTIAL_BUILDINGSCRAP"),
                    new Color(200 / 255.0f, 0, 0, 1.0f), (Texture) manager.getAssetManager()
                            .get(GameConstants.UI_BG_SIMPLE), skin, "normalFont");
            stage.addActor(blockadeBanner);
            banners.add(blockadeBanner);
        }
    }

    private void drawBuilding(Rectangle rect, int index) {
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        TextureRegion dr = new TextureRegion(texture);
        Sprite sprite = new Sprite(dr);
        sprite.setColor(Color.BLACK);
        Drawable tr = new SpriteDrawable(sprite);
        bs.up = tr;
        bs.down = tr;
        bs.over = tr;

        Button button = new Button(bs);
        button.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        int id = buildingOverview.get(index);
        BuildingInfo bi = manager.getBuildingInfos().get(id - 1);
        String path = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
        Texture tex = manager.loadTextureImmediate(path);
        loadedTextures.add(path);

        int count = starSystem.getNumberofBuilding(id);
        String s = count + " x " + bi.getBuildingName();
        Label buildingName = new Label(s, skin, "normalFont", normalColor);
        button.add(buildingName).align(Align.left);
        button.row();
        Table imageAndText = new Table();
        Image buildingImage = new Image(tex);
        float width = (int) (GameConstants.wToRelative(120));
        float pad = (int) (GameConstants.wToRelative(5));
        imageAndText.add(buildingImage).width(width).space(pad);

        width = (int) (GameConstants.wToRelative(145));
        Label buildingProduction = new Label(bi.getProductionAsString(count), skin, "mediumFont", markTextColor);
        imageAndText.add(buildingProduction).width(width);
        button.add(imageAndText);
        button.setUserObject(index);
        for (Actor a : button.getChildren())
            a.setTouchable(Touchable.disabled);

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int index = (Integer) event.getListenerActor().getUserObject();
                int id = buildingOverview.get(index);

                if ( (count >= 2 && Gdx.app.getType() == ApplicationType.Android)
                        || (button == 0 && Gdx.app.getType() == ApplicationType.Desktop)) {
                    starSystem.setBuildingDestroy(id, true);
                    show();
                }
                if (button == 1) {
                    starSystem.setBuildingDestroy(id, false);
                    show();
                }
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                int index = (Integer) actor.getUserObject();
                int id = buildingOverview.get(index);
                if(starSystem.getBuildingDestroy(id) > 0) {
                    starSystem.setBuildingDestroy(id, false);
                    show();
                }
                return false;
            }
        };
        gestureListener.getGestureDetector().setLongPressSeconds(0.8f);
        gestureListener.getGestureDetector().setTapCountInterval(0.6f);
        gestureListener.getGestureDetector().setTapSquareSize(30f);
        button.addListener(gestureListener);
        bi.getTooltip(BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);

        stage.addActor(button);
        buildingsTable.add(button);

        int dn = starSystem.getBuildingDestroy(id);
        if (dn > 0) {
            Rectangle newRect = new Rectangle(rect.x + xOffset, rect.y, rect.width, rect.height);
            String text = StringDB.getString("TALON") + ": " + dn;
            OverlayBanner banner = new OverlayBanner(newRect, text, new Color(1.0f, 0, 0, 1.0f), (Texture) manager
                    .getAssetManager().get(GameConstants.UI_BG_SIMPLE), skin, "normalFont");
            stage.addActor(banner);
            banners.add(banner);
        }
    }

    public void setStarSystem(StarSystem system) {
        this.starSystem = system;
    }
}
