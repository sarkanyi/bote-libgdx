/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;

public class SystemProduction {
    private StarSystem starSystem;
    private Skin skin;
    private float xOffset;
    private Table nameTable;
    private WorkerDisplay[] workersDisplay;
    private boolean showResources;
    private SystemBuildList buildList;
    private ScrollPane workerScroller; //for displaying the free workers
    private Table workerTable;
    private Label countLabel;
    private TextButton selectButton;

    private Major race;

    public SystemProduction(final ScreenManager manager, Stage stage, Skin skin, Rectangle position,
            SystemBuildList buildList) {
        this.skin = skin;
        this.buildList = buildList;
        race = manager.getRaceController().getPlayerRace();
        xOffset = position.getWidth();

        nameTable = new Table();
        xOffset = position.getWidth();
        Rectangle rect = GameConstants.coordsToRelative(150, 805, 510, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);

        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        TextureRegion dr = new TextureRegion(texture);
        skin.add("workerdrawable", dr);

        workersDisplay = new WorkerDisplay[WorkerType.RESEARCH_WORKER.getType() + 1];
        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.RESEARCH_WORKER.getType(); i++) {
            rect = GameConstants.coordsToRelative(245, 683 - i * 103, 430, 35);
            rect.x += xOffset;
            workersDisplay[i] = new WorkerDisplay(rect, skin, manager, this);
            workersDisplay[i].addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if (((count > 1 && Gdx.app.getType() != ApplicationType.Desktop) || button == 1) && (event.getTarget().getName() == null
                            || (!event.getTarget().getName().equals("minusButton") && !event.getTarget().getName().equals("plusButton")))) {
                        starSystem.getManager().setActive(!starSystem.getManager().isActive());
                        manager.getRaceController().getPlayerRace().reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, true);
                        show();
                    }
                }
            });
            stage.addActor(workersDisplay[i]);
        }
        showResources = false;

        rect = GameConstants.coordsToRelative(245, 168, 250, 35);
        rect.x += xOffset;
        workerTable = new Table();
        workerTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        workerTable.align(Align.left);
        workerScroller = new ScrollPane(workerTable, skin);
        workerScroller.setVariableSizeKnobs(false);
        workerScroller.setScrollingDisabled(false, true);
        workerScroller.setFadeScrollBars(true);
        workerScroller.setScrollbarsOnTop(true);
        workerScroller.setBounds(rect.x, rect.y, rect.width, rect.height);

        BitmapFont font = skin.getFont("normalFont");
        countLabel = new Label("", skin, "normalFont", race.getRaceDesign().clrNormalText);
        countLabel.setPosition((int) rect.x, (int) (rect.y + rect.height + font.getLineHeight() / 2));

        TextButtonStyle style = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        selectButton = new TextButton(StringDB.getString("BTN_RESOURCES"), style);
        rect = GameConstants.coordsToRelative(610, 110, 133, 30);
        rect.x += xOffset;
        selectButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        selectButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                showResources = !showResources;
                selectButton.setText(StringDB.getString(showResources ? "BTN_PRODUCTION" : "BTN_RESOURCES"));
                show();
                update();
            }
        });

        stage.addActor(nameTable);
        stage.addActor(workerScroller);
        stage.addActor(countLabel);
        stage.addActor(selectButton);
        hide();
    }

    public void show() {
        nameTable.clear();
        nameTable.add(StringDB.getString("WORKERS_MENUE") + " " + starSystem.getName(), "hugeFont",
                race.getRaceDesign().clrNormalText);
        nameTable.setVisible(true);

        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.RESEARCH_WORKER.getType(); i++) {
            WorkerType wk = WorkerType.fromWorkerType(showResources ? i + WorkerType.TITAN_WORKER.getType() : i); //quick and dirty switch
            workersDisplay[i].update(starSystem, wk);
            workersDisplay[i].setVisible(true);
        }
        workerScroller.setVisible(true);
        countLabel.setVisible(true);
        selectButton.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.RESEARCH_WORKER.getType(); i++) {
            workersDisplay[i].setVisible(false);
        }
        workerScroller.setVisible(false);
        countLabel.setVisible(false);
        selectButton.setVisible(false);
    }

    public void update() {
        int numWorkers = starSystem.getNumberOfWorkBuildings(WorkerType.ALL_WORKER, 0);
        workerTable.clear();
        float padding = GameConstants.wToRelative(5);
        float width = workerScroller.getWidth() / 15 - padding;
        String text = StringDB.getString("FREE_WORKERS") + " " + starSystem.getWorker(WorkerType.FREE_WORKER) + "/"
                + numWorkers;
        countLabel.setText(text);

        for (int i = 0; i < numWorkers; i++) {
            TextButtonStyle styleOff = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            Color offlineColor = new Color(42 / 255.0f, 46 / 255.0f, 30 / 255.0f, 1.0f);
            styleOff.up = skin.newDrawable("workerdrawable", offlineColor);
            styleOff.down = skin.newDrawable("workerdrawable", offlineColor);
            styleOff.over = skin.newDrawable("workerdrawable", offlineColor);

            TextButtonStyle styleOn = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            Color onlineColor = new Color(42 / 255.0f, 46 / 255.0f, 30 / 255.0f, 1.0f);
            if (i < starSystem.getWorker(WorkerType.FREE_WORKER)) {
                int c = i * 4;
                if (c > 230)
                    c = 200;
                onlineColor = new Color((230 - c) / 255.0f, (230 - c / 2) / 255.0f, 20 / 255.0f, 1.0f);
                styleOn.up = skin.newDrawable("workerdrawable", onlineColor);
                styleOn.down = skin.newDrawable("workerdrawable", onlineColor);
                styleOn.over = skin.newDrawable("workerdrawable", onlineColor);
                TextButton b = new TextButton("", styleOn);
                workerTable.add(b).width(width).height(workerScroller.getHeight()).spaceLeft(padding).align(Align.left);
            } else {
                TextButton b = new TextButton("", styleOff);
                workerTable.add(b).width(width).height(workerScroller.getHeight()).spaceLeft(padding).align(Align.left);
            }
        }
        buildList.drawProduction();
        buildList.drawAssemblyList(true);
    }

    public void setStarSystem(StarSystem system) {
        this.starSystem = system;
    }
}
