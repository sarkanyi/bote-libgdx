/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.StarSystem.SetWorkerMode;

public class WorkerDisplay extends Group {
    private int selectedIndex;
    private ScrollPane workerScroller;
    private Table workerTable;
    private StarSystem starSystem;
    private WorkerType workerType;
    private int numBuildings; //number of buildings of this type
    private Skin skin;
    private Major race;
    private Label countLabel;
    private Label yieldLabel;
    private ResourceManager manager;
    private SystemProduction systemProd; //needed to refresh production info
    private Image buildingImage;
    private Button plusButton;
    private Button minusButton;
    private String buildingPath;

    public WorkerDisplay(Rectangle position, Skin skin, ResourceManager manager, SystemProduction systemProd) {
        this.skin = skin;
        this.manager = manager;
        this.race = manager.getRaceController().getPlayerRace();
        this.systemProd = systemProd;
        selectedIndex = -1;
        workerTable = new Table();
        workerTable.setBounds((int) position.x, (int) position.y, (int) position.width, (int) position.height);
        workerTable.align(Align.left);

        BitmapFont font = skin.getFont("normalFont");
        countLabel = new Label("", skin, "normalFont", race.getRaceDesign().clrNormalText);
        countLabel.setPosition((int) position.x, (int) (position.y + position.height + font.getLineHeight() / 2));
        yieldLabel = new Label("", skin, "mediumFont", race.getRaceDesign().clrNormalText);
        yieldLabel.setPosition((int) position.x, (int) (position.y - font.getLineHeight() * 2 / 3));

        workerScroller = new ScrollPane(workerTable, skin);
        workerScroller.setVariableSizeKnobs(false);
        workerScroller.setBounds((int) position.x, (int) position.y, (int) position.width, (int) position.height);
        workerScroller.setScrollingDisabled(false, true);
        workerScroller.setFadeScrollBars(true);
        workerScroller.setScrollbarsOnTop(true);
        buildingImage = new Image();
        buildingImage.setBounds(position.x - GameConstants.wToRelative(182),
                position.y - GameConstants.wToRelative(25), GameConstants.wToRelative(100),
                GameConstants.wToRelative(75));

        ButtonStyle minusStyle = new ButtonStyle(skin.get("default", ButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        minusButton = new Button(minusStyle);
        minusButton.setName("minusButton");
        minusButton.setBounds(position.x - position.height * 1.5f, position.y, position.height, position.height);
        minusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!starSystem.getManager().isActive())
                    decrementIndex();
            }
        });

        ButtonStyle plusStyle = new ButtonStyle(skin.get("default", ButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");
        plusButton = new Button(plusStyle);
        plusButton.setName("plusButton");
        plusButton.setBounds(position.x + position.width + position.height, position.y, position.height,
                position.height);
        plusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!starSystem.getManager().isActive())
                    incrementIndex();
            }
        });

        this.addActor(workerScroller);
        this.addActor(countLabel);
        this.addActor(yieldLabel);
        this.addActor(buildingImage);
        this.addActor(plusButton);
        this.addActor(minusButton);
        buildingPath = "";
    }

    public void update(StarSystem system, WorkerType wk) {
        workerTable.clear();
        starSystem = system;
        workerType = wk;
        numBuildings = starSystem.getNumberOfWorkBuildings(wk, 0);
        selectedIndex = starSystem.getWorker(wk) - 1;

        float padding = (int) GameConstants.wToRelative(5);
        float width = (int) (workerScroller.getWidth() / 15 - padding);
        for (int i = 0; i < numBuildings; i++) {
            TextButtonStyle style = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            Color offlineColor = new Color(42 / 255.0f, 46 / 255.0f, 30 / 255.0f, 1.0f);
            style.up = skin.newDrawable("workerdrawable", offlineColor);
            style.down = skin.newDrawable("workerdrawable", offlineColor);
            style.over = skin.newDrawable("workerdrawable", offlineColor);

            TextButton workerImage = new TextButton("", style);
            workerImage.setName("" + i);
            workerTable.add(workerImage).width(width).height((int) (workerScroller.getHeight())).spaceLeft(padding)
                    .align(Align.left);
            workerImage.addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (!starSystem.getManager().isActive() && (button == 0)) {
                        selectedIndex = Integer.parseInt(event.getListenerActor().getName());
                        if (selectedIndex >= starSystem.getWorker(WorkerType.FREE_WORKER)
                                + starSystem.getWorker(workerType)) {
                            selectedIndex = starSystem.getWorker(WorkerType.FREE_WORKER) + starSystem.getWorker(workerType)
                                    - 1;
                        }
                        updateToReflectShanges();
                    }
                    return true;
                }
            });
        }
        updateToReflectShanges();
    }

    private void selectUntilIndex() {
        int id = starSystem.getNumberOfWorkBuildings(workerType, 1);
        String text = "";
        buildingImage.setDrawable(null);
        minusButton.setVisible(false);
        plusButton.setVisible(false);
        if (id != 0) {
            BuildingInfo bi = manager.getBuildingInfo(id);
            text = numBuildings + " x " + bi.getBuildingName();
            if (manager.getAssetManager().isLoaded(buildingPath))
                manager.getAssetManager().unload(buildingPath);
            buildingPath = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
            buildingImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager
                    .loadTextureImmediate(buildingPath))));
            if (selectedIndex + 1 < numBuildings)
                text += StringDB.getString("WORKERS_ACTIVE", false, "" + (selectedIndex + 1));

            if (!starSystem.getManager().isActive()) {
                minusButton.setVisible(true);
                plusButton.setVisible(true);
            }
        }
        countLabel.setText(text);
        String yield = "";
        if (id != 0) {
            yield = StringDB.getString("YIELD") + ": " + starSystem.getProduction().getXProdMax(workerType) + " ";
            if (workerType == WorkerType.INDUSTRY_WORKER)
                yield += StringDB.getString("INDUSTRY");
            else if (workerType == WorkerType.ENERGY_WORKER)
                yield += StringDB.getString("ENERGY");
            else if (workerType == WorkerType.SECURITY_WORKER)
                yield += StringDB.getString("SECURITY");
            else if (workerType == WorkerType.RESEARCH_WORKER)
                yield += StringDB.getString("RESEARCH");
            if (workerType.getResource() != null) {
                yield += StringDB.getString(workerType.getResource().getName());
                int store = starSystem.getResourceStore(workerType);
                int storeMax = starSystem.getXStoreMax(workerType);
                yield += ", " + StringDB.getString("SYSTEM_STORAGE_INFO", false, "" + store, "" + storeMax)
                        + StringDB.getString("SYSTEM_STORAGE_INFO_PERCENT", false, "" + (store * 100 / storeMax));
            }

        }
        yieldLabel.setText(yield);

        for (int k = 0; k < workerTable.getCells().size; k++) {
            TextButton b = (TextButton) workerTable.getCells().get(k).getActor();
            Color color;
            if (k <= selectedIndex) {
                int c = k * 4;
                if (c > 230)
                    c = 200;
                color = new Color((230 - c) / 255.0f, (230 - c / 2) / 255.0f, 20 / 255.0f, 1.0f);
            } else {
                color = new Color(42 / 255.0f, 46 / 255.0f, 30 / 255.0f, 1.0f);
            }

            if (starSystem.getManager().isActive())
                color = new Color(color.r * 0.5f, color.g * 0.5f, color.b * 0.5f, 1.0f);

            TextButtonStyle style = b.getStyle();
            style.up = skin.newDrawable("workerdrawable", color);
            style.down = skin.newDrawable("workerdrawable", color);
            style.over = skin.newDrawable("workerdrawable", color);
        }
    }

    private void updateToReflectShanges() {
        starSystem.setWorker(workerType, SetWorkerMode.SET_WORKER_MODE_SET, selectedIndex + 1);
        race.reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, false);
        selectUntilIndex();
        systemProd.update();
    }

    public void decrementIndex() {
        selectedIndex--;
        if (selectedIndex < -1)
            selectedIndex = -1;
        updateToReflectShanges();
    }

    public void incrementIndex() {
        selectedIndex++;
        if (selectedIndex >= starSystem.getWorker(WorkerType.FREE_WORKER) + starSystem.getWorker(workerType))
            selectedIndex = starSystem.getWorker(WorkerType.FREE_WORKER) + starSystem.getWorker(workerType) - 1;
        updateToReflectShanges();
    }

    public int selectedIndex() {
        return selectedIndex;
    }
}
