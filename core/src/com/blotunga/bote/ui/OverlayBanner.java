/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;

public class OverlayBanner extends Actor {
    private Color textColor;
    private Color backgroundColor;
    private Color borderColor;
    private int borderWidth;
    private String text;
    private String fontName;
    private Image background;
    private Array<Image> border;
    private Texture backgroundTexture;
    private Table textLabel;

    public OverlayBanner(Rectangle rect, String text, Color textColor, Texture backgroundTexture, Skin skin,
            String fontName) {
        this.text = text;
        this.textColor = new Color(textColor);
        this.fontName = fontName;
        backgroundColor = new Color(50 / 255.0f, 50 / 255.0f, 50 / 255.0f, 180 / 255.0f);
        borderColor = new Color(textColor.r, textColor.g, textColor.b, 1.0f);
        borderWidth = 2;
        this.backgroundTexture = backgroundTexture;
        Drawable tr = tintedDrawable(backgroundColor);
        background = new Image(tr);
        background.setBounds(rect.x, rect.y, rect.width, rect.height);

        border = new Array<Image>();
        for (int i = 0; i < 4; i++) {
            tr = tintedDrawable(borderColor);
            Image img = new Image(tr);
            float x = (i == 2) ? rect.x + rect.width - borderWidth : rect.x;
            float y = (i == 3) ? rect.y + rect.height - borderWidth : rect.y;
            float w = i % 2 == 0 ? borderWidth : rect.width;
            float h = i % 2 == 0 ? rect.height : borderWidth;
            img.setBounds(x, y, w, h);
            border.add(img);
        }

        textLabel = new Table(skin);
        Label l = new Label(text, skin, fontName, textColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        textLabel.add(l).width(rect.width - 2 * borderWidth).height(rect.height);
        textLabel.setTransform(true);
        textLabel.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        textLabel.setOrigin(rect.width / 2, rect.height / 2);
    }

    private SpriteDrawable tintedDrawable(Color color) {
        TextureRegion dr = new TextureRegion(backgroundTexture);
        Sprite sprite = new Sprite(dr);
        sprite.setColor(color);
        return new SpriteDrawable(sprite);
    }

    public void setBackgroundColor(Color color) {
        backgroundColor = new Color(color);
        background.setDrawable(tintedDrawable(backgroundColor));
    }

    public void setBorderColor(Color color) {
        borderColor = new Color(color);
        for (Image i : border)
            i.setDrawable(tintedDrawable(color));
    }

    public void setTextColor(Color color) {
        textColor = new Color(color);
        textLabel.clear();
        textLabel.add(text, fontName, textColor);
    }

    public void setBorderWidth(int width) {
        borderWidth = width;
    }

    public void rotateText(float amountInDegrees) {
        textLabel.rotateBy(amountInDegrees);
    }

    @Override
    public void act(float delta) {
        background.act(delta);
        for (Image i : border)
            i.act(delta);
        textLabel.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        background.draw(batch, parentAlpha);
        for (Image i : border)
            i.draw(batch, parentAlpha);
        textLabel.draw(batch, parentAlpha);
    }
}
