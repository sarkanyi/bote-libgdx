/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.transportview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class TransportView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private TextButtonStyle plusStyle;
    private TextButtonStyle minusStyle;
    private TextButtonStyle smallButtonStyle;
    private Table nameTable;
    private Table resourceTable;
    private boolean inOwnSystem;
    private Table headerTable;
    private int transportStorageQuantity;
    private Table troopMoveTable;
    private Array<Pair<Ships, Troop>> shipTroops;
    private int activeTroopInShip;
    private int activeTroopInSystem;
    private IntPoint lastSector;
    private Table multiplierTable;
    private Table leftTroopTable;
    private Table rightTroopTable;
    private ObjectSet<String> loadedTextures;

    //tooltip
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture tooltipTexture;

    public TransportView(ScreenManager manager, Stage stage, Skin skin, float xOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipHeaderColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = playerRace.getRaceDesign().clrNormalText;
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);

        inOwnSystem = false;
        transportStorageQuantity = 1;
        shipTroops = new Array<Pair<Ships, Troop>>();
        activeTroopInShip = -1;
        activeTroopInSystem = -1;
        lastSector = new IntPoint();
        loadedTextures = new ObjectSet<String>();

        plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");

        minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(285, 805, 602, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        resourceTable = new Table();
        rect = GameConstants.coordsToRelative(330, 667, 530, 360);
        resourceTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        resourceTable.align(Align.top);
        resourceTable.setSkin(skin);
        resourceTable.setVisible(false);
        stage.addActor(resourceTable);

        headerTable = new Table();
        rect = GameConstants.coordsToRelative(350, 725, 490, 25);
        headerTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        headerTable.align(Align.top);
        headerTable.setSkin(skin);
        headerTable.setVisible(false);
        stage.addActor(headerTable);

        troopMoveTable = new Table();
        rect = GameConstants.coordsToRelative(330, 191, 530, 150);
        troopMoveTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        troopMoveTable.align(Align.top);
        troopMoveTable.setSkin(skin);
        troopMoveTable.setVisible(false);
        stage.addActor(troopMoveTable);

        multiplierTable = new Table();
        rect = GameConstants.coordsToRelative(330, 280, 530, 150);
        multiplierTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        multiplierTable.align(Align.top);
        multiplierTable.setSkin(skin);
        multiplierTable.setVisible(false);
        stage.addActor(multiplierTable);

        leftTroopTable = new Table();
        rect = GameConstants.coordsToRelative(34, 720, 210, 660);
        leftTroopTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        leftTroopTable.align(Align.top);
        leftTroopTable.setSkin(skin);
        leftTroopTable.setVisible(false);
        stage.addActor(leftTroopTable);

        rightTroopTable = new Table();
        rect = GameConstants.coordsToRelative(950, 720, 210, 660);
        rightTroopTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        rightTroopTable.align(Align.top);
        rightTroopTable.setSkin(skin);
        rightTroopTable.setVisible(false);
        stage.addActor(rightTroopTable);
    }

    public void show() {
        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
        if (!lastSector.equals(ss.getCoordinates())) {
            lastSector = ss.getCoordinates();
            activeTroopInShip = -1;
            activeTroopInSystem = -1;
            shipTroops.clear();
        }
        nameTable.clear();
        String systemOwner = ss.getOwnerId();
        String shipOwner = manager.getUniverseMap().getCurrentShip().getOwnerId();
        inOwnSystem = systemOwner.equals(shipOwner);
        String text = StringDB.getString("TRANSPORT_MENUE") + " ";
        if (systemOwner.equals(shipOwner))
            text += ss.getName();
        else
            text += StringDB.getString("SHIP");
        nameTable.add(text, "hugeFont", normalColor);
        nameTable.setVisible(true);

        headerTable.clear();
        text = inOwnSystem ? StringDB.getString("SYSTEM_STORAGE") : "";
        Label l = new Label(text, skin, "normalFont", markColor);
        l.setAlignment(Align.left);
        headerTable.add(l).width((int) (headerTable.getWidth() / 2));
        text = StringDB.getString("SHIP_STORAGE");
        l = new Label(text, skin, "normalFont", markColor);
        l.setAlignment(Align.right);
        headerTable.add(l).width((int) (headerTable.getWidth() / 2));
        headerTable.setVisible(true);

        float vPadding = GameConstants.hToRelative(30);
        resourceTable.clear();
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.DERITIUM.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            if (i != 0)
                resourceTable.row().spaceTop(vPadding);
            drawResourceItem(0, 0, rt);
        }
        resourceTable.setVisible(true);

        multiplierTable.clear();
        if (inOwnSystem) {
            float buttonWidth = GameConstants.wToRelative(130);
            float width = (int) ((multiplierTable.getWidth() - buttonWidth - vPadding / 2) / 2);
            float height = GameConstants.hToRelative(30);
            text = StringDB.getString("MULTIPLIER");
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.right);
            multiplierTable.add(l).width(width).height(height).spaceRight((int) (vPadding / 2));
            text = "" + transportStorageQuantity;
            TextButton multiButton = new TextButton(text, smallButtonStyle);
            multiButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    transportStorageQuantity *= 10;
                    if (transportStorageQuantity > 10000)
                        transportStorageQuantity = 1;
                    show();
                }
            });
            multiplierTable.add(multiButton).width(buttonWidth).height(height);
            Ships ship = manager.getUniverseMap().getCurrentShip();
            int usedStorage = ship.getUsedStorageRoom(manager.getTroopInfos());
            int storageRoom = ship.getStorageRoom();
            for (int i = 0; i < ship.getFleetSize(); i++) {
                usedStorage += ship.getFleet().getAt(i).getUsedStorageRoom(manager.getTroopInfos());
                storageRoom += ship.getFleet().getAt(i).getStorageRoom();
            }
            text = String.format("%s: %d", StringDB.getString("AVAILABLE"), storageRoom - usedStorage);
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.right);
            multiplierTable.add(l).width(width).height(height);
            multiplierTable.setVisible(true);
        }
        drawTroopMoveTable();

        if (inOwnSystem)
            drawTroopTable(leftTroopTable, false);
        drawTroopTable(rightTroopTable, true);
    }

    private void drawTroopTable(Table table, boolean isShip) {
        table.clear();
        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
        Troop troop = null;
        if (isShip && shipTroops.size > 0)
            troop = shipTroops.get(activeTroopInShip).getSecond();
        if (!isShip && ss.getTroops().size > 0)
            troop = ss.getTroops().get(activeTroopInSystem);
        if (troop != null) {
            float imgHeight = GameConstants.hToRelative(185);
            float imgPad = GameConstants.hToRelative(25);
            float textHeight = GameConstants.hToRelative(30);
            TroopInfo info = manager.getTroopInfos().get(troop.getID());
            String path = "graphics/troops/" + info.getGraphicFile() + ".png";
            Image img = new Image(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(path))));
            info.getTooltip(playerRace.getEmpire().getResearch().getResearchInfo(), BaseTooltip.createTableTooltip(img, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
            loadedTextures.add(path);
            table.add(img).width((int) table.getWidth()).height((int) imgHeight);
            table.row().spaceTop((int) imgPad);
            String text = info.getName();
            Label l = new Label(text, skin, "normalFont", markColor);
            l.setAlignment(Align.center);
            table.add(l).width((int) table.getWidth()).height((int) textHeight);
            table.row();
            text = String.format("%s: %d", StringDB.getString("OPOWER"), troop.getOffense());
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth() - imgPad)).height((int) textHeight);
            table.row();
            text = String.format("%s: %d", StringDB.getString("DPOWER"), troop.getDefense());
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth() - imgPad)).height((int) textHeight);
            table.row();
            text = String.format("%s: %d", StringDB.getString("EXPERIANCE"), troop.getExperience());
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth() - imgPad)).height((int) textHeight);
            table.row();
            text = String.format("%s: %d", StringDB.getString("PLACE"), info.getSize());
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth() - imgPad)).height((int) textHeight);
            table.row();
            text = String.format("%s: %d", StringDB.getString("MORALVALUE"), info.getMoralValue());
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth() - imgPad)).height((int) textHeight);
            table.row();
            text = info.getDescription();
            l = new Label(text, skin, "normalFont", normalColor);
            l.setWrap(true);
            l.setAlignment(Align.left);
            table.add(l).width((int) (table.getWidth()));
        }
        table.setVisible(true);
    }

    private void initTroopsInfo() {
        Ships ship = manager.getUniverseMap().getCurrentShip();
        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
        shipTroops.clear();
        for (int i = 0; i < ship.getTroops().size; i++)
            shipTroops.add(new Pair<Ships, Troop>(ship, ship.getTroops().get(i)));

        for (int j = 0; j < ship.getFleetSize(); j++) {
            Ships s = ship.getFleet().getAt(j);
            for (int i = 0; i < s.getTroops().size; i++)
                shipTroops.add(new Pair<Ships, Troop>(s, s.getTroops().get(i)));
        }
        if (inOwnSystem && ss.getTroops().size > 0)
            if (activeTroopInSystem == -1 || activeTroopInSystem >= ss.getTroops().size)
                activeTroopInSystem = 0;
        if (shipTroops.size > 0)
            if (activeTroopInShip == -1 || activeTroopInShip >= shipTroops.size)
                activeTroopInShip = 0;
    }

    private boolean moveTroopsToShips(Ships ship, StarSystem ss) {
        while (transportStorageQuantity > 0) {
            int id = ss.getTroops().get(activeTroopInSystem).getID();
            int usedStorage = ship.getUsedStorageRoom(manager.getTroopInfos());
            usedStorage += manager.getTroopInfos().get(id).getSize();
            if (usedStorage <= ship.getStorageRoom()) {
                ship.getTroops().add(ss.getTroops().get(activeTroopInSystem));
                ss.getTroops().removeIndex(activeTroopInSystem);
                activeTroopInSystem = Math.max(0, activeTroopInSystem - 1);
                activeTroopInShip++;
                transportStorageQuantity--;
                if (ss.getTroops().size == 0)
                    return false;
            } else
                break;
        }
        return true;
    }

    private void drawTroopMoveTable() {
        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
        initTroopsInfo();
        float buttonHeight = GameConstants.hToRelative(35);
        float hPadding = GameConstants.wToRelative(35);
        float textWidth = GameConstants.wToRelative(130);
        float vPadding = GameConstants.hToRelative(25);
        troopMoveTable.clear();
        ButtonStyle bs = new ButtonStyle();
        Button button = new Button(bs);
        TextButton minusButton = new TextButton("", minusStyle);
        minusButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (shipTroops.size > 0) {
                    int quantity = transportStorageQuantity;
                    while (quantity > 0) {
                        Troop troop = shipTroops.get(activeTroopInShip).getSecond();
                        Ships ship = shipTroops.get(activeTroopInShip).getFirst();

                        int pos = -1;
                        for (int t = 0; t < ship.getTroops().size; t++)
                            if (ship.getTroops().get(t) == troop) {
                                pos = t;
                                break;
                            }
                        if (pos == -1) {
                            Gdx.app.error("TransportView", "Error finding troops");
                            return;
                        }
                        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
                        ss.getTroops().add(troop);
                        activeTroopInSystem = ss.getTroops().size - 1;
                        ship.getTroops().removeIndex(pos);
                        activeTroopInShip = Math.max(activeTroopInShip - 1, 0);
                        initTroopsInfo();
                        quantity--;
                        if (shipTroops.size == 0)
                            break;
                    }
                    show();
                }
            }
        });

        if (!inOwnSystem)
            minusButton.setVisible(false);
        button.add(minusButton).width(buttonHeight).height(buttonHeight).spaceRight(hPadding);
        String text = "";
        if (inOwnSystem)
            text = "" + ss.getTroops().size;
        Label l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.left);
        button.add(l).width(textWidth);
        text = StringDB.getString("TROOPS");
        l = new Label(text, skin, "normalFont", markColor);
        l.setAlignment(Align.center);
        button.add(l).width(textWidth);
        text = "" + shipTroops.size;
        l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.right);
        button.add(l).width(textWidth);
        TextButton plusButton = new TextButton("", plusStyle);
        plusButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
                if (ss.getTroops().size > 0) {
                    int oldQuantity = transportStorageQuantity;
                    Ships ship = manager.getUniverseMap().getCurrentShip();
                    boolean canDoMore = moveTroopsToShips(ship, ss);
                    if (canDoMore && ship.hasFleet())
                        for (int i = 0; i < ship.getFleetSize(); i++) {
                            canDoMore = moveTroopsToShips(ship.getFleet().getAt(i), ss);
                            if (!canDoMore)
                                break;
                        }
                    transportStorageQuantity = oldQuantity;
                    show();
                }
            }
        });
        if (!inOwnSystem)
            plusButton.setVisible(false);
        button.add(plusButton).width(buttonHeight).height(buttonHeight).spaceLeft(hPadding);
        troopMoveTable.add(button);
        troopMoveTable.row().spaceTop(vPadding);
        button = new Button(bs);
        textWidth = (int) troopMoveTable.getWidth() / 3;
        text = "";
        if (inOwnSystem)
            if (ss.getTroops().size > 0) {
                int id = ss.getTroops().get(activeTroopInSystem).getID();
                text = String.format("#%d: %s", activeTroopInSystem + 1, manager.getTroopInfos().get(id).getName());
            } else {
                text = StringDB.getString("NONE") + " " + StringDB.getString("SELECTED");
            }
        l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.left);
        button.add(l).width(textWidth);
        text = StringDB.getString("SELECTED");
        l = new Label(text, skin, "normalFont", markColor);
        l.setAlignment(Align.center);
        button.add(l).width(textWidth);
        if (shipTroops.size > 0) {
            int id = shipTroops.get(activeTroopInShip).getSecond().getID();
            text = String.format("#%d: %s", activeTroopInShip + 1, manager.getTroopInfos().get(id).getName());
        } else {
            text = StringDB.getString("NONE") + " " + StringDB.getString("SELECTED");
        }
        l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.right);
        button.add(l).width(textWidth);
        troopMoveTable.add(button).height(buttonHeight);
        troopMoveTable.row().spaceTop(vPadding / 3);
        button = new Button(bs);
        textWidth = GameConstants.hToRelative(150);
        buttonHeight = GameConstants.hToRelative(30);
        TextButton nextButton = new TextButton(StringDB.getString("BTN_NEXT"), smallButtonStyle);
        nextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                activeTroopInSystem++;
                show();
            }
        });
        nextButton.setVisible(false);
        if (inOwnSystem)
            if (ss.getTroops().size > 1)
                nextButton.setVisible(true);
        button.add(nextButton).width(textWidth).spaceRight(troopMoveTable.getWidth() - 2 * textWidth)
                .height(buttonHeight);
        nextButton = new TextButton(StringDB.getString("BTN_NEXT"), smallButtonStyle);
        nextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                activeTroopInShip++;
                show();
            }
        });
        nextButton.setVisible(false);
        if (shipTroops.size > 1)
            nextButton.setVisible(true);
        button.add(nextButton).width(textWidth).height(buttonHeight);
        troopMoveTable.add(button).height(buttonHeight);
        troopMoveTable.setVisible(true);
    }

    private int moveResFromSystemToShip(Ships ship, StarSystem ss, int res, int multi, int quantity) {
        int transportedRes = quantity;
        int usedStorage = ship.getUsedStorageRoom(manager.getTroopInfos());
        if (quantity * multi > ship.getStorageRoom() - usedStorage)
            quantity = (ship.getStorageRoom() - usedStorage) / multi;
        if (quantity > transportedRes)
            quantity = transportedRes;
        if (ss.getResourceStore(res) >= quantity) {
            transportedRes -= quantity;
            ss.addResourceStore(res, -quantity);
            ship.setLoadedResource(quantity, res);
        } else {
            transportedRes -= ss.getResourceStore(res);
            ship.setLoadedResource(ss.getResourceStore(res), res);
            ss.addResourceStore(res, -ss.getResourceStore(res));
        }
        return transportedRes;
    }

    private int moveResFromShipToSystem(Ships ship, StarSystem ss, int res, int quantity) {
        if (ship.getLoadedResources(res) >= quantity) {
            ss.addResourceStore(res, quantity);
            ship.setLoadedResource(-quantity, res);
            quantity = 0;
        } else {
            quantity -= ship.getLoadedResources(res);
            ss.addResourceStore(res, ship.getLoadedResources(res));
            ship.setLoadedResource(-ship.getLoadedResources(res), res);
        }
        return quantity;
    }

    private void drawResourceItem(int valueLeft, int valueRight, ResourceTypes rt) {
        Ships ship = manager.getUniverseMap().getCurrentShip();
        StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
        float buttonHeight = GameConstants.hToRelative(35);
        float hPadding = GameConstants.wToRelative(35);
        float textWidth = GameConstants.wToRelative(130);
        TextButton minusButton = new TextButton("", minusStyle);
        minusButton.setUserObject(rt);
        minusButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ResourceTypes rt = (ResourceTypes) event.getListenerActor().getUserObject();
                Ships ship = manager.getUniverseMap().getCurrentShip();
                StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
                int oldQuantity = transportStorageQuantity;
                int res = rt.getType();
                if (rt != ResourceTypes.DERITIUM) {
                    if (transportStorageQuantity + ss.getResourceStore(res) > GameConstants.MAX_RES_STORE)
                        transportStorageQuantity = Math.max(GameConstants.MAX_RES_STORE - ss.getResourceStore(res), 0);
                } else {
                    int maxDeritiumStore = GameConstants.MAX_DERITIUM_STORE;
                    int bonus = playerRace.getEmpire().getResearch().getResearchInfo()
                            .isResearchedThenGetBonus(ResearchComplexType.STORAGE_AND_TRANSPORT, 1);
                    if (bonus != 0)
                        maxDeritiumStore *= bonus;
                    if (transportStorageQuantity + ss.getResourceStore(res) > maxDeritiumStore)
                        transportStorageQuantity = Math.max(maxDeritiumStore - ss.getResourceStore(res), 0);
                }
                if (transportStorageQuantity == 0) {
                    transportStorageQuantity = oldQuantity;
                    return;
                }
                transportStorageQuantity = moveResFromShipToSystem(ship, ss, res, transportStorageQuantity);
                if (transportStorageQuantity != 0 && ship.hasFleet())
                    for (int i = 0; i < ship.getFleetSize(); i++) {
                        transportStorageQuantity = moveResFromShipToSystem(ship.getFleet().getAt(i), ss, res, transportStorageQuantity);
                        if (transportStorageQuantity == 0)
                            break;
                    }
                transportStorageQuantity = oldQuantity;
                show();
            }
        });
        if (!inOwnSystem)
            minusButton.setVisible(false);
        resourceTable.add(minusButton).width(buttonHeight).height(buttonHeight).spaceRight(hPadding);
        String text = "";
        if (inOwnSystem)
            text = "" + ss.getResourceStore(rt.getType());
        Label l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.left);
        resourceTable.add(l).width(textWidth);
        text = StringDB.getString(rt.getName());
        l = new Label(text, skin, "normalFont", markColor);
        l.setAlignment(Align.center);
        resourceTable.add(l).width(textWidth);
        int resNum = ship.getLoadedResources(rt.getType());
        for (int i = 0; i < ship.getFleetSize(); i++)
            resNum += ship.getFleet().getAt(i).getLoadedResources(rt.getType());
        text = "" + resNum;
        l = new Label(text, skin, "normalFont", normalColor);
        l.setAlignment(Align.right);
        resourceTable.add(l).width(textWidth);
        TextButton plusButton = new TextButton("", plusStyle);
        plusButton.setUserObject(rt);
        plusButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Ships ship = manager.getUniverseMap().getCurrentShip();
                StarSystem ss = manager.getUniverseMap().getCurrentStarSystem();
                ResourceTypes rt = (ResourceTypes) event.getListenerActor().getUserObject();
                int multi = 1;
                if (rt == ResourceTypes.DERITIUM)
                    multi = 250;
                int quantityToMove = transportStorageQuantity;
                quantityToMove = moveResFromSystemToShip(ship, ss, rt.getType(), multi, quantityToMove);
                if (quantityToMove != 0 && ship.hasFleet())
                    for (int i = 0; i < ship.getFleetSize(); i++) {
                        quantityToMove = moveResFromSystemToShip(ship.getFleet().getAt(i), ss, rt.getType(), multi,
                                quantityToMove);
                        if (quantityToMove == 0)
                            break;
                    }
                show();
            }
        });
        if (!inOwnSystem)
            plusButton.setVisible(false);
        resourceTable.add(plusButton).width(buttonHeight).height(buttonHeight).spaceLeft(hPadding);
    }

    public void hide() {
        nameTable.setVisible(false);
        resourceTable.setVisible(false);
        headerTable.setVisible(false);
        troopMoveTable.setVisible(false);
        multiplierTable.setVisible(false);
        leftTroopTable.setVisible(false);
        rightTroopTable.setVisible(false);
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }
}
