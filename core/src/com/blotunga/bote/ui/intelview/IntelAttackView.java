/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelObject;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;

public class IntelAttackView {
    private ResourceManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private ScrollPane attDescPane;
    private Table attDescTable;
    private TextButton selectButton;
    private TextButton cancelButton;
    private Table intelAttemptView;

    public IntelAttackView(final ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        Rectangle rect = GameConstants.coordsToRelative(70, 255, 1055, 55);
        attDescTable = new Table();
        attDescTable.align(Align.top);
        attDescTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        Label label = new Label(StringDB.getString("ATTEMPT_DESC"), skin, "normalFont", normalColor);
        label.setAlignment(Align.top, Align.center);
        label.setWrap(true);
        label.setTouchable(Touchable.disabled);
        attDescTable.add(label).width((int) attDescTable.getWidth());
        stage.addActor(attDescTable);
        attDescTable.setVisible(false);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        rect = GameConstants.coordsToRelative(455, 200, 140, 30);
        selectButton = new TextButton(StringDB.getString("BTN_SELECT"), smallButtonStyle);
        selectButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(selectButton);
        selectButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Intelligence intel = playerRace.getEmpire().getIntelligence();
                int activeReport = intel.getIntelReports().getActiveReport();
                if (activeReport != -1 && activeReport < intel.getIntelReports().getNumberOfReports()) {
                    IntelObject intelObj = intel.getIntelReports().getReport(activeReport);
                    if (intelObj.isSpy() && !intelObj.getEnemy().equals(playerRace.getRaceId())
                            && intelObj.getRound() > manager.getCurrentRound() - 10)
                        intel.getIntelReports().createAttemptObject(intel.getIntelReports().getReport(activeReport));
                }
                show();
            }
        });
        selectButton.setVisible(false);

        rect = GameConstants.coordsToRelative(600, 200, 140, 30);
        cancelButton = new TextButton(StringDB.getString("BTN_CANCEL"), smallButtonStyle);
        cancelButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(cancelButton);
        cancelButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Intelligence intel = playerRace.getEmpire().getIntelligence();
                intel.getIntelReports().removeAttemptObject();
                show();
            }
        });
        cancelButton.setVisible(false);

        rect = GameConstants.coordsToRelative(110, 165, 975, 100);
        intelAttemptView = new Table();
        intelAttemptView.align(Align.left);
        stage.addActor(intelAttemptView);
        intelAttemptView.setVisible(false);

        attDescPane = new ScrollPane(intelAttemptView, skin);
        attDescPane.setScrollingDisabled(true, false);
        attDescPane.setVariableSizeKnobs(false);
        attDescPane.setFadeScrollBars(false);
        attDescPane.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        attDescPane.setVisible(false);
        stage.addActor(attDescPane);
    }

    public void show() {
        Intelligence intel = playerRace.getEmpire().getIntelligence();
        attDescTable.setVisible(true);
        attDescPane.setVisible(true);
        selectButton.setVisible(true);
        cancelButton.setVisible(true);
        intelAttemptView.clear();
        if (intel.getIntelReports().getAttemptObject() != null) {
            IntelObject intelObj = intel.getIntelReports().getAttemptObject();
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.align(Align.left);
            button.setSkin(skin);
            for (IntelReportHeader ih : IntelReportHeader.values()) {
                String text = "";
                if (ih == IntelReportHeader.ROUND)
                    text = "" + intelObj.getRound();
                else if (ih == IntelReportHeader.KIND)
                    text = intelObj.isSpy() ? StringDB.getString("SPY") : StringDB.getString("SABOTAGE");
                else if (ih == IntelReportHeader.ENEMY) {
                    Major enemy = Major.toMajor(manager.getRaceController().getRace(intelObj.getEnemy()));
                    if (enemy != null)
                        text = enemy.getEmpireName();
                    else
                        text = StringDB.getString("UNKNOWN");
                } else if (ih == IntelReportHeader.TYPE) {
                    switch (intelObj.getType()) {
                        case 0:
                            text = StringDB.getString("ECONOMY");
                            break;
                        case 1:
                            text = StringDB.getString("SCIENCE");
                            break;
                        case 2:
                            text = StringDB.getString("MILITARY");
                            break;
                        case 3:
                            text = StringDB.getString("DIPLOMACY");
                            break;
                        default:
                            text = StringDB.getString("UNKNOWN");
                            break;
                    }
                }
                Label label = new Label(text, skin, "normalFont", Color.WHITE);
                label.setColor(markColor);
                button.add(label).width(ih.getWidth());
            }
            intelAttemptView.add(button);
            intelAttemptView.row();
            String text = intelObj.getOwnerDesc();
            Label txt = new Label(text, skin, "normalFont", normalColor);
            txt.setWrap(true);
            txt.setAlignment(Align.center);
            int width = (int) (attDescPane.getWidth() - attDescPane.getStyle().vScrollKnob.getMinWidth());
            intelAttemptView.add(txt).width(width);
            intelAttemptView.setVisible(true);
        }
    }

    public void hide() {
        attDescTable.setVisible(false);
        attDescPane.setVisible(false);
        selectButton.setVisible(false);
        cancelButton.setVisible(false);
        intelAttemptView.setVisible(false);
    }
}
