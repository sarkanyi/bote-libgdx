/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.utils.IntMap;

public enum IntelViewMainButtonType {
    BTN_ASSIGNMENT("BTN_ASSIGNMENT", "intelassignmenu", 0),
    BTN_SPY("BTN_SPY", "intelspymenu", 1),
    BTN_SABOTAGE("BTN_SABOTAGE", "intelsabmenu", 2),
    INFORMATION("INFORMATION", "intelinfomenu", 3),
    BTN_REPORTS("BTN_REPORTS", "intelreportmenu", 4),
    BTN_ATTEMPT("BTN_ATTEMPT", "intelattackmenu", 5);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<IntelViewMainButtonType> intToTypeMap = new IntMap<IntelViewMainButtonType>();

    IntelViewMainButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (IntelViewMainButtonType it : IntelViewMainButtonType.values()) {
            intToTypeMap.put(it.ord, it);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static IntelViewMainButtonType fromInt(int i) {
        IntelViewMainButtonType it = intToTypeMap.get(i);
        return it;
    }
}
