/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.mainmenu.LoadingBar;

public class LoadingScreen extends DefaultScreen {
    private Stage stage;
    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;
    private float startX, endX;
    private float percent;
    private Actor loadingBar;
    private Label progress;

    public LoadingScreen(ScreenManager gm) {
        super(gm);
    }

    @Override
    public void show() {
        Gdx.graphics.setContinuousRendering(true);
        // Tell the manager to load assets for the loading screen
        String path = "graphics/ui/loading.pack";
        if(!game.getAssetManager().isLoaded(path))
            game.getAssetManager().load(path, TextureAtlas.class);
        // Wait until they are finished loading
        game.getAssetManager().finishLoading();
        // Initialize the stage where we will place everything
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        // Get our textureatlas from the manager
        TextureAtlas atlas = game.getAssetManager().get("graphics/ui/loading.pack", TextureAtlas.class);
        loadingFrame = new Image(atlas.findRegion("loading-frame"));
        loadingBarHidden = new Image(atlas.findRegion("loading-bar-hidden"));
        screenBg = new Image(atlas.findRegion("screen-bg"));
        loadingBg = new Image(atlas.findRegion("loading-frame-bg"));
        // Add the loading bar animation
        Animation<TextureRegion> anim = new Animation<TextureRegion>(0.05f, atlas.findRegions("loading-bar-anim"));
        anim.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        loadingBar = new LoadingBar(anim);
        // Or if you only need a static bar, you can do
        // loadingBar = new Image(atlas.findRegion("loading-bar1"));
        // Add all the actors to the stage
        stage.addActor(screenBg);
        stage.addActor(loadingBar);
        stage.addActor(loadingBg);
        stage.addActor(loadingBarHidden);
        stage.addActor(loadingFrame);
        // Add everything to be loaded, for instance:
        Skin skin = game.getAssetManager().get(GameConstants.UISKIN_PATH);
        progress = new Label("", skin);
        stage.addActor(progress);

        Major playerRace = game.getRaceController().getPlayerRace();
        path = "graphics/star/StarMaps.pack";
        if(!game.getAssetManager().isLoaded(path))
            game.getAssetManager().load(path, TextureAtlas.class);
        for (int i = 0; i < game.getRaceController().getMajors().size; i++) {
            Major major = game.getRaceController().getMajors().getValueAt(i);
            path = "graphics/ui/" + major.getPrefix() + "ui.pack";
            if (!game.getAssetManager().isLoaded(path) && !game.fileMissing(path))
                game.getAssetManager().load(path, TextureAtlas.class);
        }
        path = "graphics/ui/general_ui.pack";
        if(!game.getAssetManager().isLoaded(path))
            game.getAssetManager().load(path, TextureAtlas.class);
        TextureParameter param = new TextureParameter();
        param.genMipMaps = game.useMipMaps();
        param.minFilter = game.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
        param.magFilter = TextureFilter.Linear;
        path = "graphics/backgrounds/" + playerRace.getPrefix() + "menuV2.png";
        if(!game.getAssetManager().isLoaded(path))
            game.getAssetManager().load(path, Texture.class, param);
        path = "graphics/backgrounds/" + playerRace.getPrefix() + "galaxyV3.png";
        if(!game.getAssetManager().isLoaded(path))
            game.getAssetManager().load(path, Texture.class, param);
        path = "graphics/galaxy/" + game.getGameSettings().backgroundMod + playerRace.getPrefix() + "galaxy.jpg";
        if(!game.getAssetManager().isLoaded(path));
            game.getAssetManager().load(path, Texture.class, param);

        if (game.getGameSettings().isPreload()) { //preload backgrounds
            String prefix = game.getRaceController().getPlayerRace().getPrefix();
            path = "graphics/backgrounds/" + prefix;
            Array<String> textures = new Array<String>();
            textures.add(path + "buildmenu" + ".jpg");
            textures.add(path + "workmenu" + ".jpg");
            textures.add(path + "diploinmenu" + ".jpg");
            textures.add(path + "emptyur" + ".jpg");
            textures.add(path + "intelassignmenu" + ".jpg");
            textures.add(path + "intelsabmenu" + ".jpg");
            textures.add(path + "newsovmenu" + ".jpg");
            textures.add(path + "shipovmenu" + ".jpg");
            textures.add(path + "trademenu" + ".jpg");
            textures.add(path + "urmenu" + ".jpg");
            textures.add(path + "demomenu" + ".jpg");
            textures.add(path + "diplomacyV3" + ".jpg");
            textures.add(path + "energymenu" + ".jpg");
            textures.add(path + "intelattackmenu" + ".jpg");
            textures.add(path + "intelspymenu" + ".jpg");
            textures.add(path + "overviewmenu" + ".jpg");
            textures.add(path + "systemovmenu" + ".jpg");
            textures.add(path + "tradetransfermenu" + ".jpg");
            textures.add(path + "victorymenu" + ".jpg");
            textures.add(path + "designmenu" + ".jpg");
            textures.add(path + "diplooutmenu" + ".jpg");
            textures.add(path + "fleetmenu" + ".jpg");
            textures.add(path + "intelinfomenu" + ".jpg");
            textures.add(path + "researchmenu" + ".jpg");
            textures.add(path + "systrademenu" + ".jpg");
            textures.add(path + "tradeV3" + ".jpg");
            textures.add(path + "diploinfomenu" + ".jpg");
            textures.add(path + "emptyscreen" + ".jpg");
            textures.add(path + "intelreportmenu" + ".jpg");
            textures.add(path + "monopolmenu" + ".jpg");
            textures.add(path + "researchV3" + ".jpg");
            textures.add(path + "top5menu" + ".jpg");
            textures.add(path + "transportmenu" + ".jpg");
            for (String s : textures)
                if (!game.getAssetManager().isLoaded(s))
                    game.getAssetManager().load(s, Texture.class, param);
        }
    }

    @Override
    public void resize(int width, int height) {
        // Set our screen to always be XXX x 480 in size
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        stage.getViewport().update(width, height, false);

        // Make the background fill the screen
        screenBg.setSize(width, height);

        // Place the loading frame in the middle of the screen
        loadingFrame.setX((stage.getWidth() - loadingFrame.getWidth()) / 2);
        loadingFrame.setY((stage.getHeight() - loadingFrame.getHeight()) / 2);

        // Place the loading bar at the same spot as the frame, adjusted a few px
        loadingBar.setX(loadingFrame.getX() + 15);
        loadingBar.setY(loadingFrame.getY() + 5);

        // Place the image that will hide the bar on top of the bar, adjusted a few px
        loadingBarHidden.setX(loadingBar.getX() + 35);
        loadingBarHidden.setY(loadingBar.getY() - 3);
        // The start position and how far to move the hidden loading bar
        startX = loadingBarHidden.getX();
        endX = 440;

        progress.setPosition(loadingBar.getX(), loadingBar.getY());

        // The rest of the hidden bar
        loadingBg.setSize(450, 50);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setY(loadingBarHidden.getY() + 3);
    }

    @Override
    public void render(float delta) {
        // Clear the screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (game.getAssetManager().update()) { // Load some, will return true if done loading
            ((ScreenManager) game).setView(ViewTypes.GALAXY_VIEW, false);
            this.dispose();
        }

        // Interpolate the percentage to make it more smooth
        percent = Interpolation.linear.apply(percent, game.getAssetManager().getProgress(), 0.1f);
        progress.setText(Math.round(percent * 10000) / 100.0f + "%");
        progress.setBounds(loadingFrame.getX(), loadingFrame.getY(), loadingFrame.getWidth(), loadingFrame.getHeight());
        progress.setAlignment(Align.center);

        // Update positions (and size) to match the percentage
        loadingBarHidden.setX(startX + endX * percent);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setWidth(450 - 450 * percent);
        loadingBg.invalidate();

        // Show the loading screen
        stage.act();
        stage.draw();
    }

    @Override
    public void hide() {
        Gdx.graphics.setContinuousRendering(false);
    }

    @Override
    public void dispose() {
        Gdx.app.postRunnable(new Runnable() {
            private final Runnable _self = this;
            private int counter = 10;

            @Override
            public void run() {
                if (counter-- > 0) {
                    Gdx.app.postRunnable(_self);
                    return;
                }
                // Dispose the loading assets as we no longer need them
                if (game.getAssetManager().isLoaded("graphics/ui/loading.pack"))
                    game.getAssetManager().unload("graphics/ui/loading.pack");
            }
        });
    }
}
