/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.LeftSideBar;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.SimpleDirectionGestureDetector;
import com.blotunga.bote.utils.ui.HelpWidget;
import com.blotunga.bote.utils.ui.ZoomWithLock;

public abstract class ZoomableScreen extends DefaultScreen {
    private final OrthographicCamera camera;
    private Image background;
    protected Image backgroundBottom;
    private String menuPath;
    protected Skin skin;
    protected final ZoomWithLock inputs;
    protected Stage mainStage;
    protected final LeftSideBar sidebarLeft;
    private String bgFile = "";
    private String bgBottomFile = "";
    private Camera oldSidebarCamera;
    private HelpWidget helpWidget;
    private float xOffset = 0.0f;
    protected float bottomHeight;

    public ZoomableScreen(final ScreenManager screenManager, String backgroundFile, String backgroundBottomFile) {
        this(screenManager, backgroundFile, backgroundBottomFile, true);
    }

    public ZoomableScreen(final ScreenManager screenManager, String backgroundFile, String backgroundBottomFile, boolean changeSystemAllowed) {
        super(screenManager);
        bgFile = backgroundFile;
        bgBottomFile = backgroundBottomFile;
        skin = screenManager.getSkin();

        sidebarLeft = screenManager.getSidebarLeft();
        xOffset = sidebarLeft.getPosition().width;

        camera = new OrthographicCamera();
        mainStage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));

        Rectangle rect = GameConstants.coordsToRelative(1150, 810, 50, 50);
        rect.x += xOffset;

        inputs = new ZoomWithLock(screenManager, camera, mainStage, rect);
        final Major playerRace = screenManager.getRaceController().getPlayerRace();
        inputs.addProcessor(sidebarLeft.getStage());

        if (changeSystemAllowed) {
            inputs.addProcessor(new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {
                int currentIndex;
                Array<IntPoint> systemList;

                public void applyNewIndex() {
                    screenManager.getUniverseMap().setSelectedCoordValue(systemList.get(currentIndex));
                    show();
                }

                @Override
                public void onUp() {
                    onLeft();
                }

                private void updateInfo() {
                    systemList = playerRace.getEmpire().getSystemList();
                    currentIndex = systemList.indexOf(sidebarLeft.getStarSystemCoord(), false);
                }

                @Override
                public void onRight() {
                    if(!inputs.isZoomable()) {
                        updateInfo();
                        if (currentIndex < systemList.size - 1)
                            currentIndex++;
                        else
                            currentIndex = 0;
                        applyNewIndex();
                    }
                }

                @Override
                public void onLeft() {
                    if(!inputs.isZoomable()) {
                        updateInfo();
                        if (currentIndex > 0)
                            currentIndex--;
                        else
                            currentIndex = systemList.size - 1;
                        applyNewIndex();
                    }
                }

                @Override
                public void onDown() {
                    onRight();
                }
            }));
        }
        setInputProcessor(inputs);

        menuPath = "graphics/backgrounds/" + getResourceManager().getRaceController().getPlayerRace().getPrefix();
        int yOffset = 0;
        bottomHeight = 0;
        if (!backgroundBottomFile.isEmpty()) {
            bottomHeight = GameConstants.hToRelative(190);
            yOffset = (int) bottomHeight;
            TextureRegion bgTexture = new TextureRegion(screenManager.loadTextureImmediate(menuPath
                    + backgroundBottomFile + ".jpg"));
            backgroundBottom = new Image(bgTexture);
            backgroundBottom.setBounds(xOffset, 0, Gdx.graphics.getWidth() - xOffset, bottomHeight);
            backgroundBottom.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("x = " + x + " y = " + y);
                    if (button == 1)
                        screenManager.setView(ViewTypes.GALAXY_VIEW);
                    return false;
                }
            });

            mainStage.addActor(backgroundBottom);
        }
        if (!backgroundFile.isEmpty()) {
            TextureRegion bgTexture = new TextureRegion(screenManager.loadTextureImmediate(menuPath + backgroundFile
                    + ".jpg"));
            background = new Image(bgTexture);
            background.setBounds(xOffset, yOffset, GameConstants.wToRelative(1440) - xOffset, GameConstants.hToRelative(810) - yOffset);
            background.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("x = " + x + " y = " + y);
                    if (button == 1)
                        screenManager.setView(ViewTypes.GALAXY_VIEW);
                    return false;
                }
            });

            if (changeSystemAllowed) {
                mainStage.addListener(new InputListener() {
                    @Override
                    public boolean keyDown(InputEvent event, int keycode) {
                        if (game.getScreen() instanceof SystemBuildScreen || game.getScreen() instanceof TradeScreen) {
                            Array<IntPoint> systemList = playerRace.getEmpire().getSystemList();
                            int currentIndex = systemList.indexOf(sidebarLeft.getStarSystemCoord(), false);
                            if (keycode == Keys.LEFT) {
                                if (currentIndex > 0)
                                    currentIndex--;
                                else
                                    currentIndex = systemList.size - 1;
                                screenManager.getUniverseMap().setSelectedCoordValue(systemList.get(currentIndex));
                                show();
                            } else if (keycode == Keys.RIGHT) {
                                if (currentIndex < systemList.size - 1)
                                    currentIndex++;
                                else
                                    currentIndex = 0;
                                screenManager.getUniverseMap().setSelectedCoordValue(systemList.get(currentIndex));
                                show();
                            }
                        }

                        if ((keycode == Keys.SPACE)
                                && (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT)
                                || !screenManager.getUniverseMap().searchNextIdleShipAndJumpToIt(ShipOrder.NONE)))
                            screenManager.endTurn();
                        return false;
                    }
                });
            }
            mainStage.addActor(background);
        }
        mainStage.addActor(inputs.getImage());

        rect = GameConstants.coordsToRelative(0, 810, 42, 42);
        rect.x += xOffset;
        helpWidget = new HelpWidget(screenManager, rect, this, mainStage);
    }

    public void setBackground(String backgroundFile) {
        String path = menuPath + bgFile + ".jpg";
        if (game.getGameSettings().isLowMemProfile()) {
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
        }
        this.bgFile = backgroundFile;
        background.setDrawable(new TextureRegionDrawable(new TextureRegion(game.loadTextureImmediate(menuPath
                + backgroundFile + ".jpg"))));
    }

    public Image getBackground() {
        return background;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sidebarLeft.getStage().act(1 / 30.0f);
        sidebarLeft.getStage().draw();

        mainStage.act(1 / 30.0f);
        mainStage.draw();

        game.getAchievementManager().render();
    }

    @Override
    public void resize(int width, int height) {
        sidebarLeft.getStage().getViewport().update(width, height, true);
        mainStage.getViewport().update(width, height, true);
        game.getAchievementManager().updateSize(width, height, true);
    }

    @Override
    public void dispose() {
        Gdx.app.postRunnable(new Runnable() {
            private final Runnable _self = this;
            private int counter = 10;

            @Override
            public void run() {
                if (counter-- > 0) {
                    Gdx.app.postRunnable(_self);
                    return;
                }
                inputs.dispose();
                helpWidget.dispose();
            }
        });
    }

    @Override
    public void show() {
        oldSidebarCamera = sidebarLeft.getStage().getViewport().getCamera();
        sidebarLeft.getStage().getViewport().setCamera(camera);            
        if (game.getGameSettings().isLowMemProfile()) {
            if (!bgFile.isEmpty()) {
                TextureRegion bgTexture = new TextureRegion(game.loadTextureImmediate(menuPath + bgFile + ".jpg"));
                background.setDrawable(new TextureRegionDrawable(bgTexture));
            }
            if (!bgBottomFile.isEmpty()) {
                TextureRegion bgTexture = new TextureRegion(game.loadTextureImmediate(menuPath + bgBottomFile + ".jpg"));
                backgroundBottom.setDrawable(new TextureRegionDrawable(bgTexture));
            }
        }
        setInputProcessor(inputs);
        sidebarLeft.showInfo();
    }

    @Override
    public void hide() {
        inputs.resetPositionAndZoom();
        if (game.getGameSettings().isLowMemProfile()) {
            if (!bgFile.isEmpty()) {
                String path = menuPath + bgFile + ".jpg";
                if (game.getAssetManager().isLoaded(path))
                    game.getAssetManager().unload(path);
            }
            if (!bgBottomFile.isEmpty()) {
                String path = menuPath + bgBottomFile + ".jpg";
                if (game.getAssetManager().isLoaded(path))
                    game.getAssetManager().unload(path);
            }
        }
        sidebarLeft.getStage().getViewport().setCamera(oldSidebarCamera);
    }

    public LeftSideBar getLeftSideBar() {
        return sidebarLeft;
    }

    public void setBackgroundSize(int width, int height) {
        background.setSize(width, height);
    }
}
