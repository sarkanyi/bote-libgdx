/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.ui.transportview.TransportView;

public class TransportScreen extends ZoomableScreen {
    private TransportView transportView;

    public TransportScreen(final ScreenManager screenManager) {
        super(screenManager, "transportmenu", "", false);
        float xOffset = sidebarLeft.getPosition().getWidth();

        transportView = new TransportView(screenManager, mainStage, skin, xOffset);
    }

    @Override
    public void show() {
        super.show();
        transportView.show();
    }

    @Override
    public void hide() {
        super.hide();
        transportView.hide();
    }

    @Override
    public void setSubMenu(int id) {
    }
}
