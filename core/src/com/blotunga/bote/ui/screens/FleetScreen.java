/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.ui.universemap.RightSideBar;
import com.blotunga.bote.ui.universemap.ShipRenderer;
import com.blotunga.bote.utils.IntPoint;

public class FleetScreen extends ZoomableScreen {
    private RightSideBar sidebarRight;
    private Table nameTable;
    private Table toFleetTable;
    private Table fromFleetTable;
    private ShipRenderer shipRenderer;
    private Color normalColor;
    private Color markPenColor;
    final private float shipPadding = GamePreferences.sceneHeight / 36;
    private Ships fleetShip;
    private ShipMap map;

    public FleetScreen(ScreenManager screenManager) {
        super(screenManager, "fleetmenu", "", false);
        float xOffset = sidebarLeft.getPosition().getWidth();
        Major playerRace = game.getRaceController().getPlayerRace();

        normalColor = playerRace.getRaceDesign().clrNormalText;
        markPenColor = playerRace.getRaceDesign().clrListMarkPenColor;

        sidebarRight = screenManager.getUniverseMap().getRenderer().getRightSideBar();
        inputs.addProcessor(sidebarRight.getStage());
        setBackgroundSize((int) (GamePreferences.sceneWidth - xOffset - sidebarRight.getPosition().getWidth()), (int) GamePreferences.sceneHeight);

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(0, 800, 945, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        mainStage.addActor(nameTable);
        nameTable.add(StringDB.getString("FLEET_MENUE"), "hugeFont", normalColor);
        nameTable.setVisible(true);

        rect = GameConstants.coordsToRelative(335, 740, 290, 650);
        rect.x += xOffset;
        float shipWidth = rect.width / 2 - shipPadding * 2;
        float shipHeight = shipWidth * 0.75f;
        shipHeight = Math.round(shipHeight / 20) * 20.0f;
        int shipsInPage = (int) (rect.height / shipHeight);
        shipWidth = shipHeight / 0.75f;
        float tableHeight = shipHeight * shipsInPage;
        Rectangle shipRendererPosition = new Rectangle(rect.x, rect.y, rect.width, tableHeight);
        shipRenderer = new ShipRenderer(screenManager, shipRendererPosition, shipWidth, shipHeight, shipPadding);

        toFleetTable = new Table();
        rect = GameConstants.coordsToRelative(30, 650, 160, 230);
        toFleetTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        toFleetTable.align(Align.top);
        toFleetTable.setSkin(skin);
        mainStage.addActor(toFleetTable);
        toFleetTable.setVisible(true);

        fromFleetTable = new Table();
        rect = GameConstants.coordsToRelative(28, 383, 160, 335);
        fromFleetTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        fromFleetTable.align(Align.top);
        fromFleetTable.setSkin(skin);
        mainStage.addActor(fromFleetTable);
        fromFleetTable.setVisible(true);
    }

    @Override
    public void show() {
        super.show();
        sidebarRight.getStage().getViewport().setCamera(sidebarLeft.getStage().getCamera());
        Rectangle rect = GameConstants.coordsToRelative(1150, 810, 50, 50);
        rect.x += sidebarLeft.getPosition().width;
        rect.x -= sidebarRight.getPosition().width;
        inputs.setLockBounds(rect);
        sidebarLeft.showInfo();
        IntPoint coord = game.getUniverseMap().getRenderer().getRightSideBar().getStarSystemCoord();
        sidebarRight.setStarSystemInfo(game.getUniverseMap().getStarSystemAt(coord));
        sidebarRight.getShipRenderer().selectShip(game.getUniverseMap().getCurrentShip());
        sidebarRight.refreshShips();
        map = game.getUniverseMap().getShipMap();
        shipRenderer.hideShips(false);
        fleetShip = map.getAt(map.fleetShip());
        shipRenderer.drawShips(null, fleetShip, mainStage, skin);

        toFleetTable.clear();
        float headerHeight = GameConstants.hToRelative(65);
        Label headerLabel = new Label(StringDB.getString("WHAT_SHIPS_TO_FLEET"), skin, "normalFont", markPenColor);
        headerLabel.setWrap(true);
        headerLabel.setAlignment(Align.center);
        toFleetTable.add(headerLabel).height((Math.round(headerHeight))).width(toFleetTable.getWidth());
        toFleetTable.row();
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int idx = (Integer) event.getListenerActor().getUserObject();

                for (int i = 0; i < map.getSize();) {
                    Ships ship = map.getAt(i);
                    if (!ship.getOwnerId().equals(fleetShip.getOwnerId())
                            || !ship.getCoordinates().equals(fleetShip.getCoordinates()) || ship.isStation()) {
                        ++i;
                        continue;
                    }
                    if (((idx == 1 && fleetShip.getShipClass().equals(ship.getShipClass())
                            || (idx == 2 && fleetShip.getShipType() == ship.getShipType())) || idx == 3)
                            && (fleetShip != ship && !ship.hasFleet())) {
                        fleetShip.addShipToFleet(ship);
                        map.eraseAt(i);
                        continue;
                    }
                    ++i;
                }
                game.getUniverseMap().getRenderer().getRightSideBar().getShipRenderer().selectShip(fleetShip);
                map.setFleetShip(map.currentShip());
                show();
            }
        };
        String text = String.format("%s-%s", fleetShip.getShipClass(), StringDB.getString("CLASS"));
        float labelHeight = (toFleetTable.getHeight() - headerHeight) / 3;
        Label filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(1);
        filterLabel.addListener(gestureListener);
        toFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(toFleetTable.getWidth());
        toFleetTable.row();
        text = String.format("%s %s", StringDB.getString("TYPE"), fleetShip.getShipTypeAsString());
        filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(2);
        filterLabel.addListener(gestureListener);
        toFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(toFleetTable.getWidth());
        toFleetTable.row();
        text = StringDB.getString("ALL_SHIPS");
        filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(3);
        filterLabel.addListener(gestureListener);
        toFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(toFleetTable.getWidth());

        fromFleetTable.clear();
        headerHeight = GameConstants.hToRelative(79);
        headerLabel = new Label(StringDB.getString("WHAT_SHIPS_FROM_FLEET"), skin, "normalFont", markPenColor);
        headerLabel.setWrap(true);
        headerLabel.setAlignment(Align.center);
        fromFleetTable.add(headerLabel).height((Math.round(headerHeight))).width(fromFleetTable.getWidth());
        fromFleetTable.row();
        gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int idx = (Integer) event.getListenerActor().getUserObject();
                ShipMap fleet = fleetShip.getFleet();

                for (int i = 0; i < fleet.getSize();) {
                    Ships ship = fleet.getAt(i);
                    if (((idx == 1 && !fleetShip.getShipClass().equals(ship.getShipClass())
                            || (idx == 2 && fleetShip.getShipType() != ship.getShipType())) || idx == 3)) {
                        map.add(ship);
                        fleet.eraseAt(i);
                        continue;
                    }
                    ++i;
                }
                game.getUniverseMap().getRenderer().getRightSideBar().getShipRenderer().selectShip(fleetShip);
                map.setFleetShip(map.currentShip());
                show();
            }
        };
        text = String.format("%s %s-%s", StringDB.getString("NOT"), fleetShip.getShipClass(),
                StringDB.getString("CLASS"));
        filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(1);
        filterLabel.addListener(gestureListener);
        fromFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(fromFleetTable.getWidth());
        fromFleetTable.row();
        text = String.format("%s %s %s", StringDB.getString("NOT"), StringDB.getString("TYPE"),
                fleetShip.getShipTypeAsString());
        filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(2);
        filterLabel.addListener(gestureListener);
        fromFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(fromFleetTable.getWidth());
        fromFleetTable.row();
        text = StringDB.getString("ALL_SHIPS");
        filterLabel = new Label(text, skin, "normalFont", normalColor);
        filterLabel.setWrap(true);
        filterLabel.setAlignment(Align.center);
        filterLabel.setUserObject(3);
        filterLabel.addListener(gestureListener);
        fromFleetTable.add(filterLabel).height((Math.round(labelHeight))).width(fromFleetTable.getWidth());
        sidebarRight.hideOverlayButtons();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        sidebarRight.getStage().act(1 / 30.0f);
        sidebarRight.getStage().draw();
    }

    @Override
    public void hide() {
        super.hide();
        Rectangle rect = GameConstants.coordsToRelative(1150, 810, 50, 50);
        rect.x += sidebarLeft.getPosition().width;
        inputs.setLockBounds(rect);
        sidebarRight.showOverlayButtonsIfNeeded();
    }
}
