/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.researchview.EmptyUniqueView;
import com.blotunga.bote.ui.researchview.ResearchBottomView;
import com.blotunga.bote.ui.researchview.ResearchButtonType;
import com.blotunga.bote.ui.researchview.ResearchOverview;
import com.blotunga.bote.ui.researchview.ResearchSpecialView;

public class ResearchScreen extends ZoomableScreen {
    private ResearchButtonType selectedButton;
    private TextButton[] buttons;
    private int numButtons = ResearchButtonType.values().length;
    private Major playerRace;
    private ResearchOverview roverView;
    private ResearchBottomView rBottomView;
    private EmptyUniqueView emptyUniqueView;
    private ResearchSpecialView specialResearchView;

    public ResearchScreen(final ScreenManager screenManager) {
        super(screenManager, "researchmenu", "researchV3");
        playerRace = screenManager.getRaceController().getPlayerRace();
        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = backgroundBottom.getHeight();

        roverView = new ResearchOverview(screenManager, mainStage, skin, xOffset, yOffset);
        rBottomView = new ResearchBottomView(screenManager, mainStage, skin, xOffset);
        emptyUniqueView = new EmptyUniqueView(screenManager, mainStage, skin, xOffset, yOffset);
        specialResearchView = new ResearchSpecialView(screenManager, mainStage, skin, xOffset, yOffset);

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);

        selectedButton = ResearchButtonType.NORMAL_BUTTON;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(ResearchButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(964, 249 - i * 40, 180, 35);
            if(ResearchButtonType.values()[i] == ResearchButtonType.DATABASE_BUTTON)
                rect = GameConstants.coordsToRelative(964, 360, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            buttons[i].setUserObject(ResearchButtonType.values()[i]);

            if (!needEnabled(ResearchButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        ResearchButtonType selected = (ResearchButtonType) event.getListenerActor().getUserObject();
                        if (selected == ResearchButtonType.NORMAL_BUTTON || selected == ResearchButtonType.SPECIAL_BUTTON)
                            setSubMenu(selected.getId());
                        else if (selected == ResearchButtonType.SHIPDESIGN_BUTTON)
                            screenManager.setView(ViewTypes.SHIPDESIGN_VIEW);
                        else if (selected == ResearchButtonType.DATABASE_BUTTON)
                            screenManager.setView(ViewTypes.ENCYCLOPEDIA_SCREEN, true);
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    @Override
    public void show() {
        super.show();
        if (selectedButton == ResearchButtonType.NORMAL_BUTTON) {
            setBackground(selectedButton.getBgImage());
            roverView.show();
        } else if (selectedButton == ResearchButtonType.SPECIAL_BUTTON) {
            roverView.drawResearchInfo();
            if (!playerRace.getEmpire().getResearch().isUniqueReady()) {
                setBackground(selectedButton.getBgImage());
                specialResearchView.show();
            } else {
                setBackground("emptyur");
                emptyUniqueView.show();
            }
        }
    }

    @Override
    public void hide() {
        //needed to trigger a mouse exit event from te button
        for (int i = 0; i < numButtons; i++) {
            if (buttons[i].isOver()) {
                InputEvent event = new InputEvent();
                event.setType(Type.exit);
                event.setPointer(-1);
                buttons[i].fire(event);
            }
        }

        super.hide();
        roverView.hide();
        rBottomView.hide();
        emptyUniqueView.hide();
        specialResearchView.hide();
    }

    private boolean needEnabled(ResearchButtonType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    private void setToSelection(ResearchButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++) {
            if (!needEnabled(ResearchButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(ResearchButtonType.fromInt(id));
    }

    public void showBottomView(ResearchType type) {
        rBottomView.show(type);
    }
}
