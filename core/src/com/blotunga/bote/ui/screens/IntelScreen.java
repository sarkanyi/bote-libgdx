/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.intelview.IntelAssignmentView;
import com.blotunga.bote.ui.intelview.IntelAttackView;
import com.blotunga.bote.ui.intelview.IntelBottomView;
import com.blotunga.bote.ui.intelview.IntelInformation;
import com.blotunga.bote.ui.intelview.IntelInfos;
import com.blotunga.bote.ui.intelview.IntelRaceLogos;
import com.blotunga.bote.ui.intelview.IntelReportView;
import com.blotunga.bote.ui.intelview.IntelSpySabotageView;
import com.blotunga.bote.ui.intelview.IntelViewMainButtonType;

public class IntelScreen extends ZoomableScreen {
    private IntelViewMainButtonType selectedButton;
    private TextButton[] buttons;
    private int numButtons = IntelViewMainButtonType.values().length;
    private IntelBottomView intelBottomView;
    private IntelRaceLogos raceLogos;
    private IntelInfos intelInfos;
    private IntelAssignmentView intelAssignmentView;
    private IntelSpySabotageView intelSpySabotageView;
    private IntelInformation intelInformation;
    private IntelReportView intelReportView;
    private IntelAttackView intelAttackView;

    public IntelScreen(ScreenManager manager) {
        super(manager, "intelassignmenu", "diplomacyV3");
        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = backgroundBottom.getHeight();

        intelAssignmentView = new IntelAssignmentView(manager, mainStage, skin, xOffset, yOffset);
        intelBottomView = new IntelBottomView(manager, mainStage, skin, xOffset);
        raceLogos = new IntelRaceLogos(manager, mainStage, skin, xOffset, yOffset);
        intelInfos = new IntelInfos(manager, mainStage, skin, xOffset, yOffset);
        intelSpySabotageView = new IntelSpySabotageView(manager, mainStage, skin, xOffset, yOffset);
        intelInformation = new IntelInformation(manager, mainStage, skin, xOffset, yOffset);
        intelReportView = new IntelReportView(manager, mainStage, skin, xOffset, yOffset);
        intelAttackView = new IntelAttackView(manager, mainStage, skin, xOffset, yOffset);

        buttons = new TextButton[numButtons];
        TextButton.TextButtonStyle style = skin.get("default", TextButton.TextButtonStyle.class);
        selectedButton = IntelViewMainButtonType.BTN_ASSIGNMENT;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(IntelViewMainButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(25 + i * 190, 50, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            buttons[i].setUserObject(IntelViewMainButtonType.values()[i]);

            if (!needEnabled(IntelViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        IntelViewMainButtonType selected = (IntelViewMainButtonType) event.getListenerActor()
                                .getUserObject();
                        setSubMenu(selected.getId());
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    @Override
    public void show() {
        super.show();
        String selectedRace = "";
        setBackground(selectedButton.getBgImage());
        if (selectedButton == IntelViewMainButtonType.BTN_ASSIGNMENT) {
            raceLogos.show();
            intelInfos.show();
            intelAssignmentView.show();
        } else if (selectedButton == IntelViewMainButtonType.BTN_SPY) {
            raceLogos.show();
            selectedRace = intelSpySabotageView.show(0, raceLogos.getSelectedRace());
            raceLogos.setSelectedRace(selectedRace);
            intelInfos.show(raceLogos.getSelectedRace());
        } else if (selectedButton == IntelViewMainButtonType.BTN_SABOTAGE) {
            raceLogos.show();
            selectedRace = intelSpySabotageView.show(1, raceLogos.getSelectedRace());
            raceLogos.setSelectedRace(selectedRace);
            intelInfos.show(raceLogos.getSelectedRace());
        } else if (selectedButton == IntelViewMainButtonType.INFORMATION) {
            raceLogos.show(true);
            intelInformation.show(raceLogos.getSelectedRace());
        } else if (selectedButton == IntelViewMainButtonType.BTN_REPORTS) {
            intelReportView.setScrollerHeight(435);
            intelReportView.show(true);
            intelBottomView.show();
        } else if (selectedButton == IntelViewMainButtonType.BTN_ATTEMPT) {
            intelReportView.setScrollerHeight(230);
            intelReportView.show(false);
            intelAttackView.show();
            intelBottomView.show();
        }
    }

    public void updateBottomInfo() {
        intelBottomView.show();
    }

    @Override
    public void hide() {
        super.hide();
        raceLogos.hide();
        intelBottomView.hide();
        intelAssignmentView.hide();
        intelInfos.hide();
        intelSpySabotageView.hide();
        intelInformation.hide();
        intelReportView.hide();
        intelAttackView.hide();
    }

    private boolean needEnabled(IntelViewMainButtonType type) {
        return type != selectedButton;
    }

    private void setToSelection(IntelViewMainButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(IntelViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(IntelViewMainButtonType.fromInt(id));
    }
}