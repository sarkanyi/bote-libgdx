/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.systemview.SystemBuildList;
import com.blotunga.bote.ui.systemview.SystemBuildingsView;
import com.blotunga.bote.ui.systemview.SystemEnergyView;
import com.blotunga.bote.ui.systemview.SystemManagerView;
import com.blotunga.bote.ui.systemview.SystemProduction;
import com.blotunga.bote.ui.systemview.SystemTradeView;
import com.blotunga.bote.ui.systemview.SystemViewButtonType;
import com.blotunga.bote.utils.IntPoint;

public class SystemBuildScreen extends ZoomableScreen {
    private SystemViewButtonType selectedButton;
    private SystemBuildList buildList;
    private SystemProduction productionView;
    private SystemEnergyView energyView;
    private SystemBuildingsView buildingsView;
    private SystemTradeView tradeView;
    private SystemManagerView managerView;
    private TextButton[] buttons;
    private int numButtons = SystemViewButtonType.values().length;
    private IntPoint lastSystem;

    public SystemBuildScreen(final ScreenManager screenManager) {
        super(screenManager, "buildmenu", "");
        lastSystem = new IntPoint();

        buildList = new SystemBuildList(screenManager, mainStage, skin, sidebarLeft.getPosition());
        productionView = new SystemProduction(screenManager, mainStage, skin, sidebarLeft.getPosition(), buildList);
        energyView = new SystemEnergyView(screenManager, mainStage, skin, sidebarLeft.getPosition());
        buildingsView = new SystemBuildingsView(screenManager, mainStage, skin, sidebarLeft.getPosition());
        tradeView = new SystemTradeView(screenManager, mainStage, skin, sidebarLeft.getPosition());
        managerView = new SystemManagerView(screenManager, mainStage, skin, sidebarLeft.getPosition());

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        float xOffset = sidebarLeft.getPosition().getWidth();
        selectedButton = SystemViewButtonType.BUILD_LIST_BUTTON;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(SystemViewButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(20 + i * 190, 60, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(SystemViewButtonType.values()[i]);

            if (!needEnabled(SystemViewButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        SystemViewButtonType selected = (SystemViewButtonType) event.getListenerActor().getUserObject();
                        setSubMenu(selected.getId());
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    private void setToSelection(SystemViewButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(SystemViewButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(SystemViewButtonType.fromInt(id));
    }

    private boolean needEnabled(SystemViewButtonType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    @Override
    public void show() {
        super.show();
        ScreenManager screenManager = (ScreenManager) game;
        IntPoint coord = screenManager.getUniverseMap().getSelectedCoordValue();
        if (!coord.equals(new IntPoint())) {
            StarSystem system = screenManager.getUniverseMap().getStarSystemAt(coord);
            Major major = screenManager.getRaceController().getPlayerRace();
            if (!system.getOwnerId().equals(major.getRaceId()) || !system.isSunSystem() || !system.isMajorized()) {
                if (!lastSystem.equals(new IntPoint()) && !lastSystem.equals(coord)) // System might have broken free or otherwise removed from the empire
                    screenManager.getUniverseMap().setSelectedCoordValue(lastSystem);
                else
                    screenManager.getUniverseMap().setSelectedCoordValue(major.getEmpire().getSystemList().get(0));
            }
            coord = screenManager.getUniverseMap().getSelectedCoordValue();
            system = screenManager.getUniverseMap().getStarSystemAt(coord);

            if (system.getOwnerId().equals(major.getRaceId()) && system.isSunSystem() && system.isMajorized()) {
                lastSystem = new IntPoint(system.getCoordinates());
            }
            sidebarLeft.setStarSystemInfo(system);
            setBackground(selectedButton.getBgImage());
            buildList.setStarSystem(system);
            productionView.setStarSystem(system);
            energyView.setStarSystem(system);
            buildingsView.setStarSystem(system);
            tradeView.setStarSystem(system);
            managerView.setStarSystem(system);
            if (selectedButton.equals(SystemViewButtonType.BUILD_LIST_BUTTON)) {
                buildList.show();
            } else if (selectedButton.equals(SystemViewButtonType.PRODUCTION_BUTTON)) {
                productionView.update();
                productionView.show();
            } else if (selectedButton.equals(SystemViewButtonType.ENERGY_BUTTON)) {
                energyView.show();
            } else if (selectedButton.equals(SystemViewButtonType.BUILDING_OVERVIEW_BUTTON)) {
                buildingsView.show();
            } else if (selectedButton.equals(SystemViewButtonType.TRADE_BUTTON)) {
                tradeView.show();
            } else if (selectedButton.equals(SystemViewButtonType.SYSTEM_MANAGER_BUTTON)) {
                managerView.show();
            }
        }
    }

    @Override
    public void hide() {
        super.hide();
        buildList.hide();
        productionView.hide();
        energyView.hide();
        buildingsView.hide();
        tradeView.hide();
        managerView.hide();
    }
}
