/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.ui.shipdesignview.ShipDesignBottomView;
import com.blotunga.bote.ui.shipdesignview.ShipDesignView;

public class ShipDesignScreen extends ZoomableScreen {
    private ShipDesignBottomView shipDesignBottomView;
    private ShipDesignView shipDesignView;

    public ShipDesignScreen(final ScreenManager screenManager) {
        super(screenManager, "designmenu", "researchV3");
        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = backgroundBottom.getHeight();

        shipDesignBottomView = new ShipDesignBottomView(screenManager, mainStage, skin, xOffset);
        shipDesignView = new ShipDesignView(screenManager, mainStage, skin, xOffset, yOffset);
    }

    @Override
    public void show() {
        super.show();
        shipDesignBottomView.show(shipDesignView.show());
    }

    @Override
    public void hide() {
        super.hide();
        shipDesignView.hide();
        shipDesignBottomView.hide();
    }

    @Override
    public void setSubMenu(int id) {
    }
}
