/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ui.diplomacyview.DiplomacyBottomView;
import com.blotunga.bote.ui.diplomacyview.DiplomacyInView;
import com.blotunga.bote.ui.diplomacyview.DiplomacyInfoView;
import com.blotunga.bote.ui.diplomacyview.DiplomacyOutView;
import com.blotunga.bote.ui.diplomacyview.DiplomacyRaceList;
import com.blotunga.bote.ui.diplomacyview.DiplomacyViewMainButtonType;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;

public class DiplomacyScreen extends ZoomableScreen {
    private DiplomacyViewMainButtonType selectedButton;
    private TextButton[] buttons;
    private int numButtons = DiplomacyViewMainButtonType.values().length;
    private DiplomacyInfoView diploInfoView;
    private DiplomacyRaceList diploRaceList;
    private DiplomacyOutView diploOutView;
    private DiplomacyInView diploInView;
    private DiplomacyBottomView diplomBottomView;

    public DiplomacyScreen(final ScreenManager manager) {
        super(manager, "diploinfomenu", "diplomacyV3");
        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = backgroundBottom.getHeight();

        diploInfoView = new DiplomacyInfoView(manager, mainStage, xOffset, yOffset);
        diploRaceList = new DiplomacyRaceList(manager, mainStage, xOffset, yOffset);
        diploOutView = new DiplomacyOutView(manager, mainStage, xOffset, yOffset);
        diploInView = new DiplomacyInView(manager, mainStage, xOffset, yOffset);
        diplomBottomView = new DiplomacyBottomView(manager, mainStage, xOffset);

        buttons = new TextButton[numButtons];
        TextButton.TextButtonStyle style = skin.get("default", TextButton.TextButtonStyle.class);
        selectedButton = DiplomacyViewMainButtonType.INFO_BUTTON;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(DiplomacyViewMainButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(25 + i * 220, 50, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            buttons[i].setUserObject(DiplomacyViewMainButtonType.values()[i]);

            if (!needEnabled(DiplomacyViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        DiplomacyViewMainButtonType selected = (DiplomacyViewMainButtonType) event.getListenerActor()
                                .getUserObject();
                        setSubMenu(selected.getId());
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    @Override
    public void show() {
        super.show();
        setBackground(selectedButton.getBgImage());
        diplomBottomView.show();
        if (selectedButton == DiplomacyViewMainButtonType.INFO_BUTTON) {
            diploRaceList.showRaceList(null);
            diploInfoView.show();
        } else if (selectedButton == DiplomacyViewMainButtonType.OFFER_BUTTON) {
            diploOutView.resetOutGoingInfo();
            diploRaceList.showRaceList(null);
            diploOutView.show();
        } else if (selectedButton == DiplomacyViewMainButtonType.RECEIVED_BUTTON) {
            Major playerRace = game.getRaceController().getPlayerRace();
            diploRaceList.showRaceList(playerRace.getIncomingDiplomacyNews());
            diploInView.show();
        }
    }

    public void updateRaceListChange() {
        diplomBottomView.show();
        if (selectedButton == DiplomacyViewMainButtonType.INFO_BUTTON) {
            diploInfoView.show();
        } else if (selectedButton == DiplomacyViewMainButtonType.OFFER_BUTTON) {
            diploOutView.resetOutGoingInfo();
            diploOutView.show();
        } else if (selectedButton == DiplomacyViewMainButtonType.RECEIVED_BUTTON)
            diploInView.show();
    }

    @Override
    public void hide() {
        super.hide();
        diploInfoView.hide();
        diploRaceList.hide();
        diploOutView.hide();
        diploInView.hide();
        diplomBottomView.hide();
    }

    private boolean needEnabled(DiplomacyViewMainButtonType type) {
        return type != selectedButton;
    }

    private void setToSelection(DiplomacyViewMainButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(DiplomacyViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(DiplomacyViewMainButtonType.fromInt(id));
    }

    public Race getSelectedRace() {
        return diploRaceList.getSelectedRace();
    }

    public DiplomacyBottomView getBottomView() {
        return diplomBottomView;
    }

    public DiplomacyRaceList getRaceList() {
        return diploRaceList;
    }

    public DiplomacyInView getDiplomacyInView() {
        return diploInView;
    }

    public Pair<String, Color> printDiplomacyStatus(Major ourRace, Race theirRace) {
        Color normalColor = ourRace.getRaceDesign().clrNormalText;
        String raceId = theirRace.getRaceId();
        String text = "";
        Color color = new Color(normalColor);
        if (!ourRace.isMajor())
            return new Pair<String, Color>(text, color);
        Major majorRace = ourRace;
        if (theirRace.isMinor() && ((Minor) theirRace).isSubjugated()) {
            text = StringDB.getString("SUBJUGATED", false, theirRace.getOwner().getName());
            color = new Color(178 / 255.0f, 0, 1.0f, 1.0f);
            return new Pair<String, Color>(text, color);
        }

        switch (majorRace.getAgreement(raceId)) {
            case NONE:
                text = StringDB.getString("NONE");
                break;
            case NAP:
                if (theirRace.isMajor() && majorRace.getAgreementDuration(raceId) != 0)
                    text = String.format("%s (%d)", StringDB.getString("NON_AGGRESSION_SHORT"),
                            majorRace.getAgreementDuration(raceId));
                else
                    text = StringDB.getString("NON_AGGRESSION_SHORT");
                color = new Color(139 / 255.0f, 175 / 255.0f, 172 / 255.0f, 1.0f);
                break;
            case TRADE:
                if (theirRace.isMajor() && majorRace.getAgreementDuration(raceId) != 0)
                    text = String.format("%s (%d)", StringDB.getString("TRADE_AGREEMENT_SHORT"),
                            majorRace.getAgreementDuration(raceId));
                else
                    text = StringDB.getString("TRADE_AGREEMENT_SHORT");
                color = new Color(233 / 255.0f, 183 / 255.0f, 12 / 255.0f, 1.0f);
                break;
            case FRIENDSHIP:
                if (theirRace.isMajor() && majorRace.getAgreementDuration(raceId) != 0)
                    text = String.format("%s (%d)", StringDB.getString("FRIENDSHIP_SHORT"),
                            majorRace.getAgreementDuration(raceId));
                else
                    text = StringDB.getString("FRIENDSHIP_SHORT");
                color = new Color(6 / 255.0f, 187 / 255.0f, 34 / 255.0f, 1.0f);
                break;
            case COOPERATION:
                if (theirRace.isMajor() && majorRace.getAgreementDuration(raceId) != 0)
                    text = String.format("%s (%d)", StringDB.getString("COOPERATION"),
                            majorRace.getAgreementDuration(raceId));
                else
                    text = StringDB.getString("COOPERATION");
                color = new Color(37 / 255.0f, 159 / 255.0f, 250 / 255.0f, 1.0f);
                break;
            case ALLIANCE:
                if (theirRace.isMajor() && majorRace.getAgreementDuration(raceId) != 0)
                    text = String.format("%s (%d)", StringDB.getString("ALLIANCE"),
                            majorRace.getAgreementDuration(raceId));
                else
                    text = StringDB.getString("ALLIANCE");
                color = new Color(29 / 255.0f, 29 / 255.0f, 248 / 255.0f, 1.0f);
                break;
            case MEMBERSHIP:
                text = StringDB.getString("MEMBERSHIP");
                color = new Color(115 / 255.0f, 12 / 255.0f, 228 / 255.0f, 1.0f);
                break;
            case WAR:
                text = StringDB.getString("WAR");
                color = new Color(220 / 255.0f, 15 / 255.0f, 15 / 255.0f, 1.0f);
                break;
            default:
                break;
        }

        return new Pair<String, Color>(text, color);
    }

    public void takeorGetBackResLat(DiplomacyInfo info, boolean take) {
        Major playerRace = game.getRaceController().getPlayerRace();
        int mul = take ? -1 : 1;
        if (info.type == DiplomaticAgreement.REQUEST && info.fromRace.equals(playerRace.getRaceId()))
            return;
        playerRace.getEmpire().setCredits(mul * info.credits);
        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
            if (info.resources[res] > 0) {
                IntPoint coord = info.coord;
                if (!coord.equals(new IntPoint())) {
                    game.getUniverseMap().getStarSystemAt(coord).addResourceStore(res, mul * info.resources[res]);
                }
            }
        }
        getLeftSideBar().showInfo();
    }
}