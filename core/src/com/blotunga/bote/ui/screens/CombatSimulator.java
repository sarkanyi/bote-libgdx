/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.editors.ResourceEditor;
import com.blotunga.bote.editors.ShipEditor;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.CombatShip;
import com.blotunga.bote.ships.CombatShipEffectSound.CombatShipEffectHandler;
import com.blotunga.bote.ships.CombatShipEffectSound.EffectType;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.ships.Torpedo;

public class CombatSimulator extends DefaultScreen implements CombatShipEffectHandler {
    enum ViewState {
        StateXY("XY"),
        StateXZ("XZ");

        String stateStr;

        private ViewState(String stateStr) {
            this.stateStr = stateStr;
        }

        @Override
        public String toString() {
            return stateStr;
        }
    };

    private boolean initialized;
    private Combat combat;
    private Combat combatBackup;
    private ObjectIntMap<String> winner;
    private SpriteBatch batch;
    private InputMultiplexer inputs;
    private ViewState state;
    private Table infoTable;
    private Label axisLabel;
    private int speed;
    private Array<String> loadedTextures;
    private Stage stage;
    private int numberOfRuns = 1;
    private ObjectIntMap<String> statistics;
    private boolean finishQuickly = false;
    private boolean calcThreadStarted = false;

    public CombatSimulator(ScreenManager game) {
        super(game);
        initialized = false;
    }

    public void init(IntMap<IntArray> participants, Anomaly anomaly) {
        Array<Ships> involvedShips = new Array<Ships>();
        for (Iterator<Entry<IntArray>> it = participants.entries().iterator(); it.hasNext();) {
            Entry<IntArray> e = it.next();
            for (int i = 0; i < e.value.size; i++) {
                int id = e.value.get(i);
                buildShip(involvedShips, e.key, id);
            }
        }
        combat = new Combat(game.getRaceController());
        combat.setInvolvedShips(involvedShips, anomaly);

        if (!combat.isReadyForCombat())
            return;

        combatBackup = new Combat(combat);
        combat.preCombatCalculation();
        initGui();
        initialized = true;
    }

    public void init(Combat combat) {
        this.combat = combat;
        initGui();
        initialized = true;
    }

    private void initGui() {
        statistics = new ObjectIntMap<String>();
        stage = new Stage();
        batch = new SpriteBatch();
        inputs = new InputMultiplexer();
        game.getSoundManager().initEffectSounds();
        state = ViewState.StateXY;
        speed = 50;
        GestureAdapter ad = new GestureAdapter();
        GestureDetector det = new GestureDetector(ad) {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Keys.PLUS)
                    speed += 10;
                if (keycode == Keys.MINUS)
                    speed -= 10;
                if (speed < 0)
                    speed = 0;
                if (speed > 100)
                    speed = 100;
                axisLabel.setText(state.toString() + " Speed: " + speed);
                return false;
            }
        };
        inputs.addProcessor(det);
        inputs.addProcessor(stage);
        setInputProcessor(inputs);
        axisLabel = new Label(state.toString() + " Speed: " + speed, game.getSkin(), "mediumFont", Color.WHITE);
        axisLabel.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                state = (state == ViewState.StateXY) ? ViewState.StateXZ : ViewState.StateXY;
                axisLabel.setText(state.toString() + " Speed: " + speed);
                return true;
            }
        });
        axisLabel.setAlignment(Align.center);
        infoTable = new Table();
        infoTable.setPosition(GameConstants.wToRelative(0), GameConstants.hToRelative(780));
        infoTable.setSize(GameConstants.wToRelative(1440), GameConstants.hToRelative(20));
        loadedTextures = new Array<String>();
        String path = "graphics/ui/ome_ui.pack";
        if (!game.getAssetManager().isLoaded(path)) {
            game.getAssetManager().load(path, TextureAtlas.class);
            game.getAssetManager().finishLoading();
            loadedTextures.add(path); //when in-game don't unload it, just for the editor
        }
        TextureAtlas raceUiAtlas = game.getAssetManager().get("graphics/ui/ome_ui.pack");
        ButtonStyle bsMinus = new ButtonStyle();
        bsMinus.up = new TextureRegionDrawable(raceUiAtlas.findRegion("ome_buttonminus"));
        bsMinus.down = new TextureRegionDrawable(raceUiAtlas.findRegion("ome_buttonminusa"));
        ButtonStyle bsPlus = new ButtonStyle();
        bsPlus.up = new TextureRegionDrawable(raceUiAtlas.findRegion("ome_buttonplus"));
        bsPlus.down = new TextureRegionDrawable(raceUiAtlas.findRegion("ome_buttonplusa"));
        Button button = new Button(bsMinus);
        button.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                speed -= 10;
                if (speed < 0)
                    speed = 0;
                axisLabel.setText(state.toString() + " Speed: " + speed);
                return true;
            }
        });
        infoTable.add(button).height(GameConstants.wToRelative(50)).width(GameConstants.wToRelative(50))
                .spaceRight(GameConstants.wToRelative(50));
        infoTable.add(axisLabel);
        button = new Button(bsPlus);
        button.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                speed += 10;
                if (speed > 100)
                    speed = 100;
                axisLabel.setText(state.toString() + " Speed: " + speed);
                return true;
            }
        });
        infoTable.add(button).height(GameConstants.wToRelative(50)).width(GameConstants.wToRelative(50))
                .spaceLeft(GameConstants.wToRelative(50));
        stage.addActor(infoTable);
        TextButtonStyle styleSmall = game.getSkin().get("small-buttons", TextButtonStyle.class);
        TextButton btn = new TextButton(StringDB.getString("LEAVE"), styleSmall);
        Rectangle rect = GameConstants.coordsToRelative(10, 800, 160, 40);
        btn.setBounds(rect.x, rect.y, rect.width, rect.height);
        btn.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getListenerActor().setVisible(false);
                finishQuickly = true;
                return true;
            }
        });
        stage.addActor(btn);
        loadShipSprites();
    }

    private void loadShipSprites() {
        for (int i = 0; i < combat.getInvolvedShips().size; i++) {
            CombatShip cs = combat.getInvolvedShips().get(i);
            cs.loadSprite(game);
            cs.setEffectsCB(this);
        }
    }

    @Override
    public void show() {
        Gdx.graphics.setContinuousRendering(true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (!initialized)
            return;

        batch.begin();
        if (!finishQuickly) {
            for (int i = 0; combat != null && combat.isReadyForCombat() && i < combat.getInvolvedShips().size; i++) {
                CombatShip cs = combat.getInvolvedShips().get(i);
                if (cs.isFiring())
                    cs.startShootingSound();
                Vector3 coord = cs.getCoord().cpy();
                coord.x += Gdx.graphics.getWidth() / 2;
                coord.y += Gdx.graphics.getHeight() / 2;
                coord.z += Gdx.graphics.getHeight() / 2;

                cs.getSprite().setPosition(coord.x, state == ViewState.StateXY ? coord.y : coord.z);
                int hullP = (100 * cs.getShip().getHull().getCurrentHull()) / cs.getShip().getHull().getMaxHull();
                int shieldP = 0;
                if (cs.getShip().getShield().getMaxShield() != 0)
                    shieldP = (100 * cs.getShip().getShield().getCurrentShield()) / cs.getShip().getShield().getMaxShield();
                ((Label) cs.getSprite().findActor("HITPOINTS")).setText(hullP + "%/" + shieldP + "%");
                if (cs.isCloaked())
                    ((Image) cs.getSprite().findActor("GFX")).setColor(new Color(0.8f, 0.8f, 0.8f, 0.8f));
                else
                    ((Image) cs.getSprite().findActor("GFX")).setColor(Color.WHITE);
                cs.getSprite().draw(batch, 1.0f);
                if (cs.getTarget() != null && cs.isFiring()) {
                    Vector3 targetCoord = cs.getTarget().getCoord();
                    Image img = cs.getSprite().findActor("GFX");
                    targetCoord.x += Gdx.graphics.getWidth() / 2
                            + img.localToParentCoordinates(new Vector2(img.getOriginX(), img.getOriginY())).x / 2;
                    targetCoord.y += Gdx.graphics.getHeight() / 2
                            + img.localToParentCoordinates(new Vector2(img.getOriginX(), img.getOriginY())).y / 2;
                    targetCoord.z += Gdx.graphics.getHeight() / 2
                            + img.localToParentCoordinates(new Vector2(img.getOriginX(), img.getOriginY())).y / 2;
                    coord.x += cs.getSprite().getWidth() / 2;
                    coord.y += cs.getSprite().getHeight() / 2;
                    coord.z += cs.getSprite().getHeight() / 2;
                    Color color = Color.WHITE;
                    Race owner = cs.getShip().getOwner();
                    if (owner.isMajor())
                        color = ((Major) owner).getRaceDesign().clrGalaxySectorText;

                    if (!cs.isPulsing())
                        img = drawLine(coord.x, targetCoord.x, state == ViewState.StateXY ? coord.y : coord.z,
                                state == ViewState.StateXY ? targetCoord.y : targetCoord.z, 2, color);
                    else
                        img = drawDashedLine(coord.x, targetCoord.x, state == ViewState.StateXY ? coord.y : coord.z,
                                state == ViewState.StateXY ? targetCoord.y : targetCoord.z, 2, color, 3, 4);
                    img.draw(batch, 1.0f);
                }
            }

            for (int i = 0; combat != null && combat.isReadyForCombat() && i < combat.getTorpedoes().size; i++) {
                Torpedo torp = combat.getTorpedoes().get(i);
                Sprite sp = new Sprite(new TextureRegion(game.getUiTexture("shippath")));
                Vector3 coord = torp.getCoord().cpy();
                coord.x += Gdx.graphics.getWidth() / 2 - GameConstants.wToRelative(20); //correct for shift
                coord.y += Gdx.graphics.getHeight() / 2 - GameConstants.wToRelative(20);
                coord.z += Gdx.graphics.getHeight() / 2 - GameConstants.wToRelative(20);

                sp.setPosition(coord.x, state == ViewState.StateXY ? coord.y : coord.z);
                sp.setScale(0.5f);
                sp.setColor(torp.getColor());
                sp.draw(batch);
            }
        }
        batch.end();

        stage.draw();
        if (combat != null && combat.isReadyForCombat() && speed > 0 && !finishQuickly) {
            combat.calculateCombatSingleTick();
        } else if (finishQuickly) {
            if (!calcThreadStarted) {
                Thread th = new Thread() {
                    public void run() {
                        while (combat != null && combat.isReadyForCombat())
                            combat.calculateCombatSingleTick();
                    }
                };
                th.start();
                calcThreadStarted = true;
            }
        }

        try {
            Thread.sleep(100 - speed);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (combat != null && !combat.isReadyForCombat()) {
            combat.calculateWinner();
            winner = combat.getWinner();
            calcThreadStarted = false;
            String text = "";
            for (Iterator<ObjectIntMap.Entry<String>> it = winner.entries(); it.hasNext();) {
                ObjectIntMap.Entry<String> e = it.next();
                if (e.value == 1) {
                    statistics.put(e.key, statistics.get(e.key, 0) + 1);
                    text = "Winner: " + e.key;
                }
            }
            if (text.isEmpty())
                text = "Draw";
            Gdx.app.debug("CombatSimulator", text);
            axisLabel.setText(text);
            writeCombatLog();
            combat = null;
            numberOfRuns--;
            if (numberOfRuns == 0 || combatBackup == null) {
                Thread th = new Thread() {
                    public void run() {
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                if (((ScreenManager) game).getPreviousScreen().getClass() == ResourceEditor.class) {
                                    DefaultScreen screen = ((ScreenManager) game).setView(ViewTypes.RESOURCE_EDITOR, true);
                                    screen.setSubMenu(ResourceEditor.EditorState.ShipEditor.getMask() | ShipEditor.ShipEditorState.StateCombatSimulator.getMask());
                                } else if (((ScreenManager) game).getPreviousScreen().getClass() == CombatScreen.class)
                                    ((ScreenManager) game).setView(ViewTypes.GALAXY_VIEW, true);
                            }
                        });
                    }
                };
                th.start();
            } else {
                //restart combat
                combat = new Combat(combatBackup);
                combat.preCombatCalculation();
                loadShipSprites();
                axisLabel.setText(state.toString() + " Speed: " + speed);
            }
        }
    }

    @Override
    public void hide() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.graphics.setContinuousRendering(false);
    }

    private void writeCombatLog() {
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle simulator = Gdx.files.absolute(pathPrefix + "/combat.log");
        String text = "";
        ArrayMap<Ships, ObjectSet<Ships>> killedShips = combat.getKilledShipInfos();
        for (int i = 0; i < killedShips.size; i++) {
            Ships ship = killedShips.getKeyAt(i);
            text += ship.getShipClass() + " kills:" + "\n";
            for (ObjectSetIterator<Ships> iter = killedShips.getValueAt(i).iterator(); iter.hasNext();) {
                Ships killed = iter.next();
                text += killed.getShipClass() + "\n";
            }
        }
        text += "Wins by races: \n";
        for (Iterator<ObjectIntMap.Entry<String>> iter = statistics.iterator(); iter.hasNext(); ) {
            ObjectIntMap.Entry<String> e = iter.next();
            text += e.key + ": " + e.value;
        }
        simulator.writeString(text, false, "ISO-8859-1");
    }

    private void unloadSprites() {
        for (ShipInfo si : game.getShipInfos()) {
            String path = "graphics/ships/" + game.getShipImgName(si.getID() - 10000) + ".png";
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
        }
        for (String path : loadedTextures)
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
    }

    private void buildShip(Array<Ships> involvedShips, int race, int id) {
        Ships ship = new Ships(game.getShipInfos().get(id));
        if (race != 255)
            ship.setOwner(game.getRaceController().getMajors().getValueAt(race - 1).getRaceId());
        else {
            for (int i = 0; i < game.getRaceController().getMinors().size; i++) {
                Minor m = game.getRaceController().getMinors().getValueAt(i);
                if (m.getHomeSystemName().equals(game.getShipInfos().get(id).getOnlyInSystem())) {
                    ship.setOwner(m.getRaceId());
                    break;
                }
            }
        }
        game.getShipNameGenerator().Init(game);
        ship.setName(game.getShipNameGenerator()
                .generateShipName(ship.getOwnerId(), ship.getOwner().getName(), ship.isStation()));
        if (ship.getStealthPower() > 3)
            ship.setCloak(true);
        involvedShips.add(ship);
    }

    public void setSpeed(int speed) {
        this.speed = speed;
        axisLabel.setText(state.toString() + " Speed: " + speed);
    }

    @Override
    public void dispose() {
        unloadSprites();
        game.getSoundManager().clearEffectSounds();
    }

    @Override
    public long playEffect(EffectType type) {
        Sound snd = game.getSoundManager().getEffectSounds().get(type).getSound();
        return snd.play(game.getGameSettings().effectsVolume * 0.4f);
    }

    @Override
    public void stopEffect(EffectType type, long id) {
        Sound snd = game.getSoundManager().getEffectSounds().get(type).getSound();
        snd.stop(id);
    }

    public void setNumberOfRuns(int numberOfRuns) {
        this.numberOfRuns = numberOfRuns;
    }
}
