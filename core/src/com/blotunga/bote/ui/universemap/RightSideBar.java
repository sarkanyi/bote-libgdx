/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.universemap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.GameSettings.GalaxyShowState;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ScanPower;
import com.blotunga.bote.constants.TutorialType;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.galaxy.WormholeLinker;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ui.UISidebar;
import com.blotunga.bote.ui.screens.UniverseRenderer;
import com.blotunga.bote.ui.screens.UniverseRenderer.GalaxyState;
import com.blotunga.bote.utils.ui.BaseTooltip;
import com.blotunga.bote.utils.ui.HelpWidget;

public class RightSideBar extends UISidebar {
    private Skin skin;
    private final TextureAtlas starAtlas;
    private final TextureAtlas uiAtlas;
    private final Label sectorLabel;
    private final Label systemLabel;
    private final Label currentPopLabel;
    private final Image currentPopImg;
    private final Image maxPopImg;
    private final Label maxPopLabel;
    private final Array<Image> planetImg;
    private final Array<Label> planetLabel;
    private final Array<Table> planetBonusTable;
    private Array<Image> terraformSpheres;
    private Array<Label> terraformLabels;
    private final Image sunimg;
    private float sidebarWidth;
    private float sidebarHeight;
    final private float sunScale = 4;
    private float sunSize;
    private Major playerRace;
    private Table scanPowerText;
    private Table resourcesText;
    private Table ownerText;
    private boolean starSystemOrShips; //false if starsystem true if ships
    final private ShipRenderer shipRenderer;
    private float shipWidth;
    private float shipHeight;
    final private float shipPadding = GameConstants.wToRelative(22.5f);
    private Array<String> loadedTextures;
    private Table troopsTable;
    private int lastPlanetTouched;
    private boolean showPlanetInfo;
    private HelpWidget helpWidget;
    private Table overlayTable;

    //tooltip constants
    private Color headerColor;
    private Color textColor;
    private String headerFont = "xlFont";
    private String textFont = "largeFont";

    public RightSideBar(final ScreenManager screenManager, Screen parent, Camera camera) {
        super(new Rectangle(GameConstants.wToRelative(1440) - GameConstants.wToRelative(247), 0, GameConstants.wToRelative(247),
                GameConstants.hToRelative(810)), "", screenManager, false, camera);
        sidebarWidth = position.getWidth();
        sidebarHeight = position.getHeight();
        sunSize = sidebarHeight / sunScale;
        skin = screenManager.getSkin();
        playerRace = screenManager.getRaceController().getPlayerRace();
        setBackground(playerRace.getPrefix() + "galaxyV3");
        starAtlas = screenManager.getAssetManager().get("graphics/star/StarMaps.pack");
        uiAtlas = screenManager.getAssetManager().get("graphics/ui/general_ui.pack");
        headerColor = playerRace.getRaceDesign().clrListMarkTextColor;
        textColor = playerRace.getRaceDesign().clrNormalText;

        sectorLabel = new Label("", skin, "normalFont", Color.WHITE);
        sectorLabel.setWrap(true);
        sectorLabel.setAlignment(1);
        sectorLabel.setVisible(false);
        systemLabel = new Label("", skin, "normalFont", Color.WHITE);
        systemLabel.setWrap(true);
        systemLabel.setAlignment(1);
        systemLabel.setVisible(false);
        currentPopLabel = new Label("", skin, "normalFont", Color.WHITE);
        currentPopLabel.setWrap(true);
        currentPopLabel.setAlignment(1);
        currentPopLabel.setVisible(false);
        currentPopImg = new Image(uiAtlas.findRegion("populationSmall"));
        currentPopImg.setVisible(false);
        maxPopLabel = new Label("", skin, "normalFont", Color.WHITE);
        maxPopLabel.setWrap(true);
        maxPopLabel.setAlignment(1);
        maxPopLabel.setVisible(false);
        maxPopImg = new Image(uiAtlas.findRegion("popmaxSmall"));
        maxPopImg.setVisible(false);

        troopsTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(160, 745, 65, 20);
        troopsTable.setBounds((int) (rect.x + position.x), (int) rect.y, (int) rect.width, (int) rect.height);
        troopsTable.align(Align.top);
        stage.addActor(troopsTable);
        troopsTable.setVisible(false);

        sunimg = new Image();
        sunimg.setVisible(false);
        Texture tooltipTexture = screenManager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        sunimg.setUserObject(BaseTooltip.createTableTooltip(sunimg, tooltipTexture).getActor());
        sunimg.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    if (starsystemInfo != null && starsystemInfo.getAnomaly() != null
                            && starsystemInfo.getAnomaly().getType() == AnomalyType.WORMHOLE) {
                        WormholeLinker wh = screenManager.getUniverseMap().getWormHole(starsystemInfo.getCoordinates());
                        if (wh.isExplored(playerRace.getRaceId())) {
                            screenManager.getUniverseMap().setSelectedCoordValue(wh.getTarget());
                            screenManager.getUniverseMap().getRenderer().show();
                        }
                    }
                }
            }
        });

        stage.addActor(sectorLabel);
        stage.addActor(systemLabel);
        stage.addActor(currentPopLabel);
        stage.addActor(currentPopImg);
        stage.addActor(maxPopLabel);
        stage.addActor(maxPopImg);
        stage.addActor(sunimg);

        planetImg = new Array<Image>();
        planetLabel = new Array<Label>();
        planetBonusTable = new Array<Table>();
        for (int i = 0; i < GamePreferences.planetsInStarSystems; i++) {
            Image img = new Image();
            img.rotateBy(270);
            img.setUserObject(BaseTooltip.createTableTooltip(img, tooltipTexture).getActor());
            planetImg.add(img);
            img.setVisible(false);
            stage.addActor(img);

            LabelStyle ls = new LabelStyle();
            ls.font = skin.getFont("normalFont");
            Label l = new Label("", ls);
            planetLabel.add(l);
            stage.addActor(l);

            Table pbt = new Table();
            planetBonusTable.add(pbt);
            stage.addActor(pbt);
            img.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    for (int i = 0; i < planetImg.size; i++) {
                        Image im = planetImg.get(i);
                        if (event.getListenerActor() == im) {
                            if (i == lastPlanetTouched)
                                showPlanetInfo = !showPlanetInfo;
                            lastPlanetTouched = i;

                            if (starsystemInfo != null) {
                                Planet p = starsystemInfo.getPlanet(i);
                                drawPlanetInfo(p);

                                if (shipRenderer.isShipMove()
                                        && p.getIsHabitable()
                                        && !p.getIsTerraformed()
                                        && shipRenderer.getSelectedShip().getCoordinates()
                                                .equals(starsystemInfo.getCoordinates())
                                        && screenManager.getUniverseMap().getRenderer().getState() == GalaxyState.TERRAFORM) {
                                    if (shipRenderer.getSelectedShip().getColonizePoints() != 0
                                            && shipRenderer.getSelectedShip().getOwnerId().equals(playerRace.getRaceId())) {
                                        screenManager.getUniverseMap().getCurrentShip().setTerraform(i);
                                        drawPlanetInfo(p);
                                    }
                                    shipRenderer.clearSelectedShip();
                                    screenManager.getUniverseMap().getRenderer().clearSelection();
                                    drawPlanets(true);
                                }
                            }
                        }
                    }
                    return true;
                }
            });
        }
        terraformSpheres = new Array<Image>();
        terraformLabels = new Array<Label>();

        scanPowerText = new Table();
        scanPowerText.setSkin(skin);
        scanPowerText.setTransform(true);
        scanPowerText.rotateBy(270);
        resourcesText = new Table();
        resourcesText.setSkin(skin);
        resourcesText.setTransform(true);
        resourcesText.rotateBy(270);
        ownerText = new Table();
        ownerText.setTransform(true);
        ownerText.rotateBy(270);
        ownerText.setUserObject(BaseTooltip.createTableTooltip(ownerText, tooltipTexture).getActor());

        stage.addActor(scanPowerText);
        stage.addActor(resourcesText);
        stage.addActor(ownerText);

        shipWidth = position.width / 2 - shipPadding * 2;
        shipHeight = shipWidth * 0.75f;
        shipHeight = Math.round(shipHeight / 20) * 20.0f;
        shipWidth = shipHeight / 0.75f;
        int shipsInPage = (int) ((position.height - 6 * (position.width / 8 + shipPadding)) / shipHeight); //to leave space for buttons 
        float tableHeight = shipHeight * shipsInPage;
        Rectangle shipRendererPosition = new Rectangle(position.x + shipPadding, position.height - tableHeight - shipPadding,
                position.width - shipPadding * 2, tableHeight);
        shipRenderer = new ShipRenderer(screenManager, shipRendererPosition, shipWidth, shipHeight, shipPadding);
        starSystemOrShips = false;
        loadedTextures = new Array<String>();
        lastPlanetTouched = -1;
        showPlanetInfo = false;

        if (parent instanceof UniverseRenderer) {
            rect = GameConstants.coordsToRelative(0, 42, 42, 42);
            rect.x += position.x;
            helpWidget = new HelpWidget(screenManager, rect, (DefaultScreen) parent, stage);

            createOverlayButtons();
        }
    }

    private void drawPlanetInfo(Planet p) {
        String path = "graphics/planets/" + p.getPlanetGraphicFile() + ".png";
        Texture tex = screenManager.loadTextureImmediate(path);
        Sprite sp = new Sprite(tex);
        sp.setColor(new Color(0.4f, 0.4f, 0.4f, 0.9f));
        Table table = screenManager.getSidebarLeft().getInfoTable();
        int width = screenManager.getSidebarLeft().getInfoTableWidth();
        table.clear();
        screenManager.getSidebarLeft().getInfoImage().setDrawable(new SpriteDrawable(sp));

        if (showPlanetInfo) {
            String text = String.format("%s %s - %s", StringDB.getString("CLASS"), p.getPlanetClass(),
                    StringDB.getString("CLASS_" + p.getPlanetClass() + "_TYPE"));
            Label head = new Label(text, skin, "mediumFont",
                    playerRace.getRaceDesign().clrListMarkTextColor);
            head.setAlignment(Align.top);
            table.add(head);
            table.row();
            Label l = new Label(StringDB.getString("CLASS_" + p.getPlanetClass() + "_INFO"), skin,
                    "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            l.setWrap(true);
            table.add(l).width(width);
        } else {
            String text = "I N F O R M A T I O N";
            Label l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrListMarkTextColor);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s %s", StringDB.getString("NAME"), p.getPlanetName());
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s %s", StringDB.getString("CLASS"), p.getPlanetClass());
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s %.3f %s", StringDB.getString("MAX_HABITANTS"),
                    p.getMaxInhabitants(), StringDB.getString("MRD"));
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s %.3f %s", StringDB.getString("CURRENT_HABITANTS"),
                    p.getCurrentInhabitants(), StringDB.getString("MRD"));
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s %.2f", StringDB.getString("GROWTH"), p.getPlanetGrowth());
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            if (p.getNeededTerraformPoints() > 0) {
                table.row();
                text = String.format("%s: %d %s", StringDB.getString("TERRAFORM_ORDER"),
                        p.getNeededTerraformPoints(), StringDB.getString("POINTS_SHORT"));
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
                l.setAlignment(Align.top);
                table.add(l);
                table.row();
                if (p.getIsTerraForming()) {
                    text = String.format("%s: %d", StringDB.getString("ROUNDS"),
                            p.calcTerraformRoundsRemaining(starsystemInfo));
                    l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
                    l.setAlignment(Align.top);
                    table.add(l);
                    table.row();
                }
            }
            if (p.getIsHabitable()) {
                table.row();
                text = StringDB.getString("EXISTING_RES") + ":";
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
                l.setAlignment(Align.top);
                table.add(l);
                table.row();
                ButtonStyle bs = new ButtonStyle();
                Button b = new Button(bs);
                b.align(Align.center);
                boolean[] res = p.getAvailableResources();
                for (int j = ResourceTypes.TITAN.getType(); j <= ResourceTypes.DERITIUM.getType(); j++) {
                    ResourceTypes rt = ResourceTypes.fromResourceTypes(j);
                    if (res[j]) {
                        Image resImg = new Image(new TextureRegionDrawable(uiAtlas.findRegion(rt
                                .getImgName())));
                        b.add(resImg).width(GameConstants.wToRelative(20))
                                .height(GameConstants.hToRelative(16));
                    }
                }
                table.add(b);
            }
        }
        screenManager.getSidebarLeft().showInfo(false);
    }

    private void drawPlanets(boolean visible) {
        for (int i = 0; i < terraformSpheres.size; i++) {
            terraformSpheres.get(i).remove();
            terraformLabels.get(i).remove();
        }
        terraformSpheres.clear();
        terraformLabels.clear();
        if (visible) {
            for (int i = 0; i < planetImg.size; i++) {
                Image img = planetImg.get(i);
                if (i < starsystemInfo.getPlanets().size) {
                    Planet p = starsystemInfo.getPlanets().get(i);
                    generatePlanetTooltip(p, img);

                    String path = "graphics/planets/" + p.getPlanetGraphicFile() + ".png";
                    loadedTextures.add(path);
                    TextureRegion ptex = new TextureRegion(screenManager.loadTextureImmediate(path));
                    img.setDrawable(new TextureRegionDrawable(ptex));
                    float size = sidebarHeight / p.getSize().getScale();

                    img.setBounds(position.x + sidebarWidth / 2 - size / 2, (i > 0) ? (planetImg.get(i - 1).getY()
                            + planetImg.get(i - 1).getHeight() + 10) : sunimg.getY() + sunSize + sunSize / 16, size, size);
                    img.setOrigin(img.getWidth() / 2, img.getHeight() / 2);
                    img.setVisible(true);

                    Label l = planetLabel.get(i);
                    float xPadding = GameConstants.wToRelative(10);
                    LabelStyle ls = l.getStyle();
                    GlyphLayout layout = new GlyphLayout(ls.font, p.getPlanetClass());
                    l.setPosition((int) (img.getX() - layout.width - xPadding), (int) (img.getY() + img.getHeight() / 2));
                    Color c;
                    if (!p.getIsHabitable())
                        c = new Color(0, 0, 1.0f, 1.0f);
                    else if (p.getIsTerraformed()) {
                        if (p.getCurrentInhabitants() > 0.0f)
                            c = new Color(0, 1.0f, 30 / 255.0f, 1.0f);
                        else
                            c = new Color(180 / 255.0f, 1.0f, 180 / 255.0f, 1.0f);
                    } else if (p.getIsTerraForming()) {
                        path = "graphics/planets/TerraformSphere.png";
                        loadedTextures.add(path);
                        Image terraformSphere = new Image(screenManager.loadTextureImmediate(path));
                        terraformSphere.setBounds(img.getX(), img.getY(), img.getWidth(), img.getHeight());
                        terraformSphere.setTouchable(Touchable.disabled);
                        stage.addActor(terraformSphere);
                        terraformSpheres.add(terraformSphere);
                        int progress = (int) ((1.0f - (float) (p.getNeededTerraformPoints())
                                / (float) (p.getStartTerraformPoints())) * 100);
                        String progressText = progress + "%";
                        layout.setText(ls.font, progressText);
                        Label tl = new Label(progressText, ls);
                        tl.setPosition((int) (l.getX() - layout.width / 4),
                                (int) (img.getY() + img.getHeight() / 2 - l.getHeight() - layout.height * 2));
                        tl.setTouchable(Touchable.disabled);
                        stage.addActor(tl);
                        terraformLabels.add(tl);
                        c = new Color(200 / 255.0f, 200 / 255.0f, 0, 1.0f);
                    } else
                        c = new Color(1.0f, 40 / 255.0f, 40 / 255.0f, 1.0f);

                    ls.fontColor = c;
                    l.setText(p.getPlanetClass());
                    l.setVisible(true);

                    Table bt = planetBonusTable.get(i);
                    bt.clear();
                    boolean[] bonuses = p.getBonuses();
                    float imgHeight = ls.font.getLineHeight() * 0.75f;
                    for (int k = 0; k < bonuses.length; k++) {
                        if (bonuses[k]) {
                            String imgName;
                            if (k < 7) {
                                ResourceTypes rt = ResourceTypes.fromResourceTypes(k);
                                imgName = rt.getImgName();
                            } else
                                imgName = "energySmall";
                            bt.add(new Image(new TextureRegion(uiAtlas.findRegion(imgName)))).height(imgHeight)
                                    .width(imgHeight * 1.25f);
                            bt.row();
                        }
                    }
                    bt.setVisible(true);
                    float tbHeight = bt.getRows() * imgHeight;
                    bt.setBounds((int) (img.getX() + img.getWidth() + xPadding),
                            (int) (img.getY() + img.getHeight() / 2 - tbHeight / 2), (int) (imgHeight * 1.25f), (int) tbHeight);
                } else {
                    img.setVisible(false);
                    planetLabel.get(i).setVisible(false);
                    planetBonusTable.get(i).setVisible(false);
                }
            }
        } else {
            for (int i = 0; i < planetImg.size; i++) {
                planetImg.get(i).setVisible(false);
                planetLabel.get(i).setVisible(false);
                planetBonusTable.get(i).setVisible(false);
            }
        }
    }

    private void showScanPowerInfo() {
        Color color = ScanPower.getColor(starsystemInfo.getScanPower(playerRace.getRaceId()), 1.0f);
        scanPowerText.add(StringDB.getString("SCANPOWER") + ": " + starsystemInfo.getScanPower(playerRace.getRaceId()) + "%",
                "smallFont", color);
        float offset = Gdx.graphics.getWidth() / 180f;
        scanPowerText.setBounds((int) (position.x + position.getWidth() - offset - 50), (int) (position.getHeight() / 3.9f), 100,
                30); //50 is half of the text width
        scanPowerText.setOrigin(scanPowerText.getWidth() / 2, scanPowerText.getHeight() / 2);
    }

    public void showStarSystemInfo() {
        lastPlanetTouched = -1;
        hideStarSystemInfo();
        starSystemOrShips = false;
        if (starsystemInfo != null) {
            if (helpWidget != null) {
                helpWidget.setType(TutorialType.TGALAXYMAP);
                if (!screenManager.getGameSettings().tutorialSeen[TutorialType.TGALAXYMAP.getType()])
                    helpWidget.startFadeIn();
                else
                    helpWidget.stopFadeIn();
            }

            generateStarTooltip();
            scanPowerText.clear();
            resourcesText.clear();
            BitmapFont font = skin.getFont("normalFont");
            sectorLabel.setText(StringDB.getString("SECTOR") + " " + starsystemInfo.getCoordinates().x + ","
                    + starsystemInfo.getCoordinates().y);
            if (starsystemInfo.getScanned(playerRace.getRaceId())) {
                showScanPowerInfo();
                String sysLabelText = "";
                if (starsystemInfo.getAnomaly() == null)
                    if (!starsystemInfo.isSunSystem()
                            && starsystemInfo.getStation() != null
                            && (playerRace.isRaceContacted(starsystemInfo.getStation().getOwnerId()) || playerRace.getRaceId()
                                    .equals(starsystemInfo.getStation().getOwnerId())))
                        sysLabelText = starsystemInfo.getStation().getName();
                    else
                        sysLabelText = starsystemInfo.getName();
                else {
                    sysLabelText = StringDB.getString(starsystemInfo.getAnomaly().getType().getMapName()) + " "
                            + starsystemInfo.getCoordinates().toString();
                    if (starsystemInfo.getAnomaly().getType() == AnomalyType.WORMHOLE) {
                        WormholeLinker lnk = screenManager.getUniverseMap().getWormHole(starsystemInfo.getCoordinates());
                        if (lnk.isExplored(playerRace.getRaceId())) {
                            maxPopLabel.setText(StringDB.getString(lnk.getLinktype().getDBName()) + " "
                                    + StringDB.getString("WORMHOLE"));
                            maxPopLabel.setAlignment(Align.center);
                            maxPopLabel.setVisible(true);
                            currentPopLabel.setText(StringDB.getString("TARGET") + ": " + lnk.getTarget());
                            currentPopLabel.setAlignment(Align.center);
                            currentPopLabel.setVisible(true);
                        }
                    }
                }
                if (!sysLabelText.isEmpty()) {
                    systemLabel.setText(sysLabelText);
                    systemLabel.setVisible(true);
                }

                if (starsystemInfo.isSunSystem() && starsystemInfo.getKnown(playerRace.getRaceId())) {
                    resourcesText.setVisible(true);
                    String text = StringDB.getString("EXISTING_RES") + ":";
                    GlyphLayout layout = new GlyphLayout(font, text);
                    Rectangle rect = GameConstants.coordsToRelative(0, 265, layout.width + 6 * 25f, 30);
                    rect.x += position.x;
                    resourcesText.setBounds((int) rect.x, (int) (rect.y + rect.height), rect.width, (int) rect.height);
                    Label textLabel = new Label(text, skin, "normalFont", playerRace.getRaceDesign().clrNormalText);
                    textLabel.setAlignment(Align.left);
                    resourcesText.add(textLabel).align(Align.left).spaceLeft(rect.height);
                    boolean[] res = starsystemInfo.getAvailableResources(false);
                    boolean[] resColonized = starsystemInfo.getAvailableResources(true);

                    for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.DERITIUM.getType(); i++) {
                        Image resImg = new Image();
                        ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
                        if (res[i])
                            resImg.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion(rt.getImgName())));
                        if (resColonized[i] == false) {
                            resImg.setColor(new Color(1, 1, 1, 100 / 255.0f));
                        }
                        resImg.setSize(rect.height * 0.4f * 1.25f, rect.height * 0.4f);
                        resImg.setOrigin(resImg.getWidth() / 2, resImg.getHeight() / 2);
                        resImg.rotateBy(90);
                        resourcesText.add(resImg).height(rect.height * 0.4f).width(rect.height * 0.4f * 1.25f);
                    }
                    layout.setText(font, text);
                    resourcesText.setWidth((int) (layout.width + 6 * rect.height * 0.4f * 1.25f)); //have to set this again as previous calculations are off
                }

                if (starsystemInfo.getFullKnown(playerRace.getRaceId()) && starsystemInfo.isSunSystem()) {
                    maxPopLabel.setText(StringDB.getString("MAX_HABITANTS") + ": "
                            + String.format("%.3f", starsystemInfo.getMaxInhabitants()) + StringDB.getString("MRD"));
                    maxPopLabel.setAlignment(Align.left);
                    maxPopLabel.setVisible(true);
                    maxPopImg.setVisible(true);
                    currentPopLabel.setText(StringDB.getString("CURRENT_HABITANTS") + ": "
                            + String.format("%.3f", starsystemInfo.getCurrentInhabitants()) + StringDB.getString("MRD"));
                    currentPopLabel.setAlignment(Align.left);
                    currentPopLabel.setVisible(true);
                    currentPopImg.setVisible(true);
                }
            } else {
                systemLabel.setVisible(true);
                systemLabel.setText(StringDB.getString("UNKNOWN"));
            }
            if (starsystemInfo.isMajorized()
                    && (starsystemInfo.getScanPower(playerRace.getRaceId()) > 50 || starsystemInfo.getOwnerId().equals(
                            playerRace.getRaceId()))) {
                troopsTable.clear();
                int troopNumber = starsystemInfo.getTroops().size;
                if (troopNumber > 0) {
                    String text = String.format("%d x ", troopNumber);
                    Label l = new Label(text, skin, "normalFont", Color.WHITE);
                    l.setAlignment(Align.right);
                    troopsTable.add(l).width((int) GameConstants.wToRelative(40));
                    Image img = new Image(uiAtlas.findRegion("troopSmall"));
                    troopsTable.add(img).width((int) GameConstants.wToRelative(25)).height(troopsTable.getHeight());
                    troopsTable.setVisible(true);
                }
            }
            sectorLabel.setVisible(true);
            BitmapFont normalFont = skin.getFont("normalFont");
            GlyphLayout layout = new GlyphLayout(normalFont, sectorLabel.getText());
            sectorLabel.setBounds((int) (position.x + sidebarWidth / 2 - layout.width / 2),
                    (int) (sidebarHeight - sidebarHeight / 24), layout.width, (int) layout.height);
            layout.setText(normalFont, systemLabel.getText());
            systemLabel.setBounds((int) (position.x + sidebarWidth / 2 - layout.width / 2), (int) (sectorLabel.getY()
                    - sectorLabel.getHeight() - layout.height / 4), layout.width, (int) layout.height);
            layout.setText(normalFont, maxPopLabel.getText());
            float xPadding = GameConstants.wToRelative(38);
            maxPopLabel.setBounds((int) (position.x + xPadding),
                    (int) (systemLabel.getY() - systemLabel.getHeight() - layout.height), (int) (sidebarWidth - xPadding * 2),
                    (int) layout.height);
            troopsTable.setY(maxPopLabel.getY());
            int imgWidth = (int) (maxPopLabel.getHeight());
            maxPopImg.setBounds(maxPopLabel.getX() - imgWidth * 1.25f, maxPopLabel.getY(), imgWidth * 1.25f, imgWidth);
            layout.setText(normalFont, currentPopLabel.getText());
            currentPopLabel.setBounds((int) (maxPopLabel.getX()),
                    (int) (maxPopLabel.getY() - maxPopLabel.getHeight() - layout.height / 4),
                    (int) (sidebarWidth - xPadding * 2), (int) layout.height);
            currentPopImg
                    .setBounds(currentPopLabel.getX() - imgWidth * 1.25f, currentPopLabel.getY(), imgWidth * 1.25f, imgWidth);
            if (starsystemInfo.getScanned(playerRace.getRaceId())) {
                if (starsystemInfo.isSunSystem()) {
                    String path = "graphics/sun/" + starsystemInfo.getStarType().getSunName() + ".png";
                    loadedTextures.add(path);
                    TextureRegion tex = new TextureRegion(screenManager.loadTextureImmediate(path));
                    sunimg.setDrawable(new TextureRegionDrawable(tex));
                    sunimg.setBounds(position.x + sidebarWidth / 2 - sunSize / 2, -sunSize / 2, sunSize, sunSize);
                    sunimg.setVisible(true);
                } else if (starsystemInfo.getAnomaly() != null) {
                    TextureRegion tex = starAtlas.findRegion(starsystemInfo.getAnomaly().getType().getName());
                    tex = new TextureRegion(tex);
                    tex.flip(starsystemInfo.getAnomaly().getFlipHorz(), false);
                    sunimg.setDrawable(new TextureRegionDrawable(tex));
                    sunimg.setBounds(position.x + sidebarWidth / 2 - sunSize / 2, sidebarHeight / 2 - sunSize / 2, sunSize,
                            sunSize);
                    sunimg.setVisible(true);
                } else if (starsystemInfo.getStation() != null
                        && (playerRace.isRaceContacted(starsystemInfo.getStation().getOwnerId()) || playerRace.getRaceId()
                                .equals(starsystemInfo.getStation().getOwnerId()))) {
                    Major major = screenManager.getRaceController().getMajors().get(starsystemInfo.getStation().getOwnerId());
                    if (major != null) {
                        Drawable dr;
                        if (screenManager.getAssetManager()
                                .isLoaded("graphics/ui/" + major.getPrefix() + "ui.pack")) {
                            TextureAtlas uiAtlas = screenManager.getAssetManager()
                                    .get("graphics/ui/" + major.getPrefix() + "ui.pack");
                            TextureRegion tex = new TextureRegion(uiAtlas.findRegion(major.getPrefix() + "Starbase"));
                            dr = new TextureRegionDrawable(tex);
                        } else {//ui_pack missing
                            dr = new TextureRegionDrawable(new TextureRegion(screenManager.loadTextureImmediate("graphics/ships/ImageMissing.png")));
                        }
                        sunimg.setDrawable(dr);
                        sunimg.setBounds(position.x + sidebarWidth / 2 - sunSize / 2, sidebarHeight / 2 - sunSize / 2, sunSize,
                                sunSize);
                        sunimg.setVisible(true);
                        starsystemInfo.getStation().getTooltip(null, (Table) sunimg.getUserObject(), skin, headerFont,
                                headerColor, textFont, textColor);
                    }
                }
            }
            drawPlanets(starsystemInfo.isSunSystem() && starsystemInfo.getFullKnown(playerRace.getRaceId()));

            //draw name of the owner
            if (starsystemInfo.getScanned(playerRace.getRaceId()) && playerRace.isRaceContacted(starsystemInfo.getOwnerId())
                    || starsystemInfo.getOwnerId().equals(playerRace.getRaceId())) {
                ownerText.setVisible(true);
                generateOwnerTooltip();
                Race owner = starsystemInfo.getOwner();
                ownerText.clearChildren();
                if (owner != null) {
                    String text = owner.getName();
                    Color c;
                    if (owner.isMajor())
                        c = new Color(((Major) owner).getRaceDesign().clrSector);
                    else
                        c = Color.WHITE;

                    Rectangle rect = GameConstants.coordsToRelative(0, 265, 180, 35);
                    rect.x = position.x + resourcesText.getHeight();
                    ownerText.setBounds((int) rect.x, (int) (rect.y + rect.height), rect.width, (int) rect.height);
                    ownerText.setOrigin(0, 0);
                    Label textLabel = new Label(text, skin, "hugeFont", c);
                    textLabel.setAlignment(Align.bottom);
                    ownerText.add(textLabel).align(Align.bottom);
                }
            }
        }
    }

    private void generateStarTooltip() {
        Table table = (Table) sunimg.getUserObject();
        String text;

        table.clearChildren();
        if (starsystemInfo.getAnomaly() != null) {
            text = starsystemInfo.getAnomaly().getMapName(starsystemInfo.getCoordinates());
            Label l = new Label(text, skin, headerFont, headerColor);
            table.add(l);
            table.row();

            text = starsystemInfo.getAnomaly().getPhysicalDescription();
            l = new Label(text, skin, textFont, textColor);
            l.setWrap(true);
            table.add(l).width(sidebarWidth * 2);
            table.row();

            text = starsystemInfo.getAnomaly().getGameplayDescription();
            l = new Label(text, skin, textFont, headerColor);
            l.setWrap(true);
            l.setAlignment(Align.center);
            table.add(l).width(sidebarWidth * 2);
            table.row();
        } else if (starsystemInfo.isSunSystem()) {
            text = StringDB.getString(starsystemInfo.getStarType().getDBName());
            Label l = new Label(text, skin, headerFont, headerColor);
            table.add(l);
            table.row();

            text = StringDB.getString(starsystemInfo.getStarType().getDBName() + "_DESC");
            l = new Label(text, skin, textFont, textColor);
            l.setWrap(true);
            table.add(l).width(sidebarWidth * 2);

            if (starsystemInfo.getFullKnown(playerRace.getRaceId())) {
                table.row();

                text = StringDB.getString("BONI_IN_SYSTEM");
                l = new Label(text, skin, textFont, headerColor);
                table.add(l);
                table.row();

                for (int j = 0; j < 8; j++) {
                    int bonus = 0;
                    for (Planet planet : starsystemInfo.getPlanets()) {
                        if (planet.getBonuses()[j])
                            if (j != ResourceTypes.DERITIUM.getType())
                                bonus += (planet.getSize().getSize() + 1) * 25;
                    }

                    if (bonus != 0) {
                        String boniText = (j == 7) ? "ENERGY" : ResourceTypes.fromResourceTypes(j).getName();
                        boniText = StringDB.getString(boniText + "_BONUS");
                        text = "" + bonus + "% " + boniText;
                        l = new Label(text, skin, textFont, textColor);
                        table.add(l);
                        table.row();
                    }
                }
            }
        }
    }

    private void generatePlanetTooltip(Planet p, Image img) {
        Table table = (Table) img.getUserObject();
        table.clearChildren();
        String text;

        text = p.getPlanetName();
        Label l = new Label(text, skin, headerFont, headerColor);
        table.add(l);
        table.row();

        text = "(" + StringDB.getString("CLASS_" + p.getPlanetClass() + "_TYPE") + ")";
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        text = StringDB.getString("CLASS_" + p.getPlanetClass() + "_INFO");
        l = new Label(text, skin, textFont, textColor);
        l.setWrap(true);
        table.add(l).width(sidebarWidth * 2);
        table.row();

        if (p.getNeededTerraformPoints() > 0) {
            text = String.format("%s: %d %s", StringDB.getString("TERRAFORM_ORDER"),
                    p.getNeededTerraformPoints(), StringDB.getString("POINTS_SHORT"));

            if (p.getIsTerraForming()) {
                text += String.format("  -  %s: %d", StringDB.getString("ROUNDS"),
                        p.calcTerraformRoundsRemaining(starsystemInfo));
            }
            l = new Label(text, skin, textFont, textColor);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
        }

        for (int j = 0; j < 8; j++) {
            int bonus = 0;
            text = "";
            if (p.getBonuses()[j])
                if (j != ResourceTypes.DERITIUM.getType()) {
                    bonus = (p.getSize().getSize() + 1) * 25;
                    String boniText = (j == 7) ? "ENERGY" : ResourceTypes.fromResourceTypes(j).getName();
                    boniText = StringDB.getString(boniText + "_BONUS");
                    text = "" + bonus + "% " + boniText;
                } else {
                    text = StringDB.getString("DERITIUM") + " " + StringDB.getString("EXISTING");
                }

            if (!text.equals("")) {
                l = new Label(text, skin, textFont, headerColor);
                table.add(l);
                table.row();
            }
        }
    }

    private void generateOwnerTooltip() {
        Table table = (Table) ownerText.getUserObject();
        Race owner = starsystemInfo.getOwner();
        owner.getTooltip(table, skin, headerFont, headerColor, textFont, textColor, false);
    }

    public void hideStarSystemInfo() {
        systemLabel.setVisible(false);
        resourcesText.setVisible(false);
        ownerText.setVisible(false);
        currentPopLabel.setVisible(false);
        currentPopImg.setVisible(false);
        maxPopLabel.setVisible(false);
        maxPopImg.setVisible(false);
        sunimg.setVisible(false);
        troopsTable.setVisible(false);
        for (Image i : planetImg)
            i.setVisible(false);
        for (Table t : planetBonusTable)
            t.setVisible(false);
        for (Label l : planetLabel)
            l.setVisible(false);
        for (Image i : terraformSpheres)
            i.setVisible(false);
        for (Label l : terraformLabels)
            l.setVisible(false);

        for (String path : loadedTextures)
            if (screenManager.getAssetManager().isLoaded(path))
                screenManager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    public void showShips() {
        starSystemOrShips = true;
        if (starsystemInfo != null) {
            if (helpWidget != null) {
                helpWidget.setType(TutorialType.TSHIPORDER);
                if (!screenManager.getGameSettings().tutorialSeen[TutorialType.TSHIPORDER.getType()])
                    helpWidget.startFadeIn();
                else
                    helpWidget.stopFadeIn();
            }

            sectorLabel.setVisible(false);
            shipRenderer.drawShips(starsystemInfo, null, stage, skin);
        }
    }

    public ShipRenderer getShipRenderer() {
        return shipRenderer;
    }

    public boolean isStarSystemShown() {
        return !starSystemOrShips;
    }

    public void refreshShips() {
        shipRenderer.hideShips();
        showShips();
    }

    public void clearSelectedShip() {
        shipRenderer.clearSelectedShip();
    }

    @Override
    public void dispose() {
        if (helpWidget != null)
            helpWidget.dispose();
        for (String path : loadedTextures)
            if (screenManager.getAssetManager().isLoaded(path))
                screenManager.getAssetManager().unload(path);
        loadedTextures.clear();
        super.dispose();
    }

    public Image getBackground() {
        return bgImage;
    }

    enum OverLayEnum {
        GRID("grid", "GRID"),
        NAMES("names", "NAMES"),
        ROUTES("routes", "ROUTES"),
        NORMAL("galaxy", "SHOW_GALAXY"),
        SCAN("scan", "SHOW_SCAN"),
        DIPLOMACY("diplomacy", "SHOW_DIPLOMACY"),
        EXPANSION("expansion", "SHOW_EXPANSION");

        String textureName;
        String dbName;

        private OverLayEnum(String textureName, String dbName) {
            this.textureName = textureName;
            this.dbName = dbName;
        }

        String getDBName() {
            return dbName;
        }

        Drawable getUp(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect_x"));
            sprite.setColor(Color.RED);
            return new SpriteDrawable(sprite);
        }

        Drawable getDown(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect"));
            sprite.setColor(Color.GRAY);
            return new SpriteDrawable(sprite);
        }

        Drawable getChecked(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect"));
            sprite.setColor(Color.GREEN);
            return new SpriteDrawable(sprite);
        }

        Drawable getCheckedOver(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect"));
            sprite.setColor(Color.YELLOW);
            return new SpriteDrawable(sprite);
        }

        Drawable getOver(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect_x"));
            sprite.setColor(Color.YELLOW);
            return new SpriteDrawable(sprite);
        }

        Drawable getDisabled(TextureAtlas atlas) {
            Sprite sprite = new Sprite(atlas.findRegion("rect"));
            sprite.setColor(Color.DARK_GRAY);
            return new SpriteDrawable(sprite);
        }

        Drawable getBackground(TextureAtlas atlas) {
            return new TextureRegionDrawable(atlas.findRegion(textureName));
        }

        boolean isNeededToCheck(ScreenManager manager) {
            switch (this) {
                case GRID:
                    return manager.getGameSettings().drawGrid;
                case NAMES:
                    return manager.getGameSettings().showStarSystemNames;
                case ROUTES:
                    return manager.getGameSettings().drawRoutes;
                case NORMAL:
                    return manager.getGameSettings().getShowState() == GalaxyShowState.POLITICAL;
                case SCAN:
                    return manager.getGameSettings().getShowState() == GalaxyShowState.SCANRANGE;
                case DIPLOMACY:
                    return manager.getGameSettings().getShowState() == GalaxyShowState.DIPLOMACY;
                case EXPANSION:
                    return manager.getGameSettings().getShowState() == GalaxyShowState.EXPANSION;
                default:
                    break;
            }
            return false;
        }
    }

    private void createShowOptionsButtons(final Table overlayTable) {
        float size = 50;
        Rectangle rect = GameConstants.coordsToRelative(0, 810, size, size);
        rect.x += position.x - rect.width;
        TextureAtlas uiAtlas = screenManager.getAssetManager().get("graphics/ui/ui.pack");
        ButtonStyle bs = new ButtonStyle();
        final Drawable downClosed = new TextureRegionDrawable(uiAtlas.findRegion("open_options")).tint(Color.DARK_GRAY);
        final Drawable downOpen = new TextureRegionDrawable(uiAtlas.findRegion("close_options")).tint(Color.DARK_GRAY);
        bs.up = new TextureRegionDrawable(uiAtlas.findRegion("close_options")).tint(Color.LIGHT_GRAY);
        bs.over = new TextureRegionDrawable(uiAtlas.findRegion("close_options"));
        bs.checked = new TextureRegionDrawable(uiAtlas.findRegion("open_options")).tint(Color.LIGHT_GRAY);
        bs.checkedOver = new TextureRegionDrawable(uiAtlas.findRegion("open_options"));
        bs.down = screenManager.getGameSettings().showOptionsMenu ? downOpen : downClosed;
        bs.disabled = new TextureRegionDrawable(uiAtlas.findRegion("open_options")).tint(Color.GRAY);
        Button button = new Button(bs);
        button.setName("OVERLAY");
        button.setChecked(!screenManager.getGameSettings().showOptionsMenu);
        button.setBounds((int) (rect.x), (int) rect.y, (int) rect.width, (int) rect.height);
        String tooltipTextFont = "largeFont";
        Texture tooltipTexture = screenManager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        Color tooltipTextColor = screenManager.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;
        Table ttable = BaseTooltip.createTableTooltip(button, tooltipTexture).getActor();
        String text = StringDB.getString(screenManager.getGameSettings().showOptionsMenu ? "CLOSE_OPTIONS" : "OPEN_OPTIONS");
        final Label tlabel = new Label(text, skin, tooltipTextFont, tooltipTextColor);
        tlabel.setAlignment(Align.center);
        tlabel.setWrap(true);
        ttable.add(tlabel).width(GameConstants.wToRelative(size * 3));
        ttable.row();
        stage.addActor(button);
        button.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button btn = (Button) event.getListenerActor();
                screenManager.getGameSettings().showOptionsMenu = !screenManager.getGameSettings().showOptionsMenu;
                overlayTable.setVisible(screenManager.getGameSettings().showOptionsMenu);
                btn.getStyle().down = screenManager.getGameSettings().showOptionsMenu ? downOpen : downClosed;
                tlabel.setText(StringDB.getString(screenManager.getGameSettings().showOptionsMenu ? "CLOSE_OPTIONS" : "OPEN_OPTIONS"));
            }
        });
    }

    public void hideOverlayButtons() {
        if (overlayTable != null) {
            stage.getRoot().findActor("OVERLAY").setVisible(false);
            overlayTable.setVisible(false);
        }
        if (helpWidget != null) {
            helpWidget.hide();
        }
    }

    public void showOverlayButtonsIfNeeded() {
        if (overlayTable != null) {
            Actor actor = stage.getRoot().findActor("OVERLAY");
            if (actor != null)
                actor.setVisible(true);
            overlayTable.setVisible(screenManager.getGameSettings().showOptionsMenu);
        }
        if (helpWidget != null) {
            helpWidget.show();
        }
    }

    private void createOverlayButtons() {
        float size = 50;
        float pad = size / 10.0f;
        Rectangle rect = GameConstants.coordsToRelative(0, 810 - size - pad, size, OverLayEnum.values().length * (size + pad) - pad);
        rect.x += position.x - rect.width;
        overlayTable = new Table();
        overlayTable.setBounds((int) (rect.x), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(overlayTable);
        createShowOptionsButtons(overlayTable);
        overlayTable.setVisible(screenManager.getGameSettings().showOptionsMenu);

        TextureAtlas uiAtlas = screenManager.getAssetManager().get("graphics/ui/ui.pack");
        String tooltipTextFont = "largeFont";
        Texture tooltipTexture = screenManager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        Color tooltipTextColor = screenManager.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;

        for (OverLayEnum enumVal : OverLayEnum.values()) {
            ImageButtonStyle bs = new ImageButtonStyle();
            bs.imageUp = enumVal.getUp(uiAtlas);
            bs.imageDown = enumVal.getDown(uiAtlas);
            bs.imageChecked = enumVal.getChecked(uiAtlas);
            bs.imageOver = enumVal.getOver(uiAtlas);
            bs.imageDisabled = enumVal.getDisabled(uiAtlas);
            bs.imageCheckedOver = enumVal.getCheckedOver(uiAtlas);
            bs.up = enumVal.getBackground(uiAtlas);
            ImageButton button = new ImageButton(bs);
            button.setChecked(enumVal.isNeededToCheck(screenManager));
            button.setUserObject(enumVal);
            overlayTable.add(button).height(GameConstants.hToRelative(size)).width(GameConstants.wToRelative(size))
                    .spaceBottom(GameConstants.hToRelative(pad));
            overlayTable.row();
            Table ttable = BaseTooltip.createTableTooltip(button, tooltipTexture).getActor();
            String text = StringDB.getString(enumVal.getDBName());
            Label l = new Label(text, skin, tooltipTextFont, tooltipTextColor);
            l.setAlignment(Align.center);
            l.setWrap(true);
            ttable.add(l).width(GameConstants.wToRelative(size * 3));
            ttable.row();
            button.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    OverLayEnum selected = (OverLayEnum) event.getListenerActor().getUserObject();
                    switch (selected) {
                        case GRID:
                            screenManager.getGameSettings().drawGrid = !screenManager.getGameSettings().drawGrid;
                            break;
                        case NAMES:
                            screenManager.getGameSettings().showStarSystemNames = !screenManager.getGameSettings().showStarSystemNames;
                            break;
                        case ROUTES:
                            screenManager.getGameSettings().drawRoutes = !screenManager.getGameSettings().drawRoutes;
                            break;
                        case NORMAL:
                            screenManager.getGameSettings().setShowState(GalaxyShowState.POLITICAL);
                            break;
                        case SCAN:
                            screenManager.getGameSettings().setShowState(GalaxyShowState.SCANRANGE);
                            break;
                        case DIPLOMACY:
                            screenManager.getGameSettings().setShowState(GalaxyShowState.DIPLOMACY);
                            break;
                        case EXPANSION:
                            screenManager.getGameSettings().setShowState(GalaxyShowState.EXPANSION);
                            break;
                        default:
                            break;
                    }

                    screenManager.getUniverseMap().getRenderer().updateView();
                    for (int i = 0; i < overlayTable.getChildren().size; i++) {
                        if (overlayTable.getChildren().get(i) instanceof ImageButton) {
                            ImageButton ib = (ImageButton) overlayTable.getChildren().get(i);
                            OverLayEnum type = (OverLayEnum) ib.getUserObject();
                            ib.setChecked(type.isNeededToCheck(screenManager));
                        }
                    }
                }
            });
        }
    }
}
