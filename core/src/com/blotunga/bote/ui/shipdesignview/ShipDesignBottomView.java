/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.shipdesignview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.ShipInfo;

public class ShipDesignBottomView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table bottomTable;

    public ShipDesignBottomView(ScreenManager manager, Stage stage, Skin skin, float xOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        bottomTable = new Table();
        bottomTable.align(Align.left);
        Rectangle rect = GameConstants.coordsToRelative(40, 170, 1115, 150);
        bottomTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(bottomTable);
        bottomTable.setVisible(false);
    }

    public void show(int currentShipInfo) {
        bottomTable.clear();
        float headHeight = GameConstants.hToRelative(30);
        ShipInfo info = manager.getShipInfos().get(currentShipInfo);
        String headline = String.format("%s %s %s-%s", info.getShipTypeAsString(), StringDB.getString("MASC_ARTICLE"),
                info.getShipClass(), StringDB.getString("CLASS"));

        Label head = new Label(headline, skin, "xlFont", markColor);
        bottomTable.add(head).align(Align.topLeft).height((int) headHeight);
        bottomTable.row();

        String text = info.getDescription();
        Label txt = new Label(text, skin, "normalFont", normalColor);
        txt.setWrap(true);
        bottomTable.add(txt).width((int) bottomTable.getWidth()).height((int) (bottomTable.getHeight() - headHeight));
        bottomTable.setVisible(true);
    }

    public void hide() {
        bottomTable.setVisible(false);
    }

}