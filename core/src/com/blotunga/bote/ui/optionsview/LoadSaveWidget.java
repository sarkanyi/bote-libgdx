/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.optionsview;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.InflaterInputStream;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.DriveIntegration.DriveIntegrationCallback;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.PlatformApiIntegration.PlatformType;
import com.blotunga.bote.PlatformApiIntegration.StorageIntegration;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.RaceController;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;

public class LoadSaveWidget implements DriveIntegrationCallback {
    private enum LoadSaveState {
        NONE,
        OVERWRITE,
        DELETE,
        SAVETODRIVE,
        LOADFROMDRIVE,
        EXPORTSAVE,
        IMPORTSAVE
    }

    private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private boolean isSave;
    private Table saveScrollTable;
    private ScrollPane saveScroller;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private Array<Button> saveItems;
    private Button saveSelection = null;
    private TextButton rightButton;
    private String saveName;
    private Array<SaveInfo> saveInfos;
    private int nextIndex;
    private Label overWriteLabel;
    private TextButton leftButton;
    private RaceController raceCtrl;
    private Array<Texture> loadedTextures;
    private LoadSaveState state;
    private TextButton saveToDriveButton;
    private TextButton loadFromDriveButton;
    private TextButton exportSavesButton;
    private TextButton importSavesButton;

    public LoadSaveWidget(final ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset, Color normal,
            Color mark) {
        this.manager = manager;
        this.stage = stage;
        this.skin = skin;
        this.isSave = false;
        state = LoadSaveState.NONE;
        normalColor = normal;
        markColor = mark;
        //This new RaceController is needed because when loading/saving during an ongoing game it can happen that the race who's save
        //we want to load/overwrite has already been eliminated and using the standard RaceController would crash the game
        raceCtrl = new RaceController(manager);
        raceCtrl.initMajors();

        saveScrollTable = new Table();
        saveScrollTable.align(Align.topLeft);
        saveScroller = new ScrollPane(saveScrollTable, skin);
        saveScroller.setVariableSizeKnobs(false);
        saveScroller.setFadeScrollBars(false);
        saveScroller.setScrollbarsOnTop(false);
        saveScroller.setScrollingDisabled(true, false);
        stage.addActor(saveScroller);
        Rectangle rect = GameConstants.coordsToRelative(250, 660, 835, 530);
        saveScroller.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset),
                (int) (rect.width + GameConstants.wToRelative(280) - xOffset), (int) rect.height);
        saveScroller.setVisible(false);

        TextButtonStyle styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        selectTexture = manager.getUiTexture("listselect");
        saveItems = new Array<Button>();
        saveInfos = new Array<SaveInfo>();

        rightButton = new TextButton(StringDB.getString("SAVE", true), styleSmall);
        rect = GameConstants.coordsToRelative(0, 130, 160, 35);
        rightButton.setBounds((int) (saveScroller.getX() + saveScroller.getWidth() - rect.width), (int) (rect.y + yOffset),
                (int) rect.width, (int) rect.height);
        rightButton.setVisible(false);
        rightButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (((TextButton) event.getListenerActor()).isDisabled())
                    return;

                if (state == LoadSaveState.NONE) {
                    if (isSave) {
                        if (saveName.isEmpty())
                            manager.getUniverseMap().saveUniverse("bote" + String.format("%04d", ++nextIndex) + ".sav");
                        else
                            state = LoadSaveState.OVERWRITE;
                    } else {
                        if (loadSavegame())
                            return;
                    }
                } else if (state == LoadSaveState.DELETE) {
                    deleteSavegame();
                } else if (state == LoadSaveState.LOADFROMDRIVE || state == LoadSaveState.SAVETODRIVE)
                    state = LoadSaveState.NONE;
                else if (state == LoadSaveState.EXPORTSAVE || state == LoadSaveState.IMPORTSAVE)
                    state = LoadSaveState.NONE;
                show(isSave);
            }
        });
        stage.addActor(rightButton);

        leftButton = new TextButton(StringDB.getString("YES", true), styleSmall);
        rect = GameConstants.coordsToRelative(250, 130, 160, 35);
        leftButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        leftButton.setVisible(false);
        leftButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (((TextButton) event.getListenerActor()).isDisabled())
                    return;

                if (state == LoadSaveState.OVERWRITE) {
                    manager.getUniverseMap().saveUniverse(saveName + ".sav");
                    rightButton.setText(StringDB.getString("SAVE", true));
                    state = LoadSaveState.NONE;
                    leftButton.setVisible(false);
                    overWriteLabel.setVisible(false);
                } else if (state == LoadSaveState.SAVETODRIVE || state == LoadSaveState.LOADFROMDRIVE) {
                    DriveIntegration di = manager.getPlatformApiIntegration().getDriveIntegration();
                    if (di != null)
                        if (state == LoadSaveState.SAVETODRIVE)
                            di.writeSavesToDrive(saveInfos);
                        else
                            di.readSavesFromDrive();
                    state = LoadSaveState.NONE;
                } else if (state == LoadSaveState.EXPORTSAVE || state == LoadSaveState.IMPORTSAVE) {
                    StorageIntegration si = manager.getPlatformApiIntegration().getStorageIntegration();
                    if (si != null) {
                        if (state == LoadSaveState.IMPORTSAVE)
                            si.importSaves();
                        else
                            si.exportSaves(saveInfos);
                        state = LoadSaveState.NONE;
                    }
                } else if (state != LoadSaveState.DELETE) {
                    state = LoadSaveState.DELETE;
                    overWriteLabel.setText(StringDB.getString("DELETE_SAVE") + " " + StringDB.getString("ARE_YOU_SURE"));
                    leftButton.setText(StringDB.getString("BTN_CANCEL", true));
                    rightButton.setText(StringDB.getString("YES", true));
                } else if (state == LoadSaveState.DELETE)
                    state = LoadSaveState.NONE;
                show(isSave);
            }
        });
        stage.addActor(leftButton);

        overWriteLabel = new Label(StringDB.getString("OVERWRITE_SAVE") + " " + StringDB.getString("ARE_YOU_SURE"), skin,
                "normalFont", normalColor);
        rect = GameConstants.coordsToRelative(410, 130, 500, 35);
        overWriteLabel.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset),
                (int) (saveScroller.getWidth() - leftButton.getWidth() - rightButton.getWidth()), (int) rect.height);
        overWriteLabel.setAlignment(Align.top);
        overWriteLabel.setVisible(false);
        overWriteLabel.setWrap(true);
        stage.addActor(overWriteLabel);

        saveToDriveButton = new TextButton(StringDB.getString("BACKUP_TO_DRIVE", true), styleSmall);
        rect = GameConstants.coordsToRelative(430, 60, 160, 35);
        saveToDriveButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        saveToDriveButton.setVisible(false);
        saveToDriveButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    state = LoadSaveState.SAVETODRIVE;
                    show(isSave);
                }
            }
        });
        stage.addActor(saveToDriveButton);

        loadFromDriveButton = new TextButton(StringDB.getString("RESTORE_FROM_DRIVE", true), styleSmall);
        rect = GameConstants.coordsToRelative(410, 60, 160, 35);
        rect.x = rightButton.getX() - rect.width - GameConstants.wToRelative(20);
        loadFromDriveButton.setBounds((int) rect.x, (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        loadFromDriveButton.setVisible(false);
        loadFromDriveButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    state = LoadSaveState.LOADFROMDRIVE;
                    show(isSave);
                }
            }
        });
        stage.addActor(loadFromDriveButton);

        exportSavesButton = new TextButton(StringDB.getString("EXPORT_SAVE", true), styleSmall);
        rect = GameConstants.coordsToRelative(250, 60, 160, 35);
        exportSavesButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        exportSavesButton.setVisible(false);
        exportSavesButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    state = LoadSaveState.EXPORTSAVE;
                    show(isSave);
                }
            }
        });
        stage.addActor(exportSavesButton);

        importSavesButton = new TextButton(StringDB.getString("IMPORT_SAVE", true), styleSmall);
        rect = GameConstants.coordsToRelative(250, 60, 160, 35);
        rect.x = rightButton.getX();
        importSavesButton.setBounds((int) rect.x, (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        importSavesButton.setVisible(false);
        importSavesButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (!((TextButton) event.getListenerActor()).isDisabled()) {
                    state = LoadSaveState.IMPORTSAVE;
                    show(isSave);
                }
            }
        });
        stage.addActor(importSavesButton);

        saveName = "";
        nextIndex = 0;
        loadedTextures = new Array<Texture>();
    }

    public void show(boolean save) {
        this.isSave = save;
        DriveIntegration di = manager.getPlatformApiIntegration().getDriveIntegration();
        if (di != null)
            di.setCallback(this);
        leftButton.setVisible(false);
        rightButton.setVisible(false);
        saveItems.clear();
        saveInfos.clear();
        initSavedFiles(save);

        saveScrollTable.clear();
        ButtonStyle bs = new ButtonStyle();
        Button btn = new Button(bs);
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                state = LoadSaveState.NONE;

                Button btn = (Button) event.getListenerActor();
                markSaveListSelected(btn);
            }
        };
        Label l = new Label(StringDB.getString("EMPTY_SAVE"), skin, "normalFont", Color.WHITE);
        l.setColor(normalColor);
        btn.add(l);
        float btnHeight = GameConstants.hToRelative(160);
        btn.setUserObject(l);
        l.setUserObject("");
        btn.addListener(gestureListener);
        float width = (saveScroller.getWidth() - saveScroller.getStyle().vScrollKnob.getMinWidth()) / 2;
        if (isSave) {
            saveItems.add(btn);
            saveScrollTable.add(btn).width(width).height(btnHeight);
        }
        for (int i = 0; i < saveInfos.size; i++) {
            SaveInfo si = saveInfos.get(i);
            bs = new ButtonStyle();
            if ((i % 2 != 0 && isSave) || (!isSave && i % 2 == 0))
                saveScrollTable.row();
            btn = new Button(bs);
            bs = new ButtonStyle();
            Button smallBtn = new Button(bs);
            l = new Label(si.fileName, skin, "normalFont", Color.WHITE);
            l.setColor(normalColor);
            l.setUserObject(si.fileName);
            btn.setUserObject(l);
            smallBtn.add(l);
            smallBtn.row();
            String text = Major.toMajor(raceCtrl.getRace(si.majorID)).getEmpireName();
            l = new Label(text, skin, "normalFont", Color.WHITE);
            l.setAlignment(Align.center);
            l.setWrap(true);
            l.setColor(normalColor);
            smallBtn.add(l).width(width - GameConstants.wToRelative(200));
            smallBtn.row();
            text = StringDB.getString("ROUND") + ": " + si.currentTurn + " - " + si.difficulty.getName();
            l = new Label(text, skin, "normalFont", Color.WHITE);
            l.setAlignment(Align.center);
            l.setWrap(true);
            l.setColor(normalColor);
            smallBtn.add(l).width(width - GameConstants.wToRelative(200));
            smallBtn.row();
            text = "Map " + si.shape + " size: " + si.width + " x " + si.height;
            l = new Label(text, skin, "normalFont", Color.WHITE);
            l.setColor(normalColor);
            smallBtn.add(l);
            smallBtn.row();
            text = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(si.date);
            l = new Label(text, skin, "normalFont", Color.WHITE);
            l.setColor(normalColor);
            smallBtn.add(l);
            String imgPath = GameConstants.getSaveLocation() + si.fileName + ".png";
            FileHandle fh = new FileHandle(imgPath);
            if (fh.exists()) {
                try {
                    Texture tex = new Texture(fh, manager.useMipMaps());
                    tex.setFilter(manager.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear,
                            TextureFilter.Linear);
                    btn.add(new Image(tex)).width(GameConstants.wToRelative(200)).height(GameConstants.hToRelative(150));
                    loadedTextures.add(tex);
                } catch (GdxRuntimeException e) {//if we can't load it, no problem
                    e.printStackTrace();
                }
            }
            btn.add(smallBtn);
            btn.addListener(gestureListener);
            saveItems.add(btn);
            saveScrollTable.add(btn).width(width).height(btnHeight);
        }
        saveScroller.setVisible(true);
        if (manager.getPlatformApiIntegration().getPlatformType() == PlatformType.PlatformGoogle
                && manager.getPlatformApiIntegration().isConnected() && manager.getPlatformApiIntegration().networkAvailable()) {
            saveToDriveButton.setVisible(true);
            loadFromDriveButton.setVisible(true);
        }

        if (Gdx.app.getType() == ApplicationType.Android) {
            importSavesButton.setVisible(true);
            exportSavesButton.setVisible(true);
        }

        stage.setScrollFocus(saveScroller);
        stage.draw();
        btn = null;
        for (Button b : saveItems) {
            if (saveName.equals(getSaveName(b))) {
                btn = b;
            }
        }
        if (btn == null) {
            if (saveItems.size > 0) {
                btn = saveItems.get(0);
                saveName = getSaveName(btn);
            }
        }
        if (btn != null)
            markSaveListSelected(btn);
        else
            enableButtonsIfNeeeded();
    }

    public void hide() {
        saveScroller.setVisible(false);
        rightButton.setVisible(false);
        leftButton.setVisible(false);
        overWriteLabel.setVisible(false);
        for (Texture tex : loadedTextures) {
            tex.dispose();
        }
        loadedTextures.clear();
        saveToDriveButton.setVisible(false);
        loadFromDriveButton.setVisible(false);
        exportSavesButton.setVisible(false);
        importSavesButton.setVisible(false);
    }

    private void initSavedFiles(boolean save) {
        FileHandle dirHandle = new FileHandle(GameConstants.getSaveLocation());
        if (!dirHandle.exists())
            dirHandle.mkdirs();
        for (FileHandle entry : dirHandle.list()) {
            if (!entry.isDirectory() && entry.extension().equals("sav")) {
                if ((isSave && entry.nameWithoutExtension().startsWith("bote")) || !isSave) {
                    SaveInfo info = new SaveInfo();
                    info.fileName = entry.nameWithoutExtension();
                    Date d = new Date(entry.file().lastModified());
                    boolean isValid = false;
                    info.date = d;
                    try {
                        Input input = new Input(new InflaterInputStream(new FileInputStream(GameConstants.getSaveLocation()
                                + entry.name())));
                        Kryo kryo = manager.getKryo();
                        GamePreferences prefs = kryo.readObject(input, GamePreferences.class);
                        if (save
                                || (prefs.saveGameVersion() == manager.getGamePreferences().saveGameVersion() || (GameConstants.SmallestCombatibleSave <= prefs
                                        .saveGameVersion())))
                            isValid = true;
                        info.majorID = input.readString();
                        info.currentTurn = input.readInt();
                        if (prefs.saveGameVersion() > 8)
                            info.date = kryo.readObject(input, Date.class);
                        input.close();
                        info.height = prefs.gridSizeY;
                        info.width = prefs.gridSizeX;
                        info.shape = prefs.generateFromShape.getName();
                        info.difficulty = prefs.difficulty;
                    } catch (IOException e) {
                        System.out.println("Error reading savegame: " + entry.name());
                        e.printStackTrace();
                    } catch (KryoException e) {
                        System.out.println("Error reading savegame: " + entry.name());
                    }
                    if (isValid)
                        saveInfos.add(info);
                }
            }
        }
        SaveInfo.sortByName = true;
        saveInfos.sort();
        if (saveInfos.size != 0) {
            nextIndex = Integer.parseInt(saveInfos.get(saveInfos.size - 1).fileName.substring(4, 8));
            for (int i = 0; i < saveInfos.size; i++) {
                if (saveInfos.get(i).fileName.startsWith("bote"))
                    if (i != 0 && i != Integer.parseInt(saveInfos.get(i - 1).fileName.substring(4, 8))
                            && saveInfos.get(i - 1).fileName.startsWith("bote")) {
                        nextIndex = i - 1;
                        break;
                    }
            }
        } else
            nextIndex = 0;
        SaveInfo.sortByName = false;
        saveInfos.sort();
        saveInfos.reverse();
    }

    private boolean loadSavegame() {
        try {
            manager.uninit();
            Input input = new Input(new InflaterInputStream(new FileInputStream(GameConstants.getSaveLocation() + saveName
                    + ".sav")));
            Kryo kryo = manager.getKryo();
            GamePreferences prefs = kryo.readObject(input, GamePreferences.class);
            String race = input.readString();
            input.close();
            //reload stuff
            if (!race.equals(manager.getRaceController().getPlayerRaceString()))
                manager.unLoadBackgounds();
            manager.clear();
            manager.getRaceController().reInit();
            manager.getRaceController().setPlayerRace(race);
            manager.setGamePreferences(prefs);
            manager.setGameLoaded(saveName + ".sav");
            manager.createUniverse();
            manager.setView(ViewTypes.LOADING_SCREEN, true);
            return true;
        } catch (IOException e) {
            System.out.println("Error reading savegame: ");
            e.printStackTrace();
        } catch (KryoException e) {
            System.out.println("Error reading savegame");
            e.printStackTrace();
        }
        return false;
    }

    private void deleteSavegame() {
        state = LoadSaveState.NONE;
        FileHandle fileHandle = new FileHandle(GameConstants.getSaveLocation() + saveName + ".sav");
        fileHandle.delete();
        fileHandle = new FileHandle(GameConstants.getSaveLocation() + saveName + ".png");
        if (fileHandle.exists())
            fileHandle.delete();
        saveName = "";
    }

    private void markSaveListSelected(Button b) {
        if (saveSelection != null) {
            saveSelection.getStyle().up = null;
            saveSelection.getStyle().down = null;
            getLabel(saveSelection).setColor(oldColor);
        }
        if (b == null)
            b = saveSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(getLabel(b).getColor());
        getLabel(b).setColor(markColor);
        saveSelection = b;
        saveName = (String) getSaveName(b);
        enableButtonsIfNeeeded();
    }

    private void enableButtonsIfNeeeded() {
        overWriteLabel.setVisible(false);
        if (state == LoadSaveState.DELETE) {
            rightButton.setText(StringDB.getString("YES", true));
            rightButton.setVisible(true);
            overWriteLabel.setText(StringDB.getString("DELETE_SAVE") + " " + StringDB.getString("ARE_YOU_SURE"));
            overWriteLabel.setVisible(true);
            leftButton.setText(StringDB.getString("NO", true));
            leftButton.setVisible(true);
        } else if (state == LoadSaveState.OVERWRITE) {
            overWriteLabel.setText(StringDB.getString("OVERWRITE_SAVE") + " " + StringDB.getString("ARE_YOU_SURE"));
            overWriteLabel.setVisible(true);
            rightButton.setText(StringDB.getString("BTN_CANCEL", true));
            rightButton.setVisible(true);
            leftButton.setText(StringDB.getString("YES", true));
            leftButton.setVisible(true);
        } else if (state == LoadSaveState.LOADFROMDRIVE || state == LoadSaveState.SAVETODRIVE) {
            String text = state == LoadSaveState.LOADFROMDRIVE ? StringDB.getString("OVERWRITE_LOCAL") : StringDB
                    .getString("OVERWRITE_REMOTE");
            overWriteLabel.setText(text + " " + StringDB.getString("ARE_YOU_SURE"));
            overWriteLabel.setVisible(true);
            rightButton.setText(StringDB.getString("BTN_CANCEL", true));
            rightButton.setVisible(true);
            leftButton.setText(StringDB.getString("YES", true));
            leftButton.setVisible(true);
        } else if (state == LoadSaveState.EXPORTSAVE || state == LoadSaveState.IMPORTSAVE) {
            String text = state == LoadSaveState.IMPORTSAVE ? StringDB.getString("OVERWRITE_INTERNAL") : StringDB
                    .getString("OVERWRITE_EXTERNAL");
            overWriteLabel.setText(text + " " + StringDB.getString("ARE_YOU_SURE"));
            overWriteLabel.setVisible(true);
            rightButton.setText(StringDB.getString("BTN_CANCEL", true));
            rightButton.setVisible(true);
            leftButton.setText(StringDB.getString("YES", true));
            leftButton.setVisible(true);
        } else {
            if (!saveName.isEmpty()) {
                leftButton.setText(StringDB.getString("DELETE", true));
                leftButton.setVisible(true);
            } else
                leftButton.setVisible(false);
            if (isSave) {
                rightButton.setText(StringDB.getString("SAVE", true));
                rightButton.setVisible(true);
            } else {
                rightButton.setText(StringDB.getString("LOAD", true));
                rightButton.setVisible(!saveName.isEmpty());
            }
        }
    }

    private String getSaveName(Button b) {
        return (String) getLabel(b).getUserObject();
    }

    private Label getLabel(Button b) {
        return (Label) b.getUserObject();
    }

    @Override
    public void signalBusy(final boolean busy) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                overWriteLabel.setText(StringDB.getString("CLOUD_BUSY"));
                saveToDriveButton.setDisabled(busy);
                loadFromDriveButton.setDisabled(busy);
                leftButton.setDisabled(busy);
                rightButton.setDisabled(busy);
                exportSavesButton.setDisabled(busy);
                importSavesButton.setDisabled(busy);
                show(isSave);
                //show hides the label
                overWriteLabel.setVisible(busy);
            }
        });
    }
}