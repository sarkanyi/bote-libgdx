/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.TradeScreen;

public class TradeMonopolView {
    private ScreenManager manager;
    private Skin skin;
    private Table nameTable;
    private Table infoTable;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private double[] monopolCosts;

    public TradeMonopolView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.add(StringDB.getString("MONOPOLY_MENUE"), "hugeFont", normalColor);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(50, 515, 1100, 440);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.center);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);
        monopolCosts = new double[5];
    }

    public void show() {
        nameTable.setVisible(true);
        infoTable.clear();
        Arrays.fill(monopolCosts, 0.0);
        float buttonWidth = GameConstants.wToRelative(120);
        float buttonHeight = GameConstants.hToRelative(30);
        float height = GameConstants.hToRelative(91);
        float width = (infoTable.getWidth() - buttonWidth) / 4;
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            String resName = StringDB.getString(ResourceTypes.fromResourceTypes(i).getName());
            if (i != 0)
                infoTable.row();
            String text = String.format("%s %s", StringDB.getString("MONOPOLY_OWNER"), resName);
            Label l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.topLeft);
            infoTable.add(l).width((int) width).height((int) height);
            text = "";
            if (manager.getMonopolOwner(i).isEmpty())
                text = StringDB.getString("NOBODY");
            else if (manager.getMonopolOwner(i).equals(playerRace.getRaceId())
                    || playerRace.isRaceContacted(manager.getMonopolOwner(i))) {
                Race monopolOwner = manager.getRaceController().getRace(manager.getMonopolOwner(i));
                if (monopolOwner != null)
                    text = monopolOwner.getName();
            } else
                text = StringDB.getString("UNKNOWN");
            l = new Label(text, skin, "normalFont", markColor);
            l.setAlignment(Align.top);
            infoTable.add(l).width((int) width).height((int) height);
            text = String.format("%s %s", StringDB.getString("MONOPOLY_COSTS"), resName);
            l = new Label(text, skin, "normalFont", normalColor);
            l.setAlignment(Align.top);
            infoTable.add(l).width((int) width).height((int) height);
            //the monopol costs are the inhabitants number of all our systems and that of the known races times 15 times i+1
            for (int j = 0; j < manager.getUniverseMap().getStarSystems().size; j++) {
                StarSystem ss = manager.getUniverseMap().getStarSystems().get(j);
                if (ss.isMajorized()
                        && (ss.getOwnerId().equals(playerRace.getRaceId()) || playerRace.isRaceContacted(ss
                                .getOwnerId())))
                    monopolCosts[i] += ss.getInhabitants();
            }
            //if we bought it already or are trying to buy it in this round
            if (manager.getMonopolOwner(i).equals(playerRace.getRaceId())
                    || playerRace.getTrade().getMonopolBuying()[i] != 0.0)
                monopolCosts[i] = 0.0;
            else if (!manager.getMonopolOwner(i).isEmpty()) //if someone else has it, double the cost
                monopolCosts[i] *= 2;
            monopolCosts[i] *= 15 * (i + 1);

            ///SPECIAL RESEARCH
            int bonus = playerRace.getEmpire().getResearch().getResearchInfo()
                    .isResearchedThenGetBonus(ResearchComplexType.TRADE, 2);
            if (bonus != 0)
                monopolCosts[i] -= monopolCosts[i] * bonus / 100;

            text = String.format("%.0f %s", monopolCosts[i], StringDB.getString("CREDITS"));
            l = new Label(text, skin, "normalFont", markColor);
            l.setAlignment(Align.topLeft);
            infoTable.add(l).width((int) width).height((int) height);
            if (monopolCosts[i] != 0.0) {
                TextButton.TextButtonStyle smallButtonStyle = new TextButton.TextButtonStyle(skin.get("small-buttons",
                        TextButton.TextButtonStyle.class));
                TextButton buyButton = new TextButton(StringDB.getString("BTN_BUY"), smallButtonStyle);
                buyButton.setUserObject(i);
                buyButton.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        int res = (Integer) event.getListenerActor().getUserObject();
                        if (monopolCosts[res] != 0.0)
                            if (playerRace.getEmpire().getCredits() >= monopolCosts[res]) {
                                playerRace.getTrade().setMonopolBuying(res, monopolCosts[res]);
                                playerRace.getEmpire().setCredits((-1) * (int) monopolCosts[res]);
                                show();
                                ((TradeScreen) manager.getScreen()).getLeftSideBar().showInfo();
                            }
                    }
                });
                infoTable.add(buyButton).width((int) buttonWidth).height((int) buttonHeight).align(Align.top);
            } else
                infoTable.add().width((int) buttonWidth).height((int) buttonHeight);
        }
        infoTable.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        infoTable.setVisible(false);
    }
}
