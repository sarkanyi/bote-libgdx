/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.trade.Trade.TradeStruct;

public class TradeTransferView {
    private Skin skin;
    private StarSystem starSystem;
    private Color normalColor;
    private Color markColor;
    private Major playerRace;
    private Table nameTable;
    private Table systemInfo;
    private Table transferInfo;

    public TradeTransferView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.add(StringDB.getString("TRADE_TRANSFER_MENUE"), "hugeFont", normalColor);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        systemInfo = new Table();
        rect = GameConstants.coordsToRelative(200, 540, 800, 25);
        systemInfo.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        systemInfo.align(Align.center);
        systemInfo.setSkin(skin);
        stage.addActor(systemInfo);
        systemInfo.setVisible(false);

        transferInfo = new Table();
        rect = GameConstants.coordsToRelative(195, 428, 800, 280);
        transferInfo.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        transferInfo.align(Align.center);
        transferInfo.setSkin(skin);
        stage.addActor(transferInfo);
        transferInfo.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);
        String text = StringDB.getString("TRANSFERS_NEXT_ROUND", false, starSystem.getName());
        systemInfo.clear();
        systemInfo.add(text, "largeFont", markColor);
        systemInfo.setVisible(true);

        long boughtResPrice[] = new long[5];
        Arrays.fill(boughtResPrice, 0);
        long boughtResNumber[] = new long[5];
        Arrays.fill(boughtResNumber, 0);
        long selledResPrice[] = new long[5];
        Arrays.fill(selledResPrice, 0);
        long selledResNumber[] = new long[5];
        Arrays.fill(selledResNumber, 0);

        for (int i = 0; i < playerRace.getTrade().getTradeActions().size; i++) {
            TradeStruct ta = playerRace.getTrade().getTradeActions().get(i);
            if (ta.system.equals(starSystem.getCoordinates())) {
                int res = ta.res;
                if (ta.price > 0) {
                    boughtResNumber[res] += ta.number;
                    boughtResPrice[res] += ta.price;
                } else {
                    selledResNumber[res] += ta.number;
                    selledResPrice[res] += ta.price;
                }
            }
        }

        float height = GameConstants.hToRelative(58);
        float width = transferInfo.getWidth() / 3;
        transferInfo.clear();
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            if (i != 0)
                transferInfo.row();
            text = "";
            if (boughtResPrice[i] > 0)
                text = String.format("%d %s %d %s", boughtResNumber[i], StringDB.getString("UNITS_FOR"),
                        (int) (boughtResPrice[i] * playerRace.getTrade().getTax()), StringDB.getString("CREDITS"));
            Label l = new Label(text, skin, "mediumFont", normalColor);
            l.setAlignment(Align.left);
            transferInfo.add(l).width((int) width).height((int) height);
            text = StringDB.getString(ResourceTypes.fromResourceTypes(i).getName());
            l = new Label(text, skin, "mediumFont", markColor);
            l.setAlignment(Align.center);
            transferInfo.add(l).width((int) width).height((int) height);
            text = "";
            if (selledResPrice[i] < 0)
                text = String.format("%d %s %d %s", selledResNumber[i], StringDB.getString("UNITS_FOR"),
                        -selledResPrice[i], StringDB.getString("CREDITS"));
            l = new Label(text, skin, "mediumFont", normalColor);
            l.setAlignment(Align.right);
            transferInfo.add(l).width((int) width).height((int) height);
        }
        transferInfo.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        systemInfo.setVisible(false);
        transferInfo.setVisible(false);
    }

    public void setStarSystem(StarSystem ss) {
        this.starSystem = ss;
    }
}
