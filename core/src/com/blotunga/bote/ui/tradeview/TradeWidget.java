/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.TradeScreen;

public class TradeWidget {
    private ResourceManager manager;
    private Skin skin;
    private Major playerRace;
    private Table widgetTable;
    private ResourceTypes type;
    private Color normalColor;
    private Color markColor;
    private StarSystem starSystem;

    public TradeWidget(Rectangle rect, ResourceManager manager, Stage stage, Skin skin, ResourceTypes rt) {
        this.manager = manager;
        this.skin = skin;
        this.type = rt;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        widgetTable = new Table();
        widgetTable.setSkin(skin);
        widgetTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        widgetTable.setVisible(false);
        widgetTable.align(Align.top);
        stage.addActor(widgetTable);
    }

    public void show() {
        widgetTable.clear();
        widgetTable.setVisible(true);
        float height = GameConstants.hToRelative(25);
        widgetTable.add(StringDB.getString(type.getName()), "mediumFont", normalColor).height((int) height);
        widgetTable.row();
        height = GameConstants.hToRelative(40);
        widgetTable.add(StringDB.getString("CURRENT_PRICE"), "mediumFont", markColor).height((int) height);
        widgetTable.row();
        int price = (int) ((playerRace.getTrade().getResourcePrice()[type.getType()] * playerRace.getTrade().getTax()) / 10);
        String text = String.format("%d %s", price, StringDB.getString("CREDITS"));
        height = GameConstants.hToRelative(28);
        widgetTable.add(text, "mediumFont", normalColor).height((int) height);
        widgetTable.row();
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        TextButton buyButton = new TextButton(StringDB.getString("BTN_BUY"), smallButtonStyle);
        buyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (starSystem.getBlockade() > 0)
                    return;
                int costs = playerRace.getTrade().buyResource(type.getType(), playerRace.getTrade().getQuantity(),
                        starSystem.getCoordinates(), playerRace.getEmpire().getCredits());
                if (costs != 0) {
                    playerRace.getEmpire().setCredits(-costs);
                    manager.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_SHIPTARGET);
                    show();
                    ((TradeScreen) manager.getScreen()).getLeftSideBar().showInfo();
                }
            }
        });
        float padTop = GameConstants.hToRelative(8);
        widgetTable.add(buyButton).height(height).width(widgetTable.getWidth()).spaceTop(padTop);
        widgetTable.row();
        TextButton sellButton = new TextButton(StringDB.getString("BTN_SELL"), smallButtonStyle);
        sellButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (starSystem.getBlockade() > 0)
                    return;
                if (starSystem.getResourceStore(type.getType()) >= playerRace.getTrade().getQuantity()) {
                    playerRace.getTrade().sellResource(type.getType(), playerRace.getTrade().getQuantity(),
                            starSystem.getCoordinates());
                    starSystem.addResourceStore(type.getType(), -playerRace.getTrade().getQuantity());
                    manager.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_SHIPTARGET);
                    show();
                }
            }
        });
        padTop = GameConstants.hToRelative(5);
        widgetTable.add(sellButton).height(height).width(widgetTable.getWidth()).spaceTop(padTop);
        widgetTable.row();
        height = GameConstants.hToRelative(32);
        widgetTable.add(StringDB.getString("STORAGE_QUANTUM"), "mediumFont", markColor).height((int) height);
        widgetTable.row();
        text = String.format("%d %s", starSystem.getResourceStore(type.getType()), StringDB.getString("UNITS"));
        height = GameConstants.hToRelative(30);
        widgetTable.add(text, "mediumFont", normalColor).height((int) height);
        widgetTable.row();
        widgetTable.add(StringDB.getString("MONOPOLIST"), "mediumFont", markColor).height((int) height);
        widgetTable.row();
        height = GameConstants.hToRelative(30);
        if (manager.getMonopolOwner(type.getType()).isEmpty())
            text = StringDB.getString("NOBODY");
        else if (manager.getMonopolOwner(type.getType()).equals(playerRace.getRaceId())
                || playerRace.isRaceContacted(manager.getMonopolOwner(type.getType())))
            text = manager.getRaceController().getRace(manager.getMonopolOwner(type.getType())).getName();
        else
            text = StringDB.getString("UNKNOWN");
        widgetTable.add(text, "mediumFont", normalColor).height((int) height);
    }

    public void hide() {
        widgetTable.setVisible(false);
    }

    public void setStarSystem(StarSystem starSystem) {
        this.starSystem = starSystem;
    }
}
