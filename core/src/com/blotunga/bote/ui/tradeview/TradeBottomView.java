/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.trade.TradeHistory;
import com.blotunga.bote.ui.OverlayBanner;

public class TradeBottomView {
    private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private float xOffset;
    private ResourceTypes selectedResource;
    private int numHistoryRounds;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table infoTable;
    private Array<Image> lines;
    private Table maxPrice;
    private TextButton[] buttons;
    private Array<OverlayBanner> banners;
    private StarSystem starSystem;

    public TradeBottomView(ScreenManager manager, Stage stage, Skin skin, float xOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = skin;
        this.xOffset = xOffset;
        selectedResource = ResourceTypes.TITAN;
        numHistoryRounds = 20;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        infoTable = new Table();
        infoTable.align(Align.center);
        Rectangle rect = GameConstants.coordsToRelative(50, 170, 210, 135);
        infoTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(infoTable);
        infoTable.setVisible(false);

        lines = new Array<Image>();

        maxPrice = new Table();
        maxPrice.setSkin(skin);
        rect = GameConstants.coordsToRelative(298, 165, 25, 20);
        maxPrice.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(maxPrice);
        maxPrice.setVisible(false);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        buttons = new TextButton[5];
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            ResourceTypes type = ResourceTypes.fromResourceTypes(i);
            rect = GameConstants.coordsToRelative(1060, 172 - (i * 30), 120, 30);
            String resName = StringDB.getString(type.getName());
            buttons[i] = new TextButton(resName, smallButtonStyle);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(type);
            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    selectedResource = (ResourceTypes) event.getListenerActor().getUserObject();
                    show();
                }
            });
            stage.addActor(buttons[i]);
        }
        banners = new Array<OverlayBanner>();
    }

    public void show() {
        TradeHistory hist = playerRace.getTrade().getTradeHistory();
        drawInfoTable(hist);
        for (Image line : lines) {
            line.setVisible(false);
            line.remove();
        }
        lines.clear();
        Color lineColor;
        switch (selectedResource) {
            case TITAN:
                lineColor = new Color(186 / 255.0f, 186 / 255.0f, 186 / 255.0f, 1.0f);
                break;
            case DEUTERIUM:
                lineColor = new Color(255 / 255.0f, 81 / 255.0f, 90 / 255.0f, 1.0f);
                break;
            case DURANIUM:
                lineColor = new Color(132 / 255.0f, 198 / 255.0f, 127 / 255.0f, 1.0f);
                break;
            case CRYSTAL:
                lineColor = new Color(153 / 255.0f, 227 / 255.0f, 255 / 255.0f, 1.0f);
                break;
            case IRIDIUM:
                lineColor = new Color(255 / 255.0f, 189 / 255.0f, 76 / 255.0f, 1.0f);
                break;
            default:
                lineColor = Color.BLACK;
                break;
        }
        int start;
        int end;
        int backupNumHistoryRounds = numHistoryRounds;
        if ((manager.getCurrentRound() - numHistoryRounds + 1) < 1) {
            start = 1;
            end = manager.getCurrentRound();
            if (end > start)
                numHistoryRounds = end - 1;
        } else {
            start = manager.getCurrentRound() - numHistoryRounds + 1;
            end = manager.getCurrentRound();
        }
        int max = hist.getMaxPrice(selectedResource.getType(), start - 1, end - 1);
        String text;
        maxPrice.setVisible(true);
        if (max / 10 == 0)
            text = "1";
        else
            text = "" + max / 10;
        maxPrice.clear();
        maxPrice.add(text, "mediumFont", markColor);
        int count = 0;
        for (int i = start; i <= end; i++) {
            float temp1 = (float) hist.getHistoryPricesFromRes(selectedResource.getType()).get(i - 1) / max;
            if (count > 0) {
                float temp2 = (float) hist.getHistoryPricesFromRes(selectedResource.getType()).get(i - 2) / max;
                float x1 = GameConstants.wToRelative(335 + (count - 1) * (600 / numHistoryRounds));
                float x2 = GameConstants.wToRelative(335 + (count) * (600 / numHistoryRounds));
                float y1 = GameConstants.hToRelative(155 * temp2);
                float y2 = GameConstants.hToRelative(155 * temp1);
                Image line = ((DefaultScreen) manager.getScreen()).drawLine(x1 + xOffset, x2 + xOffset, y1, y2, 1,
                        lineColor);
                stage.addActor(line);
                lines.add(line);
            }
            count++;
        }
        numHistoryRounds = backupNumHistoryRounds;
        for (TextButton b : buttons)
            b.setVisible(true);
    }

    private void drawInfoTable(TradeHistory hist) {
        infoTable.clear();
        float height = infoTable.getHeight() / 5;
        float width = infoTable.getWidth() / 2;
        String resName = StringDB.getString(selectedResource.getName());
        Label l = new Label(resName, skin, "mediumFont", markColor);
        l.setAlignment(Align.center);
        infoTable.add(l).height((int) height);
        infoTable.row();
        ButtonStyle bs = new ButtonStyle();
        Button button = new Button(bs);
        String text = StringDB.getString("MIN_PRICE");
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.left);
        button.add(l).height((int) height).width((int) width);
        if (hist.getMinPrice(selectedResource.getType()) / 10 == 0)
            text = "1";
        else
            text = "" + (hist.getMinPrice(selectedResource.getType()) / 10);
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.right);
        button.add(l).height((int) height).width((int) width);
        infoTable.add(button);
        infoTable.row();
        button = new Button(bs);
        text = StringDB.getString("MAX_PRICE");
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.left);
        button.add(l).height((int) height).width((int) width);
        if (hist.getMaxPrice(selectedResource.getType()) / 10 == 0)
            text = "1";
        else
            text = "" + (hist.getMaxPrice(selectedResource.getType()) / 10);
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.right);
        button.add(l).height((int) height).width((int) width);
        infoTable.add(button);
        infoTable.row();
        button = new Button(bs);
        text = StringDB.getString("AVERAGE_PRICE");
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.left);
        button.add(l).height((int) height).width((int) width);
        if (hist.getAveragePrice(selectedResource.getType()) / 10 == 0)
            text = "1";
        else
            text = "" + (hist.getAveragePrice(selectedResource.getType()) / 10);
        l = new Label(text, skin, "mediumFont", normalColor);
        l.setAlignment(Align.right);
        button.add(l).height((int) height).width((int) width);
        infoTable.add(button);
        infoTable.row();
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        TextButton numRoundsButton = new TextButton(String.format("%d %s", numHistoryRounds,
                StringDB.getString("ROUNDS")), smallButtonStyle);
        numRoundsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton b = (TextButton) event.getListenerActor();
                if (numHistoryRounds == 20)
                    numHistoryRounds = 50;
                else if (numHistoryRounds == 50)
                    numHistoryRounds = 100;
                else
                    numHistoryRounds = 20;
                b.setText(String.format("%d %s", numHistoryRounds, StringDB.getString("ROUNDS")));
                show();
            }
        });
        infoTable.add(numRoundsButton).height((int) height).width((int) width);
        infoTable.setVisible(true);
        for (OverlayBanner banner : banners)
            banner.remove();
        banners.clear();

        if (starSystem.getBlockade() > 0) {
            Rectangle rect = GameConstants.coordsToRelative(40, 172, 1140, 152);
            rect.x += xOffset;
            OverlayBanner blockadeBanner = new OverlayBanner(rect, StringDB.getString("SYSTEM_IS_BLOCKED", false, ""
                    + starSystem.getBlockade()), new Color(200 / 255.0f, 0, 0, 1.0f), (Texture) manager
                    .getAssetManager().get(GameConstants.UI_BG_SIMPLE), skin, "normalFont");
            stage.addActor(blockadeBanner);
            banners.add(blockadeBanner);
        }
    }

    public void hide() {
        infoTable.setVisible(false);
        for (Image line : lines)
            line.setVisible(false);
        maxPrice.setVisible(false);
        for (TextButton b : buttons)
            b.setVisible(false);
        for (OverlayBanner banner : banners)
            banner.remove();
        banners.clear();
    }

    public void setStarSystem(StarSystem ss) {
        this.starSystem = ss;
    }
}
