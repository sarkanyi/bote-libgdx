/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;

public class TradeEmptyMonopolView {
    private Table nameTable;
    private Table infoTable;

    public TradeEmptyMonopolView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        Major playerRace = manager.getRaceController().getPlayerRace();
        Color normalColor = playerRace.getRaceDesign().clrNormalText;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.add(StringDB.getString("MONOPOLY_MENUE"), "hugeFont", normalColor);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(300, 400, 600, 200);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.center);
        Label l = new Label(StringDB.getString("NO_MONOPOLY_BUY"), skin, "hugeFont", normalColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        infoTable.add(l).width(infoTable.getWidth());
        stage.addActor(infoTable);
        infoTable.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);
        infoTable.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        infoTable.setVisible(false);
    }
}
