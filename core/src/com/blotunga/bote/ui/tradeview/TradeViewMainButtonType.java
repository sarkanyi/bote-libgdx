/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import com.badlogic.gdx.utils.IntMap;

public enum TradeViewMainButtonType {
    EXCHANGE_BUTTON("BTN_STOCK_EXCHANGE", "trademenu", 0),
    MONOPOLY_BUTTON("BTN_MONOPOLY", "monopolmenu", 1),
    TRANSFERS_BUTTON("BTN_TRANSFERS", "tradetransfermenu", 2);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<TradeViewMainButtonType> intToTypeMap = new IntMap<TradeViewMainButtonType>();

    TradeViewMainButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (TradeViewMainButtonType tt : TradeViewMainButtonType.values()) {
            intToTypeMap.put(tt.ord, tt);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static TradeViewMainButtonType fromInt(int i) {
        TradeViewMainButtonType tt = intToTypeMap.get(i);
        return tt;
    }
}
