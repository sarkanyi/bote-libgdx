/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.StarSystem.SystemSortType;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;

public class EmpireSystemOverview {
    final private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private float xOffset;
    private Major playerRace;
    private TextButton[] buttons;
    private EmpireSystemOverviewFilterType selectedButton;
    private int numButtons = EmpireSystemOverviewFilterType.values().length;
    private Table nameTable;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private Table systemScrollTable;
    private ScrollPane systemScroller;
    private Table headerTable;
    private Button systemSelection = null;
    private Array<Button> systemItems;
    private StarSystem clickedSystem;
    private SystemSortType oldSortType = null;
    private boolean reverse;
    private boolean visible;

    private interface HeaderInterface {
        String getCaption();
        float getWidth();
        SystemSortType getSortType();
    }

    private enum GlobalHeader implements HeaderInterface {
        SECTOR("SECTOR", SystemSortType.BY_COORD, 60),
        NAME("NAME", SystemSortType.BY_NAME, 145);

        private String caption;
        private float width;
        private SystemSortType sortType;

        GlobalHeader(String caption, SystemSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public SystemSortType getSortType() {
            return sortType;
        }
    }

    private enum NormalHeader implements HeaderInterface {
        MORAL("MORAL", SystemSortType.BY_MORALE, 130),
        FOOD("FOOD", SystemSortType.BY_FOODPROD, 80),
        STORAGE("STORAGE", SystemSortType.BY_FOODSTORAGE, 100),
        INDUSTRY("INDUSTRY", SystemSortType.BY_IPPROD, 95),
        CREDITS("CREDITS", SystemSortType.BY_CREDITSPROD, 100),
        JOB("JOB", SystemSortType.BY_ORDER, 365);

        private String caption;
        private float width;
        private SystemSortType sortType;

        NormalHeader(String caption, SystemSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public SystemSortType getSortType() {
            return sortType;
        }
    }

    enum ResourceHeader implements HeaderInterface {
        TITAN("TITAN", SystemSortType.BY_TITANSTORE, 140),
        DEUTERIUM("DEUTERIUM", SystemSortType.BY_DEUTERIUMSTORE, 140),
        DURANIUM("DURANIUM", SystemSortType.BY_DURANIUMSTORE, 140),
        CRYSTAL("CRYSTAL", SystemSortType.BY_CRYSTALSTORE, 140),
        IRIDIUM("IRIDIUM", SystemSortType.BY_IRIDIUMSTORE, 140),
        DERITIUM("DERITIUM", SystemSortType.BY_DERITIUMSTORE, 170);

        private String caption;
        private float width;
        private SystemSortType sortType;

        ResourceHeader(String caption, SystemSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public SystemSortType getSortType() {
            return sortType;
        }
    }

    private enum DefenceHeader implements HeaderInterface {
        TROOPS("TROOPS", SystemSortType.BY_TROOPS, 150),
        SHIELDPOWER("SHIELDPOWER", SystemSortType.BY_SHIELD, 180),
        SHIPDEFEND("SHIPDEFEND", SystemSortType.BY_SHIPDEFENSE, 180),
        GROUNDDEFEND("GROUNDDEFEND", SystemSortType.BY_GROUNDDEFENSE, 180),
        SCANPOWER("SCANPOWER", SystemSortType.BY_SCANSTRENGTH, 180);

        private String caption;
        private float width;
        private SystemSortType sortType;

        DefenceHeader(String caption, SystemSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public SystemSortType getSortType() {
            return sortType;
        }
    }

    enum ExpansionHeader implements HeaderInterface {
        POPULATION("MAX_HABITANTS", 130),
        FOOD("FOOD", 95),
        ENERGY("ENERGY", 95),
        TITAN("TITAN", 95),
        DEUTERIUM("DEUTERIUM", 95),
        DURANIUM("DURANIUM", 95),
        CRYSTAL("CRYSTAL", 95),
        IRIDIUM("IRIDIUM", 95),
        DERITIUM("DERITIUM", 75);

        private String caption;
        private float width;

        ExpansionHeader(String caption, float width) {
            this.caption = caption;
            this.width = width;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption, true);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public SystemSortType getSortType() {
            return SystemSortType.BY_SECTORVALUE; //these sectors are sorted in priorities and should not be sorted again
        }
    }

    public EmpireSystemOverview(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;
        xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        selectedButton = EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_NORMAL;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(EmpireSystemOverviewFilterType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(60 + i * 180, 730, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(EmpireSystemOverviewFilterType.values()[i]);

            if (!needEnabled(EmpireSystemOverviewFilterType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        EmpireSystemOverviewFilterType selected = (EmpireSystemOverviewFilterType) event.getListenerActor()
                                .getUserObject();
                        setToSelection(selected);
                    }
                }
            });
            stage.addActor(buttons[i]);
        }
        for (TextButton button : buttons)
            button.setVisible(false);

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("SYSTEM_OVERVIEW_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        systemScrollTable = new Table();
        systemScrollTable.align(Align.topLeft);
        systemScrollTable.setTouchable(Touchable.childrenOnly);
        systemScroller = new ScrollPane(systemScrollTable, skin);
        systemScroller.setVariableSizeKnobs(false);
        systemScroller.setFadeScrollBars(false);
        systemScroller.setScrollbarsOnTop(true);
        systemScroller.setScrollingDisabled(true, false);
        stage.addActor(systemScroller);
        rect = GameConstants.coordsToRelative(50, 660, 1095, 560);
        systemScrollTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        systemScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        systemScroller.setVisible(false);

        headerTable = new Table();
        rect = GameConstants.coordsToRelative(60, 690, 1075, 25);
        headerTable.align(Align.left);
        headerTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(headerTable);
        headerTable.setVisible(false);

        systemItems = new Array<Button>();

        clickedSystem = null;
        visible = false;
    }

    public void show() {
        visible = true;
        nameTable.setVisible(true);

        float resImgWidth = GameConstants.wToRelative(20);
        float resImgPad = GameConstants.wToRelative(5);
        for (TextButton button : buttons) {
            button.setVisible(true);
        }
        headerTable.clear();
        headerTable.setVisible(true);
        Array<IntPoint> systemsList = playerRace.getEmpire().getSystemList();
        final Array<StarSystem> systems;

        if (selectedButton != EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION) {
            systems = new Array<StarSystem>();
            for (IntPoint coord : systemsList)
                systems.add(manager.getUniverseMap().getStarSystemAt(coord));
            systems.sort();
            if (reverse)
                systems.reverse();
        } else
            systems = manager.getUniverseMap().getExpansionSystems();

        ActorGestureListener headerListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (selectedButton != EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION) {
                    SystemSortType st = SystemSortType.BY_COORD;
                    Label l = (Label) event.getListenerActor();
                    st = ((HeaderInterface) l.getUserObject()).getSortType();
                    StarSystem.setSortType(st);
                    if (oldSortType != null && oldSortType == st)
                        reverse = !reverse;
                    else
                        reverse = false;
                    oldSortType = st;
                    show();
                }
            }
        };

        for (GlobalHeader gh : GlobalHeader.values()) {
            Label headerLabel = new Label(gh.getCaption(), skin, "largeFont", markColor);
            headerLabel.setUserObject(gh);
            headerLabel.addListener(headerListener);
            float width = gh.getWidth();
            if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_RESOURCE && gh == GlobalHeader.NAME)
                width += (resImgWidth + resImgPad);
            headerTable.add(headerLabel).width(width);
        }
        if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_NORMAL) {
            for (NormalHeader nh : NormalHeader.values()) {
                Label headerLabel = new Label(nh.getCaption(), skin, "largeFont", markColor);
                headerLabel.setUserObject(nh);
                headerLabel.addListener(headerListener);
                headerTable.add(headerLabel).width(nh.getWidth());
            }
        } else if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_RESOURCE) {
            for (ResourceHeader rh : ResourceHeader.values()) {
                Label headerLabel = new Label(rh.getCaption(), skin, "largeFont", markColor);
                headerLabel.setUserObject(rh);
                headerLabel.addListener(headerListener);
                headerTable.add(headerLabel).width(rh.getWidth());
            }
        } else if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_DEFENCE) {
            for (DefenceHeader dh : DefenceHeader.values()) {
                Label headerLabel = new Label(dh.getCaption(), skin, "largeFont", markColor);
                headerLabel.setUserObject(dh);
                headerLabel.addListener(headerListener);
                headerTable.add(headerLabel).width(dh.getWidth());
            }
        } else if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION) {
            for (ExpansionHeader ph : ExpansionHeader.values()) {
                Label headerLabel = new Label(ph.getCaption(), skin, "largeFont", markColor);
                headerLabel.setUserObject(ph);
                headerLabel.addListener(headerListener);
                headerTable.add(headerLabel).width(ph.getWidth());
            }
        }

        systemScrollTable.clear();
        systemSelection = null;
        systemItems.clear();
        systemScroller.setVisible(true);

        for (int i = 0; i < systems.size; i++) {
            StarSystem ss = systems.get(i);
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.align(Align.left);
            button.setSkin(skin);
            button.add().width(GameConstants.wToRelative(10));
            Array<Label> labels = new Array<Label>();
            for (GlobalHeader gh : GlobalHeader.values()) {
                String text = "";
                if (gh == GlobalHeader.SECTOR)
                    text = ss.getCoordinates().toString();
                else
                    text = ss.getName();
                Label label = new Label(text, skin, "normalFont", Color.WHITE);
                label.setColor(normalColor);
                button.add(label).width(gh.getWidth());
                labels.add(label);
            }

            if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_NORMAL) {
                for (NormalHeader nh : NormalHeader.values()) {
                    String text = "";
                    Color labelColor = new Color(normalColor);
                    boolean addToLabels = true;
                    switch (nh) {
                        case MORAL:
                            Pair<String, Color> p = getMoraleString(ss.getMorale());
                            text = p.getFirst();
                            labelColor = p.getSecond();
                            addToLabels = false;
                            break;
                        case FOOD:
                            int food = ss.getProduction().getFoodProd();
                            text += food;
                            if (food < 0)
                                labelColor = new Color(250 / 255.0f, 0, 0, 1);
                            else
                                labelColor = new Color(0, 250 / 255.0f, 0, 1);
                            addToLabels = false;
                            break;
                        case STORAGE:
                            text += ss.getFoodStore();
                            break;
                        case INDUSTRY:
                            text += ss.getProduction().getIndustryProd();
                            break;
                        case CREDITS:
                            text += ss.getProduction().getCreditsProd();
                            break;
                        case JOB:
                            text = ss.getProdText();
                            if (text.startsWith(StringDB.getString("AUTOBUILD"))) {
                                labelColor = new Color(Color.RED);
                                addToLabels = false;
                            }
                            break;
                    }
                    Label label = new Label(text, skin, "normalFont", Color.WHITE);
                    label.setColor(labelColor);
                    button.add(label).width(nh.getWidth());
                    if (addToLabels)
                        labels.add(label);
                }
            }

            if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_RESOURCE) {
                boolean dist[] = new boolean[ResourceTypes.DERITIUM.getType() + 1];
                Arrays.fill(dist, false);
                int lastID = -1;
                for (int l = 0; l < ss.getAllBuildings().size; l++) {
                    int id = ss.getAllBuildings().get(l).getRunningNumber();
                    if (id != lastID)
                        lastID = id;
                    else
                        continue;

                    BuildingInfo bi = manager.getBuildingInfo(id);
                    for (int k = ResourceTypes.TITAN.getType(); k <= ResourceTypes.DERITIUM.getType(); k++)
                        if (bi.getResourceDistributor(k))
                            dist[k] = true;
                }

                int k = 0;
                for (ResourceHeader nh : ResourceHeader.values()) {
                    String text = "";
                    Color labelColor = new Color(normalColor);
                    Image img = new Image();
                    if (dist[k]) {
                        TextureAtlas uiAtlas = manager.getAssetManager().get("graphics/ui/general_ui.pack");
                        TextureRegion tr = uiAtlas.findRegion(ResourceTypes.fromResourceTypes(k).getImgName());
                        img.setDrawable(new TextureRegionDrawable(tr));
                    }
                    text += ss.getResourceStore(k);
                    Label label = new Label(text, skin, "normalFont", Color.WHITE);
                    label.setColor(labelColor);
                    button.add(img).width(resImgWidth).spaceRight(resImgPad);
                    float bonusWidth = GameConstants.wToRelative(k == ResourceTypes.DERITIUM.getType() ? 100 : 40);
                    button.add(label).width(nh.getWidth() - resImgWidth - resImgPad - bonusWidth);
                    labels.add(label);
                    //bonuses from here
                    int bonus = 0;
                    for (Planet planet : ss.getPlanets())
                        if (planet.getBonuses()[k])
                            if (k != ResourceTypes.DERITIUM.getType())
                                bonus += (planet.getSize().getSize() + 1) * 25;
                            else
                                bonus += 1;
                    text = bonus > 0 && k != ResourceTypes.DERITIUM.getType() ? "+" : "";
                    text += bonus;
                    if (k != ResourceTypes.DERITIUM.getType())
                        text += "%";
                    label = new Label(text, skin, "normalFont", Color.WHITE);
                    Color color = new Color(0.0f, Math.min(1.0f, (175 + bonus) / 1.5f / 255.0f), 0.0f, 1.0f);
                    if (k == ResourceTypes.DERITIUM.getType())
                        color = new Color(Color.GREEN);
                    label.setColor(bonus > 0 ? color : labelColor);
                    label.setAlignment(k == ResourceTypes.DERITIUM.getType() ? Align.left : Align.right);
                    float padRight = GameConstants.wToRelative(5);
                    button.add(label).width(bonusWidth - padRight).padRight(padRight);
                    if (bonus == 0)
                        labels.add(label);
                    k++;
                }
            }

            if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_DEFENCE) {
                int shieldDefenceCount = 0;
                int shieldDefenceOnlineCount = 0;
                int shipDefenceCount = 0;
                int shipDefenceOnlineCount = 0;
                int groundDefenceCount = 0;
                int groundDefenceOnlineCount = 0;
                for (int l = 0; l < ss.getAllBuildings().size; l++) {
                    Building building = ss.getAllBuildings().get(l);
                    int id = building.getRunningNumber();
                    BuildingInfo bi = manager.getBuildingInfo(id);
                    if (bi.getShieldPower() > 0 || bi.getShieldPowerBonus() > 0) {
                        shieldDefenceCount++;
                        if (building.getIsBuildingOnline() || bi.getNeededEnergy() == 0 || bi.getAlwaysOnline())
                            shieldDefenceOnlineCount++;
                    }
                    if (bi.getShipDefend() > 0 || bi.getShipDefendBonus() > 0) {
                        shipDefenceCount++;
                        if (building.getIsBuildingOnline() || bi.getNeededEnergy() == 0 || bi.getAlwaysOnline())
                            shipDefenceOnlineCount++;
                    }
                    if (bi.getGroundDefend() > 0 || bi.getGroundDefendBonus() > 0) {
                        groundDefenceCount++;
                        if (building.getIsBuildingOnline() || bi.getNeededEnergy() == 0 || bi.getAlwaysOnline())
                            groundDefenceOnlineCount++;
                    }
                }

                for (DefenceHeader nh : DefenceHeader.values()) {
                    String text = "";
                    Color labelColor = new Color(normalColor);
                    switch (nh) {
                        case TROOPS:
                            text += ss.getTroops().size;
                            break;
                        case SHIELDPOWER:
                            text = String.format("%d (%d/%d)", ss.getProduction().getShieldPower(), shieldDefenceOnlineCount,
                                    shieldDefenceCount);
                            break;
                        case SHIPDEFEND:
                            text = String.format("%d (%d/%d)", ss.getProduction().getShipDefend(), shipDefenceOnlineCount,
                                    shipDefenceCount);
                            break;
                        case GROUNDDEFEND:
                            text = String.format("%d (%d/%d)", ss.getProduction().getGroundDefend(), groundDefenceOnlineCount,
                                    groundDefenceCount);
                            break;
                        case SCANPOWER:
                            text += ss.getProduction().getScanPower();
                            break;
                    }
                    Label label = new Label(text, skin, "normalFont", Color.WHITE);
                    label.setColor(labelColor);
                    button.add(label).width(nh.getWidth());
                    labels.add(label);
                }
            }

            if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION) {
                int k = 0;
                String text = "";
                Color labelColor = new Color(normalColor);
                boolean[] res = ss.getAvailableResources(false);
                boolean[] resColonized = ss.getAvailableResources(true);
                int[] bonuses = new int[8];
                int[] bonusColonized = new int[8];

                Arrays.fill(bonuses, 0);
                Arrays.fill(bonusColonized, 0);
                for (int j = 0; j < bonuses.length; j++) {
                    for (Planet planet : ss.getPlanets()) {
                        if (planet.getBonuses()[j]) {
                            if (j != ResourceTypes.DERITIUM.getType())
                                bonuses[j] += (planet.getSize().getSize() + 1) * 25;
                            else
                                ++bonuses[j];
                            if (planet.getIsInhabited()) {
                                if (j != ResourceTypes.DERITIUM.getType())
                                    bonusColonized[j] += (planet.getSize().getSize() + 1) * 25;
                                else
                                    ++bonusColonized[j];
                            }
                        }
                    }
                }

                for (ExpansionHeader ph : ExpansionHeader.values()) {
                    boolean addToLabels = true;
                    labelColor = new Color(normalColor);
                    if (ph == ExpansionHeader.POPULATION) {
                        float remainingPop = ss.getMaxInhabitants() - ss.getCurrentMaxInhabitants();
                        float percentage = 1.0f - remainingPop / ss.getMaxInhabitants();
                        text = "" + (int) (ss.getMaxInhabitants() * 1000) + " (" + (int) (percentage * 100) + "%)";
                        if (ss.getInhabitants() > 0)
                            labelColor = Color.GREEN;
                        else if (ss.isColonizable(playerRace.getRaceId()))
                            labelColor = Color.YELLOW;
                        else
                            labelColor = Color.RED;
                        addToLabels = false;
                    } else if (ph == ExpansionHeader.FOOD) {
                        int bonusRemaining = bonuses[6] - bonusColonized[6];
                        text = bonuses[6] + "(" + bonusRemaining + ")" + "%";
                        addToLabels = false;
                        if (bonuses[6] > 0) {
                            labelColor = Color.GREEN;
                            if (bonusColonized[6] < bonuses[6])
                                labelColor = new Color(labelColor.r * 0.36f, labelColor.g * 0.36f, labelColor.b * 0.36f, 1.0f);
                        } else if (!ss.isMajorized())
                            labelColor = new Color(labelColor.r * 0.6f, labelColor.g * 0.6f, labelColor.b * 0.6f, 1.0f);
                    } else if (ph == ExpansionHeader.ENERGY) {
                        int bonusRemaining = bonuses[7] - bonusColonized[7];
                        text = bonuses[7] + "(" + bonusRemaining + ")" + "%";
                        addToLabels = false;
                        if (bonuses[7] > 0) {
                            labelColor = Color.GREEN;
                            if (bonusColonized[7] < bonuses[7])
                                labelColor = new Color(labelColor.r * 0.36f, labelColor.g * 0.36f, labelColor.b * 0.36f, 1.0f);
                        } else if (!ss.isMajorized())
                            labelColor = new Color(labelColor.r * 0.6f, labelColor.g * 0.6f, labelColor.b * 0.6f, 1.0f);
                    } else {
                        addToLabels = false;
                        if (bonuses[k] > 0)
                            labelColor = Color.GREEN;
                        else if (!res[k])
                            labelColor = Color.RED;
                        if (!resColonized[k] && res[k])
                            labelColor = new Color(labelColor.r * 0.6f, labelColor.g * 0.6f, labelColor.b * 0.6f, 1.0f);
                        if (bonuses[k] > 0 && bonusColonized[k] < bonuses[k])
                            labelColor = new Color(labelColor.r * 0.6f, labelColor.g * 0.6f, labelColor.b * 0.6f, 1.0f);

                        int bonusRemaining = bonuses[k] - bonusColonized[k];
                        if (k != ResourceTypes.DERITIUM.getType())
                            text = bonuses[k] + "(" + bonusRemaining + ")" + "%";
                        else
                            text = "" + bonuses[k] + "(" + bonusRemaining + ")";

                        k++;
                    }
                    Label label = new Label(text, skin, "normalFont", Color.WHITE);
                    label.setColor(labelColor);
                    button.add(label).width(ph.getWidth());
                    if (addToLabels)
                        labels.add(label);
                }
            }

            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markSystemListSelected(b);
                    clickedSystem = getStarSystem(b);
                    if (count >= 2) {
                        manager.getUniverseMap().setSelectedCoordValue(clickedSystem.getCoordinates());
                        if (selectedButton != EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION)
                            manager.setView(ViewTypes.SYSTEM_VIEW);
                        else {
                            manager.setView(ViewTypes.GALAXY_VIEW);
                        }
                    }
                }

                public boolean longPress(Actor actor, float x, float y) {
                    Button b = (Button) actor;
                    markSystemListSelected(b);
                    clickedSystem = getStarSystem(b);
                    if (selectedButton == EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_NORMAL) {
                        boolean autoBuild = clickedSystem.getAutoBuild();
                        clickedSystem.setAutoBuild(!autoBuild);
                    }
                    show();
                    return true;
                }
            };

            gestureListener.getGestureDetector().setLongPressSeconds(1.0f);
            gestureListener.getGestureDetector().setTapCountInterval(0.6f);
            button.addListener(gestureListener);
            Pair<StarSystem, Array<Label>> pair = new Pair<StarSystem, Array<Label>>(ss, labels);
            button.setUserObject(pair);
            button.setName("" + i);
            button.add().width(GameConstants.wToRelative(10));
            systemScrollTable.add(button);
            systemScrollTable.row();
            systemItems.add(button);
        }

        stage.setKeyboardFocus(systemScrollTable);
        systemScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int limit = systemScrollTable.getCells().size;
                if (limit == 0)
                    return false;
                int selectedItem = Integer.parseInt(systemSelection.getName());
                if (limit < selectedItem)
                    selectedItem = limit - 1;
                Button b = systemScrollTable.findActor("" + selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = limit - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += systemScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= systemScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.A:
                        boolean autoBuild = clickedSystem.getAutoBuild();
                        clickedSystem.setAutoBuild(!autoBuild);
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                show();
                            }
                        });
                        return false;
                    case Keys.ENTER:
                        manager.getUniverseMap().setSelectedCoordValue(clickedSystem.getCoordinates());
                        if (selectedButton != EmpireSystemOverviewFilterType.EMPIREVIEW_SYSTEMS_EXPANSION)
                            manager.setView(ViewTypes.SYSTEM_VIEW);
                        else {
                            manager.setView(ViewTypes.GALAXY_VIEW);
                        }
                        return false;
                }

                if (EmpireSystemOverview.this.visible) {
                    if (selectedItem >= limit)
                        selectedItem = limit - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = systemScrollTable.findActor("" + selectedItem);
                    markSystemListSelected(b);
                    clickedSystem = getStarSystem(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        systemScrollTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        stage.setScrollFocus(systemScroller);
        stage.draw();
        Button btn = null;
        if (clickedSystem != null) {
            for (Button b : systemItems) {
                if (clickedSystem == getStarSystem(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (systemItems.size > 0) {
                btn = systemItems.get(0);
                clickedSystem = getStarSystem(btn);
            }
        }
        if (btn != null)
            markSystemListSelected(btn);
    }

    public void hide() {
        visible = false;
        nameTable.setVisible(false);
        for (TextButton button : buttons)
            button.setVisible(false);
        headerTable.setVisible(false);
        systemScroller.setVisible(false);
    }

    private boolean needEnabled(EmpireSystemOverviewFilterType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    public void setToSelection(EmpireSystemOverviewFilterType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(EmpireSystemOverviewFilterType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @SuppressWarnings("unchecked")
    private void markSystemListSelected(Button b) {
        if (systemSelection != null) {
            Pair<StarSystem, Array<Label>> p = (Pair<StarSystem, Array<Label>>) systemSelection.getUserObject();
            for (Label l : p.getSecond())
                l.setColor(oldColor);
            systemSelection.getStyle().up = null;
            systemSelection.getStyle().down = null;
        }
        if (b == null)
            b = systemSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        Pair<StarSystem, Array<Label>> p = (Pair<StarSystem, Array<Label>>) b.getUserObject();
        if (p.getSecond().size > 0) {
            oldColor = new Color(p.getSecond().get(0).getColor());
            for (Label l : p.getSecond())
                l.setColor(markColor);
        }

        systemSelection = b;

        float scrollerHeight = systemScroller.getScrollHeight();
        float scrollerPos = systemScroller.getScrollY();
        int selectedItem = Integer.parseInt(systemSelection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            systemScroller.setScrollY(b.getHeight() * selectedItem - systemScroller.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            systemScroller.setScrollY(b.getHeight() * (selectedItem - systemScroller.getScrollHeight() / b.getHeight() + 1));
    }

    @SuppressWarnings("unchecked")
    private StarSystem getStarSystem(Button b) {
        Pair<StarSystem, Array<Label>> p = (Pair<StarSystem, Array<Label>>) b.getUserObject();
        return p.getFirst();
    }

    private Pair<String, Color> getMoraleString(int morale) {
        String text = "";
        Color color;
        if (morale > 174) {
            text = StringDB.getString("FANATIC");
            color = new Color(0, 250 / 255.0f, 0, 1);
        } else if (morale > 154) {
            text = StringDB.getString("LOYAL");
            color = new Color(20 / 255.0f, 150 / 255.0f, 20 / 255.0f, 1);
        } else if (morale > 130) {
            text = StringDB.getString("PLEASED");
            color = new Color(20 / 255.0f, 150 / 255.0f, 100 / 255.0f, 1);
        } else if (morale > 99) {
            text = StringDB.getString("SATISFIED");
            color = new Color(150 / 255.0f, 150 / 255.0f, 200 / 255.0f, 1);
        } else if (morale > 75) {
            text = StringDB.getString("APATHETIC");
            color = new Color(160 / 255.0f, 160 / 255.0f, 160 / 255.0f, 1);
        } else if (morale > 49) {
            text = StringDB.getString("ANGRY");
            color = new Color(200 / 255.0f, 100 / 255.0f, 50 / 255.0f, 1);
        } else if (morale > 29) {
            text = StringDB.getString("FURIOUS");
            color = new Color(210 / 255.0f, 80 / 255.0f, 50 / 255.0f, 1);
        } else {
            text = StringDB.getString("REBELLIOUS");
            color = new Color(1.0f, 0, 0, 1);
        }
        text += "(" + morale + ")";
        return new Pair<String, Color>(text, color);
    }
}
