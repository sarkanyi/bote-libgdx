/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.VictoryObserver;

public class EmpireVictoryView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table nameTable;
    private Table victoryTable;

    public EmpireVictoryView(ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        float xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("VICTORY_CONDITIONS_MENUE"), "hugeFont",
                playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        victoryTable = new Table();
        rect = GameConstants.coordsToRelative(135, 650, 930, 500);
        victoryTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        victoryTable.align(Align.left);
        victoryTable.setSkin(skin);
        stage.addActor(victoryTable);
        victoryTable.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);

        victoryTable.clear();
        for (VictoryType vt : VictoryType.values()) {
            addInfo(vt);
        }
        victoryTable.setVisible(true);
    }

    private void addInfo(VictoryType type) {
        float rowHeight = victoryTable.getHeight() / 18;
        float textWidth = GameConstants.hToRelative(300);
        victoryTable.add(StringDB.getString(type.getDBName()), "largeFont", markColor).align(Align.left)
                .width((int) textWidth).height((int) rowHeight);
        ButtonStyle bs = new ButtonStyle();
        Button b = new Button(bs);
        b.setSkin(skin);
        String text = "";
        VictoryObserver vo = manager.getUniverseMap().getVictoryObserver();
        if (vo.isVictoryCondition(type)) {
            switch (type) {
                case VICTORYTYPE_ELIMINATION:
                    text = StringDB.getString("ELIMINATE_ALL_RIVALS");
                    break;
                case VICTORYTYPE_DIPLOMACY:
                    text = String.format("%d %s", vo.getNeededVictoryValue(manager, type),
                            StringDB.getString("SIGNED_HIGH_AGREEMENTS"));
                    break;
                case VICTORYTYPE_CONQUEST:
                    text = String.format("%d %s", vo.getNeededVictoryValue(manager, type),
                            StringDB.getString("CONQUERED_SYSTEMS"));
                    break;
                case VICTORYTYPE_RESEARCH:
                    text = String.format("%d %s", vo.getNeededVictoryValue(manager, type),
                            StringDB.getString("RESEARCHED_SPECIALS"));
                    break;
                case VICTORYTYPE_COMBATWINS:
                    text = String.format("%d %s", vo.getNeededVictoryValue(manager, type),
                            StringDB.getString("REACHED_COMBAT_WINNINGS"));
                    break;
                case VICTORYTYPE_SABOTAGE:
                    text = String.format("%d %s", vo.getNeededVictoryValue(manager, type),
                            StringDB.getString("SUCCESSFULL_SABOTAGE_ACTIONS"));
                    break;
            }
        } else
            text = StringDB.getString("DEACTIVATED");
        Label info = new Label(text, skin, "normalFont", normalColor);
        info.setAlignment(Align.left);
        info.setWrap(true);
        b.add(info).width((int) textWidth);
        textWidth = (victoryTable.getWidth() - textWidth) / 6;
        if (vo.isVictoryCondition(type)) {
            if (type == VictoryType.VICTORYTYPE_ELIMINATION) {
                text = StringDB.getString("RIVALS_LEFT");
                b.add(text, "normalFont", normalColor).width((int) textWidth * 2);
                text = "" + vo.getRivalsLeft();
                b.add(text, "normalFont", normalColor).width((int) textWidth);
            } else {
                text = StringDB.getString("WE");
                b.add(text, "normalFont", normalColor).width((int) textWidth * 2);
                text = "" + vo.getVictoryStatus(playerRace.getRaceId(), type);
                b.add(text, "normalFont", normalColor).width((int) textWidth);
                text = StringDB.getString("BEST");
                b.add(text, "normalFont", normalColor).width((int) textWidth * 2);
                text = "" + vo.getBestVictoryValue(type);
                b.add(text, "normalFont", normalColor).width((int) textWidth);
            }
        }
        victoryTable.row();
        victoryTable.add(b).align(Align.topLeft).height((int) (rowHeight * 2));
        victoryTable.row();
    }

    public void hide() {
        nameTable.setVisible(false);
        victoryTable.setVisible(false);
    }
}
