/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.ShipHistory;
import com.blotunga.bote.ships.ShipHistoryStruct;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.ShipHistoryStruct.ShipSortType;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.ui.empireview.TroopInfoForOverview.TroopSortType;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;

public class EmpireMilitaryOverview {
    final private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private float xOffset;
    private Major playerRace;
    private TextButton[] buttons;
    private EmpireMilitaryOverviewFilterType selectedButton;
    private int numButtons = EmpireMilitaryOverviewFilterType.values().length;
    private Table nameTable;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private Table shipScrollTable;
    private ScrollPane shipScroller;
    private Table headerTable;
    private Button selection = null;
    private Array<Button> shipItems;
    private ShipHistoryStruct clickedShip;
    private TroopInfoForOverview clickedTroop;
    private Table aliveCount;
    private Table deadCount;
    private ShipSortType oldShipSortType;
    private TroopSortType oldTroopSortType = null;
    private Array<TroopInfoForOverview> allTroops;
    private SelectBox<ShipFilterType> shipFilter;
    private ObjectMap<String, Ships> raceShips;
    private ObjectMap<String, Ships> fleetShips;
    private boolean visible;

    private interface HeaderInterface {
        String getCaption();

        float getWidth();

        @SuppressWarnings("rawtypes")
        Enum sortType();
    }

    private enum ShipGlobalHeader implements HeaderInterface {
        NAME("NAME", ShipSortType.BY_NAME, 190),
        TYPE("TYPE", ShipSortType.BY_TYPE, 165),
        CLASS("CLASS", ShipSortType.BY_CLASS, 130);

        private String caption;
        private ShipSortType sortType;
        private float width;

        ShipGlobalHeader(String caption, ShipSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public ShipSortType sortType() {
            return sortType;
        }
    }

    private enum TroopHeader implements HeaderInterface {
        NAME("NAME", TroopSortType.BY_NAME, 190),
        CURRENT_PLACE("CURRENT_PLACE", TroopSortType.BY_CURRENTSECTOR, 165),
        SPACE("PLACE", TroopSortType.BY_SPACE, 130),
        ATTACK("OPOWER_SHORT", TroopSortType.BY_ATTACK, 100),
        DEFENSE("DPOWER_SHORT", TroopSortType.BY_DEFENSE, 110),
        MORALE("MORALVALUE", TroopSortType.BY_MORALE, 130),
        EXPERIANCE_SHORT("EXPERIANCE_SHORT", TroopSortType.BY_EXP, 70),
        INSHIP("IN_SHIP", TroopSortType.BY_INSHIP, 140);

        private String caption;
        private TroopSortType sortType;
        private float width;

        TroopHeader(String caption, TroopSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public TroopSortType sortType() {
            return sortType;
        }
    }

    private enum AliveHeader implements HeaderInterface {
        CONSTRUCT("CONSTRUCT", ShipSortType.BY_ROUNDBUILD, 65),
        SYSTEM("SYSTEM", ShipSortType.BY_SECTORNAME, 140),
        CURRENT_PLACE("CURRENT_PLACE", ShipSortType.BY_CURRENTSECTOR, 120),
        EXPERIANCE_SHORT("EXPERIANCE_SHORT", ShipSortType.BY_EXP, 40),
        STATUS("STATUS", ShipSortType.BY_TASK, 110),
        TARGET("TARGET", ShipSortType.BY_TARGET, 70);

        private String caption;
        private ShipSortType sortType;
        private float width;

        AliveHeader(String caption, ShipSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public ShipSortType sortType() {
            return sortType;
        }
    }

    private enum LostHeader implements HeaderInterface {
        LOST("LOST", ShipSortType.BY_ROUNDDESTROY, 65),
        SYSTEM("SYSTEM", ShipSortType.BY_SECTORNAME, 140),
        EVENT("EVENT", ShipSortType.BY_DESTROYTYPE, 180),
        STATUS("STATUS", ShipSortType.BY_TASK, 160);

        private String caption;
        private ShipSortType sortType;
        private float width;

        LostHeader(String caption, ShipSortType sortType, float width) {
            this.caption = caption;
            this.width = width;
            this.sortType = sortType;
        }

        @Override
        public String getCaption() {
            return StringDB.getString(caption);
        }

        @Override
        public float getWidth() {
            return GameConstants.wToRelative(width);
        }

        @Override
        public ShipSortType sortType() {
            return sortType;
        }
    }

    private enum ShipFilterType {
        All("FILTER_ALL"),
        AllShips("FILTER_ALL_SHIPS"),
        AllStations("FILTER_ALL_STATIONS"),
        FightingShips("FILTER_FIGHTING_SHIPS"),
        FleetLearders("FILTER_FLEET_LEADERS"),
        Colonyships("COLONIZESHIPS"),
        Transports("TRANSPORTERS"),
        Scouts("SCOUTS"),
        Fighters("FIGHTERS"),
        Frigates("FRIGATES"),
        Destroyers("DESTROYERS"),
        Cruisers("CRUISERS"),
        HeavyDestroyers("HEAVY_DESTROYERS"),
        HeavyCruisers("HEAVY_CRUISERS"),
        Battleships("BATTLESHIPS"),
        Dreadnoughts("DREADNOUGHTS");

        private String name;
        private ShipFilterType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return StringDB.getString(name);
        }
    }

    public EmpireMilitaryOverview(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;
        xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        selectedButton = EmpireMilitaryOverviewFilterType.CURRENT;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(EmpireMilitaryOverviewFilterType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(80 + i * 500, 730, 180, 35);
            if (EmpireMilitaryOverviewFilterType.values()[i] == EmpireMilitaryOverviewFilterType.TROOPS)
                rect = GameConstants.coordsToRelative(930, 730, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(EmpireMilitaryOverviewFilterType.values()[i]);

            if (!needEnabled(EmpireMilitaryOverviewFilterType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        EmpireMilitaryOverviewFilterType selected = (EmpireMilitaryOverviewFilterType) event.getListenerActor()
                                .getUserObject();
                        setToSelection(selected);
                    }
                }
            });
            stage.addActor(buttons[i]);
        }
        for (TextButton button : buttons)
            button.setVisible(false);

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        shipScrollTable = new Table();
        shipScrollTable.align(Align.topLeft);
        shipScrollTable.setTouchable(Touchable.childrenOnly);
        shipScroller = new ScrollPane(shipScrollTable, skin);
        shipScroller.setVariableSizeKnobs(false);
        shipScroller.setFadeScrollBars(false);
        shipScroller.setScrollbarsOnTop(true);
        shipScroller.setScrollingDisabled(true, false);
        stage.addActor(shipScroller);
        rect = GameConstants.coordsToRelative(50, 660, 1095, 560);
        shipScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        shipScroller.setVisible(false);

        headerTable = new Table();
        rect = GameConstants.coordsToRelative(60, 690, 1075, 25);
        headerTable.align(Align.left);
        headerTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(headerTable);
        headerTable.setVisible(false);

        aliveCount = new Table();
        aliveCount.setSkin(skin);
        rect = GameConstants.coordsToRelative(270, 730, 100, 35);
        aliveCount.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(aliveCount);
        aliveCount.setVisible(false);

        deadCount = new Table();
        deadCount.setSkin(skin);
        rect = GameConstants.coordsToRelative(780, 730, 100, 35);
        deadCount.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(deadCount);
        deadCount.setVisible(false);

        SelectBoxStyle sbs = new SelectBoxStyle(skin.get("default", SelectBoxStyle.class));
        sbs.font = skin.getFont("normalFont");
        sbs.listStyle.font = skin.getFont("normalFont");
        Texture tex = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
        Sprite sp = new Sprite(tex);
        sp.setColor(new Color(0.15f, 0.15f, 0.15f, 0.9f));
        SpriteDrawable sdr = new SpriteDrawable(sp);
        sbs.background = sdr;
        sbs.scrollStyle.background = sdr;
        shipFilter = new SelectBox<ShipFilterType>(sbs);

        rect = GameConstants.coordsToRelative(400, 730, 170, 35);
        shipFilter.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(shipFilter);
        shipFilter.setItems(ShipFilterType.values());
        shipFilter.setVisible(false);
        shipFilter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                show();
            }
        });

        shipItems = new Array<Button>();

        allTroops = new Array<TroopInfoForOverview>();

        clickedShip = null;
        clickedTroop = null;

        raceShips = new ObjectMap<String, Ships>();
        fleetShips = new ObjectMap<String, Ships>();

        oldShipSortType = ShipHistoryStruct.getSortType();
        visible = false;
    }

    private void filterShips(ShipHistoryStruct sh) {
        boolean shouldDrawSh = false;
        ShipType st = raceShips.get(sh.shipName).getShipType();

        switch(shipFilter.getSelected()) {
            case All:
                shouldDrawSh = true;
                break;
            case AllShips:
                shouldDrawSh = (st != ShipType.OUTPOST && st != ShipType.STARBASE);
                break;
            case AllStations:
                shouldDrawSh = (st == ShipType.OUTPOST || st == ShipType.STARBASE);
                break;
            case Battleships:
                shouldDrawSh = (st == ShipType.BATTLESHIP);
                break;
            case Colonyships:
                shouldDrawSh = (st == ShipType.COLONYSHIP);
                break;
            case Cruisers:
                shouldDrawSh = (st == ShipType.CRUISER);
                break;
            case Destroyers:
                shouldDrawSh = (st == ShipType.DESTROYER);
                break;
            case Dreadnoughts:
                shouldDrawSh = (st == ShipType.DREADNOUGHT);
                break;
            case Fighters:
                shouldDrawSh = (st == ShipType.FIGHTER);
                break;
            case FightingShips:
                shouldDrawSh = (st != ShipType.OUTPOST && st != ShipType.STARBASE && st != ShipType.PROBE
                             && st != ShipType.COLONYSHIP && st != ShipType.TRANSPORTER);
                break;
            case Frigates:
                shouldDrawSh = (st == ShipType.FRIGATE);
                break;
            case HeavyCruisers:
                shouldDrawSh = (st == ShipType.HEAVY_CRUISER);
                break;
            case HeavyDestroyers:
                shouldDrawSh = (st == ShipType.HEAVY_DESTROYER);
                break;
            case Scouts:
                shouldDrawSh = (st == ShipType.SCOUT || st == ShipType.PROBE); //probes are basically scouts
                break;
            case Transports:
                shouldDrawSh = (st == ShipType.TRANSPORTER);
                break;
            case FleetLearders:
                shouldDrawSh = (fleetShips.containsKey(sh.shipName) && st != ShipType.OUTPOST && st != ShipType.STARBASE);
                break;
            default:
                break;
        }
        if (shouldDrawSh)
            drawShipHistory(sh, true, raceShips.get(sh.shipName));
    }

    public void show() {
        visible = true;
        nameTable.setVisible(true);

        for (TextButton button : buttons) {
            button.setVisible(true);
        }
        headerTable.clear();
        headerTable.setVisible(true);

        shipScrollTable.clear();
        selection = null;
        shipItems.clear();
        shipScroller.setVisible(true);

        nameTable.clear();
        if (selectedButton != EmpireMilitaryOverviewFilterType.TROOPS) {
            nameTable.add(StringDB.getString("SHIP_OVERVIEW_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
            showShipHeaders();
            shipFilter.setVisible(selectedButton == EmpireMilitaryOverviewFilterType.CURRENT);
            if (selectedButton == EmpireMilitaryOverviewFilterType.CURRENT)
                fillAllShips();
        } else {
            nameTable.add(StringDB.getString("TROOP_OVERVIEW_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
            showTroopHeaders();
        }

        int numberLive = 0;
        int numberDead = 0;
        ShipHistory hist = playerRace.getShipHistory();
        for (int i = 0; i < hist.getSizeOfShipHistory(); i++) {
            ShipHistoryStruct sh = hist.getShipHistory(i);
            if (hist.isShipAlive(i)) {
                numberLive++;
            } else {
                numberDead++;
            }
            if (hist.isShipAlive(i) && selectedButton == EmpireMilitaryOverviewFilterType.CURRENT) {
                filterShips(sh);
            } else if (!hist.isShipAlive(i) && selectedButton == EmpireMilitaryOverviewFilterType.LOST) {
                drawShipHistory(sh, hist.isShipAlive(i), null);
            }
        }

        aliveCount.clear();
        aliveCount.setVisible(true);
        String text = String.format("%s: %d", StringDB.getString("SHIPS_TOTAL"), numberLive);
        aliveCount.add(text, "largeFont", normalColor);
        deadCount.clear();
        deadCount.setVisible(true);
        text = String.format("%s: %d", StringDB.getString("SHIPS_LOST"), numberDead);
        deadCount.add(text, "largeFont", normalColor);

        if (selectedButton == EmpireMilitaryOverviewFilterType.TROOPS) {
            if (allTroops.size == 0) {
                fillAllTroops();
                allTroops.sort();
            }
            for (int i = 0; i < allTroops.size; i++)
                drawTroopInfo(allTroops.get(i));
        }

        stage.setKeyboardFocus(shipScrollTable);
        shipScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int limit = shipScrollTable.getCells().size;
                if (limit == 0)
                    return false;
                int selectedItem = Integer.parseInt(selection.getName());
                if (limit < selectedItem)
                    selectedItem = limit - 1;
                Button b = shipScrollTable.findActor("" + selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = limit - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += shipScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= shipScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.ENTER:
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                if (selectedButton == EmpireMilitaryOverviewFilterType.TROOPS)
                                    goToCurrentTroop();
                                else
                                    goToCurrentShip();
                            }
                        });
                        return false;
                }

                if (EmpireMilitaryOverview.this.visible) {
                    if (selectedItem >= limit)
                        selectedItem = limit - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = shipScrollTable.findActor("" + selectedItem);
                    markshipListSelected(b);
                    if (selectedButton == EmpireMilitaryOverviewFilterType.TROOPS)
                        clickedTroop = getTroop(b);
                    else
                        clickedShip = getShip(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        shipScrollTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        stage.setScrollFocus(shipScroller);
        updateSelection();
    }

    private void fillAllShips() {
        raceShips.clear();
        fleetShips.clear();
        ShipMap sm = manager.getUniverseMap().getShipMap();
        for(int i = 0; i < sm.getSize(); i++) {
            Ships fleets = sm.getAt(i);
            if (fleets.getOwnerId().equals(playerRace.getRaceId())) {
                raceShips.put(fleets.getName(), fleets);
                fleetShips.put(fleets.getName(), fleets);
                for (int j = 0; j < fleets.getFleetSize(); j++) {
                    Ships ship = fleets.getFleet().getAt(j);
                    raceShips.put(ship.getName(), ship);
                }
            }
        }
    }

    private void fillAllTroops() {
        allTroops.clear();
        Array<IntPoint> systems = playerRace.getEmpire().getSystemList();
        for (int i = 0; i < systems.size; i++) {
            StarSystem ss = manager.getUniverseMap().getStarSystemAt(systems.get(i));
            for (int j = 0; j < ss.getTroops().size; j++) {
                Troop ti = ss.getTroops().get(j);
                allTroops.add(new TroopInfoForOverview((TroopInfo) ti, ss.getCoordinates(), ss.getName(), "", false));
            }
        }
        ShipMap ships = manager.getUniverseMap().getShipMap();
        for (int i = 0; i < ships.getSize(); i++) {
            Ships ship = ships.getAt(i);
            if (ship.getOwnerId().equals(playerRace.getRaceId())) {
                addTroopsFromShip(ship);
                for (int k = 0; k < ship.getFleetSize(); k++) {
                    Ships fleetShip = ship.getFleet().getAt(k);
                    addTroopsFromShip(fleetShip);
                }
            }
        }
    }

    private void addTroopsFromShip(Ships ship) {
        StarSystem ss = manager.getUniverseMap().getStarSystemAt(ship.getCoordinates());
        for (int j = 0; j < ship.getTroops().size; j++) {
            Troop ti = ship.getTroops().get(j);
            allTroops.add(new TroopInfoForOverview((TroopInfo) ti, ship.getCoordinates(), ss.getName(), ship.getName(), true));
        }
    }

    private void drawTroopInfo(TroopInfoForOverview ti) {
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.left);
        button.setSkin(skin);
        button.add().width(GameConstants.wToRelative(10));

        Array<Label> labels = new Array<Label>();
        for (TroopHeader th : TroopHeader.values()) {
            String text = "";
            switch (th) {
                case NAME:
                    text = ti.getName();
                    break;
                case CURRENT_PLACE:
                    text = "" + ti.getCurrentPlace();
                    break;
                case ATTACK:
                    text = "" + ti.getOffense();
                    break;
                case DEFENSE:
                    text = "" + ti.getDefense();
                    break;
                case EXPERIANCE_SHORT:
                    text = "" + ti.getExperience();
                    break;
                case INSHIP:
                    text = ti.isInShip() ? ti.getCurrentShip() : StringDB.getString("NO");
                    break;
                case MORALE:
                    text = "" + ti.getMoralValue();
                    break;
                case SPACE:
                    text = "" + ti.getSize();
                    break;
            }
            Label label = new Label(text, skin, "normalFont", Color.WHITE);
            label.setColor(normalColor);
            button.add(label).width(th.getWidth());
            labels.add(label);
        }

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button b = (Button) event.getListenerActor();
                markshipListSelected(b);
                clickedTroop = getTroop(b);
                if (count >= 2) {
                    goToCurrentTroop();
                }
            }
        };

        Pair<TroopInfoForOverview, Array<Label>> pair = new Pair<TroopInfoForOverview, Array<Label>>(ti, labels);
        button.addListener(gestureListener);
        button.setUserObject(pair);
        button.add().width(GameConstants.wToRelative(10));
        int idx = shipScrollTable.getCells().size;
        button.setName("" + idx);
        shipScrollTable.add(button);
        shipScrollTable.row();
        shipItems.add(button);
    }

    private void goToCurrentTroop() {
        manager.getUniverseMap().setSelectedCoordValue(clickedTroop.getCoord());
        manager.setView(ViewTypes.GALAXY_VIEW);
        if (clickedTroop.isInShip()) {
            manager.getUniverseMap().getRenderer().getRightSideBar().getShipRenderer().selectShip(clickedTroop.getCurrentShip());
            manager.getUniverseMap().getRenderer().showShips(true);
        } else {
            manager.getUniverseMap().getRenderer().showPlanets(true);
        }
    }

    private void showTroopHeaders() {
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                TroopSortType tst = TroopSortType.BY_NAME;
                Label l = (Label) event.getListenerActor();
                tst = (TroopSortType) ((HeaderInterface) l.getUserObject()).sortType();
                TroopInfoForOverview.setSortType(tst);
                if (oldTroopSortType != null && oldTroopSortType == tst)
                    allTroops.reverse();
                else
                    allTroops.sort();
                oldTroopSortType = tst;
                show();
            }
        };

        for (TroopHeader gh : TroopHeader.values()) {
            Label headerLabel = new Label(gh.getCaption(), skin, "largeFont", markColor);
            headerLabel.setUserObject(gh);
            headerLabel.addListener(gestureListener);
            headerTable.add(headerLabel).width(gh.getWidth());
        }
    }

    private void showShipHeaders() {
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ShipSortType st = ShipSortType.BY_NAME;
                Label l = (Label) event.getListenerActor();
                st = (ShipSortType) ((HeaderInterface) l.getUserObject()).sortType();
                ShipHistoryStruct.setSortType(st);
                if (oldShipSortType != null && oldShipSortType == st)
                    playerRace.getShipHistory().getShipHistoryArray().reverse();
                else
                    playerRace.getShipHistory().getShipHistoryArray().sort();
                oldShipSortType = st;
                show();
            }
        };

        for (ShipGlobalHeader gh : ShipGlobalHeader.values()) {
            Label headerLabel = new Label(gh.getCaption(), skin, "largeFont", markColor);
            headerLabel.setUserObject(gh);
            headerLabel.addListener(gestureListener);
            headerTable.add(headerLabel).width(gh.getWidth());
        }
        if (selectedButton == EmpireMilitaryOverviewFilterType.CURRENT) {
            for (AliveHeader ah : AliveHeader.values()) {
                Label headerLabel = new Label(ah.getCaption(), skin, "largeFont", markColor);
                headerLabel.addListener(gestureListener);
                headerLabel.setUserObject(ah);
                headerTable.add(headerLabel).width(ah.getWidth());
            }
        } else {
            for (LostHeader lh : LostHeader.values()) {
                Label headerLabel = new Label(lh.getCaption(), skin, "largeFont", markColor);
                headerLabel.setUserObject(lh);
                headerLabel.addListener(gestureListener);
                headerTable.add(headerLabel).width(lh.getWidth());
            }
        }
    }

    private void updateSelection() {
        stage.draw();
        Button btn = null;

        if (selectedButton != EmpireMilitaryOverviewFilterType.TROOPS) {
            if (clickedShip != null)
                for (Button b : shipItems)
                    if (clickedShip.shipName.equals(getShip(b).shipName))
                        btn = b;
        } else {
            if (clickedTroop != null)
                for (Button b : shipItems)
                    if (clickedTroop == getTroop(b))
                        btn = b;
        }

        if (btn == null) {
            if (shipItems.size > 0) {
                btn = shipItems.get(0);
                if (selectedButton == EmpireMilitaryOverviewFilterType.TROOPS)
                    clickedTroop = getTroop(btn);
                else
                    clickedShip = getShip(btn);
            }
        }

        if (btn != null)
            markshipListSelected(btn);
    }

    public void hide() {
        visible = false;
        allTroops.clear();
        nameTable.setVisible(false);
        for (TextButton button : buttons)
            button.setVisible(false);
        headerTable.setVisible(false);
        shipScroller.setVisible(false);
        aliveCount.setVisible(false);
        deadCount.setVisible(false);
        shipFilter.setVisible(false);
    }

    private boolean needEnabled(EmpireMilitaryOverviewFilterType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    public void setToSelection(EmpireMilitaryOverviewFilterType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(EmpireMilitaryOverviewFilterType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void markshipListSelected(Button b) {
        if (selection != null) {
            Pair<Class, Array<Label>> p = (Pair<Class, Array<Label>>) selection.getUserObject();
            selection.getStyle().up = null;
            selection.getStyle().down = null;
            for (Label l : p.getSecond())
                l.setColor(oldColor);
        }
        if (b == null)
            b = selection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        Pair<Class, Array<Label>> p = (Pair<Class, Array<Label>>) b.getUserObject();
        if (p.getSecond().size > 0) {
            oldColor = new Color(p.getSecond().get(0).getColor());
            for (Label l : p.getSecond())
                l.setColor(markColor);
        }

        selection = b;

        float scrollerHeight = shipScroller.getScrollHeight();
        float scrollerPos = shipScroller.getScrollY();
        int selectedItem = Integer.parseInt(selection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            shipScroller.setScrollY(b.getHeight() * selectedItem - shipScroller.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            shipScroller.setScrollY(b.getHeight() * (selectedItem - shipScroller.getScrollHeight() / b.getHeight() + 1));
    }

    @SuppressWarnings("unchecked")
    private TroopInfoForOverview getTroop(Button b) {
        Pair<TroopInfoForOverview, Array<Label>> p = (Pair<TroopInfoForOverview, Array<Label>>) b.getUserObject();
        return p.getFirst();
    }

    @SuppressWarnings("unchecked")
    private ShipHistoryStruct getShip(Button b) {
        Pair<ShipHistoryStruct, Array<Label>> p = (Pair<ShipHistoryStruct, Array<Label>>) b.getUserObject();
        return p.getFirst();
    }

    private void drawShipHistory(ShipHistoryStruct sh, boolean isAlive, Ships ship) {
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.left);
        button.setSkin(skin);
        button.add().width(GameConstants.wToRelative(10));
        float offset = 0;

        Array<Label> labels = new Array<Label>();
        Label fleetLabel = null;
        for (ShipGlobalHeader gh : ShipGlobalHeader.values()) {
            String text = "";
            if (gh == ShipGlobalHeader.NAME) {
                if (ship != null && shipFilter.getSelected() == ShipFilterType.FleetLearders) {
                    text = String.format("(%d)", (ship.getFleetSize() + 1));
                    fleetLabel = new Label(text, skin, "normalFont", Color.WHITE);
                    fleetLabel.setColor(normalColor);
                    offset = GameConstants.wToRelative(40);
                    labels.add(fleetLabel);
                }
                text = sh.shipName;
            } else if (gh == ShipGlobalHeader.CLASS)
                text = sh.shipClass;
            else if (gh == ShipGlobalHeader.TYPE)
                text = sh.shipType;
            Label label = new Label(text, skin, "normalFont", Color.WHITE);
            label.setColor(normalColor);
            button.add(label).width(gh.getWidth() - offset);
            labels.add(label);
            if (fleetLabel != null) {
                button.add(fleetLabel).width(offset);
                offset = 0;
                fleetLabel = null;
            }
        }
        if (isAlive) {
            for (AliveHeader ah : AliveHeader.values()) {
                String text = "";
                switch (ah) {
                    case CONSTRUCT:
                        text += sh.buildRound;
                        break;
                    case CURRENT_PLACE:
                        text = sh.currentSector;
                        break;
                    case EXPERIANCE_SHORT:
                        text += sh.experience;
                        break;
                    case STATUS:
                        text = sh.currentTask;
                        break;
                    case SYSTEM:
                        text = sh.sectorName;
                        break;
                    case TARGET:
                        text = sh.target;
                        break;
                }
                Label label = new Label(text, skin, "normalFont", Color.WHITE);
                label.setColor(normalColor);
                button.add(label).width(ah.getWidth());
                labels.add(label);
            }
        } else {
            for (LostHeader lh : LostHeader.values()) {
                String text = "";
                switch (lh) {
                    case EVENT:
                        text = sh.kindOfDestroy;
                        break;
                    case LOST:
                        text += sh.destroyRound;
                        break;
                    case STATUS:
                        text += sh.currentTask;
                        break;
                    case SYSTEM:
                        text += sh.sectorName;
                        break;
                }
                Label label = new Label(text, skin, "normalFont", Color.WHITE);
                label.setColor(normalColor);
                button.add(label).width(lh.getWidth());
                labels.add(label);
            }
        }
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button b = (Button) event.getListenerActor();
                markshipListSelected(b);
                clickedShip = getShip(b);
                if (count >= 2) {
                    goToCurrentShip();
                }
            }
        };

        Pair<ShipHistoryStruct, Array<Label>> pair = new Pair<ShipHistoryStruct, Array<Label>>(sh, labels);
        button.addListener(gestureListener);
        button.setUserObject(pair);
        button.add().width(GameConstants.wToRelative(10));
        int idx = shipScrollTable.getCells().size;
        button.setName("" + idx);
        shipScrollTable.add(button);
        shipScrollTable.row();
        shipItems.add(button);
    }

    private void goToCurrentShip() {
        IntPoint p = new IntPoint();
        for (StarSystem ss : manager.getUniverseMap().getStarSystems()) {
            if (ss.coordsName(true).equals(clickedShip.currentSector))
                p = ss.getCoordinates();
        }
        if (!p.equals(new IntPoint()))
            manager.getUniverseMap().setSelectedCoordValue(p);
        manager.setView(ViewTypes.GALAXY_VIEW);
        if (!p.equals(new IntPoint())) {
            if (manager.getUniverseMap().getStarSystemAt(p).getIsShipInSector()) {
                manager.getUniverseMap().getRenderer().getRightSideBar().getShipRenderer().selectShip(clickedShip.shipName);
                manager.getUniverseMap().getRenderer().showShips(true);
            } else {
                manager.getUniverseMap().getRenderer().showPlanets(false);
            }
        }
    }
}
