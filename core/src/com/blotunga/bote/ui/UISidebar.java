/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class UISidebar implements Disposable {
    protected Stage stage;
    protected Rectangle position;
    private Texture texture;
    protected StarSystem starsystemInfo;
    protected ScreenManager screenManager; //we need this reference to change the screen when an UI action requires it
    private Image blackBg;
    protected Image bgImage;

    public UISidebar(final Rectangle position, String backgroundpath, ScreenManager screenManager, boolean transparent, Camera camera) {
        this.position = position;
        this.screenManager = screenManager;
        stage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));

        if (!transparent) {
            blackBg = new Image();
            stage.addActor(blackBg);
            blackBg.setBounds(position.x, position.y, position.width, position.height);
            blackBg.setDrawable(screenManager.getScreen().getTintedDrawable(GameConstants.UI_BG_SIMPLE, new Color(0f, 0f, 0f, 1f)));
        }

        bgImage = new Image();
        stage.addActor(bgImage);
        bgImage.setBounds(position.x, position.y, position.width, position.height);

        if (!backgroundpath.isEmpty()) {
            setBackground(backgroundpath);
        }

        //Global capture events, don't let other stages process events where the UI is drawn
        stage.addListener(new InputListener() {
            @Override
            public boolean handle(Event e) {
                if (!(e instanceof InputEvent))
                    return false;
                InputEvent event = (InputEvent) e;
                Vector2 tmpCoords = new Vector2();

                event.toCoordinates(event.getListenerActor(), tmpCoords);
                if (position.contains(tmpCoords)) {
                    //System.out.println("handling!!! " + e.toString() + "x = " + tmpCoords.x + "y = " + tmpCoords.y + "posx = " + position.x);
                    return true;
                } else
                    return false;
            }
        });
    }

    public void setBackground(String backgroundpath) {
        TextureRegion tr = new TextureRegion((Texture) screenManager.getAssetManager().get(
                "graphics/backgrounds/" + backgroundpath + ".png"));

        //hack because for example the Omega texture is rotated the wrong way
        if (tr.getRegionHeight() < tr.getRegionWidth()) {
            float oldWidth = bgImage.getWidth();
            float oldHeight = bgImage.getHeight();
            bgImage.setOrigin(0, oldHeight);
            bgImage.setRotation(270);
            bgImage.setWidth(oldHeight);
            bgImage.setHeight(oldWidth);
            bgImage.setX(bgImage.getX() + oldHeight);
        }
        bgImage.setDrawable(new TextureRegionDrawable(tr));
    }

    public void setStarSystemInfo(StarSystem starsystemInfo) {
        this.starsystemInfo = starsystemInfo;
    }

    public IntPoint getStarSystemCoord() {
        if (starsystemInfo != null)
            return new IntPoint(starsystemInfo.getCoordinates());
        else
            return new IntPoint();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public Stage getStage() {
        return stage;
    }

    public void setPosition(Rectangle position) {
        this.position = position;
    }

    public Texture getTexture() {
        return texture;
    }

    public Rectangle getPosition() {
        return position;
    }
}
