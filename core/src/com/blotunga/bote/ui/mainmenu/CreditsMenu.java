/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.screens.MainMenu;

public class CreditsMenu {
    private Skin skin;
    private TextButtonStyle styleSmall;
    private TextButton buttonBack;
    private Table creditsTable;
    private ScrollPane creditsPane;
    private String[] creditsText;
    private boolean canScroll;

    public CreditsMenu(final ScreenManager manager, Stage stage) {
        skin = manager.getSkin();

        BitmapFont normalFont = manager.getScaledFont();
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("normalFont", normalFont, BitmapFont.class);

        if (!skin.has("small-buttons", TextButtonStyle.class)) {
            styleSmall = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            styleSmall.font = normalFont;
            styleSmall.disabled = skin.newDrawable("default-round", Color.DARK_GRAY);
            skin.add("small-buttons", styleSmall);
        } else
            styleSmall = skin.get("small-buttons", TextButtonStyle.class);

        Rectangle rect = GameConstants.coordsToRelative(50, 130, 150, 35);
        buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.setBounds(rect.x, rect.y, rect.width, rect.height);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((MainMenu) manager.getScreen()).showMainMenu();
            }
        });
        stage.addActor(buttonBack);
        buttonBack.setVisible(false);

        creditsTable = new Table();
        creditsTable.align(Align.top);
        creditsPane = new ScrollPane(creditsTable, skin);
        creditsPane.setVariableSizeKnobs(false);
        creditsPane.setScrollingDisabled(true, false);
        stage.addActor(creditsPane);
        rect = GameConstants.coordsToRelative(250, 660, 940, 650);
        creditsPane.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        creditsTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        creditsPane.setVisible(false);

        String fileName = "data/other/credits.txt";
        FileHandle handle = Gdx.files.internal(fileName);
        String content = handle.readString("ISO-8859-1");
        creditsText = content.split("\n");
        canScroll = false;
    }

    public void show() {
        buttonBack.setVisible(true);
        creditsTable.clear();
        for (String s : creditsText) {
            s = s.trim();
            Color color = Color.YELLOW;
            if (s.endsWith(":"))
                color = Color.GREEN;
            Label l = new Label(s, skin, "normalFont", color);
            l.setWrap(true);
            l.setAlignment(Align.center, Align.center);
            creditsTable.add(l).width(creditsTable.getWidth());
            creditsTable.row();
        }
        creditsPane.setVisible(true);
        float delay = 1; // seconds

        Timer.schedule(new Task() {
            @Override
            public void run() {
                canScroll = true;
            }
        }, delay);
    }

    public void hide() {
        buttonBack.setVisible(false);
        creditsPane.setVisible(false);
        canScroll = false;
        creditsPane.setScrollY(0);
    }

    public boolean canScroll() {
        return canScroll;
    }

    public void scroll() {
        if (creditsPane.getScrollPercentY() * 100 < 100)
            creditsPane.setScrollY(creditsPane.getScrollY() + 1);
        else {
            float delay = 1; // seconds

            Timer.schedule(new Task() {
                @Override
                public void run() {
                    creditsPane.setScrollY(0);
                }
            }, delay);
        }
    }
}
