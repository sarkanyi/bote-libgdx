/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.optionsview.LoadSaveWidget;
import com.blotunga.bote.ui.screens.MainMenu;

public class LoadMenu {
    private TextButtonStyle styleSmall;
    private LoadSaveWidget loader;
    private TextButton buttonBack;

    public LoadMenu(final ScreenManager manager, Stage stage, Skin skin) {
        skin = manager.getSkin();

        BitmapFont normalFont = manager.getScaledFont();
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("normalFont", normalFont, BitmapFont.class);

        if (!skin.has("small-buttons", TextButtonStyle.class)) {
            styleSmall = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            styleSmall.font = normalFont;
            styleSmall.disabled = skin.newDrawable("default-round", Color.DARK_GRAY);
            skin.add("small-buttons", styleSmall);
        } else
            styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        loader = new LoadSaveWidget(manager, stage, skin, GameConstants.wToRelative(100), 0, Color.YELLOW, Color.BLUE);

        Rectangle rect = GameConstants.coordsToRelative(50, 130, 150, 35);
        buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.setBounds(rect.x, rect.y, rect.width, rect.height);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((MainMenu) manager.getScreen()).showMainMenu();
            }
        });
        stage.addActor(buttonBack);
        buttonBack.setVisible(false);
    }

    public void show() {
        buttonBack.setVisible(true);
        loader.show(false);
    }

    public void hide() {
        buttonBack.setVisible(false);
        loader.hide();
    }
}
