/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.screens.MainMenu;

public class InitialSettingsTutorialMenu {
    private ResourceManager manager;
    private Table nameTable;
    private Color normalColor;
    private Table textTable;
    private ScrollPane textScroller;
    private TextButton okButton;

    public InitialSettingsTutorialMenu(final ResourceManager game, Stage stage) {
        this.manager = game;
        Major playerRace = game.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(0, 790, 1440, 40);
        nameTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        textTable = new Table();
        textScroller = new ScrollPane(textTable, game.getSkin());
        textScroller.setVariableSizeKnobs(false);
        textScroller.setFadeScrollBars(false);
        textScroller.setScrollingDisabled(true, false);
        textTable.setTouchable(Touchable.disabled);
        rect = GameConstants.coordsToRelative(100, 650, 1240, 500);
        textTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        textScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        textTable.align(Align.top);
        stage.addActor(textScroller);
        textScroller.setVisible(false);

        TextButtonStyle style = manager.getSkin().get("default", TextButtonStyle.class);
        okButton = new TextButton(StringDB.getString("BTN_OKAY"), style);
        rect = GameConstants.coordsToRelative(650, 60, 150, 35);
        okButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(okButton);

        okButton.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {                    
                    ((MainMenu) game.getScreen()).showInitialSettingsMenu();
                    return false;
                }
        });
    }

    public void show() {
        nameTable.clear();
        nameTable.setVisible(true);
        Label l = new Label(StringDB.getString("TUTORIAL"), manager.getSkin(), "hugeFont", normalColor);
        nameTable.add(l);

        String str = StringDB.getString("TINITIALSETTINGS");
        l = new Label(str, manager.getSkin(), "xlFont", normalColor);
        l.setWrap(true);
        textTable.add(l).width(textScroller.getWidth() - textScroller.getStyle().vScrollKnob.getMinWidth());
        textScroller.setVisible(true);

        okButton.setVisible(true);
    }

    public void hide() {
        textScroller.setVisible(false);
        nameTable.setVisible(false);
        okButton.setVisible(false);
    }
}
