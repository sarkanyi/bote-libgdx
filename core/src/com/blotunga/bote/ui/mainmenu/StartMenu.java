/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.PlatformApiIntegration.PlatformType;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.screens.MainMenu;
import com.blotunga.bote.utils.ui.LongPressTextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip.TextTooltipStyle;

public class StartMenu {
    private final Array<TextButton> buttonArray;
    private Skin skin;
    private ScreenManager game;
    private Table table;
    private Button platformButton;
    private Label signinLabel;
    private boolean visible;

    public StartMenu(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        buttonArray = new Array<TextButton>();
        table = new Table();
        table.align(Align.top);
        Rectangle rect = GameConstants.coordsToRelative(0, 665, 1440, 665);
        table.setBounds(rect.x, rect.y, rect.width, rect.height);
        stage.addActor(table);

        rect = GameConstants.coordsToRelative(1100, 100, 250, 50);
        final PlatformType pt = manager.getPlatformApiIntegration().getPlatformType();

        if (!pt.getImageName().isEmpty()) {
            LabelStyle ls = new LabelStyle(game.getScaledFont(), Color.WHITE);
            String text = game.getPlatformApiIntegration().isConnected() ? "Sign out" : pt.getLoginText();
            signinLabel = new Label(text, ls);
            signinLabel.setTouchable(Touchable.disabled);
            float offset = 40;
            signinLabel.setBounds(rect.x + offset, rect.y, rect.width - offset, rect.height);
            signinLabel.setAlignment(Align.center);
            ButtonStyle style = new ButtonStyle();
            String prefix = "graphics/ui/" + pt.getImageName() + "-";
            String[] textureNames = {"normal", "focus", "pressed", "disabled"};
            Drawable[] drawable = new Drawable[textureNames.length];
            for(int i = 0; i < textureNames.length; i++) {
                String tn = textureNames[i];
                Texture texture = game.loadTextureImmediate(prefix + tn + pt.getExtension());
                if (pt.isNinePatch()) {
                    NinePatch np = new NinePatch(texture, 199, 79, 95, 95);
                    float scaleX = GameConstants.wToRelative(1);
                    float scaleY = GameConstants.hToRelative(1);
                    np.scale(0.25f * scaleX, 0.25f * scaleY);
                    drawable[i] = new NinePatchDrawable(np);
                } else
                    drawable[i] = new TextureRegionDrawable(new TextureRegion(texture));
            }
            int k = 0;
            style.up = drawable[k++];
            style.over = drawable[k++];
            style.down = drawable[k++];
            style.disabled = drawable[k++];
            platformButton = new Button(style);
            platformButton.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button btn = (Button) event.getListenerActor();

                    if (!btn.isDisabled())
                        if (!game.getPlatformApiIntegration().isConnected()) {
                            game.getPlatformApiIntegration().signIn();
                            signinLabel.setText("Sign out");
                            game.getGameSettings().autoSignIn = true;
                        } else {
                            game.getPlatformApiIntegration().signOut(true);
                            signinLabel.setText(pt.getLoginText());
                            game.getGameSettings().autoSignIn = false;
                        }
                }
            });
            stage.addActor(platformButton);
            stage.addActor(signinLabel);
            platformButton.setBounds(rect.x, rect.y, rect.width, rect.height);
        }
        visible = false;
    }

    public void show() {
        table.clear();
        for (int i = 0; i < MainMenuButtonType.values().length; i++) {
            MainMenuButtonType bt = MainMenuButtonType.values()[i];
            TextButton tb = new TextButton(StringDB.getString(bt.getName()), skin);

            LabelStyle ls = new LabelStyle(game.getScaledFont(), Color.BLACK);
            TextTooltipStyle style = new TextTooltipStyle(ls, game.getScreen().getTintedDrawable(GameConstants.UI_BG_SIMPLE, Color.YELLOW));
            style.wrapWidth = GamePreferences.sceneWidth / 4.3f;
            LongPressTextTooltip tp = new LongPressTextTooltip(StringDB.getString(bt.getTooltip()), style);
            tp.setInstant(true);
            tb.addListener(tp.getListener());

            tb.setUserObject(MainMenuButtonType.values()[i]);
            tb.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    MainMenuButtonType type = (MainMenuButtonType) event.getListenerActor().getUserObject();
                    switch(type) {
                        case BTN_EXIT:
                            Gdx.app.exit();
                            break;
                        case BTN_NEW_GAME:
                            ((MainMenu) game.getScreen()).showRaceSelection();
                            break;
                        case BTN_LOAD_GAME:
                            ((MainMenu) game.getScreen()).showLoadMenu();
                            break;
                        case BTN_CREDITS:
                            ((MainMenu) game.getScreen()).showCreditsMenu();
                            break;
                        case BTN_ACHIEVEMENTS:
                            if (Gdx.app.getType() == ApplicationType.Android && game.getPlatformApiIntegration().isConnected())
                                game.getPlatformApiIntegration().showAchievements();
                            else
                                ((MainMenu) game.getScreen()).showAchievementMenu();
                            break;
                    }
                }
            });

            buttonArray.add(tb);
            float buttonHeight = GamePreferences.sceneHeight / 2.5f / MainMenuButtonType.values().length;
            table.add(tb).width(GamePreferences.sceneWidth / 4.3f).height(buttonHeight).pad(buttonHeight/2);
            table.row();
        }
        table.setVisible(true);
        updateSignInLabel();
        visible = true;
    }

    public void updateSignInLabel() {
        if (signinLabel != null) {
            platformButton.setVisible(true);
            signinLabel.setVisible(true);
            if (!game.getPlatformApiIntegration().isConnected())
                signinLabel.setText(game.getPlatformApiIntegration().getPlatformType().getLoginText());
            else
                signinLabel.setText("Sign out");
            if (!game.getPlatformApiIntegration().networkAvailable()) {
                platformButton.setDisabled(true);
                signinLabel.setColor(Color.BLACK);
            } else {
                platformButton.setDisabled(false);
                signinLabel.setColor(Color.WHITE);
            }
        }
    }

    public void hide() {
        table.setVisible(false);
        if (signinLabel != null) {
            platformButton.setVisible(false);
            signinLabel.setVisible(false);
        }
        visible = false;
    }

    public boolean isVisible() {
        return visible;
    }
}
