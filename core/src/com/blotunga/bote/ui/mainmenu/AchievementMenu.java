/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.screens.MainMenu;
import com.blotunga.bote.utils.ui.ScrollEvent;
import com.blotunga.bote.utils.ui.VerticalScroller;

public class AchievementMenu implements Disposable, ScrollEvent {
    private ScreenManager manager;
    private Skin skin;
    private Stage stage;
    private Array<String> loadedTextures;
    private TextButtonStyle styleSmall;
    private Table buttonTable;
    private VerticalScroller achievementScroller;
    private Table achievementTable;
    private TextButton prevPage;
    private TextButton nextPage;
    private int currentPage;
    private int pageCnt;
    private float itemHeight;
    private int itemsInPage;

    public AchievementMenu(ScreenManager manager, Skin skin, Stage stage) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;

        loadedTextures = new Array<String>();

        BitmapFont normalFont = manager.getScaledFont();
        normalFont.setFixedWidthGlyphs("1234567890");
        normalFont.setUseIntegerPositions(true);
        skin.add("normalFont", normalFont, BitmapFont.class);

        if (!skin.has("small-buttons", TextButtonStyle.class)) {
            styleSmall = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            styleSmall.font = normalFont;
            styleSmall.disabled = skin.newDrawable("default-round", Color.DARK_GRAY);
            skin.add("small-buttons", styleSmall);
        } else
            styleSmall = skin.get("small-buttons", TextButtonStyle.class);

        Rectangle rect = GameConstants.coordsToRelative(50, 90, 600, 35);
        float buttonWidth = GameConstants.wToRelative(150);
        float buttonHeight = GameConstants.hToRelative(35);
        float buttonPad = GameConstants.wToRelative(250);
        buttonTable = new Table();
        buttonTable.align(Align.left);
        buttonTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        stage.addActor(buttonTable);

        TextButton buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((MainMenu) AchievementMenu.this.manager.getScreen()).showMainMenu();
            }
        });
        buttonTable.add(buttonBack).width(buttonWidth).height(buttonHeight).spaceRight(buttonPad);

        prevPage = new TextButton(StringDB.getString("BTN_UP", true), styleSmall);
        prevPage.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                currentPage--;
                if (currentPage >= 0) {
                    achievementScroller.setScrollY(currentPage * itemHeight * itemsInPage);
                    nextPage.setVisible(true);
                }
                if (currentPage == 0) {
                    nextPage.setVisible(false);
                }
            }
        });
        buttonTable.add(prevPage).width(buttonWidth).height(buttonHeight).spaceRight(buttonPad);

        nextPage = new TextButton(StringDB.getString("BTN_DOWN", true), styleSmall);
        nextPage.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                currentPage++;
                if (currentPage <= pageCnt) {
                    achievementScroller.setScrollY(currentPage * itemHeight * itemsInPage);
                    prevPage.setVisible(true);
                }
                if (currentPage == pageCnt) {
                    nextPage.setVisible(false);
                }
            }
        });
        buttonTable.add(nextPage).width(buttonWidth).height(buttonHeight).spaceRight(buttonPad);

        achievementTable = new Table();
        achievementTable.align(Align.topLeft);
        achievementScroller = new VerticalScroller(achievementTable);
        achievementScroller.setStyle(skin.get("default", ScrollPaneStyle.class));
        achievementScroller.setVariableSizeKnobs(false);
        achievementScroller.setFadeScrollBars(false);
        achievementScroller.setScrollingDisabled(true, false);
        stage.addActor(achievementScroller);
        rect = GameConstants.coordsToRelative(250, 660, 940, 550);
        achievementScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        achievementTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        achievementScroller.setVisible(false);
        achievementScroller.setEventHandler(this);

        currentPage = 0;
        pageCnt = 0;
        itemHeight = GameConstants.hToRelative(50);
        itemsInPage = 0;
    }

    public void show() {
        int cnt = 0;
        for (int i = 0; i < AchievementsList.values().length; i++) {
            AchievementsList achievement = AchievementsList.values()[i];
            boolean isUnlocked = achievement.isUnlocked(manager.getGameSettings().achievements[i]);
            Color color = isUnlocked ? new Color(Color.WHITE) : new Color(0.4f, 0.4f, 0.4f, 1.0f);
            if (!achievement.isHidden() || isUnlocked) {
                Table table = new Table();
                Image image = new Image(manager.loadTextureImmediate(achievement.getImgPath()));
                image.setColor(color);
                table.add(image).height(itemHeight).width(itemHeight);
                Label label = new Label(achievement.getName(), skin, "normalFont", color);
                table.add(label).width(GameConstants.wToRelative(300)).spaceLeft(GameConstants.wToRelative(10));
                label = new Label(achievement.getDescription(), skin, "normalFont", color);
                label.setWrap(true);
                table.add(label).width(GameConstants.wToRelative(580));
                achievementTable.add(table);
                achievementTable.row();
                cnt++;
            }
        }
        achievementScroller.setVisible(true);
        buttonTable.setVisible(true);
        stage.draw();
        itemsInPage = Math.max(1, (int) (achievementScroller.getHeight() / itemHeight));
        if(itemsInPage > 0 && cnt > 0)
            pageCnt = (cnt - 1) / itemsInPage;
        currentPage = (int) Math.ceil(achievementScroller.getScrollY() / (itemHeight * itemsInPage));
        showNextPrevButtonsIfNeeded();
    }

    public void hide() {
        achievementScroller.setVisible(false);
        buttonTable.setVisible(false);
    }

    @Override
    public void scrollEventY(float pixelsY) {
        currentPage = (int) Math.ceil(pixelsY / (itemHeight * itemsInPage));
        showNextPrevButtonsIfNeeded();
    }

    private void showNextPrevButtonsIfNeeded() {
        if (currentPage == pageCnt) {
            nextPage.setVisible(false);
        } else {
            nextPage.setVisible(true);
        }
        if (currentPage == 0) {
            prevPage.setVisible(false);
        } else {
            prevPage.setVisible(true);
        }
    }

    @Override
    public void dispose() {
        for (String path: loadedTextures)
            if(manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
    }
}
