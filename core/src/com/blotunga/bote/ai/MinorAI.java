/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.GenDiploMessage;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Race.RaceSpecialAbilities;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class MinorAI extends DiplomacyAI {
    public MinorAI(ResourceManager manager, Race race) {
        super(manager, race);
    }

    @Override
    public AnswerStatus reactOnOffer(DiplomacyInfo info) {
        if (!race.getRaceId().equals(info.toRace)) {
            System.err.println(String.format("Race-ID %s different for Info-ID %s", race.getRaceId(), info.toRace));
            return AnswerStatus.DECLINED;
        }

        Race fromRace = manager.getRaceController().getRace(info.fromRace);
        if (fromRace == null)
            return AnswerStatus.DECLINED;

        if (fromRace.isMajor()) {
            Minor minor = Minor.toMinor(race);

            if (minor.isSubjugated())
                return AnswerStatus.DECLINED;

            //for some things it can't answer anything but to accept
            //ie war
            if (info.type == DiplomaticAgreement.WAR)
                return AnswerStatus.ACCEPTED;
            else if (info.type == DiplomaticAgreement.NONE)
                return AnswerStatus.ACCEPTED;
            else if (info.type == DiplomaticAgreement.CORRUPTION) {
                if (tryCorruption(info))
                    return AnswerStatus.ACCEPTED;
                else
                    return AnswerStatus.DECLINED;
            } else if (info.type == DiplomaticAgreement.PRESENT) {
                reactOnDowry(info);
                return AnswerStatus.ACCEPTED;
            }

            //check if the offer can be accepted based on the current contracts
            if (!minor.canAcceptOffer(info.fromRace, info.type))
                return AnswerStatus.DECLINED;

            //save the relations to the other races, because we might need to revert
            //in case the treaty was not accepted then we have to set the values back
            ObjectIntMap<String> oldRelations = new ObjectIntMap<String>();
            ObjectIntMap<String> relations = minor.getRelations();
            for (Iterator<ObjectIntMap.Entry<String>> iter = relations.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                oldRelations.put(e.key, e.value);
            }
            //react on gifts
            reactOnDowry(info);
            int ourRelationToThem = minor.getRelation(info.fromRace);

            //if the minor itself proposed a treaty in the last 2 rounds, then accept it if it's the same or lower type
            if (info.type.getType() > DiplomaticAgreement.NONE.getType()
                    && info.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()) {
                DiplomacyInfo lastOffer = race.getLastOffer(info.fromRace);
                if (lastOffer != null)
                    if (info.type.getType() <= lastOffer.type.getType())
                        return AnswerStatus.ACCEPTED;
            }

            //check if we can accept the offer
            int neededRelation = 100;

            if (info.type == DiplomaticAgreement.TRADE) {
                neededRelation = 40;
                if (minor.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (minor.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 20;
                if (minor.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (minor.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 10;
            } else if (info.type == DiplomaticAgreement.FRIENDSHIP) {
                neededRelation = 50;
                if (minor.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (minor.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 20;
                if (minor.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (minor.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 10;
            } else if (info.type == DiplomaticAgreement.COOPERATION) {
                neededRelation = 65;
                if (minor.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (minor.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 20;
                if (minor.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (minor.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 5;
                if (minor.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 10;
            } else if (info.type == DiplomaticAgreement.ALLIANCE) {
                neededRelation = 85;
                if (minor.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (minor.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 20;
                if (minor.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
            } else if (info.type == DiplomaticAgreement.MEMBERSHIP) {
                neededRelation = 100;
                if (minor.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (minor.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 20;
                if (minor.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (minor.isRaceProperty(RaceProperty.AGRARIAN))
                    neededRelation -= 5;
                if (minor.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 10;
            }

            float multi = ourRelationToThem + 100;
            multi /= 100;
            float value = ourRelationToThem * multi;
            int random = (int) value;
            random = Math.max(1, random);
            int temp = 0;
            for (int i = 0; i < 5; i++)
                temp += ((int) (RandUtil.random() * random) + 1);
            random = temp / 5;
            //if the status was changed succesfully
            if (random > neededRelation)
                return AnswerStatus.ACCEPTED;

            //otherwise restore relationships
            relations.clear();
            for (Iterator<ObjectIntMap.Entry<String>> iter = oldRelations.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                relations.put(e.key, e.value);
            }

            //if we couldn't alter the status, the relationship could suffer
            int relationMalus = 1 + info.type.getType() - minor.getCorruptibility();
            if (relationMalus > 0)
                relationMalus = (int) (RandUtil.random() * relationMalus);
            else
                return AnswerStatus.DECLINED;

            minor.setRelation(info.fromRace, -relationMalus);
            return AnswerStatus.DECLINED;
        } else {
            System.err.println(String.format("Race-ID %s could not react on offers from non majors", race.getRaceId()));
        }

        return AnswerStatus.DECLINED;
    }

    @Override
    public boolean makeOffer(String otherRaceID, DiplomacyInfo info) {
        //only makes offers 50% of the time, thus the offers are not that frequent
        if ((int) (RandUtil.random() * 2) == 0)
            return false;

        Race otherRace = manager.getRaceController().getRace(otherRaceID);
        if (otherRace == null)
            return false;

        if (otherRace.isMajor()) {
            Minor minor = Minor.toMinor(race);
            if (minor == null)
                return false;

            if (minor.isSubjugated())
                return false;

            Major major = Major.toMajor(otherRace);
            if (major == null)
                return false;

            if (minor.isRaceContacted(otherRaceID)) {
                int ourRelationToThem = minor.getRelation(otherRaceID);
                DiplomaticAgreement agreement = minor.getAgreement(otherRaceID);

                //based on relationship makes the minor maybe an offer

                //check if we can make an offer, ie, if a different race for example has membership with us then we can't make an offer
                DiplomaticAgreement othersAgreement = DiplomaticAgreement.NONE;
                ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                for (int i = 0; i < majors.size; i++) {
                    String mID = majors.getKeyAt(i);
                    DiplomaticAgreement temp = minor.getAgreement(mID);
                    if (temp.getType() > othersAgreement.getType())
                        othersAgreement = temp;
                }

                //if the null is member somewhere then it won't make any offers
                if (othersAgreement == DiplomaticAgreement.MEMBERSHIP)
                    return false;

                float multi = ourRelationToThem + 100;
                multi /= 100;
                float value = ourRelationToThem * multi;
                int random = (int) value;
                random = Math.max(1, random);
                int temp = 0;
                for (int i = 0; i < 5; i++)
                    temp += (int) (RandUtil.random() * random + 1);
                random = temp / 5;

                //check if random is higher than a threshold, then in 50% of cases the minor makes an offer
                DiplomaticAgreement offer = DiplomaticAgreement.NONE;
                if (random > 120 && agreement.getType() < DiplomaticAgreement.MEMBERSHIP.getType()
                        && othersAgreement.getType() <= DiplomaticAgreement.FRIENDSHIP.getType())
                    offer = DiplomaticAgreement.MEMBERSHIP;
                else if (random > 105 && agreement.getType() < DiplomaticAgreement.ALLIANCE.getType()
                        && othersAgreement.getType() <= DiplomaticAgreement.FRIENDSHIP.getType())
                    offer = DiplomaticAgreement.ALLIANCE;
                else if (random > 90 && agreement.getType() < DiplomaticAgreement.COOPERATION.getType()
                        && othersAgreement.getType() <= DiplomaticAgreement.FRIENDSHIP.getType())
                    offer = DiplomaticAgreement.COOPERATION;
                else if (random > 75 && agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType()
                        && othersAgreement.getType() <= DiplomaticAgreement.COOPERATION.getType())
                    offer = DiplomaticAgreement.FRIENDSHIP;
                else if (random > 55 && agreement.getType() < DiplomaticAgreement.TRADE.getType()
                        && othersAgreement.getType() <= DiplomaticAgreement.ALLIANCE.getType())
                    offer = DiplomaticAgreement.TRADE;
                //make sure
                if (agreement.getType() >= offer.getType())
                    offer = DiplomaticAgreement.NONE;

                int minRel = 15;
                if (minor.isRaceProperty(RaceProperty.HOSTILE)) //loves war
                    minRel += 25;
                if (minor.isRaceProperty(RaceProperty.WARLIKE)) //loves war
                    minRel += 15;
                if (minor.isRaceProperty(RaceProperty.SNEAKY))
                    minRel += 5;
                if (minor.isRaceProperty(RaceProperty.FINANCIAL))
                    minRel -= 5;
                if (minor.isRaceProperty(RaceProperty.SCIENTIFIC))
                    minRel -= 10;
                if (minor.isRaceProperty(RaceProperty.PACIFIST)) //hates war
                    minRel -= 60;

                if (random < minRel && ourRelationToThem < minRel && agreement != DiplomaticAgreement.WAR)
                    offer = DiplomaticAgreement.WAR;

                if (offer != DiplomaticAgreement.NONE) {
                    //in case of alien diplomacy only war and friendship can be offered
                    if (minor.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility())
                            || major.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility())) {
                        if (offer.getType() >= DiplomaticAgreement.FRIENDSHIP.getType()) {
                            if (agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType())
                                offer = DiplomaticAgreement.FRIENDSHIP;
                        }

                        //if no war or friendship was offered, then offer nothing
                        if (offer != DiplomaticAgreement.WAR && offer != DiplomaticAgreement.FRIENDSHIP) {
                            offer = DiplomaticAgreement.NONE;
                            return false;
                        }
                    }
                    info.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
                    info.type = offer;
                    info.fromRace = minor.getRaceId();
                    info.toRace = major.getRaceId();
                    info.sendRound = manager.getCurrentRound() - 1;
                    GenDiploMessage.generateMinorOffer(manager, info);
                    return true;
                }
            }
        } else {
            Gdx.app.error("MinorAI", "Minor: " + race.getRaceId() + " makes offer to another minor");
        }
        return false;
    }

    @Override
    protected void reactOnDowry(DiplomacyInfo info) {
        int creditsFromRes = calcResInCredits(info);

        //We evaluate what the credits are actually worth in relation to relationships with other races.
        //This means that the longer somebody else knows our race, the more acceptance points it will have with us,
        //and it will be that harder for others to improve their relationship with gifts.
        //Thus we search for the race with the highest acceptance points (except the gifter) and we subtract from this our own acceptance points
        //If it's positive, we substract it from the credits
        int credits = info.credits + creditsFromRes;
        if (credits <= 0)
            return;

        Minor minor = Minor.toMinor(race);
        if (minor == null)
            return;

        int acceptancePoints = 0;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String id = majors.getKeyAt(i);
            if (!info.fromRace.equals(id))
                acceptancePoints = Math.max(acceptancePoints, minor.getAcceptancePoints(id));
        }

        //Take into account our own acceptance points. If we have a good relationship, then our money should be worth it
        if (minor.getAcceptancePoints(info.fromRace) > acceptancePoints)
            acceptancePoints = 0;
        else
            acceptancePoints -= minor.getAcceptancePoints(info.fromRace);

        if (acceptancePoints >= credits)
            credits = 1;
        else
            credits -= acceptancePoints;

        //calculate what value do we get through our credits
        int randomCredits = 0;
        //go 3 times so that for higher values we get a high average
        for (int i = 0; i < 3; i++)
            randomCredits += (int) (RandUtil.random() * (credits * 2 + 1));
        randomCredits = randomCredits / 3;

        //the value which the credits have to reach um to improve the relationship
        int neededValue = GameConstants.DIPLOMACY_PRESENT_VALUE;

        if (minor.isRaceProperty(RaceProperty.SOLOING))
            neededValue += 150;
        if (minor.isRaceProperty(RaceProperty.HOSTILE))
            neededValue += 100;
        if (minor.isRaceProperty(RaceProperty.SECRET))
            neededValue += 75;
        if (minor.isRaceProperty(RaceProperty.WARLIKE))
            neededValue += 50;
        if (minor.isRaceProperty(RaceProperty.INDUSTRIAL))
            neededValue -= 25;
        if (minor.isRaceProperty(RaceProperty.AGRARIAN))
            neededValue -= 50;
        if (minor.isRaceProperty(RaceProperty.FINANCIAL))
            neededValue -= 100;

        //now modify neededValue with the current relationship of the gifter
        //everything above 50% will be added, and what's below will be substracted
        Major major = Major.toMajor(manager.getRaceController().getRace(info.fromRace));
        if (major == null)
            return;

        int ourRelationToThem = minor.getRelation(info.fromRace);
        neededValue += ourRelationToThem - 50;
        neededValue = (int) (neededValue * (100 - major.getDiplomacyBonus()) / 100.0);

        //now calculate how much the relationship changes
        float bonus = 0.0f;
        if (randomCredits >= neededValue)
            bonus = randomCredits / neededValue; //what percentage do we get as bonus

        //also take into account the corruptibility of the races
        switch (minor.getCorruptibility()) {
            case 0:
                bonus *= 0.5f;
                break;
            case 1:
                bonus *= 0.75f;
                break;
            case 3:
                bonus *= 1.25f;
                break;
            case 4:
                bonus *= 1.5f;
                break;
        }

        //round the bonus and we get the percentage
        ourRelationToThem += (int) bonus;
        if (ourRelationToThem > 100) {
            bonus -= (ourRelationToThem - 100);
            ourRelationToThem = 100;
        }
        int oldRelation = minor.getRelation(info.fromRace);
        if (ourRelationToThem - oldRelation != 0)
            minor.setRelation(info.fromRace, ourRelationToThem - oldRelation);

        if (bonus >= 1.0)
            calcOtherMajorsRelationChange(info, (int) bonus);
    }

    /**
     * The function calculates the effects on other races if the relationship was improved by gifts for example
     * This is called at the end of reactOwnDowry() and it subtracts the percents which the gifter has received from the other races
     * @param info
     * @param relationChange
     */
    private void calcOtherMajorsRelationChange(DiplomacyInfo info, int relationChange) {
        class MajorList implements Comparable<MajorList> {
            String id;
            int relation;

            MajorList(String id, int relation) {
                this.id = id;
                this.relation = relation;
            }

            @Override
            public int compareTo(MajorList o) {
                if (relation < o.relation)
                    return -1;
                else if (relation == o.relation)
                    return 0;
                else
                    return 1;
            }
        }

        Array<MajorList> knownMajors = new Array<MajorList>();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (!info.fromRace.equals(majorID))
                if (race.isRaceContacted(majorID))
                    knownMajors.add(new MajorList(majorID, race.getRelation(majorID)));
        }
        if (knownMajors.size == 0)
            return;
        knownMajors.sort();

        //if the minor doesn't knows all majors, then reduce the bonus points a bit
        //because if it knows only one, then all of it would be subtracted
        int temp = majors.size - knownMajors.size;
        temp = (int) (temp / 1.5f);
        temp = Math.max(1, temp);
        relationChange /= temp;

        //Now subtract the percentages beginning with the race with which we have the worst relationship.
        //Do this until all percentage points are divided
        int raceNo = 0;
        while (relationChange > 0) {
            race.setRelation(knownMajors.get(raceNo).id, -1);
            raceNo++;
            relationChange--;
            if (raceNo == knownMajors.size)
                raceNo = 0;
        }
    }

    /**
     * The function calculates a bribe attempt
     * @param info
     * @return true if successful
     */
    private boolean tryCorruption(DiplomacyInfo info) {
        Minor minor = Minor.toMinor(race);
        if (minor == null)
            return false;

        Major corruptedMajor = Major.toMajor(manager.getRaceController().getRace(info.corruptedRace));
        if (corruptedMajor == null)
            return false;

        Major fromMajor = Major.toMajor(manager.getRaceController().getRace(info.fromRace));
        if (fromMajor == null)
            return false;

        int credits = info.credits + calcResInCredits(info);

        if (minor.getCoordinates().equals(new IntPoint()))
            return false;

        //get resistance from buildings (ie communication networks)
        StarSystem system = manager.getUniverseMap().getStarSystemAt(minor.getCoordinates());
        int resistance = system.getProduction().getResistance();

        //in case of a bribe of for example 5000 this will be reduced by the value of the communication network
        //for example if the resistance value is 3000, the race gets only 2000 credits
        credits -= resistance;
        credits = Math.max(credits, 0);

        //now reduce the acceptance points of the corruptedMajor
        int acceptance = minor.getAcceptancePoints(info.corruptedRace) - minor.getAcceptancePoints(info.fromRace);
        float acceptance_ratio = acceptance / 5000.0f;
        int acceptance_reduction = (int) ((-credits / 15) * (1 - acceptance_ratio));
        minor.setAcceptancePoints(info.corruptedRace, acceptance_reduction);

        //the credits value can be between 0 and about 13000
        //in case of success, the contract to the major will be cancelled
        //in case of failure only the acceptance points of the major will be lowered

        //Algorithm:
        //Try credits/250 times the loop:
        //the relationship with the corrupter and the corrupted is evaluated
        //the result is added to a random value (0-99). We will add credits/750 to this.
        //if the total is over 90, the bribe was successful
        int corruptionValue = 90;
        switch (minor.getCorruptibility()) {
            case 0: //hard to corrupt
                corruptionValue = 100;
                break;
            case 1:
                corruptionValue = 95;
                break;
            case 3:
                corruptionValue = 85;
                break;
            case 4://easy to corrupt
                corruptionValue = 80;
                break;
        }

        //106 so that it is impossible for a minor with maximum acceptance and relation to leave its current major
        corruptionValue += acceptance_ratio * (106 - corruptionValue);

        int value = 0;
        int relationFromMajor = minor.getRelation(info.fromRace);
        int relationCorruptedMajor = minor.getRelation(info.corruptedRace);
        int relationDiff = relationFromMajor - relationCorruptedMajor;

        for (int i = 0; i <= credits / 250; i++) {
            value = (int) (RandUtil.random() * 100 + relationDiff + credits / 750);
            if (value > corruptionValue) //the corruption was successful
                break;
        }

        if (value > corruptionValue) {
            DiplomaticAgreement agreement = minor.getAgreement(info.corruptedRace);
            String text = "";
            switch (agreement) {
                case TRADE:
                    text = StringDB.getString("CANCEL_TRADE_AGREEMENT", false, minor.getName());
                    break;
                case FRIENDSHIP:
                    text = StringDB.getString("CANCEL_FRIENDSHIP", false, minor.getName());
                    break;
                case COOPERATION:
                    text = StringDB.getString("CANCEL_COOPERATION", false, minor.getName());
                    break;
                case ALLIANCE:
                    text = StringDB.getString("CANCEL_ALLIANCE", false, minor.getName());
                    break;
                case MEMBERSHIP:
                    text = StringDB.getString("CANCEL_MEMBERSHIP", false, minor.getName());
                    break;
                default:
                    break;
            }

            EmpireNews message = new EmpireNews();
            if (!text.isEmpty()) {
                corruptedMajor.setAgreement(minor.getRaceId(), DiplomaticAgreement.NONE);
                minor.setAgreement(info.corruptedRace, DiplomaticAgreement.NONE);
                message.CreateNews(text, EmpireNewsType.DIPLOMACY);
                corruptedMajor.getEmpire().addMsg(message);
            }
            //message to corrupter
            text = StringDB.getString("CORRUPTION_SUCCESS", false, minor.getName());
            message = new EmpireNews();
            message.CreateNews(text, EmpireNewsType.DIPLOMACY);
            fromMajor.getEmpire().addMsg(message);
            return true;
        } else {
            String text = "";
            EmpireNews message = new EmpireNews();
            if (value < relationCorruptedMajor - 10) {
                String s = fromMajor.getEmpireNameWithArticle();
                s = Character.toString(s.charAt(0)).toUpperCase() + s.substring(1);
                text = StringDB.getString("TRYED_CORRUPTION", false, s, minor.getName());
                message.CreateNews(text, EmpireNewsType.DIPLOMACY);
                corruptedMajor.getEmpire().addMsg(message);
            }

            text = StringDB.getString("CORRUPTION_FAILED", false, minor.getName());
            message = new EmpireNews();
            message.CreateNews(text, EmpireNewsType.DIPLOMACY);
            fromMajor.getEmpire().addMsg(message);
            return false;
        }
    }

    /**
     * The function calculates how much the resources are worth in credits
     * @param info
     * @return
     */
    private int calcResInCredits(DiplomacyInfo info) {
        Minor minor = Minor.toMinor(race);
        if (minor == null)
            return 0;

        //aliens can't receive resources
        if (minor.isAlien())
            return 0;

        //first we have to calculated how much the resources are worth as credits
        //Titan is exchanged at a rate 5:1
        //Deuterium is exchanged at a rate 4.5:1
        //Duranium is exchanged at a rate 4:1
        //Crystal is exchanged at a rate 3.25:1
        //Iridium is exchanged at a rate 2.5:1
        //Deritium is exchanged at a rate 1:50

        float value = 0.0f;
        float div = 0.0f;
        StarSystem system = manager.getUniverseMap().getStarSystemAt(minor.getCoordinates());

        if (info.resources[ResourceTypes.TITAN.getType()] != 0) {
            value = info.resources[ResourceTypes.TITAN.getType()] / 5.0f;
            div = system.getResourceStore(ResourceTypes.TITAN.getType()) / 2000.0f;
        } else if (info.resources[ResourceTypes.DEUTERIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DEUTERIUM.getType()] / 4.5f;
            div = system.getResourceStore(ResourceTypes.DEUTERIUM.getType()) / 2000.0f;
        } else if (info.resources[ResourceTypes.DURANIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DURANIUM.getType()] / 4.0f;
            div = system.getResourceStore(ResourceTypes.DURANIUM.getType()) / 2000.0f;
        } else if (info.resources[ResourceTypes.CRYSTAL.getType()] != 0) {
            value = info.resources[ResourceTypes.CRYSTAL.getType()] / 3.25f;
            div = system.getResourceStore(ResourceTypes.CRYSTAL.getType()) / 2000.0f;
        } else if (info.resources[ResourceTypes.IRIDIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.IRIDIUM.getType()] / 2.5f;
            div = system.getResourceStore(ResourceTypes.IRIDIUM.getType()) / 2000.0f;
        } else if (info.resources[ResourceTypes.DERITIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DERITIUM.getType()] * 50.0f;
            div = system.getResourceStore(ResourceTypes.DERITIUM.getType()) / 10.0f;
        }

        //now we take other factors into account... the more of a resource a race has, the less it appreciates the same resource
        if (value != 0.0f) {
            div = Math.max(1.0f, div);
            value /= div;

            //take race properties into account
            if (minor.isRaceProperty(RaceProperty.PRODUCER))
                value *= 1.5f;
            if (minor.isRaceProperty(RaceProperty.INDUSTRIAL))
                value *= 1.35f;
            if (minor.isRaceProperty(RaceProperty.WARLIKE))
                value *= 1.2f;
            if (minor.isRaceProperty(RaceProperty.FINANCIAL))
                value *= 1.1f;
            if (minor.isRaceProperty(RaceProperty.SECRET))
                value *= 0.9f;
            if (minor.isRaceProperty(RaceProperty.AGRARIAN))
                value *= 0.8f;

            value = Math.max(0.0f, value);
        }
        return (int) value;
    }
}
