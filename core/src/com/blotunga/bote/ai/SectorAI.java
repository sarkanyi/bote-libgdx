/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.races.starmap.StarMap.BaseSector;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class SectorAI {
    public class SectorToTerraform implements Comparable<SectorToTerraform> {
        int pop;
        IntPoint p;

        SectorToTerraform(int pop, IntPoint p) {
            this.pop = pop;
            this.p = new IntPoint(p);
        }

        @Override
        public int compareTo(SectorToTerraform o) {
            if (pop < o.pop)
                return -1;
            else if (pop == o.pop)
                return 0;
            else
                return 1;
        }
    }

    private ResourceManager manager;
    /// This holds the added offensive and defensive powers of the races for each sector. These show the strength of the ships
    /// String -> raceID; IntPoint -> coordinate; int -> strength
    private ArrayMap<String, ObjectIntMap<IntPoint>> dangers;
    /// This holds the added offensive and defensive powers of the races for each sector, but only the warships
    /// String -> raceID; IntPoint -> coordinate; int -> strength
    private ArrayMap<String, ObjectIntMap<IntPoint>> combatShipDangers;
    /// This holds the complete ship strength of a race
    private ObjectIntMap<String> completeDanger;
    /// This holds the sector in which a race has the highest ship accumulation without stations
    private ArrayMap<String, IntPoint> highestShipDanger;
    /// This holds the sector in which the race should build an outpost
    private ArrayMap<String, BaseSector> stationBuild;
    /// This holds the sectors which are the most promising to terraform
    private ArrayMap<String, Array<SectorToTerraform>> sectorsToTerraform;
    /// This holds the sectors of the minors, which are yet unknown but can fly there to meet them
    private ArrayMap<String, Array<IntPoint>> minorRaceSectors;
    /// This holds the possible offensive targets of a race, can be a fleet or outpost
    private ArrayMap<String, Array<IntPoint>> offensiveTargets;
    /// This holds the possible systems which can be bombarded in case of a war
    private ArrayMap<String, Array<IntPoint>> bombardTargets;
    /// This holds the number of colony ships of a race
    private ObjectIntMap<String> coloShips;
    /// This holds the number of transporter ships of a race
    private ObjectIntMap<String> transportShips;

    public SectorAI(ResourceManager manager) {
        this.manager = manager;
        dangers = new ArrayMap<String, ObjectIntMap<IntPoint>>();
        combatShipDangers = new ArrayMap<String, ObjectIntMap<IntPoint>>();
        completeDanger = new ObjectIntMap<String>();
        highestShipDanger = new ArrayMap<String, IntPoint>();
        stationBuild = new ArrayMap<String, StarMap.BaseSector>();
        sectorsToTerraform = new ArrayMap<String, Array<SectorToTerraform>>();
        minorRaceSectors = new ArrayMap<String, Array<IntPoint>>();
        offensiveTargets = new ArrayMap<String, Array<IntPoint>>();
        bombardTargets = new ArrayMap<String, Array<IntPoint>>();
        coloShips = new ObjectIntMap<String>();
        transportShips = new ObjectIntMap<String>();
        clear();
    }

    public void clear() {
        ArrayMap<String, Race> races = manager.getRaceController().getRaces();
        highestShipDanger.clear();
        for (int i = 0; i < races.size; i++) {
            highestShipDanger.put(races.getKeyAt(i), new IntPoint());
        }
        dangers.clear();
        combatShipDangers.clear();
        completeDanger.clear();
        stationBuild.clear();
        sectorsToTerraform.clear();
        minorRaceSectors.clear();
        offensiveTargets.clear();
        bombardTargets.clear();
        coloShips.clear();
        transportShips.clear();
    }

    /**
     * This function returns the danger potential of a sector
     * @param raceID - race which to check
     * @param sector - sector where to look
     * @return
     */
    public int getDanger(String raceID, IntPoint sector) {
        if (dangers.containsKey(raceID)) {
            return dangers.get(raceID).get(sector, 0);
        }
        return 0;
    }

    /**
     * This function returns the danger potential of a sector considering only combat ships
     * @param raceID - race which to check
     * @param sector - sector where to look
     * @return
     */
    public int getDangerOnlyFromCombatShips(String raceID, IntPoint sector) {
        if (combatShipDangers.containsKey(raceID)) {
            ObjectIntMap<IntPoint> sectors = combatShipDangers.get(raceID);
            return sectors.get(sector, 0);
        }
        return 0;
    }

    /**
     * This function returns the danger potential of all races in a sector
     * @param ownRaceID - this will not be considered of course
     * @param sector
     * @return
     */
    public int getCompleteDanger(String ownRaceID, IntPoint sector) {
        int danger = 0;
        for (int i = 0; i < dangers.size; i++) {
            String race = dangers.getKeyAt(i);
            ObjectIntMap<IntPoint> coords = dangers.getValueAt(i);
            if (coords != null && !race.equals(ownRaceID))
                for (Iterator<ObjectIntMap.Entry<IntPoint>> iter = coords.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<IntPoint> e = iter.next();
                    if (e.key.equals(sector))
                        danger += e.value;
                }
        }
        return danger;
    }

    /**
     * Returns the danger potential of all combat ships of a race in all sectors
     * @param raceID - id of race
     * @return
     */
    public int getCompleteDanger(String raceID) {
        return completeDanger.get(raceID, 0);
    }

    /**
     * Function returns the sector coordinates where another race has the highest buildup of combat ships
     * @param raceID
     * @return
     */
    public IntPoint getHighestShipDanger(String raceID) {
        return highestShipDanger.get(raceID);
    }

    /**
     * Function returns the coordinates of the sector where a race should build an outpost
     * @param raceID
     * @return
     */
    public BaseSector getStationBuildSector(String raceID) {
        return stationBuild.get(raceID);
    }

    /**
     * This function returns an ordered list of sectors which are best suited for terraforming.
     * @param raceID
     * @return
     */
    public Array<SectorToTerraform> getSectorsToTerraform(String raceID) {
        if (sectorsToTerraform.containsKey(raceID))
            return sectorsToTerraform.get(raceID);
        else
            return new Array<SectorToTerraform>();
    }

    /**
     * The function returns an array with the sectors of the yet unknown minors.
     * @param raceID
     * @return
     */
    public Array<IntPoint> getMinorRaceSectors(String raceID) {
        if (minorRaceSectors.containsKey(raceID))
            return minorRaceSectors.get(raceID);
        else
            return new Array<IntPoint>();
    }

    /**
     * Function returns an array with sectors with possible offensive targets.
     * @param raceID
     * @return
     */
    public Array<IntPoint> getOffensiveTargets(String raceID) {
        if (offensiveTargets.containsKey(raceID))
            return offensiveTargets.get(raceID);
        else
            return new Array<IntPoint>();
    }

    /**
     * Function returns an array with sectors with possible targets for bombardments.
     * @param raceID
     * @return
     */
    public Array<IntPoint> getBombardTargets(String raceID) {
        if (bombardTargets.containsKey(raceID))
            return bombardTargets.get(raceID);
        else
            return new Array<IntPoint>();
    }

    /**
     * The function returns the number of colony ships of a race
     * @param raceID
     * @return
     */
    public int getNumberOfColoships(String raceID) {
        return coloShips.get(raceID, 0);
    }

    /**
     * The function returns the number of transport ships of a race
     * @param raceID
     * @return
     */
    public int getNumberOfTransportShips(String raceID) {
        return transportShips.get(raceID, 0);
    }

    /**
     * This function calculates the entire danger potential of ships and stations.
     * It also calculates the number of different ships of a race.
     */
    public void calculateDangers() {
        ShipMap shipMap = manager.getUniverseMap().getShipMap();
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ships = shipMap.getAt(i);
            addDanger(ships);
            //if the ship has a fleet, then those also have to be taken into account
            for (int j = 0; j < ships.getFleetSize(); j++) {
                Ships ss = ships.getFleet().getAt(j);
                addDanger(ss);
            }
        }
    }

    /**
     * This function calculates the priorities for the different sectors, like where it's worth to terraform, where can the AI meet minors etc
     * CalculateDangers() should be called before this!
     */
    public void calculateSectorPriorities() {
        RaceController raceCtrl = manager.getRaceController();
        ObjectIntMap<String> highestCombatShipDanger = new ObjectIntMap<String>();
        for (int x = 0; x < manager.getGridSizeX(); x++)
            for (int y = 0; y < manager.getGridSizeY(); y++) {
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(x, y);
                IntPoint coord = new IntPoint(x, y);

                if (ss.isSunSystem()) {
                    calculateTerraformSectors(x, y);
                    calculateMinorRaceSectors(x, y);
                }
                //Offensive fields are valid only if there is no dangerous anomaly in the sector.
                if (ss.getAnomaly() == null || ss.getAnomaly().getWaySearchWeight() < 10.0)
                    calculateOffensiveTargets(x, y);

                for (int i = 0; i < raceCtrl.getRaces().size; i++) {
                    String raceID = raceCtrl.getRaces().getKeyAt(i);
                    if (getDangerOnlyFromCombatShips(raceID, coord) > highestCombatShipDanger.get(raceID, 0)) {
                        highestCombatShipDanger.put(raceID, getDangerOnlyFromCombatShips(raceID, coord));
                        highestShipDanger.put(raceID, coord);
                    }
                }
            }

        for (int i = 0; i < raceCtrl.getMajors().size; i++) {
            String raceID = raceCtrl.getMajors().getKeyAt(i);
            if (sectorsToTerraform.containsKey(raceID)) {
                sectorsToTerraform.get(raceID).sort();
                sectorsToTerraform.get(raceID).reverse();

                calculateStationTargets(raceID);
                if (raceCtrl.getMajors().getValueAt(i).aHumanPlays()) //only humans need exploresectors
                    raceCtrl.getMajors().getValueAt(i).getStarMap().calcExploreSectors(raceID);

/*                for (int j = 0; j < getSectorsToTerraform(raceID).size; j++) {
                    System.out.println("Terraform targets: " + raceID + " " + sectorsToTerraform.get(raceID).get(j).p + " " + sectorsToTerraform.get(raceID).get(j).pop);
                }
                System.out.println("Station build priority: " + raceID + " " + " " + getStationBuildSector(raceID).position + " " + getStationBuildSector(raceID).points);
                for (int j = 0; j < getBombardTargets(raceID).size; j++) {
                    Gdx.app.log("SectorAI", "Bombard targets: " + raceID + " " + getBombardTargets(raceID).get(j));
                }
                for (int j = 0; j < getOffensiveTargets(raceID).size; j++) {
                    System.out.println("Offensive targets: " + raceID + " " + getOffensiveTargets(raceID).get(j));
                }
                for (int j = 0; j < getMinorRaceSectors(raceID).size; j++) {
                    System.out.println("Minor targets: " + raceID + " " + getMinorRaceSectors(raceID).get(j));
                }
                System.out.println("Highest ship danger: " + raceID + " " + getHighestShipDanger(raceID));
                System.out.println("Number of colony Ships: " + raceID + " " + getNumberOfColoships(raceID));
                System.out.println("Number of transport Ships: " + raceID + " " + getNumberOfTransportShips(raceID));*/
            }
        }
    }

    /**
     * This function adds the offensive and defensive powers of the ships of a race in the corresponding sector
     * @param ship
     */
    private void addDanger(Ships ship) {
        String race = ship.getOwnerId();

        int offensive = ship.getCompleteOffensivePower();
        int defensive = ship.getCompleteDefensivePower();

        ObjectIntMap<IntPoint> dngCoords;
        if (dangers.containsKey(race))
            dngCoords = dangers.get(race);
        else
            dngCoords = new ObjectIntMap<IntPoint>();
        int danger = dngCoords.get(ship.getCoordinates(), 0);
        danger += (offensive + defensive);
        dngCoords.put(ship.getCoordinates(), danger);
        dangers.put(race, dngCoords);

        if (ship.getShipType().getType() > ShipType.COLONYSHIP.getType()
                && ship.getShipType().getType() < ShipType.OUTPOST.getType()) {
            int completeDng = completeDanger.get(race, 0);
            completeDng += (offensive + defensive);
            completeDanger.put(race, completeDng);

            ObjectIntMap<IntPoint> combatDngCoords;
            if (combatShipDangers.containsKey(race))
                combatDngCoords = combatShipDangers.get(race);
            else
                combatDngCoords = new ObjectIntMap<IntPoint>();
            int combatDanger = combatDngCoords.get(ship.getCoordinates(), 0);
            combatDanger += (offensive + defensive);
            combatDngCoords.put(ship.getCoordinates(), combatDanger);
            combatShipDangers.put(race, combatDngCoords);
        }

        if (ship.getShipType() == ShipType.COLONYSHIP) {
            int coloCount = coloShips.get(race, 0);
            coloShips.put(race, ++coloCount);
        } else if (ship.getShipType() == ShipType.TRANSPORTER) {
            int transportCount = transportShips.get(race, 0);
            transportShips.put(race, ++transportCount);
        }
    }

    /**
     * This function calculates the sectors which are most suited for terraforming.
     * @param x
     * @param y
     */
    private void calculateTerraformSectors(int x, int y) {
        float pop = 0;
        StarSystem ss = manager.getUniverseMap().getStarSystemAt(x, y);
        //how much population can we still add to the system?
        for (int j = 0; j < ss.getNumberOfPlanets(); j++)
            if (ss.getPlanet(j).getIsHabitable() && !ss.getPlanet(j).getIsInhabited())
                pop += ss.getPlanet(j).getMaxInhabitants();

        if (pop >= 3) {
            ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                String majorId = majors.getKeyAt(i);
                Major major = majors.getValueAt(i);
                if (major.getStarMap().getRange(new IntPoint(x, y)) != 3)
                    if (ss.isFree() || ss.getOwnerId().equals(majorId)) {
                        SectorToTerraform stt = new SectorToTerraform((int) pop, new IntPoint(x, y));
                        Array<SectorToTerraform> stta;
                        if (sectorsToTerraform.containsKey(majorId))
                            stta = sectorsToTerraform.get(majorId);
                        else
                            stta = new Array<SectorToTerraform>();
                        stta.add(stt);
                        sectorsToTerraform.put(majorId, stta);
                    }
            }
        }
    }

    /**
     * Function calculates the sectors which contains a yet unknown minor race which is reachable
     * @param x
     * @param y
     */
    private void calculateMinorRaceSectors(int x, int y) {
        //if the minor as joined an empire then it should be avoided
        Race owner = manager.getUniverseMap().getStarSystemAt(x, y).getOwner();
        if (owner == null || !owner.isMinor())
            return;

        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorId = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            if (major.getStarMap().getRange(new IntPoint(x, y)) != 3)
                if (!major.isRaceContacted(owner.getRaceId()))
                    if (!owner.getCoordinates().equals(new IntPoint())) {
                        Array<IntPoint> coords;
                        if (minorRaceSectors.containsKey(majorId))
                            coords = minorRaceSectors.get(majorId);
                        else
                            coords = new Array<IntPoint>();
                        coords.add(owner.getCoordinates());
                        minorRaceSectors.put(majorId, coords);
                    }
        }
    }

    /**
     * This function calculates all of the possible targets for a race
     * @param x
     * @param y
     */
    private void calculateOffensiveTargets(int x, int y) {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        IntPoint coord = new IntPoint(x, y);
        for (int i = 0; i < majors.size; i++) {
            String majorId = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            if (major.getStarMap().getRange(coord) != 3) {
                int danger = getCompleteDanger(majorId, coord);
                //The danger will be added if it's on MY territory and we're at war with the other race or the relationship us below 50%.
                //If it's outside then we consider it if we're in war or the relationship is under 30
                if (danger > 0) {
                    //find the strongest enemy in the sector
                    String enemy = "";
                    int max = 0;
                    for (int j = 0; j < majors.size; j++) {
                        String otherId = majors.getKeyAt(j);
                        if (!majorId.equals(otherId) && getDanger(otherId, coord) > 9)
                            if (max < getDanger(otherId, coord)) {
                                max = getDanger(otherId, coord);
                                enemy = otherId;
                            }
                    }
                    //do we know the enemy?
                    if (major.isRaceContacted(enemy)) {
                        //check if it's own our territory
                        if (manager.getUniverseMap().getStarSystemAt(coord).getOwnerId().equals(majorId)) {
                            if (major.getRelation(enemy) < 50 || major.getAgreement(enemy) == DiplomaticAgreement.WAR) {
                                putOffensiveTarget(majorId, coord);
                            }
                        } else {
                            if (major.getRelation(enemy) < 30 || major.getAgreement(enemy) == DiplomaticAgreement.WAR) {
                                putOffensiveTarget(majorId, coord);
                            }
                        }
                    }
                }
                calculateBombardTargets(majorId, x, y);
            }
        }
    }

    private void putOffensiveTarget(String race, IntPoint coord) {
        Array<IntPoint> targets;
        if (offensiveTargets.containsKey(race))
            targets = offensiveTargets.get(race);
        else
            targets = new Array<IntPoint>();
        targets.add(coord);
        offensiveTargets.put(race, targets);
    }

    /**
     * This function calculates the systems which in case of war can be bombarded
     * @param raceID
     * @param x
     * @param y
     */
    private void calculateBombardTargets(String raceID, int x, int y) {
        StarSystem ss = manager.getUniverseMap().getStarSystemAt(x, y);
        if (!ss.isSunSystem() || ss.isFree())
            return;
        Race owner = ss.getOwner();
        if (owner == null)
            return;

        if (!owner.getRaceId().equals(raceID)) {
            Major ourRace = Major.toMajor(manager.getRaceController().getRace(raceID));
            if (ourRace == null)
                return;
            if (owner.isMinor() && ourRace.countTroops() < GameConstants.MIN_TROOPS_FOR_INVASION)
                return;
            if (ourRace.getAgreement(owner.getRaceId()) == DiplomaticAgreement.WAR) {
                Array<IntPoint> targets;
                if (bombardTargets.containsKey(raceID))
                    targets = bombardTargets.get(raceID);
                else
                    targets = new Array<IntPoint>();
                targets.add(new IntPoint(x, y));
                bombardTargets.put(raceID, targets);
            }
        }
    }

    /**
     * This function calculates the sector in which the built of an outpost is worth it
     * Call after calculating terraform sectors!
     * @param raceID
     */
    private void calculateStationTargets(String raceID) {
        Major major = manager.getRaceController().getMajors().get(raceID);
        if (major != null) {
            stationBuild.put(raceID, major.getStarMap().calcAIBaseSector(0.0));
            BaseSector bs = stationBuild.get(raceID);

            //the less terraform sectors we have, the higher the chance of building an outpost
            if (getSectorsToTerraform(raceID).size == 0)
                bs.points *= 4.0f;
            else if (getSectorsToTerraform(raceID).size == 0) {
                if (getSectorsToTerraform(raceID).first().pop < 10)
                    bs.points *= 3.0f;
                else
                    bs.points *= 2.0f;
            } else if (getSectorsToTerraform(raceID).size == 2)
                bs.points *= 1.5f;

            stationBuild.put(raceID, bs);
        }
    }
}
