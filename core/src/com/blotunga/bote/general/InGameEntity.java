/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.EntityType;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.utils.IntPoint;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class InGameEntity implements Disposable, KryoSerializable {
    protected EntityType type;
    protected String name;
    protected String description;
    protected IntPoint coordinates;
    protected String ownerID;
    protected transient ResourceManager resourceManager;

    public InGameEntity(InGameEntity ig) {
        this.type = ig.type;
        this.name = ig.name;
        this.description = ig.description;
        this.coordinates = ig.coordinates;
        this.ownerID = ig.ownerID;
        this.resourceManager = ig.resourceManager;
    }

    public InGameEntity() {
        this((ResourceManager) null);
    }

    public InGameEntity(ResourceManager res) {
        this(-1, -1, res);
    }

    public InGameEntity(int x, int y, ResourceManager res) {
        type = EntityType.NONE;
        resourceManager = res;
        name = "";
        description = "";
        ownerID = "";
        coordinates = new IntPoint(x, y);
    }

    public String getName() {
        return name;
    }

    public IntPoint getCoordinates() {
        return coordinates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Race getOwner() {
        return resourceManager.getRaceController().getRace(ownerID);
    }

    public String getOwnerId() {
        return ownerID;
    }

    public void setOwner(String id) {
        this.ownerID = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCoordinates(IntPoint coordinates) {
        this.coordinates = coordinates;
    }

    protected void reset() {
        name = "";
        description = "";
        ownerID = "";
    }

    public void setResourceManager(ResourceManager res) {
        this.resourceManager = res;
    }

    public void update(InGameEntity entity) {
        coordinates = entity.coordinates;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        kryo.writeObject(output, type);
        output.writeString(name);
        kryo.writeObject(output, coordinates);
        output.writeString(ownerID);
        if (type == EntityType.SHIPINFO || type == EntityType.RACE)
            output.writeString(description);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        type = kryo.readObject(input, EntityType.class);
        name = input.readString();
        coordinates = kryo.readObject(input, IntPoint.class);
        ownerID = input.readString();
        if (type == EntityType.SHIPINFO || type == EntityType.RACE)
            description = input.readString();
    }

    @Override
    public void dispose() {
    }
}
