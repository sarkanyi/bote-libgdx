/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;

public class ClientWorker {
    private ObjectMap<String, Array<SndMgrMessageEntry>> soundMessages;
    private ObjectMap<String, ViewTypes> selectedView;

    public ClientWorker() {
        soundMessages = new ObjectMap<String, Array<SndMgrMessageEntry>>();
        selectedView = new ObjectMap<String, ViewTypes>();
    }

    public void setNextActiveView(ViewTypes nextView, Major major) {
        if (major.isHumanPlayer())
            selectedView.put(major.getRaceId(), nextView);
    }

    public void setEmpireViewFor(Major major) {
        if (major.isHumanPlayer())
            selectedView.put(major.getRaceId(), ViewTypes.EMPIRE_VIEW);
    }

    public ViewTypes getNextActiveView(String raceID) {
        return selectedView.get(raceID, ViewTypes.NULL_VIEW);
    }

    public void addSoundMessage(SndMgrValue type, Major major, int priority) {
        addSoundMessage(type, major, priority, null);
    }

    public void addSoundMessage(SndMgrValue type, Major major, int priority, Race contacted) {
        if (!major.isHumanPlayer())
            return;
        SndMgrMessageEntry entry = new SndMgrMessageEntry(type, major.getRaceId(), priority, 1.0f);
        if (contacted != null)
            entry.race = contacted.getRaceId();
        Array<SndMgrMessageEntry> ar = soundMessages.get(major.getRaceId(), new Array<SndMgrMessageEntry>());
        ar.add(entry);
        soundMessages.put(major.getRaceId(), ar);
    }

    public void commitSoundMessages(SoundManager soundManager, Major player) {
        soundManager.clearMessages();
        Array<SndMgrMessageEntry> ar = soundMessages.get(player.getRaceId(), new Array<SndMgrMessageEntry>());
        for (SndMgrMessageEntry e : ar)
            soundManager.addMessage(e);
    }

    public void clearSoundMessages() {
        soundMessages.clear();
    }

    public void clearSoundMessages(Major race) {
        Array<SndMgrMessageEntry> ar = soundMessages.get(race.getRaceId(), new Array<SndMgrMessageEntry>());
        ar.clear();
        soundMessages.put(race.getRaceId(), ar);
    }
}
