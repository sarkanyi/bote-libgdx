/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.blotunga.bote.utils.IntPoint;

public class EmpireNews {
    public enum EmpireNewsType {
        NO_TYPE,
        ECONOMY,
        RESEARCH,
        SECURITY,
        DIPLOMACY,
        MILITARY,
        SOMETHING
    }

    private String text;
    private EmpireNewsType type;
    private IntPoint coord; //coordinates of the system which is affected by the news
    private int flag; //extra info which can be passed

    public EmpireNews() {
        text = "";
        type = EmpireNewsType.NO_TYPE;
        coord = new IntPoint();
        flag = 0;
    }

    public void CreateNews(String message, EmpireNewsType messageType, int flag) {
        CreateNews(message, messageType, "", new IntPoint(), false, flag);
    }

    public void CreateNews(String message, EmpireNewsType messageType) {
        CreateNews(message, messageType, "", new IntPoint());
    }

    public void CreateNews(String message, EmpireNewsType messageType, String systemName, IntPoint coordinates,
            boolean update) {
        CreateNews(message, messageType, systemName, coordinates, update, 0);
    }

    public void CreateNews(String message, EmpireNewsType messageType, String systemName, IntPoint coordinates) {
        CreateNews(message, messageType, systemName, coordinates, false, 0);
    }

    public void CreateNews(String message, EmpireNewsType messageType, String systemName, IntPoint coordinates,
            boolean update, int flag) {
        type = messageType;
        coord = coordinates;
        this.flag = flag;

        switch (type) {
            case ECONOMY: {
                if (!systemName.isEmpty()) {
                    if (!update)
                        text = StringDB.getString("BUILDING_FINISH", false, message, systemName);
                    else
                        text = StringDB.getString("UPGRADE_FINISH", false, message, systemName);
                } else
                    text = message;
                break;
            }
            default:
                text = message;
                break;
        }
    }

    public EmpireNewsType getType() {
        return type;
    }

    public IntPoint getCoord() {
        return coord;
    }

    public int getFlag() {
        return flag;
    }

    public String getText() {
        return text;
    }
}
