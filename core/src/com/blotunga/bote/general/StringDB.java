/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.blotunga.bote.constants.GameConstants;

public class StringDB {
    private static ObjectMap<String, String> stringMap;

    public static void Init() {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/strings/stringtable.xml");
        stringMap = new ObjectMap<String, String>();
        XmlReader xml = new XmlReader();
        Element root = xml.parse(handle.reader(GameConstants.getCharset()));
        for (int i = 0; i < root.getChildCount(); i++) {
            String name = root.getChild(i).getName();
            String value = root.get(name);
            stringMap.put(name, value);
        }
    }

    /**
     * Function returns a string from the string table
     * @param key Identifier of the string
     * @param makeUpper uppercase first letter if true
     * @param subStr1 replace a token with substring
     * @param subStr2 replace the second occurence of the token with this
     * @param subStr3 replace the third occurence of the token with this
     * @return resulting string
     */
    public static String getString(String key, boolean makeUpper, String subStr1, String subStr2, String subStr3) {
        String returnString;
        if (!stringMap.containsKey(key)) {
            return key + " is missing";
        } else
            returnString = stringMap.get(key);
            if (!subStr1.isEmpty()) {
                try {
                    returnString = returnString.replaceFirst(String.format("%c", 0xa7), subStr1);
                } catch (Exception e) {
                    System.err.println("Error replacing param#1 in " + key + " with " + "'" + subStr1 + "'");
                };
                if (!subStr2.isEmpty()) {
                    try {
                        returnString = returnString.replaceFirst(String.format("%c", 0xa7), subStr2);
                    } catch (Exception e) {
                        System.err.println("Error replacing param#2 in " + key + " with " + "'" + subStr2 + "'");
                    };
                    if (!subStr3.isEmpty()) {
                        try {
                            returnString = returnString.replaceFirst(String.format("%c", 0xa7), subStr3);
                        } catch (Exception e) {
                            System.err.println("Error replacing param#3 in " + key + " with " + "'" + subStr3 + "'");
                        };
                    }

                }
            }
        if (makeUpper)
            returnString = Character.toString(returnString.charAt(0)).toUpperCase() + returnString.substring(1);
        return returnString;
    }

    public static String getString(String key) {
        return getString(key, false, "", "", "");
    }

    public static String getString(String key, boolean makeUpper) {
        return getString(key, makeUpper, "", "", "");
    }

    public static String getString(String key, boolean makeUpper, String subStr1) {
        return getString(key, makeUpper, subStr1, "", "");
    }

    public static String getString(String key, boolean makeUpper, String subStr1, String subStr2) {
        return getString(key, makeUpper, subStr1, subStr2, "");
    }
}
