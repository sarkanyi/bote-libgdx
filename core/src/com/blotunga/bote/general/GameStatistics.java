/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectFloatMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.utils.IntPoint;

public class GameStatistics {
    private int averageTechLevel;	///< Average tech level of all races
    private int averageResourceStorages[]; ///< Average resource storages of the races
    private ObjectIntMap<String> shipPowers;
    private ObjectFloatMap<String> marks_;
    private DemographicsStorage bsp_;
    private DemographicsStorage productivity_;
    private DemographicsStorage military_;
    private DemographicsStorage research_;
    private DemographicsStorage morale_;

    public class DemographicsInfo {
        public DemographicsInfo(int place, float value, float average, float first, float last) {
            this.place = place;
            this.value = value;
            this.average = average;
            this.first = first;
            this.last = last;
        }

        public int place;
        public float value;
        public float average;
        public float first;
        public float last;
    }

    private enum DemoType {
        BSP, PRODUCTIVITY, MILITARY, RESEARCH, MORAL
    }

    private class DemographicsStorage {
        ObjectIntMap<String> places;
        ObjectFloatMap<String> values;
        float average;
        float first;
        float last;

        public DemographicsStorage() {
            places = new ObjectIntMap<String>();
            values = new ObjectFloatMap<String>();
            average = 0;
            first = 0;
            last = 0;
        }

        public void clear() {
            places.clear();
            values.clear();
            average = 0;
            first = 0;
            last = 0;
        }
    }

    private class FloatSorter implements Comparable<FloatSorter> {
        String id;
        float value;

        FloatSorter(String id, float value) {
            this.id = id;
            this.value = value;
        }

        @Override
        public int compareTo(FloatSorter o) {
            if (value < o.value)
                return -1;
            else if (value == o.value)
                return 0;
            else
                return 1;
        }
    }

    public GameStatistics() {
        averageTechLevel = 0;
        averageResourceStorages = new int[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(averageResourceStorages, 0);
        shipPowers = new ObjectIntMap<String>();
        marks_ = new ObjectFloatMap<String>();
        bsp_ = new DemographicsStorage();
        productivity_ = new DemographicsStorage();
        military_ = new DemographicsStorage();
        research_ = new DemographicsStorage();
        morale_ = new DemographicsStorage();
    }

    public void reset() {
        averageTechLevel = 0;
        Arrays.fill(averageResourceStorages, 0);
        shipPowers.clear();
        marks_.clear();
        bsp_.clear();
        productivity_.clear();
        military_.clear();
        research_.clear();
        morale_.clear();
    }

    /**
     * Function returns the universewide average techlevel
     * @return
     */
    public int getAverageTechLevel() {
        return averageTechLevel;
    }

    /**
     * Returns the average resource stores
     * @return
     */
    public int[] getAverageResourceStorages() {
        return averageResourceStorages;
    }

    /**
     * Retursn the shippower of a race
     * @param raceID
     * @return
     */
    public int getShipPower(String raceID) {
        return shipPowers.get(raceID, 0);
    }

    /**
     * Function calculates all statistics
     * @param manager
     */
    public void calcStats(ResourceManager manager) {
        reset();
        calcAverageTechLevel(manager);
        calcAverageResourceStorages(manager);
        calcShipPowers(manager);
        calcDemographicsSystem(manager);
        calcDemographicsMilitary(manager);
        calcMarks();
    }

    private void calcAverageTechLevel(ResourceManager manager) {
        int races = 0;
        averageTechLevel = 0;

        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Empire empire = majors.getValueAt(i).getEmpire();
            if (empire.countSystems() > 0) {
                for (int j = ResearchType.BIO.getType(); j <= ResearchType.WEAPON.getType(); j++) {
                    ResearchType rt = ResearchType.fromType(j);
                    averageTechLevel += empire.getResearch().getResearchLevel(rt);
                }
                races++;
            }
        }
        if (races != 0)
            averageTechLevel = averageTechLevel / (6 * races);
    }

    private void calcAverageResourceStorages(ResourceManager manager) {
        int races = 0;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.DERITIUM.getType(); i++) {
            averageResourceStorages[i] = 0;
            for (int r = 0; r < majors.size; r++) {
                Empire empire = majors.getValueAt(r).getEmpire();
                if (empire.countSystems() > 0) {
                    averageResourceStorages[i] += empire.getStorage()[i];
                    races++;
                }
            }
            if (races != 0)
                averageResourceStorages[i] /= races;
        }
    }

    private void calcShipPowers(ResourceManager manager) {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int r = 0; r < majors.size; r++)
            shipPowers.put(majors.getKeyAt(r), manager.getSectorAI().getCompleteDanger(majors.getKeyAt(r)));
    }

    private void calcDemographicsSystem(ResourceManager manager) {
        ObjectFloatMap<String> bsp = new ObjectFloatMap<String>();
        ObjectFloatMap<String> productivity = new ObjectFloatMap<String>();
        ObjectFloatMap<String> research = new ObjectFloatMap<String>();
        ObjectFloatMap<String> morale = new ObjectFloatMap<String>();
        ObjectFloatMap<String> count = new ObjectFloatMap<String>();
        Array<StarSystem> systems = manager.getUniverseMap().getStarSystems();
        for (StarSystem ss : systems) {
            if (!ss.isMajorized())
                continue;
            SystemProd prod = ss.getProduction();
            float oldvalue = bsp.get(ss.getOwnerId(), 0.0f);
            bsp.put(ss.getOwnerId(), (oldvalue + prod.getCreditsProd()));
            float resProd = 0.0f;
            for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
                ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
                resProd += prod.getResourceProd(rt);
            }
            resProd /= 2.5;
            oldvalue = productivity.get(ss.getOwnerId(), 0.0f);
            productivity.put(ss.getOwnerId(), (oldvalue + prod.getIndustryProd() + resProd));
            oldvalue = research.get(ss.getOwnerId(), 0.0f);
            research.put(ss.getOwnerId(), (oldvalue + prod.getResearchProd()));
            oldvalue = morale.get(ss.getOwnerId(), 0.0f);
            morale.put(ss.getOwnerId(), (oldvalue + ss.getMorale()));
            oldvalue = count.get(ss.getOwnerId(), 0.0f);
            count.put(ss.getOwnerId(), (oldvalue + 1));
        }
        for (Iterator<ObjectFloatMap.Entry<String>> iter = morale.entries().iterator(); iter.hasNext();) {
            ObjectFloatMap.Entry<String> e = iter.next();
            morale.put(e.key, e.value / count.get(e.key, 1.0f));
        }
        bsp_ = internalCalcDemographics(bsp_, bsp);
        productivity_ = internalCalcDemographics(productivity_, productivity);
        research_ = internalCalcDemographics(research_, research);
        morale_ = internalCalcDemographics(morale_, morale);
    }

    private void calcDemographicsMilitary(ResourceManager manager) {
        ObjectFloatMap<String> military = new ObjectFloatMap<String>();
        //init map with all majors set to 0, so that even majors without ships are represented
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int r = 0; r < majors.size; r++)
            military.put(majors.getKeyAt(r), 0.0f);
        //and iterate through all ships
        for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
            Ships s = manager.getUniverseMap().getShipMap().getAt(i);
            //stations and alien ships are not included
            if (s.isStation() || s.getOwner().isMinor())
                continue;
            int power = s.getCompleteOffensivePower(true, true, true);
            float oldValue = military.get(s.getOwnerId(), 0.0f);
            military.put(s.getOwnerId(), (oldValue + (power + power / 2)));
        }
        military_ = internalCalcDemographics(military_, military);
    }

    private DemographicsStorage internalCalcDemographics(DemographicsStorage store, ObjectFloatMap<String> m) {
        store.values = m;
        Array<FloatSorter> sortedVec = new Array<FloatSorter>();
        for (Iterator<ObjectFloatMap.Entry<String>> iter = m.entries().iterator(); iter.hasNext();) {
            ObjectFloatMap.Entry<String> e = iter.next();
            sortedVec.add(new FloatSorter(e.key, e.value));
        }
        sortedVec.sort();
        sortedVec.reverse();

        int currentRank = 1;
        float betterValue = sortedVec.first().value;
        for (int i = 0; i < sortedVec.size; i++) {
            FloatSorter fs = sortedVec.get(i);
            if (fs.value < betterValue)
                currentRank++;
            store.places.put(fs.id, currentRank);
            store.average += fs.value;
            betterValue = fs.value;
        }
        store.average /= sortedVec.size;
        store.first = sortedVec.first().value;
        store.last = sortedVec.get(sortedVec.size - 1).value;
        return store;
    }

    private void calcMarks() {
        calcMarksForDemoType(bsp_, false);
        calcMarksForDemoType(productivity_, false);
        calcMarksForDemoType(military_, false);
        calcMarksForDemoType(research_, false);
        calcMarksForDemoType(morale_, true);
    }

    private void calcMarksForDemoType(DemographicsStorage store, boolean doAverage) {
        int countOfMajors = store.values.size;
        for (Iterator<ObjectFloatMap.Entry<String>> iter = store.values.entries().iterator(); iter.hasNext();) {
            ObjectFloatMap.Entry<String> e = iter.next();
            double first = store.first;
            double last = store.last;
            float mark = 1.0f;
            if (!GameConstants.Equals(first, last))
                mark = (float) ((e.value - last) / (first - last) * (1.0 - countOfMajors) + countOfMajors);
            marks_.put(e.key, marks_.get(e.key, 0.0f) + mark);
            if (doAverage)
                marks_.put(e.key, (marks_.get(e.key, 0.0f) / (DemoType.MORAL.ordinal() + 1.0f)));
        }
    }

    /**
     * @param raceID
     * @return the demographic values of a race
     */
    public DemographicsInfo getDemographicsBSP(String raceID) {
        return new DemographicsInfo(bsp_.places.get(raceID, 0), bsp_.values.get(raceID, 0), bsp_.average, bsp_.first,
                bsp_.last);
    }

    public DemographicsInfo getDemographicsProductivity(String raceID) {
        return new DemographicsInfo(productivity_.places.get(raceID, 0), productivity_.values.get(raceID, 0),
                productivity_.average, productivity_.first, productivity_.last);
    }

    public DemographicsInfo getDemographicsMilitary(String raceID) {
        return new DemographicsInfo(military_.places.get(raceID, 0), military_.values.get(raceID, 0),
                military_.average, military_.first, military_.last);
    }

    public DemographicsInfo getDemographicsResearch(String raceID) {
        return new DemographicsInfo(research_.places.get(raceID, 0), research_.values.get(raceID, 0),
                research_.average, research_.first, research_.last);
    }

    public DemographicsInfo getDemographicsMorale(String raceID) {
        return new DemographicsInfo(morale_.places.get(raceID, 0), morale_.values.get(raceID, 0), morale_.average,
                morale_.first, morale_.last);
    }

    /**
     * The function returns the game points of a race
     * @param raceID
     * @param currentRound
     * @param difficultyLevel
     * @return
     */
    public int getGamePoints(String raceID, int currentRound, float difficultyLevel) {
        int gamePoints = 0;
        gamePoints += bsp_.values.get(raceID, 0) * 5;
        gamePoints += productivity_.values.get(raceID, 0);
        gamePoints += military_.values.get(raceID, 0) / 10;
        gamePoints += research_.values.get(raceID, 0) * 2;

        float temp = currentRound / 10.0f;
        gamePoints -= gamePoints * temp / 100.0f;

        gamePoints /= difficultyLevel;

        gamePoints /= 100;
        return Math.max(0, gamePoints);
    }

    public Array<IntPoint> getTopSystems(ResourceManager manager, int limit) {
        Array<IntPoint> lsystems = new Array<IntPoint>();

        class SystemList implements Comparable<SystemList> {
            IntPoint coord;
            int value;

            public SystemList(IntPoint coord, int value) {
                this.coord = coord;
                this.value = value;
            }

            @Override
            public int compareTo(SystemList o) {
                if (value < o.value)
                    return -1;
                else if (value == o.value)
                    return 0;
                else
                    return 1;
            }
        }
        Array<SystemList> lSystemList = new Array<SystemList>();

        for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
            StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
            if (ss.isMajorized()) {
                int value = 0;

                value += ss.getProduction().getMaxFoodProd() / 4;
                value += ss.getProduction().getIndustryProd();
                value += ss.getProduction().getMaxEnergyProd();
                value += ss.getProduction().getSecurityProd();
                value += ss.getProduction().getResearchProd();
                value += ss.getProduction().getTitanProd() / 2;
                value += ss.getProduction().getDeuteriumProd() / 3;
                value += ss.getProduction().getDuraniumProd();
                value += ss.getProduction().getCrystalProd() * 1.5;
                value += ss.getProduction().getIridiumProd() * 2;
                value += ss.getProduction().getDeritiumProd() * 100;
                value += ss.getProduction().getCreditsProd() * 3;

                lSystemList.add(new SystemList(ss.getCoordinates(), value));
            }
        }
        lSystemList.sort();
        lSystemList.reverse();
        for (int i = 0; i < limit && i < lSystemList.size; i++)
            lsystems.add(lSystemList.get(i).coord);
        return lsystems;
    }

    public float getMark(String raceID) {
        return marks_.get(raceID, 0);
    }

    public ObjectFloatMap<String> getSortedMarks() {
        Array<FloatSorter> sortedVec = new Array<FloatSorter>();
        for (Iterator<ObjectFloatMap.Entry<String>> iter = marks_.entries().iterator(); iter.hasNext();) {
            ObjectFloatMap.Entry<String> e = iter.next();
            sortedVec.add(new FloatSorter(e.key, e.value));
        }
        sortedVec.sort();
        sortedVec.reverse();
        ObjectFloatMap<String> sortedMarks = new ObjectFloatMap<String>();
        for (int i = 0; i < sortedVec.size; i++)
            sortedMarks.put(sortedVec.get(i).id, sortedVec.get(i).value);
        return sortedMarks;
    }
}
