/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;

/**
 * @author dragon
 *
 */
public class GameResources {
    public int food;
    public int titan;
    public int deuterium;
    public int duranium;
    public int crystal;
    public int iridium;
    public int deritium;

    public GameResources() {
        reset();
    }

    public GameResources(int food, int titan, int deuterium, int duranium, int crystal, int iridium, int deritium) {
        this.food = food;
        this.titan = titan;
        this.deuterium = deuterium;
        this.duranium = duranium;
        this.crystal = crystal;
        this.iridium = iridium;
        this.deritium = deritium;
    }

    /**
     * Caps the resources to the maximum values
     */
    public void cap() {
        if (food > GameConstants.MAX_FOOD_STORE) {
            food = GameConstants.MAX_FOOD_STORE;
        }
        if (titan > GameConstants.MAX_RES_STORE) {
            titan = GameConstants.MAX_RES_STORE;
        }
        if (deuterium > GameConstants.MAX_RES_STORE) {
            deuterium = GameConstants.MAX_RES_STORE;
        }
        if (duranium > GameConstants.MAX_RES_STORE) {
            duranium = GameConstants.MAX_RES_STORE;
        }
        if (crystal > GameConstants.MAX_RES_STORE) {
            crystal = GameConstants.MAX_RES_STORE;
        }
        if (iridium > GameConstants.MAX_RES_STORE) {
            iridium = GameConstants.MAX_RES_STORE;
        }
    }

    public void reset() {
        this.food = 1000;
        this.titan = 0;
        this.deuterium = 0;
        this.duranium = 0;
        this.crystal = 0;
        this.iridium = 0;
        this.deritium = 0;
    }

    public void multiply(double factor) {
        this.food *= factor;
        this.titan *= factor;
        this.deuterium *= factor;
        this.duranium *= factor;
        this.crystal *= factor;
        this.iridium *= factor;
        this.deritium *= factor;
    }

    public void add(GameResources res) {
        this.food += res.food;
        this.titan += res.titan;
        this.deuterium += res.deuterium;
        this.duranium += res.duranium;
        this.crystal += res.crystal;
        this.iridium += res.iridium;
        this.deritium += res.deritium;
    }

    public void addResource(int res, int cnt) {
        ResourceTypes type = ResourceTypes.fromResourceTypes(res);
        switch (type) {
            case FOOD:
                food += cnt;
                break;
            case TITAN:
                titan += cnt;
                break;
            case DEUTERIUM:
                deuterium += cnt;
                break;
            case DURANIUM:
                duranium += cnt;
                break;
            case CRYSTAL:
                crystal += cnt;
                break;
            case IRIDIUM:
                iridium += cnt;
                break;
            case DERITIUM:
                deritium += cnt;
                break;
            default:
                break;
        }
    }

    public int getResource(int res) {
        ResourceTypes type = ResourceTypes.fromResourceTypes(res);
        switch (type) {
            case FOOD:
                return food;
            case TITAN:
                return titan;
            case DEUTERIUM:
                return deuterium;
            case DURANIUM:
                return duranium;
            case CRYSTAL:
                return crystal;
            case IRIDIUM:
                return iridium;
            case DERITIUM:
                return deritium;
            default:
                return 0;
        }
    }

    public void setResource(int res, int val) {
        ResourceTypes type = ResourceTypes.fromResourceTypes(res);
        switch (type) {
            case FOOD:
                food = val;
                break;
            case TITAN:
                titan = val;
                break;
            case DEUTERIUM:
                deuterium = val;
                break;
            case DURANIUM:
                duranium = val;
                break;
            case CRYSTAL:
                crystal = val;
                break;
            case IRIDIUM:
                iridium = val;
                break;
            case DERITIUM:
                deritium = val;
                break;
            default:
                break;
        }
    }
}
