/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import java.util.concurrent.Semaphore;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.TimeUtils;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.CombatShipEffectSound;
import com.blotunga.bote.ships.CombatShipEffectSound.EffectType;
import com.blotunga.bote.utils.Pair;

public class SoundManager implements Disposable {
    private ResourceManager manager;
    private Array<SndMgrMessageEntry> sndMgrMessageList;
    private Array<Pair<Sound, Float>> soundList;
    private long delay;
    private long lastPlayed;
    private int index;
    private Sound sound;
    private Semaphore semaphore;
    private ArrayMap<EffectType, CombatShipEffectSound> effectSounds;

    public SoundManager(ResourceManager manager) {
        this.manager = manager;
        sndMgrMessageList = new Array<SndMgrMessageEntry>();
        soundList = new Array<Pair<Sound, Float>>();
        index = 0;
        lastPlayed = 0;
        delay = 2;
        semaphore = new Semaphore(1);
    }

    public void initEffectSounds() {
        effectSounds = new ArrayMap<EffectType, CombatShipEffectSound>();
        for (EffectType et : EffectType.values())
            effectSounds.put(et, new CombatShipEffectSound(et));
    }

    public void clearMessages() {
        try {
            semaphore.acquire();
            for (Pair<Sound, Float> snd : soundList)
                snd.getFirst().stop();
            for (Pair<Sound, Float> snd : soundList)
                snd.getFirst().dispose();
            soundList.clear();
            sndMgrMessageList.clear();
            index = 0;
            lastPlayed = 0;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphore.release();
    }

    public void addMessage(SndMgrMessageEntry entry) {
        for (int i = 0; i < sndMgrMessageList.size; i++) //check if the message is already added, then don't add it again
            if (entry.message == sndMgrMessageList.get(i).message && entry.race.equals(sndMgrMessageList.get(i).race))
                return;
        sndMgrMessageList.add(entry);
    }

    public void playMessages() {
        sndMgrMessageList.sort();
        sndMgrMessageList.reverse();
        stopSound();
        for (int i = 0; i < sndMgrMessageList.size; i++) {
            SndMgrMessageEntry e = sndMgrMessageList.get(i);
            String sndPath = "sounds/" + Major.toMajor(manager.getRaceController().getRace(e.race)).getPrefix()
                    + e.message.getPath();
            Sound snd = Gdx.audio.newSound(Gdx.files.internal(sndPath));
            soundList.add(new Pair<Sound, Float>(snd, e.volume));
        }
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                while (soundList.size > index) {
                    if (TimeUtils.nanoTime() - lastPlayed > delay * 1000000000) {
                        try {
                            semaphore.acquire();
                            playItem();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        semaphore.release();
                    }
                }
            }
        });
        th.start();
    }

    public void playSound(SndMgrValue type, String racePrefix) {
        playSound(racePrefix + type.getPath());
    }

    public void playSound(SndMgrValue type) {
        playSound(type.getPath());
    }

    public void playSound(String path) {
        stopSound();
        path = "sounds/" + path;
        try { //ignore missing sounds
            sound = Gdx.audio.newSound(Gdx.files.internal(path));
            sound.play(manager.getGameSettings().effectsVolume);
        } catch(GdxRuntimeException e) {
            e.printStackTrace();
        };
    }

    private void playItem() {
        if (soundList.size > index) {
            long id = soundList.get(index).getFirst().play(soundList.get(index).getSecond());
            sound.setVolume(id, manager.getGameSettings().effectsVolume);
            lastPlayed = TimeUtils.nanoTime();
            index++;
        }
    }

    public void stopSound() {
        if (sound != null) {
            sound.stop();
            sound.dispose();
        }
    }

    public ArrayMap<EffectType, CombatShipEffectSound> getEffectSounds() {
        return effectSounds;
    }

    public void clearEffectSounds() {
        if (effectSounds != null) {
            for (int i = 0; i < effectSounds.size; i++)
                effectSounds.getValueAt(i).dispose();
            effectSounds.clear();
        }
    }

    @Override
    public void dispose() {
        for (Pair<Sound, Float> snd : soundList)
            snd.getFirst().dispose();
        if (sound != null) {
            sound.stop();
            sound.dispose();
        }
        clearEffectSounds();
    }
}
