/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Iterator;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

public class GlobalBuildings {
    private ObjectMap<String, IntArray> globalBuildings;

    public GlobalBuildings() {
        globalBuildings = new ObjectMap<String, IntArray>();
    }

    public int getCountGlobalBuilding(String raceID, int id) {
        int cnt = 0;
        IntArray glList = globalBuildings.get(raceID, new IntArray(1));
        for (int i = 0; i < glList.size; i++)
            if (glList.get(i) == id)
                cnt++;
        return cnt;
    }

    public void addGlobalBuilding(String raceID, int id) {
        IntArray glList = globalBuildings.get(raceID, new IntArray(1));
        glList.add(id);
        globalBuildings.put(raceID, glList);
    }

    public void deleteGlobalBuilding(String raceID, int id) {
        for (Iterator<Entry<String, IntArray>> iter = globalBuildings.entries().iterator(); iter.hasNext();) {
            Entry<String, IntArray> e = iter.next();
            for (int i = 0; i < e.value.size; i++)
                if (e.value.get(i) == id) {
                    e.value.removeIndex(i);
                    return;
                }
        }
    }

    public void reset() {
        globalBuildings.clear();
    }
}
