/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Iterator;

import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.constants.WorkerType;

public class Worker {
    private class EmployedFreeAll {
        int employed;
        int free;
        int all;

        EmployedFreeAll(int employed, int free, int all) {
            this.employed = employed;
            this.free = free;
            this.all = all;
        }
    }

    private ObjectIntMap<WorkerType> workers;
    private int allWorkers;
    private int freeWorkers;

    public Worker() {
        workers = new ObjectIntMap<WorkerType>(WorkerType.ALL_WORKER.getType());
        workers.put(WorkerType.FOOD_WORKER, 0);
        workers.put(WorkerType.INDUSTRY_WORKER, 0);
        workers.put(WorkerType.ENERGY_WORKER, 0);
        workers.put(WorkerType.SECURITY_WORKER, 0);
        workers.put(WorkerType.RESEARCH_WORKER, 0);
        workers.put(WorkerType.TITAN_WORKER, 0);
        workers.put(WorkerType.DEUTERIUM_WORKER, 0);
        workers.put(WorkerType.DURANIUM_WORKER, 0);
        workers.put(WorkerType.CRYSTAL_WORKER, 0);
        workers.put(WorkerType.IRIDIUM_WORKER, 0);
        allWorkers = 0;
        freeWorkers = 0;
    }

    private EmployedFreeAll getWorkers() {
        int workersSum = 0;
        for (Iterator<ObjectIntMap.Entry<WorkerType>> iter = workers.entries().iterator(); iter.hasNext();) {
            ObjectIntMap.Entry<WorkerType> e = iter.next();
            workersSum += e.value;
        }
        int freeWorkersSum = allWorkers - workersSum;
        return new EmployedFreeAll(workersSum, freeWorkersSum, allWorkers);
    }

    private void checkWorkers() {
        EmployedFreeAll wk = getWorkers();
        if (wk.all < wk.employed) {
            int diff = wk.employed - wk.all;
            while (diff > 0) {
                WorkerType[] order = { WorkerType.RESEARCH_WORKER, WorkerType.SECURITY_WORKER,
                                       WorkerType.INDUSTRY_WORKER, WorkerType.TITAN_WORKER,
                                       WorkerType.DEUTERIUM_WORKER, WorkerType.DURANIUM_WORKER,
                                       WorkerType.CRYSTAL_WORKER, WorkerType.IRIDIUM_WORKER,
                                       WorkerType.ENERGY_WORKER, WorkerType.FOOD_WORKER };
                for (WorkerType wt : order) {
                    int value = workers.get(wt, 0);
                    if (value == 0)
                        continue;
                    value--;
                    workers.put(wt, value);
                    diff--;
                    if (diff == 0)
                        break;
                }
            }
            wk = getWorkers();
        }
        freeWorkers = wk.free;
    }

    public int getWorker(WorkerType worker) {
        if (worker == WorkerType.ALL_WORKER)
            return allWorkers;
        else if (worker == WorkerType.FREE_WORKER)
            return freeWorkers;
        return workers.get(worker, 0);
    }

    public void setWorker(WorkerType worker, int value) {
        if (worker == WorkerType.ALL_WORKER) {
            allWorkers = value;
            checkWorkers();
        } else {
            int wk = workers.get(worker, 0);
            freeWorkers += wk - value;
            workers.put(worker, value);
        }
    }

    public void incrementWorker(WorkerType worker) {
        int wk = workers.get(worker, 0);
        freeWorkers--;
        wk++;
        workers.put(worker, wk);
    }

    public void decrementWorker(WorkerType worker) {
        int wk = workers.get(worker, 0);
        freeWorkers++;
        wk--;
        workers.put(worker, wk);
    }

    public void freeAll() {
        for (Iterator<ObjectIntMap.Entry<WorkerType>> iter = workers.entries().iterator(); iter.hasNext();) {
            workers.put(iter.next().key, 0);
        }
        checkWorkers();
    }

    public int capacity(WorkerType type, int number) {
        int is = getWorker(type);
        if (number < is) {
            setWorker(type, number);
            return number;
        }
        return is;
    }
}
