/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.ships.TorpedoWeapons;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

public class AttackSystem {
    private Array<Ships> ships;				///< contains all ships that are involved
    private Array<Troop> troops;			///< contains all troops that are involved
    private Race defender;					///< defending race
    private StarSystem system;				///< System where the attack happens
    private IntPoint coord;					///< coordinate of the system
    private Array<String> news;				///< events
    private boolean troopsInvolved;
    private boolean assaultShipInvolved;
    private float killedPop;
    private int destroyedBuildings;

    public AttackSystem() {
        ships = new Array<Ships>();
        troops = new Array<Troop>();
        defender = null;
        system = null;
        coord = new IntPoint();
        news = new Array<String>();
        troopsInvolved = false;
        assaultShipInvolved = false;
        killedPop = 0.0f;
        destroyedBuildings = 0;
    }

    public Array<String> getNews() {
        return news;
    }

    public float getKilledPop() {
        return killedPop;
    }

    public int getDestroyedBuildings() {
        return destroyedBuildings;
    }

    public boolean isTroopsInvolved() {
        return troopsInvolved;
    }

    /**
     * The function initalizes the object
     * @param defender
     * @param system
     * @param shipMap
     */
    public void Init(Race defender, StarSystem system, ShipMap shipMap) {
        this.defender = defender;
        this.system = system;
        coord = system.getCoordinates();
        troopsInvolved = false;
        assaultShipInvolved = false;
        killedPop = 0.0f;
        destroyedBuildings = 0;

        ships.clear();
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships s = shipMap.getAt(i);
            if (s.getCoordinates().equals(coord) && s.getCurrentOrder() == ShipOrder.ATTACK_SYSTEM) {
                if (s.getCombatTactics() != CombatTactics.CT_RETREAT)
                    ships.add(s);

                //if the ship has a fleet, add them also
                for (int j = 0; j < s.getFleetSize(); j++) {
                    Ships ship = s.getFleet().getAt(j);
                    if (ship.getCombatTactics() != CombatTactics.CT_RETREAT)
                        ships.add(ship);
                }
            }
        }
    }

    /**
     * This function executes the battle.
     * @return - true if the attack was successful, in this case the second part of the pair contains the new owner's ID
     */
    public Pair<Boolean, String> calculate() {
        Pair<Boolean, String> returnVal = new Pair<Boolean, String>(false, "");
        killedPop = system.getCurrentInhabitants();
        int killedTroopsInSystem = system.getTroops().size;

        int shipDefense = 0;
        if (system.isMajorized() && defender != null && defender.isMajor())
            shipDefense = system.getProduction().getShipDefend();

        //first the ship defense is taken into account, some ships might get destroyed
        calculateShipDefense();
        //then the system is bombarded
        calculateBombAttack();
        //now the troops can get involved
        calculateTroopAttack();

        //rest of the calculations
        killedPop -= system.getCurrentInhabitants();
        if (killedPop != 0.0f) {
            String n = String.format("%.3f", killedPop);
            news.add(StringDB.getString("KILLED_POP_BY_SYSTEMATTACK", false, n, system.getName()));
            Gdx.app.debug("AttackSystem", news.peek());
        }
        //calculate lost troops
        int killedTroopsInTransport = 0;
        killedTroopsInSystem -= system.getTroops().size;
        if (killedTroopsInSystem > 0) {
            news.add((StringDB.getString("KILLED_TROOPS_IN_SYSTEM", false, "" + killedTroopsInSystem, system.getName())));
            Gdx.app.debug("AttackSystem", news.peek());
        }
        //calculate ship exp
        int xp = (int) (killedPop * 100 + shipDefense);
        if (ships.size > 0) {
            xp /= ships.size;
            for (Ships s : ships)
                s.setCrewExperience(xp);
        }

        //system was succesfully conquered
        if (troops.size > 0 && system.getInhabitants() > 0.0f) {
            //the system was conquered so move all troops to the system
            system.getTroops().addAll(troops);
            returnVal.setSecond(troops.get(0).getOwner());

            //remove troops from transport ships because now they are in the system
            for (Ships ship : ships) {
                ship.unsetCurrentOrder();
                for (int j = 0; j < ship.getTroops().size;) {
                    Troop troop = ship.getTroops().get(j);
                    if (troop.getOffense() == 0)
                        killedTroopsInTransport++;
                    ship.getTroops().removeIndex(j);
                }
            }

            if (killedTroopsInTransport > 0) {
                news.add(StringDB.getString("KILLED_TROOPS_IN_TRANSPORTS", false, "" + killedTroopsInTransport, system.getName()));
                Gdx.app.debug("AttackSystem", news.peek());
            }

            news.insert(0, StringDB.getString("INVASION_SUCCESSFUL", false, system.getName()));
            Gdx.app.debug("AttackSystem", news.first());
            returnVal.setFirst(true);
        } else {
            //delete all troops with strength 0 because they were killed
            for (Ships ship : ships) {
                for (int j = 0; j < ship.getTroops().size; j++) {
                    Troop troop = ship.getTroops().get(j);
                    if (troop.getOffense() == 0) {
                        killedTroopsInTransport++;
                        ship.getTroops().removeIndex(j--);
                    }
                }
            }
            if (killedTroopsInTransport > 0) {
                news.add(StringDB.getString("KILLED_TROOPS_IN_TRANSPORTS", false, "" + killedTroopsInTransport, system.getName()));
                Gdx.app.debug("AttackSystem", news.peek());
            }
            if (troopsInvolved && system.getInhabitants() > 0.0f) {
                news.insert(0, StringDB.getString("INVASION_FAILED", false, system.getName()));
                Gdx.app.debug("AttackSystem", news.first());
            } else if (!troopsInvolved && system.getInhabitants() > 0.0f) {
                news.insert(0, StringDB.getString("SYSTEM_BOMBED", false, system.getName()));
                Gdx.app.debug("AttackSystem", news.first());
            } else {
                news.insert(0, StringDB.getString("ALL_LIFE_DIED", false, system.getName()));
                Gdx.app.debug("AttackSystem", news.first());
            }
        }
        return returnVal;
    }

    /**
     * Checks if the attacker is different from the defender
     * @param def
     * @param att
     * @return
     */
    public boolean isDefenderNotAttacker(String def, ObjectSet<String> att) {
        return !att.contains(def);
    }

    public boolean isAttack() {
        return ships.size != 0;
    }

    private void calculateShipDefense() {
        int defense = 0;
        int killedShips = 0;
        if (system.isMajorized() && defender != null && defender.isMajor())
            defense = system.getProduction().getShipDefend();
        //algo:
        //The defense value is divided by the number of attacking ships. Then from the attacking ships it will fire randomly on a ship which causes defense / number (+-20%) damage
        for (int i = 0; i < ships.size; i++) {
            if (ships.get(i).getTroops().size > 0)
                troopsInvolved = true;
            int hit = (int) (RandUtil.random() * ships.size);
            int dam = (int) ((defense * (RandUtil.random() * 41 + 80)) / 100 / ships.size);
            //if it's an assaultship then 20% of the damage is avoided
            if (ships.get(hit).hasSpecial(ShipSpecial.ASSAULTSHIP))
                dam = (int) (dam * 0.8f);
            ships.get(hit).getHull().setCurrentHull(-dam);
        }
        //all ships that have no more hull are removed
        for (int i = 0; i < ships.size; i++) {
            if (ships.get(i).getHull().getCurrentHull() == 0) {
                ships.removeIndex(i--);
                killedShips++;
            } else {
                troops.addAll(ships.get(i).getTroops());
            }
        }
        if (killedShips == 0)
            news.add(StringDB.getString("NO_ATTACKING_SHIPS_KILLED", false, system.getName()));
        else
            news.add(StringDB.getString("ATTACKING_SHIPS_KILLED", false, "" + killedShips, system.getName()));
        Gdx.app.debug("AttackSystem", news.peek());
    }

    private void calculateBombAttack() {
        //For the bombardment only torpedoes are taken into account.
        //In case of a bombardment buildings, troops and population can be killed. Also shields can block or reduce this
        int shield = system.getProduction().getShieldPower();
        int torpedoDamage = 0;
        for (int i = 0; i < ships.size; i++)
            for (int j = 0; j < ships.get(i).getTorpedoWeapons().size; j++) {
                TorpedoWeapons tw = ships.get(i).getTorpedoWeapons().get(j);
                int dmg = tw.getTorpedoPower() * tw.getNumber() * tw.getNumberOfTubes();
                //if it's an assault ship, the damage is increased by 20%
                if (ships.get(i).hasSpecial(ShipSpecial.ASSAULTSHIP)) {
                    dmg = (int) (dmg * 1.2f);
                    assaultShipInvolved = true;
                }
                torpedoDamage += dmg;
            }
        if ((torpedoDamage - shield) < 0)
            torpedoDamage = 0;
        else
            torpedoDamage -= shield;

        if (shield > 0)
            news.add(StringDB.getString("SHIELDS_SAVED_LIFE", false, system.getName()));

        destroyedBuildings = 0;
        //now take damage into account
        if (torpedoDamage > 0) {
            float killedPop = (float) (RandUtil.random() * torpedoDamage) * 0.00075f;
            system.letPlanetsShrink(-killedPop);
            system.setInhabitants(system.getCurrentInhabitants());

            //Randomly there are also buildings destroyed. The more buildings there are, the higher the chance of getting destroyed. Troops can also be killed
            for (int i = 0; i < torpedoDamage; i++) {
                if ((int) (RandUtil.random() * 50) < system.getAllBuildings().size) {
                    system.getAllBuildings().removeIndex((int) (RandUtil.random() * system.getAllBuildings().size));
                    destroyedBuildings++;
                }
                if ((int) (RandUtil.random() * 25) < system.getTroops().size)
                    system.getTroops().removeIndex((int) (RandUtil.random() * system.getTroops().size));
                torpedoDamage /= 4;
            }
            //If the torpedo attack destroyed buildings then take them into account. Because of this the system is recalculated. This also takes into account buildings which have no more energy.
            if (system.isMajorized() && defender != null && defender.isMajor() && (destroyedBuildings != 0)) {
                system.calculateVariables();
                system.checkEnergyBuildings();
                system.calculateVariables();
            }

            if (destroyedBuildings != 0)
                news.add(StringDB.getString("DESTROYED_BUILDINGS_BY_SYSTEMATTACK", false, "" + destroyedBuildings, system.getName()));
        }
        if (news.size > 0)
            Gdx.app.debug("AttackSystem", news.peek());
    }

    private void calculateTroopAttack() {
        int offenseBonus = 0;
        if (assaultShipInvolved)
            offenseBonus = 20;

        //First the troops fight against the ground defenses. After that the stationed troops fight against the attackers. If these were destroyed then the population fights against the troops. If most of these are also destroyed then the system counts as taken
        int groundDefense = system.getProduction().getGroundDefend(); // - already contains bonus
        //also every 5 billion population adds a unit. This means that 50B pop gives 10 units with a strength of 10.
        for (int i = 0; i < groundDefense; i += 5) {
            if (troops.size == 0)
                break;
            TroopInfo ti = new TroopInfo();
            ti.setDefense(10);
            int number = ((int) RandUtil.random() * troops.size);
            int result = troops.get(number).attack(ti, offenseBonus, 0);
            //the attackers were defeated
            if (result > 0) {
                troops.get(number).setOffense(0); //troops with 0 offense will be removed at the end from the ships
                troops.removeIndex(number);
            }
        }

        //now the stationed troops
        while (system.getTroops().size > 0) {
            if (troops.size == 0)
                break;
            Array<Troop> systemTroops = system.getTroops();
            int numberAtt = (int) (RandUtil.random() * troops.size);
            int numberDef = (int) (RandUtil.random() * systemTroops.size);
            int result = troops.get(numberAtt).attack(systemTroops.get(numberDef), offenseBonus, system.getProduction().getGroundDefendBonus());
            if (result == 0)
                systemTroops.removeIndex(numberDef);
            else if (result == 1) {
                troops.get(numberAtt).setOffense(0);
                troops.removeIndex(numberAtt);
            } else {
                troops.get(numberAtt).setOffense(0);
                systemTroops.removeIndex(numberDef);
                troops.removeIndex(numberAtt);
            }
        }

        boolean fought = false;
        int maxFightsFromPop = (int) (Math.ceil(system.getInhabitants() / 5) * system.getMorale() / 100);
        while (troops.size > 0 && maxFightsFromPop > 0) {
            TroopInfo ti = new TroopInfo();
            //here the race is taken into account
            int power = 10;
            if (defender != null) {
                if (defender.isRaceProperty(RaceProperty.FINANCIAL))
                    power -= 2;
                if (defender.isRaceProperty(RaceProperty.WARLIKE))
                    power += 10;
                if (defender.isRaceProperty(RaceProperty.AGRARIAN))
                    power -= 3;
                if (defender.isRaceProperty(RaceProperty.PACIFIST))
                    power -= 5;
                if (defender.isRaceProperty(RaceProperty.SNEAKY))
                    power += 5;
                if (defender.isRaceProperty(RaceProperty.SOLOING))
                    power += 2;
                if (defender.isRaceProperty(RaceProperty.HOSTILE))
                    power += 7;

                if (power < 0)
                    power = 0;
            }

            ti.setDefense(power);
            ti.setDefense(ti.getDefense() * system.getMorale() / 100);
            int number = (int) (RandUtil.random() * troops.size);
            int result = troops.get(number).attack(ti, offenseBonus, system.getProduction().getGroundDefendBonus());
            if (result != 0) {
                troops.get(number).setOffense(0);
                troops.removeIndex(number);
            }
            if (result != 1) {
                system.letPlanetsShrink(-(float) (RandUtil.random() * 5));
                fought = true;
                maxFightsFromPop--;
            }
        }
        if (fought) {
            system.setInhabitants(system.getCurrentInhabitants());
            if (system.isMajorized() && defender != null && defender.isMajor()) {
                system.calculateVariables();
                system.checkEnergyBuildings();
                system.calculateVariables();
            }
        }

        //If troops from different empires are attacking then these must fight against eachother. Only one empire can invade a system
        if (troops.size > 0)
            for (int i = 0; i < troops.size; i++)
                if (troops.size > (i + 1))
                    if (!troops.get(i).getOwner().equals(troops.get(i + 1).getOwner())) {
                        int result = troops.get(i).attack(troops.get(i + 1), 0, 0);
                        if (result == 0) {
                            troops.get(i + 1).setOffense(0);
                            troops.removeIndex(i + 1);
                        } else if (result == 1) {
                            troops.get(i).setOffense(0);
                            troops.removeIndex(i);
                        } else {
                            troops.get(i + 1).setOffense(0);
                            troops.removeIndex(i + 1);
                            troops.get(i).setOffense(0);
                            troops.removeIndex(i);
                        }
                        i = 0;
                    }
    }
}
