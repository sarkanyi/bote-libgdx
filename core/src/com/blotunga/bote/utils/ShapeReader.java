/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.blotunga.bote.ResourceManager;

public class ShapeReader {
    static public int[][] getShapeFromFile(String fileName, ResourceManager manager) {
        Pixmap galaxyShapeMap = new Pixmap(Gdx.files.internal(fileName));
        int[][] shape = new int[manager.getGridSizeX()][manager.getGridSizeY()];
        for (int j = 0; j < manager.getGridSizeY(); j++)
            for (int i = 0; i < manager.getGridSizeX(); i++) {
                int x = 0, y = 0;
                if (manager.getGridSizeX() >= 30)
                    x = (int) (i / (manager.getGridSizeX() / 30.0f));
                else
                    x = (int) (i * (30.0f / manager.getGridSizeX()));
                if (manager.getGridSizeY() >= 20)
                    y = (int) (j / (manager.getGridSizeY() / 20.0f));
                else
                    y = (int) (j * (20.0f / manager.getGridSizeY()));
                Color c = new Color(galaxyShapeMap.getPixel(x, y));
                shape[i][j] = c.a < 0.3 ? 1 : 0;
            }
        galaxyShapeMap.dispose();
        return shape;
    }
}
