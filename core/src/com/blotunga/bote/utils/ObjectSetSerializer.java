/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.utils.ObjectSet;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

@SuppressWarnings("rawtypes")
public class ObjectSetSerializer extends Serializer<ObjectSet> {
    public ObjectSetSerializer() {
        setAcceptsNull(true);
    }

    private Class genericType;

    @Override
    public void setGenerics(Kryo kryo, Class[] generics) {
        if (generics != null && generics.length > 0)
            if (kryo.isFinal(generics[0]))
                genericType = generics[0];
    }

    @Override
    public void write(Kryo kryo, Output output, ObjectSet set) {
        int length = set.size;
        output.writeInt(length, true);
        if (length == 0)
            return;
        if (genericType != null) {
            Serializer serializer = kryo.getSerializer(genericType);
            genericType = null;
            for (ObjectSet.ObjectSetIterator iter = set.iterator(); iter.hasNext;)
                kryo.writeObjectOrNull(output, iter.next(), serializer);
        } else {
            for (ObjectSet.ObjectSetIterator iter = set.iterator(); iter.hasNext;)
                kryo.writeClassAndObject(output, iter.next());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public ObjectSet read(Kryo kryo, Input input, Class<ObjectSet> type) {
        ObjectSet set = new ObjectSet();
        int length = input.readInt(true);
        set.ensureCapacity(length);
        kryo.reference(set);
        if (genericType != null) {
            Class elementClass = genericType;
            Serializer serializer = kryo.getSerializer(genericType);
            genericType = null;
            for (int i = 0; i < length; i++)
                set.add(kryo.readObjectOrNull(input, elementClass, serializer));
        } else {
            for (int i = 0; i < length; i++)
                set.add(kryo.readClassAndObject(input));
        }
        return set;
    }
}
