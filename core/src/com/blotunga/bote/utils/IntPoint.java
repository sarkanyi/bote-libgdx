/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

public class IntPoint implements Comparable<IntPoint> {
    public int x;
    public int y;

    public IntPoint() {
        this(-1, -1);
    }

    public IntPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public IntPoint(IntPoint p) {
        this.x = p.x;
        this.y = p.y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        IntPoint p = (IntPoint) obj;
        return this.x == p.x && this.y == p.y;
    }

    public boolean inRect(int x1, int y1, int x2, int y2) {
        return (x1 <= x && x < x2) && (y1 <= y && y < y2);
    }

    public int getDist(IntPoint other) {
        return Math.max(Math.abs(other.x - x), Math.abs(other.y - y));
    }

    public void add(IntPoint p) {
        x += p.x;
        y += p.y;
    }

    @Override
    public String toString() {
        return x + ", " + y;
    }

    @Override
    public int hashCode() {
        return y + 1000 * x;
    }

    @Override
    public int compareTo(IntPoint o) {//it's a bit arbitrary but we consider x as the main driver of the comparison
        int coord = y + 1000 * x;
        int coordo = o.y + 1000 * o.x;

        if (coord < coordo)
            return -1;
        else if (coord > coordo)
            return 1;
        else
            return 0;
    }
}