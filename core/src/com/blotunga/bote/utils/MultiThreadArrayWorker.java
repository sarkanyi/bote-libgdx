/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import java.lang.reflect.Method;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;

//Usage Example:
//       MultiThreadArrayWorker<StarSystem> mta = new MultiThreadArrayWorker<StarSystem>(systems);
//       @SuppressWarnings("rawtypes")
//       Class[] parameterTypes = new Class[2];
//       parameterTypes[0] = StarSystem.class;
//       parameterTypes[1] = int.class;
//       Method method;
//       try {
//           method = ResourceManager.class.getMethod("synchronizeSystem", parameterTypes);
//           mta.run(this, method, MultiThreadArrayWorkerType.CoordCaller);
//       } catch (Exception e) {
//       };

public class MultiThreadArrayWorker<T> {
    public enum MultiThreadArrayWorkerType {
        SIMPLE_CALLER,
        INDEX_CALLER,
        PARAM_CALLER,
    }

    private IntArray idxList;
    private Array<T> array;

    public class ArrayThread implements Runnable {
        private int id;
        private Object obj;
        private Method method;
        private MultiThreadArrayWorkerType type;
        private int start;
        private int end;
        private Object param;

        public ArrayThread(int id, Object obj, Method method, MultiThreadArrayWorkerType type, int start, int end, Object param) {
            this.id = id;
            this.obj = obj;
            this.method = method;
            this.type = type;
            this.start = start;
            this.end = end;
            this.param = param;
        }

        @Override
        public void run() {
            try {
                for (int j = start; j < end; j++) {
                    if (type == MultiThreadArrayWorkerType.INDEX_CALLER)
                        method.invoke(obj, array.get(j), j);
                    else if (type == MultiThreadArrayWorkerType.SIMPLE_CALLER)
                        method.invoke(obj, array.get(j));
                    else if (type == MultiThreadArrayWorkerType.PARAM_CALLER)
                        method.invoke(obj, array.get(j), param);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Gdx.app.debug("MultiThreadArrayWorker", "Thread: #" + id + " finished");
        }
    }

    public MultiThreadArrayWorker(Array<T> array) {
        this.array = array;
        idxList = new IntArray();
        int numCores = Runtime.getRuntime().availableProcessors();
        int chunkLength = array.size / numCores;
        chunkLength = chunkLength == 0 ? 1 : chunkLength;

        for (int i = 0; (i < numCores) && (i * chunkLength < array.size); i++)
            idxList.add(i * chunkLength);
        idxList.add(array.size);
    }

    public void run(Object obj, Method method, MultiThreadArrayWorkerType type) {
        run(obj, method, type, null);
    }

    public void run(Object obj, Method method, MultiThreadArrayWorkerType type, Object param) {
        Array<Thread> threads = new Array<Thread>();

        for (int i = 0; i < idxList.size - 1; i++) {
            Thread thread = new Thread(new ArrayThread(i, obj, method, type, idxList.get(i), idxList.get(i + 1), param));
            thread.start();
            threads.add(thread);
        }

        for (Thread th : threads)
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
