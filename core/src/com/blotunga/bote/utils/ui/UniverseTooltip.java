/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.ObjectIntMap.Entry;
import com.badlogic.gdx.utils.Timer.Task;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.UniverseRenderer;
import com.blotunga.bote.utils.IntPoint;

public class UniverseTooltip extends BaseTooltip<Table> {
    private boolean longPressFired;
    private Actor actor;
    private float x, y;
    private boolean touchedUp;
    private float showTime = 2f;
    private ScreenManager resourceManager;
    private Table tooltipTable;
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture bgtexture;
    private Skin skin;
    private Major playerRace;

    Task longPressTask = new Task() {
        @Override
        public void run() {
            if (!longPressFired) {
                setContainerPosition(actor, x, y);
                getManager().enter(UniverseTooltip.this);
                longPressFired = true;
                if (!hideTask.isScheduled())
                    Timer.schedule(hideTask, UniverseTooltip.this.showTime);
            }
        }
    };

    Task hideTask = new Task() {
        public void run() {
            if (touchedUp)
                getManager().hide(UniverseTooltip.this);
        }
    };

    public UniverseTooltip(ScreenManager resourceManager, Skin skin) {
        super(new Table());
        setTargetActor(resourceManager.getUniverseMap().getRenderer().getRightSideBar().getBackground());
        tooltipTable = container.getActor();
        this.resourceManager = resourceManager;
        this.skin = skin;

        playerRace = resourceManager.getRaceController().getPlayerRace();
        tooltipHeaderColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = playerRace.getRaceDesign().clrNormalText;
        bgtexture = resourceManager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);

        Color backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.9f);
        float vpad = GameConstants.hToRelative(25);
        float hpad = GameConstants.wToRelative(25);
        tooltipTable.pad(vpad, hpad, vpad, hpad);
        Sprite sp = new Sprite(bgtexture);
        sp.setColor(backgroundColor);
        SpriteDrawable dr = new SpriteDrawable(sp);
        tooltipTable.setBackground(dr);
    }

    @Override
    protected EventListener createListener() {
        if (Gdx.app.getType() == ApplicationType.Android) {
            return new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (pointer > 1)
                        return false;

                    if (pointer == 0) {
                        if (!Gdx.input.isTouched(1)) {
                            int newX = ((int) x / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                            int newY = ((int) y / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                            IntPoint newPos = new IntPoint(newX / GamePreferences.spriteSize, newY / GamePreferences.spriteSize);
                            Vector2 newPosition = new Vector2(newX, newY);
                            Rectangle universeRect = new Rectangle(0, 0, GamePreferences.spriteSize
                                    * (resourceManager.getGridSizeX() - 1), GamePreferences.spriteSize
                                    * (resourceManager.getGridSizeY() - 1));
                            if (universeRect.contains(newPosition)) {
                                StarSystem ss = resourceManager.getUniverseMap().getStarSystemAt(newPos);
                                UniverseTooltip.this.getTooltip(ss);

                                getManager().hide(UniverseTooltip.this);
                                longPressFired = false;
                                touchedUp = false;
                                UniverseTooltip.this.x = x;
                                UniverseTooltip.this.y = y;
                                UniverseTooltip.this.actor = event.getListenerActor();
                                if (!longPressTask.isScheduled())
                                    Timer.schedule(longPressTask, 0.6f);
                                return true;
                            }
                        }
                    } else {
                        longPressFired = false;
                        longPressTask.cancel();
                    }

                    return false;
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    longPressTask.cancel();
                    touchedUp = true;
                    if (!hideTask.isScheduled())
                        Timer.schedule(hideTask, getManager().initialTime + getManager().subsequentTime);
                }
            };
        } else { //Desktop
            return new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (isInstant()) {
                        getContainer().toFront();
                        return false;
                    }
                    getManager().touchDown(UniverseTooltip.this);
                    return false;
                }

                public boolean mouseMoved(InputEvent event, float x, float y) {
                    int newX = ((int) x / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                    int newY = ((int) y / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                    IntPoint newPos = new IntPoint(newX / GamePreferences.spriteSize, newY / GamePreferences.spriteSize);
                    Vector2 newPosition = new Vector2(newX, newY);
                    Rectangle universeRect = new Rectangle(0, 0, GamePreferences.spriteSize
                            * (resourceManager.getGridSizeX() - 1), GamePreferences.spriteSize
                            * (resourceManager.getGridSizeY() - 1));
                    if (universeRect.contains(newPosition)) {
                        StarSystem ss = resourceManager.getUniverseMap().getStarSystemAt(newPos);
                        UniverseTooltip.this.getTooltip(ss);
                    }
                    if (getContainer().hasParent())
                        return false;
                    setContainerPosition(event.getListenerActor(), x, y);
                    return true;
                }

                public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    if (pointer != -1)
                        return;
                    if (Gdx.input.isTouched())
                        return;
                    Actor actor = event.getListenerActor();
                    if (fromActor != null && fromActor.isDescendantOf(actor))
                        return;
                    setContainerPosition(actor, x, y);
                    getManager().enter(UniverseTooltip.this);
                }

                public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                    if (toActor != null && toActor.isDescendantOf(event.getListenerActor()))
                        return;
                    hide();
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                }
            };
        }
    }

    private void getTooltip(StarSystem ss) {
        tooltipTable.clearChildren();
        String text;
        if (!ss.getScanned(playerRace.getRaceId()))
            text = StringDB.getString("UNKNOWN");
        else if (ss.getAnomaly() != null)
            text = ss.getAnomaly().getMapName(ss.getCoordinates());
        else
            text = ss.coordsName(ss.getKnown(playerRace.getRaceId()), playerRace);
        Label l = new Label(text, skin, tooltipHeaderFont, tooltipHeaderColor);
        tooltipTable.add(l);
        tooltipTable.row();

        //if there are defenses
        if (ss.isSunSystem() && !ss.isFree()
                && (ss.getScanPower(playerRace.getRaceId()) > 50 || ss.getOwnerId().equals(playerRace.getRaceId()))) {
            ObjectIntMap<String> onlineDefenseBuildings = new ObjectIntMap<String>();
            ObjectIntMap<String> allDefenseBuildings = new ObjectIntMap<String>();
            for (int i = 0; i < ss.getAllBuildings().size; i++) {
                Building building = ss.getAllBuildings().get(i);
                int id = building.getRunningNumber();
                BuildingInfo bi = resourceManager.getBuildingInfo(id);
                if (bi.getShipDefend() > 0 || bi.getShipDefendBonus() > 0 || bi.getGroundDefend() > 0
                        || bi.getGroundDefendBonus() > 0 || bi.getShieldPower() > 0 || bi.getShieldPowerBonus() > 0) {
                    int value = allDefenseBuildings.get(bi.getBuildingName(), 0);
                    allDefenseBuildings.put(bi.getBuildingName(), ++value);
                    if (building.getIsBuildingOnline() || bi.getNeededEnergy() == 0 || bi.getAlwaysOnline()) {
                        value = onlineDefenseBuildings.get(bi.getBuildingName(), 0);
                        onlineDefenseBuildings.put(bi.getBuildingName(), ++value);
                    }
                }
            }

            text = "";
            if (allDefenseBuildings.size != 0) {
                for (Iterator<Entry<String>> iter = allDefenseBuildings.entries().iterator(); iter.hasNext();) {
                    Entry<String> e = iter.next();
                    int onlines = onlineDefenseBuildings.get(e.key, 0);
                    text += String.format("%s: %d/%d\n", e.key, onlines, e.value);
                }
                text = text.trim();
                l = new Label(text, skin, tooltipTextFont, tooltipTextColor);
                l.setAlignment(Align.center);
                l.setWrap(true);
                tooltipTable.add(l).width(GameConstants.wToRelative(220));
                tooltipTable.row();
            }
        } else if (ss.getAnomaly() != null && ss.getScanned(playerRace.getRaceId())) {
            text = ss.getAnomaly().getGameplayDescription();
            l = new Label(text, skin, tooltipTextFont, tooltipTextColor);
            l.setAlignment(Align.center);
            l.setWrap(true);
            tooltipTable.add(l).width(GameConstants.wToRelative(200));
            tooltipTable.row();
        }

        if (ss.getScanned(playerRace.getRaceId())) {
            ss.drawStationData(tooltipTable, skin, tooltipTextFont, tooltipTextColor);
        }

        if (resourceManager.getUniverseMap().getRenderer().isShipMove()) {
            Ships current = resourceManager.getUniverseMap().getCurrentShip();
            if (UniverseRenderer.shouldShowWinningChance(playerRace, current, ss)) {
                Array<Ships> involvedShips = new Array<Ships>();
                involvedShips.add(current);
                ArrayMap<String, ShipMap> attacked = ss.getShipsInSector();
                for (int i = 0; i < attacked.size; i++)
                    for (int j = 0; j < attacked.getValueAt(i).getSize(); j++)
                        involvedShips.add(attacked.getValueAt(i).getAt(j));
                ObjectSet<String> dummy1 = new ObjectSet<String>();
                ObjectSet<String> dummy2 = new ObjectSet<String>();
                double chance = Combat.getWinningChance(playerRace, involvedShips, resourceManager.getRaceController(), dummy1,
                        dummy2, ss.getAnomaly(), true, true);

                text = String.format("%s: %.0f%%", StringDB.getString("WINNING_CHANCE"), chance * 100);
                l = new Label(text, skin, tooltipTextFont, tooltipTextColor);
                l.setAlignment(Align.center);
                l.setWrap(true);
                tooltipTable.add(l).width(GameConstants.wToRelative(200));
                tooltipTable.row();
            }
        }
    }

    @Override
    protected void setContainerPosition(Actor actor, float x, float y) {
        Stage stage = actor.getStage();
        if (stage == null)
            return;

        container.pack();
        float offsetX = GamePreferences.spriteSize / 4, offsetY = GamePreferences.spriteSize / 4;
        Vector2 point = stage.stageToScreenCoordinates(new Vector2(x + offsetX, y + offsetY));
        container.setPosition(point.x, GamePreferences.sceneHeight - point.y);
    }
}