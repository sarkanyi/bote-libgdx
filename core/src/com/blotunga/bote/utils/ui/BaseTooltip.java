/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.blotunga.bote.constants.GameConstants;

/** A listener that shows a tooltip actor when another actor is hovered over with the mouse.
 * @author Nathan Sweet */
public abstract class BaseTooltip<T extends Actor> {
    protected static Vector2 tmp = new Vector2();

    private final TooltipManager manager;
    final Container<T> container;
    boolean instant;

    boolean always;
    Actor targetActor;
    private EventListener listener;

    /** @param contents May be null. */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public BaseTooltip (T contents) {
        this.manager = TooltipManager.getInstance();

        container = new Container(contents) {
            public void act (float delta) {
                super.act(delta);
                if (targetActor != null && targetActor.getStage() == null) remove();
            }
        };
        container.setTouchable(Touchable.disabled);
        container.pack();
        listener = createListener();
    }

    protected abstract EventListener createListener ();

    public TooltipManager getManager () {
        return manager;
    }

    public Container<T> getContainer () {
        return container;
    }

    public EventListener getListener () {
        return listener;
    }

    public void setListener (EventListener eventListener) {
        this.listener = eventListener;
    }

    public void setActor (T contents) {
        container.setActor(contents);
        container.pack();
    }

    public T getActor () {
        return container.getActor();
    }

    public void setTargetActor (Actor actor) {
        this.targetActor = actor;
    }

    public Actor getTargetActor () {
        return targetActor;
    }

    /** If true, this tooltip is shown without delay when hovered. */
    public void setInstant (boolean instant) {
        this.instant = instant;
    }

    public boolean isInstant () {
        return instant;
    }

    /** If true, this tooltip is shown even when tooltips are not {@link TooltipManager#enabled}. */
    public void setAlways (boolean always) {
        this.always = always;
    }

    public boolean isAlways () {
        return always;
    }

    public void hide () {
        manager.hide(this);
    }

    /**
     * Helper function to create a Table tooltip far any actor
     * @param actor - the actor which owns the tooltip
     * @param bgtexture - the background of the table
     */
    public static BaseTooltip<Table> createTableTooltip(Actor actor, Texture bgtexture) {
        BaseTooltip<Table> tooltip;
        Color backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.9f);
        Table tooltipTable = new Table();
        float vpad = GameConstants.hToRelative(25);
        float hpad = GameConstants.wToRelative(25);
        tooltipTable.pad(vpad, hpad, vpad, hpad);
        Sprite sp = new Sprite(bgtexture);
        sp.setColor(backgroundColor);
        SpriteDrawable dr = new SpriteDrawable(sp);
        tooltipTable.setBackground(dr);

        if(Gdx.app.getType() == ApplicationType.Android) {
            tooltip = new LongPressTooltip<Table>(tooltipTable);
            tooltip.setInstant(true);
        } else {
            tooltip = new Tooltip<Table>(tooltipTable);
            tooltip.setInstant(false);
        }
        actor.addListener(tooltip.getListener());
        return tooltip;
    }

    protected void setContainerPosition (Actor actor, float x, float y) {
        this.targetActor = actor;
        Stage stage = actor.getStage();
        if (stage == null) return;

        container.setSize(manager.maxWidth, Integer.MAX_VALUE);
        container.validate();
        container.width(container.getActor().getWidth());
        container.pack();
        float offsetX = manager.offsetX, offsetY = manager.offsetY, dist = manager.edgeDistance;
        Vector2 point = actor.localToStageCoordinates(tmp.set(x + offsetX, y - offsetY - container.getHeight()));
        if (point.y < dist) point = actor.localToStageCoordinates(tmp.set(x + offsetX, y + offsetY));
        if (point.x < dist) point.x = dist;
        if (point.x + container.getWidth() > stage.getWidth() - dist) point.x = stage.getWidth() - dist - container.getWidth();
        if (point.y + container.getHeight() > stage.getHeight() - dist) point.y = stage.getHeight() - dist - container.getHeight();
        container.setPosition(point.x, point.y);

        point = actor.localToStageCoordinates(tmp.set(actor.getWidth() / 2, actor.getHeight() / 2));
        point.sub(container.getX(), container.getY());
        container.setOrigin(point.x, point.y);
    }
}