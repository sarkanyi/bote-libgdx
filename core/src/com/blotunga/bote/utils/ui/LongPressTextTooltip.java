/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip.TextTooltipStyle;
import com.badlogic.gdx.utils.Null;
import com.badlogic.gdx.utils.Align;

/** A tooltip that shows a label.
 * @author Nathan Sweet */
public class LongPressTextTooltip extends LongPressTooltip<Label> {
	public LongPressTextTooltip (@Null String text, Skin skin) {
		this(text, skin.get(TextTooltipStyle.class));
	}

	public LongPressTextTooltip (@Null String text, Skin skin, String styleName) {
		this(text, skin.get(styleName, TextTooltipStyle.class));
	}

	public LongPressTextTooltip (@Null String text, TextTooltipStyle style) {
		super(null);

		Label label = newLabel(text, style.label);
		label.setWrap(true);
		label.setAlignment(Align.center);
		container.fill().setActor(label);
		setStyle(style);
	}

	protected Label newLabel (String text, LabelStyle style) {
		return new Label(text, style);
	}

	public void setStyle (TextTooltipStyle style) {
		if (style == null) throw new NullPointerException("style cannot be null");
		container.getActor().setStyle(style.label);
		container.setBackground(style.background);
		container.maxWidth(style.wrapWidth);
	}
}
