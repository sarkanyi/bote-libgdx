/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.TutorialType;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.ui.screens.EmpireScreen;
import com.blotunga.bote.ui.screens.IntelScreen;
import com.blotunga.bote.ui.screens.MainMenu;
import com.blotunga.bote.ui.screens.ResearchScreen;
import com.blotunga.bote.ui.screens.ShipDesignScreen;
import com.blotunga.bote.ui.screens.SystemBuildScreen;
import com.blotunga.bote.ui.screens.UniverseRenderer;

public class HelpWidget implements Disposable {
    private Image helpImage;
    private TextureRegion helpTexture;
    private DefaultScreen parent;
    private RepeatAction repeater;
    private TutorialType type;

    public HelpWidget(final ScreenManager screenManager, Rectangle rect, final DefaultScreen parent, Stage stage) {
        this.parent = parent;
        initType();

        helpTexture = screenManager.getUiTexture("question");
        Sprite sp = new Sprite(helpTexture);
        sp.setColor(Color.YELLOW);
        SpriteDrawable drawable = new SpriteDrawable(sp);
        helpImage = new Image(drawable);
        helpImage.setVisible(true);
        helpImage.setBounds(rect.x, rect.y, rect.width, rect.height);
        helpImage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                screenManager.getGameSettings().tutorialSeen[type.getType()] = true;
                stopFadeIn();
                if(parent instanceof MainMenu)
                    ((MainMenu) parent).showTutorial();
                else
                    screenManager.setView(ViewTypes.TUTORIAL_SCREEN, true);
                return false;
            }
        });

        stage.addActor(helpImage);
        helpImage.setVisible(shouldBeVisible());

        AlphaAction actionFadeOut = new AlphaAction();
        actionFadeOut.setAlpha(0.1f);
        actionFadeOut.setDuration(2f);
        AlphaAction actionFadeIn = new AlphaAction();
        actionFadeIn.setAlpha(1f);
        actionFadeIn.setDuration(2f);
        SequenceAction sequence = new SequenceAction();
        sequence.addAction(Actions.delay(2f));
        sequence.addAction(actionFadeOut);
        sequence.addAction(Actions.delay(2f));
        sequence.addAction(actionFadeIn);
        repeater = new RepeatAction();
        repeater.setAction(sequence);
        repeater.setCount(20);
        helpImage.addAction(repeater);
        if(screenManager.getGameSettings().tutorialSeen[type.getType()])
            repeater.finish();
    }

    private void initType() {
        type = TutorialType.TGALAXYMAP; //set type initially to this
        if (parent instanceof SystemBuildScreen)
            type = TutorialType.TSYSTEMBUILDMENU;
        else if (parent instanceof ResearchScreen)
            type = TutorialType.TRESEARCHMENU;
        else if (parent instanceof DiplomacyScreen)
            type = TutorialType.TDIPLOMACYMENU;
        else if (parent instanceof EmpireScreen)
            type = TutorialType.TEMPIREMENU;
        else if (parent instanceof IntelScreen)
            type = TutorialType.TINTELMENU;
        else if (parent instanceof MainMenu)
            type = TutorialType.TINITIALSETTINGS;
        else if (parent instanceof ShipDesignScreen)
            type = TutorialType.TSHIPDESIGN;
    }

    public void setType(TutorialType type) {
        this.type = type;
    }

    public boolean shouldBeVisible() {
        return (parent instanceof UniverseRenderer)
                || (parent instanceof SystemBuildScreen)
                || (parent instanceof ResearchScreen)
                || (parent instanceof DiplomacyScreen)
                || (parent instanceof EmpireScreen)
                || (parent instanceof IntelScreen)
                || (parent instanceof MainMenu)
                || (parent instanceof ShipDesignScreen);
    }

    public void stopFadeIn() {
        repeater.finish();
    }

    public void startFadeIn() {
        repeater.restart();
    }

    public void hide() {
        helpImage.setVisible(false);
    }

    public void show() {
        helpImage.setVisible(true);
    }

    @Override
    public void dispose() {
    }
}
