/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GestureCameraController extends GestureAdapter {
    public interface GestureEvent {
        void zoomed();
        void moved();
    }

    final OrthographicCamera camera;
    final Vector3 curr = new Vector3();
    final Vector3 last = new Vector3(-1, -1, -1);
    final Vector3 delta = new Vector3();
    float origDistance;
    float origZoom;
    float minZoom;
    float maxZoom;
    private GestureEvent gestureEvent;
    private boolean enabled;

    public GestureCameraController(OrthographicCamera camera) {
        this(camera, (float) 1280.0f / Gdx.graphics.getWidth(), 10 * (float) 1280.0f / Gdx.graphics.getWidth());
    }

    public GestureCameraController(OrthographicCamera camera, float minZoom, float maxZoom) {
        this.camera = camera;
        this.minZoom = minZoom;
        this.maxZoom = maxZoom;
        gestureEvent = null;
        enabled = true;
    }

    public void setGestureHandler(GestureEvent handler) {
        gestureEvent = handler;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        if(enabled) {
            if (initialDistance != origDistance) {
                origDistance = initialDistance;
                origZoom = camera.zoom;
            }
            float amount = initialDistance / distance;
            camera.zoom = origZoom * amount;
            if (amount > 1 && camera.zoom > maxZoom)
                camera.zoom = maxZoom;
            else if (amount < 1 && camera.zoom < minZoom)
                camera.zoom = minZoom;
    
            if (gestureEvent != null)
                gestureEvent.zoomed();
        } else
            return false;
        return true;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    public boolean fling (float velocityX, float velocityY, int button) {
        if(gestureEvent != null)
            gestureEvent.moved();
        return super.fling(velocityX, velocityY, button);
    }
}
