/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.tests.utils.OrthoCamController;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.ResourceManager;

public class ZoomWithLock extends InputMultiplexer implements Disposable {
    private GestureCameraController cameraControllerGesture;
    public OrthoCamController cameraControllerOrtho;
    private GestureDetector cameraGestureDetector;
    private float initialZoom;
    private boolean zoomable;
    private TextureRegion lockTexture;
    private Stage stage;
    private OrthographicCamera camera;
    private Image lockImage;

    public ZoomWithLock(final ResourceManager manager, OrthographicCamera camera, Stage stage, Rectangle lockBounds) {
        this.camera = camera;
        this.stage = stage;
        cameraControllerGesture = new GestureCameraController(camera, 0.4f, 1.0f);
        cameraGestureDetector = new GestureDetector(cameraControllerGesture);
        cameraControllerOrtho = new OrthoCamController(camera, 0.4f, 0.8f);
        this.addProcessor(cameraGestureDetector);
        this.addProcessor(cameraControllerOrtho);
        this.addProcessor(stage);

        initialZoom = camera.zoom;
        zoomable = false;
        cameraControllerOrtho.setEnabled(zoomable);
        cameraControllerGesture.setEnabled(zoomable);

        lockTexture = manager.getUiTexture("lock_closed");
        Sprite sp = new Sprite(lockTexture);
        sp.setColor(Color.RED);
        SpriteDrawable drawable = new SpriteDrawable(sp);
        lockImage = new Image(drawable);
        lockImage.setVisible(true);
        lockImage.setBounds(lockBounds.x, lockBounds.y, lockBounds.width, lockBounds.height);
        lockImage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                zoomable = !zoomable;
                cameraControllerOrtho.setEnabled(zoomable);
                cameraControllerGesture.setEnabled(zoomable);
                String path = "lock_closed";
                Color color = Color.RED;
                if(zoomable){
                    path = "lock_open";
                    color = Color.GREEN;
                } else{
                    resetPositionAndZoom();
                }
                lockTexture = manager.getUiTexture(path);
                Sprite sp = new Sprite(lockTexture);
                sp.setColor(color);
                SpriteDrawable drawable = new SpriteDrawable(sp);
                lockImage.setDrawable(drawable);
                return false;
            }
        });
    }

    public void resetPositionAndZoom() {
        camera.zoom = initialZoom;
        camera.update();
        stage.getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    }

    public Image getImage() {
        return lockImage;
    }

    public boolean isZoomable() {
        return zoomable;
    }

    public void setLockBounds(Rectangle rect) {
        lockImage.setBounds(rect.x, rect.y, rect.width, rect.height);
    }

    @Override
    public void dispose() {
    }
}
