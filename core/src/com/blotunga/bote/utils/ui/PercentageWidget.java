/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

public class PercentageWidget {
    private int value;
    private int typeID;
    private ValueChangedEvent valueEventHandler;
    private Button button;

    public PercentageWidget(int type, Skin skin, float width, float height, float pad, int numberOfItems, int min,
            int max, int initialValue, TextureRegion dr, ValueChangedEvent handler) {
        this(type, skin, width, height, pad, numberOfItems, min, max, initialValue, dr, handler, false);
    }

    /**
     * @param type - the type of the value
     * @param skin - skin to use for getting buttons etc.
     * @param width - width of the items
     * @param height - height of the items
     * @param pad - pad between the items
     * @param numberOfItems - number of items
     * @param min - max value
     * @param max - min value
     * @param initialValue - initial value
     * @param dr - drawable which holds the texture
     * @param handler - class that implements the ValueChangedEvent interface
     * @param disabled - true if it should be disabled
     */
    public PercentageWidget(int type, Skin skin, float width, float height, float pad, int numberOfItems,
            final int min, final int max, int initialValue, TextureRegion dr, ValueChangedEvent handler,
            boolean disabled) {
        valueEventHandler = handler;
        value = initialValue;
        typeID = type;
        Button.ButtonStyle bs = new Button.ButtonStyle();
        button = new Button(bs);

        TextButtonStyle minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        TextButton minusButton = new TextButton("", minusStyle);
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                value--;
                value = Math.max(value, min);
                valueEventHandler.valueChanged(typeID, value);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                value -= 5;
                value = Math.max(value, min);
                valueEventHandler.valueChanged(typeID, value);
                return true;
            }
        };

        gestureListener.getGestureDetector().setLongPressSeconds(0.8f);
        minusButton.addListener(gestureListener);

        TextButtonStyle plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");
        TextButton plusButton = new TextButton("", plusStyle);
        plusButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                value++;
                value = Math.min(value, max);
                valueEventHandler.valueChanged(typeID, value);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                value += 5;
                value = Math.min(value, max);
                valueEventHandler.valueChanged(typeID, value);
                return true;
            }
        });

        if (!disabled)
            button.add(minusButton).width((int) (width * 3)).height((int) (height * 1.5f));
        else
            button.add(new Image()).width((int) (width * 3)).height((int) (height * 1.5f));
        for (int i = min + 1; i < max + 1; i += (max - min) / numberOfItems) {
            Sprite sprite = new Sprite(dr);
            Color color;
            int cmod = i - min;
            if (max != 0)
                cmod *= (100.0f / max);
            if (i <= value)
                color = new Color((250 - cmod * 2.5f) / 255.0f, (50 + cmod * 2) / 255.0f, 0, 1.0f);
            else
                color = new Color(75 / 255.0f, 75 / 255.0f, 75 / 255.0f, 1.0f);
            if (disabled)
                color = new Color(42 / 255.0f, 46 / 255.0f, 30 / 255.0f, 1.0f);
            sprite.setColor(color);
            Drawable newDrawable = new SpriteDrawable(sprite);
            Image img = new Image(newDrawable);
            img.setUserObject(i + (max / numberOfItems) / 2);
            if (!disabled)
                img.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        int newPercentage = (Integer) event.getListenerActor().getUserObject();
                        value = newPercentage;
                        valueEventHandler.valueChanged(typeID, value);
                    }
                });
            button.add(img).width((int) width).height((int) height).spaceLeft((int) pad);
        }
        if (!disabled)
            button.add(plusButton).width((int) (width * 3)).height((int) height * 1.5f).spaceLeft((int) pad);
        else
            button.add(new Image()).width((int) (width * 3)).height((int) height * 1.5f).spaceLeft((int) pad);
    }

    public Button getWidget() {
        return button;
    }
}
