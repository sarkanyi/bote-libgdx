/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.general.StringDB;

public class ScrollEventHandler implements ScrollEvent {
    private TextButton prevButton;
    private TextButton nextButton;
    private int currentPage;
    private int pageCnt;
    private int itemHeight;
    private int itemInPage;

    public ScrollEventHandler(Rectangle rect, Stage stage, TextButtonStyle style, final VerticalScroller parent,
            final int itemHeight, final int itemInPage, final int pageCnt) {
        this.itemHeight = itemHeight;
        this.itemInPage = itemInPage;
        currentPage = 0;
        this.pageCnt = pageCnt;

        prevButton = new TextButton(StringDB.getString("BTN_BACK"), style);
        prevButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        prevButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentPage--;
                if (currentPage >= 0) {
                    parent.setScrollY(currentPage * itemHeight * itemInPage);
                    nextButton.setVisible(true);
                }
                if (currentPage == 0) {
                    prevButton.setVisible(false);
                }
            }
        });
        nextButton = new TextButton(StringDB.getString("BTN_FURTHER"), style);
        nextButton.setBounds((int) rect.x + prevButton.getWidth(), (int) rect.y, (int) rect.width, (int) rect.height);
        nextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentPage++;
                if (currentPage <= pageCnt) {
                    parent.setScrollY(currentPage * itemHeight * itemInPage);
                    prevButton.setVisible(true);
                }
                if (currentPage == pageCnt) {
                    nextButton.setVisible(false);
                }
            }
        });
        if (pageCnt > 0) {
            stage.addActor(nextButton);
            stage.addActor(prevButton);
            prevButton.setVisible(false);
        }
    }

    @Override
    public void scrollEventY(float pixelsY) {
        currentPage = (int) Math.ceil(pixelsY / (itemHeight * itemInPage));
        showNextPrevButtonsIfNeeded();
    }

    private void showNextPrevButtonsIfNeeded() {
        if (currentPage == pageCnt) {
            nextButton.setVisible(false);
        } else {
            nextButton.setVisible(true);
        }
        if (currentPage == 0) {
            prevButton.setVisible(false);
        } else {
            prevButton.setVisible(true);
        }
    }
}
