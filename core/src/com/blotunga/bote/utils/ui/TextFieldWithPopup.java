/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.constants.GameConstants;

public class TextFieldWithPopup extends TextField {
    private Table label;
    private Task task;

    public TextFieldWithPopup(String text, Skin skin, Texture labelBgTex, Color textColor) {
        super(text, skin);
        label = new Table();
        Color backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.9f);
        Sprite sp = new Sprite(labelBgTex);
        sp.setColor(backgroundColor);
        label.setBackground(new SpriteDrawable(sp));
        Rectangle rect = GameConstants.coordsToRelative(570, 470, 300, 50);
        Label l = new Label(text, skin, "vsFont", textColor);
        l.setName("SEED_VALUE");
        label.add(l);
        label.setBounds(rect.x, rect.y, rect.width, rect.height);
        label.setVisible(false);
        label.setHeight(skin.getFont("vsFont").getLineHeight() + GameConstants.hToRelative(5));
        task = new Task() {
            @Override
            public void run() {
                label.setVisible(false);
                task.cancel();
            }
        };
        this.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                 if (Gdx.app.getType() == ApplicationType.Android) { 
                     task.cancel();
                     Timer.schedule(task, 1.2f, 0.1f);
                 }
                return false;
            }
        });
        this.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char key) {
                if (key == '\r' || key == '\n') {
                    textField.getOnscreenKeyboard().show(false);
                    label.setVisible(false);
                } else if (Gdx.app.getType() == ApplicationType.Android) {
                    label.setVisible(true);
                    Label l = label.findActor("SEED_VALUE");
                    l.setText(textField.getText());
                    label.setWidth(l.getPrefWidth() + GameConstants.wToRelative(40));
                    label.setX(GamePreferences.sceneWidth / 2 - label.getWidth() / 2);
                    task.cancel();
                    Timer.schedule(task, 1.2f, 0.1f);
                }
            }
        });
    }

    @Override
    public void setStage(Stage stage) {
        super.setStage(stage);
        if (stage != null)
            stage.addActor(label);
        else
            label.remove();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        label.setVisible(visible);
    }
}
