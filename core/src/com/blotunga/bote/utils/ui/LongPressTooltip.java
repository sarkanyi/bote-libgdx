/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class LongPressTooltip<T extends Actor> extends BaseTooltip<T> {
    private boolean longPressFired;
    private Actor actor;
    private float x, y;
    private boolean touchedUp;
    private float showTime = 2f;
    
    public LongPressTooltip (T contents) {
        super(contents);
    }

    private final Task longPressTask = new Task() {
        @Override
        public void run() {
            if (!longPressFired) {
                setContainerPosition(actor, x, y);
                getManager().enter(LongPressTooltip.this);
                longPressFired = true;
                if(!hideTask.isScheduled())
                    Timer.schedule(hideTask, LongPressTooltip.this.showTime);
            }
        }
    };

    final Task hideTask = new Task() {
        public void run() {
            if(touchedUp)
                getManager().hide(LongPressTooltip.this);
        }
    };

    @Override
    protected EventListener createListener () {
        return new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if (pointer > 1) return false;

                if (pointer == 0) {
                    if (!Gdx.input.isTouched(1)) {
                        getManager().hide(LongPressTooltip.this);
                        longPressFired = false;
                        touchedUp = false;
                        LongPressTooltip.this.x = x;
                        LongPressTooltip.this.y = y;
                        LongPressTooltip.this.actor = event.getListenerActor();
                        if (!longPressTask.isScheduled())
                            Timer.schedule(longPressTask, 0.6f);
                        return true;
                    }
                } else {
                    longPressFired = false;
                    longPressTask.cancel();
                }

                return false;
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                longPressTask.cancel();
                touchedUp = true;
                if(!hideTask.isScheduled())
                    Timer.schedule(hideTask, getManager().initialTime + getManager().subsequentTime);
            }
        };        
    }
}