/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

public class DynamicTextureManager {
    private ObjectMap<TextureRegion, Pixmap> theTextureMap;

    public DynamicTextureManager() {
        theTextureMap = new ObjectMap<TextureRegion, Pixmap>();
    }

    public void register(TextureRegion t, Pixmap p) {
        theTextureMap.put(t, p);
    }

    public void unregisterAndDispose(TextureRegion t) {
        theTextureMap.get(t).dispose();
        t.getTexture().dispose();
        theTextureMap.remove(t);
    }

    private void disposeAll() {
        for (Iterator<ObjectMap.Entry<TextureRegion, Pixmap>> iter = theTextureMap.entries().iterator(); iter.hasNext(); ) {
            ObjectMap.Entry<TextureRegion, Pixmap> e = iter.next();
            e.key.getTexture().dispose();
            e.value.dispose();
        }
    }

    public Pixmap getPixmap(TextureRegion t) {
        return theTextureMap.get(t);
    }

    public void clear() {
        disposeAll();
        theTextureMap.clear();
    }
}