/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Iterator;

import com.badlogic.gdx.utils.ObjectFloatMap;
import com.badlogic.gdx.utils.ObjectFloatMap.Entry;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

@SuppressWarnings("rawtypes")
public class ObjectFloatMapSerializer extends Serializer<ObjectFloatMap> {
    private Class keyClass;
    private Serializer keySerializer;
    private Class keyGenericType;

    /** @param keyClass The concrete class of each key. This saves 1 byte per key. Set to null if the class is not known or varies
     *           per key (default).
     * @param keySerializer The serializer to use for each key. */
    public void setKeyClass(Class keyClass, Serializer keySerializer) {
        this.keyClass = keyClass;
        this.keySerializer = keySerializer;
    }

    @Override
    public void setGenerics(Kryo kryo, Class[] generics) {
        keyGenericType = null;

        if (generics != null && generics.length > 0) {
            if (generics[0] != null && kryo.isFinal(generics[0]))
                keyGenericType = generics[0];
        }
    }

    @Override
    public void write(Kryo kryo, Output output, ObjectFloatMap map) {
        int length = map.size;
        output.writeInt(length, true);

        Serializer keySerializer = this.keySerializer;
        if (keyGenericType != null) {
            if (keySerializer == null)
                keySerializer = kryo.getSerializer(keyGenericType);
            keyGenericType = null;
        }
        for (Iterator iter = map.entries().iterator(); iter.hasNext();) {
            Entry entry = (Entry) iter.next();
            if (keySerializer != null) {
                kryo.writeObject(output, entry.key, keySerializer);
            } else
                kryo.writeClassAndObject(output, entry.key);
            output.writeFloat(entry.value);
        }
    }

    /** Used by {@link #read(Kryo, Input, Class)} to create the new object. This can be overridden to customize object creation, eg
     * to call a constructor with arguments. The default implementation uses {@link Kryo#newInstance(Class)}. */
    protected ObjectFloatMap create(Kryo kryo, Input input, Class<ObjectFloatMap> type) {
        return kryo.newInstance(type);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ObjectFloatMap read(Kryo kryo, Input input, Class<ObjectFloatMap> type) {
        ObjectFloatMap map = create(kryo, input, type);
        int length = input.readInt(true);

        Class keyClass = this.keyClass;

        Serializer keySerializer = this.keySerializer;
        if (keyGenericType != null) {
            keyClass = keyGenericType;
            if (keySerializer == null)
                keySerializer = kryo.getSerializer(keyClass);
            keyGenericType = null;
        }

        kryo.reference(map);

        for (int i = 0; i < length; i++) {
            Object key;
            if (keySerializer != null) {
                key = kryo.readObject(input, keyClass, keySerializer);
            } else
                key = kryo.readClassAndObject(input);
            float value = input.readInt();
            map.put(key, value);
        }
        return map;
    }

    protected ObjectFloatMap createCopy(Kryo kryo, ObjectFloatMap original) {
        return kryo.newInstance(original.getClass());
    }

    @SuppressWarnings("unchecked")
    @Override
    public ObjectFloatMap copy(Kryo kryo, ObjectFloatMap original) {
        ObjectFloatMap copy = createCopy(kryo, original);
        for (Iterator iter = original.entries().iterator(); iter.hasNext();) {
            Entry entry = (Entry) iter.next();
            copy.put(kryo.copy(entry.key), kryo.copy(entry.value));
        }
        return copy;
    }

    /**
     * Used to annotate fields that are maps with specific Kryo serializers for
     * their keys or values.
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface BindMap {
        /**
         * Serializer to be used for keys
         *
         * @return the class<? extends serializer> used for keys serialization
         */
        Class<? extends Serializer> keySerializer() default Serializer.class;

        /**
         * Serializer to be used for values
         *
         * @return the class<? extends serializer> used for values serialization
         */
        Class<? extends Serializer> valueSerializer() default Serializer.class;

        /**
         * Class used for keys
         *
         * @return the class used for keys
         */
        Class<?> keyClass() default Object.class;
    }
}