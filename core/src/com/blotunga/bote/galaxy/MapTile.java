/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.EntityType;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.general.InGameEntity;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ObjectSetSerializer;
import com.blotunga.bote.utils.Pair;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * @author dragon
 *
 */
public class MapTile extends InGameEntity {
    public enum DiscoverStatus {
        DISCOVER_STATUS_NONE(0),
        DISCOVER_STATUS_SCANNED(1),
        DISCOVER_STATUS_KNOWN(2),
        DISCOVER_STATUS_FULL_KNOWN(3);

        private int status;

        DiscoverStatus(int status) {
            this.status = status;
        }

        public int getValue() {
            return status;
        }
    }

    private boolean sunSystem;

    /// Variable stores statues of the sector, where 0 -> nothing, 1 -> scanned,
    /// 2 -> name known, 3 -> everything including planets are known
    private ObjectMap<String, DiscoverStatus> statusMap;
    /// Is there a shipyard present
    private ObjectSet<String> shipPort;
    //The ships currently in this map tile, by race
    //main shipmap responsible for updating, ships in fleets included inside of the ships in this variable
    protected transient ArrayMap<String, ShipMap> ships;
    /// Is a major race building a station in the sector?
    private ObjectMap<String, ShipOrder> isStationBuild;
    /// Scan strength of the races in this sector
    private ObjectIntMap<String> scanPower;
    /// Scan power needed by a major race in order to detect a ship in this sector
    private ObjectIntMap<String> neededScanPower;
    /// The build points which are needed to complete station
    private ObjectIntMap<String> neededStationPoints;
    /// The build points which are needed to build station (for percent display)
    private ObjectIntMap<String> startStationPoints;
    /// Points which show who has the most influence in this sector. The one with the most points controls
    /// this sector if nobody else has an Outpost or Colony in this sector
    private ObjectIntMap<String> ownerPoints;
    /// possible anomaly in this sector. Null if none present
    private Anomaly anomaly;
    private transient Array<Image> images;
    private transient Image[] pathImages;

    MapTile() {
        this(null);
    }

    MapTile(ResourceManager res) {
        this(-1, -1, res);
    }

    MapTile(int x, int y, ResourceManager res) {
        super(x, y, res);
        type = EntityType.SECTOR;
        sunSystem = false;
        statusMap = new ObjectMap<String, DiscoverStatus>(1);
        shipPort = new ObjectSet<String>();
        ships = new ArrayMap<String, ShipMap>(1);
        isStationBuild = new ObjectMap<String, ShipOrder>(1);
        scanPower = new ObjectIntMap<String>(1);
        neededScanPower = new ObjectIntMap<String>(1);
        neededStationPoints = new ObjectIntMap<String>(1);
        startStationPoints = new ObjectIntMap<String>(1);
        ownerPoints = new ObjectIntMap<String>(1);
        anomaly = null;
        images = new Array<Image>();
        pathImages = new Image[9];
    }

    protected void reset(boolean callUp) {
        if (callUp)
            super.reset();
        sunSystem = false;
        anomaly = null;
        statusMap.clear();
        startStationPoints.clear();
        neededStationPoints.clear();
        clearAllPoints(false);
        resetImages();
    }

    /**
     * In the beginning of each turn we have to set the isStationBuild to false, later
     * if ships make actions these will be corrected.
     */
    protected void clearAllPoints(boolean callUp) {
        // Function should be called in the beginning of each turn!!! If no station is built
        // then we can set the used points to null
        ownerPoints.clear();

        for (Iterator<ObjectIntMap.Entry<String>> iter = startStationPoints.entries().iterator(); iter.hasNext();) {
            ObjectIntMap.Entry<String> e = iter.next();
            if (e.value == 0)
                iter.remove();
        }

        isStationBuild.clear();
        neededScanPower.clear();
        scanPower.clear();
        shipPort.clear();
    }

    /**
     * @return true if a star is present
     */
    public boolean isSunSystem() {
        return sunSystem;
    }

    /**
     * Returns whether a system was scanned by
     * @param race
     * @return true if yes
     */
    public boolean getScanned(String race) {
        if (resourceManager.getGamePreferences().seeAllofMap)
            return true;

        if (statusMap.containsKey(race)) {
            return statusMap.get(race).getValue() >= DiscoverStatus.DISCOVER_STATUS_SCANNED.getValue();
        } else
            return false;
    }

    /**
     * Returns whether a system is known by
     * @param race
     * @return true if yes
     */
    public boolean getKnown(String race) {
        if (resourceManager.getGamePreferences().seeAllofMap)
            return true;

        if (statusMap.containsKey(race)) {
            return statusMap.get(race).getValue() >= DiscoverStatus.DISCOVER_STATUS_KNOWN.getValue();
        } else
            return false;
    }

    /**
     * Returns whether a system is fully known (including planets) by
     * @param race
     * @return true if yes
     */
    public boolean getFullKnown(String race) {
        if (resourceManager.getGamePreferences().seeAllofMap)
            return true;

        if (statusMap.containsKey(race)) {
            return statusMap.get(race).getValue() >= DiscoverStatus.DISCOVER_STATUS_FULL_KNOWN.getValue();
        } else
            return false;
    }

    public DiscoverStatus getDiscoverStatus(String race) {
        if (resourceManager.getGamePreferences().seeAllofMap)
            return DiscoverStatus.DISCOVER_STATUS_FULL_KNOWN;

        if (statusMap.containsKey(race)) {
            return statusMap.get(race);
        } else
            return DiscoverStatus.DISCOVER_STATUS_NONE;
    }

    /**
     * Returns whether a major race possesses a functioning shipyard (or outpost) in this sector
     * @param race ID
     * @return true if yes
     */
    public boolean getShipPort(String race) {
        return shipPort.contains(race);
    }

    /**
     * @param type
     * @param race if empty string, returns specified station type of any race, otherwise of that race
     * @return null if none found
     */
    public Ships getStation(ShipType type, String race) {
        if (race.isEmpty()) {
            for (Iterator<ObjectMap.Entry<String, ShipMap>> iter = ships.entries().iterator(); iter.hasNext();) {
                ObjectMap.Entry<String, ShipMap> e = iter.next();
                if(e.value.findShipOfType(type) != null)
                    return e.value.findShipOfType(type);
            }
        } else {
            if (ships.containsKey(race)) {
                ShipMap map = ships.get(race);
                return map.findShipOfType(type);
            }
        }
        return null;
    }

    /**
     * This function returns if
     * @param race has a station in the current sector
     * @return null if none found
     */
    public Ships getStation(String race) {
        Ships outpost = getStation(ShipType.OUTPOST, race);
        Ships starbase = getStation(ShipType.STARBASE, race);
        if (outpost != null)
            return outpost;
        else
            return starbase;
    }

    public Ships getStation(ShipType type) {
        return getStation(type, "");
    }

    public Ships getStation() {
        return getStation("");
    }

    /**
     * Returns the info about race if it has one ore more ships in the sector
     * @param race ID
     * @param stationsAsShips will return false for the owner of the station
     * unless the owner race also has non-station (neither outpost nor starbase) ships in this sector
     * @return
     */
    public boolean getOwnerOfShip(String race, boolean stationsAsShips) {
        if (ships.containsKey(race)) {
            if (stationsAsShips)
                return true;
            else {
                ShipMap map = ships.get(race);
                for (Iterator<ObjectMap.Entry<Integer, Ships>> iter = map.getShipMap().entries().iterator(); iter
                        .hasNext();)
                    if (!iter.next().value.isStation())
                        return true;
            }
        }
        return false;
    }

    public ArrayMap<String, ShipMap> getShipsInSector() {
        return ships;
    }

    /**
     * @return true if there are ships present in the sector
     */
    public boolean getIsShipInSector() {
        return ships.size != 0;
    }

    /**
     * Returns if a major race builds a station in the sector
     * @param race ID
     * @return
     */
    public boolean getIsStationBuilding(String race) {
        return isStationBuild.containsKey(race);
    }

    public ShipOrder stationWork(String race) {
        if (getIsStationBuilding(race))
            return isStationBuild.get(race);
        return ShipOrder.NONE;
    }

    /**
     * Returns the number of points needed for race to complete station building in this sector
     * @param race ID
     * @return
     */
    public int getNeededStationPoints(String race) {
        return neededStationPoints.get(race, 0);
    }

    /**
     * Returns the number of points needed for race to start building a station in this sector
     * @param race ID
     * @return
     */
    public int getStartStationPoints(String race) {
        return startStationPoints.get(race, 0);
    }

    /**
     * Returns the scan power of the major race in this sector
     * @param race
     * @return
     */
    public int getScanPower(String race) {
        return getScanPower(race, true);
    }

    public int getScanPower(String race, boolean withShips) {
        if (resourceManager.getGamePreferences().seeAllofMap)
            return 200;
        int scanPowerDueToShipNumber = 0;
        if (withShips) {
            Race r = resourceManager.getRaceController().getRace(race);
            for (int i = 0; i < ships.size; i++) {
                String sr = ships.getKeyAt(i);
                ShipMap sm = ships.getValueAt(i);
                if (sr.equals(r.getRaceId()) || r.getAgreement(sr).getType() >= DiplomaticAgreement.ALLIANCE.getType()) {
                    scanPowerDueToShipNumber += sm.getSize();
                    for (int j = 0; j < sm.getSize(); j++)
                        scanPowerDueToShipNumber += sm.getAt(j).getFleetSize();
                }
            }
        }
        scanPowerDueToShipNumber += scanPower.get(race, 0);
        return scanPowerDueToShipNumber;
    }

    /**
     * This function returns the needed scan power to reveal the ships of
     * @param race major race in this sector
     * @return the value of the needed scan power
     */
    public int getNeededScanPower(String race) {
        return neededScanPower.get(race, Integer.MAX_VALUE);
    }

    /**
     * This function returns a possible anomaly
     * @return anomaly or null if none exist
     */
    public Anomaly getAnomaly() {
        return anomaly;
    }

    /**
     * This function sets a sun into the sector
     * @param is
     */
    public void setSunsystem(boolean is) {
        sunSystem = is;
    }

    /**
     * Sets whether a major race has scanned this sector
     * @param race race id
     */
    public void setScanned(String race) {
        if (getScanned(race) == false)
            statusMap.put(race, DiscoverStatus.DISCOVER_STATUS_SCANNED);
    }

    /**
     * Sets whether a major race knows the name of the sector
     * @param race race id
     */
    public void setKnown(String race) {
        if (getKnown(race) == false)
            statusMap.put(race, DiscoverStatus.DISCOVER_STATUS_KNOWN);
    }

    /**
     * Sets whether a major race knows the sector including planets
     * @param race race id
     */
    public void setFullKnown(String race) {
        statusMap.put(race, DiscoverStatus.DISCOVER_STATUS_FULL_KNOWN);
    }

    private boolean stationBuildContinuable(String race) {
        return (getOwnerId().isEmpty() || (getOwnerId().equals(race))) || getIsStationBuilding(race);
    }

    /**
     * Is station buildable in this sector by race according to whatever station exists ?
     * @param order
     * @param race
     * @return true if yes, false otherwise
     */
    public boolean isStationBuildable(ShipOrder order, String race) {
        if (order == ShipOrder.BUILD_OUTPOST && getStation() == null)
            return stationBuildContinuable(race);
        if (order == ShipOrder.BUILD_STARBASE && getStation(ShipType.OUTPOST, race) != null)
            return stationBuildContinuable(race);
        if ((order == ShipOrder.UPGRADE_OUTPOST && getStation(ShipType.OUTPOST, race) != null)
                || (order == ShipOrder.UPGRADE_STARBASE && getStation(ShipType.STARBASE, race) != null)) {
            Major major = (Major) resourceManager.getRaceController().getRace(race);
            ShipType type = (order == ShipOrder.UPGRADE_OUTPOST) ? ShipType.OUTPOST : ShipType.STARBASE;
            int bestBuildableID = major.bestBuildableVariant(type, resourceManager.getShipInfos());
            if (bestBuildableID == -1)
                return false;
            ShipInfo bestBuildableInfo = resourceManager.getShipInfos().get(bestBuildableID - 10000);
            for (Iterator<ObjectMap.Entry<String, ShipMap>> iter = ships.entries().iterator(); iter.hasNext();) {
                ObjectMap.Entry<String, ShipMap> e = iter.next();
                for (int i = 0; i < e.value.getSize(); i++) {
                    Ships ship = e.value.getAt(i);
                    if (ship.getShipType() == type) {
                        ShipInfo info = resourceManager.getShipInfos().get(ship.getID() - 10000);
                        if (info.getBaseIndustry() < bestBuildableInfo.getBaseIndustry())
                            return stationBuildContinuable(race);
                        Ships temp = new Ships(bestBuildableInfo);
                        temp.addSpecialResearchBoni(major);
                        if (ship.isWorseThan(temp))
                            return stationBuildContinuable(race);
                        break;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Function decides if the race in the parameter is building a station in the sector
     * @param order
     * @param race
     */
    public void setIsStationBuilding(ShipOrder order, String race) {
        if (order != ShipOrder.NONE)
            isStationBuild.put(race, order);
        else
            isStationBuild.remove(race);
    }

    public void setShipPort(boolean is, String race) {
        if (is)
            shipPort.add(race);
        else
            shipPort.remove(race);
    }

    public void buildStation(ShipType station, String race) {
        setScanned(race);
        setShipPort(true, race);
        isStationBuild.clear();
        startStationPoints.clear();
        neededStationPoints.clear();
    }

    /**
     * This function substracts 'sub' points from the needed stationbuildpoints of the major race
     * @param sub
     * @param race
     * @return
     */
    public boolean setNeededStationPoints(int sub, String race) {
        int value = neededStationPoints.get(race, 0) - sub;
        neededStationPoints.put(race, value);
        if (value <= 0) {
            neededStationPoints.remove(race, 0);
            return true;
        } else
            return false;
    }

    /**
     * This function sets the station build points and start points needed to start the build
     * @param value
     * @param race
     */
    public void setStartStationPoints(int value, String race) {
        if (value != 0) {
            startStationPoints.put(race, value);
            neededStationPoints.put(race, value);
        } else {
            startStationPoints.clear();
            neededStationPoints.clear();
        }
    }

    /**
     * Sets the scanpower of the race in this sector
     * @param sp
     * @param race
     */
    public void setScanPower(int sp, String race) {
        if (sp != 0)
            scanPower.put(race, sp);
        else
            scanPower.remove(race, 0);
    }

    public void putScannedSquare(int range, int power, Race race) {
        this.putScannedSquare(range, power, race, false, false);
    }

    public void putScannedSquare(int range, int power, Race race, boolean betterScanner, boolean patrolShip) {
        this.putScannedSquare(range, power, race, betterScanner, patrolShip, false);
    }

    /**
     * Function puts a scanned square over this sector in the middle, if range == 1, 9 sectors are affected
     * for instance. Function calculates and sets the scan power values for each of these sectors.
     * @param range range of the object
     * @param power scan power of the object
     * @param race race who gets these scan powers (can be a minor race)
     * @param betterScanner are we on a scanning improving anomaly (implies the scan power affecting object is a ship or base)
     * @param patrolShip is this a patrolship (implies the scan power affecting object is a ship or base)
     * @param anomaly is the scan power affecting object a scanning deteriorating anomaly
     */
    public void putScannedSquare(int range, int power, Race race, boolean betterScanner, boolean patrolShip,
            boolean anomaly) {
        String raceid = race.getRaceId();
        float bonus = 1.0f;
        //if it's a patrol ship and is in an owned sector, it gets a 20% bonus
        if (patrolShip)
            if (raceid.equals(ownerID)
                    || race.getAgreement(ownerID).getType() >= DiplomaticAgreement.ALLIANCE.getType())
                bonus = 1.2f;
        if (betterScanner) {
            range *= 1.5;
            bonus += 0.5;
        }
        for (int i = -range; i <= range; i++) {
            int x = coordinates.x + i;
            if (0 <= x && x < resourceManager.getGridSizeX()) {
                for (int j = -range; j <= range; j++) {
                    int y = coordinates.y + j;
                    if (0 <= y && y < resourceManager.getGridSizeY()) {
                        MapTile scannedSector = resourceManager.getUniverseMap().getStarSystemAt(x, y);
                        //calculate divisor for scan strength
                        int div = Math.max(Math.abs(i), Math.abs(j));
                        if (anomaly)
                            div *= 2;
                        div = Math.max(div, 1);
                        int oldScanPower = scannedSector.getScanPower(raceid, false);
                        int newScanPower = 0;
                        if (anomaly)
                            newScanPower = oldScanPower + power / div;
                        else {
                            newScanPower = (int) (power * bonus) / div;
                            newScanPower = Math.max(oldScanPower, newScanPower);
                            if (race.isMajor())
                                scannedSector.setScanned(raceid);
                        }
                        scannedSector.setScanPower(newScanPower, raceid);
                    } //0 <= y && y < GamePreferences.gridSizeY)
                } //for(int j = -range; j <= range; j++)
            } //if(0 <= x && x < GamePreferences.gridSizeX)
        } //for(int i = -range; i <= range; i++)
    }

    public void setNeededScanPower(int scanpower, String race) {
        if (scanpower != Integer.MAX_VALUE)
            neededScanPower.put(race, scanpower);
        else
            neededScanPower.remove(race, 0);
    }

    /**
     * Should any their_race_id's ship in this sector be visible to our_race ?
     * not for outposts/starbases
     * @param ourRace
     * @param theirRaceId
     * @return
     */
    public boolean shouldDrawShip(Major ourRace, String theirRaceId) {
        if (!getOwnerOfShip(theirRaceId, false))
            return false;
        String ourId = ourRace.getRaceId();
        if (ourId.equals(theirRaceId))
            return true;
        if (getNeededScanPower(theirRaceId) < getScanPower(ourId))
            return true;
        return ourRace.getAgreement(theirRaceId).getType() >= DiplomaticAgreement.ALLIANCE.getType();
    }

    /**
     * Should any their_race_id's outpost/starbase (perhaps under construction) in this sector be visible to our_race ?
     * only for outposts/starbases
     * @param ourRace
     * @param theirRaceId
     * @return
     */
    public boolean shouldDrawOutpost(Major ourRace, String theirRaceId) {
        if (!getIsStationBuilding(theirRaceId) && getStation(ShipType.OUTPOST, theirRaceId) == null
                && getStation(ShipType.STARBASE, theirRaceId) == null)
            return false;
        String ourId = ourRace.getRaceId();
        if (ourId.equals(theirRaceId))
            return true;
        if (getScanPower(ourId) > 0)
            return true;
        return ourRace.getAgreement(theirRaceId).getType() >= DiplomaticAgreement.ALLIANCE.getType();
    }

    public ArrayList<Pair<String, IntPoint>> shipSymbolInSector(Major player) {
        ArrayList<Pair<String, IntPoint>> result = new ArrayList<Pair<String, IntPoint>>();
        int count = 0;
        for (Iterator<ObjectMap.Entry<String, Race>> iter = resourceManager.getRaceController().getRaces().entries()
                .iterator(); iter.hasNext();) {
            String fileName = "";
            ObjectMap.Entry<String, Race> e = iter.next();
            if (shouldDrawShip(player, e.key)) {
                if (!player.getRaceId().equals(e.key) && e.value.isAlien())
                    fileName = "Entity";
                else if (!player.getRaceId().equals(e.key) && !player.isRaceContacted(e.key))
                    fileName = "Unknown";
                else
                    fileName = e.key;
                IntPoint shipCoord = new IntPoint(coordinates.x * GamePreferences.spriteSize + 70 - count * 12,
                        coordinates.y * GamePreferences.spriteSize + 70);
                Pair<String, IntPoint> p = new Pair<String, IntPoint>(fileName, shipCoord);
                result.add(p);
                count++;
            }
            if (shouldDrawOutpost(player, e.key)) {
                if (!player.getRaceId().equals(e.key) && e.value.isAlien())
                    fileName = "Entity";
                else if (!player.getRaceId().equals(e.key) && !player.isRaceContacted(e.key))
                    fileName = "Unknown";
                else
                    fileName = e.key;
                IntPoint shipCoord = new IntPoint(coordinates.x * GamePreferences.spriteSize, coordinates.y
                        * GamePreferences.spriteSize + 70);
                Pair<String, IntPoint> p = new Pair<String, IntPoint>(fileName, shipCoord);
                result.add(p);
            }
        }
        return result;
    }

    public void addOwnerPoints(int op, String owner) {
        int old = ownerPoints.get(owner, 0);
        op += old;
        ownerPoints.put(owner, op);
    }

    public boolean isFree() {
        return ownerID.isEmpty();
    }

    /**
     * This function calculates based on the ownerpoints and other influences the owner of the current sector
     */
    protected void calculateOwner() {
        Ships station = getStation();
        if (station != null) {
            setOwner(station.getOwnerId());
            return;
        }

        //If the above didn't happened, the sector belongs to the one who has the most ownerpoints.
        //If the ownerpoints are equal, then we have a neutral terrain. At least 2 points are needed to be
        //recognized as a new owner of a sector
        int mostPoints = 1;
        String newOwner = "";
        for (Iterator<ObjectIntMap.Entry<String>> iter = ownerPoints.entries().iterator(); iter.hasNext();) {
            ObjectIntMap.Entry<String> e = iter.next();
            if (e.value > mostPoints) {
                mostPoints = e.value;
                newOwner = e.key;
            } else if (e.value == mostPoints)
                newOwner = "";
        }
        if (!newOwner.isEmpty())
            setScanned(newOwner);
        setOwner(newOwner);
    }

    @Override
    public void setResourceManager(ResourceManager res) {
        super.setResourceManager(res);
        if (anomaly != null)
            anomaly.setResourceManager(res);
    }

    public void createAnomaly() {
        anomaly = new Anomaly(resourceManager);
    }

    public String coordsName(boolean pretty) {
        return coordsName(pretty, null);
    }

    public String coordsName(boolean pretty, Race forRace) {
        String s;
        if (pretty) {
            if (sunSystem) {
                s = generateName(name, coordinates);
                return s;
            } else {
                Ships station = getStation();
                if (station != null) {
                    if (forRace != null && forRace.isRaceContacted(station.getOwnerId())) {
                        s = generateName(station.getName(), coordinates);
                        return s;
                    }
                }
            }
        }
        s = generateName("", coordinates);
        return s;
    }

    public static String generateName(String pureName, IntPoint co) {
        String s = "";
        if (pureName.isEmpty())
            s = StringDB.getString("SECTOR") + " " + co.toString();
        else
            s = pureName;
        return s;
    }

    public void addShip(Ships ship) {
        ShipMap raceShips = ships.get(ship.getOwnerId());
        if (raceShips != null)
            raceShips.add(ship);
        else {
            raceShips = new ShipMap(null, true, resourceManager);
            raceShips.add(ship);
        }
        ships.put(ship.getOwnerId(), raceShips);
    }

    public void eraseShip(Ships ship) {
        ShipMap raceShips = ships.get(ship.getOwnerId());
        if (raceShips == null)
            return;
        int index = raceShips.getShipMap().indexOfKey(ship.getMapTileKey());
        raceShips.eraseAt(index);
        if (raceShips.empty())
            ships.removeKey(ship.getOwnerId());
    }

    public void clearShips() {
        ships.clear();
    }

    public void addImage(Image img) {
        images.add(img);
    }

    public void resetImages() {
        for (int i = 0; i < images.size; i++)
            images.get(i).remove();
        images.clear();
    }

    private int getPathImageIdx(Vector2 vector) {
        return (Math.round(vector.x) + 1) * 3 + Math.round(vector.y) + 1;
    }

    public void putPathImage(Vector2 vector, Image image) {
        pathImages[getPathImageIdx(vector)] = image;
    }

    public Image getPathImage(Vector2 vector) {
        return pathImages[getPathImageIdx(vector)];
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeBoolean(sunSystem);
        kryo.writeObject(output, shipPort, new ObjectSetSerializer());
        kryo.writeObject(output, statusMap);
        kryo.writeObject(output, isStationBuild);
        kryo.writeObject(output, scanPower);
        kryo.writeObject(output, neededScanPower);
        kryo.writeObject(output, neededStationPoints);
        kryo.writeObject(output, startStationPoints);
        kryo.writeObject(output, ownerPoints);
        output.writeBoolean(anomaly != null);
        if (anomaly != null)
            kryo.writeObject(output, anomaly);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        sunSystem = input.readBoolean();
        shipPort = kryo.readObject(input, ObjectSet.class);
        statusMap = kryo.readObject(input, ObjectMap.class);
        isStationBuild = kryo.readObject(input, ObjectMap.class);
        scanPower = kryo.readObject(input, ObjectIntMap.class);
        neededScanPower = kryo.readObject(input, ObjectIntMap.class);
        neededStationPoints = kryo.readObject(input, ObjectIntMap.class);
        startStationPoints = kryo.readObject(input, ObjectIntMap.class);
        ownerPoints = kryo.readObject(input, ObjectIntMap.class);
        if (input.readBoolean())
            anomaly = kryo.readObject(input, Anomaly.class);
    }

    @Override
    public void dispose() {
        for (Iterator<ObjectMap.Entry<String, ShipMap>> iter = ships.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, ShipMap> e = iter.next();
            e.value.dispose();
        }
        super.dispose();
    }
}
