/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class ResourceRoute {
    private IntPoint coord; // coordinates of the target
    private int percent;
    private ResourceTypes resource; // which resource is in this route

    public ResourceRoute() {
        coord = new IntPoint(-1, -1);
        percent = 0;
        resource = ResourceTypes.TITAN;
    }

    public IntPoint getCoord() {
        return coord;
    }

    /**
     * @return the percentage of the resource which was used for the build order
     * that had to be removed from the starsystem
     */
    public int getPercent() {
        return percent;
    }

    public ResourceTypes getResource() {
        return resource;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public void generateResourceRoute(IntPoint dest, int res) {
        coord = dest;
        resource = ResourceTypes.fromResourceTypes(res);
        percent = 0;
    }

    /**
     * Function checks if the resourceRoute is still valid
     * @param owner
     * @param dest
     * @return true if still valid
     */
    public boolean checkResourceRoute(String owner, StarSystem dest) {
        if (!owner.equals(dest.getOwnerId()))
            return false;
        // if there are no inhabitants, there are also no resources
        float inhabitants = dest.getCurrentInhabitants();
        if (inhabitants == 0.0f)
            return false;
        return true;
    }
}
