/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.blotunga.bote.constants.Difficulties;
import com.blotunga.bote.constants.GalaxyShapes;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.races.starmap.ExpansionSpeed;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class GamePreferences implements KryoSerializable {
    final public static int spriteSize = 128;
    final public static int planetsInStarSystems = 8; // ought to be enough for everyone
    final public static float sceneWidth = Gdx.graphics.getWidth();
    final public static float sceneHeight = Gdx.graphics.getWidth() * 9 / 16;

    //stuff that can be modified only before starting the game and doesn't has to be serialized
    public transient int starDensity;		// value in percents
    public transient int minorDensity;	// value in percents
    public transient int anomalyDensity;	// value in percents
    public transient int nearbySystems;
    public transient String gameLoaded = "";
    //debug stuff
    public transient boolean seeAllofMap;
    public transient int autoTurns; // number of automated turns

    //stuff that has to be serialized
    private int saveGameVersion = 11; //should not be changed from code
    public GalaxyShapes generateFromShape; //for savegame info
    public Difficulties difficulty;
    public ExpansionSpeed expansionSpeed;
    public double researchSpeedFactor;

    public int gridSizeX;
    public int gridSizeY;
    public int alienFrequency;
    public boolean[] victoryTypes;
    public int majorJoinTimesShouldOccur;

    //random events
    public int randomGlobalProb;
    public int randomProbPerSystem;
    public int randomProbPerMinor;
    public boolean randomIsActivated;
    public boolean achievementsEnabled;

    public GamePreferences() {
        //savegame version is kept constant
        gridSizeX = 30;
        gridSizeY = 20;
        starDensity = 35; // value in percents
        minorDensity = 30;
        anomalyDensity = 9;
        generateFromShape = GalaxyShapes.IRREGULAR;
        seeAllofMap = false;
        expansionSpeed = ExpansionSpeed.CLASSIC;
        difficulty = Difficulties.EASY;
        autoTurns = 0;
        researchSpeedFactor = 1.25;
        alienFrequency = 5;
        victoryTypes = new boolean[VictoryType.values().length];
        Arrays.fill(victoryTypes, true);
        nearbySystems = 2;
        randomGlobalProb = 2;
        randomProbPerSystem = 7;
        randomProbPerMinor = 7;
        randomIsActivated = true;
        majorJoinTimesShouldOccur = 0;
        achievementsEnabled = true;
    }

    public GamePreferences(GamePreferences prefs) {
        //savegame version is kept constant
        gridSizeX = prefs.gridSizeX;
        gridSizeY = prefs.gridSizeY;
        starDensity = prefs.starDensity;
        minorDensity = prefs.minorDensity;
        anomalyDensity = prefs.anomalyDensity;
        generateFromShape = prefs.generateFromShape;
        seeAllofMap = prefs.seeAllofMap;
        expansionSpeed = prefs.expansionSpeed;
        difficulty = prefs.difficulty;
        autoTurns = prefs.autoTurns;
        researchSpeedFactor = prefs.researchSpeedFactor;
        alienFrequency = prefs.alienFrequency;
        victoryTypes = prefs.victoryTypes.clone();
        nearbySystems = prefs.nearbySystems;
        randomGlobalProb = prefs.randomGlobalProb;
        randomProbPerSystem = prefs.randomProbPerSystem;
        randomProbPerMinor = prefs.randomProbPerMinor;
        randomIsActivated = prefs.randomIsActivated;
        majorJoinTimesShouldOccur = prefs.majorJoinTimesShouldOccur;
        achievementsEnabled = prefs.achievementsEnabled;
    }

    public int saveGameVersion() {
        return saveGameVersion;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeInt(alienFrequency, false);
        kryo.writeObject(output, difficulty);
        kryo.writeObject(output, expansionSpeed);
        kryo.writeObject(output, generateFromShape);
        output.writeInt(gridSizeX, false);
        output.writeInt(gridSizeY, false);
        output.writeInt(majorJoinTimesShouldOccur, false);
        output.writeInt(randomGlobalProb, false);
        output.writeBoolean(randomIsActivated);
        output.writeInt(randomProbPerMinor, false);
        output.writeInt(randomProbPerSystem, false);
        output.writeDouble(researchSpeedFactor);
        output.writeInt(saveGameVersion, false);
        kryo.writeObject(output, victoryTypes);
        //new after save v. 11
        output.writeBoolean(achievementsEnabled);
        output.writeInt(anomalyDensity);
        output.writeInt(starDensity);
        output.writeInt(minorDensity);
        output.writeInt(nearbySystems);
        //debug settings
        output.writeBoolean(seeAllofMap);
        output.writeInt(autoTurns);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        alienFrequency = input.readInt(false);
        difficulty = kryo.readObject(input, Difficulties.class);
        expansionSpeed = kryo.readObject(input, ExpansionSpeed.class);
        generateFromShape = kryo.readObject(input, GalaxyShapes.class);
        gridSizeX = input.readInt(false);
        gridSizeY = input.readInt(false);
        majorJoinTimesShouldOccur = input.readInt(false);
        randomGlobalProb = input.readInt(false);
        randomIsActivated = input.readBoolean();
        randomProbPerMinor = input.readInt(false);
        randomProbPerSystem = input.readInt(false);
        researchSpeedFactor = input.readDouble();
        saveGameVersion = input.readInt(false);
        victoryTypes = kryo.readObject(input, boolean[].class);
        if (saveGameVersion > 10) {
            achievementsEnabled = input.readBoolean();
            anomalyDensity = input.readInt();
            starDensity = input.readInt();
            minorDensity = input.readInt();
            nearbySystems = input.readInt();
            seeAllofMap = input.readBoolean();
            autoTurns = input.readInt();
        }
    }
}
