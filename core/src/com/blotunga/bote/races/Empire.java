/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.events.EventScreen;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.starsystem.GlobalStorage;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class Empire {
    private int credits;				///< credits of the empire
    private int creditsChange;			///< income/loss compared to last round
    private int shipCosts;				///< ship maintenance costs
    private int popSupportCosts;		///< costs to support population
    private int researchPoints;			///< current research points
    private int[] resourceStorages;		///< total of all of the resources in all systems
    private String empireID;			///< RaceID of the owner of the empire
    private Array<EmpireNews> messages;			///< all messages received by the empire
    private transient Array<EventScreen> events;///< all events of the empire
    private Research research;			///< the research of the empire
    private Intelligence intelligence;	///< intelligence of the empire
    private GlobalStorage globalStorage;		///< global storage of the empire
    private transient Array<IntPoint> systemList;	///< list of the empire's systems
    private int moraleEmpireWide;

    public Empire() {
        credits = 1000;
        creditsChange = 0;
        shipCosts = 0;
        popSupportCosts = 0;
        researchPoints = 0;
        resourceStorages = new int[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(resourceStorages, 0);
        empireID = "";
        messages = new Array<EmpireNews>();
        events = new Array<EventScreen>();
        research = new Research();
        systemList = new Array<IntPoint>();
        moraleEmpireWide = 0;
        globalStorage = new GlobalStorage();
        intelligence = new Intelligence();
    }

    public int countSystems() {
        return systemList.size;
    }

    public Array<IntPoint> getSystemList() {
        return systemList;
    }

    public String getEmpireID() {
        return empireID;
    }

    public int getCredits() {
        return credits;
    }

    public int getCreditsChange() {
        return creditsChange;
    }

    public int getShipCosts() {
        return shipCosts;
    }

    public int getPopSupportCosts() {
        return popSupportCosts;
    }

    public int getResearchPoints() {
        return researchPoints;
    }

    public int getSecurityPoints() {
        return intelligence.getSecurityPoints();
    }

    public int[] getStorage() {
        return resourceStorages;
    }

    public Array<EmpireNews> getMsgs() {
        return messages;
    }

    public Array<EventScreen> getEvents() {
        return events;
    }

    public void insertEvent(EventScreen screen, int index) {
        events.insert(index, screen);
    }

    public void pushEvent(EventScreen screen) {
        events.add(screen);
    }

    public EventScreen firstEvent() {
        EventScreen screen = null;
        if (events.size != 0) {
            screen = events.removeIndex(0);
        }
        return screen;
    }

    public Research getResearch() {
        return research;
    }

    public Intelligence getIntelligence() {
        return intelligence;
    }

    public GlobalStorage getGlobalStorage() {
        return globalStorage;
    }

    public void setEmpireID(String raceID) {
        empireID = raceID;
        intelligence.setRaceID(raceID);
    }

    /**
     * Function adds parameter to current credits
     * @param add
     */
    public void setCredits(int add) {
        credits += add;
    }

    public void setCreditsChange(int change) {
        creditsChange = change;
    }

    public void setShipCosts(int costs) {
        ///SPECIAL RESEARCH///
        int bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.FINANCES, 2);
        costs -= costs * bonus / 100;
        shipCosts = costs;
    }

    public void addShipCosts(int add) {
        ///SPECIAL RESEARCH///
        int bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.FINANCES, 2);
        add -= add * bonus / 100;
        shipCosts += add;
    }

    public void setPopSupportCosts(int costs) {
        popSupportCosts = costs;
    }

    public void addPopSupportCosts(int add) {
        popSupportCosts += add;
    }

    public void addResearchPoints(int add) {
        if (researchPoints + add < 0)
            researchPoints = 0;
        else
            researchPoints += add;
    }

    public void addSecurityPoints(int add) {
        intelligence.addSecurityPoints(add);
    }

    public void setStorageAdd(int res, int add) {
        resourceStorages[res] += add;
    }

    public void addMsg(EmpireNews msg) {
        messages.add(msg);
    }

    public void generateSystemList(Array<StarSystem> systems, ResourceManager game) {
        systemList.clear();
        for (StarSystem s : systems)
            if (s.isMajorized() && s.getOwnerId().equals(empireID))
                systemList.add(s.getCoordinates());
        String playerRace = game.getRaceController().getPlayerRaceString();
        if (empireID.equals(playerRace)) {
            if (systemList.size >= 10)
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGalacticBarony.getAchievement());
            if (systemList.size >= 25)
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGalacticDuchy.getAchievement());
            if (systemList.size >= 50)
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGalacticKingdom.getAchievement());
            if (systemList.size >= 100)
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGalacticRepublic.getAchievement());
            if (systemList.size >= 150)
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGalacticEmpire.getAchievement());
        }
    }

    public void clearAllPoints() {
        Arrays.fill(resourceStorages, 0);
        researchPoints = 0;
        intelligence.clearSecurityPoints();
    }

    public void clearMessagesAndEvents() {
        messages.clear();
        events.clear();
    }

    public void setMoraleEmpireWide(int morale) {
        moraleEmpireWide = morale;
    }

    public int getMoraleEmpireWide() {
        return moraleEmpireWide;
    }
}
