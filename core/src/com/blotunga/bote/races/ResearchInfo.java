/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.IntArray;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

public class ResearchInfo {
    ResearchComplex[] researchComplex; // array to hold the special research
    ResearchComplexType currentComplex; // the selected complex
    boolean choiceTaken; // was a special tech direction chosen?
    private String[] techName; // name of the current technology
    private String[] techDescription; // description of the current technology

    public ResearchInfo() {
        currentComplex = ResearchComplexType.NONE;
        choiceTaken = false;
        researchComplex = new ResearchComplex[ResearchComplexType.COMPLEX_COUNT.getType()];
        for (int i = 0; i < researchComplex.length; i++)
            researchComplex[i] = new ResearchComplex();
        techName = new String[6];
        Arrays.fill(techName, "");
        techDescription = new String[6];
        Arrays.fill(techDescription, "");
    }

    /*
     * The following functions return the needed research points for the selected field.
     * The starting value for each field is the multiplier (ie: 125 or 150).
     * The parameter for each function is the techLevel for which we want the needed research points.
     */
    public long getBio(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 150.0 * researchSpeedFactor);
    }

    public long getEnergy(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 125.0 * researchSpeedFactor);
    }

    public long getComputer(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 150.0 * researchSpeedFactor);
    }

    public long getPropulsion(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 150.0 * researchSpeedFactor);
    }

    public long getConstruction(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 175.0 * researchSpeedFactor);
    }

    public long getWeapon(int techLevel, double researchSpeedFactor) {
        return (long) (Math.pow(2.25, techLevel) * 175.0 * researchSpeedFactor);
    }

    public boolean getChoiceTaken() {
        return choiceTaken;
    }

    public ResearchComplex getCurrentResearchComplex() {
        return researchComplex[currentComplex.getType()];
    }

    public ResearchComplexType getCurrentResearchComplexType() {
        return currentComplex;
    }

    public ResearchComplex getResearchComplex(ResearchComplexType type) {
        return researchComplex[type.getType()];
    }

    public String getTechName(int tech) {
        return techName[tech];
    }

    public String getTechDescription(int tech) {
        return techDescription[tech];
    }

    /**
     * This function selects randomly a unique theme from the special technologies which weren't researched yet.
     */
    public void chooseUniqueResearch() {
        IntArray researchableComplexes = new IntArray(false, ResearchComplexType.COMPLEX_COUNT.getType());
        for (int i = 0; i < ResearchComplexType.COMPLEX_COUNT.getType(); i++) {
            if (researchComplex[i].getComplexStatus() == ResearchStatus.NOTRESEARCHED)
                researchableComplexes.add(i);
        }
        if (researchableComplexes.size == 0)
            return;
        int chooseThis = (int) (RandUtil.random() * researchableComplexes.size);
        ResearchComplexType complex = ResearchComplexType.fromInt(researchableComplexes.get(chooseThis));
        researchComplex[complex.getType()].complexStatus = ResearchStatus.RESEARCHING;
        currentComplex = complex;
        choiceTaken = false;
        researchComplex[complex.getType()].generateComplex(complex);
    }

    /**
     * This function modifies the status of the current complex
     * @param newStatus
     */
    public void changeStatusOfComplex(ResearchStatus newStatus) {
        if (currentComplex == ResearchComplexType.NONE)
            return;

        ResearchComplex cc = researchComplex[currentComplex.getType()];
        cc.complexStatus = newStatus;
        if (newStatus == ResearchStatus.RESEARCHED) {
            //loop through all fields
            for (int i = 0; i < 3; i++) {
                if (cc.getFieldStatus(i + 1) == ResearchStatus.RESEARCHING) {
                    // set the others to not researched
                    Arrays.fill(cc.fieldStatus, ResearchStatus.NOTRESEARCHED);
                    // set the field which was completed to RESEARCHED
                    cc.fieldStatus[i] = ResearchStatus.RESEARCHED;
                    break;
                }
            }

            currentComplex = ResearchComplexType.NONE;
        }
    }

    /**
     * This function chooses one of the three possibilities for the unique research.
     * @param possibility (1, 2 or 3)
     */
    public void setUniqueResearchChoosePossibility(int possibility) {
        if (currentComplex != ResearchComplexType.NONE && !choiceTaken) {
            ResearchComplex cc = researchComplex[currentComplex.getType()];
            Arrays.fill(cc.fieldStatus, ResearchStatus.NOTRESEARCHED);
            cc.fieldStatus[possibility - 1] = ResearchStatus.RESEARCHING;
            choiceTaken = true;
        }
    }

    public void setTechInfos(int tech, int level) {
        Pair<String, String> str = getTechInfos(tech, level);
        techName[tech] = str.getFirst();
        techDescription[tech] = str.getSecond();
    }

    /**
     * @param tech
     * @param level
     * @return first string ist techName, second string is techDescription
     */
    public static Pair<String, String> getTechInfos(int tech, int level) {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/names/techs.txt");
        String input = handle.readString(GameConstants.getCharset());
        String[] inputs = input.split("\n");
        String name = "Future Tech";
        String desc = "-";
        if (level < 15) {
            int j = level * 12 + tech * 2;
            for (int i = j; i < input.length(); i++) {
                if (i == j)
                    name = inputs[i].trim();
                else if (i - 1 == j) {
                    desc = inputs[i].trim();
                    break;
                }
            }
        }
        return new Pair<String, String>(name, desc);
    }

    public int isResearchedThenGetBonus(ResearchComplexType type, int field) {
        if (getResearchComplex(type).getFieldStatus(field) == ResearchStatus.RESEARCHED)
            return getResearchComplex(type).getBonus(field);
        return 0;
    }
}
