/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.SystemOwningStatus;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class DiplomacyController {
    /**
     * Function which sends diplomatic messages
     * @param manager
     */
    public static void send(ResourceManager manager) {
        ArrayMap<String, Race> races = manager.getRaceController().getRandomRaces();
        for (int r = 0; r < races.size; r++)
            races.getValueAt(r).makeOffersAI(races);

        for (int r = 0; r < races.size; r++) {
            Race race = races.getValueAt(r);

            //delete all answers
            for (int i = 0; i < race.getIncomingDiplomacyNews().size; i++) {
                DiplomacyInfo info = race.getIncomingDiplomacyNews().get(i);
                if (info.flag == DiplomacyInfoType.DIPLOMACY_ANSWER.getType())
                    race.getIncomingDiplomacyNews().removeIndex(i--);
            }

            //iterate all outgoing news
            for (int i = 0; i < race.getOutgoingDiplomacyNews().size; i++) {
                DiplomacyInfo info = race.getOutgoingDiplomacyNews().get(i);
                if (manager.getRaceController().getRace(info.toRace) != null) {
                    Race toRace = manager.getRaceController().getRace(info.toRace);
                    if (toRace.isMajor()) {
                        sendToMajor(manager, (Major) toRace, info);
                    } else if (toRace.isMinor()) {
                        sendToMinor(manager, (Minor) toRace, info);
                    }
                }
            }
            race.getOutgoingDiplomacyNews().clear();
        }
    }

    /**
     * Function which receives diplomatic messages
     * @param manger
     */
    public static void receive(ResourceManager manager) {
        ArrayMap<String, Race> races = manager.getRaceController().getRandomRaces();
        for (int r = 0; r < races.size; r++) {
            Race race = races.getValueAt(r);

            for (int i = 0; i < race.getIncomingDiplomacyNews().size; i++) {
                DiplomacyInfo info = race.getIncomingDiplomacyNews().get(i);
                //does the sender still exist
                if (manager.getRaceController().getRace(info.toRace) != null) {
                    Race toRace = manager.getRaceController().getRace(info.toRace);
                    if (race.isMajor()) {
                        receiveToMajor(manager, (Major) toRace, info);
                    } else if (race.isMinor()) {
                        receiveToMinor(manager, (Minor) toRace, info);
                    }
                }
            }

            for (int i = 0; i < race.getIncomingDiplomacyNews().size; i++) {
                DiplomacyInfo info = race.getIncomingDiplomacyNews().get(i);
                if (info.sendRound < (manager.getCurrentRound() - 1))
                    race.getIncomingDiplomacyNews().removeIndex(i--);
            }
        }
        calcDiplomacyFallouts(manager);
    }

    /**
     * Function checks the diplomatic consistence and calculates the direct diplomatic consequences
     */
    private static void calcDiplomacyFallouts(ResourceManager manager) {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();

        for (Iterator<ObjectMap.Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            Minor minor = e.value;

            //check for diplomatic consistence
            minor.checkDiplomaticConsistency();
            minor.perhapsCancelAgreement();
            if (minor.getCoordinates().equals(new IntPoint()))
                continue;

            for (int i = 0; i < majors.size; i++) {
                String majorID = majors.getKeyAt(i);
                Major major = majors.getValueAt(i);
                StarSystem minorSystem = manager.getUniverseMap().getStarSystemAt(minor.getCoordinates());
                if (minor.getAgreement(majorID).getType() >= DiplomaticAgreement.TRADE.getType())
                    minorSystem.setScanned(majorID);
                if (minor.getAgreement(majorID).getType() >= DiplomaticAgreement.FRIENDSHIP.getType())
                    minorSystem.setKnown(majorID);
                if (minor.isMemberTo(majorID) && !minorSystem.isMajorized()) {
                    minorSystem.setFullKnown(majorID);
                    minorSystem.changeOwner(majorID, SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME);
                    minor.setOwner(majorID);
                    minorSystem.buildBuildingsForMinorRace(manager.getBuildingInfos(), manager.getStatistics()
                            .getAverageTechLevel(), minor);
                    minorSystem.setWorkersIntoBuildings();
                    for (int j = 0; j < manager.getUniverseMap().getShipMap().getSize(); j++) {
                        Ships s = manager.getUniverseMap().getShipMap().getAt(j);
                        StarSystem ss = manager.getUniverseMap().getStarSystemAt(s.getCoordinates());
                        if (s.getOwnerId().equals(minor.getRaceId())) {
                            //the Ehelen defender never goes to the major
                            if (minor.getRaceId().equals("EHLEN") && s.getShipType() == ShipType.STARBASE)
                                continue;
                            ss.eraseShip(s);
                            s.setOwner(majorID);
                            ss.addShip(s);
                            major.getShipHistory().addShip(s.shipHistoryInfo(), minorSystem.coordsName(true),
                                    manager.getCurrentRound());
                            s.addSpecialResearchBoni(major);
                        }
                    }
                }
            }
        }

        //if a membership was cancelled, or bribed or for some reason the major lost the system
        for (Iterator<ObjectMap.Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            Minor minor = e.value;
            if (minor.getCoordinates().equals(new IntPoint()))
                continue;

            StarSystem minorSystem = manager.getUniverseMap().getStarSystemAt(minor.getCoordinates());
            String owner = minorSystem.getOwnerId();
            if (!owner.isEmpty()) {
                if (minor.getAgreement(owner) != DiplomaticAgreement.MEMBERSHIP && minorSystem.getMinorRace() != null
                        && !minorSystem.isTaken()) {
                    minorSystem.changeOwner(minor.getRaceId(), SystemOwningStatus.OWNING_STATUS_INDEPENDENT_MINOR);
                    minor.setOwner("");
                    Major major = Major.toMajor(manager.getRaceController().getRace(owner));
                    if (major != null) {
                        major.getEmpire().getSystemList().removeValue(minorSystem.getCoordinates(), false);
                    }
                }
            }
        }
    }

    /**
     * Sends a diplomatic message to a major
     * @param manager
     * @param toMajor
     * @param info
     */
    private static void sendToMajor(ResourceManager manager, Major toMajor, DiplomacyInfo info) {
        if (info.flag != DiplomacyInfoType.DIPLOMACY_OFFER.getType())
            return;

        Race fromRace = manager.getRaceController().getRace(info.fromRace);
        if (fromRace == null)
            return;

        Major warPactEnemy = null;
        if (info.type == DiplomaticAgreement.WARPACT) {
            warPactEnemy = Major.toMajor(manager.getRaceController().getRace(info.warpactEnemy));
            if (warPactEnemy == null)
                return;
        }

        String empireAssignedArticleName = toMajor.getEmpireNameWithAssignedArticle();
        String empireArticleName = "";
        if (fromRace.isMajor()) {
            empireArticleName = Major.toMajor(fromRace).getEmpireNameWithArticle();
            empireArticleName = Character.toString(empireArticleName.charAt(0)).toUpperCase()
                    + empireArticleName.substring(1);
        }

        String agreement = (info.type == DiplomaticAgreement.WARPACT) ? StringDB.getString(info.type.getdbName()
                + "_WITH_ARTICLE", false, warPactEnemy.getName()) : StringDB.getString(info.type.getdbName()
                + "_WITH_ARTICLE");

        EmpireNews message;
        //if we waited for a round, then generate message
        if (info.sendRound == manager.getCurrentRound() - 1) {
            if (info.type == DiplomaticAgreement.PRESENT) {
                if (fromRace.isMajor()) {
                    String s = StringDB.getString("WE_GIVE_PRESENT", false, empireAssignedArticleName);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                    ((Major) fromRace).getEmpire().addMsg(message);

                    message = new EmpireNews();
                    s = StringDB.getString("WE_GET_PRESENT", false, empireArticleName);
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    toMajor.getEmpire().addMsg(message);
                    //apply the credits
                    toMajor.getEmpire().setCredits(info.credits);
                    for (int k = ResourceTypes.TITAN.getType(); k <= ResourceTypes.IRIDIUM.getType(); k++)
                        if (info.resources[k] > 0)
                            toMajor.getEmpire().getGlobalStorage().addResource(info.resources[k], k, new IntPoint());
                    //deritium goes to the home system directly
                    if (info.resources[ResourceTypes.DERITIUM.getType()] > 0) {
                        IntPoint p = toMajor.getCoordinates();

                        if (!p.equals(new IntPoint())
                                && manager.getUniverseMap().getStarSystemAt(p).getOwnerId().equals(toMajor.getRaceId()))
                            manager.getUniverseMap().getStarSystemAt(p)
                                    .setDeritiumStore(info.resources[ResourceTypes.DERITIUM.getType()]);
                    }

                    toMajor.getIncomingDiplomacyNews().add(info);
                }
            } else if (info.type == DiplomaticAgreement.REQUEST) {
                if (fromRace.isMajor()) {
                    String s = StringDB.getString("WE_HAVE_REQUEST", false, empireAssignedArticleName, agreement);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                    ((Major) fromRace).getEmpire().addMsg(message);

                    s = StringDB.getString("WE_GET_REQUEST", false, empireArticleName, agreement);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    toMajor.getEmpire().addMsg(message);

                    toMajor.getIncomingDiplomacyNews().add(info);
                }
            } else if (info.type == DiplomaticAgreement.WAR) {
                declareWar(manager, fromRace, toMajor, info, true);

                //because of diplomatic relations, there could be more ware declarations
                Array<String> enemies = new Array<String>();
                if (fromRace.isMajor())
                    enemies = getEnemiesFromContract(manager, (Major) fromRace, toMajor);

                for (int i = 0; i < enemies.size; i++) {
                    Race enemy = manager.getRaceController().getRace(enemies.get(i));
                    if (enemy != null) {
                        DiplomacyInfo war = new DiplomacyInfo(info);
                        war.toRace = enemy.getRaceId();
                        war.warPartner = toMajor.getRaceId();
                        declareWar(manager, fromRace, enemy, war, false);
                    }
                }
            } else { //other types of agreements
                if (fromRace.isMajor()) {
                    String s = StringDB.getString("WE_MAKE_MAJ_OFFER", false, empireAssignedArticleName, agreement);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                    ((Major) fromRace).getEmpire().addMsg(message);

                    s = StringDB.getString("WE_GET_MAJ_OFFER", false, empireArticleName, agreement);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    toMajor.getEmpire().addMsg(message);
                } else if (fromRace.isMinor()) {
                    String s = "";
                    switch (info.type) {
                        case TRADE:
                            s = StringDB.getString("MIN_OFFER_TRADE", false, fromRace.getName());
                            break;
                        case FRIENDSHIP:
                            s = StringDB.getString("MIN_OFFER_FRIEND", false, fromRace.getName());
                            break;
                        case COOPERATION:
                            s = StringDB.getString("MIN_OFFER_COOP", false, fromRace.getName());
                            break;
                        case ALLIANCE:
                            s = StringDB.getString("MIN_OFFER_AFFI", false, fromRace.getName());
                            break;
                        case MEMBERSHIP:
                            s = StringDB.getString("MIN_OFFER_MEMBER", false, fromRace.getName());
                            break;
                        default:
                            break;
                    }
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    toMajor.getEmpire().addMsg(message);
                }
                toMajor.getIncomingDiplomacyNews().add(info);
            }
        }
    }

    /**
     * Function which resolves a diplomatic message to a major
     * @param manager
     * @param toMajor
     * @param info
     */
    private static void receiveToMajor(ResourceManager manager, Major toMajor, DiplomacyInfo info) {
        if (info.flag != DiplomacyInfoType.DIPLOMACY_OFFER.getType())
            return;

        Race fromRace = manager.getRaceController().getRace(info.fromRace);
        if (fromRace == null)
            return;

        Major warPactEnemy = null;
        if (info.type == DiplomaticAgreement.WARPACT) {
            warPactEnemy = Major.toMajor(manager.getRaceController().getRace(info.warpactEnemy));
            if (warPactEnemy == null)
                return;
        }
        String agreement = (info.type == DiplomaticAgreement.WARPACT) ? StringDB.getString(info.type.getdbName()
                + "_WITH_ARTICLE", false, warPactEnemy.getName()) : StringDB.getString(info.type.getdbName()
                + "_WITH_ARTICLE");

        if (info.sendRound == manager.getCurrentRound() - 2) {
            if (!toMajor.aHumanPlays())
                toMajor.reactOnOfferAI(info);

            //create the answer
            if (info.type != DiplomaticAgreement.PRESENT && info.type != DiplomaticAgreement.WAR) {
                DiplomacyInfo answer = new DiplomacyInfo(info);
                answer.sendRound = manager.getCurrentRound();
                GenDiploMessage.generateMajorAnswer(answer);

                if (fromRace.isMajor()) {
                    Major fromMajor = Major.toMajor(fromRace);
                    fromRace.getIncomingDiplomacyNews().add(answer);

                    if (answer.type != DiplomaticAgreement.REQUEST && answer.type != DiplomaticAgreement.WARPACT) {
                        String agreement2 = StringDB.getString(answer.type.getdbName());
                        if (answer.answerStatus == AnswerStatus.ACCEPTED) {
                            String s = StringDB.getString("WE_ACCEPT_MAJ_OFFER", false, agreement,
                                    fromMajor.getEmpireNameWithAssignedArticle());
                            String s2 = StringDB.getString("MAJ_ACCEPT_OFFER", true,
                                    toMajor.getEmpireNameWithArticle(), agreement2);

                            fromRace.setAgreement(toMajor.getRaceId(), answer.type);
                            toMajor.setAgreement(fromRace.getRaceId(), answer.type);
                            if (answer.type != DiplomaticAgreement.DEFENCEPACT) {
                                fromMajor.setAgreementDuration(toMajor.getRaceId(), answer.duration);
                                toMajor.setAgreementDuration(fromRace.getRaceId(), answer.duration);
                            } else {
                                fromMajor.setDefencePactDuration(toMajor.getRaceId(), answer.duration);
                                toMajor.setDefencePactDuration(fromRace.getRaceId(), answer.duration);
                            }
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                            toMajor.getEmpire().addMsg(message);
                            message = new EmpireNews();
                            message.CreateNews(s2, EmpireNewsType.DIPLOMACY, 2);
                            fromMajor.getEmpire().addMsg(message);

                            //extra event messages because of the morale effects
                            String eventText = "";
                            String eventText2 = "";

                            switch (answer.type) {
                            // Sign Trade Treaty #34
                                case TRADE:
                                    eventText = toMajor.getMoraleObserver().addEvent(34, toMajor.getRaceMoralNumber(),
                                            fromMajor.getEmpireNameWithAssignedArticle());
                                    eventText2 = fromMajor.getMoraleObserver().addEvent(34,
                                            fromMajor.getRaceMoralNumber(), toMajor.getEmpireNameWithAssignedArticle());
                                    break;
                                // Sign Friendship/Cooperation Treaty #35
                                case FRIENDSHIP:
                                    eventText = toMajor.getMoraleObserver().addEvent(35, toMajor.getRaceMoralNumber(),
                                            fromMajor.getEmpireNameWithAssignedArticle());
                                    eventText2 = fromMajor.getMoraleObserver().addEvent(35,
                                            fromMajor.getRaceMoralNumber(), toMajor.getEmpireNameWithAssignedArticle());
                                    break;
                                // Sign Friendship/Cooperation Treaty #35
                                case COOPERATION:
                                    eventText = toMajor.getMoraleObserver().addEvent(35, toMajor.getRaceMoralNumber(),
                                            fromMajor.getEmpireNameWithAssignedArticle());
                                    eventText2 = fromMajor.getMoraleObserver().addEvent(35,
                                            fromMajor.getRaceMoralNumber(), toMajor.getEmpireNameWithAssignedArticle());
                                    break;
                                // Sign an Affiliation Treaty #36
                                case ALLIANCE:
                                    eventText = toMajor.getMoraleObserver().addEvent(36, toMajor.getRaceMoralNumber(),
                                            fromMajor.getEmpireNameWithAssignedArticle());
                                    eventText2 = fromMajor.getMoraleObserver().addEvent(36,
                                            fromMajor.getRaceMoralNumber(), toMajor.getEmpireNameWithAssignedArticle());
                                    break;
                                default:
                                    break;
                            }
                            if (!eventText.isEmpty()) {
                                message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.SOMETHING);
                                toMajor.getEmpire().addMsg(message);
                            }
                            if (!eventText.isEmpty()) {
                                message = new EmpireNews();
                                message.CreateNews(eventText2, EmpireNewsType.SOMETHING);
                                fromMajor.getEmpire().addMsg(message);
                            }

                            //give the credits and resources
                            toMajor.getEmpire().setCredits(answer.credits);
                            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.IRIDIUM.getType(); res++)
                                toMajor.getEmpire().getGlobalStorage()
                                        .addResource(answer.resources[res], res, new IntPoint());
                            //Deritium is transferred to the home system directly
                            if (info.resources[ResourceTypes.DERITIUM.getType()] > 0) {
                                IntPoint p = toMajor.getCoordinates();
                                if (!p.equals(new IntPoint())
                                        && manager.getUniverseMap().getStarSystemAt(p).getOwnerId()
                                                .equals(toMajor.getRaceId()))
                                    manager.getUniverseMap().getStarSystemAt(p)
                                            .setDeritiumStore(answer.resources[ResourceTypes.DERITIUM.getType()]);
                            }

                            //improve relations
                            toMajor.setRelation(fromRace.getRaceId(), Math.abs(answer.type.getType() * 2));
                            fromMajor.setRelation(toMajor.getRaceId(), Math.abs(answer.type.getType() * 2));

                            //if the relationship is too bad (ie: after a war a NAP was accepted, set the relationship to a minimum
                            if (toMajor.getRelation(fromRace.getRaceId()) < (answer.type.getType() + 3) * 10) {
                                int add = (answer.type.getType() + 3) * 10 - toMajor.getRelation(fromRace.getRaceId());
                                toMajor.setRelation(fromRace.getRaceId(), add);
                            }
                            if (fromRace.getRelation(toMajor.getRaceId()) < (answer.type.getType() + 3) * 10) {
                                int add = (answer.type.getType() + 3) * 10 - fromRace.getRelation(toMajor.getRaceId());
                                fromRace.setRelation(toMajor.getRaceId(), add);
                            }
                        } else { //we have declined the offer or we didn't react
                            if (answer.answerStatus == AnswerStatus.DECLINED) {
                                String s = StringDB.getString("WE_DECLINE_MAJ_OFFER", false, agreement,
                                        fromRace.getName());
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                                toMajor.getEmpire().addMsg(message);

                                String s2 = StringDB.getString("MAJ_DECLINE_OFFER", true,
                                        toMajor.getEmpireNameWithArticle(), agreement2);
                                message = new EmpireNews();
                                message.CreateNews(s2, EmpireNewsType.DIPLOMACY, 2);
                                fromMajor.getEmpire().addMsg(message);

                                //relation hit if the offer was declined
                                toMajor.setRelation(fromRace.getRaceId(),
                                        -(int) (RandUtil.random() * Math.abs(answer.type.getType()) / 2));
                                fromRace.setRelation(toMajor.getRaceId(),
                                        -(int) (RandUtil.random() * Math.abs(answer.type.getType())));
                            } else { //not reacted
                                String s = StringDB.getString("NOT_REACTED", true, toMajor.getEmpireNameWithArticle());
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                                fromMajor.getEmpire().addMsg(message);
                            }

                            //if there were credits or resources coupled with the offer, these have to be transferred back
                            fromMajor.getEmpire().setCredits(answer.credits);
                            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                                IntPoint pt = answer.coord;
                                if (!pt.equals(new IntPoint()))
                                    if (manager.getUniverseMap().getStarSystemAt(pt).getOwnerId()
                                            .equals(fromRace.getRaceId()))
                                        manager.getUniverseMap().getStarSystemAt(pt)
                                                .addResourceStore(res, answer.resources[res]);
                            }
                        }
                    } else if (answer.type == DiplomaticAgreement.REQUEST) {
                        String s = "";
                        //if it was accepted
                        if (answer.answerStatus == AnswerStatus.ACCEPTED) {
                            s = StringDB.getString("WE_ACCEPT_REQUEST", true,
                                    fromMajor.getEmpireNameWithAssignedArticle());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                            toMajor.getEmpire().addMsg(message);

                            s = StringDB.getString("OUR_REQUEST_ACCEPT", true, toMajor.getEmpireNameWithArticle());
                            message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                            fromMajor.getEmpire().addMsg(message);

                            //add the requested credits/resources
                            fromMajor.getEmpire().setCredits(answer.credits);
                            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.IRIDIUM.getType(); res++)
                                fromMajor.getEmpire().getGlobalStorage()
                                        .addResource(answer.resources[res], res, new IntPoint());
                            if (answer.resources[ResourceTypes.DERITIUM.getType()] > 0) {
                                IntPoint p = fromRace.getCoordinates();
                                if (!p.equals(new IntPoint())
                                        && manager.getUniverseMap().getStarSystemAt(p).getOwnerId()
                                                .equals(fromRace.getRaceId()))
                                    manager.getUniverseMap().getStarSystemAt(p)
                                            .setDeritiumStore(answer.resources[ResourceTypes.DERITIUM.getType()]);
                            }
                            //give a bonus to relationship
                            fromRace.setRelation(toMajor.getRaceId(),
                                    (int) (RandUtil.random() * DiplomaticAgreement.REQUEST.getType()));
                        } else if (answer.answerStatus == AnswerStatus.DECLINED) {
                            s = StringDB.getString("WE_DECLINE_REQUEST", true,
                                    fromMajor.getEmpireNameWithAssignedArticle());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                            toMajor.getEmpire().addMsg(message);

                            s = StringDB.getString("OUR_REQUEST_DECLINE", true, toMajor.getEmpireNameWithArticle());
                            message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                            fromMajor.getEmpire().addMsg(message);
                            //relationship will suffer on decline, but the human suffers more than the computer
                            if (!fromMajor.aHumanPlays())
                                fromMajor.setRelation(toMajor.getRaceId(), -(int) (RandUtil.random()
                                        * DiplomaticAgreement.REQUEST.getType() / 5));
                            else
                                fromMajor.setRelation(toMajor.getRaceId(),
                                        -(int) (RandUtil.random() * DiplomaticAgreement.REQUEST.getType()));
                        } else { //not reacted
                            s = StringDB.getString("NOT_REACTED_REQUEST", false, toMajor.getEmpireNameWithArticle());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                            fromMajor.getEmpire().addMsg(message);
                            //relationship will suffer on decline, but the human suffers more than the computer
                            if (!fromMajor.aHumanPlays())
                                fromMajor.setRelation(toMajor.getRaceId(), -(int) (RandUtil.random()
                                        * DiplomaticAgreement.REQUEST.getType() / 10));
                            else
                                fromMajor.setRelation(toMajor.getRaceId(), -(int) (RandUtil.random()
                                        * DiplomaticAgreement.REQUEST.getType() / 2));
                        }
                    } else if (answer.type == DiplomaticAgreement.WARPACT) {
                        String s = "";
                        if (answer.answerStatus == AnswerStatus.ACCEPTED) {
                            Gdx.app.log("DiplomacyController", String.format("Race: %s accepted WARPACT from %s versus %s", toMajor.getRaceId(), fromRace.getRaceId(), warPactEnemy.getRaceId()));
                            s = StringDB.getString("WE_ACCEPT_WARPACT", false, warPactEnemy.getName(),
                                    fromMajor.getEmpireNameWithAssignedArticle());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                            toMajor.getEmpire().addMsg(message);
                            s = StringDB.getString("OUR_WARPACT_ACCEPT", true, toMajor.getEmpireNameWithArticle(),
                                    warPactEnemy.getName());
                            message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                            fromMajor.getEmpire().addMsg(message);

                            //get a relationship boost
                            toMajor.setRelation(fromRace.getRaceId(), Math.abs(answer.type.getType()));
                            fromRace.setRelation(toMajor.getRaceId(), Math.abs(answer.type.getType()));

                            //add the credits/resources if needed
                            toMajor.getEmpire().setCredits(answer.credits);
                            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.IRIDIUM.getType(); res++)
                                toMajor.getEmpire().getGlobalStorage()
                                        .addResource(answer.resources[res], res, new IntPoint());
                            //Deritium is transferred to the home system directly
                            if (info.resources[ResourceTypes.DERITIUM.getType()] > 0) {
                                IntPoint p = toMajor.getCoordinates();
                                if (!p.equals(new IntPoint())
                                        && manager.getUniverseMap().getStarSystemAt(p).getOwnerId()
                                                .equals(toMajor.getRaceId()))
                                    manager.getUniverseMap().getStarSystemAt(p)
                                            .setDeritiumStore(answer.resources[ResourceTypes.DERITIUM.getType()]);
                            }

                            //create war offers (for the accepting party)
                            DiplomacyInfo warOffer = new DiplomacyInfo();
                            warOffer.toRace = warPactEnemy.getRaceId();
                            warOffer.fromRace = answer.fromRace;
                            warOffer.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
                            warOffer.sendRound = answer.sendRound;
                            warOffer.type = DiplomaticAgreement.WAR;
                            GenDiploMessage.generateMajorOffer(manager, warOffer);

                            declareWar(manager, toMajor, warPactEnemy, warOffer, true);
                            //declare war to allies of enemy
                            Array<String> enemies = getEnemiesFromContract(manager, toMajor, warPactEnemy);
                            for (int i = 0; i < enemies.size; i++) {
                                Race enemy = manager.getRaceController().getRace(enemies.get(i));
                                if (enemy != null && !enemy.getRaceId().equals(fromRace.getRaceId())) {
                                    DiplomacyInfo war = new DiplomacyInfo(warOffer);
                                    //update race and reason
                                    war.toRace = enemy.getRaceId();
                                    war.warPartner = warPactEnemy.getRaceId();
                                    declareWar(manager, toMajor, enemy, war, false);
                                }
                            }
                            //create war offer for the second party (the one who initiated the pact)
                            warOffer = new DiplomacyInfo(warOffer);
                            GenDiploMessage.generateMajorOffer(manager, warOffer);

                            declareWar(manager, fromRace, warPactEnemy, warOffer, true);
                            //declare war to allies of enemy
                            enemies.clear();
                            enemies = getEnemiesFromContract(manager, fromMajor, warPactEnemy);
                            for (int i = 0; i < enemies.size; i++) {
                                Race enemy = manager.getRaceController().getRace(enemies.get(i));
                                if (enemy != null && !enemy.getRaceId().equals(toMajor.getRaceId())) {
                                    DiplomacyInfo war = new DiplomacyInfo(warOffer);
                                    //update race and reason
                                    war.toRace = enemy.getRaceId();
                                    war.warPartner = warPactEnemy.getRaceId();
                                    declareWar(manager, fromRace, enemy, war, false);
                                }
                            }
                        } else {
                            if (answer.answerStatus == AnswerStatus.DECLINED) {
                                s = StringDB.getString("WE_DECLINE_WARPACT", false,
                                        fromMajor.getEmpireNameWithAssignedArticle(), warPactEnemy.getName());
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                                toMajor.getEmpire().addMsg(message);
                                s = StringDB.getString("OUR_WARPACT_DECLINE", true, toMajor.getEmpireNameWithArticle(),
                                        warPactEnemy.getName());
                                message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                                fromMajor.getEmpire().addMsg(message);

                                //relationship worsens
                                toMajor.setRelation(fromRace.getRaceId(),
                                        (-1) * (int) (RandUtil.random() * Math.abs(answer.type.getType() / 2)));
                                fromRace.setRelation(toMajor.getRaceId(),
                                        (-1) * (int) (RandUtil.random() * Math.abs(answer.type.getType() / 2)));
                            } else {
                                s = StringDB.getString("NOT_REACTED_WARPACT", true, toMajor.getEmpireNameWithArticle(),
                                        warPactEnemy.getName());
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                                fromMajor.getEmpire().addMsg(message);
                            }

                            //if there were credits or resources coupled with the offer, these have to be transferred back
                            fromMajor.getEmpire().setCredits(answer.credits);
                            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                                IntPoint pt = answer.coord;
                                if (!pt.equals(new IntPoint()))
                                    if (manager.getUniverseMap().getStarSystemAt(pt).getOwnerId()
                                            .equals(fromMajor.getRaceId()))
                                        manager.getUniverseMap().getStarSystemAt(pt)
                                                .addResourceStore(res, answer.resources[res]);
                            }
                        }
                    }
                } else if (fromRace.isMinor()) {
                    if (info.answerStatus == AnswerStatus.ACCEPTED) {
                        String eventText = "";
                        if (info.type == DiplomaticAgreement.MEMBERSHIP)
                            eventText = toMajor.getMoraleObserver().addEvent(10, toMajor.getRaceMoralNumber(),
                                    fromRace.getName());

                        //if not corruption
                        if (info.type != DiplomaticAgreement.CORRUPTION) {
                            //check if the treaty can be accepted based on current treaty
                            //for example if they've accepted in the same round our offer, then we can't apply it
                            Minor minor = Minor.toMinor(fromRace);
                            if (!minor.canAcceptOffer(toMajor.getRaceId(), info.type))
                                return;

                            if (!agreement.isEmpty()) {
                                String s = StringDB.getString("FEMALE_ARTICLE", true)
                                        + " "
                                        + fromRace.getName()
                                        + " "
                                        + StringDB.getString("MIN_ACCEPT_OFFER", false, agreement,
                                                toMajor.getEmpireNameWithAssignedArticle());
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                                toMajor.getEmpire().addMsg(message);
                            }

                            //plan morale event
                            switch (answer.type) {
                            // Sign Trade Treaty #34
                                case TRADE:
                                    eventText = toMajor.getMoraleObserver().addEvent(34, toMajor.getRaceMoralNumber(),
                                            fromRace.getName());
                                    break;
                                // Sign Friendship/Cooperation Treaty #35
                                case FRIENDSHIP:
                                    eventText = toMajor.getMoraleObserver().addEvent(35, toMajor.getRaceMoralNumber(),
                                            fromRace.getName());
                                    break;
                                // Sign Friendship/Cooperation Treaty #35
                                case COOPERATION:
                                    eventText = toMajor.getMoraleObserver().addEvent(35, toMajor.getRaceMoralNumber(),
                                            fromRace.getName());
                                    break;
                                // Sign an Affiliation Treaty #36
                                case ALLIANCE:
                                    eventText = toMajor.getMoraleObserver().addEvent(36, toMajor.getRaceMoralNumber(),
                                            fromRace.getName());
                                    break;
                                default:
                                    break;
                            }

                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.SOMETHING);
                                toMajor.getEmpire().addMsg(message);
                            }

                            fromRace.setAgreement(toMajor.getRaceId(), info.type);
                            toMajor.setAgreement(fromRace.getRaceId(), info.type);
                        }
                    } else if (info.answerStatus == AnswerStatus.DECLINED) {
                        agreement = StringDB.getString(info.type.getdbName());
                        String s = StringDB.getString("WE_DECLINE_MIN_OFFER", false, agreement, fromRace.getName());
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                        toMajor.getEmpire().addMsg(message);
                    }
                }
            }
        }
    }

    /**
     * Sends a diplomatic message to a minor
     * @param manager
     * @param toMinor
     * @param info
     */
    private static void sendToMinor(ResourceManager manager, Minor toMinor, DiplomacyInfo info) {
        if (info.flag != DiplomacyInfoType.DIPLOMACY_OFFER.getType())
            return;

        //Only if a major sent the message has it a point to continue
        Major fromMajor = Major.toMajor(manager.getRaceController().getRace(info.fromRace));
        if (fromMajor == null)
            return;

        //if we have "waited" a turn then create a reply
        if (info.sendRound == (manager.getCurrentRound() - 1)) {
            if (info.type != DiplomaticAgreement.WAR && info.type != DiplomaticAgreement.NONE)
                toMinor.getIncomingDiplomacyNews().add(info);

            String empireName = fromMajor.getEmpireNameWithArticle();
            empireName = Character.toString(empireName.charAt(0)).toUpperCase() + empireName.substring(1);

            String s = "";
            EmpireNews message;
            if (info.type == DiplomaticAgreement.PRESENT) {
                if (info.credits > 0) {
                    s = empireName + " "
                            + StringDB.getString("CREDITS_PRESENT", false, "" + info.credits, toMinor.getName());
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                    fromMajor.getEmpire().addMsg(message);
                    s = "";
                }
                if (info.resources[ResourceTypes.TITAN.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.TITAN.getType()];
                    s = empireName + " " + StringDB.getString("TITAN_PRESENT", false, num, toMinor.getName());
                } else if (info.resources[ResourceTypes.DEUTERIUM.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.DEUTERIUM.getType()];
                    s = empireName + " " + StringDB.getString("DEUTERIUM_PRESENT", false, num, toMinor.getName());
                } else if (info.resources[ResourceTypes.DURANIUM.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.DURANIUM.getType()];
                    s = empireName + " " + StringDB.getString("DURANIUM_PRESENT", false, num, toMinor.getName());
                } else if (info.resources[ResourceTypes.CRYSTAL.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.CRYSTAL.getType()];
                    s = empireName + " " + StringDB.getString("CRYSTAL_PRESENT", false, num, toMinor.getName());
                } else if (info.resources[ResourceTypes.IRIDIUM.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.IRIDIUM.getType()];
                    s = empireName + " " + StringDB.getString("IRIDIUM_PRESENT", false, num, toMinor.getName());
                } else if (info.resources[ResourceTypes.DERITIUM.getType()] > 0) {
                    String num = "" + info.resources[ResourceTypes.DERITIUM.getType()];
                    s = empireName + " " + StringDB.getString("DERITIUM_PRESENT", false, num, toMinor.getName());
                }
            } else if (info.type == DiplomaticAgreement.CORRUPTION) {
                s = empireName + " " + StringDB.getString("TRY_CORRUPTION", false, toMinor.getName());
            } else if (info.type == DiplomaticAgreement.NONE) { //the contract was cancelled
                s = empireName + " " + StringDB.getString("CANCEL_CONTRACT", false, toMinor.getName());
                fromMajor.setAgreement(toMinor.getRaceId(), DiplomaticAgreement.NONE);
                toMinor.setAgreement(fromMajor.getRaceId(), DiplomaticAgreement.NONE);
            } else if (info.type == DiplomaticAgreement.WAR) {
                declareWar(manager, fromMajor, toMinor, info, true);
                //declare war to other enemies too
                Array<String> enemies = getEnemiesFromContract(manager, fromMajor, toMinor);
                for (int i = 0; i < enemies.size; i++) {
                    Race enemy = manager.getRaceController().getRace(enemies.get(i));
                    if (enemy != null) {
                        DiplomacyInfo war = new DiplomacyInfo(info);
                        //update race and reason
                        war.toRace = enemy.getRaceId();
                        war.warPartner = toMinor.getRaceId();
                        declareWar(manager, fromMajor, enemy, war, false);
                    }
                }
            } else { //create message that we've offered a contract
                String agreement = StringDB.getString(info.type.getdbName() + "_WITH_ARTICLE");
                s = empireName + " " + StringDB.getString("OUR_MIN_OFFER", false, toMinor.getName(), agreement);
            }

            if (!s.isEmpty()) {
                message = new EmpireNews();
                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                fromMajor.getEmpire().addMsg(message);
            }
        }
    }

    /**
     * Function used to receive and treat diplomatic messages to a minor
     * @param manager
     * @param toMinor
     * @param info
     * @return
     */
    private static void receiveToMinor(ResourceManager manager, Minor toMinor, DiplomacyInfo info) {
        if (info.flag != DiplomacyInfoType.DIPLOMACY_OFFER.getType())
            return;

        Major fromMajor = Major.toMajor(manager.getRaceController().getRace(info.fromRace));
        if (fromMajor == null)
            return;

        if (info.sendRound == manager.getCurrentRound() - 2) {
            info = toMinor.reactOnOfferAI(info);
            if (info.type != DiplomaticAgreement.PRESENT && info.type != DiplomaticAgreement.WAR) {
                if (info.type != DiplomaticAgreement.NONE) {
                    DiplomacyInfo answer = new DiplomacyInfo(info);
                    answer.sendRound = manager.getCurrentRound();
                    GenDiploMessage.generateMinorAnswer(manager, answer);
                    fromMajor.getIncomingDiplomacyNews().add(answer);
                }

                //if it was accepted
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    String eventText = "";

                    if (info.type != DiplomaticAgreement.CORRUPTION) {
                        String agreement = StringDB.getString(info.type.getdbName() + "_WITH_ARTICLE");
                        if (!agreement.isEmpty()) {
                            String s = StringDB.getString("FEMALE_ARTICLE", true)
                                    + " "
                                    + toMinor.getName()
                                    + " "
                                    + StringDB.getString("MIN_ACCEPT_OFFER", false, agreement,
                                            fromMajor.getEmpireNameWithAssignedArticle());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                            fromMajor.getEmpire().addMsg(message);
                        }
                        toMinor.setAgreement(fromMajor.getRaceId(), info.type);
                        fromMajor.setAgreement(toMinor.getRaceId(), info.type);

                        //generate morale event
                        switch (info.type) {
                            case TRADE:
                                eventText = fromMajor.getMoraleObserver().addEvent(34, fromMajor.getRaceMoralNumber(), toMinor.getName());
                                break;
                            // Sign Friendship/Cooperation Treaty #35
                            case FRIENDSHIP:
                                eventText = fromMajor.getMoraleObserver().addEvent(35, fromMajor.getRaceMoralNumber(), toMinor.getName());
                                break;
                            // Sign Friendship/Cooperation Treaty #35
                            case COOPERATION:
                                eventText = fromMajor.getMoraleObserver().addEvent(35, fromMajor.getRaceMoralNumber(), toMinor.getName());
                                break;
                            // Sign an Affiliation Treaty #36
                            case ALLIANCE:
                                eventText = fromMajor.getMoraleObserver().addEvent(36, fromMajor.getRaceMoralNumber(), toMinor.getName());
                                break;
                            // Sign a Membership #10
                            case MEMBERSHIP:
                                eventText = fromMajor.getMoraleObserver().addEvent(10, fromMajor.getRaceMoralNumber(), toMinor.getName());
                                break;
                            default:
                                break;
                        }
                        if (!eventText.isEmpty()) {
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(eventText, EmpireNewsType.SOMETHING);
                            fromMajor.getEmpire().addMsg(message);
                        }
                    }

                    //put resources into the system
                    IntPoint pt = toMinor.getCoordinates();
                    if (!pt.equals(new IntPoint())) {
                        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                            manager.getUniverseMap().getStarSystemAt(pt).addResourceStore(res, info.resources[res]);
                    }
                } else if (info.answerStatus == AnswerStatus.DECLINED) {
                    if (info.type != DiplomaticAgreement.CORRUPTION) {
                        String s = "";
                        String agreement = info.type.getdbName();
                        if (!agreement.isEmpty())
                            agreement = StringDB.getString(agreement);

                        s = StringDB.getString("MIN_DECLINE_OFFER", false, toMinor.getName(), agreement);
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                        fromMajor.getEmpire().addMsg(message);

                        //if the offer was rejected the major race gets the credits and resources back
                        fromMajor.getEmpire().setCredits(info.credits);
                        IntPoint pt = info.coord;
                        if (!pt.equals(new IntPoint()))
                            if (manager.getUniverseMap().getStarSystemAt(pt).getOwnerId().equals(fromMajor.getRaceId()))
                                for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                                    manager.getUniverseMap().getStarSystemAt(pt).addResourceStore(res, info.resources[res]);
                    }
                }
            }
        }
    }

    /**
     * The function calculates all involved races which were declared war to
     * This can happen because of defence pacts
     * @param manager
     * @param fromMajor
     * @param toRace
     * @return
     */
    private static Array<String> getEnemiesFromContract(ResourceManager manager, Major fromMajor, Race toRace) {
        Array<String> enemies = new Array<String>();
        ArrayMap<String, Race> races = manager.getRaceController().getRaces();
        for (int i = 0; i < races.size; i++) {
            String otherID = races.getKeyAt(i);
            if (!otherID.equals(toRace.getRaceId())) {
                //has the race an alliance or a defense pact
                if (toRace.getAgreement(otherID).getType() >= DiplomaticAgreement.ALLIANCE.getType()
                        || (toRace.isMajor() && ((Major) toRace).getDefencePact(otherID))) {
                    //we don't have war with the other race
                    if (fromMajor.getAgreement(otherID) != DiplomaticAgreement.WAR) {
                        enemies.add(otherID);
                        Gdx.app.log("DiplomacyController", String.format("Race: %s declares Race: %s WAR because affiliation contract with Race %s", fromMajor.getRaceId(), otherID, toRace.getRaceId()));
                    }
                }
            }
        }

        return enemies;
    }

    /**
     * The function declares war between two races and does all the work
     * @param fromRace
     * @param enemy
     * @param info
     * @param withMoraleEvent
     */
    private static void declareWar(ResourceManager manager, Race fromRace, Race enemy, DiplomacyInfo info,
            boolean withMoraleEvent) {
        //if there is already war, then we don't have to declare it again
        if (fromRace.getAgreement(enemy.getRaceId()) == DiplomaticAgreement.WAR)
            return;

        String s = "";
        EmpireNews message;
        if (fromRace.isMajor()) {
            Major fromMajor = (Major) fromRace;
            if (enemy.isMajor())
                s = StringDB.getString("WE_DECLARE_WAR", false, ((Major) enemy).getEmpireNameWithAssignedArticle());
            else if (enemy.isMinor())
                s = StringDB.getString("WE_DECLARE_WAR_TO_MIN", false, enemy.getName());

            message = new EmpireNews();
            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
            fromMajor.getEmpire().addMsg(message);

            if (withMoraleEvent) {
                //generate extra message because of the morale
                String eventText = "";
                DiplomaticAgreement agreement = fromRace.getAgreement(enemy.getRaceId());
                String param = StringDB.getString("FEMALE_ARTICLE") + " " + enemy.getName();
                if (enemy.isMajor())
                    param = ((Major) enemy).getEmpireNameWithArticle();

                // Declare War on an Empire with Defense Pact #28
                if (enemy.isMajor() && agreement.getType() < DiplomaticAgreement.COOPERATION.getType()
                        && fromMajor.getDefencePact(enemy.getRaceId()))
                    eventText = fromMajor.getMoraleObserver().addEvent(28, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire when Neutral #24
                else if (agreement == DiplomaticAgreement.NONE)
                    eventText = fromMajor.getMoraleObserver().addEvent(24, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire when Non-Aggression #25
                else if (agreement == DiplomaticAgreement.NAP)
                    eventText = fromMajor.getMoraleObserver().addEvent(25, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire with Trade Treaty #26
                else if (agreement == DiplomaticAgreement.TRADE)
                    eventText = fromMajor.getMoraleObserver().addEvent(26, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire with Friendship Treaty #27
                else if (agreement == DiplomaticAgreement.FRIENDSHIP)
                    eventText = fromMajor.getMoraleObserver().addEvent(27, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire with CooperationTreaty #29
                else if (agreement == DiplomaticAgreement.COOPERATION)
                    eventText = fromMajor.getMoraleObserver().addEvent(29, fromMajor.getRaceMoralNumber(), param);
                // Declare War on an Empire with Affiliation #30
                else if (agreement == DiplomaticAgreement.ALLIANCE)
                    eventText = fromMajor.getMoraleObserver().addEvent(30, fromMajor.getRaceMoralNumber(), param);

                if (!eventText.isEmpty()) {
                    message = new EmpireNews();
                    message.CreateNews(eventText, EmpireNewsType.SOMETHING);
                    fromMajor.getEmpire().addMsg(message);
                }
            }

            if (enemy.isMajor()) {
                Major enemyMajor = (Major) enemy;
                //create answer on declaration of war
                DiplomacyInfo answer = new DiplomacyInfo(info);
                GenDiploMessage.generateMajorAnswer(answer);
                fromMajor.getIncomingDiplomacyNews().add(answer);

                String empireArticleName = fromMajor.getEmpireNameWithArticle();
                empireArticleName = Character.toString(empireArticleName.charAt(0)).toUpperCase()
                        + empireArticleName.substring(1);

                //if the war was declared directly to us (not by a warpartner
                if (info.warPartner.isEmpty()) {
                    s = StringDB.getString("WE_GET_WAR", false, empireArticleName);
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    enemyMajor.getEmpire().addMsg(message);
                } else {
                    Race partner = manager.getRaceController().getRace(info.warPartner);
                    if (partner == null)
                        return;

                    s = empireArticleName + " " + StringDB.getString("WAR_TO_PARTNER", false, partner.getName());
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    enemyMajor.getEmpire().addMsg(message);

                    s = empireArticleName + " " + StringDB.getString("WAR_TO_US_AS_PARTNER");
                    message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
                    enemyMajor.getEmpire().addMsg(message);
                }
            } else if (enemy.isMinor()) {
                DiplomacyInfo answer = new DiplomacyInfo(info);
                GenDiploMessage.generateMinorAnswer(manager, answer);
                fromMajor.getIncomingDiplomacyNews().add(answer);
            }
        } else if (fromRace.isMinor()) {
            s = StringDB.getString("MIN_OFFER_WAR", false, fromRace.getName());
            message = new EmpireNews();
            message.CreateNews(s, EmpireNewsType.DIPLOMACY, 2);
            ((Major) enemy).getEmpire().addMsg(message);
        }

        //morale for those who received the war declaration
        if (enemy.isMajor()) {
            Major enemyMajor = (Major) enemy;
            String param = StringDB.getString("FEMALE_ARTICLE") + " " + fromRace.getName();
            if (fromRace.isMajor())
                param = ((Major) fromRace).getEmpireNameWithArticle();
            String eventText = "";
            DiplomaticAgreement agreement = fromRace.getAgreement(enemy.getRaceId());
            // Other Empire Declares War when Neutral #31
            if (agreement == DiplomaticAgreement.NONE)
                eventText = enemyMajor.getMoraleObserver().addEvent(31, enemyMajor.getRaceMoralNumber(), param);
            // Other Empire Declares War with an Affiliation (or Membership) #33
            else if (agreement.getType() >= DiplomaticAgreement.ALLIANCE.getType())
                eventText = enemyMajor.getMoraleObserver().addEvent(33, enemyMajor.getRaceMoralNumber(), param);
            // Other Empire Declares War with Treaty #32
            else
                eventText = enemyMajor.getMoraleObserver().addEvent(32, enemyMajor.getRaceMoralNumber(), param);
            if (!eventText.isEmpty()) {
                message = new EmpireNews();
                message.CreateNews(eventText, EmpireNewsType.SOMETHING);
                enemyMajor.getEmpire().addMsg(message);
            }

            enemyMajor.getIncomingDiplomacyNews().add(info);
        }

        //set relations to bad in case of war
        fromRace.setRelation(enemy.getRaceId(), -100);
        enemy.setRelation(fromRace.getRaceId(), -100);
        //set agreement type to war
        fromRace.setAgreement(enemy.getRaceId(), DiplomaticAgreement.WAR);
        enemy.setAgreement(fromRace.getRaceId(), DiplomaticAgreement.WAR);
    }
}
