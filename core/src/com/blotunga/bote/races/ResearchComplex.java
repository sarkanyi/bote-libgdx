/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ShipRange;

public class ResearchComplex {
    ResearchStatus complexStatus; // status of the complex (researched, not researched, researching)
    ResearchStatus[] fieldStatus; // status of the individual fields of the complex
    private String complexName;
    private String complexDescription;
    private String[] fieldName;
    private String[] fieldDescription;
    private int[] bonus;

    public ResearchComplex() {
        complexStatus = ResearchStatus.NOTRESEARCHED;
        fieldStatus = new ResearchStatus[3];
        Arrays.fill(fieldStatus, ResearchStatus.NOTRESEARCHED);
        complexName = "";
        complexDescription = "";
        fieldName = new String[3];
        Arrays.fill(fieldName, "");
        fieldDescription = new String[3];
        Arrays.fill(fieldDescription, "");
        bonus = new int[3];
        Arrays.fill(bonus, 0);
    }

    public void reset() {
        Arrays.fill(fieldStatus, ResearchStatus.NOTRESEARCHED);
        complexName = "";
        complexDescription = "";
        Arrays.fill(fieldName, "");
        Arrays.fill(fieldDescription, "");
        Arrays.fill(bonus, 0);
    }

    public ResearchStatus getComplexStatus() {
        return complexStatus;
    }

    /**
     * Returns the status of a special research field
     * @param field - begins with 1, not 0 based!
     * @return
     */
    public ResearchStatus getFieldStatus(int field) {
        return fieldStatus[field - 1];
    }

    public String getComplexName() {
        return complexName;
    }

    public String getComplexDescription() {
        return complexDescription;
    }

    public String getFieldName(int field) {
        return fieldName[field - 1];
    }

    public String getFieldDescription(int field) {
        return fieldDescription[field - 1];
    }

    public int getBonus(int field) {
        return bonus[field - 1];
    }

    public void generateComplex(ResearchComplexType complex) {
        switch (complex) {
            case WEAPONS_TECHNOLOGY:
                bonus[0] = 30;
                bonus[1] = 25;
                bonus[2] = 20;
                break;
            case CONSTRUCTION_TECHNOLOGY:
                bonus[0] = 20;
                bonus[1] = 20;
                bonus[2] = 50;
                break;
            case GENERAL_SHIP_TECHNOLOGY:
                bonus[0] = ShipRange.MIDDLE.getRange();
                bonus[1] = 2;
                bonus[2] = 10;
                break;
            case PEACEFUL_SHIP_TECHNOLOGY:
                bonus[0] = 25;
                bonus[1] = 0;
                bonus[2] = 25;
                break;
            case TROOPS:
                bonus[0] = 20;
                bonus[1] = 500;
                bonus[2] = 20;
                break;
            case ECONOMY:
                bonus[0] = 10;
                bonus[1] = 5;
                bonus[2] = 25;
                break;
            case PRODUCTION:
                bonus[0] = 15;
                bonus[1] = 5;
                bonus[2] = 20;
                break;
            case DEVELOPMENT_AND_SECURITY:
                bonus[0] = 10;
                bonus[1] = 10;
                bonus[2] = 5;
                break;
            case RESEARCH:
                bonus[0] = 20;
                bonus[1] = 20;
                bonus[2] = 20;
                break;
            case SECURITY:
                bonus[0] = 20;
                bonus[1] = 15;
                bonus[2] = 25;
                break;
            case STORAGE_AND_TRANSPORT:
                bonus[0] = 2;
                bonus[1] = 0;
                bonus[2] = 1;
                break;
            case TRADE:
                bonus[0] = 0;
                bonus[1] = 30;
                bonus[2] = 1;
                break;
            case FINANCES:
                bonus[0] = 5;
                bonus[1] = 10;
                bonus[2] = 35;
                break;
            default:
                reset();
                break;
        }
        readSpecialTech(complex);
    }

    public void readSpecialTech(ResearchComplexType complex) {
        String[] data = new String[8];
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/names/specialtechs.txt");
        String input = handle.readString(GameConstants.getCharset());
        String[] inputs = input.split("\n");
        int z = 0;
        int i = complex.getType() * 8;
        while (i < inputs.length) {
            if (i == complex.getType() * 8)
                data[z++] = inputs[i].trim();
            else
                data[z++] = inputs[i].trim();
            if (z == 8)
                break;
            i++;
        }
        i = 0;
        complexName = data[i++];
        complexDescription = data[i++];
        fieldName[0] = data[i++];
        fieldName[1] = data[i++];
        fieldName[2] = data[i++];
        fieldDescription[0] = data[i++];
        fieldDescription[1] = data[i++];
        fieldDescription[2] = data[i++];
    }
}
