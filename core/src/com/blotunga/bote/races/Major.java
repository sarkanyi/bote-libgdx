/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ai.MajorAI;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.events.EventFirstContact;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipHistory;
import com.blotunga.bote.ships.ShipHistoryStruct;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.trade.Trade;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ObjectSetSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Major extends Race {
    private transient boolean player; 					///< is the race played by a human?
    private String empireName;							///< longer empire name
    private String empireWithArticle;					///< article of empire name
    private String empireWithAssignedArticle;			///< assigned article for the empire name
    private String prefix;								///< race prefix

    private int diplomacyBonus;							///< bonus for diplomatic treaties, 0 == no bonus/malus

    private ObjectIntMap<String> agrDuration;		///< remaining turns of an agreement, (0 == unlimited)
    private ObjectSet<String> defencePact;				///< does the race have a defence pact with an another race
    private ObjectIntMap<String> defDuration;		///< saves the duration of the defence pacts

    private RaceDesign raceDesign;						///< holds the information about racial colors, fonts etc

    private Empire empire;								///< the empire of the main race
    private Trade trade;								///< trade info for this race
    private ShipHistory shipHistory;					///< statistic data about the ships of this race
    private transient StarMap starMap;

    private MoraleObserver moraleObserver;				///< here are the events saved which influence the morale of a race
    private WeaponObserver weaponObserver;              ///< observes the weapons of the ships, needed to design ships
    private transient boolean disabled;

    public Major() {
        this(null);
    }

    public Major(ResourceManager res) {
        super(RaceType.RACE_TYPE_MAJOR, res);
        player = false;
        empireName = "";
        empireWithArticle = "";
        empireWithAssignedArticle = "";
        prefix = "";
        diplomacyBonus = 0;

        agrDuration = new ObjectIntMap<String>();
        defencePact = new ObjectSet<String>();
        defDuration = new ObjectIntMap<String>();

        raceDesign = new RaceDesign();
        empire = new Empire();
        trade = new Trade();
        shipHistory = new ShipHistory();
        moraleObserver = new MoraleObserver();
        weaponObserver = new WeaponObserver();
        starMap = null;
    }

    @Override
    public int create(String[] info, int pos) {
        raceID = info[pos++].trim();
        homeSystem = info[pos++].trim();
        name = info[pos++].trim();
        nameArticle = info[pos++].trim();

        empireName = info[pos++].trim();
        empireWithArticle = info[pos++].trim() + " " + empireName;
        empireWithAssignedArticle = info[pos++].trim() + " " + empireName;
        prefix = info[pos++].trim();

        description = info[pos++].trim();
        buildingNumber = Integer.parseInt(info[pos++].trim());
        shipNumber = Integer.parseInt(info[pos++].trim());
        moralNumber = Integer.parseInt(info[pos++].trim());

        String raceProperties = info[pos++].trim();
        String[] racePropertyArray = raceProperties.split(",");
        for (int i = 0; i < racePropertyArray.length; i++) {
            RaceProperty prop = RaceProperty.fromInt(Integer.parseInt(racePropertyArray[i].trim()));
            setRaceProperty(prop, true);
        }
        String raceAbilities = info[pos++].trim();
        String[] raceAbilityArray = raceAbilities.split(",");
        for (int i = 0; i < raceAbilityArray.length; i++) {
            int ability = Integer.parseInt(raceAbilityArray[i].trim());
            setSpecialAbility(ability, true);
        }

        diplomacyBonus = Integer.parseInt(info[pos++].trim());
        graphicFile = info[pos++].trim().replaceFirst(".bop", "");
        raceDesign.clrSector = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrSmallBtn = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrLargeBtn = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrSmallText = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrNormalText = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrSecondText = RaceDesign.stringToColor(info[pos++].trim());

        raceDesign.clrGalaxySectorText = RaceDesign.stringToColor(info[pos++].trim());

        raceDesign.clrListMarkTextColor = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrListMarkPenColor = RaceDesign.stringToColor(info[pos++].trim());
        raceDesign.clrRouteColor = RaceDesign.stringToColor(info[pos++].trim());

        pos += 2; // font size shouldn't be important for us

        raceDesign.fontName = info[pos++].trim();

        player = false;
        float fTax = 1.15f;
        if (isRaceProperty(RaceProperty.FINANCIAL))
            fTax = Math.min(fTax, 1.05f);
        trade.setTax(fTax);
        trade.getTradeHistory().saveCurrentPrices(trade.getResourcePrice(), trade.getTax());

        empire.setEmpireID(raceID);

        diplomacyAI = new MajorAI(resourceManager, this);
        return pos;
    }

    @Override
    public Empire getEmpire() {
        return empire;
    }

    public RaceDesign getRaceDesign() {
        return raceDesign;
    }

    public MoraleObserver getMoraleObserver() {
        return moraleObserver;
    }

    public WeaponObserver getWeaponObserver() {
        return weaponObserver;
    }

    public Trade getTrade() {
        return trade;
    }

    public ShipHistory getShipHistory() {
        return shipHistory;
    }

    public StarMap getStarMap() {
        return starMap;
    }

    public void createStarMap() {
        starMap = new StarMap(!player, 3 - ShipRange.MIDDLE.getRange(), resourceManager);
    }

    public String getEmpireName() {
        return empireName;
    }

    public String getEmpireNameWithArticle() {
        return empireWithArticle;
    }

    public String getEmpireNameWithAssignedArticle() {
        return empireWithAssignedArticle;
    }

    public String getPrefix() {
        return prefix;
    }

    public boolean isHumanPlayer() {
        return player;
    }

    /**
     * in addition to that this major belongs to a human player, consider whether autoturns is active
     * @return if false, the ai plays
     */
    public boolean aHumanPlays() {
        if (!isHumanPlayer())
            return false;
        return resourceManager.getCurrentRound() > resourceManager.getGamePreferences().autoTurns;
    }

    public boolean canBuildShip(ShipType type, int[] researchLevel, ShipInfo info) {
        return info.getRace() == getRaceShipNumber() && info.getShipType() == type
                && info.isThisShipBuildableNow(researchLevel);
    }

    public int bestBuildableVariant(ShipType type, Array<ShipInfo> shipInfoArray) {
        int id = -1;
        int costs = 0;
        Research rs = empire.getResearch();
        int[] researchLevels = rs.getResearchLevels();
        for (int i = 0; i < shipInfoArray.size; i++) {
            ShipInfo info = shipInfoArray.get(i);
            if (canBuildShip(type, researchLevels, info) && info.getBaseIndustry() > costs) {
                costs = info.getBaseIndustry();
                id = info.getID();
            }
        }
        return id;
    }

    public void setHumanPlayer(boolean is) {
        player = is;
    }

    public int getDiplomacyBonus() {
        return diplomacyBonus;
    }

    public int getAgreementDuration(String raceId) {
        return agrDuration.get(raceId, 0);
    }

    public void setAgreementDuration(String raceId, int newValue) {
        if (newValue == 0)
            agrDuration.remove(raceId, 0);
        else
            agrDuration.put(raceId, newValue);
    }

    @Override
    public void setAgreement(String otherRace, DiplomaticAgreement newAgreement) {
        if (newAgreement == DiplomaticAgreement.DEFENCEPACT) {
            setDefencePact(otherRace, true);
            if (getAgreement(otherRace) == DiplomaticAgreement.WAR)
                newAgreement = DiplomaticAgreement.NONE;
            else if (getAgreement(otherRace) == DiplomaticAgreement.ALLIANCE)
                newAgreement = DiplomaticAgreement.ALLIANCE;
        }
        // if there is no agreement, we can remove the entries
        if (newAgreement == DiplomaticAgreement.NONE) {
            agreements.remove(otherRace);
            setAgreementDuration(otherRace, 0);
        } else if (newAgreement != DiplomaticAgreement.DEFENCEPACT)
            agreements.put(otherRace, newAgreement);

        // in war the defencepact is erased, in alliance it's granted automatically
        if (newAgreement == DiplomaticAgreement.WAR || newAgreement == DiplomaticAgreement.ALLIANCE) {
            defencePact.remove(otherRace);
            setDefencePactDuration(otherRace, 0);
        }
        //in case of ware set the duration to 0
        if (newAgreement == DiplomaticAgreement.WAR)
            setAgreementDuration(otherRace, 0);
    }

    @Override
    public void contact(Race race, StarSystem p) {
        super.contact(race, p);
        String sectorCoord = p.coordsName(true);

        String key = race.isMajor() ? "GET_CONTACT_TO_MAJOR" : "GET_CONTACT_TO_MINOR";
        String msg = StringDB.getString(key, false, race.getName(), sectorCoord);

        EmpireNews message = new EmpireNews();
        message.CreateNews(msg, EmpireNewsType.DIPLOMACY, "", p.getCoordinates());
        empire.addMsg(message);

        if (isHumanPlayer()) {
            EventFirstContact event = new EventFirstContact(resourceManager, race.getRaceId());
            empire.pushEvent(event);
        }
    }

    @Override
    public void addLostShipHistory(ShipHistoryStruct ship, String event, String status, int round, IntPoint coord) {
        shipHistory.modifyShip(ship, resourceManager.getUniverseMap().getStarSystemAt(coord).coordsName(true), round,
                event, status);
    }

    @Override
    public void lostFlagShip(Ship ship) {
        String eventText = moraleObserver.addEvent(7, getRaceMoralNumber(), ship.getName());
        EmpireNews message = new EmpireNews();
        message.CreateNews(eventText, EmpireNewsType.MILITARY, "", ship.getCoordinates());
        empire.addMsg(message);
    }

    @Override
    public void lostStation(ShipType type) {
        String eventText = moraleObserver.addEvent(9, getRaceMoralNumber());
        if (type == ShipType.OUTPOST)
            eventText = moraleObserver.addEvent(8, getRaceMoralNumber());
        EmpireNews message = new EmpireNews();
        message.CreateNews(eventText, EmpireNewsType.MILITARY);
        empire.addMsg(message);
    }

    @Override
    public void lostShipToAnomaly(Ships ship, String anomaly) {
        String ss = String.format("%s (%s, %s)", ship.getName(), ship.getShipTypeAsString(), ship.getShipClass());
        String s = StringDB.getString("ANOMALY_SHIP_LOST", false, ss, anomaly);
        EmpireNews message = new EmpireNews();
        message.CreateNews(s, EmpireNewsType.MILITARY, "", ship.getCoordinates());
        empire.addMsg(message);
        if (isHumanPlayer())
            resourceManager.getClientWorker().setEmpireViewFor(this);
    }

    /**
     * Returns whether the major race a defense pact with the other race has
     * @param raceID
     * @return
     */
    public boolean getDefencePact(String raceID) {
        return defencePact.contains(raceID);
    }

    public void setDefencePact(String otherRace, boolean is) {
        if (is && !getDefencePact(otherRace))
            defencePact.add(otherRace);
        else if (!is && getDefencePact(otherRace)) {
            defencePact.remove(otherRace);
            setDefencePactDuration(otherRace, 0);
        }
    }

    public int getDefencePactDuration(String otherRace) {
        return defDuration.get(otherRace, 0);
    }

    public void setDefencePactDuration(String raceId, int newValue) {
        if (newValue == 0)
            defDuration.remove(raceId, 0);
        else
            defDuration.put(raceId, newValue);
    }

    /**
     * @return true if some agreement has ended
     */
    public boolean decrementAgreementsDuration() {
        boolean end = false;
        Array<String> delAgrs = new Array<String>();

        //Reduce the number of rounds by 1 except if the agreement is unlimited (duration equals 0)
        for (Iterator<ObjectMap.Entry<String, DiplomaticAgreement>> iter = agreements.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, DiplomaticAgreement> e = iter.next();
            if (e.key.equals(raceID))
                continue;

            int duration = getAgreementDuration(e.key);
            if (duration > 1)
                setAgreementDuration(e.key, duration - 1);
            else if (duration == 1) {
                String agr = StringDB.getString(getAgreement(e.key).getdbName());
                String rname = "";
                Race r = resourceManager.getRaceController().getRace(e.key);
                if (r.isMajor())
                    rname = ((Major) r).empireWithAssignedArticle;
                String s = StringDB.getString("CONTRACT_ENDED", false, agr, rname);
                EmpireNews message = new EmpireNews();
                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                empire.addMsg(message);
                delAgrs.add(e.key);
            }
        }

        for (int i = 0; i < delAgrs.size; i++) {
            agreements.remove(delAgrs.get(i));
            agrDuration.remove(delAgrs.get(i), 0);
            end = true;
        }
        delAgrs.clear();

        //handle defense pacts separateley
        for (Iterator<String> iter = defencePact.iterator(); iter.hasNext();) {
            String val = iter.next();
            if (val.equals(raceID))
                continue;
            int duration = getDefencePactDuration(val);
            if (duration > 1)
                setDefencePactDuration(val, duration - 1);
            else if (duration == 1) {
                String rname = "";
                Race r = resourceManager.getRaceController().getRace(val);
                if (r.isMajor())
                    rname = ((Major) r).empireWithAssignedArticle;
                String s = StringDB.getString("DEFENCE_PACT_ENDED", false, rname);
                EmpireNews message = new EmpireNews();
                message.CreateNews(s, EmpireNewsType.DIPLOMACY);
                empire.addMsg(message);
                delAgrs.add(val);
            }
        }
        for (int i = 0; i < delAgrs.size; i++) {
            defencePact.remove(delAgrs.get(i));
            defDuration.remove(delAgrs.get(i), 0);
            end = true;
        }
        delAgrs.clear();
        return end;
    }

    public float creditsMulti() {
        float multi = 1.0f;
        if (isRaceProperty(RaceProperty.FINANCIAL))
            multi += 0.5f;
        if (isRaceProperty(RaceProperty.PRODUCER))
            multi += 0.25f;
        if (isRaceProperty(RaceProperty.WARLIKE) || isRaceProperty(RaceProperty.HOSTILE))
            multi -= 0.1f;
        if (isRaceProperty(RaceProperty.SNEAKY))
            multi -= 0.2f;
        if (isRaceProperty(RaceProperty.SECRET))
            multi -= 0.25f;
        if (isRaceProperty(RaceProperty.SOLOING))
            multi -= 0.5f;
        multi = Math.max(multi, 0.0f);
        return multi;
    }

    public int countTroops() {
        int cnt = 0;
        for (int i = 0; i < empire.getSystemList().size; i++) {
            StarSystem ss = resourceManager.getUniverseMap().getStarSystemAt(empire.getSystemList().get(i));
            if (ss.getTroops().size > 0)
                cnt += ss.getTroops().size;
        }
        return cnt;
    }

    /**
     * Function moved from system view in original
     */
    public void reflectPossibleResearchOrSecurityWorkerChange(IntPoint coords, boolean manager, boolean energy) {
        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(coords);
        empire.addResearchPoints(-system.getProduction().getResearchProd());
        empire.addSecurityPoints(-system.getProduction().getSecurityProd());
        system.calculateVariables();
        if (manager)
            system.executeManager(this, false, energy);

        empire.addResearchPoints(system.getProduction().getResearchProd());
        empire.addSecurityPoints(system.getProduction().getSecurityProd());
    }

    public void setDisabled(boolean is) {
        disabled = is;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public static Major toMajor(Race race) {
        if (race != null && race.isMajor())
            return (Major) race;
        else
            return null;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeString(empireName);
        output.writeString(empireWithArticle);
        output.writeString(empireWithAssignedArticle);
        output.writeString(prefix);
        output.writeInt(diplomacyBonus);
        kryo.writeObject(output, agrDuration);
        kryo.writeObject(output, defencePact, new ObjectSetSerializer());
        kryo.writeObject(output, defDuration);
        kryo.writeObject(output, raceDesign);
        kryo.writeObject(output, empire);
        kryo.writeObject(output, trade);
        kryo.writeObject(output, shipHistory);
        kryo.writeObject(output, moraleObserver);
        kryo.writeObject(output, weaponObserver);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        empireName = input.readString();
        empireWithArticle = input.readString();
        empireWithAssignedArticle = input.readString();
        prefix = input.readString();
        diplomacyBonus = input.readInt();
        agrDuration = kryo.readObject(input, ObjectIntMap.class);
        defencePact = kryo.readObject(input, ObjectSet.class);
        defDuration = kryo.readObject(input, ObjectIntMap.class);
        raceDesign = kryo.readObject(input, RaceDesign.class);
        empire = kryo.readObject(input, Empire.class);
        trade = kryo.readObject(input, Trade.class);
        shipHistory = kryo.readObject(input, ShipHistory.class);
        moraleObserver = kryo.readObject(input, MoraleObserver.class);
        weaponObserver = kryo.readObject(input, WeaponObserver.class);
    }
}
