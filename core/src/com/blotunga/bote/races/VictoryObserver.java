/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class VictoryObserver {
    private boolean conditionStatus[];			//is the victory condition active?
    private ObjectIntMap<String> diplomacy;
    private ObjectIntMap<String> slavery;
    private ObjectIntMap<String> research;
    private ObjectIntMap<String> combatWins;
    private ObjectIntMap<String> sabotage;

    private int rivalsLeft;

    private boolean isVictory;
    private VictoryType victoryType;
    private String victoryRace;

    public VictoryObserver() {
        conditionStatus = new boolean[VictoryType.values().length];
        Arrays.fill(conditionStatus, true);
        diplomacy = new ObjectIntMap<String>();
        slavery = new ObjectIntMap<String>();
        research = new ObjectIntMap<String>();
        combatWins = new ObjectIntMap<String>();
        sabotage = new ObjectIntMap<String>();

        rivalsLeft = Integer.MAX_VALUE;
        isVictory = false;
        victoryType = VictoryType.VICTORYTYPE_ELIMINATION;
        victoryRace = "";
    }

    public void reset() {
        Arrays.fill(conditionStatus, true);

        diplomacy.clear();
        slavery.clear();
        research.clear();
        combatWins.clear();
        sabotage.clear();
        rivalsLeft = Integer.MAX_VALUE;

        isVictory = false;
        victoryType = VictoryType.VICTORYTYPE_ELIMINATION;
        victoryRace = "";
    }

    public boolean isVictory() {
        return isVictory;
    }

    public String getVictoryRace() {
        return victoryRace;
    }

    public VictoryType getVictoryType() {
        return victoryType;
    }

    public boolean isVictoryCondition(VictoryType type) {
        return conditionStatus[type.ordinal()];
    }

    public int getRivalsLeft() {
        return rivalsLeft;
    }

    public int getVictoryStatus(String raceId, VictoryType type) {
        switch (type) {
            case VICTORYTYPE_ELIMINATION:
                return rivalsLeft;
            case VICTORYTYPE_DIPLOMACY:
                return diplomacy.get(raceId, 0);
            case VICTORYTYPE_CONQUEST:
                return slavery.get(raceId, 0);
            case VICTORYTYPE_RESEARCH:
                return research.get(raceId, 0);
            case VICTORYTYPE_COMBATWINS:
                return combatWins.get(raceId, 0);
            case VICTORYTYPE_SABOTAGE:
                return sabotage.get(raceId, 0);
        }
        return 0;
    }

    public int getBestVictoryValue(VictoryType type) {
        IntArray values = new IntArray();
        switch (type) {
            case VICTORYTYPE_ELIMINATION:
                return rivalsLeft;
            case VICTORYTYPE_DIPLOMACY:
                for (Iterator<ObjectIntMap.Entry<String>> iter = diplomacy.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<String> e = iter.next();
                    values.add(e.value);
                }
                break;
            case VICTORYTYPE_CONQUEST:
                for (Iterator<ObjectIntMap.Entry<String>> iter = slavery.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<String> e = iter.next();
                    values.add(e.value);
                }
                break;
            case VICTORYTYPE_RESEARCH:
                for (Iterator<ObjectIntMap.Entry<String>> iter = research.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<String> e = iter.next();
                    values.add(e.value);
                }
                break;
            case VICTORYTYPE_COMBATWINS:
                for (Iterator<ObjectIntMap.Entry<String>> iter = combatWins.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<String> e = iter.next();
                    values.add(e.value);
                }
                break;
            case VICTORYTYPE_SABOTAGE:
                for (Iterator<ObjectIntMap.Entry<String>> iter = sabotage.entries().iterator(); iter.hasNext();) {
                    ObjectIntMap.Entry<String> e = iter.next();
                    values.add(e.value);
                }
                break;
        }

        values.sort();
        if (values.size != 0)
            return values.get(values.size - 1);
        return 0;
    }

    public int getNeededVictoryValue(ResourceManager manager, VictoryType type) {
        int currentRound = manager.getCurrentRound();
        int value = 0;

        switch (type) {
            case VICTORYTYPE_ELIMINATION:
                return 0; //all other races must be eliminated
            case VICTORYTYPE_DIPLOMACY:
                value = manager.getRaceController().getRaces().size;
                value--;
                return Math.max(value >> 1, 10);
            case VICTORYTYPE_CONQUEST:
                for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
                    StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
                    if (ss.getCurrentInhabitants() > 0.0 && !ss.isFree())
                        value++;
                }
                return Math.max(value >> 1, 10);
            case VICTORYTYPE_RESEARCH:
                return 10;
            case VICTORYTYPE_COMBATWINS:
                return (int) Math.max(currentRound / 2.25, 125);
            case VICTORYTYPE_SABOTAGE:
                return (int) Math.max(currentRound * 1.6, 250);
        }
        return Integer.MAX_VALUE;
    }

    public void observe(ResourceManager manager) {
        conditionStatus = manager.getGamePreferences().victoryTypes;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_ELIMINATION
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_ELIMINATION.ordinal()]) {
            int majorsAlive = 0;
            for (int i = 0; i < majors.size; i++)
                if (majors.getValueAt(i).getEmpire().countSystems() != 0)
                    majorsAlive++;

            rivalsLeft = majorsAlive - 1;
            if (rivalsLeft == getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_ELIMINATION)) {
                isVictory = true;
                for (int i = 0; i < majors.size; i++)
                    if (majors.getValueAt(i).getEmpire().countSystems() > 0) {
                        victoryRace = majors.getKeyAt(i);
                        break;
                    }
                victoryType = VictoryType.VICTORYTYPE_ELIMINATION;
                return;
            }
        }

        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_DIPLOMACY
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_DIPLOMACY.ordinal()]) {
            diplomacy.clear();
            ArrayMap<String, Race> races = manager.getRaceController().getRaces();
            for (int i = 0; i < majors.size; i++) {
                int highAgreements = 0;
                for (int j = 0; j < races.size; j++) {
                    if (!majors.getKeyAt(i).equals(races.getKeyAt(j))) {
                        DiplomaticAgreement agreement = majors.getValueAt(i).getAgreement(races.getKeyAt(j));
                        if (agreement == DiplomaticAgreement.ALLIANCE || agreement == DiplomaticAgreement.MEMBERSHIP)
                            highAgreements++;
                    }
                }
                if (highAgreements > 0)
                    diplomacy.put(majors.getKeyAt(i), highAgreements);
            }

            int neededValue = getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_DIPLOMACY);
            for (Iterator<ObjectIntMap.Entry<String>> iter = diplomacy.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                if (e.value >= neededValue) {
                    isVictory = true;
                    victoryRace = e.key;
                    victoryType = VictoryType.VICTORYTYPE_DIPLOMACY;
                    return;
                }
            }
        }

        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_CONQUEST
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_CONQUEST.ordinal()]) {
            slavery.clear();
            for (int i = 0; i < majors.size; i++) {
                Major major = majors.getValueAt(i);
                int value = 0;
                for (int j = 0; j < major.getEmpire().getSystemList().size; j++) {
                    IntPoint info = major.getEmpire().getSystemList().get(j);
                    if (manager.getUniverseMap().getStarSystemAt(info).isTaken())
                        value++;
                }
                if (value > 0)
                    slavery.put(majors.getKeyAt(i), value);
            }

            int neededValue = getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_CONQUEST);
            for (Iterator<ObjectIntMap.Entry<String>> iter = slavery.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                if (e.value >= neededValue) {
                    isVictory = true;
                    victoryRace = e.key;
                    victoryType = VictoryType.VICTORYTYPE_CONQUEST;
                    return;
                }
            }
        }

        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_RESEARCH
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_RESEARCH.ordinal()]) {
            research.clear();
            for (int i = 0; i < majors.size; i++) {
                Major major = majors.getValueAt(i);
                int value = major.getEmpire().getResearch().getNumberOfUnique() - 1;
                if (value > 0)
                    research.put(majors.getKeyAt(i), value);
            }

            int neededValue = getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_RESEARCH);
            for (Iterator<ObjectIntMap.Entry<String>> iter = research.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                if (e.value >= neededValue) {
                    isVictory = true;
                    victoryRace = e.key;
                    victoryType = VictoryType.VICTORYTYPE_RESEARCH;
                    return;
                }
            }
        }

        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_RESEARCH
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_COMBATWINS.ordinal()]) {
            int neededValue = getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_COMBATWINS);
            for (Iterator<ObjectIntMap.Entry<String>> iter = combatWins.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                if (e.value >= neededValue) {
                    isVictory = true;
                    victoryRace = e.key;
                    victoryType = VictoryType.VICTORYTYPE_COMBATWINS;
                    return;
                }
            }
        }

        //////////////////////////////////////////////////////////////////////
        // VICTORYTYPE_SABOTAGE
        //////////////////////////////////////////////////////////////////////
        if (conditionStatus[VictoryType.VICTORYTYPE_SABOTAGE.ordinal()]) {
            int neededValue = getNeededVictoryValue(manager, VictoryType.VICTORYTYPE_SABOTAGE);
            for (Iterator<ObjectIntMap.Entry<String>> iter = sabotage.entries().iterator(); iter.hasNext();) {
                ObjectIntMap.Entry<String> e = iter.next();
                if (e.value >= neededValue) {
                    isVictory = true;
                    victoryRace = e.key;
                    victoryType = VictoryType.VICTORYTYPE_SABOTAGE;
                    return;
                }
            }
        }
    }

    public void addSabotageAction(String raceID, int val) {
        int oldValue = sabotage.get(raceID, 0);
        oldValue += val;
        sabotage.put(raceID, oldValue);
    }

    public void addCombatWin(String raceID) {
        int oldValue = combatWins.get(raceID, 0);
        combatWins.put(raceID, ++oldValue);
    }
}
