/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.ships.BeamWeapons;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.TorpedoInfo;
import com.blotunga.bote.ships.TorpedoWeapons;

public class WeaponObserver {
    public static class BeamWeaponsObserverStruct {
        public String weaponName;
        public int maxLevel;

        public BeamWeaponsObserverStruct() {
            weaponName = "";
            maxLevel = 0;
        }

        public BeamWeaponsObserverStruct(String weaponName, int maxLevel) {
            this.weaponName = weaponName;
            this.maxLevel = maxLevel;
        }
    }

    public static class TubeWeaponsObserverStruct {
        public String tubeName;
        public int number;
        public int fireRate;
        public boolean onlyMicro;
        public int fireAngle;

        public TubeWeaponsObserverStruct() {
            tubeName = "";
            number = 0;
            fireRate = 0;
            fireAngle = 0;
            onlyMicro = false;
        }

        public TubeWeaponsObserverStruct(String tubeName, int number, int fireRate, boolean onlyMicro, int fireAngle) {
            this.tubeName = tubeName;
            this.number = number;
            this.fireRate = fireRate;
            this.onlyMicro = onlyMicro;
            this.fireAngle = fireAngle;
        }
    }

    private Array<BeamWeaponsObserverStruct> beamWeapons; //< Array with all buildable weapons of the empire
    private Array<TubeWeaponsObserverStruct> tubeWeapons; //< Array with all buildable torpedo launchers of the empire
    private boolean buildableTorpedos[];                  //< stores what types of torpedoes can be used on ships
    private int maxShieldLevel;                           //< currently buildable maximum shield level

    public WeaponObserver() {
        beamWeapons = new Array<BeamWeaponsObserverStruct>();
        tubeWeapons = new Array<TubeWeaponsObserverStruct>();
        buildableTorpedos = new boolean[GameConstants.DIFFERENT_TORPEDOS];
        Arrays.fill(buildableTorpedos, false);
        maxShieldLevel = 0;
    }

    /**
     * The function returns the maximum buildable type of a beam weapon.
     * @param nameOfBeamWeapon
     * @return 0 if not buildable
     */
    public int getMaxBeamType(String nameOfBeamWeapon) {
        for (int i = 0; i < beamWeapons.size; i++)
            if (beamWeapons.get(i).weaponName.equals(nameOfBeamWeapon))
                return beamWeapons.get(i).maxLevel;
        return 0;
    }

    /**
     * The function searches a new buildable torpedo
     * @param currentTorpedoType
     * @param onlyMicroTube
     * @return
     */
    public int getNextTorpedo(int currentTorpedoType, boolean onlyMicroTube) {
        int i = currentTorpedoType + 1;
        if (i == GameConstants.DIFFERENT_TORPEDOS)
            i = 0;
        while (i < GameConstants.DIFFERENT_TORPEDOS) {
            if (buildableTorpedos[i]) {
                //if the current launcher can only fire microtorpedoes then check that it is a micro
                if (onlyMicroTube && TorpedoInfo.isMicro(i))
                    return i;
                else if (!onlyMicroTube)
                    return i;
            }
            i++;
            if (i == GameConstants.DIFFERENT_TORPEDOS)
                i = 0;
        }
        return currentTorpedoType;
    }

    /**
     * The function searches for a new Torpedolauncher which can be mounted on a ship
     * @param currentTubeName
     * @param currentTorpedoType
     * @return
     */
    public TubeWeaponsObserverStruct getNextTube(String currentTubeName, int currentTorpedoType) {
        TubeWeaponsObserverStruct twos = null;
        int count = 0;
        //search for the current
        for (int i = 0; i < tubeWeapons.size; i++)
            if (tubeWeapons.get(i).tubeName.equals(currentTubeName)) {
                count = i;
                twos = tubeWeapons.get(i);
                break;
            }
        //now search for the next one
        for (int i = (count + 1); i <= tubeWeapons.size; i++) {
            if (i == tubeWeapons.size)
                i = 0;
            if (TorpedoInfo.isMicro(currentTorpedoType) && tubeWeapons.get(i).onlyMicro) {
                twos = tubeWeapons.get(i);
                return twos;
            } else if (!tubeWeapons.get(i).onlyMicro) {
                twos = tubeWeapons.get(i);
                return twos;
            }
        }
        return twos;
    }

    /**
     * Gets the maximum shield level of the empire
     * @return
     */
    public int getMaxShieldType() {
        return maxShieldLevel;
    }

    /**
     * The function checks all buildable beams of the empire and sets all variables to the correct value
     * @param info - is a currently buildable ship
     */
    public void checkBeamWeapons(ShipInfo info) {
        for (int j = 0; j < info.getBeamWeapons().size; j++) {
            BeamWeapons bwi = info.getBeamWeapons().get(j);
            boolean foundBeamName = false;
            for (int i = 0; i < beamWeapons.size; i++) {
                if (beamWeapons.get(i).weaponName.equals(bwi.getBeamName())) {
                    foundBeamName = true;
                    if (beamWeapons.get(i).maxLevel < bwi.getBeamType()) {
                        BeamWeaponsObserverStruct bwos = new BeamWeaponsObserverStruct(bwi.getBeamName(),
                                bwi.getBeamType());
                        beamWeapons.set(i, bwos);
                    }
                }
            }
            if (!foundBeamName) {
                BeamWeaponsObserverStruct bwos = new BeamWeaponsObserverStruct(bwi.getBeamName(), bwi.getBeamType());
                beamWeapons.add(bwos);
            }
        }
        if (info.getShield().getShieldType() > maxShieldLevel)
            maxShieldLevel = info.getShield().getShieldType();
    }

    /**
     * Theis function checks all types of torpedoweapons
     * @param info
     */
    public void checkTorpedoWeapons(ShipInfo info) {
        for (int j = 0; j < info.getTorpedoWeapons().size; j++) {
            TorpedoWeapons twi = info.getTorpedoWeapons().get(j);
            buildableTorpedos[twi.getTorpedoType()] = true;
            boolean foundTubeName = false;

            for (int k = 0; k < tubeWeapons.size; k++) {
                if (tubeWeapons.get(k).tubeName.equals(twi.getTubeName())) {
                    foundTubeName = true;
                    break;
                }
            }
            if (!foundTubeName) {
                TubeWeaponsObserverStruct twos = new TubeWeaponsObserverStruct(twi.getTubeName(), twi.getNumber(),
                        twi.getTubeFirerate(), twi.isOnlyMicroPhoton(), twi.getFireArc().getAngle());
                tubeWeapons.add(twos);
            }
        }
    }
}
