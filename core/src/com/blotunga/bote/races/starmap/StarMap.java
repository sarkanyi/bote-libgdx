/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races.starmap;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

/**
 * @author dragon
 * @version 1.1
 */
public class StarMap {
    public enum RangeTypes {
        SM_RANGE_SPACE(0),
        SM_RANGE_FAR(1),
        SM_RANGE_MIDDLE(2),
        SM_RANGE_NEAR(3);

        private int type;

        RangeTypes(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    private class PathSector implements Comparable<PathSector> {
        boolean used;		///< true if the node was already used, false otherwise
        double distance;	///< distance to start
        int hops;			///< number of hops from start sector
        IntPoint parent;	///< coordinates of the parent
        IntPoint position;	///< coordinates of the sector

        public PathSector() {
            used = false;
            distance = 0.0;
            hops = 0;
            parent = new IntPoint(-1, -1);
            position = new IntPoint(-1, -1);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof PathSector) {
                PathSector a = (PathSector) obj;
                return a.position.equals(position);
            } else
                return false;
        }

        @Override
        public int compareTo(PathSector o) {
            if (distance < o.distance)
                return -1;
            else if (distance == o.distance)
                return 0;
            else
                return 1;
        }
    }

    public class BaseSector implements Comparable<BaseSector> {
        public IntPoint position;
        public int points; // score of the sector

        public BaseSector() {
            position = new IntPoint(-1, -1);
            points = -1;
        }

        @Override
        public int compareTo(BaseSector o) {
            if (points > o.points) // we want to sort in a descending order
                return -1;
            else if (points == o.points)
                return 0;
            else
                return 1;
        }
    }

    private int[] range;			///< map which contains the distance to the next outpost (x, y) - models influence of the outpost
    private IntPoint selection;		///< coordinates of the selected sector (-1,-1) if none is selected
    private Array<IntPoint> bases;	///< list of sectors which contain an outpost
    private RangeMap rangeMap;		///< the local rangeMap which is used for newly added outposts

    private PathSector[] pathMap;		///< array which contains informations for calculating the intest routes
    private Array<PathSector> leaves;	///< list of neighbors which can be reached from the current leaf
    private IntPoint pathStart;				///< start sector by last call of calcPath(), (-1,-1) if not set
    private int pathRange;					///< range by last call of calcPath(), 0 if not set
    boolean isAICalculation;				///< activates extra calculations for outpost build
    private int aiRange;					///< maximum range inside which an outpost should be built and which should be contiguous
    private Array<IntPoint> aiTargets;		///< target which are preferred when expanding
    private Array<IntPoint> aiKnownSystems; ///< list of systems known to AI (by scanning)
    private int[] aiRangePoints;			///< territory growth for sector (x,y) if an outpost was built in that sector
    private int[] aiNeighbourCount;			///< for sectors outside the selected range the number of neighbors inside range
    private int[] aiConnectionPoints;		///< for the connection of a territory so that there are no holes and to avoid territories which aren't connected
    private int[] aiTargetPoints;			///< values for preferred growth directions
    private int[] aiBadPoints;				///< negative scoring for a territory because of enemy borders. The ai should build an outpost in an empy space in the middle of enemy territory
    private int[] explorePoints;
    private Array<BaseSector> exploreTargets;
    private Array<BaseSector> exploreSectors;
    private ResourceManager manager;

    public StarMap(boolean aiCalculation, int aiRange, ResourceManager manager) {
        this.isAICalculation = aiCalculation;
        this.manager = manager;
        this.aiRange = aiRange;
        init();
    }

    private void init() {
        isAICalculation = true; // so that stuff is calculated even for the player, but not applied
        int size = manager.getGridSizeX() * manager.getGridSizeY();
        range = new int[size];
        Arrays.fill(range, RangeTypes.SM_RANGE_SPACE.getType());
        selection = new IntPoint(-1, -1);
        bases = new Array<IntPoint>(false, 16);

        rangeMap = new RangeMap();
        rangeMap.w = rangeMap.h = 7;
        rangeMap.x0 = rangeMap.y0 = 3;
        int rm[] = { 0, 0, 1, 1, 1, 0, 0,
                     0, 1, 2, 2, 2, 1, 0,
                     1, 2, 3, 3, 3, 2, 1,
                     1, 2, 3, 3, 3, 2, 1,
                     1, 2, 3, 3, 3, 2, 1,
                     0, 1, 2, 2, 2, 1, 0,
                     0, 0, 1, 1, 1, 0, 0 };
        rangeMap.range = rm;

        pathMap = new PathSector[size];
        for (int i = 0; i < size; i++)
            pathMap[i] = new PathSector();
        leaves = new Array<PathSector>(false, 16);
        pathStart = new IntPoint(-1, -1);
        pathRange = 0;
        aiTargets = new Array<IntPoint>();
        aiKnownSystems = new Array<IntPoint>();
        aiRangePoints = new int[size];
        Arrays.fill(aiRangePoints, 0);
        aiNeighbourCount = new int[size];
        Arrays.fill(aiNeighbourCount, 0);
        aiConnectionPoints = new int[size];
        Arrays.fill(aiConnectionPoints, 0);
        aiTargetPoints = new int[size];
        Arrays.fill(aiTargetPoints, 0);
        aiBadPoints = new int[size];
        Arrays.fill(aiBadPoints, 0);
        explorePoints = new int[size];
        Arrays.fill(explorePoints, 0);
        exploreTargets = new Array<BaseSector>(true, 16);
        exploreSectors = new Array<BaseSector>();
    }

    public int getRangeValue(IntPoint p) {
        return range[manager.coordsToIndex(p)];
    }

    public int getRange(IntPoint p) {
        return (3 - range[manager.coordsToIndex(p)]);
    }

    private int getRangeMapValue(int x, int y) {
        x += rangeMap.x0;
        y += rangeMap.y0;
        return rangeMap.range[y * rangeMap.w + x];
    }

    public void select(IntPoint sector) {
        if (sector.x > -1 && sector.y > -1)
            selection = sector;
    }

    public void deSelect() {
        selection.x = selection.y = -1;
    }

    /**
     * returns if a base exists at the specified coordinates
     * @param sector
     * @return
     */
    public boolean hasBase(IntPoint sector) {
        for (IntPoint p : bases)
            if (sector.equals(p))
                return true;
        return false;
    }

    public void addBase(IntPoint sector, int propTech) {
        if (!hasBase(sector))
            bases.add(sector);

        // calculate rangemap
        this.rangeMap = RangeMaps.calcRangeMap(propTech, manager.getGamePreferences().expansionSpeed);

        // iterate rangemap
        for (int y = -rangeMap.y0; y < rangeMap.h - rangeMap.y0; y++)
            for (int x = -rangeMap.x0; x < rangeMap.w - rangeMap.x0; x++) {
                IntPoint pt = new IntPoint(sector.x + x, sector.y + y);
                if (pt.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY())) {
                    //overwrite if the new influence is higher
                    range[manager.coordsToIndex(pt)] = Math.max(range[manager.coordsToIndex(pt)], getRangeMapValue(x, y));
                }
            }

        // reset pathStart so that all routes are recalculated when calcPath() is called
        pathStart = new IntPoint(-1, -1);
    }

    public void setFullRangeMap() {
        setFullRangeMap(RangeTypes.SM_RANGE_NEAR.getType(), new Array<IntPoint>());
    }

    public void setFullRangeMap(int nRange, Array<IntPoint> exceptions) {
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if ((exceptions.size == 0) || (exceptions.contains(new IntPoint(x, y), false) == false))
                    range[manager.coordsToIndex(x, y)] = nRange;
    }

    /**
     * Synchronizes the rangemap with the galaxy map with races with whom we have NAP(non aggression pact) and these are removed from the rangemap
     * @param systems list of all sectors
     * @param races list of races with whom we have NAP
     */
    public void synchronizeWithMap(Array<StarSystem> systems, ObjectSet<String> races) {
        for (StarSystem s : systems) {
            IntPoint co = s.getCoordinates();
            if ((range[manager.coordsToIndex(co)] > 0) && races.contains(s.getOwnerId()))
                range[manager.coordsToIndex(co)] = 0;
        }
    }

    /**
     * Calculates the intest path from position to target inside the given range range, which is stored inside the path Array.
     * Return value is the next point which should be reached and the new Path.If there is a problem (for example the target is not reachable),
     * the function returns (-1, -1)
     * @param pos
     * @param target
     * @param range
     * @param speed
     * @return
     */
    public Pair<IntPoint, Array<IntPoint>> calcPath(IntPoint pos, IntPoint target, int range, int speed) {
        Array<IntPoint> path = new Array<IntPoint>();
        // weights for distance, the diagonal weight should be higher else it's possible that
        // the path goes in zig-zag instead of in a straight line
        final double WEIGHT_DIAG = 1.4143;
        final double WEIGHT_DIR = 1.0;
        if (pos.equals(target) || range < 1 || range > 3 || this.range[manager.coordsToIndex(pos)] < range // start outside the range
                || this.range[manager.coordsToIndex(target)] < range //targe outside of range
                || speed < 1) {
            return new Pair<IntPoint, Array<IntPoint>>(new IntPoint(-1, -1), path);
        }
        IntPoint[] neighbours = { new IntPoint(-1, -1), new IntPoint(0, -1), new IntPoint(1, -1), new IntPoint(1, 0),
                new IntPoint(1, 1), new IntPoint(0, 1), new IntPoint(-1, 1), new IntPoint(-1, 0) };

        // start anew calculations?
        if (!pos.equals(pathStart) || range != pathRange) {
            for (int j = 0; j < manager.getGridSizeY(); j++)
                for (int i = 0; i < manager.getGridSizeX(); i++) {
                    int index = manager.coordsToIndex(i, j);
                    pathMap[index].used = false;
                    pathMap[index].distance = 0;
                    pathMap[index].hops = 0;
                    pathMap[index].parent.x = -1;
                    pathMap[index].parent.y = -1;
                    pathMap[index].position.x = i;
                    pathMap[index].position.y = j;
                }

            leaves.clear();
            //add start node to list of leaves
            leaves.add(pathMap[manager.coordsToIndex(pos)]);
            //save parameters
            pathStart = pos;
            pathRange = range;
        }

        // is the route to the target known?
        if ((pathMap[manager.coordsToIndex(target)].parent.x == -1) || (pathMap[manager.coordsToIndex(target)].parent.y == -1)) {
            boolean found = false;
            while (!found) {
                if (leaves.size == 0) {  // no more nodes, target can't be reached
                    return new Pair<IntPoint, Array<IntPoint>>(new IntPoint(-1, -1), path);
                }
                PathSector next = leaves.removeIndex(0);
                leaves.sort();
                if (next.used)
                    continue; // node was already selected

                next.used = true;

                // the so far unused neighbors in range are added to the leaves
                // the neighbors have to be introduced even if next coincides with the target
                // because the next call of calcPath() will use the intermediary results
                for (int i = 0; i < 8; i++) {
                    // establish the coordinates of the
                    IntPoint npos = new IntPoint(next.position);
                    npos.add(neighbours[i]);
                    if (!npos.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY()))
                        continue;
                    // consider only neighbors which were not selected and are in range
                    PathSector neighb = pathMap[manager.coordsToIndex(npos)];
                    if (neighb.used || this.range[manager.coordsToIndex(npos)] < range)
                        continue;

                    // if the neighbor can be reached via a inter route as before, overrite
                    double distance = next.distance + ((i % 2) == 1 ? WEIGHT_DIR : WEIGHT_DIAG);
                    // take into account anomalies
                    double[] badMapModifiers = manager.getBadMapModifiers();
                    distance += badMapModifiers[manager.coordsToIndex(next.position)];
                    if ((int) neighb.distance == 0 || distance < neighb.distance) {
                        neighb.distance = distance;
                        neighb.hops = next.hops + 1;
                        neighb.parent = new IntPoint(next.position);

                        if (leaves.contains(neighb, false)) // if a different value exists for the same position, we remove it first
                            leaves.removeValue(neighb, false);
                        leaves.add(neighb);
                        leaves.sort();
                    }
                }
                if (next.position.equals(target))
                    found = true;
            }
        }

        // found target, calculate the way back from target back to the start
        IntPoint next = new IntPoint(target);
        int idx = pathMap[manager.coordsToIndex(target)].hops;
        for (int i = 0; i < idx; i++)
            path.add(new IntPoint());
        while (next.x > -1 && next.y > -1 && --idx >= 0) { // start sector should not be in path
            path.set(idx, next);
            next = new IntPoint(pathMap[manager.coordsToIndex(next)].parent);
        }
        return new Pair<IntPoint, Array<IntPoint>>(path.get(Math.min(speed - 1, path.size - 1)), path);
    }

    /**
     * Adds a new target into the preferred directions. It is used to calculate where
     * an outpost should be built in order to get nearer to a target
     * @param target
     */
    public void addTarget(IntPoint target) {
        if (!isAICalculation || !target.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY()))
            return;
        //check if the list already
        if (aiTargets.contains(target, false))
            return;
        aiTargets.add(target);
    }

    /**
     * Returns true if the sector at target was already added as a destination
     * @param target
     * @return
     */
    public boolean isTarget(IntPoint target) {
        if (aiTargets.contains(target, false))
            return true;
        return false;
    }

    /**
     * Adds a sector to the sectors known by the AI. They will be preferred for expansion but less aggressively than
     * target systems if they are in range
     * @param target
     */
    public void addKnownSystem(IntPoint target) {
        if (!isAICalculation || !target.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY()))
            return;
        if (aiKnownSystems.contains(target, false))
            return;
        aiKnownSystems.add(target);
    }

    public boolean isKnownSystem(IntPoint sector) {
        if (aiKnownSystems.contains(sector, false))
            return true;
        return false;
    }

    /**
     * Function calculates a matrix which should be used for calculating the best position
     * for an outpost.
     * @param systems list of the starsystems in the galaxy
     * @param race which has the StarMap object
     */
    public void setBadAIBaseSectors(Array<StarSystem> systems, String race) {
        Arrays.fill(aiBadPoints, 0);

        for (StarSystem sector : systems) {
            IntPoint coord = sector.getCoordinates();
            String owner = sector.getOwnerId();
            if (range[manager.coordsToIndex(coord)] >= aiRange) {
                double value = 0.0;
                if (owner.equals(race) || sector.isFree()) {
                    int number = 0;
                    // scan around sector
                    for (int j = -1; j <= 1; j++)
                        for (int i = -1; i <= 1; i++)
                            if (coord.y + j > -1 && coord.y + j < manager.getGridSizeY() && coord.x + i > -1
                                    && coord.x + i < manager.getGridSizeX())
                                if (!systems.get(manager.coordsToIndex(coord.x + i, coord.y + j)).getOwnerId().equals(race)
                                        && !systems.get(manager.coordsToIndex(coord.x + i, coord.y + j)).isFree())
                                    number++;
                    value += (50 * number);
                } else
                    value += 1000;
                if (sector.getAnomaly() != null)
                    value += sector.getAnomaly().getWaySearchWeight() * 100.0;

                if (aiBadPoints[manager.coordsToIndex(coord)] + value > Integer.MAX_VALUE)
                    aiBadPoints[manager.coordsToIndex(coord)] = Integer.MAX_VALUE;
                else
                    aiBadPoints[manager.coordsToIndex(coord)] += value;
            }
        }
    }

    /**
     * Gets the score of the sector, recalcRangePoints(); recalcConnectionPoints(); recalcTargetPoints(); should be called before this!
     * @param sector
     * @return
     */
    public int getPoints(IntPoint sector) {
        int points = aiRangePoints[manager.coordsToIndex(sector)];
        if (points > 0) {
            // connecting territory and preferred expansion direction are important only where it has advantages
            points += aiConnectionPoints[manager.coordsToIndex(sector)] + aiTargetPoints[manager.coordsToIndex(sector)];
        }
        points -= aiBadPoints[manager.coordsToIndex(sector)];
        return points;
    }

    public BaseSector calcAIBaseSector() {
        return calcAIBaseSector(0.1);
    }

    /**
     * Calculates a sector in which an outpost is worthwhile
     * @param variance maximum variance from the best score; in interval [0, 1] - 0 means no variance, 1 means 100% is possible
     * @return a BaseSector object which in case if nowhere an outpost can be built has the position value (-1, -1) and the points also -1
     */
    public BaseSector calcAIBaseSector(double variance) {
        if (!isAICalculation)
            return new BaseSector();
        variance = Math.min(Math.max(variance, 0), 1);

        recalcRangePoints();
        recalcConnectionPoints();
        recalcTargetPoints();

        Array<BaseSector> sectors = new Array<BaseSector>();
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (range[manager.coordsToIndex(x, y)] >= aiRange) {
                    BaseSector sector = new BaseSector();
                    sector.position = new IntPoint(x, y);
                    sector.points = getPoints(sector.position);
                    sectors.add(sector);
                }
        if (sectors.size == 0)
            return new BaseSector();

        //sort in order of score
        sectors.sort();
        // calculate the steps of scoring which are inside the best (100 * variance)%
        int count = 0;
        int max_points = -1, old_points = -1;
        for (BaseSector b : sectors) {
            if (max_points == -1)
                max_points = b.points;
            if ((double) (max_points - b.points) / max_points > variance)
                break;
            if (b.points != old_points) {
                old_points = b.points;
                count += b.points;
            }
        }

        int n = (int) (RandUtil.random() * count);
        old_points = -1;
        for (BaseSector b : sectors) {
            if (b.points != old_points) {
                old_points = b.points;
                n -= b.points;
                if (n < 0) {
                    n = b.points;
                    break;
                }
            }
        }

        // count the entries
        count = 0;
        for (BaseSector b : sectors) {
            if (b.points == n)
                count++;
            else if (b.points < n)
                break;
        }

        // pick one by chance
        int m = (int) (RandUtil.random() * count);
        for (BaseSector b : sectors) {
            if (b.points == n) {
                if (m-- == 0)
                    return b;
            } else if (b.points < n)
                break;
        }

        return new BaseSector();
    }

    private static boolean checkCoords(int x, int y, String ownedBy, ResourceManager manager) {
        return checkCoords(new IntPoint(x, y), ownedBy, manager);
    }

    private static boolean checkCoords(IntPoint p, String ownedBy, ResourceManager manager) {
        if (!p.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY()))
            return false;
        StarSystem system = manager.getUniverseMap().getStarSystems().get(manager.coordsToIndex(p));
        return system.getOwnerId().equals(ownedBy) && system.getShipPort(ownedBy);
    }

    /**
     * Returns the nearest ship port to start owned by passed race
     * @param manager
     * @param start
     * @param ownedBy
     * @return
     */
    public static IntPoint getNearestPort(ResourceManager manager, IntPoint start, String ownedBy) {
        if (checkCoords(start, ownedBy, manager))
            return start;
        for (int radius = 1; radius < Math.max(manager.getGridSizeY(), manager.getGridSizeX()); ++radius) {
            int from_x = start.x - radius;
            int to_x = start.x + radius;
            int from_y = start.y - radius;
            int to_y = start.y + radius;

            int x = from_x;
            int y = from_y;
            for (; x < to_x; ++x)
                if (checkCoords(x, y, ownedBy, manager))
                    return new IntPoint(x, y);

            for (; y < to_y; ++y)
                if (checkCoords(x, y, ownedBy, manager))
                    return new IntPoint(x, y);

            for (; x > from_x; --x)
                if (checkCoords(x, y, ownedBy, manager))
                    return new IntPoint(x, y);

            for (; y > from_y; --y)
                if (checkCoords(x, y, ownedBy, manager))
                    return new IntPoint(x, y);
        }
        return new IntPoint(0, 0);
    }

    private void recalcRangePoints() {
        Arrays.fill(aiRangePoints, 0);

        // iterates starmap, but for efficiency take into account only sectors which are in range
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (range[manager.coordsToIndex(x, y)] >= aiRange) {
                    // for sectors inside the range calculate a new score which would result if an outpost would be built
                    aiRangePoints[manager.coordsToIndex(x, y)] = 0;
                    // iterate local rangemap
                    for (int my = -rangeMap.y0; my < rangeMap.h - rangeMap.y0; my++)
                        for (int mx = -rangeMap.x0; mx < rangeMap.w - rangeMap.x0; mx++) {
                            IntPoint mpt = new IntPoint(x + mx, y + my);
                            if (mpt.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY())) {
                                // calculate territory growth
                                aiRangePoints[manager.coordsToIndex(x, y)] += Math.max(
                                        getRangeMapValue(mx, my) - range[manager.coordsToIndex(mpt)], 0);
                            }
                        }
                }
    }

    private void recalcConnectionPoints() {
        Arrays.fill(aiNeighbourCount, 0);
        Arrays.fill(aiConnectionPoints, 0);

        //for the sectors which are outside of the range calculate the number of neighbours inside the range
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++) {
                if (range[manager.coordsToIndex(x, y)] >= aiRange) {
                    for (int nx = -1; nx <= 1; nx++)
                        for (int ny = -1; ny <= 1; ny++) {
                            IntPoint npt = new IntPoint(x + nx, y + ny);
                            if (npt.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY())
                                    && range[manager.coordsToIndex(npt)] < aiRange) {
                                aiNeighbourCount[manager.coordsToIndex(npt)]++;
                            }
                        }
                }
            }

        // recalculate the connections
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++) {
                if (range[manager.coordsToIndex(x, y)] >= aiRange) {
                    // iterate local rangemap
                    for (int my = -rangeMap.y0; my < rangeMap.h - rangeMap.y0; my++)
                        for (int mx = -rangeMap.x0; mx < rangeMap.w - rangeMap.x0; mx++) {
                            IntPoint mpt = new IntPoint(x + mx, y + my);
                            if (mpt.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY())) {
                                // for the sectors around (x,y) take into account the number of neighbors inside range: if this number is high
                                // then in the vicinity of (x,y) there is an another territory inside range which isn't directly connected
                                // directly to (x,y)
                                aiConnectionPoints[manager.coordsToIndex(mpt)] += aiNeighbourCount[manager.coordsToIndex(mpt)]
                                        * getRangeMapValue(mx, my);
                            }
                        }
                }
            }
    }

    private void recalcTargetPoints() {
        final int NEAR_TARGET_DISTANCE = 5; // distance which is considered near for a target
        final int NEAR_SYSTEM_DISTANCE = 3; // distance which is considered near for a known system

        Arrays.fill(aiTargetPoints, 0);

        // stop if there are no targets or no known systems
        if (aiTargets.size == 0 && aiKnownSystems.size == 0)
            return;

        // recalculate the connections
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (range[manager.coordsToIndex(x, y)] >= aiRange) {
                    for (IntPoint sector : aiTargets) {
                        // skip target if it's not inside the given range
                        if (range[manager.coordsToIndex(sector)] >= aiRange)
                            continue;

                        // count how many sectors are visited inside the range if from
                        // sector at IntPoint(x, y) to (tx,y) horizontally and to (x, ty) horizontally
                        // where (tx, ty) are the target's coordinates

                        boolean intersect = false;
                        int incx = (int) Math.signum(sector.x - x);
                        int px = x + incx;
                        while (incx != 0) {
                            if (range[manager.coordsToIndex(px, y)] >= aiRange) {
                                intersect = true;
                                break;
                            }
                            if (px == sector.x)
                                break;
                            px += incx;
                        }

                        int incy = (int) Math.signum(sector.y - y);
                        int py = y + incy;
                        while (incy != 0 && !intersect) {
                            if (range[manager.coordsToIndex(x, py)] >= aiRange) {
                                intersect = true;
                                break;
                            }
                            if (py == sector.y)
                                break;
                            py += incy;
                        }

                        // bonus if no sectors were visited inside the range
                        if (!intersect)
                            aiTargetPoints[manager.coordsToIndex(px, py)] += 20;

                        // calculate distance to target (because (x,y) is inside the range but the target is outside,
                        // the distance is >= 1
                        int distance = Math.max(Math.abs(sector.x - x), Math.abs(sector.y - y));
                        if (distance <= NEAR_TARGET_DISTANCE)
                            aiTargetPoints[manager.coordsToIndex(x, y)] += (NEAR_TARGET_DISTANCE - distance + 1) * 5;
                    }

                    // verify the known systems
                    for (IntPoint sector : aiKnownSystems) {
                        if (range[manager.coordsToIndex(sector)] >= aiRange)
                            continue;

                        // bonus if the build of an outpost in system (x, y) would include the system inside the given range
                        IntPoint system = new IntPoint(sector.x - x, sector.y - y);
                        if (system.inRect(-rangeMap.x0, -rangeMap.y0, rangeMap.w - rangeMap.x0, rangeMap.h - rangeMap.y0)
                                && getRangeMapValue(system.x, system.y) >= aiRange) {
                            aiTargetPoints[manager.coordsToIndex(x, y)] += 30;
                        } else { // else it depends on the distance to the system
                            int distance = Math.max(Math.abs(sector.x - x), Math.abs(sector.y - y));
                            if (distance <= NEAR_SYSTEM_DISTANCE)
                                aiTargetPoints[manager.coordsToIndex(x, y)] += (NEAR_SYSTEM_DISTANCE - distance + 1) * 5;
                        }
                    }
                }
    }

    private void calcExplorePoints(String raceID) {
        Arrays.fill(explorePoints, 0);

        if (exploreTargets.size == 0)
            return;

        for (int i = 0; i < exploreTargets.size; i++) {
            BaseSector bs = exploreTargets.get(i);
            if (bs.points == 0)
                continue;
            IntPoint sector = bs.position;
            if (range[manager.coordsToIndex(sector)] < 1)
                continue;
            int x = sector.x;
            int y = sector.y;

            StarSystem ss = manager.getUniverseMap().getStarSystemAt(sector);
            //max value is 3 * 2 + 3 = 9 if scanned starsytem
            explorePoints[manager.coordsToIndex(x, y)] = 2 * range[manager.coordsToIndex(sector)] + 1;
            if (ss.getScanned(raceID) && !ss.isSunSystem())
                if (range[manager.coordsToIndex(sector)] > 1)
                    explorePoints[manager.coordsToIndex(x, y)] = 0; //no need to explore, not a fringe world
                else
                    explorePoints[manager.coordsToIndex(x, y)]--; //lower priority
            if (ss.getScanned(raceID) && ss.isSunSystem())
                explorePoints[manager.coordsToIndex(x, y)] += 2;
            if (ss.getScanned(raceID) && ss.getAnomaly() != null)
                explorePoints[manager.coordsToIndex(x, y)] -= ss.getAnomaly().getWaySearchWeight(); //avoid dangerous anomalies
            if (!ss.getOwnerId().isEmpty() && !ss.getOwnerId().equals(raceID) && ss.getOwner() != null && ss.getOwner().isRaceContacted(raceID))
                explorePoints[manager.coordsToIndex(x, y)] -= 2 * range[manager.coordsToIndex(sector)]; //lower priority
            for (int j = 0; j < ss.getShipsInSector().size; j++) {
                ShipMap sm = ss.getShipsInSector().getValueAt(j);
                for (int k = 0; k < sm.getSize(); k++) {
                    Ships ships = sm.getAt(k);
                    Race owner = ships.getOwner();
                    if (owner != null) {
                        if (!owner.isRaceContacted(raceID))
                            explorePoints[manager.coordsToIndex(x, y)] = 0;
                        else if (owner.getAgreement(raceID).getType() < DiplomaticAgreement.NAP.getType())
                            explorePoints[manager.coordsToIndex(x, y)] = 0;
                    }
                }
            }
        }
    }

    public void addExploreTarget(IntPoint target) {
        if (!target.inRect(0, 0, manager.getGridSizeX(), manager.getGridSizeY()))
            return;
        BaseSector bs = new BaseSector();
        bs.position = target;
        bs.points = 1;
        if (getExploreTarget(target).position.equals(new IntPoint())) {
            exploreTargets.add(bs);
        }
    }

    public BaseSector getExploreTarget(IntPoint target) {
        BaseSector bs = new BaseSector();
        for (int i = 0; i < exploreTargets.size; i++) {
            BaseSector other = exploreTargets.get(i);
            if (other.position.equals(target))
                return other;
        }
        return bs;
    }

    public void calcExploreSectors(String raceID) {
        calcExplorePoints(raceID);

        exploreSectors.clear();
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (range[manager.coordsToIndex(x, y)] > RangeTypes.SM_RANGE_SPACE.type
                        && explorePoints[manager.coordsToIndex(x, y)] > 0) {
                    BaseSector sector = new BaseSector();
                    sector.position = new IntPoint(x, y);
                    sector.points = explorePoints[manager.coordsToIndex(x, y)];
                    exploreSectors.add(sector);
                }
        exploreSectors.sort();
    }

    public void removeExploreSector(IntPoint target) {
        getExploreTarget(target).points = 0;
    }

    public IntPoint popClosestExploreSector(Ship ship) {
        int minIdx = -1;
        int minDist = Integer.MAX_VALUE;
        IntPoint sector = new IntPoint();
        for (int i = 0; i < exploreSectors.size; i++) {
            BaseSector bs = exploreSectors.get(i);
            IntPoint p = bs.position;
            int dist = ship.getCoordinates().getDist(p) - bs.points;

            if (dist < minDist) {
                minIdx = i;
                minDist = dist;
            }
        }
        if (minIdx != -1) {
            //found a target
            sector = new IntPoint(exploreSectors.removeIndex(minIdx).position);
        }
        return sector;
    }
}