/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.*;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.utils.RandUtil;

public class GenDiploMessage {
    private static Array<String> getSearchText(Race race) {
        Array<String> searchText = new Array<String>();
        if (race.isRaceProperty(RaceProperty.FINANCIAL))
            searchText.add("FINANCIAL:");
        if (race.isRaceProperty(RaceProperty.WARLIKE))
            searchText.add("WARLIKE:");
        if (race.isRaceProperty(RaceProperty.AGRARIAN))
            searchText.add("FARMER:");
        if (race.isRaceProperty(RaceProperty.INDUSTRIAL))
            searchText.add("INDUSTRIAL:");
        if (race.isRaceProperty(RaceProperty.SECRET))
            searchText.add("SECRET:");
        if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
            searchText.add("RESEARCHER:");
        if (race.isRaceProperty(RaceProperty.PRODUCER))
            searchText.add("PRODUCER:");
        if (race.isRaceProperty(RaceProperty.PACIFIST))
            searchText.add("PACIFIST:");
        if (race.isRaceProperty(RaceProperty.SNEAKY))
            searchText.add("SNEAKY:");
        if (race.isRaceProperty(RaceProperty.SOLOING))
            searchText.add("SOLOING:");
        if (race.isRaceProperty(RaceProperty.HOSTILE))
            searchText.add("HOSTILE:");
        if (searchText.size == 0)
            searchText.add("NOTHING_SPECIAL:");
        return searchText;
    }

    /**
     * Generates an offer text for a minor race
     * @param manager
     * @param info
     * @return
     */
    public static void generateMinorOffer(ResourceManager manager, DiplomacyInfo info) {
        Race race = manager.getRaceController().getRace(info.fromRace);
        Array<String> searchText = getSearchText(race);
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix()
                + "/races/minorsdiplooffers.txt");
        String search = String.format("%s:", info.fromRace);
        search = search.toUpperCase();
        String search2 = searchText.get((int) (RandUtil.random() * searchText.size));

        int infoCount = 6;
        String read[] = new String[infoCount];
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.equals(search) || line.equals(search2)) {
                    for (int i = 0; i < infoCount; i++) {
                        read[i] = input[startPos++].trim();
                    }
                    break;
                }
            }
        }

        String offerText = "";
        String headLine = "";
        switch (info.type) {
            case TRADE:
                headLine = StringDB.getString("TRADE_AGREEMENT_OFFER");
                offerText = read[0];
                break;
            case FRIENDSHIP:
                headLine = StringDB.getString("FRIENDSHIP_OFFER");
                offerText = read[1];
                break;
            case COOPERATION:
                headLine = StringDB.getString("COOPERATION_OFFER");
                offerText = read[2];
                break;
            case ALLIANCE:
                headLine = StringDB.getString("ALLIANCE_OFFER");
                offerText = read[3];
                break;
            case MEMBERSHIP:
                headLine = StringDB.getString("MEMBERSHIP_OFFER");
                offerText = read[4];
                break;
            case WAR:
                headLine = StringDB.getString("WAR_OFFER");
                offerText = read[5];
                break;
            default:
                break;
        }
        info.headLine = headLine;
        info.text = offerText;
    }

    public static void generateMinorAnswer(ResourceManager manager, DiplomacyInfo info) {
        Race race = manager.getRaceController().getRace(info.toRace);
        Array<String> searchText = getSearchText(race);
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix()
                + "/races/minorsdiploanswers.txt");
        String search = String.format("%s:", info.toRace);
        search = search.toUpperCase();
        String search2 = searchText.get((int) (RandUtil.random() * searchText.size));

        int infoCount = 13;
        String read[] = new String[infoCount];
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.equals(search) || line.equals(search2)) {
                    for (int i = 0; i < infoCount; i++) {
                        read[i] = input[startPos++].trim();
                    }
                    break;
                }
            }
        }

        String answer = "";
        switch (info.type) {
            case TRADE:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[0];
                    info.headLine = StringDB.getString("ACCEPT_TRADE_AGREEMENT");
                } else {
                    answer = read[6];
                    info.headLine = StringDB.getString("DECLINE_TRADE_AGREEMENT");
                }
                break;
            case FRIENDSHIP:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[1];
                    info.headLine = StringDB.getString("ACCEPT_FRIENDSHIP");
                } else {
                    answer = read[7];
                    info.headLine = StringDB.getString("DECLINE_FRIENDSHIP");
                }
                break;
            case COOPERATION:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[2];
                    info.headLine = StringDB.getString("ACCEPT_COOPERATION");
                } else {
                    answer = read[8];
                    info.headLine = StringDB.getString("DECLINE_COOPERATION");
                }
                break;
            case ALLIANCE:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[3];
                    info.headLine = StringDB.getString("ACCEPT_ALLIANCE");
                } else {
                    answer = read[9];
                    info.headLine = StringDB.getString("DECLINE_ALLIANCE");
                }
                break;
            case MEMBERSHIP:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[4];
                    info.headLine = StringDB.getString("ACCEPT_MEMBERSHIP");
                } else {
                    answer = read[10];
                    info.headLine = StringDB.getString("DECLINE_MEMBERSHIP");
                }
                break;
            case CORRUPTION:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[5];
                    info.headLine = StringDB.getString("ACCEPT_CORRUPTION");
                } else {
                    answer = read[11];
                    info.headLine = StringDB.getString("DECLINE_CORRUPTION");
                }
                break;
            case WAR:
                answer = read[12];
                info.headLine = StringDB.getString("WAR_OFFER");
                break;
            default:
                break;
        }

        info.flag = DiplomacyInfoType.DIPLOMACY_ANSWER.getType();
        info.text = answer;
        //switch sender/receiver
        info.toRace = info.fromRace;
        info.fromRace = race.getRaceId();
    }

    /**
     * This function generates an answer on a diplomatic offer to a major race
     * @param info
     * @return
     */
    public static void generateMajorAnswer(DiplomacyInfo info) {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix()
                + "/races/majorsdiploanswers.txt");

        String search = String.format("%s:", info.toRace).toUpperCase();
        int infoCount = 17;
        String read[] = new String[infoCount];
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.equals(search)) {
                    for (int i = 0; i < infoCount; i++) {
                        read[i] = input[startPos++].trim();
                    }
                    break;
                }
            }
        }

        String answer = "";
        switch (info.type) {
            case TRADE:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[0];
                    info.headLine = StringDB.getString("ACCEPT_TRADE_AGREEMENT");
                } else {
                    answer = read[8];
                    info.headLine = StringDB.getString("DECLINE_TRADE_AGREEMENT");
                }
                break;
            case FRIENDSHIP:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[1];
                    info.headLine = StringDB.getString("ACCEPT_FRIENDSHIP");
                } else {
                    answer = read[9];
                    info.headLine = StringDB.getString("DECLINE_FRIENDSHIP");
                }
                break;
            case COOPERATION:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[2];
                    info.headLine = StringDB.getString("ACCEPT_COOPERATION");
                } else {
                    answer = read[10];
                    info.headLine = StringDB.getString("DECLINE_COOPERATION");
                }
                break;
            case ALLIANCE:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[3];
                    info.headLine = StringDB.getString("ACCEPT_ALLIANCE");
                } else {
                    answer = read[11];
                    info.headLine = StringDB.getString("DECLINE_ALLIANCE");
                }
                break;
            case NAP:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[4];
                    info.headLine = StringDB.getString("ACCEPT_NON_AGGRESSION");
                } else {
                    answer = read[12];
                    info.headLine = StringDB.getString("DECLINE_NON_AGGRESSION");
                }
                break;
            case DEFENCEPACT:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[5];
                    info.headLine = StringDB.getString("ACCEPT_DEFENCE_PACT");
                } else {
                    answer = read[13];
                    info.headLine = StringDB.getString("DECLINE_DEFENCE_PACT");
                }
                break;
            case WARPACT:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[6];
                    info.headLine = StringDB.getString("ACCEPT_WARPACT");
                } else {
                    answer = read[14];
                    info.headLine = StringDB.getString("DECLINE_WARPACT");
                }
                break;
            case REQUEST:
                if (info.answerStatus == AnswerStatus.ACCEPTED) {
                    answer = read[7];
                    info.headLine = StringDB.getString("ACCEPT_REQUEST");
                } else {
                    answer = read[15];
                    info.headLine = StringDB.getString("DECLINE_REQUEST");
                }
                break;
            case WAR:
                answer = read[16];
                info.headLine = StringDB.getString("WAR_OFFER");
                break;
            default:
                break;
        }

        info.flag = DiplomacyInfoType.DIPLOMACY_ANSWER.getType();
        info.text = answer;
        //switch from and to
        String oldToRace = info.toRace;
        info.toRace = info.fromRace;
        info.fromRace = oldToRace;
    }

    public static void generateMajorOffer(ResourceManager manager, DiplomacyInfo info) {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix()
                + "/races/majorsdiplooffers.txt");

        String search = String.format("%s:", info.fromRace).toUpperCase();
        int infoCount = 12;
        String read[] = new String[infoCount];
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.equals(search)) {
                    for (int i = 0; i < infoCount; i++) {
                        read[i] = input[startPos++].trim();
                    }
                    break;
                }
            }
        }

        String offerText = "";
        String headLine = "";
        switch (info.type) {
            case TRADE:
                headLine = StringDB.getString("TRADE_AGREEMENT_OFFER");
                offerText = read[0];
                break;
            case FRIENDSHIP:
                headLine = StringDB.getString("FRIENDSHIP_OFFER");
                offerText = read[1];
                break;
            case COOPERATION:
                headLine = StringDB.getString("COOPERATION_OFFER");
                offerText = read[2];
                break;
            case ALLIANCE:
                headLine = StringDB.getString("ALLIANCE_OFFER");
                offerText = read[3];
                break;
            case MEMBERSHIP:
                headLine = StringDB.getString("MEMBERSHIP_OFFER");
                offerText = read[4];
                break;
            case NAP:
                headLine = StringDB.getString("NON_AGGRESSION_OFFER");
                offerText = read[5];
                break;
            case DEFENCEPACT:
                headLine = StringDB.getString("DEFENCE_PACT_OFFER");
                offerText = read[6];
                break;
            case WARPACT:
                headLine = StringDB.getString("WAR_PACT_OFFER");
                offerText = read[7];
                Major major = Major.toMajor(manager.getRaceController().getRace(info.warpactEnemy));
                if (major != null)
                    offerText = offerText.replace("$enemy$", major.getEmpireNameWithArticle());
                break;
            case WAR:
                headLine = StringDB.getString("WAR_OFFER");
                offerText = read[8];
                break;
            case CORRUPTION:
                headLine = StringDB.getString("CORRUPTION");
                if (!info.corruptedRace.isEmpty())
                    offerText = read[9];
                offerText = offerText.replace("$credits$", "" + info.credits);
                break;
            case REQUEST:
                headLine = StringDB.getString("REQUEST");
                offerText = read[10];
                String resStr = "";
                for (int res = ResourceTypes.TITAN.getType(); res < ResourceTypes.DERITIUM.getType(); res++) {
                    ResourceTypes type = ResourceTypes.fromResourceTypes(res);
                    if (info.resources[res] > 0) {
                        String s = "" + info.resources[res];
                        resStr = StringDB.getString(type.getName() + "_REQUEST", false, s);
                        break;
                    }
                }
                offerText = offerText.replace("$ressource$", resStr);
                offerText = offerText.replace("$credits$", "" + info.credits);
                break;
            case PRESENT:
                headLine = StringDB.getString("PRESENT");
                offerText = read[11];
                offerText = offerText.replace("$credits$", "" + info.credits);
                break;
            default:
                break;
        }

        if (info.credits > 0 && info.type != DiplomaticAgreement.PRESENT && info.type != DiplomaticAgreement.REQUEST
                && info.type != DiplomaticAgreement.CORRUPTION) {
            offerText = StringDB.getString("EXTRA_CREDITS", false, offerText, "" + info.credits);
        }
        if (info.type != DiplomaticAgreement.REQUEST) {
            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                ResourceTypes type = ResourceTypes.fromResourceTypes(res);
                if (info.resources[res] > 0) {
                    offerText = StringDB.getString("EXTRA_" + type.getName(), false, offerText, ""
                            + info.resources[res]);
                    break;
                }
            }
        }
        if (info.type != DiplomaticAgreement.CORRUPTION && info.type != DiplomaticAgreement.PRESENT
                && info.type != DiplomaticAgreement.REQUEST && info.type != DiplomaticAgreement.WAR
                && info.type != DiplomaticAgreement.WARPACT) {
            if (info.duration == 0)
                offerText = StringDB.getString("UNLIMITED_CONTRACT_DURATION", false, offerText);
            else
                offerText = StringDB.getString("LIMITED_CONTRACT_DURATION", false, offerText, "" + info.duration);
        }
        info.headLine = headLine;
        info.text = offerText;
    }
}
