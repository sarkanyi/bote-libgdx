/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.badlogic.gdx.tests.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.blotunga.bote.utils.ui.GestureCameraController.GestureEvent;

public class OrthoCamController extends InputAdapter {
    final OrthographicCamera camera;
    final Vector3 curr = new Vector3();
    final Vector3 last = new Vector3(-1, -1, -1);
    final Vector3 delta = new Vector3();
    float minZoom;
    float maxZoom;
    private GestureEvent zoomEvent;
    private boolean enabled;

    public OrthoCamController(OrthographicCamera camera) {
        this(camera, (float) 1280.0f / Gdx.graphics.getWidth(), 12 * (float) 1280.0f / Gdx.graphics.getWidth());
    }

    public OrthoCamController(OrthographicCamera camera, float minZoom, float maxZoom) {
        this.camera = camera;
        this.minZoom = minZoom;
        this.maxZoom = maxZoom;
        zoomEvent = null;
        enabled = true;
    }

    public void setZoomHandler(GestureEvent handler) {
        zoomEvent = handler;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        if(enabled) {
            camera.unproject(curr.set(x, y, 0));
            if (!(last.x == -1 && last.y == -1 && last.z == -1)) {
                camera.unproject(delta.set(last.x, last.y, 0));
                delta.sub(curr);
                camera.position.add(delta.x, delta.y, 0);
            }
            last.set(x, y, 0);
        }
        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        if(enabled)
            last.set(-1, -1, -1);
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if(enabled) {
            if (amountY > 0 && camera.zoom > maxZoom)
                return true;
            else if (amountY < 0 && camera.zoom < minZoom)
                return true;
            camera.zoom += amountY / 5;
            if (zoomEvent != null)
                zoomEvent.zoomed();
        } else
            return false;

        return true;
    }
}