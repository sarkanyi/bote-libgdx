#0	Eliminer un Empire
		La chute de $param$ est c�l�br�e avec enthusiaste par beaucoup de citoyens de la Coalition
		La chute de $param$ a relev� les taux d'�changes � des niveaux records
		Les Guerriers ont c�l�br� la chute de $param$ avec du vin et des chants
		Une f�te Imp�riale comm�more la chute de $param$
		Tous les Cartares sont ravis de la chute de $param$
		La chute de $param$ a augment� la foi de nos citoyens au pouvoir et � la sagesse des Vi
#1	Gagner une bataille majeure
		Cette victoire a augment� l'enthusiasme des citoyens de la Coaltion
		Les investisseurs sont impressinn�s par le grand succ�s militaire
		Les Guerriers sont vivement inspir�s par la magnifique victoire
		Cette victoire magnifique est une nouvelle bien acceuillie par tous les Rotharians loyaux
		Les citoyens Cartare sont vivement inspir�s par la grande victoire
		Les Vi sont ravis par la progression du bataille
#2	Gagner une bataille significative
		La victoire est le principal sujet de d�bat dans la plupart des services d'actualit�s
		Les commissionnaires sont entrain d'acheter des actions de notre arm�e apr�s la victoire
		La victoire a renouvel� notre foi aux enseignements de Khaj'Avrom
		Le soutien populaire de notre arm�e a �t� renforc� par la victoire
		Le peuple est clairement excit� par notre victoire
		Les Vi sont satisfaits du d�nouement de la bataille
#3	Gagner une victoire mineure
		Le moral dans le champ de bataille a �t� quelque peu relev�s par la victoire
		La victoire a men� � une l�g�re hausse de la confiance des consommateurs
		Cette victoire a augment� le moral de nos guerriers
		Le peuple est r�joui par notre succ�s militaire
		Les citoyens Caratre sont un peu satisfaits d'entendre la victoire
		Les Vi ont �t� satisfaits par l'aboutissement de ce combat
#4.  Perdre une bataille majeure
		Les manifestations anti-guerres sont plus fr�quentes depuis la d�faite r�cente
		Les certificats militaires ont flamb�s en valeur suivant la d�faite au combat
		La d�faite au combat a soulev� des inqui�tudes profondes parmi nombre de nos guerriers
		Les citoyens ont publiquement codamn�s notre �chec � la bataille
		La d�faite dans la bataille a men� les citoyens � rritiquer ouvertement notre politique
		Les Vi sont furieux de cette d�faite
#5	Perdre une bataille significative
		Nos dirigeants militaires ont �t� ridiculis�s depuis la d�faite
		La d�faite dans le secteur $param$ a d�clench� une chuye de la confiance des consommateurs
		Les Guerriers r�clament que les morts dans le secteur $param$ soient veng�s
		Le d�sastre dans le secteur $param$ a �rod� le support populaire � l'arm�e
		Les ouvriers Cartares sont d�sabus�s par la d�faite
		Les Vi sont enrag� � cause de cette d�faite
#6	Perdre une bataille mineure
		Les citoyens de la Coalition sont l�g�rement contrari�s par notre d�faite
		La d�faite a soulev�e les inqui�tudes de beaucoup d'hommes d'affaire
		La d�faite dans la bataille a soulev�e des inqui�tudes parmi nos guerriers
		La d�faite dans le secteur $param$ a fait baiss� le moral de la population
		Les citoyens Cartares sont d��us d'entendre notre d�faite
		Les Vi sont contrari�s par le d�nouement de ce combat
#7	Perdre un vaisseau amiral
		Le peuple est choqu� de la perte de notre vaisseu amiral $param$
		Les financiers ont perdu des actifs � cause de la perte du vaisseau amiral $param$
		Les Guerriers refusent de croire � la perte de la fiert� de la flotte $param$
		Les citoyens sont indign�s de la perte du vaisseau amiral $param$
		Le peuple a �t� choqu� de la perte du vaisseau amiral $param$
		Les Vi ont pris la perte du vaisseau amiral $param$ avec col�re
#8	Perdre un avant-poste
		Le peuple est choqu� par la perte de cet avant-poste
		Les financiers ont perdu des actifs � cause de la perte de cet avant-poste
		Les guerriers refusent de croire � la perte de cet avant-poste
		Les citoyens sont indign�s par la perte de cet avant-poste
		Le peuple a �t� choqu� par la perte de cet avant-poste
		Les Vi ont pris la perte de cet avant-poste avec d�plaisir
#9	Perdre une Base Stellaire
		Le peuple est choqu� par la perte de cette Base Stellaire
		Les financiers ont perdu des actifs � cause de la perte de cette Base Stellaire
		Les guerriers refusent de croire � la perte de cette Base Stellaire
		Les citoyens sont indign�s par la perte de cette Base Stellaire
		Le peuple a �t� choqu� par la perte de cette Base Stellaire
		Les Vi ont pris la perte de cette Base Stellaire avec d�plaisir
#10	Signer un trait� d'adh�sion avec un Mineur
		Les $param$ ont �t� acceuilli dans la Coalition � bras ouverts
		Les investisseurs expectent de nouvelles opportunit�s d'affaires avec les $param$
		Les guerriers sont ravis que les  $param$ seront maintenant consid�r�s comme leurs fr�res
		Le peuple semble �tre heureux d'acceuillir les $param$ comme des membres de notre grande Empire
		Les $param$ ont �t� re�us chaleureusement pat nos citoyens
		Les Vi ont approuv� l'entr�e des $param$ dans le Dominion
#11  Capturer un syst�me
		La subjugation brutale du syst�me $param$ a d�clench� des protestations tr�s r�pandues
		Les investisseurs sont soulag�s d'entendre que le syst�me $param$ est maintenant un march� stable
		Nos guerriers se d�lectent � la gloire de notre victoire dans le syst�me $param$
		Les citoyens sont soulag�s qu'il n'y aura plus de troubles � $param$
		Les citoyens Cartares sont ravis d'entendre que le syst�me $param$ est sous contr�le
		Les Vi ont observ� la conqu�te du syst�me $param$ avec bienveillance
#12	Coloniser un syst�me
		Les citoyens � travers la coalition c�l�brent la colonisation de $param$
		Les investisseurs sont enthusiastes � propros des opportunit�s dans la nouvelle colonie $param$
		Le syst�me $param$ donnera � notre peuple un espace de soufflement tr�s longuement attendu
		Les citoyens sont contents d'entendre que le syst�me $param$ fait maintenant partie de notre Empire
		Les citoyens c�l�brent la colonisation de $param$
		Les Vi c�l�brent la fondation de notre nouvelle colonie en $param$
#13	Lib�rer un syst�me de Race Mineur
		Des c�l�brations sont planifi�es pour honorer les h�ros qui ont lib�r�s $param$
		Les financiers sont contents que la commerce peut �tre repris � $param$ 
		Les guerriers ne sont pas fiers de la lib�ration de $param$
		La lib�ration de $param$ a relev� le moral de beaucoup de nos citoyens concern�s
		Les citoyens ne sont pas ravis que $param$ a �t� lib�r�
		$param$ a �t� lib�r�: Les Vi ont des doutes � l'�gard de telles tactiques
#14	Lib�rer un ancien syst�me natif
		La lib�ration du syst�me $param$ a g�n�r� de nouveaux supports � la guerre
		La restauration du march� $param$ est une bonne nouvelle pour tous les exportateurs Heyoun
		Beaucoup de guerriers rejoindront l'Ordre Kha'rehc pour leurs actes � $param$
		Le sauvetage de $param$ est devenu un motif populaire de c�l�bration
		La lib�ration de $param$ a relev� le moral de tous Cartares loyaux
		La lib�ration de $param$ faiq plaisir au Vi
#15	 Perdre le syst�me natal en faveur des forces ext�rieures
		Les manifestants demandent une explication � notre incapacit� de prot�ger le syst�me Solaire
		La perte de Hanar a baiss� les taux de change � un niveau historique
		De nombreux guerriers ont choisi le suicide honorable au-dessus de la vie sans le monde natal
		Le peuple croit que la perte de Rotharius est un signe clair d'incomp�tence du gouvernement
		La perte de Cartar Prime a entra�n� une d�sillusion g�n�ralis�e et plusieurs suicides
		Perte de Vi Homeworld: Les Vi ne voient plus aucune perspective d'�tre ici dans le quadrant alpha
#16	Perdre un syst�me membre en faveur des forces ext�rieures
		Les citoyens sont scandalis�s par notre incapacit� � d�fendre correctement $param$
		La perte de $param$ de notre base de consommateurs est un d�sastre pour les exportateurs de Heyoun
		Chaque guerrier Khaoron est honteux de notre incapacit� � d�fendre le syst�me $param$
		De nombreux citoyens ont exprim� leur d�ception face � la perte du syst�me $param$
		Les citoyens de Cartare sont d�courag�s par la perte de $param$ aux forces ennemies
		Le Vi a refus� de croire � la perte de $param$
#17	 Perdre un syst�me soumis � des forces ext�rieures
		Le peuple n'est pas content de notre incapacit� � contr�ler $param$
		La confiance des consommateurs a l�g�rement baiss� depuis les rapports indiquant que nous avons perdu $param$
		En omettant de d�fendre $param$, nous avons perdu beaucoup d'honneur
		Le peuple est tr�s m�content de notre incapacit� � prot�ger $param$
		Les citoyens de Cartare sont d�courag�s par la perte du syst�me $param$
		La perte du syst�me $param$ a enrag� les Vi
#18	 Perdez un syst�me � la r�bellion
		Les citoyens de l'ensemble de la coalition condamnent la r�bellion dans le syst�me $param$
		La r�bellion dans le secteur $param$ a provoqu� une baisse importante de l'�change
		Les guerriers se sentent furieux de la r�bellion dans le secteur $param$
		Le peuple ne peut pas comprendre la r�bellion dans le secteur $param$
		De nombreux citoyens de Cartare semblent sympathiser avec les rebelles de $param$
		La r�bellion dans le syst�me $param$ a offens� les Vi
#19	 Bombarder un syst�me
		L' attentat contre $param$ a �rod� le soutien populaire � nos militaires
		La Bourse a enregistr� de graves pertes aujourd'hui en r�ponse � l'attentat � la bombe contre $param$
		On pense que le bombardement de $param$ prouve que nous sommes toujours Khaoron
		De nombreux dissidents vocaux ont �t� r�duits au silence par le bombardement de $param$
		Le bombardement du syst�me $param$ a fait taire plusieurs dissidents
		Le bombardement du syst�me $param$ attire l'attention des Vi
#20	 Bombarder un syst�me natif qui s'est rebell�
		L'attaque contre le syst�me rebelle $param$ r�duit le support � nos politiques militaires
		La Bourse a affich� plusieurs pertes aujourd'hui en r�ponse � l'attentat � la bombe contre $param$
		Le bombardement de nos fr�res infid�les dans le syst�me $param$ pla�t aux vrais Khaorons
		Le bombardement du syst�me rebel $param$ peut facilement �tre camoufl�
		Le bombardement du syst�me de voyous $param$ r�jouit l'Union
		Les Vi justifient le bombardement des rebelles dans le syst�me $param$
#21	 �liminer compl�tement une race mineure
		Protestations: L'extinction des $param$ rend le peuple indign�
		Extinction des $param$: la Bourse r�agit avec une forte baisse de toutes les activit�s de vente
		Les Khaorons c�l�brent l'extinction des $param$
		Le peuple condamne l'extinction de $param$,
		L'extinction des $param$ est justifi�e: ils ne valaient pas la peine de vivre
		L'extermination des $param$ pla�t aux Vi
#22	Subir les bombardement sur le Syst�me
		Le peuple demande la fin du bombardement de $param$
		Le bombardement de $param$ ravagent des march�s
		Les Khaorons r�clament: Aidez nos fr�res sur $param$
		Le peuple a demand� au S�nat d'intervenir contre le bombardement de $param$
		Les citoyens de l'Union n'ont aucune sympathie pour la population de $param$
		Les Vi demandent l'arr�t imm�diat des bombardements sur $param$
#23	 Perte de 50% de la pop dans les bombardements sur le Sys. natal
		""
		""
		""
		""
		""
		""
#24	D�clarez la guerre contre un Empire Neutre
		Plusieurs �ditoriaux populaires ont fortement critiqu� la guerre contre $param$
		R�action positive � la d�claration de guerre contre $param$
		Tous les guerriers attendent la guerre contre $param$
		Le peuple est heureux que nous pouvons combattre $param$
		Le peuple re�oit cette guerre contre $param$ avec des acclamations
		Les Vi ont h�te de faire la guerre au $param$
#25	 D�clarer la guerre � un Empire d�tenant un Pacte de Non-Agression
		De nombreux citoyens ont le sentiment qu'en trahissant $param$, nous avons trahi notre propre peuple
		Les r�actions sont divis�es concernant la d�claration de guerre contre $param$
		Tous les vrais guerriers attendent la guerre contre $param$
		Le peuple est heureux de pouvoir combattre $param$
		Le peuple re�oit cette guerre contre $param$ avec des acclamations
		Les Vi ont h�te de faire la guerre au $param$
#26	 D�clarer la guerre � un Empire d�tenant un Trait� Commercial
		La majorit� des citoyens a le sentiment qu'en trahissant $param$, nous avons trahi notre propre peuple
		La rupture de notre pacte avec $param$ choque les investisseurs
		Notre guerre contre $param$ plait � certains guerriers
		Le peuple est heureux que nous exterminions $param$
		Le peuple re�oit cette guerre contre $param$ avec des acclamations
		Les Vi ont h�te de faire la guerre au $param$
#27	 D�clarer la guerre � un Empire d�tenant un Trait� d'Amiti�
		La majorit� des citoyens a le sentiment qu'en trahissant $param$, nous avons trahi notre propre peuple
		La rupture de notre pacte avec $param$ sera co�teuse
		Notre violation du code d'honneur avec $param$ n'a pas �t� bien re�ue
		Certains citoyens agit�s ont remis en question notre violation du trait� avec $param$
		Le peuple acclame cette nouvelle guerre contre $param$
		Les Vi ont h�te de faire la guerre au $param$
#28	D�clarer la guerre � un Empire d�tenant le pacte de d�fense
		Presque tous les citoyens ont le sentiment qu'en trahissant $param$, nous avons trahi notre propre peuple
		La rupture de notre pacte avec $param$ sera co�teuse
		Notre violation du code d'honneur avec $param$ n'a pas �t� bien re�ue
		De nombreux citoyens agit�s ont remis en question notre violation du trait� avec $param$
		Le peuple a des sentiments mitig�s concernant cette guerre contre $param$
		Les Vi ont h�te de faire la guerre au $param$
#29	 D�clarer la guerre � un Empire d�tenant un Trait� de Coop�ration
		Presque tous les citoyens ont le sentiment qu'en trahissant $param$, nous avons trahi notre propre peuple
		La rupture de notre pacte avec $param$ sera extr�mement co�teuse
		Notre violation du code d'honneur avec $param$ n'a pas �t� bien re�ue
		Des citoyens agit�s de tout l'Empire ont remis en question notre violation du trait� avec $param$
		Les citoyens m�prisent notre guerre contre $param$.
		Les Vi ont h�te de faire la guerre au $param$
#30	 D�clarer la guerre � un Empire d�tenant une Affiliation
		Notre guerre contre $param$ provoque des manifestations violentes dans tous les syst�mes
		Les investisseurs sont choqu�s par cette guerre contre $param$
		Notre violation du code d'honneur avec $param$ a �t� rejet�e par la plupart des guerriers
		La population enti�re a de graves probl�mes avec la guerre contre $param$
		De nombreux citoyens de Cartare ont �t� choqu�s d'apprendre que nous avons trahi $param$
		Les Vi ont h�te de faire la guerre au $param$
#31	 Un Empire Neutre vous d�clare la guerre
		La guerre contre $param$ a renforc� le soutien � nos politiques militaires
		Les analystes estiment que la guerre contre $param$ sera tr�s rentable
		Tous les vrais guerriers attendent avec impatience la guerre contre $param$
		Le peuple est heureux que nous r�glions les comptes avec $param$
		La chance de faire ses preuves contre $param$ a stimul� les recrutements
		Les Vi ont h�te de faire la guerre au $param$
#32	L'autre Empire d�clare la guerre avec un Trait�
		La guerre contre $param$ a renforc� le soutien � nos politiques militaires
		Les analystes estiment que la guerre contre $param$ sera tr�s rentable
		Tous les vrais guerriers attendent avec impatience la guerre contre $param$
		Le peuple est heureux que nous r�glions leurs comptes aux $param$
		La chance de faire ses preuves contre $param$ a stimul� les recrutements
		Les Vi ont h�te de faire la guerre au $param$
#33	 L'autre Empire d�clare la guerre avec une Affiliation
		La guerre contre $param$ a renforc� le soutien � nos politiques militaires
		Les analystes estiment que la guerre contre $param$ sera tr�s rentable
		Tous les vrais guerriers attendent avec impatience la guerre contre $param$
		Le peuple est heureux que nous allons maintenant r�gler son compte au $param$ pour de bon
		La chance de faire ses preuves contre $param$ a stimul� les recrutements
		Les Vi ont h�te de faire la guerre avec les $param$
#34	 Signer un Trait� de Commerce
		Les nouveaux �changes commerciaux avec les $param$ ravissent le peuple
		Les entrepreneurs ont h�te de n�gocier avec les $param$
		Le commerce avec les $param$ n'int�resse pas beaucoup les vrais guerriers
		Le peuple semble approuver notre arrangement avec les $param$
		Les nouvelles de notre trait� de paix avec les $param$ ont �t� bien re�ues par nos citoyens
		Les Vi n'ont eu aucune appr�hension pour un trait� avec les $param$
#35	 Signer un Trait� d'Amiti� et de Coop�ration
		Notre trait� avec les $param$ a apais� les craintes de nombreux citoyens inquiets
		Le nouveau contrat avec les $param$ est bon pour les affaires
		Les guerriers comprennent la n�cessit� de signer ce trait� avec les $param$
		Le peuple semble approuver notre arrangement avec les $param$
		Les nouvelles de notre trait� de paix avec les $param$ ont �t� chaleureusement re�ues par nos citoyens
		Les Vi n'ont absolument aucune appr�hension pour un trait� avec les $param$
#36	Signer un Trait� d'Affiliation
		Le soutien populaire � notre alliance avec les $param$ est vraiment �crasant
		La Bourse a atteint un niveau record alors que les courtiers c�l�brent notre alliance avec les $param$
		Le peuple croit que nous avons trouv� des alli�s dignes de $param$
		Le peuple croit que notre alliance avec les $param$ sera notre salut
		La r�ponse du public � notre alliance avec les $param$ est tr�s favorable
		La r�ponse des Vi � notre alliance avec les $param$ est tr�s favorable
#37	 Recevoir l'Acceptation du Pacte de Non-Agression
		Notre trait� avec les $param$ a apais� les craintes de nombreux citoyens inquiets
		Les entrepreneurs attendent avec impatience de nouvelles opportunit�s parmi $param$
		Le peuple est d��u qu'il n'y ait pas de batailles avec les $param$
		Le peuple semble approuver notre arrangement avec les $param$
		Les nouvelles de notre trait� de paix avec les $param$ ont �t� bien re�ues par nos citoyens
		Les Vi n'ont absolument aucune appr�hension pour un trait� avec les $param$
#38 	Accepter un Pacte de Non-Agression
		Notre trait� avec les $param$ a apais� les craintes de nombreux citoyens inquiets
		Les entrepreneurs attendent avec impatience de nouvelles opportunit�s parmi les $param$
		Le peuple est d��u qu'il n'y ait pas de combat avec les $param$
		Le peuple semble approuver notre arrangement avec les $param$
		Les nouvelles de notre trait� de paix avec les $param$ ont �t� bien re�ues par nos citoyens
		Les Vi n'ont absolument aucune appr�hension pour un trait� avec les $param$
#39	Recevoir l'Acceptation d'un Pacte de D�fense
		""
		""
		""
		""
		""
		""
#40	Accepter un Pacte de D�fense
		""
		""
		""
		""
		""
		""
#41	Recevoir une acceptation d'un Pacte de Guerre
		""
		""
		""
		""
		""
		""
#42	Accepter un Pacte de Guerre
		""
		""
		""
		""
		""
		""
#43	D�cliner un trait� (Commerce)
		""
		""
		""
		""
		""
		""
#44	D�cliner un trait� (Amiti�, Coop)
		""
		""
		""
		""
		""
		""
#45	D�cliner un trait� (Affiliation)
		""
		""
		""
		""
		""
		""
#46	Refuser un trait� (non-agression, pacte de d�fense)
		Les activistes ont fermement condamn� notre refus de paix avec les $param$
		Les investisseurs ont �t� d��us qu'il n'y ait pas de trait� de paix avec les $param$
		Notre peuple est ravi d'apprendre que la paix a �t� �vit�e
		Le peuple est ravi que notre statut avec les $param$ ne change pas
		Nos citoyens sont d��us d'entendre qu'il n'y aura pas de paix avec les $param$
		Les Vi sont heureux d'apprendre que la paix avec les $param$ n'est pas n�cessaire
# 47 Refuser un trait� (Warpact)
		""
		""
		""
		""
		""
		""
#48	Accepter une Requ�te
		""
		""
		""
		""
		""
		""
#49	Refuser une requ�te
		""
		""
		""
		""
		""
		""
#50	Recevoir l'acceptation d'une Requ�te
		""
		""
		""
		""
		""
		""
#51	Accepter la Capitulation
		""
		""
		""
		""
		""
		""
#52	Recevoir une Capitulation
		""
		""
		""
		""
		""
		""
#53	Accepter la 'Victoire' Trait� de Non-Aggression
		""
		""
		""
		""
		""
		""
#54	Recevoir l'Acceptation de demande de Trait� de Victoire
		""
		""
		""
		""
		""
		""
#55	Accepter la requ�te d'un syst�me pour l'Ind�pendance
		""
		""
		""
		""
		""
		""
#56	Refuser la requ�te d'un syst�me pour l'Ind�pendance
		""
		""
		""
		""
		""
		""
#57	Repousser une attaque de Borg/Droid
		""
		""
		""
		""
		""
		""
#58	Perdre la population d'un syst�me � cause d'un �v�nement naturel
		""
		""
		""
		""
		""
		""
#59	Espionnage r�ussi
		Les activit�s d'espionnage n'ont aucun impact sur l'opinion du peuple
		Nos marchands se concentrent plut�t sur les profits que sur l'espionnage
		Les guerriers appellent nos activit�s d'espionnage d�shonorantes
		Le peuple approuve les m�thodes d'espionnage du Tal'Shiar
		Les citoyens de l'Union sont l�g�rement satisfaits des activit�s d'espionnage
		Les Vi s'amusent de nos activit�s d'espionnage
#60	 Sabotage r�ussi
		Les activit�s de sabotage n'ont aucun impact sur l'opinion du peuple
		Nos marchands se concentrent plut�t sur les profits que sur les missions de sabotage
		Les guerriers appellent nos activit�s de sabotage d�shonorantes
		Le peuple approuve les m�thodes de sabotage du Tal'Shiar
		Les citoyens de l'Union sont l�g�rement satisfaits des activit�s de sabotage
		Les Vi s'amusent de notre sabotage d'espionnage
