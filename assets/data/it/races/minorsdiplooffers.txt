NOTHING_SPECIAL:
We like to offer you a trade agreement. The advantages for both sides can not be disproved. Are you onboard?
Our relationships go far beyond mere economic business. We hope for a signation of a friendship treaty at your earliest convenience.
Our strategists have argued in favor of a military cooperation. We would like to hereby submit a corresponding proposal to you.
We should not limit our cooperation on the military sector. An alliance treaty would further intensify our relationships. We hope for confirmation at your earliest convenience.
After due consideration we have decided to offer you our membership.
Our people cannot tolerate your behaviour any longer. You are from now on in the state of war with our world.
FINANCIAL:
To both maximize our profits, we offer your people an agreement about intensive trade relationships. You're on it?
The intensification of our friendly relationship will undoubtedly have positive impulses on the economic interactions between our peoples.
A military cooperation would not have just financial advantages for both our worlds. We hope to convince you with the enclosed contract that this is the most profitable way to secure the military predominance in this sector.
The logical consequence of a further intensification of our relationship is an alliance. Predicted profits exclude any denial. We are certain your analysts will agree to that.
Each long-term welfare prognosis leads our leading economy experts to the sole conclusion, a membership in your empire. We hereby officially submit a request of membership.
On the latest conference of the 4 biggest interplanetary armament trusts your destruction was decided with absolute majority. We are in state of war!
WARLIKE:
To guarantee the upkeep of both our forces for future times, we propose a trade agreement to your people.
It becomes more and more obvious that we are no enemies. On the contrary, let us seal our friendship by this treaty.
Our forces are strong. Together we would even be stronger! The strategic advantages of a military cooperation are not to be dismissed.
Our enemies will have nothing to oppose an alliance between our worlds. Our strategists have manufactured a corresponding treaty. We hope on your approval!
It would fill our warriors' hearts with pride to be able to serve under your command. Our people hereby offers you full membership.
Prepare to be perished by our forces! War!
FARMER:
To secure both our food supplies we should strive for a better economic relationship. We hope you recognize the benefit of such a trade agreement.
Nothing speaks against a more friendly relationship between our systems and worlds. We should seal this with this treaty.
We should mutually secure our systems. Do you agree with a military cooperation?
An alliance between our worlds would momentarily be an ideal agreement. We hope your government will see this the same way.
Our people strives for membership in your empire for such a long time. Finally a corresponding request was drafted complete. We hope for a positive resonance.
You are a plague for our people! We have nothing left to solve it other than by declaring war on you in this situation!
INDUSTRIAL:
The productivity of both parties could clearly be optimized by signing this trade agreement. We are certain that your industry experts agree with us.
Our relationship goes far beyond mere business. Not just from our industries the voices get louder for a friendship treaty. We hope you take this agreement.
We propose you a bundling of a military ressources. The striking power of our armies would be maximized enormously!
So far we do not have maxed out our possibilities of cooperation. We propose you a deeper alliance between our worlds!
We are certain to add an enormous contribution to your productivity within your empire. Our government hopes you are ready to accept our request for full membership.
Our production has been totally set on military goods. A friendly coexistence is from now on out of option! Take good notice of our declaration of war now!
SECRET:
We give your people the possibility to start economic relationships with us!
We are no opponents. There is nothing to be said against a friendly relationship between our worlds. This could be the beginning of a mighty connection!
You know, like we do, that our sphere of influence could massively be extended by a military cooperation. Are you interested in a corresponding treaty?
Our cooperation develops further and further. The logical consequence is a treaty of alliance!
Let us be a part of your empire! You know about our abilities.
We cannot tolerate the threat of your people any longer. We officially declare war on you!
RESEARCHER:
An intensive exchange of knowledge and goods will be of benefit for both of us. A trade agreement is an ideal agreement!
Out of the cooperation of our scientific departements there has beyond all doubt developed into a true friendship. Let us seal this with a friendship treaty.
The logical consequence in terms of a military cooperation would mean a clearly improved situation in the security sector for both sides. We hope such an agreement is in your interest too.
Numerous breakthroughs in various areas would virtually be preprogrammed by an alliance of our worlds. The possibilities would be infinite! A denial would be illogical.
The scientific frameworks in our empires are fantastic. We have come to the conclusion to offer you our membership.
Your behaviour lacks every bit of logic. We cannot tolerate this any longer. A war cannot be avoided!
PRODUCER:
Our world is interested in an intensive exchange of ressources and other trade goods with you. A corresponding treaty is ready for your signation.
We see you no longer as mere business partners. What's more we propose you a friendship treaty. This could mark the beginning of an even deeper partnership.
Our military advisers propose an extended cooperation on the military sector. The striking power of the resulting armada would greatly surpass the one of the single fleets alone.
We should further extend our relationship between our worlds. In our view, an alliance would constitute an ideal constellation.
Let us further strengthen your empire by full membership. Our ressources will provide an enormous help for your systems.
We do know how to defend our ressources! There is war now between our worlds!
PACIFIST:
The trade agreement shall constitute the basis for a friendly coexistence of both our peoples. We hope for a positive response.
Our people is very interested in a friendly relationship of our worlds. Your approval would secure enduring peace between us.
To secure peace we should strive for a military cooperation. Let us exclude any military confrontation for the future to come.
We should ally. This alliance would further stabilize this sector and make war even more impossible.
We hereby officially file a request for membership. Let us work together peacefully as part of your empire.
No, war is dumb!
SNEAKY:
If you want to trade with us, shake with us! If not, we will look out for a more competent partner.
Strong friends are useful. We hope you recognize the possibilities of such a treaty.
Nobody will be able to stop us in this sector with a military cooperation! Don't be a fool and take our offer.
An alliance would greatly expand our political influence in this area of space. No one will be able to elude the power of our worlds.
You lead your empire with a skillful hand. We long for a full membership. You should accept.
We do not need you any longer. That means war!